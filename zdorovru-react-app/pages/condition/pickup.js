import React from 'react'
import Pickup from '../../src/components/Pages/Condition/Pickup/Pickup'
import { RuleEnum } from '../../src/consts'
import { getRule } from '../../src/api/siteApis'
import Head from 'next/head'
import MobileDetect from 'mobile-detect'
import PropTypes from 'prop-types'
import PageWithPadding from '@mobileShared/PageWithPadding/PageWithPadding'
import { useRouter } from 'next/router'

export default function PickupIndex(props) {
    const router = useRouter()
    if (props.mobile)
        return (
            <PageWithPadding
                headerText="Условия самовывоза"
                pageHeaderAttributes={{
                    backClickHandler: async () => {
                        await router.push('/account')
                    },
                    isShowArrowBack: true
                }}>
                <div
                    dangerouslySetInnerHTML={{
                        __html: props.bodyHtml
                    }}
                />
            </PageWithPadding>
        )
    return (
        <React.Fragment>
            <Head>
                <title>Самовывоз и заказ лекарств - сеть аптек ЗДОРОВ.ру</title>
            </Head>
            <Pickup>{props.bodyHtml}</Pickup>
        </React.Fragment>
    )
}
PickupIndex.propTypes = {
    bodyHtml: PropTypes.string.isRequired,
    mobile: PropTypes.bool.isRequired
}
export const getServerSideProps = async (ctx) => {
    const response = await getRule(RuleEnum.Pickup)
    const md = new MobileDetect(ctx.req.headers['user-agent'])
    return { props: { bodyHtml: response.bodyHtml, mobile: md.mobile() } }
}
