import React from 'react'
import { RuleEnum } from 'src/consts'
import { getRule } from '@api/siteApis'
import Head from 'next/head'
import { useRouter } from 'next/router'
import PageWithPadding from '@mobileShared/PageWithPadding/PageWithPadding'
import PropTypes from 'prop-types'
import MobileDetect from 'mobile-detect'
import Delivery from '../../src/components/Pages/Condition/Delivery/Delivery'

export default function DeliveryIndex(props) {
    const router = useRouter()
    if (props.mobile)
        return (
            <PageWithPadding
                headerText="Условия доставки"
                pageHeaderAttributes={{
                    backClickHandler: async () => {
                        await router.push('/account')
                    },
                    isShowArrowBack: true
                }}>
                <div
                    dangerouslySetInnerHTML={{
                        __html: props.bodyHtml
                    }}
                />
            </PageWithPadding>
        )
    return (
        <React.Fragment>
            <Head>
                <title>
                    Доставка лекарств - интернет-аптека ЗДОРОВ.ру. Быстрая и бесплатная доставка товаров для
                    здоровья.
                </title>
            </Head>
            <Delivery>{props.bodyHtml}</Delivery>
        </React.Fragment>
    )
}
DeliveryIndex.propTypes = {
    bodyHtml: PropTypes.string.isRequired,
    mobile: PropTypes.bool.isRequired
}

export const getServerSideProps = async (ctx) => {
    const response = await getRule(RuleEnum.Delivery)
    const md = new MobileDetect(ctx.req.headers['user-agent'])
    return { props: { bodyHtml: response.bodyHtml, mobile: md.mobile() } }
}
