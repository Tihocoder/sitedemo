import React from 'react'
import { getRule } from '@api/siteApis'
import { RuleEnum } from 'src/consts'
import PropTypes from 'prop-types'
import MobileDetect from 'mobile-detect'
import PageWithPadding from '@mobileShared/PageWithPadding/PageWithPadding'
import { useRouter } from 'next/router'
import Head from 'next/head'
import TermsOfUse from '@components/Pages/Company/TermsOfUse/TermsOfUse'

export default function TermsOfUseIndex(props) {
    const router = useRouter()

    if (props.mobile)
        return (
            <PageWithPadding
                headerText="Пользовательское соглашение"
                pageHeaderAttributes={{
                    backClickHandler: async () => {
                        await router.push('/account')
                    },
                    isShowArrowBack: true
                }}>
                <div
                    dangerouslySetInnerHTML={{
                        __html: props.bodyHtml
                    }}
                />
            </PageWithPadding>
        )

    return (
        <React.Fragment>
            <Head>
                <title>Пользовательское соглашение - интернет аптека ЗДОРОВ.ру</title>
            </Head>
            <TermsOfUse>{props.bodyHtml}</TermsOfUse>
        </React.Fragment>
    )
}
TermsOfUseIndex.propTypes = {
    bodyHtml: PropTypes.string.isRequired,
    mobile: PropTypes.bool.isRequired
}
export const getServerSideProps = async (ctx) => {
    const response = await getRule(RuleEnum.TermsOfUse)
    const md = new MobileDetect(ctx.req.headers['user-agent'])
    return { props: { bodyHtml: response.bodyHtml, mobile: md.mobile() } }
}
