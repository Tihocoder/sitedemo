import React from 'react'
import Head from 'next/head'
import { getRule } from '@api/siteApis'
import { RuleEnum } from 'src/consts'
import MobileDetect from 'mobile-detect'
import MobilePageHeader from '@mobileShared/MobilePageHeader/MobilePageHeader'
import About from '../../src/components/Pages/Company/About/About'
import { useRouter } from 'next/router'
import { MobileBody } from '@styles/ui-element.styled'
import MobileHeader from "@mobileShared/MobileHeader/MobileHeader";

export default function AboutIndex(props) {
    const router = useRouter()
    if (props.mobile) {
        return (
            <React.Fragment>
                <Head>
                    <title>О компании - сеть аптек ЗДОРОВ.ру</title>
                </Head>
                <MobileHeader
                    backClickHandler={async () => {
                        await router.push('/account')
                    }}>
                    О компании
                </MobileHeader>
                <MobileBody>
                    <About isMobile />
                </MobileBody>
            </React.Fragment>
        )
    }
    return (
        <React.Fragment>
            <Head>
                <title>О компании - сеть аптек ЗДОРОВ.ру</title>
            </Head>
            <About />
        </React.Fragment>
    )
}

export const getServerSideProps = async (ctx) => {
    const response = await getRule(RuleEnum.PrivacyPolicy)
    const md = new MobileDetect(ctx.req.headers['user-agent'])
    return { props: { bodyHtml: response.bodyHtml, mobile: md.mobile() } }
}
