import React from 'react'
import Head from 'next/head'
import Vacancies from '../../src/components/Pages/Company/Vacancies/Vacancies'

export default function VacanciesIndex() {
    return (
        <React.Fragment>
            <Head>
                <title>Вакансии в сети аптек ЗДОРОВ.ру</title>
            </Head>
            <Vacancies />
        </React.Fragment>
    )
}
