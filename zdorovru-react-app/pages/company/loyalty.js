import React from 'react'
import { getBonusCartsCampaign, getGiftCardsCampaigns } from '../../src/api/siteApis'
import Loyalty from '../../src/components/Pages/Company/Loyalty/Loyalty'
import nookies from 'nookies'
import Head from 'next/head'

export default function LoyaltyIndex(props) {
    return (
        <React.Fragment>
            <Head>
                <title>Программа лояльности в сети аптек ЗДОРОВ.ру</title>
            </Head>
            <Loyalty
                bonusCartsCampaign={props.bonusCartsCampaign}
                giftCardsCampaigns={props.giftCardsCampaigns}
            />
        </React.Fragment>
    )
}
export const getServerSideProps = async (ctx) => {
    const cookies = nookies.get(ctx)
    const shipmentCookie = cookies['storage-shipment']
    const shipment = shipmentCookie ? JSON.parse(shipmentCookie) : { cityId: 1, stockId: 0 }
    const bonusCartsCampaign = await getBonusCartsCampaign(shipment)
    const giftCardsCampaigns = await getGiftCardsCampaigns(shipment)
    return { props: { bonusCartsCampaign, giftCardsCampaigns } }
}
