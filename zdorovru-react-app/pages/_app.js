import React from 'react'
import ym, { YMInitializer } from 'react-yandex-metrika'
import { Provider } from 'react-redux'
import Router from 'next/router'
import '../src/App.css'
import NProgress from 'nprogress' // nprogress module
import { ThemeProvider } from 'styled-components'
import 'url-search-params-polyfill'
import { GoogleReCaptchaProvider } from 'react-google-recaptcha-v3'
import Head from 'next/head'
import { useStore } from 'src/store'
import AlertsState from '@context/AlertsState'
import { getUA } from 'react-device-detect'
import MobileDetect from 'mobile-detect'
import * as Theme from '../src/styles/theme-const'
import Base from '../src/components/Layout/Base'
import MobileLayout from '../src/components/Layout/MobileLayout'

const isProduction = process.env.ENVS === 'prod'

function scrollPositionRestorer() {
    const scrollMemories = {}
    let isPop = false

    if (process.browser) {
        window.history.scrollRestoration = 'manual'
        window.onpopstate = () => {
            isPop = true
        }
    }

    Router.events.on('routeChangeStart', (url) => {
        saveScroll()
        NProgress.start()
        if (isProduction) {
            ym('hit', url)
        }
    })

    Router.events.on('routeChangeComplete', () => {
        NProgress.done()
        restoreScroll()
    })
    Router.events.on('routeChangeError', () => NProgress.done())

    function saveScroll() {
        scrollMemories[Router.asPath] = window.scrollY
    }

    function restoreScroll() {
        const prevScrollY = scrollMemories[Router.asPath]
        if (prevScrollY !== undefined) {
            window.requestAnimationFrame(() => window.scrollTo(0, prevScrollY))
        }
    }

    function scrollToTop() {
        window.requestAnimationFrame(() => window.scrollTo(0, 0))
    }
}

scrollPositionRestorer()

const App = ({ Component, pageProps, router }) => {
    const store = useStore(pageProps.initialReduxState)
    const md = new MobileDetect(getUA)
    const isCurrentMobile = pageProps.mobile || !!md.mobile()

    if (router.pathname.startsWith('/contact/feedback') && !isCurrentMobile) {
        return (
            <Provider store={store}>
                <GoogleReCaptchaProvider reCaptchaKey="6LdbOf8UAAAAAFAj22u14hzBaL-7yxQ7EDRmlLu3">
                    <Head>
                        <meta httpEquiv="Content-Type" content="text/html; charset=utf-8" />
                        <meta name="language" content="ru" />
                        <title>Интернет-аптека, заказ и доставка лекарств в сети аптек ЗДОРОВ.ру</title>
                        <link rel="icon" href="/favicon.ico" />
                        <meta name="yandex-verification" content="62458a841a05f28f" />
                        {/* <meta
                            name="viewport"
                            content="width=device-width, initial-scale=1.0, maximum-scale=1.0"
                        /> */}
                        <link rel="icon" type="image/png" href="/favicon.ico" />
                        <meta name="apple-mobile-web-app-title" content="Zdorov.ru" />
                        <meta name="application-name" content="Zdorov.ru" />
                        <meta name="theme-color" content="#ffffff" />
                        <meta name="apple-itunes-app" content="app-id=1252077901" />
                    </Head>
                    {isProduction && (
                        <YMInitializer
                            accounts={[26662365]}
                            options={{
                                defer: true,
                                clickmap: true,
                                trackLinks: true,
                                accurateTrackBounce: true,
                                webvisor: true
                            }}
                        />
                    )}

                    <ThemeProvider
                        theme={{
                            colors: Theme.ColorConsts,
                            shadows: Theme.shadowMaterialList
                        }}>
                        <Base>
                            {/* eslint-disable-next-line react/jsx-props-no-spreading */}
                            <Component {...pageProps} />
                        </Base>
                    </ThemeProvider>
                </GoogleReCaptchaProvider>
            </Provider>
        )
    }
    // eslint-disable-next-line react/prop-types
    if (router.pathname.startsWith('/contact/feedback') && isCurrentMobile) {
        return (
            <Provider store={store}>
                <GoogleReCaptchaProvider reCaptchaKey="6LdbOf8UAAAAAFAj22u14hzBaL-7yxQ7EDRmlLu3">
                    <React.Fragment>
                        <Head>
                            <meta httpEquiv="Content-Type" content="text/html; charset=utf-8" />
                            <meta name="language" content="ru" />
                            <meta name="viewport" content="width=device-width, initial-scale=1" />
                            <title>Интернет-аптека, заказ и доставка лекарств в сети аптек ЗДОРОВ.ру</title>
                            <link rel="icon" href="/favicon.ico" />
                            <meta name="yandex-verification" content="62458a841a05f28f" />
                            <link rel="icon" type="image/png" href="/favicon.ico" />
                            <meta name="apple-mobile-web-app-title" content="Zdorov.ru" />
                            <meta name="application-name" content="Zdorov.ru" />
                            <meta name="theme-color" content="#ffffff" />
                            <meta name="apple-itunes-app" content="app-id=1252077901" />
                        </Head>
                        {isProduction && (
                            <YMInitializer
                                accounts={[26662365]}
                                options={{
                                    defer: true,
                                    clickmap: true,
                                    trackLinks: true,
                                    accurateTrackBounce: true,
                                    webvisor: true
                                }}
                                version="2"
                            />
                        )}
                        {isProduction && (
                            <script
                                type="text/javascript"
                                dangerouslySetInnerHTML={{
                                    __html: `
                                    (function(w, d, s, p) { var f = d.getElementsByTagName(s)[0], j = d.createElement(s); j.async = true; 
                                        j.src = '//cdn.rutarget.ru/static/tag/tag.js'; f.parentNode.insertBefore(j, f); 
                                        w[p] = {rtgNoSync: false, rtgSyncFrame: true}; })(window, document, 'script', '_rtgParams');
                      `
                                }}
                            />
                        )}
                        <ThemeProvider
                            theme={{
                                colors: Theme.ColorConsts,
                                shadows: Theme.shadowMaterialList
                            }}>
                            <AlertsState>
                                <MobileLayout>
                                    <Component {...pageProps} />
                                </MobileLayout>
                            </AlertsState>
                        </ThemeProvider>
                        {isProduction && (
                            <script
                                dangerouslySetInnerHTML={{
                                    __html: `
                                    var _rutarget = window._rutarget || [];
                                    _rutarget.push({'event': 'otherPage'});
                      `
                                }}
                            />
                        )}
                    </React.Fragment>
                </GoogleReCaptchaProvider>
            </Provider>
        )
    }
    if (isCurrentMobile) {
        return (
            <Provider store={store}>
                <React.Fragment>
                    <Head>
                        <meta httpEquiv="Content-Type" content="text/html; charset=utf-8" />
                        <meta name="language" content="ru" />
                        <title>Интернет-аптека, заказ и доставка лекарств в сети аптек ЗДОРОВ.ру</title>
                        <link rel="icon" href="/favicon.ico" />
                        <meta name="yandex-verification" content="62458a841a05f28f" />
                        <link rel="icon" type="image/png" href="/favicon.ico" />
                        <meta name="viewport" content="width=device-width, initial-scale=1" />
                        <meta name="apple-mobile-web-app-title" content="Zdorov.ru" />
                        <meta name="application-name" content="Zdorov.ru" />
                        <meta name="theme-color" content="#ffffff" />
                        <meta name="apple-itunes-app" content="app-id=1252077901" />
                    </Head>
                    {isProduction && (
                        <YMInitializer
                            accounts={[26662365]}
                            options={{
                                defer: true,
                                clickmap: true,
                                trackLinks: true,
                                accurateTrackBounce: true,
                                webvisor: true
                            }}
                            version="2"
                        />
                    )}
                    {isProduction && (
                        <script
                            type="text/javascript"
                            dangerouslySetInnerHTML={{
                                __html: `
                                    (function(w, d, s, p) { var f = d.getElementsByTagName(s)[0], j = d.createElement(s); j.async = true; 
                                        j.src = '//cdn.rutarget.ru/static/tag/tag.js'; f.parentNode.insertBefore(j, f); 
                                        w[p] = {rtgNoSync: false, rtgSyncFrame: true}; })(window, document, 'script', '_rtgParams');
                      `
                            }}
                        />
                    )}
                    <ThemeProvider
                        theme={{
                            colors: Theme.ColorConsts,
                            shadows: Theme.shadowMaterialList
                        }}>
                        <AlertsState>
                            <MobileLayout>
                                <Component {...pageProps} />
                            </MobileLayout>
                        </AlertsState>
                    </ThemeProvider>
                    {isProduction && (
                        <script
                            dangerouslySetInnerHTML={{
                                __html: `
                                    var _rutarget = window._rutarget || [];
                                    _rutarget.push({'event': 'otherPage'});
                      `
                            }}
                        />
                    )}
                </React.Fragment>
            </Provider>
        )
    }
    return (
        <Provider store={store}>
            <React.Fragment>
                <Head>
                    <meta httpEquiv="Content-Type" content="text/html; charset=utf-8" />
                    <meta name="language" content="ru" />
                    <title>Интернет-аптека, заказ и доставка лекарств в сети аптек ЗДОРОВ.ру</title>
                    <link rel="icon" href="/favicon.ico" />
                    <meta name="yandex-verification" content="62458a841a05f28f" />
                    <link rel="icon" type="image/png" href="/favicon.ico" />

                    <meta name="apple-mobile-web-app-title" content="Zdorov.ru" />
                    <meta name="application-name" content="Zdorov.ru" />
                    <meta name="theme-color" content="#ffffff" />
                    <meta name="apple-itunes-app" content="app-id=1252077901" />
                </Head>
                {isProduction && (
                    <YMInitializer
                        accounts={[26662365]}
                        options={{
                            defer: true,
                            clickmap: true,
                            trackLinks: true,
                            accurateTrackBounce: true,
                            webvisor: true
                        }}
                        version="2"
                    />
                )}
                {isProduction && (
                    <script
                        type="text/javascript"
                        dangerouslySetInnerHTML={{
                            __html: `
                                (function(w, d, s, p) { var f = d.getElementsByTagName(s)[0], j = d.createElement(s); j.async = true; 
                                    j.src = '//cdn.rutarget.ru/static/tag/tag.js'; f.parentNode.insertBefore(j, f); 
                                    w[p] = {rtgNoSync: false, rtgSyncFrame: true}; })(window, document, 'script', '_rtgParams');
                  `
                        }}
                    />
                )}
                <ThemeProvider
                    theme={{
                        colors: Theme.ColorConsts,
                        shadows: Theme.shadowMaterialList
                    }}>
                    <Base>
                        <Component {...pageProps} />
                    </Base>
                </ThemeProvider>
                {isProduction && (
                    <script
                        dangerouslySetInnerHTML={{
                            __html: `
                                var _rutarget = window._rutarget || [];
                                _rutarget.push({'event': 'otherPage'});
                  `
                        }}
                    />
                )}
            </React.Fragment>
        </Provider>
    )
}

export default App
