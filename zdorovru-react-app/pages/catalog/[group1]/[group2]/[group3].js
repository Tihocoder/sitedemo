import { PageWrapper } from '@styles/base.styled'
import Group from '../../../../src/components/Group/Group'
import { initializeStore } from '../../../../src/store'
import nookies from 'nookies'
import { initGoodsByGroupId } from '@actions/catalog'
import MobileGroupPage from '../../../../src/components/mobilePages/catalog/MobileGroupPage/MobileGroupPage'
import MobileDetect from 'mobile-detect'
import React from 'react'

export default function Group3Index(props) {
    if (props.mobile) {
        return <MobileGroupPage isAvaliableGroups={false} groupId={props.groupId} groupPosition="group3" />
    }
    return (
        <PageWrapper>
            <Group groupId={props.groupId} />
        </PageWrapper>
    )
}
export const getServerSideProps = async (ctx) => {
    const reduxStore = initializeStore()
    const { dispatch } = reduxStore
    const groupId = parseInt(ctx.params.group3, 10)
    const md = new MobileDetect(ctx.req.headers['user-agent'])
    const cookies = nookies.get(ctx)
    const shipmentCookie = cookies['storage-shipment']
    const shipment = shipmentCookie ? JSON.parse(shipmentCookie) : { stockId: 0, cityId: 1 }
    await dispatch(initGoodsByGroupId(groupId, shipment))
    return {
        props: { groupId, initialReduxState: reduxStore.getState(), mobile: md.mobile() }
    }
}
