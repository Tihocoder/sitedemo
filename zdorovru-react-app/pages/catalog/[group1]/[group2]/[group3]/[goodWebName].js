import React from 'react'
import Head from 'next/head'
import nookies from 'nookies'
import { getGoodByWebGoodId, getGroupsByWebGoodId } from '@api/siteApis'
import MobileDetect from 'mobile-detect'
import GoodPageMobile from '@mobilePages/catalog/GoodPageMobile/GoodPageMobile'
import Good from 'src/components/Good/Good'

export default function GroupIndex(props) {
    if (props.mobile) {
        return (
            <React.Fragment>
                <Head>
                    <title>
                        {`${props.good.webData.drugTitle} ${props.good.webData.outFormTitle} - купить по выгодной цене, инструкция и отзывы в интернет-аптеке ЗДОРОВ.ру`}
                    </title>
                </Head>
                <GoodPageMobile
                    good={props.good}
                    groups={props.groups}
                    groupId={props.groupId}
                    isShowedAnalogues={props.isShowedAnalogues}
                />
            </React.Fragment>
        )
    }
    return (
        <React.Fragment>
            <Head>
                <title>
                    {`${props.good.webData.drugTitle} ${props.good.webData.outFormTitle} - купить по выгодной цене, инструкция и отзывы в интернет-аптеке ЗДОРОВ.ру`}
                </title>
            </Head>
            <Good
                good={props.good}
                groups={props.groups}
                groupId={props.groupId}
                isShowedAnalogues={props.isShowedAnalogues}
            />
        </React.Fragment>
    )
}

export const getServerSideProps = async (ctx) => {
    const cookies = nookies.get(ctx)
    const goodName = ctx.params.goodWebName.split('-')
    const goodWebId = goodName[goodName.length - 1]
    const groupId = parseInt(ctx.params.group3, 10)
    const md = new MobileDetect(ctx.req.headers['user-agent'])
    const shipmentCookie = cookies['storage-shipment']
    const shipment = shipmentCookie ? JSON.parse(shipmentCookie) : { stockId: 0, cityId: 1 }
    const goodData = await getGoodByWebGoodId(goodWebId, shipment)
    const responseGroup = await getGroupsByWebGoodId(goodWebId)
    return {
        props: {
            good: goodData,
            groups: responseGroup,
            groupId,
            mobile: md.mobile(),
            isShowedAnalogues: ctx.query.isShowedAnalogues === 'true'
        }
    }
}
