import { PageWrapper } from '@styles/base.styled'
import { initGoodsByGroupId } from '@actions/catalog'
import nookies from 'nookies'
import MobileDetect from 'mobile-detect'
import React from 'react'
import MobileGroupPage from '../../../src/components/mobilePages/catalog/MobileGroupPage/MobileGroupPage'
import { initializeStore } from '../../../src/store'
import Group from '../../../src/components/Group/Group'

export default function Group2Index(props) {
    if (props.mobile) {
        return <MobileGroupPage groupId={props.groupId} groupPosition="group2" />
    }
    return (
        <PageWrapper>
            <Group groupId={props.groupId} />
        </PageWrapper>
    )
}

export const getServerSideProps = async (ctx) => {
    const reduxStore = initializeStore()
    const { dispatch } = reduxStore
    const md = new MobileDetect(ctx.req.headers['user-agent'])
    const groupId = parseInt(ctx.params.group2, 10)
    const cookies = nookies.get(ctx)
    const shipmentCookie = cookies['storage-shipment']
    const shipment = shipmentCookie ? JSON.parse(shipmentCookie) : { stockId: 0, cityId: 1 }
    await dispatch(initGoodsByGroupId(groupId, shipment))
    return {
        props: { groupId, initialReduxState: reduxStore.getState(), mobile: md.mobile() }
    }
}
