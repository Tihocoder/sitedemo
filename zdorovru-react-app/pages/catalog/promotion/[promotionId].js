import React from 'react'
import { initPromotionGoods } from '@actions/catalog'
import { initializeStore } from 'src/store'
import nookies from 'nookies'
import MobileDetect from 'mobile-detect'
import CatalogPromotion from '@mobilePages/catalog/CatalogPromotion/CatalogPromotion'
import PropTypes from 'prop-types'
import PromotionGoods from '../../../src/components/Pages/Promotion/PromotionGoods/PromotionGoods'

export default function PromotionIndex(props) {
    if (props.mobile) return <CatalogPromotion />
    return <PromotionGoods />
}

PromotionIndex.propTypes = {
    mobile: PropTypes.bool.isRequired
}

export const getServerSideProps = async (ctx) => {
    const reduxStore = initializeStore()
    const { dispatch } = reduxStore
    const cookies = nookies.get(ctx)
    const shipmentCookie = cookies['storage-shipment']
    const shipment = shipmentCookie ? JSON.parse(shipmentCookie) : { stockId: 0, cityId: 1 }
    const promotionId = parseInt(ctx.params.promotionId, 10)
    await dispatch(initPromotionGoods(promotionId, shipment))
    const md = new MobileDetect(ctx.req.headers['user-agent'])
    return {
        props: { initialReduxState: reduxStore.getState(), mobile: md.mobile() }
    }
}
