import React from 'react'
import { getPromotions } from '@api/siteApis'
import { PageWrapper } from '@styles/base.styled'
import PropTypes from 'prop-types'
import Head from 'next/head'
import PromotionsPage from '../../../src/components/Pages/Promotion/PromotionsPage/PromotionsPage'
import MobileDetect from 'mobile-detect'
import CatalogPromotionList from '@mobilePages/catalog/CatalogPromotionList/CatalogPromotionList'

export default function PromoIndex(props) {
    if (props.mobile) {
        return <CatalogPromotionList promotions={props.promotions} />
    }
    return (
        <PageWrapper>
            <Head>
                <title>Скидки и акции в сети аптек ЗДОРОВ.ру</title>
            </Head>
            <PromotionsPage promotions={props.promotions} />
        </PageWrapper>
    )
}
PromoIndex.propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    promotions: PropTypes.array.isRequired
}

export const getServerSideProps = async (ctx) => {
    const md = new MobileDetect(ctx.req.headers['user-agent'])
    const promotions = await getPromotions()
    return { props: { promotions, mobile: md.mobile() } }
}
