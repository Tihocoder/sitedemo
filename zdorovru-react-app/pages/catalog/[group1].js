import { initGoodsByGroupId } from '../../src/actions/catalog'
import Group from '../../src/components/Group/Group'
import { PageWrapper } from '../../src/styles/base.styled'
import nookies from 'nookies'
import { initializeStore } from '../../src/store'
import MobileDetect from 'mobile-detect'
import MobileGroupPage from '../../src/components/mobilePages/catalog/MobileGroupPage/MobileGroupPage'
import React from 'react'

export default function Group2Index(props) {
    if (props.mobile) {
        return <MobileGroupPage groupId={props.groupId} groupPosition="group1" />
    }
    return (
        <PageWrapper>
            <Group groupId={props.groupId} />
        </PageWrapper>
    )
}

export const getServerSideProps = async (ctx) => {
    const reduxStore = initializeStore()
    const { dispatch } = reduxStore
    const md = new MobileDetect(ctx.req.headers['user-agent'])
    const groupId = parseInt(ctx.params.group1, 10)
    const cookies = nookies.get(ctx)
    const shipmentCookie = cookies['storage-shipment']
    const shipment = shipmentCookie ? JSON.parse(shipmentCookie) : { stockId: 0, cityId: 1 }
    await dispatch(initGoodsByGroupId(groupId, shipment))
    return {
        props: {
            groupId,
            initialReduxState: reduxStore.getState(),
            mobile: md.mobile()
        }
    }
}
