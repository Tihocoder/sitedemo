// eslint-disable-next-line import/no-duplicates
import { initGoodsBySearch } from '@actions/catalog'
import nookies from 'nookies'
import MobileDetect from 'mobile-detect'
import React from 'react'
import Search from 'src/components/Search/Search'
import { initializeStore } from 'src/store'
import MobileSearchPage from 'src/components/mobilePages/catalog/MobileSearchPage/MobileSearchPage'

export default function SearchIndex(props) {
    if (props.mobile) {
        return <MobileSearchPage />
    }
    return <Search />
}

export const getServerSideProps = async (ctx) => {
    const reduxStore = initializeStore()
    const { dispatch } = reduxStore
    const { q } = ctx.query
    const cookies = nookies.get(ctx)
    const shipmentCookie = cookies['storage-shipment']
    const shipment = shipmentCookie ? JSON.parse(shipmentCookie) : { stockId: 0, cityId: 1 }
    const md = new MobileDetect(ctx.req.headers['user-agent'])
    await dispatch(initGoodsBySearch(encodeURIComponent(q), shipment))
    return {
        props: { initialReduxState: reduxStore.getState(), mobile: md.mobile() }
    }
}
