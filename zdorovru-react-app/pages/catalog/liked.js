import MobileDetect from 'mobile-detect'
import { initializeStore } from 'src/store'
import React from 'react'
import LikedPage from '../../src/components/mobilePages/catalog/LikedPage/LikedPage'
import Liked from '../../src/components/Pages/Catalog/Liked'

export default function LikedIndex(props) {
    if (props.mobile) {
        return <LikedPage />
    }
    return <Liked />
}

export const getServerSideProps = async (ctx) => {
    const reduxStore = initializeStore()
    const md = new MobileDetect(ctx.req.headers['user-agent'])
    return {
        props: { initialReduxState: reduxStore.getState(), mobile: md.mobile() }
    }
}
