import MobileDetect from 'mobile-detect'
import React from 'react'
import AccountPage from '@mobilePages/account/AccountPage/AccountPage'
import { getCustomerProfile } from '@api/siteApis'

export default function AccountIndexPage(props) {
    if (props.mobile) {
        return <AccountPage profile={props.profile} />
    }
    return <React.Fragment />
}

export const getServerSideProps = async (ctx) => {
    const md = new MobileDetect(ctx.req.headers['user-agent'])
    const customerProfile = await getCustomerProfile(ctx)
    return { props: { mobile: md.mobile(), profile: customerProfile?.profile || null } }
}