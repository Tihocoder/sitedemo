import ErrorPage from '@components/Pages/Error/ErrorPage'
import MobileDetect from 'mobile-detect'
import React from 'react'
import { errorLog } from '@api/siteApis'
import Router from 'next/router'

function Error(props) {
    return <ErrorPage error={props.error} />
}

Error.getInitialProps = async ({ req, err, res }) => {
    // eslint-disable-next-line no-nested-ternary
    const statusCode = res ? res.statusCode : err ? err.statusCode : 404
    const userAgent = req ? req.headers['user-agent'] : navigator.userAgent
    const md = new MobileDetect(userAgent)
    // eslint-disable-next-line no-unused-vars
    try {
        const response = await errorLog({
            description: 'UnknownError',
            error: err.stack
        })
        // eslint-disable-next-line no-empty
    } catch (e) {}
    if (res) {
        res.writeHead(302, {
            Location: '/error/unknown'
        })
        res.end()
    } else {
        await Router.push('/error/unknown')
    }
    return {
        props: { statusCode, userAgent, mobile: !!md.mobile(), error: err }
    }
}
export default Error
