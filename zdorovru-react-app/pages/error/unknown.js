import ErrorPage from '@components/Pages/Error/ErrorPage'
import React from 'react'

export default function UnknownPage() {
    return <ErrorPage />
}