import React from 'react'
import { getRule } from '@api/siteApis'
import { RuleEnum } from 'src/consts'
import Head from 'next/head'
import PageWithPadding from '@mobileShared/PageWithPadding/PageWithPadding'
import MobileDetect from 'mobile-detect'
import { useRouter } from 'next/router'
import PropTypes from 'prop-types'
import PrivacyPolicy from '../src/components/Pages/Company/PrivacyPolicy/PrivacyPolicy'

export default function PrivacyPolicyIndex(props) {
    const router = useRouter()
    if (props.mobile)
        return (
            <PageWithPadding
                headerText="Политика конфиденциальности"
                pageHeaderAttributes={{
                    backClickHandler: async () => {
                        await router.push('/account')
                    },
                    isShowArrowBack: true
                }}>
                <div
                    dangerouslySetInnerHTML={{
                        __html: props.bodyHtml
                    }}
                />
            </PageWithPadding>
        )
    return (
        <React.Fragment>
            <Head>
                <title>Политика конфиденциальности - интернет аптека ЗДОРОВ.ру</title>
            </Head>
            <PrivacyPolicy bodyHtml={props.bodyHtml} />
        </React.Fragment>
    )
}
PrivacyPolicyIndex.propTypes = {
    bodyHtml: PropTypes.string.isRequired,
    mobile: PropTypes.bool.isRequired
}
export const getServerSideProps = async (ctx) => {
    const response = await getRule(RuleEnum.PrivacyPolicy)
    const md = new MobileDetect(ctx.req.headers['user-agent'])
    return { props: { bodyHtml: response.bodyHtml, mobile: md.mobile() } }
}
