import React from 'react'
import Head from 'next/head'
import MobileDetect from 'mobile-detect'
import FinishSuccess from '@mobilePages/order/FinishSuccess/FinishSuccess'
import Success from '@components/Pages/Order/Delivery/Success'
import { getOrderFullInfo, getOrderItems } from '@api/siteApis'
import nookies from 'nookies'

export default function deliverySuccess(props) {
    if (props.mobile) {
        return (
            <React.Fragment>
                <Head>
                    <meta name="robots" content="noindex" />
                </Head>
                <FinishSuccess
                    orderId={props.orderId}
                    orderDetails={props.orderDetails}
                    orderGoods={props.orderGoods}
                />
            </React.Fragment>
        )
    }
    return (
        <React.Fragment>
            <Head>
                <meta name="robots" content="noindex" />
            </Head>
            <Success orderId={props.orderId} orderDetails={props.orderDetails} isMobile={props.mobile} />
        </React.Fragment>
    )
}

export const getServerSideProps = async (ctx) => {
    const orderId = parseInt(ctx.params.orderId, 10)
    const md = new MobileDetect(ctx.req.headers['user-agent'])
    const orderDetails = await getOrderFullInfo(orderId, ctx)
    const cookies = nookies.get(ctx)
    const shipmentCookie = cookies['storage-shipment']
    const shipment = shipmentCookie ? JSON.parse(shipmentCookie) : { stockId: 0, cityId: 1 }
    const orderGoods = await getOrderItems(orderId, shipment, ctx)
    return {
        props: {
            orderId,
            orderDetails,
            orderGoods,
            mobile: md.mobile()
        }
    }
}
