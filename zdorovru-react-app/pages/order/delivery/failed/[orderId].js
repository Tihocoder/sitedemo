import React from 'react'
import FailedOrder from '../../../../src/components/Pages/Order/FailedOrder'

export default function deliveryFailed(props) {
    return <FailedOrder orderId={props.orderId} />
}

export const getServerSideProps = async ({ store, params, query }) => {
    const orderId = parseInt(query.orderId, 10)
    return { props: { orderId: orderId } }
}
