import React from 'react'
import { getCustomerProfile, getPreorderFullInfo, getPreorderTimeSlots } from '@api/siteApis'
import MobileDetect from 'mobile-detect'
import CreateOrderMobile from '@mobilePages/order/CreateOrderMobile/CreateOrderMobile'
import CreateOrder from '@components/Order/CreateOrder/CreateOrder'
import ErrorPage from '@components/Pages/Error/ErrorPage'

export default function createOrderPage(props) {
    if (props.isError) {
        return <ErrorPage />
    }
    if (props.mobile) {
        return (
            <CreateOrderMobile
                isDelivery={props.isDelivery}
                timeSlots={props.timeSlots}
                preorderInfo={props.preorderInfo}
                customerProfile={props.customerProfile}
            />
        )
    }
    return (
        <CreateOrder
            isDelivery={props.isDelivery}
            timeSlots={props.timeSlots}
            preorderInfo={props.preorderInfo}
        />
    )
}
export const getServerSideProps = async (ctx) => {
    const UA = ctx.req.headers['user-agent']
    const md = new MobileDetect(UA)
    const customerProfile = await getCustomerProfile(ctx)
    if (!ctx.query.preorderUid) {
        return { props: { isError: true, mobile: md.mobile() } }
    }

    try {
        const { isDelivery, preorderUid } = ctx.query
        const preorderInfo = await getPreorderFullInfo(preorderUid)
        const preorderTimeSlot = await getPreorderTimeSlots(preorderUid)
        return {
            props: {
                mobile: md.mobile(),
                customerProfile,
                isDelivery: isDelivery === 'true',
                preorderInfo,
                timeSlots:
                    isDelivery !== 'true' ? preorderTimeSlot.pickupTimeSlots : preorderTimeSlot.timeSlots
            }
        }
    } catch (error) {
        console.error('createOrderPage ', UA, error)
        return {
            props: {
                isError: true,
                customerProfile,
                mobile: md.mobile()
            }
        }
    }
}
