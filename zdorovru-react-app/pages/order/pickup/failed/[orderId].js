import React from 'react'
import MobileDetect from 'mobile-detect'
import Head from 'next/head'
import FinishSuccess from '@mobilePages/order/FinishSuccess/FinishSuccess'
import FailedOrder from '@components/Pages/Order/FailedOrder'

export default function pickupFailed(props) {
    // if (props.mobile) {
    //     return (
    //         <React.Fragment>
    //             <Head>
    //                 <meta name="robots" content="noindex" />
    //             </Head>
    //             <FinishSuccess orderId={props.orderId} isDelivery />
    //         </React.Fragment>
    //     )
    // }
    return <FailedOrder orderId={props.orderId} />
}

export const getServerSideProps = async ({ query }) => {
    const orderId = parseInt(query.orderId, 10)
    const md = new MobileDetect(ctx.req.headers['user-agent'])
    return {
        props: {
            orderId,
            mobile: md.mobile()
        }
    }
}
