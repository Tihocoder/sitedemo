import React from 'react'
import BasketMobile from '@mobilePages/order/BasketMobile/BasketMobile'
import MobileDetect from 'mobile-detect'
import { initializeStore } from 'src/store'
import Basket from '../../src/components/Basket/Basket'

export default function basketPage(props) {
    if (props.mobile) {
        return <BasketMobile isDelivery={props.isDelivery} />
    }
    return <Basket />
}

export const getServerSideProps = async (ctx) => {
    const reduxStore = initializeStore()
    const md = new MobileDetect(ctx.req.headers['user-agent'])
    return {
        props: {
            initialReduxState: reduxStore.getState(),
            mobile: md.mobile(),
            isDelivery: ctx.query.isDelivery === 'true'
        }
    }
}
