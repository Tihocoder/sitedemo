import React from 'react'
import Head from 'next/head'
import MobileDetect from 'mobile-detect'
import MobileFeedBack from '@mobilePages/MobileFeedBack/MobileFeedBack'
import Feedback from '../../src/components/Pages/Contact/Feedback/Feedback'

export default function FeedbackIndex(props) {
    return (
        <React.Fragment>
            <Head>
                <title>Обратная связь - сеть аптек ЗДОРОВ.ру</title>
            </Head>
            {props.mobile ? <MobileFeedBack /> : <Feedback />}
        </React.Fragment>
    )
}

export const getServerSideProps = async (ctx) => {
    const md = new MobileDetect(ctx.req.headers['user-agent'])
    return { props: { mobile: md.mobile() } }
}
