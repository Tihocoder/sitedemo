import React from 'react'
import Head from 'next/head'
import MobileDetect from 'mobile-detect'
import PageWithPadding from '@mobileShared/PageWithPadding/PageWithPadding'
import FeedbackCompleted from '../../src/components/Pages/Contact/Feedback/FeedbackCompleted'
import { useRouter } from 'next/router'

export default function FeedbackCompletedIndex(props) {
    const router = useRouter()
    return (
        <React.Fragment>
            <Head>
                <title>Обратная связь - сеть аптек ЗДОРОВ.ру</title>
            </Head>
            {props.mobile ? (
                <PageWithPadding
                    headerText="Обратная связь"
                    pageHeaderAttributes={{
                        isShowArrowBack: true,
                        backClickHandler: () => router.push('/account')
                    }}>
                    <FeedbackCompleted />
                </PageWithPadding>
            ) : (
                <FeedbackCompleted />
            )}
        </React.Fragment>
    )
}
export const getServerSideProps = async (ctx) => {
    const md = new MobileDetect(ctx.req.headers['user-agent'])
    return { props: { mobile: md.mobile() } }
}
