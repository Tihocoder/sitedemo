import Nordpharm from '../src/components/Pages/Company/About/Nordpharm'
import MobileDetect from 'mobile-detect'
import { useRouter } from 'next/router'
import React from 'react'
import Head from 'next/head'
import MobilePageHeader from '@mobileShared/MobilePageHeader/MobilePageHeader'

export default function NordpharmIndex(props) {
    const router = useRouter()
    return (
        <React.Fragment>
            <Head>
                <title>ООО "НордФарм" - сеть аптек ЗДОРОВ.ру</title>
            </Head>
            {props.mobile && (
                <MobileHeader
                    backClickHandler={async () => {
                        await router.push('/account')
                    }}>
                    ООО "НордФарм"
                </MobileHeader>
            )}
            <Nordpharm isMobile={props.mobile} />
        </React.Fragment>
    )
}
export const getServerSideProps = async (ctx) => {
    const md = new MobileDetect(ctx.req.headers['user-agent'])
    return { props: { mobile: md.mobile() } }
}
