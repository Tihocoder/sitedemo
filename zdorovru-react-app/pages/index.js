import React from 'react'
import { initializeStore } from 'src/store'
import MobileDetect from 'mobile-detect'
import Banners from '../src/components/Banners/Banners'
import Home from '../src/components/mobilePages/Home/Home'

export default function HomeIndex(props) {
    if (props.mobile) {
        return <Home />
    }
    return <Banners />
}

export const getServerSideProps = async (ctx) => {
    const reduxStore = initializeStore()
    const md = new MobileDetect(ctx.req.headers['user-agent'])
    return {
        props: {
            initialReduxState: reduxStore.getState(),
            mobile: md.mobile()
        }
    }
}
