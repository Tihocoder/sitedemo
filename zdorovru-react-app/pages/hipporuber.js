import MobileDetect from 'mobile-detect'
import { useRouter } from 'next/router'
import React from 'react'
import Head from 'next/head'
import MobilePageHeader from '@mobileShared/MobilePageHeader/MobilePageHeader'
import Hipporuber from '../src/components/Pages/Company/About/Hipporuber'
import MobileHeader from "@mobileShared/MobileHeader/MobileHeader";

export default function hipporuberIndex(props) {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const router = useRouter()
    return (
        <React.Fragment>
            <Head>
                <title>ООО "ГиппоРубер" - сеть аптек ЗДОРОВ.ру</title>
            </Head>
            {props.mobile && (
                <MobileHeader
                    backClickHandler={async () => {
                        await router.push('/account')
                    }}>
                    ООО "ГиппоРубер"
                </MobileHeader>
            )}
            <Hipporuber isMobile={props.mobile} />
        </React.Fragment>
    )
}
export const getServerSideProps = async (ctx) => {
    const md = new MobileDetect(ctx.req.headers['user-agent'])
    return { props: { mobile: md.mobile() } }
}
