import MobileDetect from 'mobile-detect'
import CatalogGroupList from '../src/components/mobileComponents/catalog/CatalogGroupList/CatalogGroupList'
import CatalogPage from '../src/components/mobilePages/catalog/CatalogPage/CatalogPage'
import MobileGroupPage from '../src/components/mobilePages/catalog/MobileGroupPage/MobileGroupPage'
import { initializeStore } from '../src/store'

export default function CatalogIndex(props) {
    return <CatalogPage />
}

export const getServerSideProps = async (ctx) => {
    const reduxStore = initializeStore()
    const md = new MobileDetect(ctx.req.headers['user-agent'])
    return {
        props: {
            initialReduxState: reduxStore.getState(),
            mobile: md.mobile()
        }
    }
}
