import React from 'react'
import Head from 'next/head'
import { useRouter } from 'next/router'
import PropTypes from 'prop-types'
import MobileDetect from 'mobile-detect'
import AccountProfile from '@mobilePages/account/AccountProfile/AccountProfile'
import AccountHistoryOrder from '@mobilePages/account/AccountHistoryOrder/AccountHistoryOrder'
import { getCustomerOrders, getCustomerProfile } from '@api/siteApis'
import AccountOrderInfo from '@mobilePages/account/AccountOrderInfo/AccountOrderInfo'
import { OrderType } from 'src/consts'
import AccountPurchased from '@mobilePages/account/AccountPurchased/AccountPurchased'
import * as SA from '../../src/styles/account.styled'
import HistoryOrder from '../../src/components/Account/Index/HistoryOrder/HistoryOrder'
import Liked from '../../src/components/Account/Index/Liked/Liked'
import Purchased from '../../src/components/Account/Index/Purchased/Purchased'
import AccountNavigation from '../../src/components/Account/Index/AccountNavigation/AccountNavigation'
import OrderDetails from '../../src/components/Account/Index/OrderDetails/OrderDetails'

const getTitlePage = (path) => {
    if (path.startsWith('/account/history')) {
        return 'Мои заказы'
    }
    if (path.startsWith('/account/liked')) {
        return 'Мои избранные'
    }
    if (path.startsWith('/account/purchased')) {
        return 'Купленные товары'
    }
    if (path.startsWith('/account/order')) {
        return false
    }
}

// eslint-disable-next-line consistent-return
const getMobilePage = (profile, genderTypes, orders) => {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const router = useRouter()
    if (router.asPath.startsWith('/account/profile')) {
        return <AccountProfile profile={profile} genderTypes={genderTypes} />
    }
    if (router.asPath.startsWith('/account/history')) {
        return <AccountHistoryOrder />
    }
    if (router.asPath.startsWith('/account/liked')) {
        return <Liked />
    }
    if (router.asPath.startsWith('/account/purchased')) {
        return <AccountPurchased />
    }
    if (router.asPath.startsWith('/account/order')) {
        const { tags } = router.query
        const orderId = parseInt(tags[1] || 0, 10)
        const order = orders.find((x) => x.id === orderId)
        return <AccountOrderInfo order={order} />
    }
}
export default function AccountIndex(props) {
    const router = useRouter()

    if (props.mobile) {
        return getMobilePage(props.profile, props.genderTypes, props.orders)
    }
    // eslint-disable-next-line consistent-return
    const getPage = () => {
        if (router.asPath.startsWith('/account/history')) {
            return <HistoryOrder orders={props.orders} />
        }
        if (router.asPath.startsWith('/account/liked')) {
            return <Liked />
        }
        if (router.asPath.startsWith('/account/purchased')) {
            return <Purchased />
        }
        if (router.asPath.startsWith('/account/order')) {
            const { tags } = router.query
            const orderId = parseInt(tags[1] || 0, 10)
            const order = props.orders.find((x) => x.id === orderId)
            return <OrderDetails order={order} />
        }
    }
    const titlePage = getTitlePage(router.asPath)
    return (
        <React.Fragment>
            <Head>
                <title>Личный кабинет - сеть аптек ЗДОРОВ.ру</title>
                <meta name="robots" content="noindex" />
            </Head>
            <SA.Container>
                <AccountNavigation profile={props.profile} genderTypes={props.genderTypes} />
                <SA.Body>
                    {titlePage && <SA.TitlePage>{titlePage}</SA.TitlePage>}
                    {getPage()}
                </SA.Body>
            </SA.Container>
        </React.Fragment>
    )
}
AccountIndex.propTypes = {
    orders: PropTypes.arrayOf(PropTypes.shape(OrderType)).isRequired,
    // eslint-disable-next-line react/forbid-prop-types
    profile: PropTypes.object.isRequired,
    // eslint-disable-next-line react/forbid-prop-types
    genderTypes: PropTypes.array.isRequired,
    mobile: PropTypes.string.isRequired
}

export const getServerSideProps = async (ctx) => {
    const orders = await getCustomerOrders(ctx)
    const customerProfile = await getCustomerProfile(ctx)
    const md = new MobileDetect(ctx.req.headers['user-agent'])
    return {
        props: {
            orders,
            profile: customerProfile?.profile || null,
            genderTypes: customerProfile?.genderTypes || null,
            mobile: md.mobile()
        }
    }
}
