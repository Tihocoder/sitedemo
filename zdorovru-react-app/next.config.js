const webpack = require('webpack')

function getBackendUrl() {
    if (process.env.BACKEND_URL) {
        if (process.env.ENVS === 'test' || process.env.ENVS === 'prod') {
            return `https://zdorov.ru/${process.env.BACKEND_URL}`
        }
        if (process.env.ENVS === 'local') {
            return `http://localhost:5000/${process.env.BACKEND_URL}`
        }
        return `https://zdorov.ru/${process.env.BACKEND_URL}`
    }
    if (process.env.ENVS === 'local') {
        return 'http://localhost:5000/backend'
    }
    if (process.env.ENVS === 'prod') {
        if (process.env.BASE_PATH) {
            return `https://zdorov.ru${process.env.BASE_PATH}/backend`
        }
        return `https://zdorov.ru/backend`
    }
    if (process.env.ENVS === 'test') {
        return `https://zdorov.ru/new/backend`
    }
}

module.exports = {
    basePath: process.env.BASE_PATH || '',
    env: {
        BACKEND_URL: process.env.NEXT_PUBLIC_URL || getBackendUrl(),
        NEXT_PUBLIC_URL: process.env.NEXT_PUBLIC_URL || getBackendUrl(),
        ENVS: process.env.ENVS,
        BASE_PATH: process.env.BASE_PATH || ''
    },
    webpack: (config, { buildId, dev, isServer, defaultLoaders, webpack }) => {
        config.plugins.push(
            new webpack.DefinePlugin({
                SC_DISABLE_SPEEDY: true
            })
        )

        const originalEntry = config.entry

        config.entry = async () => {
            const entries = await originalEntry()

            if (entries['main.js'] && !entries['main.js'].includes('./src/polyfills.js')) {
                entries['main.js'].unshift('./src/polyfills.js')
            }
            return entries
        }

        return config
    }
}
