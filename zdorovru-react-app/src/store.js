import { useMemo } from 'react'
import { createStore, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import ReduxThunk from 'redux-thunk'
import rootReducer from './reducers/rootReducer'

let store

function initStore(preloadedState) {
    return createStore(rootReducer, preloadedState, composeWithDevTools(applyMiddleware(ReduxThunk)))
}
// BINDING MIDDLEWARE

export const initializeStore = (preloadedState) => {
    let _store = store ?? initStore(preloadedState)

    // After navigating to a page with an initial Redux state, merge that state
    // with the current state in the store, and create a new store
    if (preloadedState && store) {
        const state = store.getState()
        const inittialStore = {
            ...preloadedState,
            ...state
        }

        if (preloadedState.catalog.goods) {
            inittialStore.catalog.goods = preloadedState.catalog.goods
        }
        if (preloadedState.catalog.baseImageUrl) {
            inittialStore.catalog.baseImageUrl = preloadedState.catalog.baseImageUrl
        }
        if (preloadedState.catalog.goodsGroups) {
            inittialStore.catalog.goodsGroups = preloadedState.catalog.goodsGroups
        }
        if (preloadedState.catalog.goodsData) {
            inittialStore.catalog.goodsData = preloadedState.catalog.goodsData
        }
        inittialStore.catalog.categories = preloadedState.catalog.categories
        if (preloadedState.catalog.foundCategories) {
            inittialStore.catalog.foundCategories = preloadedState.catalog.foundCategories
        } else if (state.catalog.foundCategories) {
            inittialStore.catalog.foundCategories = state.catalog.foundCategories
        }

        // if (!inittialStore.customer.preorder.uid) {
        //     inittialStore.customer.preorder = preloadedState.customer.preorder
        // }
        inittialStore.customer.preorder = preloadedState.customer.preorder
        inittialStore.createOrder.validate = preloadedState.createOrder.validate

        _store = initStore(inittialStore)
        // Reset the current store
        store = undefined
    }

    // For SSG and SSR always create a new store
    if (typeof window === 'undefined') return _store
    // Create the store once in the client
    if (!store) store = _store

    return _store
}

export function useStore(initialState) {
    const store = useMemo(() => initializeStore(initialState), [initialState])
    return store
}
