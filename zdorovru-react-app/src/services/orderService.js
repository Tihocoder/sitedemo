import { Emailvalidate, PhoneValidate } from '@helpers/validations'
import { parseCookies } from 'nookies'
import { getUpdatedGoods } from '@api/siteApis'
import * as consts from '../consts'

export const validateFormForDelivery = (formData, timeSlots) => {
    const validate = { ...formData.validate }
    let isValid = true
    if (!formData.userContacts.name) {
        validate.form.name = false
        isValid = false
    }
    if (!Emailvalidate(formData.userContacts.email)) {
        validate.form.email = false
        isValid = false
    }
    if (!PhoneValidate(formData.userContacts.phone)) {
        validate.form.phone = false
        isValid = false
    }
    if (!formData.shipTimeSlotInfo.shipTimeSlotId) {
        validate.shipTimeSlotInfo.shipTimeSlotId = false
        isValid = false
    } else if (!timeSlots.some((x) => x.id === formData.shipTimeSlotInfo.shipTimeSlotId)) {
        validate.shipTimeSlotInfo.shipTimeSlotId = false
        isValid = false
    }

    if (!formData.paymentInfo.paymentType) {
        validate.paymentInfo.paymentType = false
        isValid = false
    }

    if (formData.deliveryData.houseType === consts.HouseType.apt) {
        if (!formData.deliveryData.apt) {
            validate.deliveryData.apt = false
            isValid = false
        }
    }

    if (formData.anotherUserContacts.isEnabled) {
        if (!formData.anotherUserContacts.name) {
            validate.anotherUserContacts.name = false
            isValid = false
        }
        if (!Emailvalidate(formData.anotherUserContacts.email)) {
            validate.anotherUserContacts.email = false
            isValid = false
        }
        if (!PhoneValidate(formData.anotherUserContacts.phone)) {
            validate.anotherUserContacts.phone = false
            isValid = false
        }
    }

    return { isValid, formValidate: validate }
}

export const validateFormForStock = (formData, timeSlots) => {
    const validate = { ...formData.validate }
    let isValid = true
    if (!PhoneValidate(formData.userContacts.phone)) {
        validate.form.phone = false
        isValid = false
    }
    if (formData.paymentInfo.paymentType === consts.PaymentType.CardOnline) {
        if (!Emailvalidate(formData.userContacts.email)) {
            validate.form.email = false
            isValid = false
        }
    }

    if (timeSlots?.length && !formData.shipTimeSlotInfo.shipTimeSlotId) {
        validate.shipTimeSlotInfo.shipTimeSlotId = false
        isValid = false
    } else if (
        timeSlots?.length &&
        !timeSlots.some((x) => x.id === formData.shipTimeSlotInfo.shipTimeSlotId)
    ) {
        validate.shipTimeSlotInfo.shipTimeSlotId = false
        isValid = false
    }

    if (timeSlots?.length && !formData.paymentInfo.paymentType) {
        validate.paymentInfo.paymentType = false
        isValid = false
    }

    return { isValid, formValidate: validate }
}

export const checkInvalidResponse = (formData, responseData) => {
    const validate = { ...formData.validate }
    let isValid = true
    if (!responseData.invalidModel) {
        return { isValid }
    }
    responseData.invalidList.forEach((x) => {
        if (x.includes('name') || x.includes('phone') || x.includes('email')) {
            validate.form[x] = false
            isValid = false
        }
        if (x.includes('apt') || x.includes('paymentTypeId') || x.includes('shipTimeSlotId')) {
            validate.deliveryData[x] = false
            isValid = false
        }
    })
    return { isValid, formValidate: validate }
}

const getTimeSlots = async () => {
    const cookie = parseCookies()
    const response = await getUpdatedGoods(
        basketItems.map((item) => ({
            webId: item.webData.webId,
            goodId: item.webData.goodId
        })),
        JSON.parse(cookie['storage-shipment'])
    )
    return response
}
