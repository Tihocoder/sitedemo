import { FeaturesEnum } from 'src/consts'
import { generateDate, generateTime } from '@helpers/site'
import moment from 'moment'
import nookies, { parseCookies, setCookie } from "nookies";

export const ImageSizeType = {
    regular: '240',
    large: '480'
}

export const getUslImage = (hasImage, goodId, sizeType = ImageSizeType.regular) => {
    if (hasImage) return `https://zdorov.ru/goodsimg/${sizeType}/${goodId}.jpg`
    return `${process.env.BASE_PATH || ''}/images/noimg.gif`
}

export const getGoodUrl = (webData, groupId) => {
    try {
        if (groupId) {
            const group = webData.goodGroups.find((x) => x.clsGroupID === groupId)
            if (group) {
                return `/catalog/${group.groupUrl}/${webData.url}-${webData.webId}`
            }
        }

        const groupDefault = webData.goodGroups.find((x) => x.isDefault)
        return `/catalog/${groupDefault.groupUrl}/${webData.url}-${webData.webId}`
    } catch {
        console.error('webId not found groups: ', webData.webId)
        return ''
    }
}

export const getFeatureHtml = (good, features) => {
    if (good.deliveryPrice.isDemand && good.stockPrice.isDemand) {
        return features[FeaturesEnum.DescriptionDemandStockAndDeliveryBasketAdd]
            .replace('[dateStock]', generateDate(good.stockPrice.aptEtaDateTime, false))
            .replace('[dateDelivery]', generateDate(good.deliveryPrice.aptEtaDateTime, false))
            .replace('[time]', generateTime(good.deliveryPrice.aptEtaDateTime))
    }
    if (good.deliveryPrice.isDemand) {
        return features[FeaturesEnum.DemandDeliveryBasketAdd]
            .replace('[date]', generateDate(good.deliveryPrice.aptEtaDateTime, false))
            .replace('[time]', generateTime(good.deliveryPrice.aptEtaDateTime))
    }
    if (good.stockPrice.isDemand) {
        return features[FeaturesEnum.DemandPVZBasketAdd].replace(
            '[date]',
            generateDate(good.stockPrice.aptEtaDateTime, true)
        )
    }
}
export const checkShowedDateShipmentSearch = () => {
    if (typeof window === 'undefined') return
    const showedShipmentDateStorage = localStorage.getItem('showed-shipment')
    if (!showedShipmentDateStorage) return false
    const currentDate = moment()
    const showedShipmentDate = moment(showedShipmentDateStorage)
    const showedShipmentDateDiff = currentDate.diff(showedShipmentDate, 'days')
    return showedShipmentDateDiff > 0
}

// eslint-disable-next-line consistent-return
export const checkShowedShipmentSearch = () => {
    // достаем куки
    const cookies = parseCookies()
    const showedShipmentString = cookies['showed-shipment']
    if (!showedShipmentString) return true
    // сравниваем количество дней
    const currentDate = moment()
    const showedShipmentDate = moment(showedShipmentString)
    const showedShipmentDateDiff = currentDate.diff(showedShipmentDate, 'days')
    return showedShipmentDateDiff > 0
}
export const updateShowedDateShipmentSearch = () => {
    const currentDate = moment().format()
    setCookie(null, 'showed-shipment', currentDate, {
        maxAge: 5 * 24 * 60 * 60,
        path: '/'
    })
}
