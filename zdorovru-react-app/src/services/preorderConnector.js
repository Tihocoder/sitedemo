import { createPreorder } from '../api/siteApis'
import { GenerateCreatePreorderModel } from '../helpers/order'
import * as signalR from '@microsoft/signalr'

export const createConnectionPreorder = async (basketItems, shipment, isDelivery) => {
    const requestData = GenerateCreatePreorderModel(shipment, isDelivery, basketItems)
    if (!requestData.preorderItems || requestData.preorderItems.length === 0) {
        throw 'модель сгенерировалась с ошибкой'
    }
    const response = await createPreorder(requestData)
    if (!response.preorderUid) throw `Предзаказ не создан: ${response}`
    const connection = initSignalRConnection(response.preorderUid)
    return { connection, response }
}

export const createConnection = (connection, messageHandler) => {
    connection
        .start()
        .then((result) => {
            console.log('Connected!')
            connection.on('SendPreorder', (message) => {
                messageHandler(message)
            })
        })
        .catch((e) => console.log('Connection failed: ', e))
}

const initSignalRConnection = (uid) => {
    const newConnection = new signalR.HubConnectionBuilder()
        .configureLogging(signalR.LogLevel.Debug)
        .withUrl(`${process.env.NEXT_PUBLIC_URL}/api/preorder-hub?preorderUid=${uid}`, {
            transport: signalR.HttpTransportType.LongPolling
            //signalR.HttpTransportType.WebSockets | signalR.HttpTransportType.LongPolling
        })
        .withAutomaticReconnect()
        .build()

    return newConnection
}
