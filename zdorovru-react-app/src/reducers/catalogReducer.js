import * as _ from 'lodash'
import {
    INIT_GROUPS,
    INIT_GOODS,
    PAGE_LANDING_ON,
    PAGE_LANDING_OFF,
    CURRENT_GROUP_ID,
    INIT_CATEGORIES,
    GOODS_SHIPMENT,
    SET_SEARCH_TEXT,
    SET_FOUND_CATEGORIES,
    SET_GOODS_FILTER,
    INIT_FEATURES,
    REMOVE_GOODS_FILTER,
    INIT_GOODS_GROUPS,
    REMOVE_GOODS_GROUPS,
    SET_BASE_IMAGE_URL
} from '../actions/types'

const initialState = {
    loading: false,
    currentGroupId: 0,
    goods: [],
    goodsData: {
        makers: [],
        outForms: []
    },
    baseImageUrl: '',
    goodsGroups: [],
    isMinPrice: false,
    categories: [],
    foundCategories: [],
    searchText: '',
    mainGroups: [],
    features: {},
    goodsShipment: {
        stockId: 0,
        shipAddressId: 0
    },
    goodsFilters: { makers: [], outForms: [] }
}

export default function catalogReducer(state = initialState, action) {
    switch (action.type) {
        case INIT_GROUPS: {
            return {
                ...state,
                mainGroups: [...action.data]
            }
        }
        case PAGE_LANDING_ON: {
            return {
                ...state,
                loading: true
            }
        }
        case INIT_GOODS_GROUPS: {
            return {
                ...state,
                goodsGroups: [...action.data]
            }
        }
        case REMOVE_GOODS_GROUPS: {
            return {
                ...state,
                goodsGroups: []
            }
        }
        case SET_SEARCH_TEXT: {
            return {
                ...state,
                searchText: action.data
            }
        }
        case SET_BASE_IMAGE_URL: {
            return {
                ...state,
                baseImageUrl: action.data
            }
        }
        case INIT_FEATURES: {
            return {
                ...state,
                features: { ...action.data }
            }
        }
        case SET_FOUND_CATEGORIES: {
            return {
                ...state,
                foundCategories: [...action.data]
            }
        }
        case GOODS_SHIPMENT: {
            return {
                ...state,
                goodsShipment: {
                    ...state.goodsShipment,
                    stockId: action.data.stockId
                }
            }
        }
        case PAGE_LANDING_OFF: {
            return {
                ...state,
                loading: false
            }
        }
        case SET_GOODS_FILTER: {
            return {
                ...state,
                goodsFilters: { ...state.goodsFilters, [action.data.name]: [...action.data.value] }
            }
        }
        case REMOVE_GOODS_FILTER: {
            return {
                ...state,
                goodsFilters: { ...state.goodsFilters, [action.data]: [] }
            }
        }
        case INIT_GOODS: {
            return {
                ...state,
                goods: action.data?.length ? [...action.data] : [],
                goodsData: {
                    makers: action.data?.length
                        ? _.uniq(
                              action.data.map((item) => ({
                                  id: item.webData.makerTitle,
                                  title: item.webData.makerTitle
                              }))
                          ).sort()
                        : [],
                    outForms: action.data?.length
                        ? _.uniq(
                              action.data.map((item) => ({
                                  id: item.webData.outFormTitle,
                                  title: item.webData.outFormTitle
                              }))
                          ).sort()
                        : []
                }
            }
        }
        case INIT_CATEGORIES: {
            return {
                ...state,
                categories: [...action.data]
            }
        }
        case CURRENT_GROUP_ID: {
            return {
                ...state,
                currentGroupId: action.data
            }
        }
        default:
            return state
    }
}
