import {
    ADD_BASKET_ITEM,
    ADD_HISTORY_PATH,
    CHANGE_BASKET_ITEM,
    CLEAR_BASKET,
    INIT_BASKET_ITEMS,
    INIT_BASKET_SHIPMENT,
    INIT_DELIVERY_TRESHHOLD,
    INIT_LIKED_GOODS,
    INIT_TIME_SLOTS,
    NEW_HISTORY_PATHS,
    POP_HISTORY_PATH,
    REMOVE_BASKET_ITEM,
    REMOVE_BASKET_ITEM_BY_ID,
    SELECT_SHIPMENT,
    SELECT_STOCK,
    SELECTED_CITY,
    SET_BASKET_ISDELIVERY,
    SET_BASKET_SHIPADDRESS,
    SET_BASKET_SHIPMENT,
    SET_BASKET_SHIPMENT_DELIVERY,
    SET_BASKET_SHIPMENT_STOCK,
    SET_ORDER_ID_ON_BASKET,
    TOGGLE_BASKET_ISDELIVERY,
    TOGGLE_HIDDEN_BASKET_ITEM,
    TOGGLE_LIKED_GOOD,
    SET_IS_CONVERTED, INIT_PICKUP_TIME_SLOTS
} from "@actions/types";

const initialState = {
    basketItems: [],
    shipment: {
        stockId: 0,
        cityId: 1,
        shipAddressId: 0,
        shipAddressTitle: '',
        stockTitle: ''
    },
    orderId: 0,
    likedGoods: [],
    preorder: {
        uid: '',
        connection: null,
        isLoading: false,
        timer: null,
        signalRMessage: null
    },
    isConverted: false,
    basket: {
        shipAddress: {
            id: 0,
            title: ''
        },
        deliveryTreshhold: {
            minSum: 0,
            cost: 0
        },
        isDelivery: false
    },
    historyPaths: []
}

export default function catalogReducer(state = initialState, action) {
    switch (action.type) {
        case SELECTED_CITY: {
            return {
                ...state,
                shipment: {
                    ...state.shipment,
                    cityId: action.data
                }
            }
        }
        case SET_IS_CONVERTED: {
            return {
                ...state,
                isConverted: action.value
            }
        }
        case ADD_HISTORY_PATH: {
            const paths = [...state.historyPaths]
            paths.push(action.value)
            return {
                ...state,
                historyPaths: paths
            }
        }
        case POP_HISTORY_PATH: {
            const paths = [...state.historyPaths]
            paths.pop()
            return {
                ...state,
                historyPaths: paths
            }
        }
        case NEW_HISTORY_PATHS: {
            return {
                ...state,
                historyPaths: [...action.value]
            }
        }
        case SELECT_STOCK: {
            return {
                ...state,
                shipment: {
                    ...state.shipment,
                    stockId: action.data
                }
            }
        }
        case SET_BASKET_SHIPADDRESS: {
            return {
                ...state,
                basket: {
                    ...state.basket,
                    shipAddress: {
                        ...action.data
                    }
                }
            }
        }
        case TOGGLE_BASKET_ISDELIVERY: {
            return {
                ...state,
                basket: {
                    ...state.basket,
                    isDelivery: !state.basket.isDelivery
                }
            }
        }
        case SET_BASKET_ISDELIVERY: {
            return {
                ...state,
                basket: {
                    ...state.basket,
                    isDelivery: action.value
                }
            }
        }
        case SET_BASKET_SHIPMENT_STOCK: {
            return {
                ...state,
                basket: {
                    ...state.basket,
                    shipment: {
                        ...state.basket.shipment,
                        stock: { ...action.data }
                    }
                }
            }
        }
        case SET_BASKET_SHIPMENT_DELIVERY: {
            return {
                ...state,
                basket: {
                    ...state.basket,
                    shipment: {
                        ...state.basket.shipment,
                        delivery: { ...action.data }
                    }
                }
            }
        }
        case TOGGLE_LIKED_GOOD: {
            const isLike = state.likedGoods.includes(action.data)
            if (isLike) {
                return {
                    ...state,
                    likedGoods: [...state.likedGoods.filter((good) => good !== action.data)]
                }
            }
            const newLikedGoods = [...state.likedGoods]
            newLikedGoods.push(action.data)
            return {
                ...state,
                likedGoods: newLikedGoods
            }
        }
        case INIT_DELIVERY_TRESHHOLD: {
            return {
                ...state,
                basket: {
                    ...state.basket,
                    deliveryTreshhold: {
                        ...action.data
                    }
                }
            }
        }
        case INIT_BASKET_SHIPMENT: {
            return {
                ...state,
                basket: {
                    ...state.basket,
                    shipment: {
                        ...action.data
                    }
                }
            }
        }
        case SET_BASKET_SHIPMENT: {
            return {
                ...state,
                basket: {
                    ...state.basket,
                    shipment: {
                        ...action.data
                    }
                }
            }
        }
        case INIT_PICKUP_TIME_SLOTS: {
            return {
                ...state,
                basket: {
                    ...state.basket,
                    pickupTimeSlots: [...action.data]
                }
            }
        }
        case SELECT_SHIPMENT: {
            const newShipment = action.data
            if (newShipment.stockId === 0) {
                newShipment.stockTitle = ''
            }
            if (newShipment.shipAddressId === 0) {
                newShipment.shipAddressTitle = ''
            }
            return {
                ...state,
                shipment: {
                    ...state.shipment,
                    ...newShipment
                }
            }
        }
        case INIT_BASKET_ITEMS: {
            return {
                ...state,
                basketItems: [...action.data]
            }
        }
        case INIT_LIKED_GOODS: {
            return {
                ...state,
                likedGoods: [...action.data]
            }
        }
        case CLEAR_BASKET: {
            const hiddenBasketItems = state.basketItems.filter((x) => x.isHidden)
            const newItems = hiddenBasketItems.map((x) => ({ ...x, isHidden: false }))
            return {
                ...state,
                basketItems: [...newItems]
            }
        }
        case TOGGLE_HIDDEN_BASKET_ITEM: {
            return {
                ...state,
                basketItems: [
                    ...state.basketItems.map((item) => {
                        if (item.webData.goodId === action.data) {
                            return {
                                ...item,
                                isHidden: !item.isHidden
                            }
                        }
                        return item
                    })
                ]
            }
        }
        case SET_ORDER_ID_ON_BASKET: {
            return {
                ...state,
                orderId: action.data
            }
        }
        case ADD_BASKET_ITEM: {
            const basketItems = [...state.basketItems]
            basketItems.push({ ...action.data })
            return {
                ...state,
                basketItems
            }
        }
        case REMOVE_BASKET_ITEM: {
            const newBasketItems = state.basketItems.filter(
                (item) => item.webData.webId !== action.data.webId
            )
            return {
                ...state,
                basketItems: [...newBasketItems]
            }
        }
        case REMOVE_BASKET_ITEM_BY_ID: {
            const newBasketItems = state.basketItems.filter((item) => item.webData.goodId !== action.data)
            return {
                ...state,
                basketItems: [...newBasketItems]
            }
        }
        case CHANGE_BASKET_ITEM: {
            const newBasketItems = state.basketItems.map((item) => {
                if (item.webData.webId === action.data.webData.webId) {
                    return { ...action.data }
                }
                return item
            })

            return {
                ...state,
                basketItems: newBasketItems
            }
        }
        default:
            return state
    }
}
