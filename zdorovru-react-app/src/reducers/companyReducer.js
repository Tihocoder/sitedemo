import { SET_CITY, LOADING_ON, LOADING_OFF, SET_STOCKS } from '../actions/types'

const initialState = {
    loading: true,
    cities: [],
    stocks: []
}

export default function companyReducer(state = initialState, action) {
    switch (action.type) {
        case SET_CITY: {
            return {
                ...state,
                cities: [...action.data]
            }
        }
        case SET_STOCKS: {
            return {
                ...state,
                stocks: [...action.data]
            }
        }
        case LOADING_ON: {
            return {
                ...state,
                loading: true
            }
        }
        case LOADING_OFF: {
            return {
                ...state,
                loading: false
            }
        }
        default:
            return state
    }
}
