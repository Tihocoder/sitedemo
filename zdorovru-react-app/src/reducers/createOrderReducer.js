import {
    CREATE_ORDER__SET_PAYMENT,
    CREATE_ORDER__SET_TIMESLOT,
    CREATE_ORDER_INIT_VALIDATE,
    INIT_CREATE_ORDER_DATA,
    INIT_TIME_SLOTS,
    PREORDER_CHANGE,
    PREORDER_CLEAR,
    PREORDER_STOP,
    PREORDER_STOP_LOADING,
    RESET_VALIDATE,
    SET_ANOTHER_USER_CONTACT,
    SET_DELIVERY_DATA,
    SET_USER_CONTACT,
    SET_VALIDATE_DELIVERY_DATA,
    SET_VALIDATE_FORM,
    SET_VALIDATE_FORM_ANOTHER_USER
} from '@actions/types'

const defaultValidateInit = {
    form: {
        name: true,
        email: true,
        phone: true
    },
    anotherUserContacts: {
        name: true,
        email: true,
        phone: true
    },
    shipTimeSlotInfo: {
        shipTimeSlotId: true
    },
    paymentInfo: {
        paymentType: true
    },
    deliveryData: {
        apt: true,
        paymentTypeId: true,
        shipTimeSlotId: true
    }
}
const initialState = {
    userContacts: {
        name: '',
        email: '',
        phone: '',
        isNeedOperatorCall: false
    },
    anotherUserContacts: {
        name: '',
        email: '',
        phone: '',
        isEnabled: ''
    },
    shipTimeSlotInfo: {
        shipTimeSlotId: 0,
        isNearestTime: false
    },
    paymentInfo: {
        paymentType: 0,
        cashExchange: null
    },
    deliveryData: {
        apt: '',
        floor: '',
        entrance: '',
        code: '',
        comment: '',
        paymentType: 0,
        houseType: 0,
        organization: '',
        office: '',
        cashExchange: 0.0,
        isNearestTime: false,
        shipTimeSlotId: 0
    },
    preorder: {
        uid: '',
        connection: null,
        message: null,
        isLoading: false
    },
    validate: defaultValidateInit,
    timeSlots: []
}

export default function createOrderReducer(state = initialState, action) {
    switch (action.type) {
        case SET_USER_CONTACT:
            return {
                ...state,
                userContacts: {
                    ...state.userContacts,
                    [action.data.name]: action.data.value
                }
            }
        case SET_ANOTHER_USER_CONTACT: {
            return {
                ...state,
                anotherUserContacts: {
                    ...state.anotherUserContacts,
                    [action.data.name]: action.data.value
                }
            }
        }
        case SET_DELIVERY_DATA:
            return {
                ...state,
                deliveryData: {
                    ...state.deliveryData,
                    [action.data.name]: action.data.value
                }
            }
        case SET_VALIDATE_FORM:
            return {
                ...state,
                validate: {
                    ...state.validate,
                    form: {
                        ...state.validate.form,
                        [action.data.name]: action.data.value
                    }
                }
            }
        case INIT_TIME_SLOTS: {
            return {
                ...state,
                timeSlots: [...action.data]
            }
        }
        case SET_VALIDATE_FORM_ANOTHER_USER: {
            return {
                ...state,
                validate: {
                    ...state.validate,
                    anotherUserContacts: {
                        ...state.validate.anotherUserContacts,
                        [action.data.name]: action.data.value
                    }
                }
            }
        }
        case SET_VALIDATE_DELIVERY_DATA: {
            return {
                ...state,
                validate: {
                    ...state.validate,
                    deliveryData: {
                        ...state.validate.deliveryData,
                        [action.data.name]: action.data.value
                    }
                }
            }
        }
        case RESET_VALIDATE: {
            return {
                ...state,
                validate: {
                    form: {
                        name: true,
                        email: true,
                        phone: true
                    },
                    anotherUserContacts: {
                        name: true,
                        email: true,
                        phone: true
                    },
                    shipTimeSlotInfo: {
                        shipTimeSlotId: true
                    },
                    paymentInfo: {
                        paymentType: true
                    },
                    deliveryData: {
                        apt: true,
                        paymentTypeId: true,
                        shipTimeSlotId: true
                    }
                }
            }
        }
        case CREATE_ORDER_INIT_VALIDATE: {
            return {
                ...state,
                validate: {
                    ...state.validate,
                    ...action.data
                }
            }
        }
        case CREATE_ORDER__SET_TIMESLOT: {
            return {
                ...state,
                shipTimeSlotInfo: {
                    ...action.data
                }
            }
        }
        case CREATE_ORDER__SET_PAYMENT: {
            return {
                ...state,
                paymentInfo: {
                    ...action.data
                }
            }
        }
        case INIT_CREATE_ORDER_DATA: {
            return action.data
        }
        case PREORDER_CHANGE: {
            return {
                ...state,
                preorder: {
                    ...state.preorder,
                    ...action.data
                }
            }
        }
        case PREORDER_STOP: {
            try {
                if (state.preorder?.connection) {
                    state.preorder.connection.stop()
                }
            } catch (e) {
                console.error(e)
            }

            return {
                ...state,
                preorder: {
                    ...state.preorder,
                    connection: null,
                    message: null
                }
            }
        }
        case PREORDER_STOP_LOADING: {
            return {
                ...state,
                preorder: {
                    ...state.preorder,
                    isLoading: false
                }
            }
        }
        case PREORDER_CLEAR: {
            if (state.preorder.connection) {
                state.preorder.connection.stop()
            }
            return {
                ...state,
                preorder: {
                    ...state.preorder,
                    connection: null,
                    message: null,
                    uid: ''
                }
            }
        }
        default:
            return state
    }
}
