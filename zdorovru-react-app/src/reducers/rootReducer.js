import { combineReducers } from 'redux'
import company from './companyReducer'
import customer from './customerReducer'
import catalog from './catalogReducer'
import createOrder from './createOrderReducer'

const rootReducer = combineReducers({
    company,
    customer,
    catalog,
    createOrder
})

export default rootReducer
