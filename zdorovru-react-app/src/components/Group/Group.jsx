import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { useSelector, useDispatch } from 'react-redux'
import * as _ from 'lodash'
import { useRouter } from 'next/router'
import Head from 'next/head'
import * as S from './Group.styled'
import * as SharedStyled from '../../styles/base.styled'
import * as Theme from '../../styles/theme-const'
import Paginator from '../shared/Paginator/Paginator'
import CatalogNavigation from '../CatalogNavigation/CatalogNavigation'
import Filter from '../shared/Filter/Filter'
import CatalogGoodItem from '../CatalogGoodItem/CatalogGoodItem'
import { toggleLikedGood } from '../../actions/customer'
import { filterGoods, FilterTypes, usePage } from '../../helpers/site'
import { removerGoodsFilter, setGoodsFilter } from '../../actions/catalog'

const Group = (props) => {
    const goods = useSelector((state) => state.catalog.goods)
    const shipment = useSelector((state) => state.customer.shipment)
    const goodsFilters = useSelector((state) => state.catalog.goodsFilters)
    const goodsData = useSelector((state) => state.catalog.goodsData)
    const likedGoods = useSelector((state) => state.customer.likedGoods)
    const groups = useSelector((state) => state.catalog.mainGroups)
    const dispatch = useDispatch()
    const router = useRouter()
    const [goodsGroups, setGoodsGroups] = useState([])
    const [filteredGoods, setFilteredGoods] = useState([])

    const page = usePage()

    const clearFilters = async () => {
        await dispatch(removerGoodsFilter(FilterTypes.makers))
        await dispatch(removerGoodsFilter(FilterTypes.outForms))
    }

    useEffect(() => {
        clearFilters()
    }, [])

    useEffect(() => {
        const goodsgroups = []
        goods.forEach((good) => {
            if (good.webData.goodGroups && good.webData.goodGroups.length > 0) {
                good.webData.goodGroups.forEach((group) =>
                    goodsgroups.push({ goodId: good.webData.goodId, groupId: group.clsGroupID })
                )
            }
        })
        setGoodsGroups(goodsgroups)
    }, [goods.length])

    const currentFilters = [
        {
            name: FilterTypes.outForms,
            title: 'Формы выпуска'
        },
        {
            name: FilterTypes.makers,
            title: 'Производитель'
        }
    ]

    useEffect(() => {
        const newFilteredGoods = filterGoods(goods, goodsFilters, [FilterTypes.makers, FilterTypes.outForms])
        setFilteredGoods(newFilteredGoods)
    }, [router.asPath, goodsFilters, goods])

    const basketItems = useSelector((state) => state.customer.basketItems)
    const getBasketItem = (goodId) => {
        const basketItem = basketItems.find((x) => x.webData.goodId === goodId)
        return basketItem ? basketItem.count : null
    }

    // const selectPageHandler = (pageNumber) => {
    //     setCurrentPage(pageNumber)
    // }
    const changeSelectedFilter = (name) => async (newItems) => {
        await dispatch(setGoodsFilter(name, newItems))
        if (page.number !== 1) page.changePage(1)
    }

    const goodsPageCount = Math.ceil(filteredGoods.length / Theme.goodsCountOnPage)
    const startNumberGoods = (page.number - 1) * Theme.goodsCountOnPage
    const goodsVisible = filteredGoods.slice(startNumberGoods, startNumberGoods + Theme.goodsCountOnPage)

    const toggleLikeHandler = async (webId) => {
        await dispatch(toggleLikedGood(webId))
    }
    const group = groups.find((x) => x.id === props.groupId)
    return (
        <React.Fragment>
            {group && (
                <Head>
                    <title>{group.title} - купить товары в интернет-аптеке ЗДОРОВ.ру</title>
                </Head>
            )}
            <S.Container>
                <SharedStyled.LeftMenu>
                    {!props.isSearch && (
                        <CatalogNavigation
                            groups={[{ groupId: props.groupId, isDefault: true }]}
                            goodsGroups={goodsGroups}
                            selectedGroup={props.groupId}
                        />
                    )}
                    {currentFilters.map((cf) => (
                        <Filter
                            key={cf.name}
                            title={cf.title}
                            items={goodsData[cf.name]}
                            selectedItems={goodsFilters[cf.name]}
                            changeSelectedItemsHandler={changeSelectedFilter(cf.name)}
                        />
                    ))}
                </SharedStyled.LeftMenu>
                <SharedStyled.RightBlock>
                    <S.GoodsContainer>
                        <Paginator
                            pageCount={goodsPageCount}
                            currentPage={page.number}
                            selectPageHandler={page.changePage}
                        />
                        <S.GoodListContainer>
                            <S.GoodList>
                                {goodsVisible.length > 0 &&
                                    goodsVisible.map((good) => (
                                        <CatalogGoodItem
                                            key={good.webData.goodId}
                                            basketItemCount={getBasketItem(good.webData.goodId)}
                                            availableStocksCount={good.availableStocksCount}
                                            analoguesQty={good.analoguesQty}
                                            anotherOutFormsQty={good.anotherOutFormsQty}
                                            webData={good.webData}
                                            groupId={props.groupId}
                                            stockPrice={good.stockPrice}
                                            deliveryPrice={good.deliveryPrice}
                                            minPrice={good.minPrice}
                                            deliveryStatus={good.deliveryStatus}
                                            isDemand={good.isDemand}
                                            aptEtaDateTime={good.aptEtaDateTime}
                                            isAvailable={good.isAvailable}
                                            isAvailableOnStock={good.isAvailableOnStock}
                                            isStrictlyByPrescription={good.isStrictlyByPrescription}
                                            isOriginalGood={good.isOriginalGood}
                                            isCold={good.isCold}
                                            liked={likedGoods.includes(good.webData.goodId)}
                                            toggleLikeHandler={toggleLikeHandler}
                                            shipment={shipment}
                                        />
                                    ))}
                            </S.GoodList>
                            {/* <SharedStyled.BlockBanner>
                                <SharedStyled.BannerImageTest />
                            </SharedStyled.BlockBanner> */}
                        </S.GoodListContainer>
                        <Paginator
                            pageCount={goodsPageCount}
                            currentPage={page.number}
                            selectPageHandler={page.changePage}
                        />
                    </S.GoodsContainer>
                </SharedStyled.RightBlock>
            </S.Container>
        </React.Fragment>
    )
}

export default Group

Group.propTypes = {
    isSearch: PropTypes.bool,
    groupId: PropTypes.number.isRequired
}

Group.defaultProps = {
    isSearch: false
}
