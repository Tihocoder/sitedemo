import styled from 'styled-components'

export const Container = styled.div`
    display: flex;
    justify-content: space-between;
    width: 100%;
`
export const GoodsContainer = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
`
export const GoodListContainer = styled.div`
    display: flex;
    padding-top: 1rem;
`
export const GoodList = styled.div`
    display: flex;
    flex-direction: column;
    padding-bottom: 1rem;
    width: 100%;
`
