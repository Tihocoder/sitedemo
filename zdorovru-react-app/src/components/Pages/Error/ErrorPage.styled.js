import styled from 'styled-components'

export const Container = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    height: 100%;
`
export const Body = styled.div`
    display: flex;
    flex-direction: column;
    height: 100%;
    justify-content: center;
    min-height: calc(100vh - 5rem);
    & > * {
        margin-bottom: 1rem;
    }
`
export const TextError = styled.div`
    font-size: 1.4rem;
`
