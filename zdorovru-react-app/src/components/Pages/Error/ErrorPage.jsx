import React, { useEffect } from 'react'
// eslint-disable-next-line no-unused-vars
import Button from '@shared/Button/Button'
// import PropTypes from 'prop-types'
import { ElementSizeType } from '@styles/theme-const'
import { useRouter } from 'next/router'
import ym from 'react-yandex-metrika'
import { errorLog } from '@api/siteApis'
import * as S from './ErrorPage.styled'

const ErrorPage = (props) => {
    const router = useRouter()

    // useEffect(() => {
    //     const saveLogError = async () => {
    //         // eslint-disable-next-line no-unused-vars
    //         const response = await errorLog({
    //             description: 'UnknownError',
    //             error: props.error
    //         })
    //     }
    //     ym('reachGoal', 'UnknownError')
    //     saveLogError()
    // }, [])
    return (
        <S.Container>
            <S.Body>
                <S.TextError>Произошла неизвестная ошибка</S.TextError>
                <Button elementSizeTypeValue={ElementSizeType.large} clickHandler={() => router.push('/')}>
                    вернуться на главную
                </Button>
            </S.Body>
        </S.Container>
    )
}
//
// ErrorPage.propTypes = {
//     ua: PropTypes.string.isRequired
// }

export default ErrorPage
