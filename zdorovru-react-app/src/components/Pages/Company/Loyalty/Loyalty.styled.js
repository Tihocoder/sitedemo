import styled from 'styled-components'


export const CartsBlock = styled.div`
    margin-top: 1.5rem;
    padding-right: 3rem;
    display: flex;
    align-items: center;
    & > div {
        margin-top: 0;
        margin-right: 3rem;
    }
`
