import React from 'react'
import PropTypes from 'prop-types'
import Link from 'next/link'
import * as IS from '../../../../styles/info.styled'
import * as S from './Loyalty.styled'
import * as BS from '../../../../styles/base.styled'
import { generateDate } from '../../../../helpers/site'

const Loyalty = (props) => (
    <IS.Container>
        <IS.Title>Программа лояльности</IS.Title>
        <IS.SpaceBetween>
            <IS.CartBlock>
                <Link href="/company/carts/zdorov" passHref>
                    <IS.ImageZdorovCart />
                </Link>
            </IS.CartBlock>
            <IS.ContentBlock>
                <IS.Header>Карта постоянного покупателя в подарок в аптеках:</IS.Header>
                <IS.BlockBody>
                    <IS.Ul>
                        {props.giftCardsCampaigns &&
                            props.giftCardsCampaigns.length > 0 &&
                            props.giftCardsCampaigns.map((x) => <IS.Li>{x.stockTitle}</IS.Li>)}
                    </IS.Ul>
                    <IS.TextInfo>
                        Карта позволяет покупать товары со скидкой. Скидка будет распространяться начиная с
                        первой покупки, независимо от суммы
                    </IS.TextInfo>
                    <IS.TextInfo>
                        <Link href="/company/carts/zdorov" passHref>
                            <IS.A>
                                Подробности о возможностях и правилах использования карты постоянного
                                покупателя.
                            </IS.A>
                        </Link>
                    </IS.TextInfo>
                    <IS.TextInfo>Спешите! Время акции ограничено!</IS.TextInfo>
                </IS.BlockBody>
            </IS.ContentBlock>
        </IS.SpaceBetween>
        {props.bonusCartsCampaign && props.bonusCartsCampaign.length === 0 ? (
            <React.Fragment />
        ) : (
            <IS.SpaceBetween>
                <IS.CartBlock>
                    <IS.ImageDiscount />
                </IS.CartBlock>
                <IS.ContentBlock>
                    <IS.Header>Дарим лекарства и изделия медицинского назначения на здоровье!</IS.Header>
                    <IS.BlockBody>
                        <IS.TextInfo>Список аптек, участвующих в акции и дата окончания акции:</IS.TextInfo>
                        <IS.TableCheckerContainer>
                            <BS.Table>
                                <BS.TableHeader>
                                    <BS.TableHeaderRow>
                                        <BS.TableHeaderColumn>Аптека</BS.TableHeaderColumn>
                                        <BS.TableHeaderColumn> Дата окончания</BS.TableHeaderColumn>
                                    </BS.TableHeaderRow>
                                </BS.TableHeader>
                                <BS.TableBody>
                                    {props.bonusCartsCampaign &&
                                        props.bonusCartsCampaign.length > 0 &&
                                        props.bonusCartsCampaign.map((x) => (
                                            <BS.TableRow>
                                                <BS.TableColumn>{x.stockTitle}</BS.TableColumn>
                                                <BS.TableColumn>{generateDate(x.endDate)}</BS.TableColumn>
                                            </BS.TableRow>
                                        ))}
                                </BS.TableBody>
                            </BS.Table>
                        </IS.TableCheckerContainer>
                        <IS.UlTitle>
                            В районах аптек из списка проходит раздача конвертов с картами постоянного
                            покупателя. На конвертах указана сумма подарка. Карта позволяет:
                        </IS.UlTitle>
                        <IS.Ul>
                            <IS.Li>Получить любые товары на сумму подарка бесплатно</IS.Li>
                            <IS.Li>
                                Получить скидку в размере суммы подарка, если сумма заказа превышает сумму
                                подарка. Вам надо доплатить только разницу
                            </IS.Li>
                            <IS.Li>
                                В дальнейшем получать скидки на покупки согласно{' '}
                                <Link href="/company/carts/zdorov" passHref>
                                    <IS.A>
                                        правилам использования карты постоянного покупателя «ЗДОРОВ.ру»
                                    </IS.A>
                                </Link>
                            </IS.Li>
                        </IS.Ul>
                        <IS.UlTitle>Для участия в акции необходимо:</IS.UlTitle>
                        <IS.Ul>
                            <IS.Li>
                                Получить конверт с подарочной картой у наших промоутеров или найти его в своем
                                почтовом ящике
                            </IS.Li>
                            <IS.Li>Посетить аптеку из списка</IS.Li>
                            <IS.Li>Предъявить подарочную карту сотруднику аптеки</IS.Li>
                            <IS.Li>Заполнить анкету</IS.Li>
                            <IS.Li>
                                Предъявить социальную карту москвича или получить на мобильный телефон код
                                подтверждения
                            </IS.Li>
                        </IS.Ul>
                    </IS.BlockBody>
                </IS.ContentBlock>
            </IS.SpaceBetween>
        )}
        <IS.Title>Карты</IS.Title>
        <S.CartsBlock>
            <IS.ContentBlock>
                <IS.CartBlock>
                    <Link href="/company/carts/zdorov" passHref>
                        <IS.ImageZdorovCart />
                    </Link>
                </IS.CartBlock>
                <IS.TextInfo>
                    <Link href="/company/carts/zdorov" passHref>
                        <IS.A>Карта постоянного покупателя «ЗДОРОВ.ру»</IS.A>
                    </Link>
                </IS.TextInfo>
            </IS.ContentBlock>
            <IS.ContentBlock>
                <IS.CartBlock>
                    <Link href="/company/carts/mos" passHref>
                        <IS.ImageMosCart />
                    </Link>
                </IS.CartBlock>
                <IS.TextInfo>
                    <Link href="/company/carts/mos" passHref>
                        <IS.A>Социальная карта москвича</IS.A>
                    </Link>
                </IS.TextInfo>
            </IS.ContentBlock>
        </S.CartsBlock>
    </IS.Container>
)

Loyalty.propTypes = {
    bonusCartsCampaign: PropTypes.arrayOf(
        PropTypes.shape({
            stockTitle: PropTypes.string.isRequired,
            endDate: PropTypes.string
        })
    ),
    giftCardsCampaigns: PropTypes.arrayOf(
        PropTypes.shape({
            stockTitle: PropTypes.string.isRequired
        })
    )
}

export default Loyalty
