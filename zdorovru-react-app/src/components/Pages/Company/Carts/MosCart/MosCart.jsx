import React from 'react'
import * as IS from '../../../../../styles/info.styled'

const MosCart = (props) => {
    return (
        <IS.Container>
            <IS.Title>Социальная карта москвича</IS.Title>
            <IS.ContentBlock>
                <IS.CartBlock>
                    <IS.ImageMosCart />
                </IS.CartBlock>
                <IS.BlockBody>
                    <IS.TextInfo>Социальная карта москвича даёт право:</IS.TextInfo>
                    <IS.Ul>
                        <IS.Li>Бесплатно получить карту постоянного покупателя «ЗДОРОВ.ру».</IS.Li>
                        <IS.Li>Покупать товары со скидкой, независимо он суммы покупок по карте.</IS.Li>
                        <IS.Li>Размер скидки по карте устанавливается на каждый товар индивидуально.</IS.Li>
                        <IS.Li>Скидки не предоставляются на товары, реализуемые по минимальным ценам</IS.Li>
                        <IS.Li>Карта действует только в аптеках торговой марки «ЗДОРОВ.ру».</IS.Li>
                        <IS.Li>
                            Узнать цены со скидкой можно на ценниках в аптеке, по телефону справочной службы,
                            у сотрудников аптеки или на сайте zdorov.ru
                        </IS.Li>
                    </IS.Ul>
                    <IS.TextInfo>
                        Для возможности получения скидок необходимо заполнить анкету. Получить анкету можно в
                        одной из наших аптек «ЗДОРОВ.ру»
                    </IS.TextInfo>
                    <IS.TextInfo>
                        Более подробную информацию по получению скидок можно получить в{' '}
                        <IS.A>разделе «Карта постоянного покупателя»</IS.A>.
                    </IS.TextInfo>
                </IS.BlockBody>
            </IS.ContentBlock>
        </IS.Container>
    )
}

export default MosCart
