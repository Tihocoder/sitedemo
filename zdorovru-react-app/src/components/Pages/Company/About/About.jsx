import React from 'react'
import * as IS from '../../../../styles/info.styled'

const About = (props) => (
    <IS.Container isMobile={props.isMobile}>
        {!props.isMobile && <IS.Title>О компании</IS.Title>}
        <IS.ContentBlock>
            <IS.BlockBody>
                <IS.TextInfo>
                    Наша аптечная сеть работает с 2003 года. Политика компании – продажа широкого ассортимента
                    лекарств и изделий медицинского назначения по минимально возможным ценам.
                </IS.TextInfo>
                <IS.TextInfo>
                    В 2014 году открываются аптеки под новым брендом «ЗДОРОВ.ру». Помимо низких цен и широкого
                    ассортимента, они характеризуются внедрением программ поддержки постоянных покупателей и
                    малообеспеченных слоев населения.
                </IS.TextInfo>
                <IS.TextInfo>
                    Все препараты сертифицированы. Прямые контракты с производителями и крупнейшими
                    дистрибьюторами исключают возможность появления фальсификатов. Строго соблюдаются условия
                    хранения и сроки годности.
                </IS.TextInfo>
            </IS.BlockBody>
        </IS.ContentBlock>
        <IS.ContentBlock>
            <IS.Header>Почему у нас низкие цены:</IS.Header>
            <IS.BlockBody>
                <IS.Ul>
                    <IS.Li>
                        Прямые поставки от производителей – наша компания имеет оптовую фармацевтическую
                        лицензию
                    </IS.Li>
                    <IS.Li>
                        Низкая наценка – за счёт большого оборота каждой аптеки мы можем продавать лекарства с
                        низкой наценкой
                    </IS.Li>
                    <IS.Li>
                        Максимальные скидки у дистрибьюторов – за счёт большого объема закупаемого товара и
                        централизованной доставки
                    </IS.Li>
                    <IS.Li>
                        Снижение затрат – наша компания осуществляет централизованные поставки с собственного
                        склада. Закупки и управление осуществляются центральным офисом.
                    </IS.Li>
                </IS.Ul>
            </IS.BlockBody>
        </IS.ContentBlock>
        <IS.ContentBlock>
            <IS.Header>Почему цены в наших аптеках различаются:</IS.Header>
            <IS.BlockBody>
                <IS.TextInfo>
                    Отпускные цены зависят от многих факторов. Вот несколько из них: арендные ставки, затраты
                    на доставку товара в аптеки, проходимость аптеки, цены поставки конкретной партии и т.д.
                </IS.TextInfo>
            </IS.BlockBody>
        </IS.ContentBlock>
        <IS.ContentBlock>
            <IS.Header>Состав аптечной сети:</IS.Header>
            <IS.BlockBody>
                <IS.TextInfo>
                    <IS.A target="_blank" href={`${process.env.BASE_PATH || ''}/hipporuber`}>
                        ООО "ГиппоРубер"
                    </IS.A>
                </IS.TextInfo>
                <IS.TextInfo>
                    <IS.A target="_blank" href={`${process.env.BASE_PATH || ''}/zdorovru`}>
                        ООО "ЗДОРОВ.ру"
                    </IS.A>
                </IS.TextInfo>
                <IS.TextInfo>
                    <IS.A target="_blank" href={`${process.env.BASE_PATH || ''}/technopharm`}>
                        ООО "Технофарм"
                    </IS.A>
                </IS.TextInfo>
                <IS.TextInfo>
                    <IS.A target="_blank" href={`${process.env.BASE_PATH || ''}/alphapharm`}>
                        ООО "АльфаФарм"
                    </IS.A>
                </IS.TextInfo>
                <IS.TextInfo>
                    <IS.A target="_blank" href={`${process.env.BASE_PATH || ''}/nordpharm`}>
                        ООО "НордФарм"
                    </IS.A>
                </IS.TextInfo>
            </IS.BlockBody>
        </IS.ContentBlock>
    </IS.Container>
)

export default About
