import React, { useEffect, useState } from 'react'
import { CompanyEnum } from '../../../../consts'
import * as IS from '../../../../styles/info.styled'
import * as BS from '../../../../styles/base.styled'
import { getCompanyStock } from '../../../../api/siteApis'
import { generateTime } from '../../../../helpers/site'

const Technopharm = (props) => {
    const [stocks, initStocks] = useState([])
    useEffect(() => {
        const initCompany = async () => {
            const companyStocks = await getCompanyStock(CompanyEnum.TechnoPharm)
            initStocks(companyStocks)
        }
        initCompany()
    }, [])
    return (
        <IS.SpaceBetween isMobile={props.isMobile}>
            {!props.isMobile && (
                <BS.LeftMenu>
                    <IS.Title>Технофарм</IS.Title>
                    <IS.NavPageBlock>
                        <IS.NavItem href="#organization">Аптечная организация</IS.NavItem>
                        <IS.NavItem href="#stocks">Структурные подразделения и графики работы</IS.NavItem>
                        <IS.NavItem href="#license">Лицензия и разрешения</IS.NavItem>
                        <IS.NavItem href="#distance">Условия дистанционной торговли</IS.NavItem>
                        <IS.NavItem href="#contact">Контакты</IS.NavItem>
                        <IS.NavItem href="#roszdrav">Сведения о контролирующих органах</IS.NavItem>
                    </IS.NavPageBlock>
                </BS.LeftMenu>
            )}
            <BS.RightBlock>
                <IS.Container>
                    <IS.ContentBlock>
                        <IS.Header id="organization">Аптечная организация</IS.Header>
                        <IS.BlockBody>
                            <IS.TableContainerDescription>
                                <BS.Table>
                                    <BS.TableRow>
                                        <BS.TableColumn>Наименование организации:</BS.TableColumn>
                                        <BS.TableColumn>ООО «Технофарм»</BS.TableColumn>
                                    </BS.TableRow>
                                    <BS.TableRow>
                                        <BS.TableColumn>ОГРН:</BS.TableColumn>
                                        <BS.TableColumn>1167746218364</BS.TableColumn>
                                    </BS.TableRow>
                                    <BS.TableRow>
                                        <BS.TableColumn>ИНН/КПП:</BS.TableColumn>
                                        <BS.TableColumn>7708284070/770801001</BS.TableColumn>
                                    </BS.TableRow>
                                    <BS.TableRow>
                                        <BS.TableColumn>Юридический адрес:</BS.TableColumn>
                                        <BS.TableColumn>
                                            107140, г. Москва, Леснорядский переулок, д. 18, стр. 2
                                        </BS.TableColumn>
                                    </BS.TableRow>
                                    <BS.TableRow>
                                        <BS.TableColumn>Почтовый адрес:</BS.TableColumn>
                                        <BS.TableColumn>107140, г. Москва, а/я 142</BS.TableColumn>
                                    </BS.TableRow>
                                </BS.Table>
                            </IS.TableContainerDescription>
                        </IS.BlockBody>
                    </IS.ContentBlock>
                    <IS.ContentBlock>
                        <IS.Header id="stocks">Структурные подразделения и графики работы</IS.Header>
                        <IS.TableCheckerContainer>
                            <BS.Table>
                                <BS.TableHeaderRow>
                                    <BS.TableHeaderColumn>структурное подразделение</BS.TableHeaderColumn>
                                    <BS.TableHeaderColumn>Адрес</BS.TableHeaderColumn>
                                    <BS.TableHeaderColumn>Время работы (ежедневно)</BS.TableHeaderColumn>
                                </BS.TableHeaderRow>
                                {stocks.length > 0 &&
                                    stocks.map((x) => (
                                        <BS.TableHeaderRow key={x.id}>
                                            <BS.TableColumn>{`АП "${x.name}"`}</BS.TableColumn>
                                            <BS.TableColumn>{x.address}</BS.TableColumn>
                                            <BS.TableColumn>{`${generateTime(x.optTimeFrom)}-${generateTime(
                                                x.optTimeTo
                                            )}`}</BS.TableColumn>
                                        </BS.TableHeaderRow>
                                    ))}
                            </BS.Table>
                        </IS.TableCheckerContainer>
                    </IS.ContentBlock>
                    <IS.ContentBlock>
                        <IS.Header id="license">Лицензия и разрешения</IS.Header>
                        <IS.BlockBody>
                            <IS.TextInfo>
                                <IS.A
                                    target="_blank"
                                    href={`${process.env.BASE_PATH || ''}/files/Технофарм-7708284070.pdf`}>
                                    Графическое изображение лицензии
                                </IS.A>
                            </IS.TextInfo>
                            <IS.TextInfo>
                                <IS.A
                                    target="_blank"
                                    href={`${
                                        process.env.BASE_PATH || ''
                                    }/files/Разрешение-на-ДТ-Технофарм.pdf`}>
                                    Разрешение на дистанционную торговлю
                                </IS.A>
                            </IS.TextInfo>
                        </IS.BlockBody>
                    </IS.ContentBlock>
                    <IS.ContentBlock>
                        <IS.Header id="distance">Условия дистанционной торговли</IS.Header>
                        <IS.BlockBody>
                            <IS.TextInfo>
                                <IS.A
                                    target="_blank"
                                    href={`${process.env.BASE_PATH || ''}/condition/delivery`}>
                                    Правила доставки, порядок оплаты и возврата
                                </IS.A>
                            </IS.TextInfo>
                        </IS.BlockBody>
                    </IS.ContentBlock>
                    <IS.ContentBlock>
                        <IS.Header id="contact">Контакты</IS.Header>
                        <IS.BlockBody>
                            <IS.TextInfo>Справочная служба (прием заказов)</IS.TextInfo>
                            <IS.TableContainerDescription>
                                <BS.Table>
                                    <BS.TableRow>
                                        <BS.TableColumn>Телефон:</BS.TableColumn>
                                        <BS.TableColumn>(495) 363-35-00</BS.TableColumn>
                                    </BS.TableRow>
                                    <BS.TableRow>
                                        <BS.TableColumn>Время работы:</BS.TableColumn>
                                        <BS.TableColumn>07:00-23:00 ежедневно</BS.TableColumn>
                                    </BS.TableRow>
                                    <BS.TableRow>
                                        <BS.TableColumn>e-mail:</BS.TableColumn>
                                        <BS.TableColumn>help@zdorov.ru</BS.TableColumn>
                                    </BS.TableRow>
                                </BS.Table>
                            </IS.TableContainerDescription>
                            <IS.TextInfo>
                                Сотрудник, ответственный за размещение информации о лекарственных препаратах:
                            </IS.TextInfo>
                            <IS.TableContainerDescription>
                                <BS.Table>
                                    <BS.TableRow>
                                        <BS.TableColumn>ФИО:</BS.TableColumn>
                                        <BS.TableColumn>Докина Кристина Сергеевна</BS.TableColumn>
                                    </BS.TableRow>
                                    <BS.TableRow>
                                        <BS.TableColumn>Телефон:</BS.TableColumn>
                                        <BS.TableColumn>(495) 363-3500 доб.139</BS.TableColumn>
                                    </BS.TableRow>
                                    <BS.TableRow>
                                        <BS.TableColumn>e-mail:</BS.TableColumn>
                                        <BS.TableColumn> dist@zdorov.ru</BS.TableColumn>
                                    </BS.TableRow>
                                    <BS.TableRow>
                                        <BS.TableColumn>Время работы:</BS.TableColumn>
                                        <BS.TableColumn>09:00-18:00 по будням</BS.TableColumn>
                                    </BS.TableRow>
                                </BS.Table>
                            </IS.TableContainerDescription>
                        </IS.BlockBody>
                    </IS.ContentBlock>
                    <IS.ContentBlock>
                        <IS.Header id="roszdrav">Сведения о контролирующих органах</IS.Header>
                        <IS.BlockBody>
                            <IS.TextInfo>Федеральная служба по надзору в сфере здравоохранения</IS.TextInfo>
                            <IS.TableContainerDescription>
                                <BS.Table>
                                    <BS.TableRow>
                                        <BS.TableColumn>Адрес:</BS.TableColumn>
                                        <BS.TableColumn>
                                            109074, Москва, Славянская площадь, д. 4, стр. 1
                                        </BS.TableColumn>
                                    </BS.TableRow>
                                    <BS.TableRow>
                                        <BS.TableColumn>Телефон:</BS.TableColumn>
                                        <BS.TableColumn>(495) 698-45-38, (499) 578-02-30</BS.TableColumn>
                                    </BS.TableRow>
                                    <BS.TableRow>
                                        <BS.TableColumn>Сайт:</BS.TableColumn>
                                        <BS.TableColumn>https://roszdravnadzor.ru/</BS.TableColumn>
                                    </BS.TableRow>
                                </BS.Table>
                            </IS.TableContainerDescription>
                            <IS.TextInfo>
                                Территориальный орган Росздравнадзора по г. Москве и Московской области
                            </IS.TextInfo>
                            <IS.TableContainerDescription>
                                <BS.Table>
                                    <BS.TableRow>
                                        <BS.TableColumn>Адрес:</BS.TableColumn>
                                        <BS.TableColumn>
                                            127206, г. Москва, ул. Вучетича, д.12 А
                                        </BS.TableColumn>
                                    </BS.TableRow>
                                    <BS.TableRow>
                                        <BS.TableColumn>Телефон:</BS.TableColumn>
                                        <BS.TableColumn>(495) 611-47-74, (916) 256-76-76</BS.TableColumn>
                                    </BS.TableRow>
                                    <BS.TableRow>
                                        <BS.TableColumn>Сайт:</BS.TableColumn>
                                        <BS.TableColumn>http://77reg.roszdravnadzor.ru/</BS.TableColumn>
                                    </BS.TableRow>
                                    <BS.TableRow>
                                        <BS.TableColumn>e-mail:</BS.TableColumn>
                                        <BS.TableColumn>dist@zdorov.ru</BS.TableColumn>
                                    </BS.TableRow>
                                </BS.Table>
                            </IS.TableContainerDescription>
                        </IS.BlockBody>
                    </IS.ContentBlock>
                </IS.Container>
            </BS.RightBlock>
        </IS.SpaceBetween>
    )
}

Technopharm.propTypes = {}

export default Technopharm
