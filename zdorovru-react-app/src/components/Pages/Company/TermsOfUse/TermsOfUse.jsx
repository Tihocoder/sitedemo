import React from 'react'
import PropTypes from 'prop-types'
import * as IS from '../../../../styles/info.styled'

const TermsOfUse = (props) => {
    return (
        <IS.Container>
            <IS.Title>Пользовательское соглашение</IS.Title>
            <IS.RuleContainer
                dangerouslySetInnerHTML={{
                    __html: props.children
                }}
            />
        </IS.Container>
    )
}

TermsOfUse.propTypes = {
    children: PropTypes.string.isRequired
}

export default TermsOfUse
