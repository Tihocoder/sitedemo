import React from 'react'
import PropTypes from 'prop-types'
import * as IS from '../../../../styles/info.styled'

const PrivacyPolicy = (props) => {
    return (
        <IS.Container>
            <IS.Title>Политика конфиденциальности</IS.Title>
            <IS.RuleContainer
                dangerouslySetInnerHTML={{
                    __html: props.bodyHtml
                }}
            />
        </IS.Container>
    )
}

PrivacyPolicy.propTypes = {
    bodyHtml: PropTypes.string.isRequired
}
export default PrivacyPolicy
