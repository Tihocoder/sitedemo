import React from 'react'
import PropTypes from 'prop-types'
import * as IS from '../../../../styles/info.styled'
import * as S from './Vacancies.styled'

const Vacancies = (props) => {
    const contactBlock = (
        <S.ContactBlock>
            <IS.TextInfo>Отдел подбора персонала:</IS.TextInfo>
            <IS.TextInfo>8 (495) 363-35-00 доб. 138; доб. 161; доб. 185</IS.TextInfo>
            <IS.TextInfo>8 (967) 101-23-95</IS.TextInfo>
        </S.ContactBlock>
    )

    return (
        <IS.Container>
            <IS.Title>Вакансии</IS.Title>
            <S.InsideBlock>
                <IS.TextInfo>
                    Аптечная сеть «ЗДОРОВ.РУ» в связи с активным развитием и открытием новых аптек приглашает
                    на работу:
                </IS.TextInfo>
                <S.Column>
                    <IS.A href="#farm">Фармацевт/Провизор</IS.A>
                    <IS.A href="#zam">Заместитель заведующего аптекой</IS.A>
                    <IS.A href="#goodm">Специалист по приемке товара</IS.A>
                </S.Column>
                {contactBlock}
            </S.InsideBlock>
            <IS.ContentBlock>
                <IS.Header id="farm">Фармацевт/Провизор</IS.Header>
                <IS.BlockBody>
                    <S.InsideBlock>
                        <IS.UlTitle>Мы приглашаем Вас в наш дружный коллектив, если Вы имеете:</IS.UlTitle>
                        <IS.Ul>
                            <IS.Li>
                                Образование профильное фармацевтическое (высшее, среднее специальное);
                            </IS.Li>
                            <IS.Li>Действующий сертификат специалиста;</IS.Li>
                            <IS.Li>Знаете кассовую дисциплину, есть опыт работы на кассовом аппарате;</IS.Li>
                            <IS.Li>Готовность к большим нагрузкам;</IS.Li>
                        </IS.Ul>
                    </S.InsideBlock>
                    <S.InsideBlock>
                        <IS.UlTitle>Наши предложения:</IS.UlTitle>
                        <IS.Ul>
                            <IS.Li>Работа в стабильной компании;</IS.Li>
                            <IS.Li>Оформление в соответствии с трудовым законодательством РФ;</IS.Li>
                            <IS.Li>Комфортные условия работы и удобный график 2/2;</IS.Li>
                            <IS.Li>Нет товара дня, акций, тайных покупателей, СТМ, планов;</IS.Li>
                            <IS.Li>Есть возможность подработок;</IS.Li>
                            <IS.Li>
                                Стабильная и достойная оплата труда (белая заработная плата от 45000 до 70000
                                тыс. руб за 15 смен «чистыми» на карту Сбербанка РФ);
                            </IS.Li>
                            <IS.Li>Возможность карьерного роста;</IS.Li>
                        </IS.Ul>
                    </S.InsideBlock>
                    {contactBlock}
                </IS.BlockBody>
            </IS.ContentBlock>
            <IS.ContentBlock>
                <IS.Header id="zam">Заместитель заведующего аптекой</IS.Header>
                <IS.BlockBody>
                    <S.InsideBlock>
                        <IS.UlTitle>Мы приглашаем Вас в наш дружный коллектив, если Вы имеете:</IS.UlTitle>
                        <IS.Ul>
                            <IS.Li>
                                Образование профильное фармацевтическое (высшее, среднее специальное);
                            </IS.Li>
                            <IS.Li>Опыт руководства аптекой от 3 лет;</IS.Li>
                            <IS.Li>Действующий сертификат;</IS.Li>
                            <IS.Li>Знаете ассортимент лекарственных средств и сопутствующих товаров;</IS.Li>
                            <IS.Li>Знаете действующие приказы и правила розничной торговли;</IS.Li>
                        </IS.Ul>
                    </S.InsideBlock>
                    <S.InsideBlock>
                        <IS.UlTitle>Наши предложения:</IS.UlTitle>
                        <IS.Ul>
                            <IS.Li>Работа в стабильной компании;</IS.Li>
                            <IS.Li>Оформление в соответствии с трудовым законодательством РФ;</IS.Li>
                            <IS.Li>Комфортные условия работы и удобный график 5/2;</IS.Li>
                            <IS.Li>Нет товара дня, акций, тайных покупателей, СТМ, планов;</IS.Li>
                            <IS.Li>
                                Стабильная и достойная оплата труда (белая заработная плата 65000 - 70000 тыс.
                                руб. «чистыми» на карту Сбербанка РФ);
                            </IS.Li>
                        </IS.Ul>
                    </S.InsideBlock>
                    {contactBlock}
                </IS.BlockBody>
            </IS.ContentBlock>
            <IS.ContentBlock>
                <IS.Header id="goodm">Специалист по приемке товара</IS.Header>
                <IS.BlockBody>
                    <S.InsideBlock>
                        <IS.UlTitle>Мы приглашаем Вас в наш дружный коллектив, если Вы имеете:</IS.UlTitle>
                        <IS.Ul>
                            <IS.Li>Опыт работы в аптеке;</IS.Li>
                            <IS.Li>Знание аптечного ассортимента;</IS.Li>
                            <IS.Li>Вы являетесь уверенным пользователем ПК;</IS.Li>
                            <IS.Li>Готовность к большим нагрузкам;</IS.Li>
                        </IS.Ul>
                    </S.InsideBlock>
                    <S.InsideBlock>
                        <IS.UlTitle>Наши предложения:</IS.UlTitle>
                        <IS.Ul>
                            <IS.Li>Работа в стабильной компании;</IS.Li>
                            <IS.Li>Оформление в соответствии с трудовым законодательством РФ;</IS.Li>
                            <IS.Li>Комфортные условия работы и удобный график 5/2;</IS.Li>
                            <IS.Li>
                                Стабильная и достойная оплата труда (средняя заработная плата от 30000 руб. на
                                карту Сбербанка РФ);
                            </IS.Li>
                        </IS.Ul>
                    </S.InsideBlock>
                </IS.BlockBody>
                {contactBlock}
            </IS.ContentBlock>
        </IS.Container>
    )
}

Vacancies.propTypes = {}

export default Vacancies
