import styled from 'styled-components'

export const ContactBlock = styled.div`
    & > div {
        margin-top: 0.4rem;
    }
    & > div:first-child {
        margin-top: 0;
    }
`

export const InsideBlock = styled.div`
    margin-top: 1rem;
    line-height: 1.4rem;
    font-size: 0.9rem;
`
export const Column = styled.div`
    display: flex;
    flex-direction: column;
`
