import styled, { css } from 'styled-components'

export const Container = styled.div`
    margin-top: ${(props) => (props.isMobile ? '1rem' : '4rem')};
    padding: ${(props) => (props.isMobile ? '0.4rem' : '3rem 0')};
`

export const Description = styled.div`
    margin-top: 1rem;
`
const notValidatedInput = css`
    border: 1px solid ${(props) => props.theme.colors.getRedColor(0.6)};
    background-color: ${(props) => props.theme.colors.getRedColor(0.1)};
`

export const SuccessText = styled.div`
    margin-top: 1.2rem;
    font-size: 1.2rem;
    color: ${(props) => props.theme.colors.getGrayColor(0.8)};
`
export const ReturnHomeButtonContainer = styled.div`
    margin-top: 1.2rem;
    width: 16rem;
`

export const FeedBackForm = styled.div`
    margin-top: 1.2rem;
    display: flex;
    flex-wrap: wrap;
    & > div {
        margin-right: 2rem;
    }
`
export const Input = styled.input.attrs({ type: 'text' })``

export const InputTest = styled.input.attrs({ type: 'text' })`
    width: 100%;
    padding: 0.9rem 1rem;
    font-size: 1rem;
    border: 0;
    color: ${(props) => props.theme.colors.getGrayColor(0.6)};
    box-sizing: border-box;
    outline: none;
    border-radius: 0.3rem;
    transition: all 0.35s ease;
    font: inherit;
    background-color: ${(props) => props.theme.colors.getGrayColor(0.04)};
    ${(props) => props.isNotValid && notValidatedInput};

    ::placeholder {
        color: ${(props) => props.theme.colors.getGrayColor(0.4)};
    }
    &:focus {
        background-color: ${(props) => props.theme.colors.getGrayColor(0.03)};
        color: ${(props) => props.theme.colors.getGrayColor(0.6)};
        box-shadow: ${(props) => props.theme.shadows.shadowKitHover};
        ::placeholder {
            opacity: 0.3;
        }
    }
`

export const TextArea = styled.textarea`
    width: 100%;
    resize: none;
`

const inputCSS = css`
    width: 100%;
    padding: 0.9rem 1rem;
    font-size: 1rem;
    border: 0;
    color: ${(props) => props.theme.colors.getGrayColor(0.6)};
    box-sizing: border-box;
    outline: none;
    border-radius: 0.3rem;
    transition: all 0.35s ease;
    font: inherit;
    background-color: ${(props) => props.theme.colors.getGrayColor(0.04)};
    ${(props) => props.isNotValid && notValidatedInput};

    ::placeholder {
        color: ${(props) => props.theme.colors.getGrayColor(0.4)};
    }
    &:focus {
        background-color: ${(props) => props.theme.colors.getGrayColor(0.03)};
        color: ${(props) => props.theme.colors.getGrayColor(0.6)};
        box-shadow: ${(props) => props.theme.shadows.shadowKitHover};
        ::placeholder {
            opacity: 0.3;
        }
    }
`

export const ContactBlock = styled.div`
    & > * {
        margin-bottom: 1rem;
    }
    max-width: 20rem;
    /* & > input {
        ${inputCSS}
    } */
`
export const MessageBlock = styled.div`
    & > * {
        margin-bottom: 1rem;
    }
    max-width: 25rem;
    /* & > input,
    textarea {
        ${inputCSS}
    } */
    & > textarea {
        height: 8rem;
    }
`
export const Information = styled.div`
    box-sizing: border-box;
    height: 100%;
    padding: 0.6rem 0;
    display: flex;
    align-items: center;
    color: ${(props) => props.color};
    & > svg {
        height: 1.3rem;
    }
`
export const InformationText = styled.span`
    margin-left: 0.5rem;
`

export const ErrorDescription = styled.div`
    color: ${(props) => props.theme.colors.getRedColor(0.6)};
    margin-top: 1rem;
`
