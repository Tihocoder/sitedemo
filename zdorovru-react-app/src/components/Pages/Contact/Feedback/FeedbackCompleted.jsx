import React from 'react'
import * as S from './Feedback.styled'
import * as Theme from '../../../../styles/theme-const'
import { ElementSizeType } from '../../../../styles/theme-const'
import { ButtonLink } from '../../../../styles/base.styled'
import Link from 'next/link'

const buttonColors = {
    backgroundColor: Theme.ColorConsts.getGreenColor(1),
    color: Theme.ColorConsts.white,
    hoverBackgroundColor: Theme.ColorConsts.getGreenColor(0.85)
}

const FeedbackCompleted = (props) => {
    return (
        <S.Container>
            <S.SuccessText>Ваше сообщение успешно отправлено!</S.SuccessText>
            <S.ReturnHomeButtonContainer>
                <Link href="/" passHref>
                    <ButtonLink elementSizeTypeValue={ElementSizeType.regular} colors={buttonColors}>
                        Вернуться на главную
                    </ButtonLink>
                </Link>
            </S.ReturnHomeButtonContainer>
        </S.Container>
    )
}

export default FeedbackCompleted
