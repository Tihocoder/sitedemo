import React from 'react'
import InputMask from 'react-input-mask'
import Link from 'next/link'
import { useFeedBack } from '@hooks/feedback-hook'
import { ElementSizeType } from '@styles/theme-const'
import { ButtonLink } from '@styles/base.styled'
import * as Theme from '../../../../styles/theme-const'
import * as S from './Feedback.styled'
import StockItem from '../../../libs/Stock/StockItem/StockItem'
import Button from '../../../shared/Button/Button'
import ComboBox from '../../../shared/ComboBox/ComboBox'
import Input from '../../../shared/Input/Input'
import Textarea from '../../../shared/Textarea/Textarea'
import Warning from '../../../Icons/Warning'

const Feedback = (props) => {
    const feedBack = useFeedBack()
    const generateStockComboBox = () =>
        feedBack.stocksAll
            .filter((x) => x.isOpened)
            .map((stock) => ({
                id: stock.id,
                text: (
                    <StockItem
                        isMetroLocation={stock.isMetroLocation}
                        name={stock.name}
                        title={stock.title}
                        metroColor={stock.metroColor}
                    />
                )
            }))

    const buttonColors = {
        backgroundColor: Theme.ColorConsts.getGreenColor(1),
        color: Theme.ColorConsts.white,
        hoverBackgroundColor: Theme.ColorConsts.getGreenColor(0.85)
    }
    if (feedBack.isSent) {
        return (
            <S.Container>
                <S.SuccessText>Ваше сообщение успешно отправлено!</S.SuccessText>
                <S.ReturnHomeButtonContainer>
                    <Link href="/" passHref>
                        <ButtonLink elementSizeTypeValue={ElementSizeType.regular} colors={buttonColors}>
                            Вернуться на главную
                        </ButtonLink>
                    </Link>
                </S.ReturnHomeButtonContainer>
            </S.Container>
        )
    }
    const warningColor = Theme.ColorConsts.getRedColor(0.5)
    return (
        <S.Container isMobile={props.isMobile}>
            <S.Description>
                Если у Вас возникли вопросы, замечания или предложения — отправьте их нам, заполнив
                приведенную ниже форму. Спасибо!
            </S.Description>
            {feedBack.isNotValidate && (
                <S.Information color={warningColor}>
                    <Warning color={warningColor} />
                    <S.InformationText>Некоторые поля введены некорректно</S.InformationText>
                </S.Information>
            )}
            <S.FeedBackForm>
                <S.ContactBlock>
                    <Input
                        value={feedBack.formDataFeedBack.firstName}
                        onChange={feedBack.changeFormData('firstName')}
                        isValid={feedBack.validateForm.firstName}
                        placeholder="Ваше Имя"
                    />
                    <Input
                        value={feedBack.formDataFeedBack.email}
                        isValid={feedBack.validateForm.email}
                        onChange={feedBack.changeFormData('email')}
                        placeholder="Email"
                    />
                    <InputMask
                        value={feedBack.formDataFeedBack.phone}
                        onChange={feedBack.changeFormData('phone')}
                        placeholder="Ваш номер"
                        mask="+7\(999) 999 99 99">
                        <Input type="tel" disableUnderline />
                    </InputMask>
                </S.ContactBlock>
                <S.MessageBlock>
                    <ComboBox
                        placeholder="Аптека"
                        items={generateStockComboBox()}
                        selectHandler={feedBack.selectedStockHandler}
                        selectItemId={feedBack.formDataFeedBack.selectedStockId}
                    />
                    <Input
                        value={feedBack.formDataFeedBack.subjectMessage}
                        isValid={feedBack.validateForm.subjectMessage}
                        onChange={feedBack.changeFormData('subjectMessage')}
                        placeholder="Тема"
                    />
                    <Textarea
                        value={feedBack.formDataFeedBack.bodyMessage}
                        isValid={feedBack.validateForm.bodyMessage}
                        onChange={feedBack.changeFormData('bodyMessage')}
                        placeholder="Сообщение"
                    />
                </S.MessageBlock>
            </S.FeedBackForm>
            <Button elementSizeTypeValue={ElementSizeType.regular} clickHandler={feedBack.submitHandler}>
                Отправить
            </Button>
        </S.Container>
    )
}

export default Feedback
