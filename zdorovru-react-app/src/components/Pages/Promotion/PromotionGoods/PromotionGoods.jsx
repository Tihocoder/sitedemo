import React, { useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import { useDispatch, useSelector } from 'react-redux'
import * as S from './PromotionGoods.styled'
import * as SharedStyled from '../../../../styles/base.styled.js'
import * as Theme from '../../../../styles/theme-const'
import * as _ from 'lodash'
import CategoriesNavigation from '../../../CategoriesNavigation/CategoriesNavigation'
import Filter from '../../../shared/Filter/Filter'
import Paginator from '../../../shared/Paginator/Paginator'
import { getCategoriesTreeByGoodIds, getPromotionById } from '../../../../api/siteApis'
import CatalogGoodItem from '../../../CatalogGoodItem/CatalogGoodItem'
import { filterGoods, FilterTypes, usePage } from '../../../../helpers/site'
import { toggleLikedGood } from '../../../../actions/customer'
import Head from 'next/head'

const PromotionGoods = (props) => {
    const router = useRouter()
    const { promotionId } = router.query
    const goods = useSelector((state) => state.catalog.goods)
    const [categories, setCategories] = useState([])
    const [goodsCategoriesTree, setGoodsCategoriesTree] = useState([])
    const [currentPromotion, setCurrentPromotion] = useState({})
    const basketItems = useSelector((state) => state.customer.basketItems)
    const shipment = useSelector((state) => state.customer.shipment)
    const goodsFilters = useSelector((state) => state.catalog.goodsFilters)
    const goodsData = useSelector((state) => state.catalog.goodsData)
    const likedGoods = useSelector((state) => state.customer.likedGoods)
    const dispatch = useDispatch()

    const page = usePage()

    useEffect(() => {
        const initGroupsTreeByGoods = async () => {
            const selectGoods = goods.slice(0, 12)
            const categoriesTree = await getCategoriesTreeByGoodIds(selectGoods.map((x) => x.webData.goodId))
            setGoodsCategoriesTree(categoriesTree)
        }
        initGroupsTreeByGoods()
        const initCurrentPromotion = async () => {
            const promotion = await getPromotionById(promotionId)
            setCurrentPromotion(promotion)
        }
        initCurrentPromotion()
    }, [])

    const currentFilters = [
        {
            name: FilterTypes.makers,
            title: 'Производитель'
        },
        {
            name: FilterTypes.outForms,
            title: 'Формы выпуска'
        }
    ]


    const DateToString = (date) => {
        let dateInstance = new Date(date)
        let options = { year: 'numeric', month: 'long', day: 'numeric' }
        return dateInstance.toLocaleDateString('ru-RU', options)
    }
  

    const changeSelectedMakers = (name) => async (newItems) => {
        await dispatch(setGoodsFilter(name, newItems))
    }

    const filteredGoods = filterGoods(goods, goodsFilters, [FilterTypes.makers, FilterTypes.outForms])
    var startNumberGoods = (page.number - 1) * Theme.goodsCountOnPage
    const goodsVisible = filteredGoods.slice(startNumberGoods, startNumberGoods + Theme.goodsCountOnPage)
    const goodsPageCount = Math.ceil(filteredGoods.length / Theme.goodsCountOnPage)

    //const slectedGroups = groups.filter((x) => categoryIds.some((c) => c === x.id)).slice(0, 5)

    const getCategories = () => categories.slice(0, 3)

    const getBasketItem = (goodId) => {
        const basketItem = basketItems.find((x) => x.webData.goodId === goodId)
        return basketItem ? basketItem.count : null
    }
    const toggleLikeHandler = async (webId) => {
        await dispatch(toggleLikedGood(webId))
    }
    return (
        <S.Container>
            <Head>
                <title>{currentPromotion.promotionName} - акция в сети аптек ЗДОРОВ.ру</title>
            </Head>
            <S.PromoContainer>
                <S.PromoImageBlock>
                    <S.Image src={currentPromotion.imageUrl} />
                </S.PromoImageBlock>
                <S.PromoBody>
                    <S.PromoBodyInfoBlock>
                        <S.PromoBodyName>{currentPromotion.promotionName}</S.PromoBodyName>
                        <S.PromoBodyDescription
                            dangerouslySetInnerHTML={{
                                __html: currentPromotion.promotionDescription
                            }}></S.PromoBodyDescription>
                        {!currentPromotion.isInfoNoDiscount && (
                            <S.PromoDateBetween>
                                Период действия акции с {DateToString(currentPromotion.beginDate)} -{' '}
                                {DateToString(currentPromotion.endDate)}
                            </S.PromoDateBetween>
                        )}
                    </S.PromoBodyInfoBlock>

                    {!currentPromotion.isInfoNoDiscount && (
                        <S.RulePormotion>
                            * Организатор акции вправе вносить изменения в условия акции и ассортимент,
                            участвующий в акции в одностороннем порядке без предварительного уведомления.
                            Организатор акции не гарантирует постоянное наличие полного акционного
                            ассортимента в течение всего периода проведения акции.
                        </S.RulePormotion>
                    )}
                </S.PromoBody>
            </S.PromoContainer>
            {goodsVisible.length > 0 && (
                <S.ContainerBody>
                    <SharedStyled.LeftMenu>
                        <S.Categories>
                            <CategoriesNavigation categories={goodsCategoriesTree} />
                        </S.Categories>
                        {currentFilters.map((cf) => (
                            <Filter
                                key={cf.name}
                                title={cf.title}
                                items={goodsData[cf.name]}
                                selectedItems={goodsFilters[cf.name]}
                                changeSelectedItemsHandler={changeSelectedMakers(cf.name)}
                            />
                        ))}
                    </SharedStyled.LeftMenu>
                    <SharedStyled.RightBlock>
                        <Paginator
                            pageCount={goodsPageCount}
                            currentPage={page.number}
                            selectPageHandler={page.changePage}
                        />
                        <S.GoodList>
                            {goodsVisible.map((good) => (
                                <CatalogGoodItem
                                    key={good.webData.goodId}
                                    basketItemCount={getBasketItem(good.webData.goodId)}
                                    webData={good.webData}
                                    stockPrice={good.stockPrice}
                                    deliveryPrice={good.deliveryPrice}
                                    minPrice={good.minPrice}
                                    isDemand={good.isDemand}
                                    aptEtaDateTime={good.aptEtaDateTime}
                                    deliveryStatus={good.deliveryStatus}
                                    isAvailable={good.isAvailable}
                                    isAvailableOnStock={good.isAvailableOnStock}
                                    isStrictlyByPrescription={good.isStrictlyByPrescription}
                                    isOriginalGood={good.isOriginalGood}
                                    isCold={good.isCold}
                                    shipment={shipment}
                                    liked={likedGoods.includes(good.webData.goodId)}
                                    toggleLikeHandler={toggleLikeHandler}
                                />
                            ))}
                        </S.GoodList>
                    </SharedStyled.RightBlock>
                </S.ContainerBody>
            )}
        </S.Container>
    )
}

PromotionGoods.propTypes = {}

export default PromotionGoods
