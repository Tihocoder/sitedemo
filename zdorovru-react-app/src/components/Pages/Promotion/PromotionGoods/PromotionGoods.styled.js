import styled from 'styled-components'

export const Container = styled.div`
    display: flex;
    margin-top: 1rem;
    flex-direction: column;
    padding: 0 3rem;
`

export const PromoContainer = styled.div`
    display: flex;
    max-height: 420px;
`
export const PromoImageBlock = styled.div``
export const Image = styled.img`
    width: auto;
    box-sizing: border-box;
    border-top-left-radius: 0.3rem;
    border-bottom-left-radius: 0.3rem;
`
export const PromoBody = styled.div`
    display: flex;
    flex-direction: column;
    padding: 1rem;
    justify-content: space-between;
`
export const PromoBodyInfoBlock = styled.div`
    display: flex;
    flex-direction: column;
    & > * {
        margin: 0;
        margin-bottom: 0.6rem;
    }
    & > *:last-child {
        margin-bottom: 0;
    }
`
export const PromoBodyName = styled.span`
    white-space: nowrap;
    color: ${(props) => props.theme.colors.getGrayColor(0.9)};
`

export const PromoBodyDescription = styled.p`
    font-size: 0.9rem;
`
export const PromoDateBetween = styled.span`
    font-size: 0.9rem;
`
export const After = styled.span`
    margin: 0 0.2rem;
    white-space: nowrap;
`
export const RulePormotion = styled.p`
    margin: 0;
    font-size: 0.8rem;
    max-width: 42rem;
`

export const ContainerBody = styled.div`
    display: flex;
    margin-top: 1rem;
    justify-content: space-between;
`

export const Categories = styled.div`
    margin: 0;
`
export const GoodList = styled.div`
    display: flex;
    flex-direction: column;
`
