import styled from 'styled-components'

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    color: ${(props) => props.theme.colors.getGrayColor(0.9)};
`

export const PageContainerWithPadding = styled.div`
    display: flex;
    justify-content: center;
    flex-direction: column;
    height: 100%;
    max-width: 1180px;
    min-width: 1180px;
    width: 100%;
    margin-top: 4rem;
    box-sizing: border-box;
`

export const Title = styled.h2`
    font-size: 1.4rem;
    margin: 0;
    font-weight: bold;
`

export const BockWithShadow = styled.a`
    box-shadow: rgba(0, 0, 0, 0.04) 0px -2px 8px 0px, rgba(0, 0, 0, 0.04) 0px 8px 8px 0px;
    transition: all 0.2s ease;
    box-sizing: border-box;
    padding: 1.5rem;
    border-radius: 0.3rem;
    cursor: pointer;
    text-decoration: none;
    flex: 1 1 auto;
    margin: 0 5px;
    max-width: 24rem;
    &:hover {
        box-shadow: ${(props) => props.theme.shadows.shadowKitHover};
        //box-shadow: 0px -2px 8px 0px rgba(0, 0, 0, 0.04), 0px 8px 8px 0px rgba(0, 0, 0, 0.08);
        transform: translateY(-3px);
    }
`

export const ContainerPromotion = styled.div`
    flex-wrap: wrap;
    display: flex;
    justify-content: flex-start;
    margin: 0 -5px;
    margin-top: 1rem;
    padding-left: 0.2rem;
    ${BockWithShadow} {
        padding: 0;
        margin-bottom: 3rem;
    }
`

export const Promo = styled.div`
    display: flex;
    flex-direction: column;
    height: 100%;
`
export const PromoImageHeader = styled.div`
    box-sizing: border-box;
    background-color: ${(props) => props.theme.colors.getGrayColor(0.03)};
`
export const PromoBody = styled.div`
    padding: 1.5rem;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    height: 100%;
    min-height: 7rem;
`

export const Image = styled.img`
    width: 100%;
    height: auto;
    box-sizing: border-box;
    border-top-left-radius: 0.3rem;
    border-top-right-radius: 0.3rem;
`

export const Description = styled.div``

export const BottomBlock = styled.div`
    display: flex;
    justify-content: space-between;
    color: ${(props) => props.theme.colors.getGrayColor(0.5)};
    flex-wrap: wrap;
`
export const BottomBlockIsNotActual = styled.div`
    display: flex;
    justify-content: space-between;
    color: ${(props) => props.theme.colors.getGrayColor(0.5)};
    flex-wrap: wrap;
    font-size: 0.8rem;
`

export const DescriptionBottomRigth = styled.span`
    user-select: none;
    white-space: nowrap;
`

export const Footer = styled.div`
    display: flex;
    flex-direction: row;
    flex-wrap: nowrap;
    justify-content: space-between;
`

export const DateDesct = styled.span`
    user-select: none;
    white-space: nowrap;
`
export const After = styled.span`
    margin: 0 0.2rem;
    white-space: nowrap;
`
export const Link = styled.a``
