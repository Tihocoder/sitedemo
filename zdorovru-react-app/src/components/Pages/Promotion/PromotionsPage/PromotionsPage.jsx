import React from 'react'
import PropTypes from 'prop-types'
import Link from 'next/link'
import { PromotionType } from 'src/consts'
import * as S from './PromotionsPage.styled'

const PromotionsPage = (props) => {
    const DateToString = (date) => {
        const dateInstance = new Date(date)
        const options = { year: 'numeric', month: 'long', day: 'numeric' }
        return dateInstance.toLocaleDateString('ru-RU', options)
    }
    return (
        <S.PageContainerWithPadding>
            <S.Container>
                <S.Title>Скидки и акции</S.Title>
                <S.ContainerPromotion>
                    {props.promotions.map((promotion) => (
                        <Link
                            key={promotion.promotionId}
                            href={`/catalog/promotion/${promotion.promotionId}`}
                            passHref>
                            <S.BockWithShadow key={promotion.promotionId}>
                                <S.Promo>
                                    <S.PromoImageHeader>
                                        <S.Image src={promotion.imageUrl} />
                                    </S.PromoImageHeader>
                                    <S.PromoBody>
                                        <S.Description>{promotion.promotionName}</S.Description>
                                        {promotion.isActual ? (
                                            <S.BottomBlock>
                                                <S.DateDesct>
                                                    До {DateToString(promotion.endDate)}
                                                </S.DateDesct>
                                                <S.DescriptionBottomRigth>
                                                    Подробнее об акции
                                                </S.DescriptionBottomRigth>
                                            </S.BottomBlock>
                                        ) : (
                                            <S.BottomBlockIsNotActual>
                                                <S.DateDesct>
                                                    Завершилась {DateToString(promotion.endDate)}
                                                </S.DateDesct>
                                                <S.DescriptionBottomRigth>
                                                    Подробнее об акции
                                                </S.DescriptionBottomRigth>
                                            </S.BottomBlockIsNotActual>
                                        )}
                                    </S.PromoBody>
                                </S.Promo>
                            </S.BockWithShadow>
                        </Link>
                    ))}
                </S.ContainerPromotion>
            </S.Container>
        </S.PageContainerWithPadding>
    )
}

PromotionsPage.propTypes = {
    promotions: PropTypes.arrayOf(PropTypes.shape(PromotionType)).isRequired
}

export default PromotionsPage
