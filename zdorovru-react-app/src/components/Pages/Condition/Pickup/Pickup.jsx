import React from 'react'
import PropTypes from 'prop-types'
import * as IS from '../../../../styles/info.styled'

const Pickup = (props) => {
    return (
        <IS.Container>
            <IS.Title>Условия самовывоза</IS.Title>
            <IS.RuleContainer
                dangerouslySetInnerHTML={{
                    __html: props.children
                }}
            />
        </IS.Container>
    )
}

Pickup.propTypes = {}

export default Pickup
