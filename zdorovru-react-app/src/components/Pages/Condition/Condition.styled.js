import styled from 'styled-components'
import * as Shared from '../../../styles/base.styled'

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    color: ${(props) => props.theme.colors.getGrayColor(0.7)};
    padding: 3rem;
`

export const Title = styled.h2`
    font-size: 1.4rem;
    margin: 0;
    font-weight: bold;
`

export const ContentBlock = styled.div`
    margin-top: 1.8rem;
    &:first-child {
        margin-top: 0;
    }
`

export const Header = styled.h3`
    font-size: 1.1rem;
    margin: 0;
    font-weight: bold;
`
export const BlockBody = styled.div`
    & > * {
        margin-top: 1rem;
    }

    ${Shared.Table} {
        max-width: 40rem;
    }
`

export const TextInfo = styled.div``

export const ImagesBlock = styled.div`
    justify-content: space-between;
    max-width: 30rem;
    display: flex;
`

export const Img = styled.img`
    max-width: 7rem;
    height: auto;
    max-height: 4rem;
`

export const SpanBold = styled.span`
    font-weight: bold;
`

export const Ul = styled.ul`
    padding-left: 1rem;
`

export const Li = styled.li`
    ${SpanBold} {
        margin-right: 0.5rem;
    }
`
// export const Table = styled.table`
// position: relative;
//     border: 1px solid rgba(0,0,0,.12);
//     border-collapse: collapse;
//     white-space: nowrap;
//     font-size: 13px;
//     background-color: #fff;`

// export const TableHeader = styled.thead``

// export const TableHeaderRow = styled.tr``

// export const TableHeaderColumn = styled.th``

// export const TableBody = styled.tbody``

// export const TableRow = styled.tr``

// export const TableColumn = styled.td``
