import React from 'react'
import * as Shared from '../../../../styles/base.styled'
import * as IS from '../../../../styles/info.styled'
const Delivery = (props) => {
    return (
        <IS.Container>
            <IS.Title>Условия доставки</IS.Title>
            <IS.RuleContainer
                dangerouslySetInnerHTML={{
                    __html: props.children
                }}
            />
        </IS.Container>
    )
}

export default Delivery
