import React, { useEffect } from 'react'
import * as S from './Liked.styled'
import { useDispatch, useSelector } from 'react-redux'
import CatalogGoodsWithFilters from '../../CatalogGoodsWithFilters/CatalogGoodsWithFilters'
import { initGoodsFromLiked } from '../../../actions/catalog'
import { initCustomerLikedGoods } from '../../../actions/customer'

const Liked = (props) => {
    const dispatch = useDispatch()
    const shipment = useSelector((state) => state.customer.shipment)
    const likedGoods = useSelector((state) => state.customer.likedGoods)
    const isConverted = useSelector((state) => state.customer.isConverted)

    // useEffect(() => {
    //     const mergeLikedGoods = async () => {
    //         await dispatch(initCustomerLikedGoods())
    //     }
    //     mergeLikedGoods()
    // }, [])

    useEffect(() => {
        const initGoods = async () => {
            await dispatch(initGoodsFromLiked())
        }
        initGoods()
    }, [shipment.stockId, shipment.cityId, likedGoods.length])

    return (
        <S.Container>
            <CatalogGoodsWithFilters />
        </S.Container>
    )
}

export default Liked
