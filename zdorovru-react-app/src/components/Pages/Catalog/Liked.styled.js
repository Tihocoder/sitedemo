import styled from 'styled-components'

export const Container = styled.div`
    display: flex;
    margin-top: 1rem;
    flex-direction: column;
    padding: 0 3rem;
`
export const TitlePage = styled.h2`
    font-size: 1.4rem;
    margin: 0;
    font-weight: bold;
`
