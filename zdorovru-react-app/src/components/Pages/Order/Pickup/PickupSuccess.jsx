import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import MobileHeader from '@mobileShared/MobileHeader/MobileHeader'
import * as siteHelpers from '@helpers/site'
import { clearBasketOrderCreated } from '@actions/customer'
import SymbolRuble from '@shared/SymbolRuble/SymbolRuble'
import * as Shared from '@styles/base.styled'
import PropTypes from 'prop-types'
import * as DS from '../OrderDetail.styled'

const PickupSuccess = ({ isMobile, orderDetails, orderId }) => {
    const dispatch = useDispatch()
    const basketItemsRedux = useSelector((state) => state.customer.basketItems)

    useEffect(() => {
        localStorage.removeItem('ccreate-order-data')
    }, [])

    const clearBasketAsync = async () => {
        await dispatch(clearBasketOrderCreated(orderId))
    }

    useEffect(() => {
        if (basketItemsRedux.length > 0) {
            clearBasketAsync()
        }
    }, [basketItemsRedux.length])

    const checkItems = () => {
        if (!orderDetails.items) return false
        if (orderDetails.items.length === 0) return false
        return true
    }
    return (
        <DS.Container isMobile={isMobile}>
            {isMobile && <MobileHeader isDefaultRouterBack={false}>Заказ оформлен</MobileHeader>}
            <DS.Title isMobile={isMobile}>
                Заказ принят и будет забронирован после получения СМС или звонка оператора
            </DS.Title>
            <DS.Body isMobile={isMobile}>
                <DS.DetailBlock>
                    <DS.DetailItem>
                        <DS.DetailName>Номер заказа:</DS.DetailName>
                        <DS.DetailInfoAlarm>{orderDetails.siteOrderNo}</DS.DetailInfoAlarm>
                    </DS.DetailItem>
                    <DS.DetailItem>
                        <DS.DetailName>Аптека:</DS.DetailName>
                        <DS.DetailInfo>{orderDetails.stock?.title}</DS.DetailInfo>
                    </DS.DetailItem>
                    <DS.DetailItem>
                        <DS.DetailName>Адрес:</DS.DetailName>
                        <DS.DetailInfo>{orderDetails.stock?.address}</DS.DetailInfo>
                    </DS.DetailItem>
                    <DS.DetailItem>
                        <DS.DetailName>Дата заказа:</DS.DetailName>
                        <DS.DetailInfo>{siteHelpers.convertDate(orderDetails.orderDate)}</DS.DetailInfo>
                    </DS.DetailItem>
                    <DS.DetailItem>
                        <DS.DetailName>Сумма заказа:</DS.DetailName>
                        <DS.DetailInfo>
                            {orderDetails.ordSum} <SymbolRuble />
                        </DS.DetailInfo>
                    </DS.DetailItem>

                    <DS.DetailItem>
                        <DS.DetailName>Время получения:</DS.DetailName>
                        {orderDetails.shipTimeSlotInfo?.isNearestTime ? (
                            <DS.DetailInfo>В ближайшее время</DS.DetailInfo>
                        ) : (
                            <DS.DetailInfo>{orderDetails.shipTimeSlotInfo?.timeSlotText}</DS.DetailInfo>
                        )}
                    </DS.DetailItem>
                    <DS.DetailItem>
                        <DS.DetailName>Имя получателя:</DS.DetailName>
                        <DS.DetailInfo>{orderDetails.customer.name}</DS.DetailInfo>
                    </DS.DetailItem>
                    <DS.DetailItem>
                        <DS.DetailName>Телефон получателя:</DS.DetailName>
                        <DS.DetailInfo>{orderDetails.customer.phone}</DS.DetailInfo>
                    </DS.DetailItem>
                    {orderDetails.customer.email && (
                        <DS.DetailItem>
                            <DS.DetailName>Email получателя:</DS.DetailName>
                            <DS.DetailInfo>{orderDetails.customer.email}</DS.DetailInfo>
                        </DS.DetailItem>
                    )}
                    {orderDetails.ordStateTitle && (
                        <DS.DetailItem>
                            <DS.DetailName>Статус заказа:</DS.DetailName>
                            <DS.DetailInfo>{orderDetails.ordStateTitle}</DS.DetailInfo>
                        </DS.DetailItem>
                    )}
                    {orderDetails.paymentInfo?.paymentTypeTitle && (
                        <DS.DetailItem>
                            <DS.DetailName>Способ оплаты:</DS.DetailName>
                            <DS.DetailInfo>{orderDetails.paymentInfo?.paymentTypeTitle}</DS.DetailInfo>
                        </DS.DetailItem>
                    )}
                </DS.DetailBlock>
                <Shared.Table>
                    <Shared.TableHeader>
                        <Shared.TableHeaderRow>
                            <Shared.TableHeaderColumn>Название</Shared.TableHeaderColumn>
                            <Shared.TableHeaderColumn>Кол-во</Shared.TableHeaderColumn>
                            <Shared.TableHeaderColumn>Цена</Shared.TableHeaderColumn>
                            <Shared.TableHeaderColumn>Сумма</Shared.TableHeaderColumn>
                        </Shared.TableHeaderRow>
                    </Shared.TableHeader>
                    <Shared.TableBody>
                        {checkItems() &&
                            orderDetails.items.map((item) => (
                                <Shared.TableRow key={item.goodsID}>
                                    <Shared.TableColumn>{`${item.drugTitle} ${item.outFormTitle}`}</Shared.TableColumn>
                                    <Shared.TableColumn>{item.drugCount}</Shared.TableColumn>
                                    <Shared.TableColumn>
                                        {item.price.toLocaleString('RU-ru')} <SymbolRuble />
                                    </Shared.TableColumn>
                                    <Shared.TableColumn>
                                        {(item.price * item.drugCount).toLocaleString('RU-ru')}{' '}
                                        <SymbolRuble />
                                    </Shared.TableColumn>
                                </Shared.TableRow>
                            ))}
                    </Shared.TableBody>
                </Shared.Table>
            </DS.Body>
        </DS.Container>
    )
}

PickupSuccess.propTypes = {
    orderId: PropTypes.number.isRequired,
    isMobile: PropTypes.bool.isRequired,
    orderDetails: PropTypes.shape({
        siteOrderNo: PropTypes.string.isRequired,
        items: PropTypes.array.isRequired,
        customer: PropTypes.shape({
            name: PropTypes.string,
            phone: PropTypes.string,
            email: PropTypes.string
        }),
        deliveryData: PropTypes.shape({
            addressText: PropTypes.string,
            apt: PropTypes.string,
            cashExchange: PropTypes.number,
            code: PropTypes.string,
            comment: PropTypes.string,
            deliveryPrice: PropTypes.number,
            entrance: PropTypes.string,
            floor: PropTypes.string,
            house: PropTypes.string,
            houseType: PropTypes.number,
            isNearestTime: PropTypes.bool,
            office: PropTypes.string,
            organization: PropTypes.string,
            paymentTypeId: PropTypes.number,
            timeSlotText: PropTypes.string
        }),
        ordSum: PropTypes.number,
        ordStateTitle: PropTypes.string,
        paymentTypeTitle: PropTypes.string
    })
}

export default PickupSuccess
