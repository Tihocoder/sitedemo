import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import { useDispatch, useSelector } from 'react-redux'
import * as Shared from '@styles/base.styled'
import { clearBasketOrderCreated } from '@actions/customer'
import SymbolRuble from '@shared/SymbolRuble/SymbolRuble'
import MobileHeader from '@mobileShared/MobileHeader/MobileHeader'
import * as siteHelpers from '@helpers/site'
import * as DS from '../OrderDetail.styled'

const Success = ({ orderId, orderDetails, isMobile }) => {
    const dispatch = useDispatch()
    const basketItemsRedux = useSelector((state) => state.customer.basketItems)

    useEffect(() => {
        localStorage.removeItem('create-order-data')
    }, [])

    const clearBasketAsync = async () => {
        await dispatch(clearBasketOrderCreated(orderId))
    }

    useEffect(() => {
        if (basketItemsRedux.length > 0) {
            clearBasketAsync()
        }
    }, [basketItemsRedux.length])

    const checkItems = () => {
        if (!orderDetails.items) return false
        if (orderDetails.items.length === 0) return false
        return true
    }
    const generateDeliveryInfo = () => {
        const dd = orderDetails.deliveryData
        let addressfull = dd.addressText
        if (dd.apt) {
            addressfull = `${addressfull}, кв ${dd.apt}`
        }
        if (dd.entrance) {
            addressfull = `${addressfull}, подъезд ${dd.entrance}`
        }
        if (dd.floor) {
            addressfull = `${addressfull}, этаж ${dd.floor}`
        }
        if (dd.code) {
            addressfull = `${addressfull}, домофон ${dd.code}`
        }
        if (dd.office) {
            addressfull = `${addressfull}, офис ${dd.office}`
        }
        if (dd.organization) {
            addressfull = `${addressfull}, организация ${dd.organization}`
        }
        return addressfull
    }

    const getFullPrice = () => {
        const positionsSum = orderDetails.ordSum
        return orderDetails.deliveryData.deliveryPrice
            ? positionsSum + orderDetails.deliveryData.deliveryPrice
            : positionsSum
    }
    return (
        <DS.Container isMobile={isMobile}>
            <script
                dangerouslySetInnerHTML={{
                    __html: `
                    var _rutarget = window._rutarget || [];
                    _rutarget.push({'event': 'thankYou', 'conv_id': 'transaction', 'order_id': ${orderId}});
                  `
                }}
            />
            {isMobile && <MobileHeader isDefaultRouterBack={false}>Заказ оформлен</MobileHeader>}
            <DS.Title isMobile={isMobile}>Заказ принят и оплачен</DS.Title>
            <DS.Body isMobile={isMobile}>
                <DS.DetailBlock>
                    <DS.DetailItem>
                        <DS.DetailName>Номер заказа:</DS.DetailName>
                        <DS.DetailInfoAlarm>{orderDetails.siteOrderNo}</DS.DetailInfoAlarm>
                    </DS.DetailItem>
                    <DS.DetailItem>
                        <DS.DetailName>Адрес:</DS.DetailName>
                        <DS.DetailInfo>{generateDeliveryInfo()}</DS.DetailInfo>
                    </DS.DetailItem>
                    <DS.DetailItem>
                        <DS.DetailName>Дата заказа:</DS.DetailName>
                        <DS.DetailInfo>{siteHelpers.convertDate(orderDetails.orderDate)}</DS.DetailInfo>
                    </DS.DetailItem>
                    <DS.DetailItem>
                        <DS.DetailName>Стоимость товаров:</DS.DetailName>
                        <DS.DetailInfo>
                            {orderDetails.ordSum.toLocaleString('RU-ru')} <SymbolRuble />
                        </DS.DetailInfo>
                    </DS.DetailItem>
                    {orderDetails.deliveryData.deliveryPrice ? (
                        <DS.DetailItem>
                            <DS.DetailName>Стоимость доставки:</DS.DetailName>
                            <DS.DetailInfo>
                                {orderDetails.deliveryData.deliveryPrice.toLocaleString('RU-ru')}{' '}
                                <SymbolRuble />
                            </DS.DetailInfo>
                        </DS.DetailItem>
                    ) : (
                        <DS.DetailItem>
                            <DS.DetailName>Стоимость доставки:</DS.DetailName>
                            <DS.DetailInfo>Бесплатно</DS.DetailInfo>
                        </DS.DetailItem>
                    )}
                    <DS.DetailItem>
                        <DS.DetailName>Общая стоимость:</DS.DetailName>
                        <DS.DetailInfo>
                            {getFullPrice().toLocaleString('RU-ru')} <SymbolRuble />
                        </DS.DetailInfo>
                    </DS.DetailItem>
                    {orderDetails.shipTimeSlotInfo.isNearestTime ? (
                        <DS.DetailItem>
                            <DS.DetailName>Интервал доставки:</DS.DetailName>
                            <DS.DetailInfo>В ближайшее время</DS.DetailInfo>
                        </DS.DetailItem>
                    ) : (
                        <DS.DetailItem>
                            <DS.DetailName>Интервал доставки:</DS.DetailName>
                            <DS.DetailInfo>{orderDetails.shipTimeSlotInfo.timeSlotText}</DS.DetailInfo>
                        </DS.DetailItem>
                    )}
                    <DS.DetailItem>
                        <DS.DetailName>Имя получателя:</DS.DetailName>
                        <DS.DetailInfo>{orderDetails.customer.name}</DS.DetailInfo>
                    </DS.DetailItem>
                    <DS.DetailItem>
                        <DS.DetailName>Телефон получателя:</DS.DetailName>
                        <DS.DetailInfo>
                            {orderDetails.customer.phone.replace(
                                /(\d{1})(\d{3})(\d{3})(\d{2})(\d{2})/,
                                '+$1 ($2) $3-$4-$5'
                            )}
                        </DS.DetailInfo>
                    </DS.DetailItem>

                    {orderDetails.customer.email && (
                        <DS.DetailItem>
                            <DS.DetailName>Email получателя:</DS.DetailName>
                            <DS.DetailInfo>{orderDetails.customer.email}</DS.DetailInfo>
                        </DS.DetailItem>
                    )}
                    {orderDetails.ordStateTitle && (
                        <DS.DetailItem>
                            <DS.DetailName>Статус заказа:</DS.DetailName>
                            <DS.DetailInfo>{orderDetails.ordStateTitle}</DS.DetailInfo>
                        </DS.DetailItem>
                    )}
                    {orderDetails.paymentTypeTitle && (
                        <DS.DetailItem>
                            <DS.DetailName>Способ оплаты:</DS.DetailName>
                            <DS.DetailInfo>{orderDetails.paymentTypeTitle}</DS.DetailInfo>
                        </DS.DetailItem>
                    )}
                </DS.DetailBlock>
                <Shared.Table>
                    <Shared.TableHeader>
                        <Shared.TableHeaderRow>
                            <Shared.TableHeaderColumn>Название</Shared.TableHeaderColumn>
                            <Shared.TableHeaderColumn>Кол-во</Shared.TableHeaderColumn>
                            <Shared.TableHeaderColumn>Цена</Shared.TableHeaderColumn>
                            <Shared.TableHeaderColumn>Сумма</Shared.TableHeaderColumn>
                        </Shared.TableHeaderRow>
                    </Shared.TableHeader>
                    <Shared.TableBody>
                        {checkItems() &&
                            orderDetails.items.map((item) => (
                                <Shared.TableRow key={item.goodsID}>
                                    <Shared.TableColumn>{`${item.drugTitle} ${item.outFormTitle}`}</Shared.TableColumn>
                                    <Shared.TableColumn>{item.drugCount}</Shared.TableColumn>
                                    <Shared.TableColumn>
                                        {item.price.toLocaleString('RU-ru')} <SymbolRuble />
                                    </Shared.TableColumn>
                                    <Shared.TableColumn>
                                        {(item.price * item.drugCount).toLocaleString('RU-ru')}{' '}
                                        <SymbolRuble />
                                    </Shared.TableColumn>
                                </Shared.TableRow>
                            ))}
                    </Shared.TableBody>
                </Shared.Table>
            </DS.Body>
        </DS.Container>
    )
}

Success.propTypes = {
    orderId: PropTypes.number.isRequired,
    isMobile: PropTypes.number.isRequired,
    // eslint-disable-next-line react/require-default-props
    orderDetails: PropTypes.shape({
        siteOrderNo: PropTypes.string.isRequired,
        items: PropTypes.array.isRequired,
        customer: PropTypes.shape({
            name: PropTypes.string,
            phone: PropTypes.string,
            email: PropTypes.string
        }),
        deliveryData: PropTypes.shape({
            addressText: PropTypes.string,
            apt: PropTypes.string,
            cashExchange: PropTypes.number,
            code: PropTypes.string,
            comment: PropTypes.string,
            deliveryPrice: PropTypes.number,
            entrance: PropTypes.string,
            floor: PropTypes.string,
            house: PropTypes.string,
            houseType: PropTypes.number,
            isNearestTime: PropTypes.bool,
            office: PropTypes.string,
            organization: PropTypes.string,
            paymentTypeId: PropTypes.number,
            timeSlotText: PropTypes.string
        }),
        ordSum: PropTypes.number,
        ordStateTitle: PropTypes.string,
        paymentTypeTitle: PropTypes.string
    })
}

Success.defaultProps = {
    orderDetails: {
        customer: {
            name: '',
            phone: '',
            email: ''
        },
        deliveryData: {
            addressText: '',
            apt: '',
            cashExchange: null,
            code: '',
            comment: '',
            deliveryPrice: 0,
            entrance: '',
            floor: '',
            house: '',
            houseType: 0,
            isNearestTime: false,
            office: '',
            organization: '',
            paymentTypeId: 0,
            timeSlotText: ''
        },
        ordSum: 0,
        ordStateTitle: '',
        paymentTypeTitle: ''
    }
}
export default Success
