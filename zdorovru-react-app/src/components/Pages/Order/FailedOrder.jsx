import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { getOrderFullInfo } from '../../../api/siteApis'
import { useDispatch } from 'react-redux'
import * as DS from './OrderDetail.styled'
import * as Shared from '../../../styles/base.styled'
import { clearBasket } from '../../../actions/customer'
import { generateDate } from '../../../helpers/site'

const FailedOrder = (props) => {
    useEffect(() => {
        const getOrder = async () => {
            const response = await getOrderFullInfo(props.orderId)
            setOrderDetails(response)
        }
        getOrder()
    }, [])

    const checkItems = () => {
        if (!orderDetails.items) return false
        if (orderDetails.items.length === 0) return false
        return true
    }
    const generateDeliveryInfo = () => {
        const dd = orderDetails.deliveryData
        let addressfull = dd.addressText
        if (dd.entrance) {
            addressfull = `${addressfull}, подъезд ${dd.entrance}`
        }
        if (dd.apt) {
            addressfull = `${addressfull}, кв ${dd.apt}`
        }
        if (dd.floor) {
            addressfull = `${addressfull}, этаж ${dd.floor}`
        }
        if (dd.code) {
            addressfull = `${addressfull}, домофон ${dd.code}`
        }
        if (dd.office) {
            addressfull = `${addressfull}, офис ${dd.office}`
        }
        if (dd.organization) {
            addressfull = `${addressfull}, организация ${dd.organization}`
        }
        return addressfull
    }

    const [orderDetails, setOrderDetails] = useState({
        siteOrderNo: '',
        customer: {
            name: '',
            phone: '',
            email: ''
        },
        deliveryData: {
            addressText: '',
            apt: '',
            cashExchange: null,
            code: '',
            comment: '',
            deliveryPrice: 0,
            entrance: '',
            floor: '',
            house: '',
            houseType: 0,
            isNearestTime: false,
            office: '',
            organization: '',
            paymentTypeId: 0,
            timeSlotText: ''
        },
        ordSum: 0,
        ordStateTitle: '',
        paymentTypeTitle: ''
    })

    return (
        <DS.Container>
            <DS.TitleError>Произошла ошибка при оплате</DS.TitleError>
            <DS.Body>
                <DS.DetailBlock>
                    <DS.DetailItem>
                        <DS.DetailName>Номер заказа:</DS.DetailName>
                        <DS.DetailInfoAlarm>{orderDetails.siteOrderNo}</DS.DetailInfoAlarm>
                    </DS.DetailItem>
                    <DS.DetailItem>
                        <DS.DetailName>Адресс:</DS.DetailName>
                        <DS.DetailInfo>{generateDeliveryInfo()}</DS.DetailInfo>
                    </DS.DetailItem>
                    <DS.DetailItem>
                        <DS.DetailName>Дата заказа:</DS.DetailName>
                        <DS.DetailInfo>{generateDate(orderDetails.orderDate)}</DS.DetailInfo>
                    </DS.DetailItem>
                    <DS.DetailItem>
                        <DS.DetailName>Сумма заказа:</DS.DetailName>
                        <DS.DetailInfo>{orderDetails.ordSum}</DS.DetailInfo>
                    </DS.DetailItem>
                    <DS.DetailItem>
                        <DS.DetailName>Имя получателя:</DS.DetailName>
                        <DS.DetailInfo>{orderDetails.customer.name}</DS.DetailInfo>
                    </DS.DetailItem>
                    <DS.DetailItem>
                        <DS.DetailName>Телефон получателя:</DS.DetailName>
                        <DS.DetailInfo>{orderDetails.customer.phone}</DS.DetailInfo>
                    </DS.DetailItem>
                    {orderDetails.customer.email && (
                        <DS.DetailItem>
                            <DS.DetailName>Email получателя:</DS.DetailName>
                            <DS.DetailInfo>{orderDetails.customer.email}</DS.DetailInfo>
                        </DS.DetailItem>
                    )}
                    {orderDetails.ordStateTitle && (
                        <DS.DetailItem>
                            <DS.DetailName>Статус заказа:</DS.DetailName>
                            <DS.DetailInfo>{orderDetails.ordStateTitle}</DS.DetailInfo>
                        </DS.DetailItem>
                    )}
                    {orderDetails.paymentTypeTitle && (
                        <DS.DetailItem>
                            <DS.DetailName>Способ оплаты:</DS.DetailName>
                            <DS.DetailInfo>{orderDetails.paymentTypeTitle}</DS.DetailInfo>
                        </DS.DetailItem>
                    )}
                </DS.DetailBlock>
                <Shared.Table>
                    <Shared.TableHeader>
                        <Shared.TableHeaderRow>
                            <Shared.TableHeaderColumn>Название</Shared.TableHeaderColumn>
                            <Shared.TableHeaderColumn>Дата</Shared.TableHeaderColumn>
                        </Shared.TableHeaderRow>
                    </Shared.TableHeader>
                    <Shared.TableBody>
                        {checkItems() &&
                            orderDetails.items.map((item) => (
                                <Shared.TableRow key={item.goodsID}>
                                    <Shared.TableColumn>{`${item.drugTitle} ${item.outFormTitle}`}</Shared.TableColumn>
                                    <Shared.TableColumn>{`${item.price}₽ x${item.drugCount}`}</Shared.TableColumn>
                                </Shared.TableRow>
                            ))}
                    </Shared.TableBody>
                </Shared.Table>
            </DS.Body>
        </DS.Container>
    )
}

FailedOrder.propTypes = {}

export default FailedOrder
