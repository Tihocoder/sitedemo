import styled, { css } from 'styled-components'

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    padding: ${(props) => (props.isMobile ? '0' : '0 3rem')};
    ${(props) =>
        props.isMobile &&
        css`
            overflow-x: auto;
            height: calc(100vh - 8.5rem);
            box-sizing: border-box;
        `}
`
export const Body = styled.div`
    display: flex;
    padding-top: 1rem;
    ${(props) =>
        props.isMobile
            ? css`
                  flex-direction: column;
                  justify-content: flex-start;
              `
            : css`
                  justify-content: space-between;
              `}
`
export const Title = styled.h2`
    font-size: 1.2rem;
    margin: 0;
    font-weight: bold;
    margin-top: 3.4rem;
`
export const TitleError = styled.h2`
    font-size: 1.2rem;
    color: ${(props) => props.theme.colors.getRedColor(0.6)};
    font-weight: bold;
`

export const ContentBlock = styled.div`
    margin-top: 1.8rem;
    &:first-child {
        margin-top: 0;
    }
`

export const DetailBlock = styled.div`
    display: flex;
    flex-direction: column;
    width: 26rem;
    & > div {
        margin-top: 1rem;
    }
    & > div:first-child {
        margin-top: 0;
    }
    padding: 1rem 0;
`

export const DetailItem = styled.div`
    display: flex;
    justify-content: space-between;
    & > *:last-child {
        width: 15rem;
    }
`

export const DetailName = styled.span`
    color: ${(props) => props.theme.colors.getGrayColor(0.6)};
`

export const DetailInfo = styled.span`
    color: ${(props) => props.theme.colors.getGrayColor(0.9)};
`

export const DetailInfoAlarm = styled.span`
    font-size: 1.2rem;
    color: ${(props) => props.theme.colors.getRedheadColor(0.8)};
`
