import styled, { css } from 'styled-components'

export const StockItem = styled.div`
    display: flex;
`
export const StockColorChar = styled.div`
    ${(props) => {
        switch (props.metroColor) {
            case 'shabalovka': {
                return 'color: #ff8103'
            }
            case 'darkblue': {
                return 'color: #283981'
            }
            case 'domoded': {
                return 'color: #029a55'
            }
            case 'vernads': {
                return 'color: #b1d332'
            }
            case 'maryno': {
                return ' color: #b1d332'
            }
            case 'varshav': {
                return 'color: #5091bb'
            }
            case 'sokol': {
                return 'color: #029a55'
            }
            case 'butovo': {
                return 'color: #85d4f3'
            }
            case 'orange': {
                return 'color: #ff8103'
            }
            case 'kolomenskaya': {
                return 'color: #029a55'
            }
            case 'saddlebrown': {
                return 'color: #945340'
            }
            case 'yellow': {
                return 'color: #ffd803'
            }
            case 'darkgreen': {
                return 'color: #029a55'
            }
            case 'red': {
                return 'color: #ef1e25'
            }
            case 'lightgreen': {
                return 'color: #b1d332'
            }
            case 'lightblue': {
                return 'color: #85d4f3'
            }
            case 'black': {
                return 'color: black'
            }
            case 'medblue': {
                return 'color: #5091bb'
            }
            case 'purple': {
                return 'color: #b61d8e'
            }
            case 'gray': {
                return 'color: #acadaf'
            }
            case 'aquablue': {
                return 'color: #019ee0'
            }
            default: {
                return 'color: black'
            }
        }
    }};
    font-weight: bold;
    margin-right: 0.4rem;
`
export const StockTitle = styled.a`
    white-space: nowrap;
    color: inherit;
    font: inherit;
    ${(props) =>
        props.isHover &&
        css`
            :hover {
                color: ${(props) => props.theme.colors.getGreenColor(1)};
            }
        `}
`
