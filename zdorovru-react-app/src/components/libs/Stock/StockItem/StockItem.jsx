import React from 'react'
import PropTypes from 'prop-types'
import * as S from './StockItem.styled'
import { getColorPrefix } from '../../../../helpers/site'

const StockItem = (props) => {
    return (
        <S.StockItem isHover={props.isHover}>
            <S.StockColorChar metroColor={props.metroColor}>{getColorPrefix(props.title)}</S.StockColorChar>
            <S.StockTitle isHover={props.isHover}>{props.name}</S.StockTitle>
        </S.StockItem>
    )
}

StockItem.propTypes = {
    isMetroLocation: PropTypes.bool.isRequired,
    metroColor: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    title: PropTypes.string,
    isHover: PropTypes.bool
}

export default StockItem
