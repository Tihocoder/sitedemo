import React from 'react'
import PropTypes from 'prop-types'
import * as S from '@mobilePages/account/AccountLoginPage/AccountLoginPage.styled'
import MobilePageHeader from '@mobileShared/MobilePageHeader/MobilePageHeader'
import { MobileBody } from '@styles/ui-element.styled'
import MobileHeader from "@mobileShared/MobileHeader/MobileHeader";

const MobileWriteCodePage = (props) => (
    // const inputRef = useRef(null)
    // useEffect(() => {
    //     setTimeout(() => {
    //         inputRef.current.focus()
    //     })
    // }, [inputRef.current])
    <S.Container>
        <MobileHeader isDefaultRouterBack={false} backClickHandler={props.authorize.toggleShowedSubmitSmsWindow}>
            Код подтверждения
        </MobileHeader>
        <MobileBody>
            <S.Body>
                <S.TitleDescription>Введите код из СМС</S.TitleDescription>
                <S.CodeInputsBlock>
                    <S.InputFullCode
                        ref={(inputElement) => {
                            if (inputElement) {
                                inputElement.focus()
                            }
                        }}
                        type="number"
                        inputmode="numeric"
                        pattern="[0-9]*"
                        value={props.authorize.code}
                        onChange={props.authorize.changeCodeSubmit}
                    />
                </S.CodeInputsBlock>
                {props.authorize.errorText && (
                    <S.CodeConfirmErrorBlock>{props.authorize.errorText}</S.CodeConfirmErrorBlock>
                )}
                {props.authorize.timer.isActive ? (
                    <S.TImeDescriptionBlock>
                        <S.TImeDescription>Повторная отправка</S.TImeDescription>
                        <S.TImeDescription>кода через {props.authorize.timer.seconds} сек</S.TImeDescription>
                    </S.TImeDescriptionBlock>
                ) : (
                    <S.ResetSms onClick={props.authorize.submitPhone}>Получить новый код</S.ResetSms>
                )}
                <S.CodeConfirmPhone>
                    {props.authorize.phoneNumber.replace(
                        /(\d{1})(\d{3})(\d{3})(\d{2})(\d{2})/,
                        '+$1 ($2) $3-$4-$5'
                    )}
                </S.CodeConfirmPhone>
                <S.CodeConfirmAgain onClick={props.authorize.selectInputPhonePage}>
                    Ввести другой номер
                </S.CodeConfirmAgain>
            </S.Body>
        </MobileBody>
    </S.Container>
)

MobileWriteCodePage.propTypes = {
    authorize: PropTypes.shape({
        isSendSms: PropTypes.bool.isRequired,
        phoneNumber: PropTypes.string.isRequired,
        setPhoneNumber: PropTypes.func.isRequired,
        errorText: PropTypes.string,
        isShowedSubmitSmsWindow: PropTypes.bool.isRequired,
        toggleShowedSubmitSmsWindow: PropTypes.func.isRequired,
        // eslint-disable-next-line react/forbid-prop-types
        timer: PropTypes.any.isRequired,
        code: PropTypes.string.isRequired,
        sendConde: PropTypes.func.isRequired,
        submitPhone: PropTypes.func.isRequired,
        changeCodeSubmit: PropTypes.func.isRequired,
        selectInputPhonePage: PropTypes.func.isRequired
    }).isRequired
}

export default MobileWriteCodePage
