import React, { createContext, useContext, useState } from 'react'
import { useRouter } from 'next/router'
import ModalWindow from '@shared/ModalWindow/ModalWindow'
import { AlertsStateContext } from '@context/AlertsState'
import { PageMobile } from '@styles/base.styled'
import { InitCustomer } from 'src/hoc/InitCustomer'
import { InitSiteData } from 'src/hoc/InitSiteData'
import AccountLoginPage from '@mobilePages/account/AccountLoginPage/AccountLoginPage'
import CatalogMenuState from '@context/CatalogMenuState'
import * as S from './MobileLayout.styled'
import FooterMenu from '../mobileComponents/FooterMenu/FooterMenu'

export const SearchContext = createContext({
    searchText: '',
    changeSearchText: () => {},
    keyDownHandler: () => {},
    changeSearchHandler: () => {},
    searchButtonHandler: () => {}
})

const MobileLayout = ({ children }) => {
    const { deleteFromBasket, confirmCustomer } = useContext(AlertsStateContext)

    const router = useRouter()
    const [searchText, setSearchText] = useState('')
    const changeSearchText = (term) => setSearchText(term)
    const changeSearchHandler = (e) => {
        if (e.target) setSearchText(e.target.value)
        else setSearchText(e)
    }
    const keyDownHandler = (e, enterHandler) => {
        if (e.key === 'Enter') {
            if (searchText.length > 2) {
                router.push({
                    pathname: '/catalog/search',
                    query: {
                        q: searchText
                    }
                })
                if (enterHandler) enterHandler()
            }
        }
    }
    const searchButtonHandler = () => {
        if (searchText.length > 2) {
            router.push({
                pathname: '/catalog/search',
                query: {
                    q: searchText
                }
            })
        }
    }
    return (
        <InitSiteData>
            <InitCustomer>
                <CatalogMenuState>
                    <SearchContext.Provider
                        value={{
                            searchText,
                            changeSearchText,
                            keyDownHandler,
                            changeSearchHandler,
                            searchButtonHandler
                        }}>
                        <PageMobile>
                            {deleteFromBasket.isShowed && (
                                <ModalWindow
                                    title="Удаление из корзины"
                                    isMobile
                                    submit={{
                                        text: 'Удалить',
                                        clickHandler: deleteFromBasket.confirmHandler
                                    }}
                                    cancel={{
                                        text: 'Отмена',
                                        clickHandler: deleteFromBasket.closeHandler
                                    }}>
                                    Вы действительно хотите удалить выбранный товар из корзины?
                                </ModalWindow>
                            )}
                            {confirmCustomer.isShowed && router.asPath.startsWith('/account') ? (
                                <AccountLoginPage closeHandler={confirmCustomer.toggleClose} />
                            ) : (
                                <S.Container>{children}</S.Container>
                            )}
                            <FooterMenu />
                        </PageMobile>
                    </SearchContext.Provider>
                </CatalogMenuState>
            </InitCustomer>
        </InitSiteData>
    )
}

MobileLayout.propTypes = {}

export default MobileLayout
