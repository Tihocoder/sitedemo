import React, { useEffect, useState } from 'react'
import { parseCookies, setCookie } from 'nookies'
import { InitSiteData } from 'src/hoc/InitSiteData'
import { InitCustomer } from 'src/hoc/InitCustomer'
import * as SharedStyled from '../../styles/base.styled'
import NavBar from '../Navbar/Navbar'
import Header from '../Header/Header'
import CatalogMenuState from '../../context/CatalogMenuState'
import AlertsState from '../../context/AlertsState'
import Footer from '../Footer/Footer'
// eslint-disable-next-line import/no-unresolved
// import '../../App.css'

const Base = (props) => {
    const [isAcceptCookie, setIsAcceptCookie] = useState(true)

    useEffect(() => {
        const cookies = parseCookies()
        if (!cookies['zdorov-accept-cookie']) {
            setIsAcceptCookie(false)
        }
    }, [])

    const acceptCookie = () => {
        setCookie(null, 'zdorov-accept-cookie', JSON.stringify(true), {
            maxAge: 365 * 24 * 60 * 60,
            path: '/'
        })
        setIsAcceptCookie(true)
    }
    return (
        <InitSiteData>
            <InitCustomer>
                <CatalogMenuState>
                    <AlertsState>
                        <SharedStyled.CenterPage>
                            <SharedStyled.PageContainer>
                                <SharedStyled.Page>
                                    <NavBar />
                                    <Header />
                                    <SharedStyled.Body>{props.children}</SharedStyled.Body>
                                </SharedStyled.Page>
                                <Footer />
                            </SharedStyled.PageContainer>
                        </SharedStyled.CenterPage>

                        {/* {!isAcceptCookie && (
                            <CookieBottomBlock closeHandler={acceptCookie}>
                                Продолжая использовать наш сайт, вы даете согласие на обработку файлов cookie,
                                которые обеспечивают правильную работу сайта.
                            </CookieBottomBlock>
                        )} */}
                    </AlertsState>
                </CatalogMenuState>
            </InitCustomer>
        </InitSiteData>
    )
}

export default Base
