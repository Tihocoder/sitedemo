import styled, { css } from 'styled-components'

export const Container = styled.div`
    display: flex;
    justify-content: space-between;
    width: 100%;
    align-items: flex-start;

    font-size: 0.9rem;
    color: rgba(50, 62, 52, 1);
    background-color: white;
    box-sizing: border-box;
    padding: 0.9rem;
    box-shadow: inset -1px 0 0 0 rgba(7, 20, 8, 0.08), inset 0 -1px 0 0 rgba(7, 20, 8, 0.1),
        -1px -1px 0 0 rgba(7, 20, 8, 0.035), -1px 0 0 0 rgba(7, 20, 8, 0.045), 0 -1px 0 0 rgba(7, 20, 8, 0.03);
    &:first-child {
        border-top-left-radius: 0.3rem;
        border-top-right-radius: 0.3rem;
    }
    &:last-child {
        border-bottom-left-radius: 0.3rem;
        border-bottom-right-radius: 0.3rem;
    }

    ${(props) =>
        props.isWarningBorder &&
        css`
            border: 1px solid ${props.theme.colors.accentRed};
        `}
`

export const ImageContainer = styled.a`
    height: 100%;
    box-sizing: border-box;
    display: flex;
    align-items: flex-end;
    cursor: pointer;
    min-width: 144px;
`

export const InfoContainer = styled.div`
    display: flex;
    height: 100%;
    width: 100%;
    padding: 0 0.8rem;
    justify-content: flex-start;

    & button {
        max-height: 4rem;
        max-width: 9rem;
    }
`
export const FullInfo = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    height: 100%;
    margin-left: 1rem;
`
export const FullTitle = styled.a`
    color: ${(props) => props.theme.colors.getGrayColor(1)};
    font-weight: bold;
    font-size: 1rem;
    margin-bottom: 1rem;
    text-decoration: none;
    cursor: pointer;
`

export const TextInfoContainer = styled.div``

export const ButtonBasketContainer = styled.div`
    width: 11rem;
`

export const TextInfo = styled.div`
    font-size: 0.875rem;
    display: flex;
    flex-wrap: wrap;
    margin-bottom: 0.4rem;
`

export const TextInfoName = styled.div`
    color: ${(props) => props.theme.colors.getGrayColor(0.7)};
`

export const TextInfoDescription = styled.div`
    margin-left: 0.2rem;
    color: ${(props) => props.theme.colors.getGrayColor(1)};
`

export const PriceContainer = styled.div`
    display: flex;
    width: 14rem;
    flex-direction: column;
    height: 100%;
    align-items: flex-end;
`
export const DeleteContainer = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    height: 5.5rem;
    width: 9.2rem;
`

export const LeftBlockContainer = styled.div`
    display: flex;
    flex-direction: column;
`
export const CloseContainer = styled.div`
    display: flex;
    justify-content: space-between;
    width: 9rem;
`

export const LikedContainer = styled.div`
    display: flex;
    align-items: flex-end;
`
export const LikedIconBlock = styled.div`
    cursor: pointer;
    padding: 0.2rem;
    padding-right: 0.6rem;
    width: 1.8rem;
`

export const NotFoundText = styled.div`
    color: ${(props) => props.theme.colors.getGrayColor(0.6)};
`

export const PriceDataContainer = styled.div`
    display: flex;
    width: 14rem;
    justify-content: flex-end;
    align-items: flex-end;
    height: 100%;
`

export const Price = styled.div`
    flex-direction: column;
    display: flex;
    align-items: flex-end;
`
export const MarkerBlock = styled.div`
    display: flex;
    flex-wrap: wrap;
    & > div {
        margin: 0.2rem;
    }
`

export const PriceDescription = styled.span`
    color: ${(props) => props.theme.colors.getGrayColor(0.6)};
    font-size: 0.8rem;
`
export const PriceData = styled.div`
    color: ${(props) => props.theme.colors.accentGreen};
    font-size: 1.3rem;
    display: flex;
    flex-wrap: nowrap;
`

export const GoodImage = styled.img`
    height: 100%;
    max-height: 9rem;
    max-width: 9rem;
`

export const ReasonContainer = styled.div`
    display: flex;
    flex-direction: column;
    max-width: 32rem;
`
export const BodyModel = styled.div`
    max-width: 32rem;
    & hr {
        border: none;
        color: ${(props) => props.theme.colors.getGrayColor(0.2)};
        height: 1px;
        background-color: ${(props) => props.theme.colors.getGrayColor(0.2)};
    }
`

export const ReasonTextItem = styled.div``

export const StocksContainer = styled.div`
    display: flex;
    max-width: 32rem;
    flex-direction: column;
`

export const ModalIsNotShipmentContainer = styled.div`
    display: flex;
    flex-direction: column;
    max-width: 32rem;
`
export const ModalIsNotShipmentFooter = styled.div`
    width: 100%;
    display: flex;
    justify-content: flex-end;
`
export const ModalIsNotShipmentFooterButtons = styled.div`
    display: flex;
    flex-direction: column;
    max-width: 17rem;
    margin: -3px 0;
    & button {
        width: 100%;
        flex: 1 1 auto;
        margin: 3px 0;
    }
`

export const ModalIsNotShipmentP = styled.p``

export const ModalIsNotShipmentUl = styled.ul``

export const ModalIsNotShipmentLi = styled.li``
