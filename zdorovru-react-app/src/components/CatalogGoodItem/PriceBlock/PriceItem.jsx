import React from 'react'
import PropTypes from 'prop-types'
import * as S from './PriceBlock.styled'
import { _PricePropTypes } from '../../../consts'
import SymbolRuble from '../../shared/SymbolRuble/SymbolRuble'
import { declOfNum } from '../../../helpers/site'

const PriceItem = ({
    title,
    price,
    isFrom,
    availableStocksCount,
    isSelectedStock,
    clickOpenAnotherStockHandler
}) => {
    const getPriceActual = (price) => {
        if (price.withoutPromo) {
            return (
                <React.Fragment>
                    <S.PriceDataActual>
                        <S.PriceDataOld>
                            {price.withoutPromo} <SymbolRuble />
                        </S.PriceDataOld>
                        <S.PriceNumberSaleBold>
                            {price.normal.toLocaleString('ru-RU')} <SymbolRuble />
                        </S.PriceNumberSaleBold>
                    </S.PriceDataActual>
                </React.Fragment>
            )
        }
        return (
            <React.Fragment>
                <S.PriceDataActual>
                    <S.PriceNumberActualBold>
                        {price.normal.toLocaleString('ru-RU')} <SymbolRuble />
                    </S.PriceNumberActualBold>
                </S.PriceDataActual>
            </React.Fragment>
        )
    }
    return (
        <S.Price>
            {isFrom && isSelectedStock && availableStocksCount && availableStocksCount > 0 ? (
                <S.AnotherStocksWarning onClick={clickOpenAnotherStockHandler}>
                    Нет в выбранной, самовывоз из {availableStocksCount}{' '}
                    {declOfNum(availableStocksCount, ['аптеки', 'аптек', 'аптек'])}
                </S.AnotherStocksWarning>
            ) : (
                <React.Fragment />
            )}
            {isFrom && isSelectedStock && availableStocksCount === 0 ? (
                <S.AnotherStocksWarning>Нет в выбранной</S.AnotherStocksWarning>
            ) : (
                <React.Fragment />
            )}

            <S.PriceDescription>{title}</S.PriceDescription>
            {isFrom ? (
                <S.PriceDataBodyChangeStock onClick={clickOpenAnotherStockHandler}>
                    <S.PricePrefixText>от</S.PricePrefixText>
                    {getPriceActual(price)}
                </S.PriceDataBodyChangeStock>
            ) : (
                <S.PriceDataBody>{getPriceActual(price)}</S.PriceDataBody>
            )}
        </S.Price>
    )
}
PriceItem.propTypes = {
    title: PropTypes.string,
    price: _PricePropTypes,
    availableStocksCount: PropTypes.number,
    isSelectedStock: PropTypes.bool.isRequired,
    clickOpenAnotherStockHandler: PropTypes.func
}

PriceItem.defaultProps = {
    isFrom: false,
    isSelectedStock: true
}

export default PriceItem
