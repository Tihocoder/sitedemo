import React from 'react'
import PropTypes from 'prop-types'
import * as S from './PriceBlock.styled'
import { _PricePropTypes } from '../../../consts'
import Button from '../../shared/Button/Button'
import CountChanger from '../../shared/CountChanger/CountChanger'
import SymbolRuble from '../../shared/SymbolRuble/SymbolRuble'
import {
    ColorConsts,
    deleteButtonColors,
    ElementSizeType,
    errorButtonColors,
    errorButtonLightColors,
    grayButtonLight,
    warningButtonColors,
    warningButtonLightColors
} from '../../../styles/theme-const'
import PriceItem from './PriceItem'
import { useState } from 'react'
import ModalWindow from '../../shared/ModalWindow/ModalWindow'
import LikeIcon from '../../Icons/LikeIkon'
import { useDispatch, useSelector } from 'react-redux'
import { toggleLikedGood } from '../../../actions/customer'

export const PriceType = {
    Delivery: 1,
    Stock: 2
}
export const buttonDeliveryNotAvailableColors = {
    backgroundColor: ColorConsts.white,
    color: 'rgba(0, 0, 0, 0.87)',
    hoverBackgroundColor: ColorConsts.redLightBackground,
    shadowColor: ColorConsts.redLight,
    shadowHoverColor: ColorConsts.redLightBackground,
    borderColor: ColorConsts.redLight
}

const hocRender = (isLiked, likeHandler, content) => (
    <S.Container>
        <S.BlockLike>
            <S.LikedContainer>
                <S.LikedIconBlock isLiked={isLiked} onClick={likeHandler}>
                    <LikeIcon />
                </S.LikedIconBlock>
            </S.LikedContainer>
        </S.BlockLike>
        <S.BlockPrice>{content}</S.BlockPrice>
    </S.Container>
)

const PriceBlock = (props) => {
    const likedGoods = useSelector((state) => state.customer.likedGoods)
    const dispatch = useDispatch()
    const isStockAvl = props.isAvailableOnStock && props.stockPrice.normal
    const isDeliveryAvl = props.deliveryStatus.isAvailableForDelivery && props.deliveryPrice.normal

    const toggleLikeHandler = async (goodId) => {
        await dispatch(toggleLikedGood(goodId))
    }

    const isLiked = likedGoods.includes(props.goodId)

    if (!props.isAvailable) {
        return hocRender(
            isLiked,
            () => toggleLikeHandler(props.goodId),
            props.options.isBasket ? (
                <S.ButtonBasketContainer>
                    <S.DeleteContainer>
                        <Button elementSizeTypeValue={ElementSizeType.regular} colors={grayButtonLight}>
                            Нет в наличии
                        </Button>
                        <Button
                            elementSizeTypeValue={ElementSizeType.regular}
                            colors={errorButtonColors}
                            clickHandler={props.deleteItemHandler}>
                            Удалить
                        </Button>
                        <Button
                            elementSizeTypeValue={ElementSizeType.regular}
                            colors={warningButtonColors}
                            clickHandler={props.hiddenItemHandler}>
                            Отложить
                        </Button>
                    </S.DeleteContainer>
                </S.ButtonBasketContainer>
            ) : (
                <S.Price>
                    <S.ButtonBasketContainer>
                        <S.DeleteContainer>
                            <Button elementSizeTypeValue={ElementSizeType.regular} colors={grayButtonLight}>
                                Нет в наличии
                            </Button>
                        </S.DeleteContainer>
                    </S.ButtonBasketContainer>
                </S.Price>
            )
        )
    }

    const clickHandlerDeliveryReasons = () => {
        props.openDeliveryReasons(
            props.deliveryStatus.reasons.map((reason) => ({
                id: reason.deliveryReasonID,
                text: reason.warningText
            }))
        )
    }

    const generatePrice = () => {
        let prices = []
        prices.push(
            <React.Fragment>
                <PriceItem
                    isSelectedStock={props.isSelectedStock}
                    availableStocksCount={props.availableStocksCount}
                    clickOpenAnotherStockHandler={props.clickOpenAnotherStockHandler}
                    isFrom={!isStockAvl}
                    price={isStockAvl ? props.stockPrice : props.minPrice}
                    title={'Цена самовывоза'}
                />
            </React.Fragment>
        )
        if (isDeliveryAvl) {
            prices.push(<PriceItem price={props.deliveryPrice} title={'Цена доставки'} />)
        } else {
            prices.push(
                <S.Price>
                    <S.ButtonBasketContainer>
                        <Button
                            colors={errorButtonLightColors}
                            clickHandler={clickHandlerDeliveryReasons}
                            textTransform={'none'}
                            elementSizeTypeValue={ElementSizeType.regular}>
                            Доставка невозможна
                        </Button>
                    </S.ButtonBasketContainer>
                </S.Price>
            )
        }
        return prices.map((Component, index) => React.cloneElement(Component, { key: index }))
    }

    const generatePriceBasket = () => {
        if (!props.options.isDelivery) {
            if (!isStockAvl) {
                return (
                    <React.Fragment>
                        <PriceItem
                            price={props.minPrice}
                            isSelectedStock={props.isSelectedStock}
                            availableStocksCount={props.availableStocksCount}
                            clickOpenAnotherStockHandler={props.clickOpenAnotherStockHandler}
                            title={'Цена самовывоза'}
                            isFrom={true}
                        />
                    </React.Fragment>
                )
            }
            return <PriceItem price={props.stockPrice} title={'Цена самовывоза'} />
        }
        if (isDeliveryAvl) {
            return <PriceItem title={'Цена доставки'} price={props.deliveryPrice} />
        }
        return (
            <S.Price>
                <S.ButtonBasketContainer>
                    <Button
                        colors={buttonDeliveryNotAvailableColors}
                        clickHandler={clickHandlerDeliveryReasons}
                        textTransform={'none'}
                        elementSizeTypeValue={ElementSizeType.regular}>
                        Доставка невозможна (причина)
                    </Button>
                </S.ButtonBasketContainer>
            </S.Price>
        )
    }

    if (!props.options.isBasket) {
        return hocRender(
            isLiked,
            () => toggleLikeHandler(props.goodId),
            <S.BlockPrice>
                <S.PriceDataContainer>{generatePrice()}</S.PriceDataContainer>
                <S.ButtonBasketContainer>
                    {props.count ? (
                        <CountChanger value={props.count} changeValueHandler={props.changePriceHandler} />
                    ) : (
                        <Button
                            elementSizeTypeValue={ElementSizeType.regular}
                            clickHandler={props.addBasketHandler}>
                            В корзину
                        </Button>
                    )}
                </S.ButtonBasketContainer>
            </S.BlockPrice>
        )
    }
    const isNotAvailable =
        ((!props.options.isDelivery && !isStockAvl) || (props.options.isDelivery && !isDeliveryAvl)) &&
        props.isSelectedShipment
            ? true
            : false

    const isChangedCount = props.bookedCount < props.count && props.isChanged

    if (props.options.isDelivery) {
        return hocRender(
            isLiked,
            () => toggleLikeHandler(props.goodId),
            <S.BlockPrice>
                <S.PriceDataContainer>{generatePriceBasket()}</S.PriceDataContainer>
                {isChangedCount && (
                    <S.PreorderWarningContainer>
                        <S.WarningCountSpan>В наличии {props.bookedCount} уп.</S.WarningCountSpan>
                    </S.PreorderWarningContainer>
                )}
                <S.ButtonBasketContainer>
                    {!isDeliveryAvl && props.isSelectedShipment ? (
                        <S.DeleteContainerWithoutNotAvailable>
                            <Button
                                elementSizeTypeValue={ElementSizeType.regular}
                                colors={errorButtonColors}
                                clickHandler={props.deleteItemHandler}>
                                Удалить
                            </Button>
                            <Button
                                elementSizeTypeValue={ElementSizeType.regular}
                                colors={warningButtonColors}
                                clickHandler={props.hiddenItemHandler}>
                                Отложить
                            </Button>
                        </S.DeleteContainerWithoutNotAvailable>
                    ) : (
                        <React.Fragment />
                    )}
                    {!isNotAvailable && props.count && (
                        <CountChanger value={props.count} changeValueHandler={props.changePriceHandler} />
                    )}
                </S.ButtonBasketContainer>
            </S.BlockPrice>
        )
    } else {
        return hocRender(
            isLiked,
            () => toggleLikeHandler(props.goodId),
            <S.BlockPrice>
                <S.PriceDataContainer>{generatePriceBasket()}</S.PriceDataContainer>
                {isChangedCount && (
                    <S.PreorderWarningContainer>
                        <S.WarningCountSpan>В наличии {props.bookedCount} уп.</S.WarningCountSpan>
                    </S.PreorderWarningContainer>
                )}
                <S.ButtonBasketContainer>
                    {!isStockAvl && props.isSelectedShipment ? (
                        <S.DeleteContainerWithoutNotAvailable>
                            <Button
                                elementSizeTypeValue={ElementSizeType.regular}
                                colors={errorButtonColors}
                                clickHandler={props.deleteItemHandler}>
                                Удалить
                            </Button>
                            <Button
                                elementSizeTypeValue={ElementSizeType.regular}
                                colors={warningButtonColors}
                                clickHandler={props.hiddenItemHandler}>
                                Отложить
                            </Button>
                        </S.DeleteContainerWithoutNotAvailable>
                    ) : (
                        <React.Fragment />
                    )}
                    {!isNotAvailable && props.count && (
                        <CountChanger value={props.count} changeValueHandler={props.changePriceHandler} />
                    )}
                </S.ButtonBasketContainer>
            </S.BlockPrice>
        )
    }
}

PriceBlock.propTypes = {
    stockPrice: _PricePropTypes,
    deliveryPrice: _PricePropTypes,
    minPrice: _PricePropTypes,
    deliveryStatus: PropTypes.shape({
        isAvailableForDelivery: PropTypes.bool.isRequired,
        reasons: PropTypes.arrayOf(
            PropTypes.shape({
                deliveryReasonID: PropTypes.number.isRequired,
                warningText: PropTypes.string
            })
        )
    }),
    count: PropTypes.number,
    bookedCount: PropTypes.number,
    isChanged: PropTypes.bool,
    isAvailable: PropTypes.bool.isRequired,
    isAvailableOnStock: PropTypes.bool.isRequired,
    addBasketHandler: PropTypes.func.isRequired,
    changePriceHandler: PropTypes.func.isRequired,
    showedAnotherStocks: PropTypes.func.isRequired,
    openDeliveryReasons: PropTypes.func.isRequired,
    closeDeliveryReasons: PropTypes.func.isRequired,
    hiddenItemHandler: PropTypes.func,
    isSelectedStock: PropTypes.bool.isRequired,
    goodName: PropTypes.string,
    options: PropTypes.shape({
        isBasket: PropTypes.bool,
        isDelivery: PropTypes.bool
    })
}

PriceBlock.defaultProps = {
    options: {
        isBasket: false
    }
}

export default PriceBlock
