import React, { useContext, useState } from 'react'
import PropTypes from 'prop-types'
import { useRouter } from 'next/router'
import { useDispatch, useSelector } from 'react-redux'
import Link from 'next/link'
import { FeaturesEnum, _PricePropTypes, _WebDataPropTypes } from 'src/consts'
import { AlertsStateContext } from '@context/AlertsState'
import { generateDate, generateTime, generateUrlImage } from '@helpers/site'
import {
    addBasketItem,
    changeBasketItemPrice,
    changeShipment,
    toggleHiddenBasketItem
} from '@actions/customer'
import { getGoodStocks } from '@api/siteApis'
import { MenuContext } from '@context/CatalogMenuState'
import * as S from './CatalogGoodItem.styled'
import * as Theme from '../../styles/theme-const'
import PriceBlock from './PriceBlock/PriceBlock'
import ModalWindow from '../shared/ModalWindow/ModalWindow'
import StockItem from '../libs/Stock/StockItem/StockItem'
import Button from '../shared/Button/Button'
import ButtonTransparent from '../shared/ButtonTransparent/ButtonTransparent'
import MarkerContainer from '../MarkerContainer/MarkerContainer'
import ModalWindowCustom from '../shared/ModalWindowCustom/ModalWindowCustom'
import ModalAddressSelect from '../Basket/BasketShipment/ModalAddressSelect/ModalAddressSelect'
import SelectorStock from '../SelectorStock/SelectorStock'
import FixPriceBlock from './FixPriceBlock/FixPriceBlock'

Theme.ColorConsts.getRedheadColor(0.4)
const buttonHiddenolors = {
    backgroundColor: Theme.ColorConsts.white,
    color: 'rgba(0, 0, 0, 0.87)',
    shadowColor: Theme.ColorConsts.redLight,
    borderColor: Theme.ColorConsts.redLight
}

const checkAvailable = (isDelivery, isAvailable, isAvailableOnStock, isAvailableForDelivery, priceNormal) => {
    if (!isDelivery && isAvailable && isAvailableOnStock && priceNormal) {
        return true
    }
    if (isDelivery && isAvailable && isAvailableForDelivery && priceNormal) {
        return true
    }
    false
}

const getGoodsUrl = (goodGroups, groupId, url, webId) => {
    try {
        if (groupId) {
            const group = goodGroups.find((x) => x.clsGroupID === groupId)
            if (group) {
                return `/catalog/${group.groupUrl}/${url}-${webId}`
            }
        }

        const groupDefault = goodGroups.find((x) => x.isDefault)
        return `/catalog/${groupDefault.groupUrl}/${url}-${webId}`
    } catch {
        console.error('webId not found groups: ', webId)
        return ''
    }
}

const colorsButtonText = {
    color: Theme.ColorConsts.getGrayColor(0.6),
    hoverColor: Theme.ColorConsts.getGrayColor(0.4)
}
const optionsButtonText = {
    isBold: false
}

const CatalogGoodItem = (props) => {
    const router = useRouter()
    const dispatch = useDispatch()
    const baseImageUrl = useSelector((state) => state.catalog.baseImageUrl)
    const features = useSelector((state) => state.catalog.features)
    const basketItemsAll = useSelector((state) => state.customer.basketItems)
    const basketItems = basketItemsAll.filter((item) => !item.isHidden)
    const stocksAll = useSelector((state) => state.company.stocks)
    const cities = useSelector((state) => state.company.cities)
    const { deleteFromBasket } = useContext(AlertsStateContext)

    const getFeatureHtml = () => {
        if (props.deliveryPrice.isDemand && props.stockPrice.isDemand) {
            return features[FeaturesEnum.DescriptionDemandStockAndDeliveryBasketAdd]
                .replace('[dateStock]', generateDate(props.stockPrice.aptEtaDateTime, false))
                .replace('[dateDelivery]', generateDate(props.deliveryPrice.aptEtaDateTime, false))
                .replace('[time]', generateTime(props.deliveryPrice.aptEtaDateTime))
        }
        if (props.deliveryPrice.isDemand) {
            return features[FeaturesEnum.DemandDeliveryBasketAdd]
                .replace('[date]', generateDate(props.deliveryPrice.aptEtaDateTime, false))
                .replace('[time]', generateTime(props.deliveryPrice.aptEtaDateTime))
        }
        if (props.stockPrice.isDemand) {
            return features[FeaturesEnum.DemandPVZBasketAdd].replace(
                '[date]',
                generateDate(props.stockPrice.aptEtaDateTime, true)
            )
        }
    }

    const [deliveryReasonState, setDeliveryReasonState] = useState({
        isShowedModal: false,
        reasons: []
    })
    const [anotherStocks, setAnotherStocks] = useState({
        isShowedModal: false,
        stocks: []
    })
    const { toggleStockSelectorByGood } = useContext(MenuContext)

    const [isShowedModalDemand, setIsShowedModalDemand] = useState(false)

    const [isShowedModalNotSelectedShipment, setIsShowedModalNotSelectedShipment] = useState(false)
    const [isShowedSelectorAddress, setIsShowedSelectorAddress] = useState(false)
    const [isShowedSelectorStock, setIsShowedSelectorStock] = useState(false)

    const selectedStocAndCitykHandler = async (id, cityId) => {
        await dispatch(changeShipment({ stockId: id, cityId }))
        setIsShowedSelectorStock(!isShowedSelectorStock)
        router.reload()
    }

    const changeSelectedAddressHandler = async (id) => {
        await dispatch(changeShipment({ shipAddressId: id }))
        setIsShowedSelectorAddress(false)
        router.reload()
    }

    const modalIsNotShipmentHandler = () => {
        setIsShowedModalNotSelectedShipment(false)
        setIsShowedSelectorStock(true)
    }
    const modalIsNotShipmentAddressHandler = () => {
        setIsShowedModalNotSelectedShipment(false)
        setIsShowedSelectorAddress(true)
    }

    const openDeliveryReasons = (reasons) => {
        setDeliveryReasonState({
            ...deliveryReasonState,
            isShowedModal: true,
            reasons
        })
    }
    const closeDeliveryReasons = () => {
        setDeliveryReasonState({
            ...deliveryReasonState,
            isShowedModal: false,
            reasons: []
        })
    }

    const closeDemandModal = () => {
        setIsShowedModalDemand(false)
    }
    const getSelectedStock = () => stocksAll.find((stock) => stock.id === props.shipment.stockId)
    const showedAnotherStocks = async () => {
        const stocks = await getGoodStocks(props.webData.goodId, props.shipment.cityId)

        setAnotherStocks({
            isShowedModal: true,
            stocks
        })
    }
    const closeAnotherStocks = () => {
        setAnotherStocks({
            isShowedModal: false,
            stocks: []
        })
    }

    const changePriceHandler = async (count) => {
        if (count > 0) {
            await dispatch(changeBasketItemPrice(props.webData.webId, count))
        } else {
            deleteFromBasket.openHandler(props.webData.webId)
        }
    }

    const deleteItemHandler = async () => deleteFromBasket.openHandler(props.webData.webId)

    const hiddenItemHandler = async () => {
        await dispatch(toggleHiddenBasketItem(props.webData.goodId))
    }

    const addBasket = async () => {
        /// /RuTarget
        const _rutarget = window._rutarget || []
        _rutarget.push({ event: 'thankYou', conv_id: 'cart' })

        await dispatch(
            addBasketItem({
                webData: props.webData,
                stockPrice: props.stockPrice,
                deliveryPrice: props.deliveryPrice,
                minPrice: props.minPrice,
                isAvailable: props.isAvailable,
                isAvailableOnStock: props.isAvailableOnStock,
                deliveryStatus: props.deliveryStatus,
                isStrictlyByPrescription: props.isStrictlyByPrescription,
                analoguesQty: props.analoguesQty,
                isOriginalGood: props.isOriginalGood,
                isCold: props.isCold,
                isDemand: props.isDemand,
                aptEtaDateTime: props.aptEtaDateTime,
                availableStocksCount: props.availableStocksCount
            })
        )
        setIsShowedModalDemand(false)
    }

    const addBasketHandler = async () => {
        if (basketItemsAll.length === 0 && !props.shipment.shipAddressId && !props.shipment.stockId) {
            setIsShowedModalNotSelectedShipment(true)
            await addBasket()
            return
        }
        if (props.isDemand) {
            setIsShowedModalDemand(true)
            return
        }
        await addBasket()
    }

    const modalIsNotShipmentAddBasket = async () => {
        setIsShowedModalNotSelectedShipment(false)
        // await addBasket()
    }

    const getUslImage = () => {
        if (props.webData.hasImage) return `https://zdorov.ru/goodsimg/240/${props.webData.goodId}.jpg`
        return `${process.env.BASE_PATH || ''}/images/noimg.gif`
    }

    if (props.options.isBasket && props.isHidden) {
        return (
            <S.Container>
                <S.InfoContainer>
                    <Button
                        colors={buttonHiddenolors}
                        clickHandler={() => {}}
                        textTransform="none"
                        elementSizeTypeValue={Theme.ElementSizeType.regular}>
                        Товар отложен
                    </Button>
                    <S.FullInfo>
                        <Link href={props.webData.url} passHref>
                            <S.FullTitle>{`${props.webData.drugTitle} ${props.webData.outFormTitle}`}</S.FullTitle>
                        </Link>
                        <S.TextInfoContainer>
                            <S.TextInfo>
                                <S.TextInfoDescription>{props.webData.makerTitle}</S.TextInfoDescription>
                            </S.TextInfo>
                        </S.TextInfoContainer>
                    </S.FullInfo>
                </S.InfoContainer>
                <S.PriceContainer>
                    <S.DeleteContainer>
                        <Button
                            elementSizeTypeValue={Theme.ElementSizeType.regular}
                            colors={Theme.successButtonLight}
                            clickHandler={hiddenItemHandler}>
                            Вернуть
                        </Button>
                        <Button
                            elementSizeTypeValue={Theme.ElementSizeType.regular}
                            colors={Theme.errorButtonLightColors}
                            clickHandler={deleteItemHandler}>
                            Удалить
                        </Button>
                    </S.DeleteContainer>
                </S.PriceContainer>
            </S.Container>
        )
    }

    const mnn = props.webData.mnnRusTitle

    const isMNN = mnn && mnn.toLowerCase().trim() !== 'не установлено' && mnn.toLowerCase().trim() !== '-'

    const clickOpenAnotherStockHandler = () => {
        toggleStockSelectorByGood([props.webData.goodId])
    }

    const goodUrl = getGoodsUrl(
        props.webData.goodGroups,
        props.groupId,
        props.webData.url,
        props.webData.webId,
        props
    )

    const isSHowDeletes =
        (!props.options.isDelivery && props.isAvailableOnStock && props.stockPrice.normal) ||
        (props.options.isDelivery &&
            props.deliveryStatus.isAvailableForDelivery &&
            props.deliveryPrice.normal)

    const available = checkAvailable(
        props.isDelivery,
        props.isAvailable,
        props.isAvailableOnStock,
        props.deliveryStatus?.isAvailableForDelivery,
        props.options.isDelivery ? props.deliveryPrice?.normal : props.stockPrice?.normal
    )

    const getModalIsNotShipmentFooter = () => (
        <S.ModalIsNotShipmentFooter>
            <S.ModalIsNotShipmentFooterButtons>
                <Button
                    elementSizeTypeValue={Theme.ElementSizeType.regular}
                    clickHandler={modalIsNotShipmentHandler}>
                    Выбрать аптеку самовывоза
                </Button>
                <Button
                    elementSizeTypeValue={Theme.ElementSizeType.regular}
                    clickHandler={modalIsNotShipmentAddressHandler}>
                    Выбрать адрес доставки
                </Button>
                <Button
                    elementSizeTypeValue={Theme.ElementSizeType.regular}
                    clickHandler={modalIsNotShipmentAddBasket}>
                    Сделать выбор позже
                </Button>
            </S.ModalIsNotShipmentFooterButtons>
        </S.ModalIsNotShipmentFooter>
    )

    return (
        <S.Container isWarningBorder={props.isShowedWarninBorder && !available}>
            <Link href={goodUrl} passHref>
                <S.ImageContainer>
                    <S.GoodImage
                        src={generateUrlImage(baseImageUrl, props.webData.goodId, props.webData.hasImage)}
                        onError={(e) => {
                            e.target.onerror = null
                            e.target.src = `${process.env.BASE_PATH || ''}/images/noimg.gif`
                        }}
                    />
                </S.ImageContainer>
            </Link>
            <S.InfoContainer>
                <S.FullInfo>
                    <Link href={goodUrl} passHref>
                        <S.FullTitle>{`${props.webData.drugTitle} ${props.webData.outFormTitle}`}</S.FullTitle>
                    </Link>
                    <S.TextInfoContainer>
                        <S.TextInfo>
                            <S.TextInfoDescription>{props.webData.makerTitle}</S.TextInfoDescription>
                        </S.TextInfo>
                        {isMNN && (
                            <S.TextInfo>
                                <S.TextInfoName>Действующее вещество: </S.TextInfoName>
                                <S.TextInfoDescription>{props.webData.mnnRusTitle}</S.TextInfoDescription>
                            </S.TextInfo>
                        )}
                    </S.TextInfoContainer>
                    <S.MarkerBlock>
                        <MarkerContainer
                            options={props.options}
                            stockPrice={props.stockPrice}
                            deliveryPrice={props.deliveryPrice}
                            analoguesQty={props.analoguesQty}
                            anotherOutFormsQty={props.anotherOutFormsQty}
                            aptEtaDateTime={props.aptEtaDateTime}
                            isCold={props.isCold}
                            isDemand={props.isDemand}
                            isHidden={props.isHidden}
                            isOriginalGood={props.isOriginalGood}
                            isStrictlyByPrescription={props.isStrictlyByPrescription}
                        />
                    </S.MarkerBlock>
                </S.FullInfo>
            </S.InfoContainer>

            <S.PriceContainer>
                <S.PriceDataContainer>
                    {/* <S.LikedContainer>
                        <S.LikedIconBlock onClick={likeHandler}>
                            <LikeIcon color={likedColor} />
                        </S.LikedIconBlock>
                    </S.LikedContainer> */}
                    {!props.options.isPositions ? (
                        <PriceBlock
                            isSelectedShipment={props.isSelectedShipment}
                            isSelectedStock={!!(props.shipment.stockId && props.shipment.stockId > 0)}
                            bookedCount={props.bookedCount}
                            isChanged={props.isChanged}
                            availableStocksCount={props.availableStocksCount}
                            addBasketHandler={addBasketHandler}
                            changePriceHandler={changePriceHandler}
                            clickOpenAnotherStockHandler={clickOpenAnotherStockHandler}
                            stockPrice={props.stockPrice}
                            deliveryPrice={props.deliveryPrice}
                            showedAnotherStocks={showedAnotherStocks}
                            minPrice={props.minPrice}
                            deliveryStatus={props.deliveryStatus}
                            isAvailable={props.isAvailable}
                            isAvailableOnStock={props.isAvailableOnStock}
                            count={props.basketItemCount}
                            options={props.options}
                            openDeliveryReasons={openDeliveryReasons}
                            closeDeliveryReasons={closeDeliveryReasons}
                            deleteItemHandler={deleteItemHandler}
                            hiddenItemHandler={() => props.hiddenPositionHandler(props.webData.goodId)}
                            goodId={props.webData.goodId}
                            goodName={`${props.webData.drugTitle} ${props.webData.outFormTitle}`}
                        />
                    ) : (
                        <FixPriceBlock
                            basketItemCount={props.basketItemCount}
                            changePriceHandler={changePriceHandler}
                            addBasketHandler={addBasketHandler}
                            price={props.fixPrice}
                            count={props.count}
                            goodId={props.webData.goodId}
                        />
                    )}
                </S.PriceDataContainer>
                {props.options.isBasket && (isSHowDeletes || !props.isSelectedShipment) && (
                    <S.CloseContainer>
                        <ButtonTransparent
                            options={optionsButtonText}
                            colors={colorsButtonText}
                            clickHandler={deleteItemHandler}>
                            Удалить
                        </ButtonTransparent>
                        <ButtonTransparent
                            options={optionsButtonText}
                            colors={colorsButtonText}
                            clickHandler={() => props.hiddenPositionHandler(props.webData.goodId)}>
                            Отложить
                        </ButtonTransparent>
                    </S.CloseContainer>
                )}
            </S.PriceContainer>
            {/* {props.options.isBasket && (
                <S.CloseContainer onClick={deleteItemHandler}>
                    <Cross />
                </S.CloseContainer>
            )} */}
            {isShowedModalNotSelectedShipment && (
                <ModalWindowCustom
                    footer={getModalIsNotShipmentFooter()}
                    title="Внимание"
                    cancel={{
                        text: 'Отмена',
                        clickHandler: () => setIsShowedModalNotSelectedShipment(false)
                    }}>
                    <S.ModalIsNotShipmentContainer>
                        <S.ModalIsNotShipmentP>
                            Для Вашего удобства рекомендуем выбрать аптеку самовывоза или адрес доставки. Это
                            позволит:
                        </S.ModalIsNotShipmentP>
                        <S.ModalIsNotShipmentUl>
                            <S.ModalIsNotShipmentLi>
                                Видеть наличие товаров и цены в конкретной аптеке самовывоза
                            </S.ModalIsNotShipmentLi>
                            <S.ModalIsNotShipmentLi>
                                Определить возможность доставки и ассортимент товаров, доступных по
                                конкретному адресу
                            </S.ModalIsNotShipmentLi>
                        </S.ModalIsNotShipmentUl>
                        <S.ModalIsNotShipmentP>
                            Выбор можно сделать позже (на главной странице или в корзине), но не все товары,
                            добавленные в корзину ранее, могут быть доступны для самовывоза или доставки из-за
                            их отсутствия.
                        </S.ModalIsNotShipmentP>
                    </S.ModalIsNotShipmentContainer>
                </ModalWindowCustom>
            )}
            {isShowedSelectorStock && (
                <SelectorStock
                    stocks={stocksAll}
                    cities={cities}
                    goodIds={basketItems.map((x) => x.webData.goodId)}
                    changeSelectedStockHandler={selectedStocAndCitykHandler}
                    closeHandler={() => setIsShowedSelectorStock(false)}
                    selectedStock={getSelectedStock()}
                    cityId={props.shipment.cityId}
                />
            )}
            {isShowedSelectorAddress && (
                <ModalAddressSelect
                    shipAddressId={props.shipment.shipAddressId}
                    closeHandler={() => setIsShowedSelectorAddress(false)}
                    changeAddressHandler={changeSelectedAddressHandler}
                />
            )}
            {anotherStocks.isShowedModal && (
                <ModalWindow
                    title=""
                    isButtonFooter={false}
                    cancel={{
                        text: 'Ок',
                        clickHandler: closeAnotherStocks
                    }}>
                    <S.StocksContainer>
                        {anotherStocks.stocks.map((stock) => (
                            <StockItem
                                isMetroLocation={stock.isMetroLocation}
                                metroColor={stock.metroColor}
                                title={stock.title}
                                name={stock.name}
                            />
                        ))}
                    </S.StocksContainer>
                </ModalWindow>
            )}
            {isShowedModalDemand && (
                <ModalWindow
                    title="Внимание"
                    submit={{
                        text: 'Добавить в корзину',
                        clickHandler: addBasket
                    }}
                    cancel={{
                        text: 'Отмена',
                        clickHandler: closeDemandModal
                    }}>
                    <S.BodyModel
                        dangerouslySetInnerHTML={{
                            __html: getFeatureHtml()
                        }}
                    />
                </ModalWindow>
            )}
            {deliveryReasonState.isShowedModal && (
                <ModalWindow
                    title="Внимание"
                    isButtonFooter={false}
                    isFooterOnlyOneButton
                    submit={{
                        text: 'Ок',
                        clickHandler: closeDeliveryReasons
                    }}
                    cancel={{
                        text: 'Ок',
                        clickHandler: closeDeliveryReasons
                    }}>
                    <S.ReasonContainer>
                        {deliveryReasonState.reasons.map((reason) => (
                            <S.ReasonTextItem
                                key={reason.id}
                                dangerouslySetInnerHTML={{ __html: reason.text }}
                            />
                        ))}
                    </S.ReasonContainer>
                </ModalWindow>
            )}
        </S.Container>
    )
}

CatalogGoodItem.propTypes = {
    webData: _WebDataPropTypes,
    stockPrice: _PricePropTypes,
    deliveryPrice: _PricePropTypes,
    minPrice: _PricePropTypes,
    fixPrice: _PricePropTypes,
    count: PropTypes.number,
    deliveryStatus: PropTypes.shape({
        isAvailableForDelivery: PropTypes.bool.isRequired,
        reasons: PropTypes.arrayOf(
            PropTypes.shape({
                id: PropTypes.number,
                warningText: PropTypes.string
            })
        )
    }),
    bookedCount: PropTypes.number,
    isChanged: PropTypes.bool,
    deletePositionHandler: PropTypes.func,
    isAvailable: PropTypes.bool.isRequired,
    isAvailableOnStock: PropTypes.bool.isRequired,
    isStrictlyByPrescription: PropTypes.bool.isRequired,
    basketItemCount: PropTypes.number,
    availableStocksCount: PropTypes.number,
    analoguesQty: PropTypes.number,
    anotherOutFormsQty: PropTypes.number,
    isOriginalGood: PropTypes.bool,
    isHidden: PropTypes.bool.isRequired,
    isCold: PropTypes.bool,
    isDemand: PropTypes.bool,
    aptEtaDateTime: PropTypes.string,
    shipment: PropTypes.object,
    groupId: PropTypes.number,
    hiddenPositionHandler: PropTypes.func,
    options: PropTypes.shape({
        isBasket: PropTypes.bool,
        isDelivery: PropTypes.bool,
        isPositions: PropTypes.bool
    })
}
CatalogGoodItem.defaultProps = {
    options: {
        isBasket: false,
        isDelivery: false,
        isPositions: false
    },
    isHidden: false
}

export default CatalogGoodItem
