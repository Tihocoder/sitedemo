import styled from 'styled-components'
import * as CC from '../../shared/CountChanger/CountChanger.styled'

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    height: 100%;
    min-height: fit-content;
    font-size: 1.25rem;
    align-items: flex-end;
    max-width: 13em;
    min-height: 9rem;
`
export const ButtonBasketContainer = styled.div`
    font-size: 1rem;
    box-sizing: border-box;
    display: flex;
    justify-content: flex-end;
    flex-direction: column;
    width: 9.2rem;
    ${CC.Container} {
        max-height: 2.5rem;
    }
    & > * {
        margin-top: 0.3rem;
    }
    & > a {
        text-align: center;
        padding: 0.7rem 1.2rem;
    }
`

export const PriceDataActual = styled.div`
    display: flex;
    flex-direction: column;
    & span:first-child {
        margin-right: 0.2rem;
    }
`
export const PriceCountInfoBlock = styled.div``
export const PriceCountInfo = styled.span`
    font-size: 0.875rem;
`

export const PriceNumberActualBold = styled.span`
    font-weight: bold;
`
export const BlockLike = styled.div`
    height: 100%;
`

export const PriceBottomBlock = styled.div`
    display: flex;
    min-width: 11rem;
    justify-content: flex-end;
    align-items: center;
`

export const LikedContainer = styled.div`
    display: flex;
    align-items: flex-end;
`
export const LikedIconBlock = styled.div`
    cursor: pointer;
    padding: 0.2rem;
    padding-right: 0.6rem;
    width: 1.8rem;
    & svg > path {
        fill: ${(props) =>
            props.isLiked ? props.theme.colors.pureRed : props.theme.colors.getGrayColor(0.1)};
    }
`

