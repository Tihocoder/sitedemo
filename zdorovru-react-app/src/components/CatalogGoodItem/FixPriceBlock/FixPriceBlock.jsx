import React from 'react'
import PropTypes from 'prop-types'
import * as S from './FixPriceBlock.styled'
import SymbolRuble from '../../shared/SymbolRuble/SymbolRuble'
import { toggleLikedGood } from '../../../actions/customer'
import { useDispatch, useSelector } from 'react-redux'
import LikeIcon from '../../Icons/LikeIkon'
import PriceItem from '../PriceBlock/PriceItem'
import { ElementSizeType } from '../../../styles/theme-const'
import Button from '../../shared/Button/Button'
import CountChanger from '../../shared/CountChanger/CountChanger'

const FixPriceBlock = ({ price, count, goodId, basketItemCount, changePriceHandler, addBasketHandler }) => {
    const likedGoods = useSelector((state) => state.customer.likedGoods)
    const dispatch = useDispatch()
    const toggleLikeHandler = async (webId) => {
        await dispatch(toggleLikedGood(webId))
    }
    return (
        <S.Container>
            <S.PriceDataActual>
                <PriceItem price={price} title={'Стоимость'} />
                <S.PriceCountInfoBlock>
                    <S.PriceCountInfoBlock>
                        <S.PriceCountInfo>
                            {count} шт. по {price.normal.toLocaleString('ru-RU')} <SymbolRuble />
                        </S.PriceCountInfo>
                    </S.PriceCountInfoBlock>
                </S.PriceCountInfoBlock>
            </S.PriceDataActual>
            <S.PriceBottomBlock>
                <S.BlockLike>
                    <S.LikedContainer>
                        <S.LikedIconBlock
                            isLiked={likedGoods.includes(goodId)}
                            onClick={() => toggleLikeHandler(goodId)}>
                            <LikeIcon />
                        </S.LikedIconBlock>
                    </S.LikedContainer>
                </S.BlockLike>
                <S.ButtonBasketContainer>
                    {basketItemCount ? (
                        <CountChanger value={basketItemCount} changeValueHandler={changePriceHandler} />
                    ) : (
                        <Button
                            elementSizeTypeValue={ElementSizeType.regular}
                            clickHandler={addBasketHandler}>
                            В корзину
                        </Button>
                    )}
                </S.ButtonBasketContainer>
            </S.PriceBottomBlock>
        </S.Container>
    )
}

FixPriceBlock.propTypes = {
    price: {
        normal: PropTypes.number.isRequired
    },
    goodId: PropTypes.number.isRequired,
    basketItemCount: PropTypes.func.isRequired,
    count: PropTypes.number.isRequired
}

export default FixPriceBlock
