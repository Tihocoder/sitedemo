import styled from 'styled-components'

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: ${(props) => (props.isAvaliableSlots ? 'space-between' : 'center')};
    min-height: 2.5rem;
    box-sizing: border-box;
`
export const Title = styled.span`
    white-space: nowrap;
`

export const PaymentTypesBlock = styled.div`
    display: flex;
    flex-direction: column;
    margin: 0;
    padding: 0;
    outline: none;
    font-size: 0.8rem;
`

export const PaymentType = styled.span`
    white-space: nowrap;
    color: ${(props) => props.theme.colors.getGrayColor(0.4)};
`
