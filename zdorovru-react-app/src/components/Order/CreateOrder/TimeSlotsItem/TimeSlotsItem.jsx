import React from 'react'
import PropTypes from 'prop-types'
import * as S from './TimeSlotsItem.styled'

const TimeSlotsItem = ({ title, paymentTypes }) => (
    <S.Container isAvaliableSlots={!!paymentTypes?.length}>
        <S.Title>{title}</S.Title>
        {paymentTypes?.length ? (
            <S.PaymentTypesBlock>
                {paymentTypes.map((paymentType) => (
                    <S.PaymentType key={paymentType.id}>{paymentType.title}</S.PaymentType>
                ))}
            </S.PaymentTypesBlock>
        ) : (
            <React.Fragment />
        )}
    </S.Container>
)

TimeSlotsItem.propTypes = {
    title: PropTypes.string.isRequired,
    paymentTypes: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.any.isRequired,
            title: PropTypes.string.isRequired
        })
    )
}
TimeSlotsItem.defaultProps = {
    paymentTypes: null
}

export default TimeSlotsItem
