import styled, { css } from 'styled-components'

export const Container = styled.div`
    min-width: 25rem;
    background: #fff;
    padding: 2rem 0;
    z-index: 10;
    display: flex;
`

export const LeftBlock = styled.div`
    min-width: 25rem;
    background: #fff;
    z-index: 10;
    max-width: 30rem;
`
export const RigthBlock = styled.div`
    z-index: 10;
    max-width: 30rem;
    margin-left: 7rem;
`

export const PointDescriptionBlock = styled.div`
    display: flex;
    flex-direction: column;
    font-size: 1rem;
`
export const PointTitle = styled.div`
    font-size: 0.85rem;
    color: ${(props) => props.theme.colors.getGrayColor(0.5)};
`

export const PointDescription = styled.div`
    font-size: 1.1rem;
    color: ${(props) => props.theme.colors.getGrayColor(1)};
    margin-top: 0.5rem;
`

export const Header = styled.header`
    height: 3rem;
    display: flex;
    justify-content: space-between;
    align-items: center;
`
export const HeaderTitle = styled.h1`
    font-size: 1.1rem;
`

export const ButtonClose = styled.button`
    box-shadow: 0px;
    outline: none;
    margin: 0;
    background-color: inherit;
    border: none;
    cursor: pointer;
    width: 2.6rem;

    & svg {
        fill: ${(props) => props.theme.colors.blackTr};
    }

    &:hover > svg {
        fill: ${(props) => props.theme.colors.black};
    }
`
export const Body = styled.div`
    min-height: 10rem;
    display: flex;
`

export const CreatedContainer = styled.div`
    display: flex;
    flex-direction: column;
`

export const BodyBlockLeft = styled.div`
    display: flex;
    flex-direction: column;
    margin-right: 13rem;
`

export const BodyBlockRight = styled.div`
    display: flex;
    flex-direction: column;
    padding: 2rem 0;
`

export const Footer = styled.footer`
    display: flex;
    flex-direction: column;

    & button {
        margin-right: 1rem;
    }

    & button:last-child {
        margin-right: 0;
    }
`
export const ButtonContainer = styled.div`
    display: flex;
`

export const ContactDataBlock = styled.div`
    display: flex;
    flex-direction: column;
    width: 25rem;

    & > * {
        margin-bottom: 0.7rem;
    }
`

export const Description = styled.div`
    font-size: 0.8rem;
    color: ${(props) => props.theme.colors.getGrayColor(0.4)};
`

export const ContactBlock = styled.div`
    font-size: 0.8rem;
`

export const ContactBlockTitile = styled.h2`
    font-size: 1rem;
`

export const ReceiveBlock = styled.div`
    display: flex;
    flex-direction: column;
`

export const ReceiveSelector = styled.div`
    box-sizing: border-box;
    display: flex;
    justify-content: space-between;
    width: 15rem;
`
export const ReceiveButton = styled.div`
    padding: 1rem;
    background-color: inherit;
    color: ${(props) => props.theme.colors.blackTr};
    border-radius: 0.3rem;
    transition: all 0.2s ease;
    font-size: inherit;
    font-weight: bold;

    &:hover {
        transition: all 0.2s ease;
        color: ${(props) => props.theme.colors.black};
    }

    /* border: 1px solid ${(props) => props.theme.colors.blackTr2}; */
    border: 0;
    background-color: ${(props) => props.theme.colors.blackTr3};

    ${(props) =>
        props.isSelected
            ? css`
                  background-color: ${(props) => props.theme.colors.accentGreen};
                  color: ${(props) => props.theme.colors.white};
                  border: 0;
              `
            : css`
                  box-shadow: 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 2px 2px 0 rgba(0, 0, 0, 0.14),
                      0 1px 5px 0 rgba(0, 0, 0, 0.12);
              `};
    cursor: pointer;
`

export const ReceiveDataBlock = styled.div`
    margin-top: 1rem;
`
export const DeliveryBlock = styled.div`
    display: flex;
    flex-direction: column;
`

export const PickupBlock = styled.div`
    display: block;
    width: 30rem;
`

export const DeliveryAddressData = styled.div`
    margin-top: 1rem;
    display: flex;
    flex-direction: column;
    width: 32rem;

    & > * {
        margin-bottom: 0.8rem;
    }
`

export const PointTypeContainer = styled.div`
    margin-top: 2rem;
    display: flex;

    & > div {
        justify-content: space-between;
        width: 25rem;
    }
`
export const TimeSlotContainer = styled.div`
    display: flex;
    flex-direction: column;

    & > div {
        width: 100%;
        max-width: 19rem;
    }
`

export const BlockInDataBlock = styled.div`
    padding: 0.4rem 0;
`
export const HomeDataApts = styled.div`
    display: flex;

    & > input {
        margin-right: 1rem;
        min-width: 4rem;
        max-width: 7rem;
    }

    & > input:last-child {
        margin-right: 0;
    }

    justify-content: space-between;
`
export const HomeDataOffice = styled.div`
    display: flex;

    & > input:first-child {
        margin-right: 1rem;
    }

    & > input:last-child {
        max-width: 30%;
    }

    justify-content: space-between;
`
export const OrderDescription = styled.div`
    font-size: 1rem;
    color: ${(props) => props.theme.colors.getGrayColor(0.6)};
`

export const Price = styled.span`
    font-weight: bold;
    font-size: 1.1rem;
    color: ${(props) => props.theme.colors.getGrayColor(0.8)};
`

export const OrderNumberBlock = styled.div`
    margin-top: 1rem;
    padding: 1.2rem;
    background-color: ${(props) => props.theme.colors.getRedheadColor(0.1)};
    display: flex;
    flex-direction: column;
`
export const OrderNumberTitle = styled.div`
    font-size: 1.5rem;
    margin-bottom: 1rem;
`
export const OrderNumber = styled.div`
    font-size: 3rem;
    color: ${(props) => props.theme.colors.getRedheadColor(0.8)};
`

export const CreateOrderPolicyDescription = styled.div`
    margin-top: 1rem;
    font-size: 0.9rem;
    color: ${(props) => props.theme.colors.getGrayColor(0.45)};
    max-width: 30rem;
`
export const DescriptionLink = styled.a`
    color: ${(props) => props.theme.colors.getGreenColor(1)};
    text-decoration: none;

    :hover {
        text-decoration: dashed;
        color: ${(props) => props.theme.colors.getGreenColor(0.85)};
        border-bottom: 1px dashed ${(props) => props.theme.colors.getGreenColor(0.85)};
    }
`

export const Address = styled.div`
    font-weight: bold;
    font-size: 1.1rem;
    color: ${(props) => props.theme.colors.getGrayColor(0.8)};
`
export const InputContainer = styled.div`
    & > input {
        width: 100%;
        padding: 0.7rem 1rem;
        font-size: 1rem;
        border: 1px solid rgba(50, 62, 52, 0.1);
        color: ${(props) => props.theme.colors.getGrayColor(0.6)};
        box-sizing: border-box;
        outline: none;
        border-radius: 0.3rem;
        transition: all 0.35s ease;
        font: inherit;

        ${(props) => (props.isValid ? validatedInput : notValidatedInput)}
        ::placeholder {
            color: ${(props) => props.theme.colors.getGrayColor(0.4)};
        }

        &:focus {
            color: ${(props) => props.theme.colors.getGrayColor(0.6)};
            box-shadow: ${(props) => props.theme.shadows.shadowKitHover};

            ::placeholder {
                transition: opacity 0.45s ease;
                color: ${(props) => props.theme.colors.getGrayColor(0.4)};
            }
        }
    }
`

export const OperatorContainer = styled.div`
    & > div > div > div {
        margin-right: 3rem;
    }
`

export const LinkAccentLarge = styled.a`
    border: 0;
    background-color: inherit;
    color: ${(props) => props.theme.colors.accentGreen};
    border-radius: 0.3rem;
    transition: color 0.2s ease;
    display: flex;
    align-items: center;
    cursor: pointer;
    font-weight: bold;
    font-size: 1.2rem;
    padding: 0.5rem 0.8rem;
    text-decoration: none;

    :hover {
        color: ${(props) => props.theme.colors.accentGreenLight};
    }
`
const validatedInput = css`
    border: 1px solid ${(props) => props.theme.colors.getGrayColor(0.1)};
    background-color: inherit;
`
const notValidatedInput = css`
    border: 1px solid ${(props) => props.theme.colors.accentGreen};
`
