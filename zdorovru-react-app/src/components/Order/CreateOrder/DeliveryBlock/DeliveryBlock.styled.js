import styled from 'styled-components'

export const DeliveryBlock = styled.div`
    display: flex;
    flex-direction: column;
`
export const DeliveryBlockTitile = styled.span`
    color: ${(props) => props.theme.colors.getGrayColor(0.4)};
`

export const DeliveryAddressData = styled.div`
    margin-top: 1rem;
    display: flex;
    flex-direction: column;
    width: 32rem;
    & > * {
        margin-bottom: 0.8rem;
    }
`

export const PointTypeContainer = styled.div`
    margin-top: 2rem;
    display: flex;
    & > div {
        justify-content: space-between;
        width: 25rem;
    }
`
export const TimeSlotContainer = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    max-width: 25rem;
`
export const BlockInDataBlock = styled.div`
    padding: 0.4rem 0;
`
export const ShipAddressContainer = styled.div`
    display: flex;
    padding: 1rem 0;
    justify-content: space-between;
    & > div:first-child {
        width: 23rem;
    }
    & button {
        width: 8rem;
    }
`
export const SelectedAddressBlock = styled.div``
export const HouseTypeContainer = styled.div`
    & > div {
        justify-content: space-between;
        width: 25rem;
    }
`

export const HomeDataApts = styled.div`
    display: flex;
    width: 33rem;
    & > div > input {
        margin-right: 1rem;
        min-width: 4rem;
        max-width: 7rem;
    }
    & > div > input:last-child {
        margin-right: 0;
    }
    justify-content: space-between;
`
export const HomeDataOffice = styled.div`
    display: flex;
    & > input:first-child {
        margin-right: 1rem;
    }
    & > input:last-child {
        max-width: 30%;
    }

    justify-content: space-between;
`
