import React from 'react'
import PropTypes from 'prop-types'
import { HouseType } from 'src/consts'
import * as S from './DeliveryBlock.styled'
import TimeSlotsItem from '../TimeSlotsItem/TimeSlotsItem'
import ComboBox from '../../../shared/ComboBox/ComboBox'
import RadioButtonGroup from '../../../shared/RadioButtonGroup/RadioButtonGroup'
import RadioButton from '../../../shared/RadioButton/RadioButton'
import Input from '../../../shared/Input/Input'
import Textarea from '../../../shared/Textarea/Textarea'
import DataBlock from '../../../shared/DataBlock/DataBlock'

const DeliveryBlock = (props) => {
    const deliveryDataEventHandler = (name) => async (e) => {
        await props.changeDeliveryData(name)(e.target.value)
    }

    const generateHomeData = () => {
        if (props.deliveryData.houseType === HouseType.house) {
            return <React.Fragment />
        }
        if (props.deliveryData.houseType === HouseType.office) {
            return (
                <S.HomeDataOffice>
                    <Input
                        value={props.deliveryData.organization}
                        onChange={deliveryDataEventHandler('organization')}
                        placeholderRaised="Организация"
                    />
                    <Input
                        value={props.deliveryData.office}
                        onChange={deliveryDataEventHandler('office')}
                        placeholderRaised="Офис"
                    />
                </S.HomeDataOffice>
            )
        }
        if (props.deliveryData.houseType === HouseType.apt) {
            return (
                <S.HomeDataApts>
                    <Input
                        value={props.deliveryData.entrance}
                        onChange={deliveryDataEventHandler('entrance')}
                        placeholderRaised="Подъезд"
                    />
                    <Input
                        value={props.deliveryData.code}
                        onChange={deliveryDataEventHandler('code')}
                        placeholderRaised="Домофон"
                    />
                    <Input
                        value={props.deliveryData.floor}
                        onChange={deliveryDataEventHandler('floor')}
                        placeholderRaised="Этаж"
                    />
                    <Input
                        value={props.deliveryData.apt}
                        isValid={props.validateDeliveryData.apt}
                        onChange={deliveryDataEventHandler('apt')}
                        placeholderRaised="Квартира"
                    />
                </S.HomeDataApts>
            )
        }
    }
    const { createOrder } = props
    return (
        <S.DeliveryBlock>
            <DataBlock description="Время доставки и способ оплаты:">
                <S.TimeSlotContainer>
                    <ComboBox
                        items={createOrder.timeSlotItems}
                        selectItemId={createOrder.stateFormData?.shipTimeSlotInfo?.shipTimeSlotId}
                        defaultItem={
                            <TimeSlotsItem
                                title="Время доставки"
                                paymentTypes={[{ id: 'default', title: 'Способ оплаты' }]}
                            />
                        }
                        placeholder="Выберите интервал доставки"
                        isValid={createOrder.stateFormData.validate.shipTimeSlotInfo?.shipTimeSlotId}
                        isBorder
                        marginTopSelector="4rem"
                        selectHandler={createOrder.changeTimeSlotHandler}
                    />
                    {!createOrder.stateFormData?.shipTimeSlotInfo?.shipTimeSlotId ? (
                        <React.Fragment />
                    ) : (
                        <S.BlockInDataBlock>
                            <RadioButtonGroup
                                isColumn
                                changeValue={createOrder.changePaymentType}
                                selectValue={createOrder.stateFormData.paymentInfo?.paymentType}
                                isValid={createOrder.stateFormData.validate.paymentInfo?.paymentType}>
                                {createOrder.paymentTypes.map((type) => (
                                    <RadioButton
                                        key={type.id.toString()}
                                        id={type.id.toString()}
                                        value={type.id}>
                                        {type.title}
                                    </RadioButton>
                                ))}
                            </RadioButtonGroup>
                        </S.BlockInDataBlock>
                    )}
                </S.TimeSlotContainer>
            </DataBlock>
            <DataBlock description="">
                {/* <S.ShipAddressContainer>
                    <Textarea
                        value={props.selectedAddress.title}
                        placeholderRaised={'Улица и дом'}
                        withotFocus
                    />
                    <Button
                        elementSizeTypeValue={ElementSizeType.regular}
                        colors={successButtonLight}
                        clickHandler={props.toggleSelectorAddressHandler}>
                        Изменить адрес доставки
                    </Button>
                </S.ShipAddressContainer> */}
                <S.HouseTypeContainer>
                    <RadioButtonGroup
                        changeValue={props.changeDeliveryData('houseType')}
                        selectValue={props.deliveryData.houseType}>
                        <RadioButton key={HouseType.apt} id={HouseType.apt} value={HouseType.apt}>
                            Квартира
                        </RadioButton>
                        <RadioButton key={HouseType.office} id={HouseType.office} value={HouseType.office}>
                            Офис
                        </RadioButton>
                        <RadioButton key={HouseType.house} id={HouseType.house} value={HouseType.house}>
                            Частный дом
                        </RadioButton>
                    </RadioButtonGroup>
                </S.HouseTypeContainer>
                <S.DeliveryAddressData>
                    {generateHomeData()}
                    <Textarea
                        value={props.deliveryData.comment}
                        onChange={deliveryDataEventHandler('comment')}
                        placeholderRaised="Комментарий"
                    />
                </S.DeliveryAddressData>
            </DataBlock>
        </S.DeliveryBlock>
    )
}

DeliveryBlock.propTypes = {
    changeDeliveryData: PropTypes.func.isRequired,
    deliveryData: PropTypes.object.isRequired,
    validateDeliveryData: PropTypes.object,
    /* toggleSelectorAddressHandler: PropTypes.func, */
    selectedAddress: PropTypes.shape({
        id: PropTypes.number.isRequired,
        title: PropTypes.string.isRequired
    })
}

export default DeliveryBlock
