import React from 'react'
import PropTypes from 'prop-types'
import { useCreateOrder } from '@hooks/order/order-hooks'
import InputMask from 'react-input-mask'
import Link from 'next/link'
import StockBlock from '@components/Order/CreateOrder/StockBlock/StockBlock'
import * as S from './CreateOrder.styled'
import * as Theme from '../../../styles/theme-const'
import DataBlock from '../../shared/DataBlock/DataBlock'
import Input from '../../shared/Input/Input'
import Button from '../../shared/Button/Button'
import DeliveryBlock from './DeliveryBlock/DeliveryBlock'
import RadioButtonGroup from '../../shared/RadioButtonGroup/RadioButtonGroup'
import RadioButton from '../../shared/RadioButton/RadioButton'

const CreateOrder = (props) => {
    const createOrder = useCreateOrder(props)
    const changeContactDataHandler = (name) => async (event) => {
        if (name === 'isNeedOperatorCall') {
            createOrder.changeContactDataHandler(name, event)
        } else {
            createOrder.changeContactDataHandler(name, event.target.value)
        }
    }

    if (!props.preorderInfo.isAvailableForOrdering) {
        return (
            <S.Container>
                <S.Header>
                    <S.HeaderTitle>Оформление заказа</S.HeaderTitle>
                </S.Header>
                <S.Body>Данный заказ больше не может быть оформлен, попробуйте еще раз</S.Body>
            </S.Container>
        )
    }

    return (
        <S.Container>
            <S.LeftBlock>
                <S.Header>
                    <S.HeaderTitle>Оформление заказа</S.HeaderTitle>
                </S.Header>
                <S.Body>
                    <S.BodyBlockLeft>
                        <DataBlock description="Ваши контактные данные:">
                            <S.ContactDataBlock>
                                <Input
                                    value={createOrder.stateFormData.userContacts.name}
                                    onChange={changeContactDataHandler('name')}
                                    isValid={createOrder.stateFormData.validate.form.name}
                                    placeholderRaised="Ваше Имя"
                                />
                                <Input
                                    value={createOrder.stateFormData.userContacts.email}
                                    onChange={changeContactDataHandler('email')}
                                    isValid={createOrder.stateFormData.validate.form.email}
                                    placeholderRaised="Электронная почта"
                                />
                                <S.InputContainer isValid={createOrder.stateFormData.validate.form.phone}>
                                    <InputMask
                                        value={createOrder.stateFormData.userContacts.phone}
                                        onChange={changeContactDataHandler('phone')}
                                        isValid={createOrder.stateFormData.validate.form.phone}
                                        type="tel"
                                        mask="+7(999) 999 99 99">
                                        <Input placeholderRaised="Номер телефона" />
                                    </InputMask>
                                </S.InputContainer>
                            </S.ContactDataBlock>
                        </DataBlock>
                        <DataBlock description="Способ подтверждения заказа:">
                            <S.OperatorContainer>
                                <RadioButtonGroup
                                    changeValue={changeContactDataHandler('isNeedOperatorCall')}
                                    selectValue={createOrder.stateFormData.userContacts.isNeedOperatorCall}>
                                    <RadioButton key="1false" id={false} value={false}>
                                        СМС
                                    </RadioButton>
                                    <RadioButton key="2true" id value>
                                        Звонок оператора
                                    </RadioButton>
                                </RadioButtonGroup>
                            </S.OperatorContainer>
                        </DataBlock>
                        {props.isDelivery ? (
                            <DataBlock>
                                <S.ReceiveBlock>
                                    <S.ReceiveDataBlock>
                                        <DeliveryBlock
                                            createOrder={createOrder}
                                            deliveryData={createOrder.stateFormData.deliveryData}
                                            timeSlots={props.timeSlots}
                                            selectedAddress={{
                                                id: createOrder.selectedShipment.shipAddressId,
                                                title: createOrder.selectedShipment.shipAddressTitle
                                            }}
                                            // toggleSelectorAddressHandler={toggleSelectorAddressHandler}
                                            validateDeliveryData={
                                                createOrder.stateFormData.validate.deliveryData
                                            }
                                            changeDeliveryData={createOrder.changeDeliveryDataHandler}
                                        />
                                    </S.ReceiveDataBlock>
                                </S.ReceiveBlock>
                            </DataBlock>
                        ) : (
                            <StockBlock createOrder={createOrder} timeSlots={props.timeSlots} />
                        )}
                    </S.BodyBlockLeft>
                    <S.BodyBlockRight />
                </S.Body>
                <S.Footer>
                    <S.ButtonContainer>
                        <Link href="/order/basket" passHref>
                            <S.LinkAccentLarge>Вернуться к корзине</S.LinkAccentLarge>
                        </Link>
                        <Button
                            elementSizeTypeValue={Theme.ElementSizeType.large}
                            clickHandler={createOrder.submitHandler}
                            disabled={createOrder.isWaitingSubmit}>
                            Оформить заказ
                        </Button>
                    </S.ButtonContainer>
                    <S.CreateOrderPolicyDescription>
                        {/* eslint-disable-next-line react/no-unescaped-entities */}
                        Нажимая кнопку "Оформить заказ", Вы подтверждаете наличие рецепта и соглашаетесь с{' '}
                        <Link href="/privacy-policy" passHref>
                            <S.DescriptionLink>Политикой конфиденциальности</S.DescriptionLink>
                        </Link>
                        ,{' '}
                        <Link href="/terms-of-use" passHref>
                            <S.DescriptionLink>Пользовательским соглашением</S.DescriptionLink>
                        </Link>{' '}
                        и{' '}
                        <Link href="/condition/delivery" passHref>
                            <S.DescriptionLink>Условиями доставки.</S.DescriptionLink>
                        </Link>
                    </S.CreateOrderPolicyDescription>
                </S.Footer>
            </S.LeftBlock>
            <S.RigthBlock>
                {props.preorderInfo.isDelivery ? (
                    <S.PointDescriptionBlock>
                        <S.PointTitle>Адрес доставки заказа</S.PointTitle>
                        <S.PointDescription>{props.preorderInfo.shipAddressText}</S.PointDescription>
                    </S.PointDescriptionBlock>
                ) : (
                    <S.PointDescriptionBlock>
                        <S.PointTitle>Аптека самовывоза заказа</S.PointTitle>
                        <S.PointDescription>
                            {props.preorderInfo.stockTitle} {props.preorderInfo.stockAddress}
                        </S.PointDescription>
                    </S.PointDescriptionBlock>
                )}
            </S.RigthBlock>
        </S.Container>
    )
}

CreateOrder.propTypes = {
    isDelivery: PropTypes.bool.isRequired,
    // eslint-disable-next-line react/forbid-prop-types
    timeSlots: PropTypes.array.isRequired
}

export default CreateOrder
