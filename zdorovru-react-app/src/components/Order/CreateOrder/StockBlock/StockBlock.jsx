import React from 'react'
import DataBlock from '@shared/DataBlock/DataBlock'
import ComboBox from '@shared/ComboBox/ComboBox'
import TimeSlotsItem from '@components/Order/CreateOrder/TimeSlotsItem/TimeSlotsItem'
import RadioButtonGroup from '@shared/RadioButtonGroup/RadioButtonGroup'
import RadioButton from '@shared/RadioButton/RadioButton'
import * as S from './StockBlock.styled'

const StockBlock = ({ createOrder, timeSlots }) => {
    if (!createOrder.timeSlotItems?.length) return <React.Fragment />
    const firstTimeSlot = timeSlots?.length ? timeSlots[0] : null
    if (!firstTimeSlot) return <React.Fragment />
    if (!timeSlots?.length) return <React.Fragment />

    return (
        <DataBlock description="Примерное время получения и способ оплаты:">
            <S.TimeSlotContainer>
                <S.BlockInDataBlock>
                    <RadioButtonGroup
                        isColumn
                        changeValue={createOrder.changePaymentType}
                        selectValue={createOrder.stateFormData.paymentInfo?.paymentType}
                        isValid={createOrder.stateFormData.validate.paymentInfo?.paymentType}>
                        {firstTimeSlot.paymentTypes.map((type) => (
                            <RadioButton key={type.id.toString()} id={type.id.toString()} value={type.id}>
                                {type.title}
                            </RadioButton>
                        ))}
                    </RadioButtonGroup>
                </S.BlockInDataBlock>
                <ComboBox
                    items={createOrder.timeSlotItems}
                    selectItemId={createOrder.stateFormData?.shipTimeSlotInfo?.shipTimeSlotId}
                    defaultItem={<TimeSlotsItem title="Время получения" />}
                    placeholder="Выбрать примерное время получения"
                    isValid={createOrder.stateFormData.validate.shipTimeSlotInfo?.shipTimeSlotId}
                    isBorder
                    marginTopSelector="4rem"
                    selectHandler={createOrder.changeTimeSlotHandler}
                />
            </S.TimeSlotContainer>
        </DataBlock>
    )
}

StockBlock.propTypes = {}

export default StockBlock
