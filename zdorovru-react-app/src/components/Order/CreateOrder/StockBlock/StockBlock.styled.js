import styled from 'styled-components'

export const TimeSlotContainer = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    max-width: 25rem;
`
export const BlockInDataBlock = styled.div`
    padding: 0.4rem 0;
`