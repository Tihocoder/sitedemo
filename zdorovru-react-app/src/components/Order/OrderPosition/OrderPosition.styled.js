import styled from 'styled-components'

export const Container = styled.div`
    display: flex;
    position: sticky;
    flex-direction: column;
    width: 20rem;
    box-shadow: ${(props) => props.theme.shadows.shadow2};
    padding: 1rem;
    border-radius: 0.3rem;
    color: ${(props) => props.theme.colors.colorGray8};
    font-size: 0.8rem;
    top: 1rem;
`

export const priceSpan = styled.span``

export const Header = styled.header`
    display: flex;
    flex-direction: column;
`
export const HeaderTitle = styled.h2`
    font-weight: bold;
    font-size: 1.1rem;
`
export const HeaderPositionInfo = styled.div``

export const PositionContainer = styled.div``

export const PositionItem = styled.div`
    display: flex;
    justify-content: space-between;
    border-bottom: 1px solid ${(props) => props.theme.colors.colorGray07};
    padding: 0.2rem 0;
`

export const PositionDescription = styled.span``

export const PriceContainer = styled.div``

export const Footer = styled.footer``

export const FooterSumBlock = styled.div`
    display: flex;
    flex-wrap: nowrap;
    & > div {
        margin-left: 0.5rem;
    }
`

export const DiscountBlock = styled.div``

export const SumPriceBlock = styled.div``
