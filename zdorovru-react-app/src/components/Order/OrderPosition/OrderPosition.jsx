import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import * as S from './OrderPosition.styled'
import * as _ from 'lodash'
import SymbolRuble from '../../shared/SymbolRuble/SymbolRuble'

const OrderPosition = (props) => {
    const checkActualPosition = (goodId) =>
        props.actualPositions.some((position) => position.goodId === goodId)

    // const [positions, setPositions] = useState([])

    // useEffect(() => {
    const positions = props.basketItems.map((item) => {
        let result = {
            goodId: item.webData.goodId,
            price: props.isDelivery ? item.deliveryPriceSum : item.stockPriceSum,
            count: item.count,
            aptEtaDateTime: item.aptEtaDateTime,
            title: `${item.webData.drugTitle} ${item.webData.outFormTitle}`
        }
        if (!props.actualPositions || props.actualPositions.length === 0) return result

        const actualPosition = props.actualPositions.find(
            (position) => position.goodId === item.webData.goodId
        )
        if (actualPosition) {
            result.price = actualPosition.price
            result.count = actualPosition.count
        }
        return result
    })
    //     setPositions(positions)
    // }, [props.actualPositions])

    return (
        <S.Container>
            <S.Header>
                <S.HeaderTitle>Ваш заказ</S.HeaderTitle>
                <S.HeaderPositionInfo>{`В выбранной аптеке доступно позиций: ${props.actualPositions.length} из ${props.basketItems.length}`}</S.HeaderPositionInfo>
            </S.Header>
            <S.PositionContainer>
                {positions
                    .filter((x) => checkActualPosition(x.goodId))
                    .map((position) => {
                        return (
                            <S.PositionItem>
                                <S.PositionDescription>{position.title}</S.PositionDescription>
                                <S.PriceContainer>
                                    {position.price} <SymbolRuble /> {`x${position.count}`}
                                </S.PriceContainer>
                            </S.PositionItem>
                        )
                    })}
            </S.PositionContainer>
            <S.Footer>
                <S.FooterSumBlock>
                    Итого:{' '}
                    <S.SumPriceBlock>
                        {_.sumBy(
                            positions.filter((x) => checkActualPosition(x.goodId)),
                            (x) => x.price
                        )}
                    </S.SumPriceBlock>{' '}
                    <SymbolRuble />
                </S.FooterSumBlock>
            </S.Footer>
        </S.Container>
    )
}

OrderPosition.propTypes = {
    actualPositions: PropTypes.arrayOf(
        PropTypes.shape({
            goodId: PropTypes.number.isRequired,
            price: PropTypes.number.isRequired,
            count: PropTypes.number.isRequired,
            aptEtaDateTime: PropTypes.instanceOf(Date)
        })
    ).isRequired,
    basketItems: PropTypes.array.isRequired,
    isDelivery: PropTypes.bool.isRequired
}

export default OrderPosition
