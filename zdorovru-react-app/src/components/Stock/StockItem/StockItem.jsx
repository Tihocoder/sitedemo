import React from 'react'
import PropTypes from 'prop-types'
import * as S from './StockItem.styled'

const StockItem = (props) => {
    return (
        <S.StockItem>
            <S.StockColorChar metroColor={props.metroColor}>
                {props.isMetroLocation ? 'м.' : 'г.'}
            </S.StockColorChar>
            <S.StockTitle>{props.name}</S.StockTitle>
        </S.StockItem>
    )
}

StockItem.propTypes = {
    isMetroLocation: PropTypes.bool.isRequired,
    metroColor: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired
}

export default StockItem
