import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { useSelector } from 'react-redux'
import NavList from '../shared/NavList/NavList'
import * as _ from 'lodash'

const calcGoodsInGroup = (id, goodsGroups) => {
    const groups = goodsGroups.filter((x) => x.groupId === id)
    if (groups && groups.length > 0) return groups.length
    return 0
}

const CatalogNavigation = (props) => {
    // if (!props.currentGroupId && (!props.groups )) return <NavList />
    const allGroups = useSelector((state) => state.catalog.mainGroups)

    let currentGroupId = props.currentGroupId
    if (!currentGroupId) {
        const groupDefault = props.groups.find((x) => x.isDefault)
        if (groupDefault) {
            currentGroupId = groupDefault.groupId
        }
    }
    const currentGroup = allGroups.find((x) => x.id === currentGroupId)
    if (!currentGroup) return <NavList />

    const getGroups = () => {
        let arrayGroups = []
        let nodeGroup = {}
        nodeGroup = currentGroup
        arrayGroups.push(nodeGroup)
        while (nodeGroup && nodeGroup.parentClsGroupId) {
            let node = allGroups.find((x) => x.id === nodeGroup.parentClsGroupId)
            if (node) {
                arrayGroups.push(node)
                nodeGroup = node
            }
        }
        return _.sortBy(arrayGroups, (x) => x.level)
    }
    const sortedGroups = getGroups()
    const generateNav = (group) => {
        const childGroup = sortedGroups.find((x) => x.parentClsGroupId === group.id)
        let result = [
            {
                id: group.id,
                text: group.title,
                count: calcGoodsInGroup(group.id, props.goodsGroups),
                childs: null
            }
        ]

        if (childGroup) {
            result[0].childs = generateNav(childGroup)
            return result
        } else {
            const innerGroups = allGroups.filter((x) => x.parentClsGroupId === group.id)
            if (innerGroups && innerGroups.length > 0) {
                result[0].childs = innerGroups.map((x) => ({
                    id: x.id,
                    text: x.title,
                    count: calcGoodsInGroup(x.id, props.goodsGroups),
                    childs: null
                }))
            } else {
                const upperGroup = allGroups.find((x) => x.id === group.parentClsGroupId)
                result = allGroups
                    .filter((x) => x.parentClsGroupId === upperGroup.id)
                    .map((x) => ({
                        id: x.id,
                        text: x.title,
                        count: calcGoodsInGroup(x.id, props.goodsGroups),
                        childs: null
                    }))
            }
        }
        return result
    }

    const firstGroup = sortedGroups.find((x) => x.parentClsGroupId === null)

    let navList = generateNav(firstGroup)
    if (!navList || navList.length === 0) return <React.Fragment />
    return <NavList title={'Категории'} navItems={navList} selectedGroup={props.selectedGroup} />
}

CatalogNavigation.propTypes = {
    currentGroupId: PropTypes.number,
    selectedGroup: PropTypes.number,
    goodsGroups: PropTypes.array.isRequired,
    groups: PropTypes.arrayOf(
        PropTypes.shape({
            groupId: PropTypes.number,
            isDefault: PropTypes.bool.isRequired
        })
    )
}

export default CatalogNavigation
