import React, { useState } from 'react'
import * as S from './Footer.styled'
import { useRouter } from 'next/router'
import { useDispatch, useSelector } from 'react-redux'
import Link from 'next/link'
import { changeShipment } from '../../actions/customer'
import { changeLinkExceptQueryParams } from '../../helpers/site'
import SelectorStock from '../SelectorStock/SelectorStock'

const Footer = (props) => {
    const router = useRouter()
    const dispatch = useDispatch()
    const stocksAll = useSelector((state) => state.company.stocks)
    const shipment = useSelector((state) => state.customer.shipment)
    const cities = useSelector((state) => state.company.cities)
    const basketItems = useSelector((state) => state.customer.basketItems)
    const [isShowedSelectorStock, setIsShowedSelectorStock] = useState(false)
    const city = cities.find((city) => city.id === (shipment.cityId || 1))

    const toggleIsShowedSelectorStockHandler = () => {
        setIsShowedSelectorStock(!isShowedSelectorStock)
    }

    const getSelectedStock = () => stocksAll.find((stock) => stock.id === shipment.stockId)

    //TODO: Вытащить в общий контекст
    const selectedStocAndCitykHandler = async (id, cityId) => {
        await dispatch(changeShipment({ stockId: id, cityId: cityId }))
        setIsShowedSelectorStock(!isShowedSelectorStock)
        changeLinkExceptQueryParams(router, null, null, false)
    }

    const clicLinkHandler = (link) => () => {
        linkWithShipQuery(link, router, shipment)
    }

    return (
        <S.Footer>
            {/* <S.LeftContainer>
                <LogoHeader clickHandler={clickLogoHandler} />
            </S.LeftContainer>
            <S.RightContainer>
                <CityContacts />
            </S.RightContainer> */}
            <S.NavBlock>
                {' '}
                <S.feedbackContainer>
                    <S.PhoneContainer>
                        <S.Phone
                            href={`tel:${((city ? city.phone : false) || '+7 (495) 363-3500').replace(
                                /\D+/g,
                                ''
                            )}`}>
                            {' '}
                            {(city ? city.phone : false) || '+7 (495) 363-3500'}
                        </S.Phone>
                        <S.PhoneTime>07:00-23:00</S.PhoneTime>
                    </S.PhoneContainer>
                    <S.SocityBlock>
                        <S.SocityIconImage
                            src={`${process.env.BASE_PATH || ''}/assets/logo/InstaFrame.svg`}
                        />
                        <S.SocityIconImage
                            src={`${process.env.BASE_PATH || ''}/assets/logo/FaceBookFrame.svg`}
                        />
                        <S.SocityIconImage src={`${process.env.BASE_PATH || ''}/assets/logo/VKFrame.svg`} />
                        <S.SocityIconImage
                            src={`${process.env.BASE_PATH || ''}/assets/logo/WhatsAppFrame.svg`}
                        />
                        <S.SocityIconImage
                            src={`${process.env.BASE_PATH || ''}/assets/logo/YouTubeFrame.svg`}
                        />
                        <S.SocityIconImage src={`${process.env.BASE_PATH || ''}/assets/logo/OKFrame.svg`} />
                    </S.SocityBlock>
                    <S.AppsBlock>
                        <S.AppLink
                            href="https://play.google.com/store/apps/details?id=ru.zdorov"
                            target="_blank">
                            <S.AppIconImage
                                src={`${process.env.BASE_PATH || ''}/assets/logo/GooglePlayBtn.svg`}
                            />
                        </S.AppLink>
                        <S.AppLink href="https://appsto.re/ru/nftOkb.i" target="_blank">
                            <S.AppIconImage
                                src={`${process.env.BASE_PATH || ''}/assets/logo/AppleStoreBtn.svg`}
                            />
                        </S.AppLink>
                    </S.AppsBlock>
                </S.feedbackContainer>
                <S.SectionContainer>
                    <S.Title>Покупателям</S.Title>
                    <S.NavItems>
                        <Link href="/condition/delivery" passHref>
                            <S.NavItem>Условия доставки</S.NavItem>
                        </Link>
                        <Link href={'/condition/pickup'} passHref>
                            <S.NavItem>Условия самовывоза</S.NavItem>
                        </Link>
                        <Link href="/company/loyalty" passHref>
                            <S.NavItem>Программа лояльности</S.NavItem>
                        </Link>
                        <Link href={'/catalog/promotion/list'} passHref>
                            <S.NavItem>Скидки и акции</S.NavItem>
                        </Link>
                    </S.NavItems>
                </S.SectionContainer>
                <S.SectionContainer>
                    <S.Title>Аптечная сеть</S.Title>
                    <S.NavItems>
                        <Link href={'/company/about'} passHref>
                            <S.NavItem>О компании</S.NavItem>
                        </Link>
                        <S.NavItem onClick={toggleIsShowedSelectorStockHandler}>Адреса аптек</S.NavItem>
                        <Link href={'/company/vacancies'} passHref>
                            <S.NavItem>Вакансии</S.NavItem>
                        </Link>
                    </S.NavItems>
                </S.SectionContainer>
                <S.SectionContainer>
                    <S.Title>Юридическая информация</S.Title>
                    <S.NavItems>
                        <Link href="/privacy-policy" passHref>
                            <S.NavItem>Политика конфиденциальности</S.NavItem>
                        </Link>
                        <Link href="/terms-of-use" passHref>
                            <S.NavItem>Пользовательское соглашение</S.NavItem>
                        </Link>
                    </S.NavItems>
                    <S.Title>Контакты</S.Title>
                    <S.NavItems>
                        <Link href={'/contact/feedback'} passHref>
                            <S.NavItem>Обратная связь</S.NavItem>
                        </Link>
                    </S.NavItems>
                </S.SectionContainer>
            </S.NavBlock>
            {isShowedSelectorStock && (
                <SelectorStock
                    stocks={stocksAll}
                    cities={cities}
                    goodIds={basketItems.map((x) => x.webData.goodId)}
                    changeSelectedStockHandler={selectedStocAndCitykHandler}
                    closeHandler={toggleIsShowedSelectorStockHandler}
                    selectedStock={getSelectedStock()}
                    cityId={shipment.cityId}
                />
            )}
        </S.Footer>
    )
}

export default Footer
