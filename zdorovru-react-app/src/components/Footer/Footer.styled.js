import styled from 'styled-components'
import { CityContactTitle, CityContactDescription } from '../Header/CityContacts/CityContacts.styled'

export const Footer = styled.footer`
    padding: 2rem 0;
    height: 11rem;
    display: flex;
    flex-direction: column;
    flex: 0 0 auto;
`

export const LeftContainer = styled.div`
    & svg path {
        fill: ${(props) => props.theme.colors.blackTrI3};
    }
`
export const RightContainer = styled.div`
    color: ${(props) => props.theme.colors.blackTrI3};
    & svg path {
        fill: ${(props) => props.theme.colors.blackTrI3};
    }
    ${CityContactTitle} {
        color: ${(props) => props.theme.colors.blackTrI3};
    }
    ${CityContactDescription} {
        color: ${(props) => props.theme.colors.blackTrI3};
    }
`
export const LinkNav = styled.div``

export const NavBlock = styled.div`
    display: flex;
    justify-content: space-between;
`

export const SectionContainer = styled.div`
    display: flex;
    flex-direction: column;
`

export const Title = styled.h3`
    font-size: 1.25rem;
    font-weight: normal;
    color: ${(props) => props.theme.colors.getGrayColor(0.65)};
`

export const NavItems = styled.div`
    user-select: none;
    margin: 0;
    padding: 0;
    font-weight: normal;
    display: flex;
    flex-direction: column;
`

export const NavItem = styled.a`
    font-size: 0.88rem;
    user-select: none;
    text-decoration: none;
    cursor: pointer;
    color: ${(props) => props.theme.colors.getGrayColor(0.45)};
    :hover {
        color: ${(props) => props.theme.colors.getGreenColor(1)};
    }
    list-style-type: none;
    & > a {
        font: inherit;
        color: inherit;
        text-decoration: none;
    }
`
export const feedbackContainer = styled.div`
    display: flex;
    flex-direction: column;
    margin-top: 2rem;
    & svg path {
        fill: ${(props) => props.theme.colors.getGrayColor(0.1)};
    }
    ${CityContactTitle} {
        color: ${(props) => props.theme.colors.getGrayColor(0.4)};
    }
    ${CityContactDescription} {
        color: ${(props) => props.theme.colors.getGrayColor(0.4)};
    }
`

export const PhoneContainer = styled.div`
    display: flex;
    cursor: pointer;
    justify-content: space-between;
    align-items: flex-end;
`
export const Phone = styled.a`
    display: flex;
    font-size: 1.5rem;
    font-weight: bold;
    text-decoration: none;
    color: ${(props) => props.theme.colors.grayText};
    & input {
        border: 0;
    }
`
export const PhoneTime = styled.span`
    color: ${(props) => props.theme.colors.grayText};
    font-size: 0.9rem;
    margin-bottom: 0.2rem;
`
export const SocityBlock = styled.div`
    display: flex;
    margin-top: 0.5rem;
`

export const AppsBlock = styled.div`
    display: flex;
    justify-content: space-between;
    margin-top: 0.5rem;
`
export const AppLink = styled.a``

export const SocityIconImage = styled.img`
    box-sizing: border-box;
`
export const AppIconImage = styled.img`
    box-sizing: border-box;
`
