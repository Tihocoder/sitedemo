import styled, { css } from 'styled-components'

export const BaseBlock = styled.div`
    max-width: 9.7rem;
    width: 100%;
`

export const GoodsCountBlock = styled.div`
    width: fit-content;
    display: flex;
    flex-direction: column;
`

export const RightBlock = styled.div`
    width: fit-content;
    display: flex;
    flex-direction: column;
    margin-left: 0.5rem;
`

export const SelectBlock = styled.div`
    width: 7rem;
    z-index: 10;
    padding: 0.2rem 0 0.2rem 0.8rem;
    & > button {
        display: flex;
        text-align: center;
        padding: 0.5rem 0.2rem;
    }
    // transform: scaleX(50);
    //transition: transform 0.8s ease-in-out;
`

export const MarkerClosed = styled.div`
    border: 1px solid ${(props) => props.theme.colors.getGrayColor(0.85)};
    font-size: 0.82rem;
    padding: 0.1rem;
    border-radius: 0.3rem;
    display: flex;
    justify-content: center;
    width: 6.7rem;
`

export const StockTitleContainer = styled.div`
    display: flex;
`

export const GeneralConainer = styled.div`
    display: flex;
    justify-content: space-between;
    width: 100%;
    min-width: 16rem;
`
export const StockItemContainer = styled.div`
    display: flex;
    justify-content: space-between;
    width: 100%;
`

export const StockItem = styled.div`
    box-shadow: ${(props) => props.theme.shadows.shadowKit};
    transition: all 0.2s ease;
    padding: 0.8rem;
    line-height: 1.2rem;
    border-radius: 0.3rem;
    min-height: 4.7rem;

    cursor: pointer;
    box-shadow: ${(props) => props.theme.shadows.shadowKitHover};
    box-shadow: 0px -2px 8px 0px rgba(0, 0, 0, 0.04), 0px 8px 8px 0px rgba(0, 0, 0, 0.08);
    /* &:hover {
        box-shadow: ${(props) => props.theme.shadows.shadowKitHover};
        box-shadow: 0px -2px 8px 0px rgba(0, 0, 0, 0.04), 0px 8px 8px 0px rgba(0, 0, 0, 0.08);
        transform: translateY(-3px);
    } */

    font-size: 0.8rem;
    color: ${(props) => props.theme.colors.getGrayColor(0.85)};
    display: flex;
    justify-content: space-between;
    z-index: 1;

    ${StockItemContainer} {
        overflow: hidden;
        z-index: 2;
    }
    ${GeneralConainer} {
        min-width: 100%;
        transition: min-width 0.18s ease-in-out;
        box-sizing: border-box;
    }
    ${SelectBlock} {
        transition: min-width 0.18s ease-in-out;
    }
    ${(props) =>
        props.isSelect &&
        css`
            :hover {
                ${GeneralConainer} {
                    min-width: 15rem;
                }

                ${SelectBlock} {
                    // transform: scaleX(3);
                }
            }
        `}
`

export const StockColorChar = styled.div`
    ${(props) => {
        switch (props.metroColor) {
            case 'shabalovka': {
                return 'color: #ff8103'
            }
            case 'darkblue': {
                return 'color: #283981'
            }
            case 'domoded': {
                return 'color: #029a55'
            }
            case 'vernads': {
                return 'color: #b1d332'
            }
            case 'maryno': {
                return ' color: #b1d332'
            }
            case 'varshav': {
                return 'color: #5091bb'
            }
            case 'sokol': {
                return 'color: #029a55'
            }
            case 'butovo': {
                return 'color: #85d4f3'
            }
            case 'orange': {
                return 'color: #ff8103'
            }
            case 'kolomenskaya': {
                return 'color: #029a55'
            }
            case 'saddlebrown': {
                return 'color: #945340'
            }
            case 'yellow': {
                return 'color: #ffd803'
            }
            case 'darkgreen': {
                return 'color: #029a55'
            }
            case 'red': {
                return 'color: #ef1e25'
            }
            case 'lightgreen': {
                return 'color: #b1d332'
            }
            case 'lightblue': {
                return 'color: #85d4f3'
            }
            case 'black': {
                return 'color: black'
            }
            case 'medblue': {
                return 'color: #5091bb'
            }
            case 'purple': {
                return 'color: #b61d8e'
            }
            case 'gray': {
                return 'color: #acadaf'
            }
            case 'aquablue': {
                return 'color: #019ee0'
            }
            default: {
                return 'color: black'
            }
        }
    }};
    font-weight: bold;
    margin-right: 0.4rem;
`
export const StockTitle = styled.span`
    margin: 0;
    // white-space: nowrap;
`

export const DescriptionBlock = styled.div`
    display: flex;
    flex-direction: column;
    color: ${(props) => props.theme.colors.getGrayColor(0.5)};
`

export const InfoText = styled.span`
    display: flex;
    color: ${(props) => props.theme.colors.getGrayColor(0.5)};
    white-space: nowrap;
`
export const GoodCount = styled.span`
    white-space: nowrap;
    color: ${(props) => props.theme.colors.getGrayColor(0.85)};
`
export const GreetInfo = styled.span`
    color: ${(props) => props.theme.colors.accentGreenLight};
`

export const Schedule = styled.div``

export const City = styled.div``

export const TradingCenter = styled.div``

export const Address = styled.div``
