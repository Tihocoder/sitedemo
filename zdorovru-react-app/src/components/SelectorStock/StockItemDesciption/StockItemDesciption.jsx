import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import * as S from './StockItemDesciption.styled'
import { NoWrapSpan } from '../../../styles/base.styled'
import Button from '../../shared/Button/Button'
import { ElementSizeType } from '../../../styles/theme-const'
import { useState } from 'react'

const getColorPrefix = (title) => {
    if (title.includes('м. ')) return 'м.'
    if (title.includes('г. ')) return 'г.'
    return ''
}

const StockItemDesciption = (props) => {
    const [isHoverBlock, setIsHoverBlock] = useState(false)
    const [goodsIndicators, setgoodsIndicators] = useState([])

    const hoverHandler = () => {
        setIsHoverBlock(true)
    }
    const outHoverHandler = () => {
        setIsHoverBlock(false)
    }

    const clickHandler = () => {
        props.clickHandler(props.id, props.coords, props.isOpened)
    }

    const changeStockHandler = (e) => {
        e.stopPropagation()
        props.changeStockWithIdHandler(props.id)
    }
    const generateDate = (date) => {
        const dateInstance = new Date(date)
        let options = { year: 'numeric', month: 'numeric', day: 'numeric' }
        return dateInstance.toLocaleDateString('ru-RU', options)
    }
    const goodsToday =
        props.goodsCount &&
        props.goodsCount.goodOrders &&
        props.goodsCount.goodOrders.filter((good) => !good.aptEtaDateTime)
    const goodsIsDemand = props.goodsCount && !props.goodsCount.isNotDemandInStock

    useEffect(() => {
        let goodsCountIndicators = []
        if (goodsToday && goodsToday.length > 0 && goodsToday.length === props.maxCount) {
            goodsCountIndicators.push(
                <S.GoodCount>
                    <S.GreetInfo>Сегодня</S.GreetInfo> - {goodsToday.length} из {props.goodsCount.maxCount}
                </S.GoodCount>
            )
        } else {
            goodsCountIndicators.push(
                <S.GoodCount>
                    Сегодня - {goodsToday?.length ?? 0} из {props.maxCount}
                </S.GoodCount>
            )
        }
        if (goodsIsDemand && props.goodsCount.maxAptEtaDateTime && props.goodsCount.count > 0) {
            goodsCountIndicators.push(
                <S.GoodCount>
                    {generateDate(props.goodsCount.maxAptEtaDateTime)} - {props.goodsCount.count} из{' '}
                    {props.goodsCount.maxCount}
                </S.GoodCount>
            )
        }
        setgoodsIndicators(goodsCountIndicators)
    }, [])
    const getBasketInfo = () => {
        if (props.fromGood) {
            return <React.Fragment />
        }
        if (!props.maxCount) {
            return <S.GoodsCountBlock></S.GoodsCountBlock>
        }

        return (
            <S.GoodsCountBlock>
                <S.InfoText>Выдача товаров</S.InfoText>
                {goodsIndicators.map((Component, index) => React.cloneElement(Component, { key: index }))}
            </S.GoodsCountBlock>
        )
    }

    return (
        <S.StockItem
            onClick={clickHandler}
            onMouseEnter={hoverHandler}
            onMouseLeave={outHoverHandler}
            isSelect={props.isOpened}>
            <S.StockItemContainer>
                <S.GeneralConainer>
                    <S.BaseBlock>
                        <S.StockTitleContainer>
                            <S.StockColorChar metroColor={props.metroColor}>
                                {getColorPrefix(props.title)}
                            </S.StockColorChar>
                            <S.StockTitle>{props.name}</S.StockTitle>
                        </S.StockTitleContainer>
                        <S.DescriptionBlock>
                            <S.Schedule>{props.timeString}</S.Schedule>
                            {props.tradingCenterTitle && (
                                <S.TradingCenter>{props.tradingCenterTitle}</S.TradingCenter>
                            )}
                            <S.Address>{props.address}</S.Address>
                        </S.DescriptionBlock>
                    </S.BaseBlock>
                    <S.RightBlock>
                        {props.isOpened ? (
                            getBasketInfo()
                        ) : (
                            <S.MarkerClosed>
                                <NoWrapSpan>Скоро открытие</NoWrapSpan>
                            </S.MarkerClosed>
                        )}
                    </S.RightBlock>
                </S.GeneralConainer>
                {/* {isHoverBlock && ( */}
                <S.SelectBlock>
                    <Button elementSizeTypeValue={ElementSizeType.regular} clickHandler={changeStockHandler}>
                        Выбрать аптеку
                    </Button>
                </S.SelectBlock>
                {/* )} */}
            </S.StockItemContainer>
        </S.StockItem>
    )
}

StockItemDesciption.propTypes = {
    id: PropTypes.number.isRequired,
    isMetroLocation: PropTypes.bool.isRequired,
    metroColor: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    maxCount: PropTypes.number.isRequired,
    isOpened: PropTypes.bool.isRequired,
    tradingCenterTitle: PropTypes.string.isRequired,
    goodsCount: PropTypes.shape({
        stockId: PropTypes.number.isRequired,
        count: PropTypes.number.isRequired,
        maxCount: PropTypes.number.isRequired,
        maxAptEtaDateTime: PropTypes.string.isRequired,
        isNotDemandInStock: PropTypes.bool.isRequired,
        goodOrders: PropTypes.array
    }),
    time: PropTypes.shape({
        from: PropTypes.string.isRequired,
        to: PropTypes.string.isRequired
    }).isRequired,
    coords: PropTypes.array.isRequired,
    clickHandler: PropTypes.func.isRequired
}

export default StockItemDesciption
