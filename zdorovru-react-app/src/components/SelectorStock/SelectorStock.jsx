import React, { useEffect, useState } from 'react'
import ReactDOM from 'react-dom'
import ReactDOMServer from 'react-dom/server'
import PropTypes from 'prop-types'
import * as S from './SelectorStock.styled'
import * as Theme from '../../styles/theme-const'
import * as _ from 'lodash'
import {
    FullscreenControl,
    GeolocationControl,
    Map,
    Placemark,
    RouteButton,
    RulerControl,
    SearchControl,
    TrafficControl,
    TypeSelector,
    YMaps,
    ZoomControl
} from 'react-yandex-maps'
import StockItemDesciption from './StockItemDesciption/StockItemDesciption'
import Input from '../shared/Input/Input'
import { getStocksGoods } from '../../api/siteApis'
import Cross from '../Icons/Cross'
import Button from '../shared/Button/Button'
import { generateDate } from '../../helpers/site'
import BalloonMap from './BalloonMap/BalloonMap'

const SelectorStock = (props) => {
    const [selectedStock, setSelectedStock] = useState({
        id: 0,
        title: ''
    })
    const [ymaps, setYmaps] = useState(null)
    const [cityId, setCityId] = useState(1)
    const FilterTypes = {
        All: 1,
        Today: 2,
        Other: 3
    }
    const [activeFilters, setActiveFilters] = useState({
        searchText: '',
        filterType: FilterTypes.All
    })
    const [stocksGoods, setStocksGoods] = useState([])
    const [filters, setFilters] = useState([])

    const changeSearchTextHandler = (e) => {
        setActiveFilters({
            ...activeFilters,
            searchText: e.target.value
        })
    }
    const changeLocalSeletedStock = (id) => {
        const stock = stocksGoods.find((x) => x.id === id)
        if (stock) {
            if (stock.isOpened) {
                setSelectedStock({
                    id: stock.id,
                    title: `${stock.title} ${stock.address}`
                })
            } else {
                setSelectedStock({
                    id: 0,
                    title: ''
                })
            }
        }
    }

    const [mapState, setMapState] = useState({
        center: [55.751574, 37.573856],
        zoom: 9
    })

    const setCenterHandler = (coords, zoom) => {
        setMapState({
            ...mapState,
            center: coords,
            zoom: zoom
        })
    }

    const clickPlacemarkHandler = (id) => () => {
        const stock = stocksGoods.find((stock) => stock.id === id)
        changeLocalSeletedStock(id, stock.isOpened)
        setMapState({
            ...mapState,
            center: [stock.coordinateX, stock.coordinateY],
            zoom: 16
        })
    }

    const selectedStockHandler = (id, coords, isOpened) => {
        changeLocalSeletedStock(id, coords, isOpened)
        setCenterHandler(coords, 16)
    }
    const getLayout = (Component, props) => {
        if (ymaps && ymaps.templateLayoutFactory) {
            const html = ReactDOMServer.renderToString(<Component {...props} />)
            const Layout = ymaps.templateLayoutFactory.createClass(`<div id="balloon">${html}</div>`, {
                build: function () {
                    Layout.superclass.build.call(this)
                    ReactDOM.hydrate(<Component {...props} />, document.getElementById('balloon'))
                }
            })

            return Layout
        }
        return null
    }

    const onLoadMap = async (inst) => {}

    const initFiltredStocks = async (cityId) => {
        const newStocksGoods = await getStocksGoods(cityId, props.goodIds)
        // const stocks = _.map(
        //     props.stocks.filter((x) => x.cityId === cityId),
        //     function (obj) {
        //         return _.assign(obj, _.find(newStocksGoods, { id: obj.id }))
        //     }
        // )
        const stocks = props.stocks
            .filter((x) => x.cityId === cityId)
            .map((x) => {
                var stockWithGoods = newStocksGoods?.find((ns) => ns.id === x.id) || null
                if (stockWithGoods) {
                    return {
                        ...x,
                        ...stockWithGoods
                    }
                } else return x
            })
        setStocksGoods(stocks)
        const stocksCount = stocks.length

        const demandsStocks = stocks.filter((x) => x.count === props.goodIds.length)
        const stockWithMinDate = _.minBy(demandsStocks, (x) => x.maxAptEtaDateTime)

        let arrayFilters = !props.fromGood
            ? [
                  {
                      count: stocksCount,
                      title: 'Все аптеки',
                      type: FilterTypes.All
                  },
                  {
                      count: stocks.filter((x) => x.count === props.goodIds.length && x.isNotDemandInStock)
                          .length,
                      title: 'Выдача сегодня',
                      type: FilterTypes.Today
                  }
              ]
            : [
                  {
                      count: stocks.filter(
                          (x) => x.count === props.goodIds.length && x.count > 0 && x.isNotDemandInStock
                      ).length,
                      title: 'Выдача сегодня',
                      type: FilterTypes.Today
                  }
              ]
        if (stockWithMinDate?.maxAptEtaDateTime) {
            arrayFilters.push({
                count: demandsStocks.length,
                title: `Выдача ${stockWithMinDate && generateDate(stockWithMinDate.maxAptEtaDateTime)}`,
                type: FilterTypes.Other
            })
        }
        switchFilterHandler(arrayFilters[0].type)()
        setFilters(arrayFilters)
    }

    useEffect(() => {
        changeLocalSeletedStock(props.selectedStock?.id || 0)
        setCityId(props.cityId)
        const city = props.cities.find((c) => c.id === props.cityId)
        setCenterHandler([city.coordinateX, city.coordinateY], city.mapZoom)
        initFiltredStocks(props.cityId)
    }, [])

    const changeSelectedCityHandler = (id) => () => {
        const city = props.cities.find((c) => c.id === id)
        setCityId(id)
        initFiltredStocks(id)
        setCenterHandler([city.coordinateX, city.coordinateY], city.mapZoom)
    }

    const switchFilterHandler = (type) => () => {
        setActiveFilters({
            ...activeFilters,
            filterType: type
        })
    }

    const changeStockHandler = () => {
        props.changeSelectedStockHandler(selectedStock.id, cityId)
    }

    const changeStockWithIdHandler = (id) => {
        const stock = stocksGoods.find((x) => x.id === id)
        props.changeSelectedStockHandler(stock.id, cityId)
    }

    const getFiltredStocks = () => {
        let stocksFiltred = stocksGoods.filter((stock) => stock.cityId == cityId)
        if (activeFilters.searchText) {
            stocksFiltred = stocksFiltred.filter((stock) =>
                `${stock.name} ${stock.city} ${stock.address}`
                    .toLowerCase()
                    .includes(activeFilters.searchText.toLowerCase())
            )
        }
        if (activeFilters.filterType === FilterTypes.Today) {
            stocksFiltred = stocksFiltred.filter(
                (stock) => stock.count === props.goodIds.length && stock.isNotDemandInStock
            )
        }
        if (activeFilters.filterType === FilterTypes.Other) {
            stocksFiltred = stocksFiltred.filter((stock) => stock.count === props.goodIds.length)
        }
        return stocksFiltred
    }

    return (
        <S.AbsoluteContainer>
            <S.Window>
                <S.Header>
                    <S.HeaderBlock>
                        {props.fromGood ? (
                            <S.HeaderTitle>Аптеки в которых есть выбранный товар</S.HeaderTitle>
                        ) : (
                            <S.HeaderTitle>Выберите Аптеку</S.HeaderTitle>
                        )}
                        <S.ButtonSubmitContainer>
                            {selectedStock.id ? (
                                <Button
                                    elementSizeTypeValue={Theme.ElementSizeType.regular}
                                    clickHandler={changeStockHandler}>
                                    Подтвердить
                                </Button>
                            ) : (
                                <React.Fragment />
                            )}
                        </S.ButtonSubmitContainer>
                    </S.HeaderBlock>
                    <S.ButtonClose onClick={props.closeHandler}>
                        <Cross />
                    </S.ButtonClose>
                </S.Header>
                <S.Body>
                    <S.StockSelectedTitle>{selectedStock.title}</S.StockSelectedTitle>
                    <S.Container>
                        <S.StockComboboxSearch>
                            <S.FilterBlock>
                                <Input
                                    value={activeFilters.searchText}
                                    placeholder={'Адрес аптеки, Метро, Город, ТЦ'}
                                    onChange={changeSearchTextHandler}
                                />
                                <S.SwitchFilterButtons>
                                    {filters
                                        .filter((x) => x.count > 0)
                                        .map((filter) => (
                                            <S.FilterButton
                                                isActive={filter.type === activeFilters.filterType}
                                                onClick={switchFilterHandler(filter.type)}>
                                                <S.FilerButtonText>{filter.title}</S.FilerButtonText>{' '}
                                                <S.CountIndicate>{filter.count}</S.CountIndicate>
                                            </S.FilterButton>
                                        ))}
                                </S.SwitchFilterButtons>
                            </S.FilterBlock>

                            <S.StocksBlock>
                                {getFiltredStocks().map((x) => (
                                    <StockItemDesciption
                                        key={x.id}
                                        id={x.id}
                                        fromGood={props.fromGood}
                                        isMetroLocation={x.isMetroLocation}
                                        goodsCount={{
                                            count: x.count,
                                            goodOrders: x.goodOrders,
                                            isNotDemandInStock: x.isNotDemandInStock,
                                            maxAptEtaDateTime: x.maxAptEtaDateTime,
                                            maxCount: x.maxCount,
                                            id: x.id
                                        }}
                                        changeStockWithIdHandler={changeStockWithIdHandler}
                                        isOpened={x.isOpened}
                                        maxCount={props.goodIds.length}
                                        name={x.name}
                                        title={x.title}
                                        tradingCenterTitle={x.tradingCenterTitle}
                                        city={x.city}
                                        address={x.address}
                                        metroColor={x.metroColor}
                                        coords={[x.coordinateX, x.coordinateY]}
                                        time={{
                                            from: x.optTimeFrom,
                                            to: x.optTimeTo
                                        }}
                                        timeString={x.timeString}
                                        clickHandler={selectedStockHandler}
                                    />
                                ))}
                            </S.StocksBlock>
                        </S.StockComboboxSearch>

                        <S.MapBlock>
                            <S.SwitchFilterButtons>
                                {props.cities.map((city) => (
                                    <S.FilterButton
                                        onClick={changeSelectedCityHandler(city.id)}
                                        isActive={city.id === cityId}>
                                        {city.title}
                                    </S.FilterButton>
                                ))}
                            </S.SwitchFilterButtons>
                            <YMaps>
                                <Map
                                    modules={[
                                        'geolocation',
                                        'geocode',
                                        'templateLayoutFactory',
                                        'geoObject.addon.balloon',
                                        'geoObject.addon.hint'
                                    ]}
                                    onLoad={(ymaps) => setYmaps(ymaps)}
                                    width={'100%'}
                                    height={'100%'}
                                    state={{
                                        center: mapState.center,
                                        zoom: mapState.zoom
                                    }}>
                                    <FullscreenControl />
                                    <GeolocationControl options={{ float: 'left' }} />
                                    <RouteButton options={{ float: 'right' }} />
                                    <RulerControl options={{ float: 'right' }} />
                                    <TypeSelector options={{ float: 'right' }} />
                                    <TrafficControl options={{ float: 'right' }} />
                                    <SearchControl options={{ float: 'left' }} />
                                    <ZoomControl options={{ float: 'right' }} />
                                    {getFiltredStocks().map((stock) => (
                                        <Placemark
                                            onClick={clickPlacemarkHandler(stock.id)}
                                            options={{
                                                iconLayout: 'default#image',
                                                iconImageHref: stock.isOpened
                                                    ? `${process.env.BASE_PATH || ''}/images/map_logo.png`
                                                    : `${
                                                          process.env.BASE_PATH || ''
                                                      }/images/map_logo_notopened.png`,
                                                iconImageSize: [25, 23],
                                                iconImageOffset: [-13, -14],
                                                balloonCloseButton: true,
                                                hideIconOnBalloonOpen: false,
                                                hintContent: stock.title,
                                                balloonContentLayout: getLayout(BalloonMap, {
                                                    stock: stock,
                                                    changeStockHandler: () =>
                                                        changeStockWithIdHandler(stock.id)
                                                })
                                            }}
                                            geometry={[stock.coordinateX, stock.coordinateY]}
                                        />
                                    ))}
                                </Map>
                            </YMaps>
                        </S.MapBlock>
                    </S.Container>
                </S.Body>
            </S.Window>
        </S.AbsoluteContainer>
    )
}

SelectorStock.propTypes = {
    stocks: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.number.isRequired,
            title: PropTypes.string.isRequired,
            name: PropTypes.string.isRequired,
            isMetroLocation: PropTypes.bool.isRequired,
            coordinateX: PropTypes.number.isRequired,
            isOpened: PropTypes.bool.isRequired,
            coordinateY: PropTypes.number.isRequired,
            metroColor: PropTypes.string.isRequired,
            address: PropTypes.string.isRequired,
            tradingCenterTitle: PropTypes.string.isRequired
        })
    ),
    goodIds: PropTypes.array.isRequired,
    changeSelectedStockHandler: PropTypes.func.isRequired,
    closeHandler: PropTypes.func.isRequired,
    selectedStock: PropTypes.func.isRequired,
    basketItems: PropTypes.array.isRequired,
    cityId: PropTypes.number.isRequired,
    cities: PropTypes.shape({
        id: PropTypes.number,
        title: PropTypes.string
    }).isRequired,
    fromGood: PropTypes.bool
}

export default SelectorStock
