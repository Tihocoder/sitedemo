import styled, { css } from 'styled-components'

export const AbsoluteContainer = styled.div`
    height: 100%;
    width: 100%;
    position: fixed;
    min-height: 100vh;
    left: 0;
    top: 0;
    display: flex;
    justify-content: center;
    align-items: center;
    z-index: 30;
`

export const Window = styled.div`
    padding: 1rem;
    min-width: 25rem;
    background: #fff;
    border-radius: 0.3rem;
    box-shadow: 0 19px 38px rgba(0, 0, 0, 0.3), 0 15px 12px rgba(0, 0, 0, 0.22);
    padding: 2rem;
    z-index: 32;
`
export const Header = styled.header`
    height: 3rem;
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin-bottom: 1rem;
`

export const ButtonSubmitContainer = styled.div`
    display: flex;
`
export const HeaderBlock = styled.div`
    display: flex;
    width: 29rem;
    justify-content: space-between;
`
export const HeaderTitle = styled.h1`
    font-size: 1rem;
    color: ${(props) => props.theme.colors.getGrayColor(0.6)};
`
export const StockSelectedTitle = styled.h2`
    font-size: 1rem;
    font-weight: normal;
    color: ${(props) => props.theme.colors.getGrayColor(0.8)};
`

export const ButtonClose = styled.button`
    box-shadow: 0px;
    outline: none;
    margin: 0;
    border: none;
    cursor: pointer;
    width: 2.6rem;
    background-color: white;
    height: 100%;
    & svg {
        fill: ${(props) => props.theme.colors.blackTr};
    }

    &:hover > svg {
        fill: ${(props) => props.theme.colors.black};
    }
`
export const Body = styled.div`
    min-height: 10rem;
`
export const Footer = styled.footer`
    display: flex;
    font-size: 1rem;
    justify-content: flex-end;
    padding-top: 2rem;
    & button {
        margin-right: 1rem;
    }

    & button:last-child {
        margin-right: 0;
    }
`
export const Container = styled.div`
    display: flex;
    max-height: 32rem;
`
export const StockComboboxSearch = styled.div`
    display: flex;
    flex-direction: column;
    width: 25.5rem;
    font-size: 0.8rem;
`

export const SwitchFilterButtons = styled.ul`
    display: flex;
    padding: 0;
    margin: 0;
    margin-bottom: 1rem;
    & > li {
        margin-right: 0.5rem;
    }
    & > li:last-child {
        margin-right: 0;
    }
`
export const FilterButton = styled.li`
    cursor: pointer;
    ${(props) =>
        props.isActive &&
        css`
            border-top: 3px solid ${props.theme.colors.getGreenColor(1)};
        `};
    display: flex;
    transition: all 0.2s ease;
    font-size: 0.82rem;
    padding: 0.4rem 0.8rem;
    list-style-type: none;
    justify-content: space-between;
    align-items: center;
    width: 6rem;
    height: 2.5rem;
    line-height: 1rem;
    border-radius: 0.3rem;
    box-shadow: ${(props) => props.theme.shadows.shadowKit};
`
export const FilerButtonText = styled.span`
    line-height: 1rem;
`

export const CountIndicate = styled.div`
    background-color: ${(props) => props.theme.colors.getRedColor(0.6)};
    color: white;
    font-weight: bold;
    border-radius: 0.3rem;
    padding: 0.15rem 0.35rem;
    height: 0.8rem;
    display: flex;
    justify-content: center;
    align-items: center;
    font-size: 0.82rem;
`

export const FilterBlock = styled.div`
    padding: 0 0.8rem 0.8rem 0.8rem;
    & > input:first-child {
        margin-bottom: 1rem;
    }
    & input {
        border-radius: 0.3rem;
        padding: 0.5rem 1rem;
        font-size: inherit;
    }
`

export const StocksBlock = styled.div`
    display: flex;
    flex-direction: column;
    overflow-x: auto;
    padding: 0 0.8rem 0.8rem 0.8rem;
    & > div {
        margin-top: 0.4rem;
    }
    & > div:first-child {
        margin-top: 0;
    }
`
export const MapBlock = styled.div`
    display: flex;
    flex-direction: column;
    min-width: 40rem;
    height: 33rem;
`
