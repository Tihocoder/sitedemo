import React from 'react'
import PropTypes from 'prop-types'
import * as S from './BalloonMap.styled'
import Button from '../../shared/Button/Button'
import { ElementSizeType } from '../../../styles/theme-const'
import { generateDate, generateTime } from '../../../helpers/site'

const getTimeBetweenString = (from, to) =>
    `Часы работы с ${generateTime(from)} по ${generateTime(to)}`

const BalloonMap = (props) => {
    return (
        <S.Container>
            <S.Title>{props.stock.name}</S.Title>
            <S.Description>{props.stock.address}</S.Description>
            <S.Description>
                {props.stock.isOpened
                    ? getTimeBetweenString(props.stock.optTimeFrom, props.stock.optTimeTo)
                    : 'Скоро открытие!'}
            </S.Description>
            {props.stock.isOpened && (
                <S.ButtonContainer>
                    <Button
                        elementSizeTypeValue={ElementSizeType.regular}
                        clickHandler={props.changeStockHandler}>
                        Выбрать
                    </Button>
                </S.ButtonContainer>
            )}
        </S.Container>
    )
}

BalloonMap.propTypes = {
    stock: PropTypes.shape({
        id: PropTypes.number.isRequired,
        title: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        isMetroLocation: PropTypes.bool.isRequired,
        coordinateX: PropTypes.number.isRequired,
        isOpened: PropTypes.bool.isRequired,
        coordinateY: PropTypes.number.isRequired,
        metroColor: PropTypes.string.isRequired,
        address: PropTypes.string.isRequired
    }).isRequired,
    changeStockHandler: PropTypes.string.isRequired
}

export default BalloonMap
