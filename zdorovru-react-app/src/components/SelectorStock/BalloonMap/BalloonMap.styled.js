import styled from 'styled-components'

export const Container = styled.div`
    display: flex;
    flex-direction: column;
`

export const Title = styled.p`
    font-size: 1rem;
    margin: 0;
    color: rgba(7, 20, 8, 0.9);
`
export const Description = styled.p`
    font-size: 0.8rem;
    margin: 0;
    margin-top: 0.8rem;
    color: rgba(7, 20, 8, 0.9);
`

export const ButtonContainer = styled.div`
    margin-top: 0.8rem;
    width: 100%;
`
