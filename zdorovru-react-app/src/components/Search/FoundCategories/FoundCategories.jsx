import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import NavList from '../../shared/NavList/NavList'
import * as _ from 'lodash'
import { useRouter } from 'next/router'
import { useSelector } from 'react-redux'

const FoundCategories = (props) => {
    const [categoriesLinks, setCategoriesLinks] = useState([])
    const router = useRouter()

    const generateNav = (category, goodsgroups, goodsCount) => {
        let result = {
            id: category.id,
            text: category.title,
            count: goodsCount,
            childs: []
        }
        const childsCategories = props.categories.filter((cat) => cat.parentGroupId === category.id)
        if (!childsCategories || childsCategories.length < 1) return result
        childsCategories.forEach((cat) => {
            const groupInGoods = goodsgroups.filter((x) => x.groupId === cat.id)
            const goodsCountChild = groupInGoods ? groupInGoods.length : 0
            result.childs.push(generateNav(cat, goodsgroups, goodsCountChild))
        })

        return result
    }
    useEffect(() => {
        let goodsgroups = []
        props.goods.length
        props.goods.forEach((good) => {
            if (good.webData.goodGroups && good.webData.goodGroups.length > 0) {
                good.webData.goodGroups.forEach((group) =>
                    goodsgroups.push({ goodId: good.webData.goodId, groupId: group.clsGroupID })
                )
            }
        })
        const generalCategories = props.categories.filter((x) => x.level === 0)
        let resultNav = []
        generalCategories.forEach((category) => {
            const groupInGoods = goodsgroups.filter((x) => x.groupId === category.id)
            const goodsCount = groupInGoods ? groupInGoods.length : 0
            resultNav.push(generateNav(category, goodsgroups, goodsCount))
        })
        setCategoriesLinks(resultNav)
        return () => {
            setCategoriesLinks([])
        }
    }, [props.categories.length, props.goods.length, router.basePath, router.query])

    return <NavList title={'Найдено в категориях'} navItems={categoriesLinks} />
}

FoundCategories.propTypes = {
    goods: PropTypes.array.isRequired,
    categories: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.number.isRequired,
            parentGroupId: PropTypes.number.isRequired,
            title: PropTypes.string.isRequired,
            level: PropTypes.number.isRequired,
            textOrder: PropTypes.string.isRequired
        })
    ).isRequired
}

export default FoundCategories
