import React from 'react'
import PropTypes from 'prop-types'
import Link from 'next/link'
import { useSelector } from 'react-redux'
import * as S from './BlockCategory.styled'

const generateLink = (id, allGroups) => {
    const three = []
    let node = allGroups.find((x) => x.id === id)
    if (!node) {
        return ''
    }
    three.push(node.id)
    while (node.parentClsGroupId) {
        node = allGroups.find((x) => x.id === node.parentClsGroupId)
        if (node) three.push(node.id)
    }
    return three.reverse().join('/')
}

const BlockCategory = (props) => {
    const groups = useSelector((state) => state.catalog.mainGroups)

    return (
        <Link href={`/catalog/${generateLink(props.id, groups)}`} passHref>
            <S.Container>
                <S.ImageContainer>
                    <S.Image
                        src={props.imgUrl}
                        onError={(e) => {
                            e.target.onerror = null
                            e.target.src = `${process.env.BASE_PATH || ''}/images/noimg.gif`
                        }}
                    />
                </S.ImageContainer>
                <S.TextContainer>
                    <S.Title>{props.title}</S.Title>
                    <S.TitleParrent>{props.parrentGroupTitle}</S.TitleParrent>
                </S.TextContainer>
            </S.Container>
        </Link>
    )
}

BlockCategory.propTypes = {
    id: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
    imgUrl: PropTypes.string.isRequired,
    parrentGroupTitle: PropTypes.string
}
BlockCategory.defaultProps = {
    imgUrl: `${process.env.BASE_PATH || ''}/images/noimg.gif`
}

export default BlockCategory
