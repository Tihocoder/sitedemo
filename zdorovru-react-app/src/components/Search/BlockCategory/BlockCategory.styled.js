import styled from 'styled-components'

export const Container = styled.a`
    display: flex;
    max-width: 15rem;
    height: 100%;
    border-radius: 0.3rem;
    box-shadow: 0 16px 62px rgba(0, 0, 0, 0.03), 0 4px 14px rgba(0, 0, 0, 0.04);
`
export const ImageContainer = styled.div`
    width: 96px;
    height: 96px;
`
export const Image = styled.img`
    height: 100%;
    box-sizing: border-box;
    border-bottom-left-radius: 0.3rem;
    border-top-left-radius: 0.3rem;
`
export const TextContainer = styled.div`
    display: flex;
    padding: 0.8rem;
    font-size: 0.8rem;
    width: 11rem;
    flex-direction: column;
`
export const Title = styled.h3`
    margin: 0;
    font-size: inherit;
    font-weight: normal;
`
export const TitleParrent = styled.h3`
    margin: 0;
    margin-top: 0.4rem;
    font-size: inherit;
    font-weight: normal;
`
