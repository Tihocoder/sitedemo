import styled from 'styled-components'

export const Container = styled.div`
    display: flex;
    margin-top: 1rem;
    justify-content: space-between;
`

export const SearchTitle = styled.div`
    color: ${(props) => props.theme.colors.getGrayColor(0.7)};
    font-size: 1.1rem;
`

export const SearchTitleSpanBold = styled.span`
    color: ${(props) => props.theme.colors.getGrayColor(0.9)};
    font-weight: bold;
    white-space: nowrap;
`

export const SearchBody = styled.div`
    margin-top: 1.2rem;
`

export const Categories = styled.div`
    margin: 0;
`

export const FilterBlock = styled.div`
    display: flex;
    flex-direction: column;
`

export const SearchResult = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
    box-sizing: border-box;
    padding: 0 3.5rem;
`
export const TopCategories = styled.div`
    width: 100%;
    height: 6rem;
    display: flex;
    justify-content: space-between;
    margin-bottom: 1rem;
`
export const GoodList = styled.div`
    display: flex;
    flex-direction: column;
    padding-top: 1rem;
    padding-bottom: 1rem;
`
