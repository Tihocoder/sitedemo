import React, { useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import { useDispatch, useSelector } from 'react-redux'
import { filterGoods, FilterTypes, urlCombine, usePage } from '@helpers/site'
import { toggleLikedGood } from '@actions/customer'
import { removerGoodsFilter, setGoodsFilter } from '@actions/catalog'
import * as S from './Search.styled'
import * as SharedStyled from '../../styles/base.styled'
import * as Theme from '../../styles/theme-const'
import Filter from '../shared/Filter/Filter'
import BlockCategory from './BlockCategory/BlockCategory'
import Paginator from '../shared/Paginator/Paginator'
import CatalogGoodItem from '../CatalogGoodItem/CatalogGoodItem'
import FoundCategories from './FoundCategories/FoundCategories'

const Search = (props) => {
    const router = useRouter()
    const shipment = useSelector((state) => state.customer.shipment)
    const goods = useSelector((state) => state.catalog.goods)
    const categories = useSelector((state) => state.catalog.categories)
    const groups = useSelector((state) => state.catalog.mainGroups)
    const foundCategories = useSelector((state) => state.catalog.foundCategories)
    const basketItems = useSelector((state) => state.customer.basketItems)
    const goodsFilters = useSelector((state) => state.catalog.goodsFilters)
    const goodsData = useSelector((state) => state.catalog.goodsData)
    const likedGoods = useSelector((state) => state.customer.likedGoods)
    const baseImageUrl = useSelector((state) => state.catalog.baseImageUrl)
    const [filteredGoods, setFilteredGoods] = useState([])
    const dispatch = useDispatch()

    const clearFilters = async () => {
        await dispatch(removerGoodsFilter(FilterTypes.makers))
        await dispatch(removerGoodsFilter(FilterTypes.outForms))
    }

    const currentFilters = [
        {
            name: FilterTypes.outForms,
            title: 'Формы выпуска'
        },
        {
            name: FilterTypes.makers,
            title: 'Производитель'
        }
    ]

    const page = usePage()

    const changeSelectedFilters = (name) => async (newItems) => {
        await dispatch(setGoodsFilter(name, newItems))
        if (page.number !== 1) page.changePage(1)
    }
    useEffect(() => {
        clearFilters()
    }, [router.asPath])

    // const initCategories = async () => {
    //     const categoriesTree = await getCategoriesTreeByGoodIds(goods.map((x) => x.webData.goodId))
    //     setCategories(categoriesTree)
    // }

    // useEffect(() => {}, [goods.length])

    const getParrentGroupTitile = (id) => {
        const currentGroup = groups.find((x) => x.id === id)
        if (!currentGroup) return ''
        if (!currentGroup.parentClsGroupId) return ''
        const parrentGroup = groups.find((x) => x.id === currentGroup.parentClsGroupId)
        if (parrentGroup && parrentGroup.title) return parrentGroup.title
        return ''
    }

    const startNumberGoods = (page.number - 1) * Theme.goodsCountOnPage
    const goodsVisible = filteredGoods.slice(startNumberGoods, startNumberGoods + Theme.goodsCountOnPage)
    const goodsPageCount = Math.ceil(filteredGoods.length / Theme.goodsCountOnPage)

    useEffect(() => {
        const newFilteredGoods = filterGoods(goods, goodsFilters, [FilterTypes.makers, FilterTypes.outForms])
        setFilteredGoods(newFilteredGoods)
    }, [router.asPath, goodsFilters])

    const getBasketItem = (goodId) => {
        const basketItem = basketItems.find((x) => x.webData.goodId === goodId)
        return basketItem ? basketItem.count : null
    }
    const toggleLikeHandler = async (webId) => {
        await dispatch(toggleLikedGood(webId))
    }

    return (
        <S.Container>
            <SharedStyled.LeftMenu>
                <S.Categories>
                    <FoundCategories categories={categories} goods={filteredGoods} />
                </S.Categories>
                {currentFilters.map((cf) => (
                    <Filter
                        key={cf.name}
                        title={cf.title}
                        items={goodsData[cf.name]}
                        selectedItems={goodsFilters[cf.name]}
                        changeSelectedItemsHandler={changeSelectedFilters(cf.name)}
                    />
                ))}
            </SharedStyled.LeftMenu>
            <SharedStyled.RightBlock>
                <S.SearchTitle>
                    Вы искали: <S.SearchTitleSpanBold>"{router.query.q}"</S.SearchTitleSpanBold>.{' '}
                    {goodsVisible.length === 0 ? 'По вашему запросу ничего не найдено' : ''}
                </S.SearchTitle>
                <S.SearchBody>
                    {foundCategories && foundCategories.length > 0 && (
                        <S.TopCategories>
                            {foundCategories.map((category) => (
                                <BlockCategory
                                    imgUrl={urlCombine(baseImageUrl, 120, category.imageEndPath)}
                                    id={category.id}
                                    key={category.id}
                                    parrentGroupTitle={getParrentGroupTitile(category.id)}
                                    title={category.title}
                                />
                            ))}
                        </S.TopCategories>
                    )}
                    <Paginator
                        pageCount={goodsPageCount}
                        currentPage={page.number}
                        selectPageHandler={page.changePage}
                    />
                    <S.GoodList>
                        {goodsVisible.length > 0 &&
                            goodsVisible.map((good) => (
                                <CatalogGoodItem
                                    key={good.webData.goodId}
                                    basketItemCount={getBasketItem(good.webData.goodId)}
                                    availableStocksCount={good.availableStocksCount}
                                    webData={good.webData}
                                    stockPrice={good.stockPrice}
                                    deliveryPrice={good.deliveryPrice}
                                    minPrice={good.minPrice}
                                    isDemand={good.isDemand}
                                    analoguesQty={good.analoguesQty}
                                    anotherOutFormsQty={good.anotherOutFormsQty}
                                    aptEtaDateTime={good.aptEtaDateTime}
                                    deliveryStatus={good.deliveryStatus}
                                    isAvailable={good.isAvailable}
                                    isAvailableOnStock={good.isAvailableOnStock}
                                    isStrictlyByPrescription={good.isStrictlyByPrescription}
                                    isOriginalGood={good.isOriginalGood}
                                    isCold={good.isCold}
                                    shipment={shipment}
                                    liked={likedGoods.includes(good.webData.goodId)}
                                    toggleLikeHandler={toggleLikeHandler}
                                    deletePositionHandler={() => {}}
                                />
                            ))}
                    </S.GoodList>
                    <Paginator
                        pageCount={goodsPageCount}
                        currentPage={page.number}
                        selectPageHandler={page.changePage}
                    />
                    {/* <GoodList goods={goodsVisible} checkBasket={checkBasket} isMinPrice={isMinPrice} /> */}
                </S.SearchBody>
            </SharedStyled.RightBlock>
        </S.Container>
    )
}

Search.propTypes = {}

export default Search
