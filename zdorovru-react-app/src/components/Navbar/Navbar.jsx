import React, { useState } from 'react'
import { useRouter } from 'next/router'
import PropTypes from 'prop-types'
import { useDispatch, useSelector } from 'react-redux'
import Link from 'next/link'
import { parseCookies, setCookie } from 'nookies'
import * as S from './Navbar.styled'
import ModalWindow from '../shared/ModalWindow/ModalWindow'
import ListSelector from '../shared/SelectorList/ListSelector'
import { changeShipment, selectCity } from '../../actions/customer'
import * as Theme from '../../styles/theme-const'
import { changeLinkExceptQueryParams } from '../../helpers/site'
import Geo from '../Icons/Geo'
import SelectorStock from '../SelectorStock/SelectorStock'
import GeoSmall from '../Icons/GeoSmall'
import { checkAuthorization } from '../../api/siteApis'
import ModalAuthorize from '../Account/ModalAouthorize/ModalAuthorize'

const Navbar = (props) => {
    const router = useRouter()
    const dispatch = useDispatch()
    const [isShowedGeo, changeShowedGeo] = useState(false)
    const [confirmCustomerWindow, setConfirmCustomerWindow] = useState({
        isShowed: false,
        isSendSms: false
    })
    const stocksAll = useSelector((state) => state.company.stocks)
    const shipment = useSelector((state) => state.customer.shipment)
    const cities = useSelector((state) => state.company.cities)
    const basketItems = useSelector((state) => state.customer.basketItems)
    const [isShowedSelectorStock, setIsShowedSelectorStock] = useState(false)
    const selectedCityId = useSelector((state) => state.customer.shipment.cityId)
    const city = cities.find((city) => city.id === selectedCityId)

    const toggleIsShowedSelectorStockHandler = () => {
        setIsShowedSelectorStock(!isShowedSelectorStock)
    }

    const getSelectedStock = () => stocksAll.find((stock) => stock.id === shipment.stockId)

    // TODO: Вытащить в общий контекст
    const selectedStocAndCitykHandler = async (id, cityId) => {
        await dispatch(changeShipment({ stockId: id, cityId }))
        setIsShowedSelectorStock(!isShowedSelectorStock)
        changeLinkExceptQueryParams(router, null, null, false)
    }

    const clickGeoHandler = () => {
        changeShowedGeo(true)
    }
    const closeGeoHandler = () => {
        changeShowedGeo(false)
    }

    const changeSelectedCity = async (id) => {
        if (shipment.cityId !== id) {
            await dispatch(changeShipment({ cityId: id, stockId: 0, shipAddressId: 0 }))
            changeShowedGeo(false)
            router.reload()
        } else {
            changeShowedGeo(false)
        }
    }

    const openWindowAuthorize = () => {
        setConfirmCustomerWindow({
            ...confirmCustomerWindow,
            isShowed: true
        })
    }
    const closeWindowAutorize = () => {
        setConfirmCustomerWindow({
            ...confirmCustomerWindow,
            isShowed: false
        })
    }

    const clickAccountHandler = async () => {
        const isAuthorization = await checkAuthorization()
        if (isAuthorization) {
            router.push('/account/history')
        } else {
            openWindowAuthorize()
        }
    }

    return (
        <S.Container>
            {isShowedGeo && (
                <ModalWindow
                    title="Выберите ваш регион"
                    isButtonFooter={false}
                    cancel={{
                        text: 'Отмена',
                        clickHandler: closeGeoHandler
                    }}>
                    <S.SelectorRegion>
                        <ListSelector clickItemHandler={changeSelectedCity}>
                            {cities.length > 0 &&
                                cities.map((city) => ({
                                    id: city.id,
                                    content: <S.ComboboxCityItem>{city.title}</S.ComboboxCityItem>
                                }))}
                        </ListSelector>
                    </S.SelectorRegion>
                </ModalWindow>
            )}

            <S.NavBar>
                <S.NavItem onClick={clickGeoHandler}>
                    <GeoSmall />
                    {city?.title || 'Выберите регион'}
                </S.NavItem>
                <S.NavItem onClick={toggleIsShowedSelectorStockHandler}>Адреса аптек</S.NavItem>
                <Link href="/condition/delivery" passHref>
                    <S.NavItem>Доставка</S.NavItem>
                </Link>
                <Link href="/condition/pickup" passHref>
                    <S.NavItem>Самовывоз</S.NavItem>
                </Link>
                <Link href="/company/loyalty">
                    <S.NavItem>Программа лояльности</S.NavItem>
                </Link>
                <Link href="/contact/feedback" passHref>
                    <S.NavItem>Обратная связь</S.NavItem>
                </Link>
            </S.NavBar>
            <S.NavBar>
                <S.NavItem onClick={clickAccountHandler}>
                    <S.UserImage /> Личный кабинет
                </S.NavItem>
            </S.NavBar>
            {isShowedSelectorStock && (
                <SelectorStock
                    stocks={stocksAll}
                    cities={cities}
                    goodIds={basketItems.map((x) => x.webData.goodId)}
                    changeSelectedStockHandler={selectedStocAndCitykHandler}
                    closeHandler={toggleIsShowedSelectorStockHandler}
                    selectedStock={getSelectedStock()}
                    cityId={shipment.cityId}
                />
            )}
            {confirmCustomerWindow.isShowed && <ModalAuthorize closeHandler={closeWindowAutorize} />}
        </S.Container>
    )
}

export default Navbar
