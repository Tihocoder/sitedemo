import styled from 'styled-components'
import * as Theme from '../../styles/theme-const'

export const Container = styled.div`
    display: flex;
    margin: 0;
    background-color: ${(props) => props.theme.colors.greenYellowLight};
    min-height: 1.563rem;
    /* padding: 0 2rem 0.1rem 2rem; */
    //padding: 0.5rem 3.5rem;
    justify-content: space-between;
    & > div {
        margin-right: 1rem;
    }
    & > div:last-child {
        margin-right: 0;
    }
`

export const NavBar = styled.div`
    display: flex;
    list-style-type: none;
    font-size: 0.875rem;
    color: ${(props) => props.theme.colors.getGrayColor(1)};
    align-items: center;
    margin: 0;
    padding: 0;
    & a {
        margin-right: 0.7rem;
    }
    & a:last-child {
        margin-right: 0;
    }
`
export const NavItem = styled.a`
    text-decoration: none;
    cursor: pointer;
    box-sizing: border-box;
    white-space: nowrap;
    padding: 0.2rem 0;
    height: 1.5rem;
    align-items: center;
    display: flex;
    :hover {
        color: ${(props) => props.theme.colors.getGreenColor(1)};
    }
    & > svg {
        max-width: 1.4rem;
        margin-right: 0.2rem;
    }
`
export const UserImage = styled.img.attrs({
    src: `${process.env.BASE_PATH}/images/user.svg`
})`
    height: 100%;
    width: auto;
`

export const IconContainer = styled.div`
    max-width: 2rem;
`

export const ComboboxCityItem = styled.div`
    color: ${(props) => props.theme.colors.getGrayColor(0.5)};
    :hover {
        color: ${(props) => props.theme.colors.accentGreen};
    }
`
export const CityContainer = styled.div`
    display: flex;
    flex-wrap: nowrap;
`

export const SelectorRegion = styled.div`
    display: flex;
    flex-direction: column;
`
export const SelectorRegionTitle = styled.h3`
    font-size: 1rem;
    color: ${(props) => props.theme.colors.getGrayColor(0.5)};
`
