import React from 'react'
import PropTypes from 'prop-types'
import * as S from './ModalWindow.styled'
import * as Theme from '../../../styles/theme-const'
import Cross from '../../Icons/Cross'
import Button from '../Button/Button'
import ButtonTransparent from '../ButtonTransparent/ButtonTransparent'

const ModalWindow = (props) => (
    <S.AbsoluteBlock isMobile={props.isMobile}>
        <S.Container isMobile={props.isMobile}>
            <S.Header isMobile={props.isMobile}>
                <S.HeaderTitle>{props.title}</S.HeaderTitle>
                <S.ButtonClose onClick={props.cancel.clickHandler}>
                    <Cross />
                </S.ButtonClose>
            </S.Header>
            <S.Body>{props.children}</S.Body>
            <S.Footer
                isHidden={!props.isButtonFooter && !props.isFooterOnlyOneButton}
                isMobile={props.isMobile}>
                {props.footer ? props.footer : <React.Fragment />}
                {props.isMobile && props.isButtonFooter ? (
                    <React.Fragment>
                        <Button
                            elementSizeTypeValue={Theme.ElementSizeType.regular}
                            clickHandler={props.submit.clickHandler}>
                            {props.submit.text}
                        </Button>
                        <Button
                            elementSizeTypeValue={Theme.ElementSizeType.regular}
                            clickHandler={props.cancel.clickHandler}>
                            {props.cancel.text}
                        </Button>
                    </React.Fragment>
                ) : (
                    <React.Fragment />
                )}
                {!props.isMobile && props.isButtonFooter && !props.footer ? (
                    <React.Fragment>
                        <ButtonTransparent clickHandler={props.cancel.clickHandler}>
                            {props.cancel.text}
                        </ButtonTransparent>
                        <Button
                            elementSizeTypeValue={Theme.ElementSizeType.regular}
                            clickHandler={props.submit.clickHandler}>
                            {props.submit.text}
                        </Button>
                    </React.Fragment>
                ) : (
                    <React.Fragment />
                )}
                {props.isFooterOnlyOneButton && !props.footer ? (
                    <React.Fragment>
                        <Button
                            elementSizeTypeValue={Theme.ElementSizeType.regular}
                            clickHandler={props.submit.clickHandler}>
                            {props.submit.text}
                        </Button>
                    </React.Fragment>
                ) : (
                    <React.Fragment />
                )}
            </S.Footer>
        </S.Container>
    </S.AbsoluteBlock>
)

const buttonPropTypes = PropTypes.shape({
    text: PropTypes.string.isRequired,
    clickHandler: PropTypes.func.isRequired
})

ModalWindow.propTypes = {
    title: PropTypes.string,
    children: PropTypes.any.isRequired,
    isButtonFooter: PropTypes.bool.isRequired,
    isFooterOnlyOneButton: PropTypes.bool,
    isCustomFooter: PropTypes.bool.isRequired,
    footer: PropTypes.element,
    submit: buttonPropTypes,
    cancel: buttonPropTypes,
    isMobile: PropTypes.bool
}

ModalWindow.defaultProps = {
    isButtonFooter: true,
    isFooterOnlyOneButton: false,
    isCustomFooter: false,
    isMobile: false
}
export default ModalWindow
