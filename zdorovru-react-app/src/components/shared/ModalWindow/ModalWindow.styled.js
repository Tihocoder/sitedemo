import styled, { css } from 'styled-components'

export const Container = styled.div`
    @media only screen and (min-width: 960px) {
        min-width: 25rem;
    }
    background: #fff;
    border-radius: 0.3rem;
    //box-shadow: 0 19px 38px rgba(0, 0, 0, 0.3), 0 15px 12px rgba(0, 0, 0, 0.22);
    padding: ${(props) => (props.isMobile ? '0.9rem' : '2rem')};
`

export const AbsoluteBlock = styled.div`
    height: 100%;
    width: 100%;
    position: fixed;
    left: 0;
    top: 0;
    display: flex;
    justify-content: center;
    align-items: center;
    z-index: 62;
    box-sizing: border-box;
    @media only screen and (max-width: 960px) {
        padding: 1rem;
    }

    ${(props) =>
        props.isMobile
            ? css`
                  background-color: ${(props) => props.theme.colors.getGrayColor(0.4)};

                  ${Container} {
                      box-shadow: none;
                      margin-bottom: 3rem;
                  }
              `
            : css`
                  ${Container} {
                      box-shadow: 0 19px 38px rgba(0, 0, 0, 0.3), 0 15px 12px rgba(0, 0, 0, 0.22);
                  }
              `}
`

export const HeaderTitle = styled.h1`
    font-size: 1.1rem;
`

export const ButtonClose = styled.button`
    box-shadow: 0px;
    outline: none;
    margin: 0;
    background-color: white;
    height: 100%;
    border: none;
    cursor: pointer;
    width: 2.6rem;

    & svg {
        fill: ${(props) => props.theme.colors.blackTr};
    }

    &:hover > svg {
        fill: ${(props) => props.theme.colors.black};
    }
`

export const Header = styled.header`
    height: 3rem;
    display: flex;
    justify-content: space-between;
    align-items: center;
    ${(props) =>
        props.isMobile &&
        css`
            margin-bottom: 0.5rem;

            ${HeaderTitle} {
                width: 100%;
                font-size: 1.25rem;
                text-align: center;
            }

            ${ButtonClose} {
                display: none;
            }
        `}
`

export const Body = styled.div`
    min-height: 5rem;
    @media only screen and (max-width: 960px) {
        min-height: 3rem;
    }
`
export const Footer = styled.footer`
    display: flex;
    font-size: 1rem;
    justify-content: flex-end;

    ${(props) =>
        props.isMobile
            ? css`
                  flex-direction: column;

                  & button {
                      margin-top: 0.5rem;
                      padding: 0.8rem 1.2rem;
                  }
              `
            : css`
                  & > * {
                      margin-left: 1rem;
                  }
              `}
`
