import React from 'react'
import PropTypes from 'prop-types'
import * as S from './CheckBox.styled'
import Checkmark from '../../Icons/Checkmark'

const CheckBox = ({ clickHandler, checked, children }) => (
    <S.Container onClick={clickHandler}>
        <S.CheckboxContainer>
            <S.DrawnCheckbox checked={checked}>
                <Checkmark />
            </S.DrawnCheckbox>
        </S.CheckboxContainer>
        <S.Content>{children}</S.Content>
    </S.Container>
)

CheckBox.propTypes = {
    children: PropTypes.any.isRequired,
    checked: PropTypes.bool,
    clickHandler: PropTypes.func
}

export default CheckBox
