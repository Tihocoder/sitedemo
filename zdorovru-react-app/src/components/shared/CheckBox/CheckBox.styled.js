import styled from 'styled-components'
import * as Theme from '../../../styles/theme-const'

export const Container = styled.div`
    display: flex;
    cursor: pointer;
    padding: 0.025rem;

    & > div:last-child {
        margin-left: 0.6rem;
    }
    min-height: fit-content;
`

export const CheckboxContainer = styled.div`
    display: inline-block;
    vertical-align: middle;
`

export const HiddenCheckbox = styled.input.attrs({ type: 'checkbox' })`
    border: 0;
    height: 0px;
    margin: -1px;
    overflow: hidden;
    padding: 0;
    position: absolute;
    white-space: nowrap;
    width: 1px;
`

export const DrawnCheckbox = styled.div`
    display: inline-block;
    height: 1.1rem;
    width: 1.1rem;
    background-color: inherit;
    border: 1px solid ${(props) => props.theme.colors.getGrayColor(0.2)};
    border-radius: 0.3rem;
    transition: all 150ms;

    ${HiddenCheckbox}:focus + & {
        box-shadow: 0 0 0 3px pink;
    }

    & > svg {
        visibility: ${(props) => (props.checked ? 'visible' : 'hidden')};
    }
`

export const Content = styled.div`
    color: ${(props) => props.theme.colors.getGrayColor(0.6)};
    font-size: inherit;
`
