import styled, { css } from 'styled-components'

export const AbsoluteBlock = styled.div`
    height: 100%;
    width: 100%;
    position: fixed;
    left: 0;
    top: 0;
    display: flex;
    justify-content: center;
    align-items: center;
    z-index: 46;
`

export const Container = styled.div`
    padding: 1.25rem;
    min-width: 5rem;
    ${(props) =>
        props.maxWidth &&
        css`
            max-width: ${props.maxWidth};
        `}
    background: #fff;
    border-radius: 0.3rem;
    box-shadow: 0 19px 38px rgba(0, 0, 0, 0.3), 0 15px 12px rgba(0, 0, 0, 0.22);
`

export const Header = styled.header`
    height: ${(props) => (props.isTitle ? '3rem' : '1.5rem')};
    display: flex;
    justify-content: space-between;
    align-items: center;
`

export const HeaderTitle = styled.h1`
    font-size: 1rem;
`

export const ButtonClose = styled.button`
    box-shadow: 0px;
    outline: none;
    margin: 0;
    background-color: inherit;
    border: none;
    cursor: pointer;
    width: 37px;
    & svg {
        fill: ${(props) => props.theme.colors.blackTr};
    }

    &:hover > svg {
        fill: ${(props) => props.theme.colors.black};
    }
`

export const Body = styled.div`
    padding: 1rem 0;
`
export const Footer = styled.footer`
    display: flex;
    font-size: 1rem;
    justify-content: flex-end;

    & button {
        margin-right: 1rem;
    }
    & button:last-child {
        margin-right: 0;
    }
`
