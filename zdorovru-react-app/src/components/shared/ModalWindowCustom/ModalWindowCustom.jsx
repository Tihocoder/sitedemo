import React from 'react'
import PropTypes from 'prop-types'
import * as S from './ModalWindowCustom.styled'
import Cross from '../../Icons/Cross'

const ModalWindowCustom = (props) => {
    return (
        <S.AbsoluteBlock>
            <S.Container maxWidth={props.maxWidth}>
                <S.Header isTitle={props.title ? true : false}>
                    <S.HeaderTitle>{props.title}</S.HeaderTitle>
                    <S.ButtonClose onClick={props.cancel.clickHandler}>
                        <Cross />
                    </S.ButtonClose>
                </S.Header>
                <S.Body>{props.children}</S.Body>
                <S.Footer>{props.footer ? props.footer : <React.Fragment />}</S.Footer>
            </S.Container>
        </S.AbsoluteBlock>
    )
}

const buttonPropTypes = PropTypes.shape({
    text: PropTypes.string.isRequired,
    clickHandler: PropTypes.func.isRequired
})

ModalWindowCustom.propTypes = {
    title: PropTypes.string,
    children: PropTypes.any.isRequired,
    footer: PropTypes.element,
    maxWidth: PropTypes.string,
    cancel: buttonPropTypes
}

export default ModalWindowCustom
