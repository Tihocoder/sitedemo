import React from 'react'
import PropTypes from 'prop-types'
import { YMaps, Map, Placemark } from 'react-yandex-maps'
import { useSelector } from 'react-redux'

const Maps = (props) => {
    const stocks = useSelector((state) =>
        state.company.stocks.filter((x) => x.cityId === 1 && x.isOpened && x.isMetroLocation)
    )

    const onLoadMap = async (inst) => {
        //     var locationPromise = inst.geolocation.get(
        //       { mapStateAutoApply: true },
        //     )
        //     console.log('ymaps  ', inst)
        //     const location = await locationPromise;
        //    console.log('[geo]', location)
        // // Асинхронная обработка ответа.
        //     location.then(
        //       function(result) {
        //         // Добавление местоположения на карту.
        //         console.log('location ', result)
        //         result.geoObjects.options.set('preset', 'islands#redCircleIcon');
        //         result.geoObjects.get(0).properties.set({
        //           balloonContentBody: 'Мое местоположение'
        //         });
        //         inst.geoObjects.add(result.geoObjects);      },
        //       function(err) {
        //         console.log('Ошибка: ' + err)
        //       },
        //     )
    }

    return (
        <YMaps>
            <Map
                modules={['geolocation', 'geocode']}
                onLoad={(inst) => onLoadMap(inst)}
                defaultState={{ center: [55.751574, 37.573856], zoom: 9 }}>
                {stocks.map((stock) => (
                    <Placemark geometry={[stock.coordinateX, stock.coordinateY]} />
                ))}
            </Map>
        </YMaps>
    )
}

Maps.propTypes = {
    defaultsState: PropTypes.shape({
        center: PropTypes.array.isRequired,
        zoom: PropTypes.number.isRequired
    }).isRequired
}

Maps.defaultProps = {
    defaultsState: {
        center: [55.45, 37.67],
        zoom: 9
    }
}

export default Maps
