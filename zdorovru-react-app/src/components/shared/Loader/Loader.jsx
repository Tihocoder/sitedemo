import React from 'react'
import PropTypes from 'prop-types'
import * as S from './Loader.styled'

const Loader = (props) => {
    return (
        <S.Svg>
            <S.Circle />
        </S.Svg>
    )
}

Loader.propTypes = {}

export default Loader
