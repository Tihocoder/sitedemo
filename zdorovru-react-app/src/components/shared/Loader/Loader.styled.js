import styled, { keyframes } from 'styled-components'

const circleAnime = keyframes`
   0%,
  25% {
    stroke-dashoffset: 280;
    transform: rotate(0);
  }
  
  50%,
  75% {
    stroke-dashoffset: 75;
    transform: rotate(45deg);
  }
  
  100% {
    stroke-dashoffset: 280;
    transform: rotate(360deg);
  }
`

const spin = keyframes`
   0% {
        transform: rotateZ(0deg);
    }
    100% {
        transform: rotateZ(360deg)
    }
`

export const Svg = styled.svg.attrs((props) => ({
    viewBox: '0 0 100 100'
}))`
    position: relative;
    animation: ${spin} 2s linear infinite;
`

export const Circle = styled.circle.attrs((props) => ({
    cx: '50',
    cy: '50',
    r: '45'
}))`
    fill: none;
    stroke-width: 8;
    stroke: #6eb6ff;
    stroke-linecap: round;
    stroke-dasharray: 283;
    stroke-dashoffset: 280;
    stroke-width: 10px;
    transform-origin: 50% 50%;
    animation: ${circleAnime} 1.4s ease-in-out infinite both;
`
