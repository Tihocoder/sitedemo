import React from 'react'
import PropTypes from 'prop-types'
import * as S from './Button.styled'
import * as Theme from '../../../styles/theme-const'

const Button = (props) => {
    const clickHandler = async (e) => {
        if (props.clickHandler && !props.disabled) {
            await props.clickHandler(e)
        }
    }

    return (
        <S.ButtonElement
            onClick={clickHandler}
            elementSizeTypeValue={props.elementSizeTypeValue}
            isShadow={props.isShadow}
            isClickable={props.isClickable}
            isDisabled={props.disabled}
            textTransform={props.textTransform}
            href={props.href}
            colors={props.colors}>
            {props.children}
        </S.ButtonElement>
    )
}

Button.propTypes = {
    isClickable: PropTypes.bool.isRequired,
    elementSizeTypeValue: PropTypes.string.isRequired,
    children: PropTypes.any.isRequired,
    disabled: PropTypes.bool.isRequired,
    colors: PropTypes.shape({
        color: PropTypes.string,
        backgroundColor: PropTypes.string,
        borderColor: PropTypes.string,
        hoverBackgroundColor: PropTypes.string,
        shadowColor: PropTypes.string,
        shadowHoverColor: PropTypes.string
    }),
    isUpperRegister: PropTypes.bool,
    href: PropTypes.string,
    clickHandler: PropTypes.func,
    isShadow: PropTypes.bool.isRequired
}
Button.defaultProps = {
    isClickable: true,
    isShadow: false,
    disabled: false,
    isUpperRegister: true,
    textTransform: 'none',
    colors: Theme.successButtonColor,
    elementSizeTypeValue: Theme.ElementSizeType.large
}

export default Button
