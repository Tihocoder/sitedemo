import styled, { css } from 'styled-components'
import { ElementSizeType } from '../../../styles/theme-const'

export const ButtonElement = styled.button`
    border: 0;
    background-color: ${(props) => props.colors.backgroundColor};
    color: ${(props) => props.colors.color};
    border-radius: 0.3rem;
    font-size: inherit;
    box-sizing: border-box;

    ${(props) =>
        props.isShadow && !props.colors.borderColor
            ? css`
                  box-shadow: ${props.theme.shadows.getShadowKitColor(props.colors.shadowColor)};
              `
            : ''}
    ${(props) => {
        if (props.isClickable) {
            return css`
                cursor: pointer;
            `
        }
        return css`
            cursor: default;
        `
    }};
    ${(props) =>
        props.colors.borderColor &&
        css`
            border: 1px solid ${props.colors.borderColor};
        `};
    &:hover {
        ${(props) =>
            props.isShadow
                ? css`
                      box-shadow: ${props.theme.shadows.getShadowKitColor(props.colors.shadowHoverColor)};
                  `
                : css`
                      background-color: ${props.colors.hoverBackgroundColor};
                  `};
    }
    ${(props) => {
        if (props.elementSizeTypeValue === ElementSizeType.large) {
            return css`
                font-size: 1.2rem;
                padding: 0.9rem 1.6rem;
            `
        }
        if (props.elementSizeTypeValue === ElementSizeType.regular) {
            return css`
                padding: 0.7rem 1.2rem;
                text-transform: ${(props) => props.textTransform};
            `
        }
        if (props.elementSizeTypeValue === ElementSizeType.small) {
            return css`
                font-size: 0.8rem;
                padding: 0.7rem 1.2rem;
            `
        }
        if (props.elementSizeTypeValue === ElementSizeType.superSmall) {
            return css`
                font-size: 0.8rem;
            `
        }
    }};

    ${(props) =>
        props.isDisabled &&
        css`
            box-shadow: none;
            opacity: 0.6;
            &:hover {
                background-color: ${(props) => props.theme.colors.getGrayColor(0.1)};
            }
            // cursor: default;
        `}

    ${(props) =>
        !props.isClickable &&
        css`
            box-shadow: none;
            opacity: 0.6;
            cursor: default;
            background-color: ${(props) => props.theme.colors.getGrayColor(0.1)};
        `}
`
