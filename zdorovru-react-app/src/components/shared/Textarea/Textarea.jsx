import React from 'react'
import PropTypes from 'prop-types'
import * as S from './Textarea.styled'

const Textarea = (props) => {
    if (props.placeholderRaised) {
        return (
            <S.Container>
                <S.InputPlacholderLabel {...props} required />
                <S.Label>{props.placeholderRaised}</S.Label>
            </S.Container>
        )
    }
    return <S.TextareaBase {...props} isValid={props.isValid} />
}
Textarea.propTypes = {
    isValid: PropTypes.bool.isRequired,
    placeholderRaised: PropTypes.string,
    withotFocus: PropTypes.bool
}
Textarea.defaultProps = {
    isValid: true
}

export default Textarea
