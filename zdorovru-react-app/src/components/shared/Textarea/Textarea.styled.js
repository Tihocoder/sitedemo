import styled, { css } from 'styled-components'

const validatedInput = css`
    border: 1px solid ${(props) => props.theme.colors.getGrayColor(0.1)};
    background-color: inherit;
`
const notValidatedInput = css`
    border: 1px solid ${(props) => props.theme.colors.accentRed};
`

export const TextareaBase = styled.textarea`
    width: 100%;
    padding: 0.7rem 1rem;
    font-size: 1rem;
    border: 1px solid rgba(50, 62, 52, 0.1);
    color: ${(props) => props.theme.colors.getGrayColor(0.6)};
    box-sizing: border-box;
    outline: none;
    border-radius: 0.3rem;
    transition: all 0.35s ease;
    font: inherit;
    ${(props) => (props.isValid ? validatedInput : notValidatedInput)}
    ::placeholder {
        color: ${(props) => props.theme.colors.getGrayColor(0.4)};
    }
    &:focus {
        color: ${(props) => props.theme.colors.getGrayColor(0.6)};
        box-shadow: ${(props) => props.theme.shadows.shadowKitHover};
        border: 1px solid ${(props) => props.theme.colors.getGrayColor(0.3)};
        ::placeholder {
            transition: opacity 0.45s ease;
            color: ${(props) => props.theme.colors.getGrayColor(0.4)};
        }
    }

    resize: none;
`

export const Label = styled.label`
    position: absolute;
    pointer-events: none;
    font-size: 1rem;
    left: 1.2rem;
    top: 1.2rem;
    transition: 0.2s ease all;
    color: ${(props) => props.theme.colors.getGrayColor(0.4)};
`
export const InputPlacholderLabel = styled.textarea`
    width: 100%;
    padding: 1.1rem 1rem;
    font-size: 1rem;
    border: 1px solid ${(props) => props.theme.colors.getGrayColor(0.03)};
    color: ${(props) => props.theme.colors.getGrayColor(0.6)};
    ${(props) =>
        !props.withotFocus &&
        css`
            :focus {
                border: 1px solid ${props.theme.colors.getGrayColor(0.6)};
            }
        `}

    box-sizing: border-box;
    outline: none;
    border-radius: 0.3rem;
    transition: all 0.35s ease;
    font: inherit;
    height: 6rem;
    ${(props) => (props.isValid ? validatedInput : notValidatedInput)}
    resize: none;
`

export const Container = styled.div`
    position: relative;
    & > textarea:focus ~ label,
    textarea:not(:focus):valid ~ label {
        top: 0.1rem;
        bottom: 0.5rem;
        left: 1rem;
        font-size: 0.75rem;
        opacity: 1;
    }
`
