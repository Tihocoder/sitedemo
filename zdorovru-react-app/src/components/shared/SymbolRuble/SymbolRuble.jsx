import React from 'react'
import PropTypes from 'prop-types'
import { Span } from './SymbolRuble.styled'

const SymbolRuble = (props) => {
    return <Span>₽</Span>
}

export default SymbolRuble
