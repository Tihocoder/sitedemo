import styled from 'styled-components'

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    font-size: 0.875rem;
    min-width: 11rem;
    & input {
        padding: 0.5rem 0.8rem;
    }
`

export const Title = styled.h3`
    font-size: 0.9rem;
    font-weight: normal;
    color: ${(props) => props.theme.colors.getGrayColor(0.9)};
`

export const FilterList = styled.div`
    display: flex;
    flex-direction: column;
    padding: 0.8rem 0;
    overflow-x: auto;
    max-height: 25rem;
`
