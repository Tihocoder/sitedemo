import React, { useState } from 'react'
import PropTypes from 'prop-types'
import Input from '../Input/Input'
import * as S from './Filter.styled'
import * as _ from 'lodash'
import CheckBoxGroup from '../CheckBoxGroup/CheckBoxGroup'
import CheckBox from '../CheckBox/CheckBox'

const Filter = (props) => {
    const [searchText, setSearchText] = useState('')

    const changeSearchText = (e) => {
        setSearchText(e.target.value)
    }

    if (!props.items || props.items.length === 0) {
        return <React.Fragment />
    }

    const getItemsSearch = () => {
        if (searchText) {
            return props.items.filter((x) => x.title.toLowerCase().includes(searchText.toLowerCase()))
        }
        return props.items
    }

    return (
        <S.Container>
            <S.Title>{props.title}</S.Title>
            {props.items.length > 7 && <Input value={searchText} onChange={changeSearchText} type="search" />}
            <S.FilterList>
                <CheckBoxGroup
                    checkedItems={props.selectedItems}
                    changeItemsHandler={props.changeSelectedItemsHandler}>
                    {_.sortBy(_.uniqBy(getItemsSearch(), 'id'), (x) => x.title).map((item) => (
                        <CheckBox key={item.id} id={item.title}>
                            {item.title}
                        </CheckBox>
                    ))}
                </CheckBoxGroup>
            </S.FilterList>
        </S.Container>
    )
}

Filter.propTypes = {
    title: PropTypes.string.isRequired,
    items: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.string.isRequired,
            title: PropTypes.string.isRequired
        })
    ),
    selectedItems: PropTypes.array.isRequired,
    changeSelectedItemsHandler: PropTypes.func.isRequired
}

export default Filter
