import styled, { css } from 'styled-components'

export const Container = styled.div`
    display: flex;
    height: 100%;
    border-radius: 0.3rem;
    width: 100%;

    & > * {
        padding: 0.3rem;
        height: 100%;
        -webkit-box-sizing: border-box;
        display: flex;
        align-items: center;
    }
`

export const Input = styled.input.attrs({
    type: 'number'
})`
    box-shadow: 0px;
    outline: none;
    margin: 0;
    border: none;
    width: 40%;
    /* border-top: 2px solid ${(props) => props.theme.colors.gray};
    border-bottom: 2px solid ${(props) => props.theme.colors.gray}; */
    text-align: center;
    font-size: inherit;
    padding: 0.5rem 1rem;
    appearance: none;
    -webkit-appearance: none;

    &::-webkit-outer-spin-button,
    &::-webkit-inner-spin-button {
        -webkit-appearance: none;
    }
`
export const ButtonLeft = styled.button`
    box-shadow: 0px;
    outline: none;
    margin: 0;
    width: 30%;
    background-color: ${(props) => props.theme.colors.getGrayColor(0.1)};

    &:hover {
        background-color: ${(props) => props.theme.colors.getGrayColor(0.08)};
    }

    border: none;
    border-top-left-radius: 0.3rem;
    border-bottom-left-radius: 0.3rem;
    cursor: pointer;
`
export const ButtonRight = styled.button`
    box-shadow: 0px;
    outline: none;
    margin: 0;
    width: 30%;
    border: none;
    background-color: ${(props) => props.theme.colors.accentGreen};

    &:hover {
        background-color: ${(props) => props.theme.colors.accentGreenLight};
    }

    border-top-right-radius: 0.3rem;
    border-bottom-right-radius: 0.3rem;
    cursor: pointer;
`

export const IconImg = styled.img`
  width: 100%;
  height: auto;
`
