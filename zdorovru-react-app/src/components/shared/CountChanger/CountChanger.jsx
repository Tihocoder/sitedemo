import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import * as S from './CountChanger.styled'
import Minus from '../../Icons/Minus'
import Plus from '../../Icons/Plus'

const CountChanger = (props) => {
    const changeCount = (value) => {
        const valueInt = parseInt(value, 10)
        if (valueInt >= 0) {
            props.changeValueHandler(valueInt)
        }
    }

    const changeInputHandler = (event) => {
        changeCount(event.target.value)
    }

    const incrementHandler = () => {
        changeCount(props.value + 1)
    }
    const decrementHandler = () => {
        changeCount(props.value - 1)
    }
    return (
        <S.Container>
            <S.ButtonLeft onClick={decrementHandler}>
                <S.IconImg src={`${process.env.BASE_PATH}/images/minus.svg`} />
            </S.ButtonLeft>
            <S.Input value={props.value} onChange={changeInputHandler} />
            <S.ButtonRight onClick={incrementHandler}>
                <S.IconImg src={`${process.env.BASE_PATH}/images/plus.svg`} />
            </S.ButtonRight>
        </S.Container>
    )
}

CountChanger.propTypes = {
    value: PropTypes.number,
    changeValueHandler: PropTypes.func.isRequired
}

export default CountChanger
