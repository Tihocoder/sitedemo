import styled from 'styled-components'

export const Container = styled.div`
    display: flex;
    height: 2rem;
    width: 100%;
    max-width: 21rem;
    box-sizing: content-box;
    margin: 0.6rem 0;
    & > button {
        box-sizing: border-box;
        margin: 0 .1rem;
        height: 100%;
        width: 2rem;
    }
`
