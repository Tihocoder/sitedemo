import React from 'react'
import PropTypes from 'prop-types'
import * as S from './Paginator.styled'
import * as Theme from '../../../styles/theme-const'
import Button from '../Button/Button'
import ArrowLeft from '../../Icons/ArrowLeft'
import ArrowRight from '../../Icons/ArrowRight'
import ThreeDots from '../../Icons/ThreeDots'

const navType = {
    default: 1,
    separator: 2,
    left: 3,
    right: 4
}
const colorsButtonDefault = {
    color: Theme.ColorConsts.getGrayColor(0.6),
    backgroundColor: 'inherit',
    hoverBackgroundColor: Theme.ColorConsts.getGrayColor(0.12)
}
const colorsButtonSelect = {
    color: Theme.ColorConsts.white,
    backgroundColor: Theme.ColorConsts.accentGreen,
    // borderColor: Theme.ColorConsts.getGrayColor(0.6),
    hoverBackgroundColor: Theme.ColorConsts.accentGreenLight
}

const Paginator = (props) => {
    if (props.pageCount <= 1) return <S.Container></S.Container>
    const selectHandler = (type, value) => () => {
        if (type === navType.left && props.currentPage > 1) {
            props.selectPageHandler(props.currentPage - 1)
            return
        }
        if (type === navType.right && props.currentPage < props.pageCount) {
            props.selectPageHandler(props.currentPage + 1)
            return
        }
        if (type === navType.default) {
            props.selectPageHandler(value)
            return
        }
    }
    const getPageButtons = () => {
        let pageButtons = []
        const limitLastPages = props.pageCount - Theme.PagintaionMaxCount + 3

        pageButtons.push({
            type: navType.left,
            key: 'left',
            value: <ArrowLeft />,
            colors: colorsButtonDefault,
            selectHandler: selectHandler(navType.left),
            isClickable: true
        })

        const separatorButton = {
            type: navType.separator,
            key: 'separator',
            value: <ThreeDots />,
            colors: colorsButtonDefault,
            selectHandler: selectHandler(navType.separator),
            isClickable: false
        }
        {
            /* Если количество страниц меньше чем максимальное количество кнопок в пагинаторе*/
        }
        if (props.pageCount < Theme.PagintaionMaxCount) {
            for (let pageNumber = 1; pageNumber <= props.pageCount; pageNumber++) {
                pageButtons.push({
                    type: navType.default,
                    key: pageNumber,
                    value: pageNumber,
                    colors: props.currentPage === pageNumber ? colorsButtonSelect : colorsButtonDefault,
                    selectHandler: selectHandler(navType.default, pageNumber),
                    isClickable: true
                })
            }
        } else if (
            props.pageCount >= Theme.PagintaionMaxCount &&
            props.currentPage < Theme.PagintaionMaxCount - 2
        ) {
            {
                /* Если выбранная страница в начале */
            }
            for (let pageNumber = 1; pageNumber <= Theme.PagintaionMaxCount - 2; pageNumber++) {
                pageButtons.push({
                    type: navType.default,
                    key: pageNumber,
                    value: pageNumber,
                    colors: props.currentPage === pageNumber ? colorsButtonSelect : colorsButtonDefault,
                    selectHandler: selectHandler(navType.default, pageNumber),
                    isClickable: true
                })
            }
            pageButtons.push(separatorButton)
            pageButtons.push({
                type: navType.default,
                key: props.pageCount,
                value: props.pageCount,
                colors: colorsButtonDefault,
                selectHandler: selectHandler(navType.default, props.pageCount),
                isClickable: true
            })
        } else if (props.pageCount >= Theme.PagintaionMaxCount && props.currentPage > limitLastPages) {
            {
                /* Если выбранная страница в конце */
            }
            pageButtons.push({
                type: navType.default,
                key: 1,
                value: 1,
                colors: colorsButtonDefault,
                selectHandler: selectHandler(navType.default, 1),
                isClickable: true
            })
            pageButtons.push(separatorButton)

            for (let pageNumber = limitLastPages; pageNumber <= props.pageCount; pageNumber++) {
                pageButtons.push({
                    type: navType.default,
                    key: pageNumber,
                    value: pageNumber,
                    colors: props.currentPage === pageNumber ? colorsButtonSelect : colorsButtonDefault,
                    selectHandler: selectHandler(navType.default, pageNumber),
                    isClickable: true
                })
            }
        } else if (props.pageCount > Theme.PagintaionMaxCount && props.currentPage <= limitLastPages) {
            {
                /* Если выбранная страница в середине */
            }
            pageButtons.push({
                type: navType.default,
                key: 1,
                value: 1,
                colors: colorsButtonDefault,
                selectHandler: selectHandler(navType.default, 1),
                isClickable: true
            })
            pageButtons.push(separatorButton)
            pageButtons.push({
                type: navType.default,
                key: props.currentPage - 1,
                value: props.currentPage - 1,
                colors: colorsButtonDefault,
                selectHandler: selectHandler(navType.default, props.currentPage - 1),
                isClickable: true
            })
            pageButtons.push({
                type: navType.default,
                key: props.currentPage,
                value: props.currentPage,
                colors: colorsButtonSelect,
                selectHandler: selectHandler(navType.default, props.currentPage),
                isClickable: true
            })
            pageButtons.push({
                type: navType.default,
                key: props.currentPage + 1,
                value: props.currentPage + 1,
                colors: colorsButtonDefault,
                selectHandler: selectHandler(navType.default, props.currentPage + 1),
                isClickable: true
            })
            {
                /* Это нужно для уникальности компонентов */
            }
            let separator2 = { ...separatorButton, key: 'separator2' }

            pageButtons.push(separator2)
            pageButtons.push({
                type: navType.default,
                key: props.pageCount,
                value: props.pageCount,
                colors: colorsButtonDefault,
                selectHandler: selectHandler(navType.default, props.pageCount),
                isClickable: true
            })
        }
        pageButtons.push({
            type: navType.right,
            key: 'right',
            value: <ArrowRight />,
            colors: colorsButtonDefault,
            selectHandler: selectHandler(navType.right),
            isClickable: true
        })
        return pageButtons
    }

    if (props.pageCount <= 1) return <S.Container></S.Container>
    return (
        <S.Container>
            {getPageButtons().map((x) => (
                <Button
                    key={x.key}
                    elementSizeTypeValue={Theme.ElementSizeType.superSmall}
                    colors={x.colors}
                    isClickable={x.isClickable}
                    clickHandler={x.selectHandler}>
                    {x.value}
                </Button>
            ))}
        </S.Container>
    )
}

Paginator.propTypes = {
    pageCount: PropTypes.number.isRequired,
    currentPage: PropTypes.number.isRequired,
    selectPageHandler: PropTypes.func.isRequired
}

export default Paginator
