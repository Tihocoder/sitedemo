import styled, { css } from 'styled-components'

export const Container = styled.div`
    display: flex;
    ${(props) =>
        props.isColumn &&
        css`
            flex-direction: column;
        `}
`
