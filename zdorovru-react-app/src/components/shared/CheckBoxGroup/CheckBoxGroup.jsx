import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import * as S from './CheckBoxGroup.styled'

const CheckBoxGroup = (props) => {
    const [checkedItems, setCheckedItems] = useState([])

    const getCheckedState = (idItem) => {
        return props.checkedItems.some((item) => item === idItem)
    }

    const changeCheckedState = (idItem) => () => {
        const checked = props.checkedItems.some((item) => item === idItem)
        let newCheckedItems = [...props.checkedItems]

        if (checked) {
            newCheckedItems = [...props.checkedItems.filter((item) => item !== idItem)]
        } else {
            newCheckedItems.push(idItem)
        }
        props.changeItemsHandler(newCheckedItems)
    }

    const checkBoxGroup = []

    let children = null
    if (props.children) {
        children = React.Children.toArray(props.children)
        React.Children.forEach(children, (checkBox, index) => {
            if (React.isValidElement(checkBox)) {
                const checkBoxNode = React.cloneElement(checkBox, {
                    ...checkBox.props,
                    key: checkBox.props.id,
                    checked:
                        checkBox.props.checked === undefined
                            ? getCheckedState(checkBox.props.id)
                            : checkbox.props.checked,
                    clickHandler:
                        checkBox.props.clickHandler === undefined
                            ? changeCheckedState(checkBox.props.id)
                            : checkbox.props.clickHandler,
                    ...props,
                    children: checkBox.props.children
                })
                checkBoxGroup.push(checkBoxNode)
            }
        })
    }
    return (
        <S.Container isColumn={props.isColumn}>
            {checkBoxGroup.map((Component) => React.cloneElement(Component))}
        </S.Container>
    )
}

CheckBoxGroup.propTypes = {
    checkedItems: PropTypes.array.isRequired,
    changeItemsHandler: PropTypes.func.isRequired,
    isColumn: PropTypes.bool.isRequired
}
CheckBoxGroup.defaultProps = {
    isColumn: true
}

export default CheckBoxGroup
