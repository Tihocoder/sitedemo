import React, { useState } from 'react'
import PropTypes from 'prop-types'
import * as S from './InputSearch.styled'
import Input from '../Input/Input'

const InputSearch = ({ value, changeValue, type, placeholder, items, selectHandler, custom }) => {
    const [isShowList, setIsShowList] = useState(false)

    const inputFocusHandler = () => {
        setIsShowList(true)
    }
    const inputBlurHandler = () => {
        setIsShowList(false)
    }

    const selectItemHandler = (e) => {
        if (type === 'search') {
            const id = parseInt(e.target.id, 10)
            if (id >= 0) {
                setIsShowList(false)
            }
        }
        selectHandler(e)
    }

    return (
        <S.Container>
            <Input
                value={value}
                onChange={changeValue}
                type={type}
                placeholderRaised={placeholder}
                onFocus={inputFocusHandler}
                onBlur={inputBlurHandler}
            />
            {isShowList && items && items.length > 0 && (
                <S.ListItem>
                    {items.map((item, index) => (
                        <S.Item
                            key={index}
                            id={item.id}
                            onMouseDown={(e) => e.preventDefault()}
                            onClick={selectItemHandler}>
                            {item.text}
                        </S.Item>
                    ))}
                </S.ListItem>
            )}
        </S.Container>
    )
}

InputSearch.propTypes = {
    value: PropTypes.string.isRequired,
    changeValue: PropTypes.func.isRequired,
    type: PropTypes.string.isRequired,
    placeholder: PropTypes.string.isRequired,
    selectHandler: PropTypes.func.isRequired,
    custom: PropTypes.string,
    items: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.number.isRequired,
            text: PropTypes.string.isRequired,
            additional: PropTypes.any.isRequired
        })
    )
}

InputSearch.defaultProps = {
    type: 'search',
    custom: 'address'
}

export default InputSearch
