import styled from 'styled-components'

export const Container = styled.div`
    position: relative;
    display: flex;
    flex-direction: column;
    width: 100%;
`

export const Input = styled.input`
    border: 0;
    background-color: inherit;
    box-shadow: ${(props) => props.theme.shadows.shadowKit};
    width: 100%;
    padding: 0.7rem 1rem;
    -webkit-appearance: none;
    color: ${(props) => props.theme.colors.getGrayColor(0.7)};
    outline: none;
    border-radius: 7px;
    transition: all 0.2s ease;
    font: inherit;
    &:focus {
        box-shadow: ${(props) => props.theme.shadows.shadowKitHover};
        color: ${(props) => props.theme.colors.getGrayColor(0.8)};
        &::placeholder {
            transition: opacity 0.45s ease;
            opacity: 0.3;
        }
    }
`

export const ListItem = styled.div`
    position: absolute;
    width: 100%;
    background-color: white;
    margin-top: 3.8rem;
    box-sizing: border-box;
    -webkit-appearance: none;
    border-radius: 0.3rem;
    box-shadow: ${(props) => props.theme.shadows.shadowKit};
    overflow: auto;
    max-height: 20rem;
    & > div {
        border-bottom: 1px solid ${(props) => props.theme.colors.getGrayColor(0.05)};
    }
    & > div:last-child {
        border-bottom: 0;
    }
`

export const Item = styled.div`
    padding: 0.6rem 1rem;
    cursor: pointer;
`
