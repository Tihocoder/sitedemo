import React from 'react'
import { CountBlock } from '@shared/NotificationCount/NotificationCount.styled'
import PropTypes from 'prop-types'

const NotificationCount = (props) => <CountBlock>{props.children}</CountBlock>

NotificationCount.propTypes = {
    children: PropTypes.number.isRequired
}

export default NotificationCount
