import styled from 'styled-components'

export const CountBlock = styled.div`
    color: white;
    background-color: ${(props) => props.theme.colors.getRedheadColor(1)};
    border-radius: 3rem;
    position: absolute;
`
