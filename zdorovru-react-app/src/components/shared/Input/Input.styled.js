import styled, { css } from 'styled-components'

export const InputBase = styled.input`
    width: 100%;
    padding: 0.7rem 1rem;
    border: 1px solid ${(props) => props.theme.colors.accentGreen};
    color: ${(props) => props.theme.colors.getGrayColor(1)};
    box-sizing: border-box;
    outline: none;
    -webkit-appearance: none;
    border-radius: 7px;
    transition: all 0.35s ease;
    font: inherit;
    ${(props) => (props.isValid ? validatedInput : notValidatedInput)}
    ::placeholder {
        color: ${(props) => props.theme.colors.getGrayColor(0.4)};
    }
    &:focus {
        color: ${(props) => props.theme.colors.getGrayColor(1)};
        box-shadow: ${(props) => props.theme.shadows.shadowKitHover};
        ::placeholder {
            transition: opacity 0.45s ease;
            color: ${(props) => props.theme.colors.getGrayColor(0.4)};
        }
    }
`
export const Label = styled.label`
    position: absolute;
    pointer-events: none;
    font-size: 1rem;
    font-weight: normal;
    left: 1.2rem;
    top: 0.875rem;
    transition: 0.2s ease all;
    color: ${(props) => props.theme.colors.getGrayColor(0.4)};
`
export const InputPlacholderLabel = styled.input`
    width: 100%;
    padding: 1.1rem 0.844rem 0.544rem 0.844rem;
    border: 1px solid ${(props) => props.theme.colors.accentGreen};
    color: ${(props) => props.theme.colors.getGrayColor(1)};
    box-sizing: border-box;
    outline: none;
    border-radius: 7px;
    -webkit-appearance: none;
    transition: all 0.35s ease;
    font: inherit;
    ${(props) =>
        props.borderColor
            ? props.isValid
                ? css`
                      border-color: ${props.borderColor};
                  `
                : notValidatedInput
            : props.isValid
            ? validatedInput
            : notValidatedInput};
`

export const Container = styled.div`
    position: relative;
    display: flex;
    align-items: center;
    & > input:focus ~ label,
    input:not(:focus):valid ~ label {
        top: 0.1rem;
        bottom: 0.5rem;
        left: 1rem;
        font-size: 0.75rem;
        opacity: 1;
        height: 0.9rem;
    }
`

const validatedInput = css`
    border: 1px solid ${(props) => props.theme.colors.accentGreen};
    :focus {
        border: 1px solid ${(props) => props.theme.colors.getGreenColor(0.8)};
    }
    background-color: inherit;
`
const notValidatedInput = css`
    border: 1px solid ${(props) => props.theme.colors.accentRed};
    :focus {
        border: 1px solid ${(props) => props.theme.colors.accentRed};
    }
`
