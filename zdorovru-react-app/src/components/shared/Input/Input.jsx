import React from 'react'
import PropTypes from 'prop-types'
import * as S from './Input.styled'

const Input = (props) => {
    if (props.placeholderRaised) {
        return (
            <S.Container>
                <S.InputPlacholderLabel {...props} ref={props.inputRef} required />
                <S.Label>{props.placeholderRaised}</S.Label>
            </S.Container>
        )
    }
    return <S.InputBase {...props} />
}

Input.propTypes = {
    isValid: PropTypes.bool.isRequired,
    placeholderRaised: PropTypes.string,
    borderColor: PropTypes.string
}
Input.defaultProps = {
    isValid: true
}

export default Input
