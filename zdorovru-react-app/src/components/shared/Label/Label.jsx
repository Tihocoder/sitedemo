import React from 'react'
import PropTypes from 'prop-types'
import * as S from './Label.styled'

const Label = (props) => {
    return (
        <S.Container>
            <S.Label color={props.color}>{props.children}</S.Label>
        </S.Container>
    )
}

Label.propTypes = {
    children: PropTypes.string.isRequired,
    color: PropTypes.string
}

export default Label
