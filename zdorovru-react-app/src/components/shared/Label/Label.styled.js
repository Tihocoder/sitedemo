import styled from 'styled-components'
import * as SC from '../../../styles/theme-const'

export const Container = styled.div`
    padding: 0.02rem;
    font-size: inherit;
`

export const Label = styled.span`
    color: ${(props) => props.color || SC.ColorConsts.textColor};
`
