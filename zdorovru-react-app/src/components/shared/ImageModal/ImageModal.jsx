import React, { useEffect, useRef } from 'react'
import PropTypes from 'prop-types'
import * as S from './ImageModal.styled'
import Cross from '../../Icons/Cross'

const useOutsideAlerter = (ref, closeHandler) => {
    useEffect(() => {
        /**
         * Alert if clicked on outside of element
         */
        const handleClickOutside = (event) => {
            if (ref.current && !ref.current.contains(event.target)) {
                closeHandler()
            }
        }
        // Bind the event listener
        document.addEventListener('mousedown', handleClickOutside)
        return () => {
            // Unbind the event listener on clean up
            document.removeEventListener('mousedown', handleClickOutside)
        }
    }, [ref])
}

const ImageModal = (props) => {
    const wrapperRef = useRef(null)
    useOutsideAlerter(wrapperRef, props.closeHandler)

    return (
        <S.AbsoluteBlock>
            <S.Container ref={wrapperRef} imageUrl={props.imageUrl}>
                <S.Header>
                    <S.ButtonClose onClick={props.closeHandler}>
                        <Cross />
                    </S.ButtonClose>
                </S.Header>
                <S.Img src={props.imageUrl} />
            </S.Container>
        </S.AbsoluteBlock>
    )
}

ImageModal.propTypes = {
    imageUrl: PropTypes.string.isRequired,
    closeHandler: PropTypes.func.isRequired
}

export default ImageModal
