import styled from 'styled-components'

export const AbsoluteBlock = styled.div`
    height: 100%;
    width: 100%;
    position: fixed;
    left: 0;
    top: 0;
    display: flex;
    justify-content: center;
    align-items: center;
    z-index: 10;
`

export const Container = styled.div`
    border-radius: 0.3rem;
    box-shadow: 0 5px 13px -7px rgba(0, 0, 0, 0.4);
    background-color: white;
    background-image: url(${(props) => props.imageUrl});
    background-size: contain;
    background-repeat: no-repeat;
    position: relative;
    height: 27rem;
    width: 27rem;
`
export const Img = styled.div`
    height: 100%;
    width: auto;
`
export const Header = styled.header`
    height: 3rem;
    display: flex;
    justify-content: flex-end;
    align-items: center;
    margin-bottom: 2rem;
`


export const ButtonClose = styled.button`
    box-shadow: 0px;
    outline: none;
    margin: 0;
    background-color: inherit;
    border: none;
    cursor: pointer;
    width: 2.6rem;
    & svg {
        fill: ${(props) => props.theme.colors.blackTr};
    }

    &:hover > svg {
        fill: ${(props) => props.theme.colors.black};
    }
`
