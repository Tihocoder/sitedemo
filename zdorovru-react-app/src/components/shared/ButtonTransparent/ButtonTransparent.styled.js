import styled, { css } from 'styled-components'
import { ElementSizeType } from '../../../styles/theme-const'

export const ButtonElement = styled.a`
    border: 0;
    background-color: inherit;
    color: ${(props) => props.colors.color};
    border-radius: 0.3rem;
    transition: color 0.2s ease;
    font-size: inherit;
    display: flex;
    align-items: center;
    ${(props) => {
        if (props.isClickable) {
            return css`
                cursor: pointer;
            `
        }
        return css`
            cursor: default;
        `
    }};
    &:hover {
        color: ${(props) => props.colors.hoverColor};
    }
    ${(props) =>
        props.options.isBold
            ? css`
                  font-weight: bold;
              `
            : ''}

    ${(props) => {
        if (props.elementSizeTypeValue === ElementSizeType.large) {
            return css`
                font-size: 1.2rem;
                padding: 0.5rem 0.8rem;
            `
        }
        if (props.elementSizeTypeValue === ElementSizeType.regular) {
            return css`
                padding: 0.5rem 0;
            `
        }
        if (props.elementSizeTypeValue === ElementSizeType.small) {
            return css`
                font-size: 1.1rem;
                padding: 0.2rem 0.2rem;
            `
        }
    }};
`
