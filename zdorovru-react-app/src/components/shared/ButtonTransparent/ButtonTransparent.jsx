import React from 'react'
import PropTypes from 'prop-types'
import * as S from './ButtonTransparent.styled'
import * as Theme from '../../../styles/theme-const'

const ButtonTransparent = (props) => {
    return (
        <S.ButtonElement
            onClick={props.clickHandler}
            
            elementSizeTypeValue={props.elementSizeTypeValue}
            isClickable={props.isClickable}
            options={props.options}
            colors={props.colors}>
            {props.children}
        </S.ButtonElement>
    )
}

ButtonTransparent.propTypes = {
    isClickable: PropTypes.bool.isRequired,
    elementSizeTypeValue: PropTypes.string.isRequired,
    children: PropTypes.any.isRequired,
    colors: PropTypes.shape({
        color: PropTypes.string.isRequired,
        hoverColor: PropTypes.string.isRequired
    }).isRequired,
    options: PropTypes.shape({
        isBold: PropTypes.bool.isRequired
    }),
    clickHandler: PropTypes.func.isRequired
}
ButtonTransparent.defaultProps = {
    isClickable: true,
    colors: {
        color: Theme.ColorConsts.accentGreen,
        hoverColor: Theme.ColorConsts.accentGreenLight
    },
    options: {
        isBold: true
    },
    elementSizeTypeValue: Theme.ElementSizeType.regular
}

export default ButtonTransparent
