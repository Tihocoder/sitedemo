import React from 'react'
import PropTypes from 'prop-types'
import Link from 'next/link'
import * as S from './AccentLink.styled'

const AccentLink = ({ link, children }) => (
    <Link href={link} passHref>
        <S.A>{children}</S.A>
    </Link>
)

AccentLink.propTypes = {
    link: PropTypes.string.isRequired,
    children: PropTypes.string.isRequired
}
