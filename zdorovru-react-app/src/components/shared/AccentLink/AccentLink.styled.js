import styled from 'styled-components'

export const A = styled.a`
    border-bottom: 1px dashed #c1e3e4;
    text-decoration: none;
    cursor: pointer;
    color: ${(props) => props.theme.colors.getGreenColor(1)};
`
