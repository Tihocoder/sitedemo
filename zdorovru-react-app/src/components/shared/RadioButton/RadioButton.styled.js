import styled, { css } from 'styled-components'

export const Container = styled.div`
    display: flex;
    cursor: pointer;
    & > div:last-child {
        margin-left: 0.6rem;
    }
    align-items: center;
    margin: 0.2rem;
`

export const RadioButtonContainer = styled.div`
    ::before {
        content: '';
        background: inherit;
        border-radius: 50%;
        border: 1px solid ${(props) => props.theme.colors.accentGreen};
        display: inline-block;
        width: 1.2em;
        height: 1.2em;
        position: relative;
        vertical-align: top;
        cursor: pointer;
        text-align: center;
        transition: all 250ms ease;
        ${(props) =>
            props.checked &&
            css`
                background-color: ${(props) => props.theme.colors.accentGreen};
                box-shadow: inset 0 0 0 3px ${(props) => props.theme.colors.white};
            `}
    }
`

export const HiddenRadioButton = styled.input.attrs({ type: 'radio' })`
    border: 0;
    height: 0px;
    margin: -1px;
    overflow: hidden;
    padding: 0;
    position: absolute;
    white-space: nowrap;
    width: 1px;
    opacity: 0;
`

export const DrawnRadio = styled.div`
    display: inline-block;
    height: 1.1rem;
    width: 1.1rem;
    background-color: inherit;
    border: 1px solid ${(props) => props.theme.colors.accentGreen};
    border-radius: 50%;
    transition: all 150ms;
`

export const Content = styled.div`
    color: ${(props) => props.theme.colors.getGrayColor(0.6)};
    font-size: 1.1rem;
`
