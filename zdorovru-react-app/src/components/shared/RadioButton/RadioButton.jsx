import React from 'react'
import PropTypes from 'prop-types'
import * as S from './RadioButton.styled'

const RadioButton = ({ checked, children, clickHandler, ...props }) => {
    return (
        <S.Container onClick={clickHandler}>
            <S.RadioButtonContainer checked={checked}>
                <S.HiddenRadioButton checked={checked} {...props} onChange={clickHandler} />
            </S.RadioButtonContainer>
            <S.Content>{children}</S.Content>
        </S.Container>
    )
}

RadioButton.propTypes = {
    children: PropTypes.any.isRequired,
    checked: PropTypes.bool,
    value: PropTypes.value,
    clickHandler: PropTypes.func
}

export default RadioButton
