import styled, { css } from 'styled-components'

const invalidatedInput = css`
    border: 1px solid ${(props) => props.theme.colors.accentRed};
`
const validatedInput = css`
    border: 1px solid ${(props) => props.theme.colors.getGrayColor(0.1)};
    border-radius: 0.3rem;
    background-color: inherit;
`

export const Container = styled.div`
    display: flex;
    position: relative;
    box-sizing: border-box;
    width: 100%;
    ${(props) => props.isBorder && validatedInput}
    ${(props) => !props.isValid && invalidatedInput}

    & > svg {
        width: 1rem;
        fill: ${(props) => props.theme.colors.getGrayColor(0.3)};
        position: absolute;
        right: 1rem;
        top: 30%;
        transition: transform 0.35s ease;
        ${(props) =>
            props.isArrowDown
                ? css`
                      transform: rotate(180deg);
                  `
                : ``}
    }
`

export const SelectedPosition = styled.div`
    width: 100%;
    padding: 0.7rem 1rem;
    font-size: 1rem;
    caret-color: transparent;
    cursor: pointer;
    border: 0;
    color: ${(props) => props.theme.colors.getGrayColor(0.8)};
    box-sizing: border-box;
    outline: none;
    border-radius: 0.3rem;
    font: inherit;
    box-sizing: border-box;
    &:focus {
        transition: all 0.35s ease;
        color: ${(props) => props.theme.colors.getGrayColor(1)};
        box-shadow: 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 2px 2px 0 rgba(0, 0, 0, 0.14),
            0 1px 5px 0 rgba(0, 0, 0, 0.12);
    }
    display: flex;
    align-items: center;
`
export const ListItemBlock = styled.ul`
    list-style: none;
    position: absolute;
    padding: 0;
    margin: 0;
    width: 100%;
    background-color: ${(props) => props.theme.colors.white};
    margin-top: ${(props) => props.marginTop};
    box-sizing: border-box;
    box-shadow: ${(props) => props.theme.shadows.shadowKitHover};
    overflow: auto;
    max-height: 20rem;
    border-radius: 0.3rem;
    & > div {
        border-bottom: 1px solid ${(props) => props.theme.colors.getGrayColor(0.1)};
    }
    & > div:last-child {
        border-bottom: 0;
    }
    z-index: 15;
`
export const Item = styled.div`
    padding: 0.6rem 1rem;
    cursor: pointer;
    z-index: 15;
    :hover {
        background-color: ${(props) => props.theme.colors.getGrayColor(0.03)};
    }
`
