import React, { useState, useRef, useEffect } from 'react'
import PropTypes from 'prop-types'
import * as S from './ComboBox.styled'
import ArrowUp from '../../Icons/ArrowUp'

function useOutsideAlerter(ref, funcOut) {
    useEffect(() => {
        /**
         * Alert if clicked on outside of element
         */
        function handleClickOutside(event) {
            if (ref.current && !ref.current.contains(event.target)) {
                funcOut()
            }
        }

        // Bind the event listener
        document.addEventListener('mousedown', handleClickOutside)
        return () => {
            // Unbind the event listener on clean up
            document.removeEventListener('mousedown', handleClickOutside)
        }
    }, [ref])
}

const ComboBox = (props) => {
    const [isShowList, setIsShowList] = useState(false)

    const inputFocusHandler = () => {
        setIsShowList(true)
    }

    const inputBlurHandler = () => {
        setIsShowList(false)
    }

    const itemClickId = (id) => () => {
        props.selectHandler(id)
        setIsShowList(false)
    }

    const selectedText = () => {
        if (props.items && props.items.length > 0) {
            if (props.selectItemId) {
                const item = props.items.find((x) => x.id === props.selectItemId)

                return item?.text || props.defaultItem
            }
        }
        return props.defaultItem || props.placeholder
    }

    const wrapperRef = useRef(null)

    useOutsideAlerter(wrapperRef, inputBlurHandler)

    const isOpenList = isShowList && props.items && props.items.length > 0
    return (
        <S.Container
            ref={wrapperRef}
            isArrowDown={!isOpenList}
            isValid={props.isValid}
            isBorder={props.isBorder}>
            <ArrowUp />
            <S.SelectedPosition
                onFocus={inputFocusHandler}
                onBlur={inputBlurHandler}
                onClick={inputFocusHandler}>
                {selectedText()}
            </S.SelectedPosition>
            {isOpenList && (
                <S.ListItemBlock marginTop={props.marginTopSelector}>
                    {props.items.map((item) => (
                        <S.Item
                            id={item.id}
                            data-id={item.id}
                            key={item.id}
                            onMouseDown={(e) => e.preventDefault()}
                            onClick={itemClickId(item.id)}>
                            {item.text}
                        </S.Item>
                    ))}
                </S.ListItemBlock>
            )}
        </S.Container>
    )
}

ComboBox.propTypes = {
    items: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.number.isRequired,
            text: PropTypes.any.isRequired,
            additional: PropTypes.any
        })
    ),
    selectItemId: PropTypes.number.isRequired,
    placeholder: PropTypes.string.isRequired,
    selectHandler: PropTypes.func.isRequired,
    isValid: PropTypes.bool.isRequired,
    defaultItem: PropTypes.element,
    marginTopSelector: PropTypes.string
}

ComboBox.defaultProps = {
    isValid: true,
    isBorder: false,
    marginTopSelector: '2.6rem'
}

export default ComboBox
