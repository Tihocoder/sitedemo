import styled from 'styled-components'
import * as Theme from '../../../styles/theme-const'

export const Marker = styled.div`
    background-color: ${(props) => props.backgroundColor};
    padding: 0.1rem 0.3rem;
    font-size: 0.8rem;
    color: ${Theme.ColorConsts.white};
    border-radius: 0.3rem;
    cursor: default;
`
