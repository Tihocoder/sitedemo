import React from 'react'
import PropTypes from 'prop-types'
import * as S from './Marker.styled'
import * as Theme from '../../../styles/theme-const'

const Marker = (props) => {
    return (
        <S.Marker borderColor={props.borderColor} color={props.color} backgroundColor={props.backgroundColor}>
            {props.children}
        </S.Marker>
    )
}

Marker.propTypes = {
    backgroundColor: PropTypes.string.isRequired,
    borderColor: PropTypes.string,
    color: PropTypes.string.isRequired,
    children: PropTypes.any.isRequired
}
Marker.defaultProps = {
    backgroundColor: Theme.ColorConsts.orange,
    color: Theme.ColorConsts.white
}

export default Marker
