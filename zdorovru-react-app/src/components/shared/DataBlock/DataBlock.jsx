import React from 'react'
import PropTypes from 'prop-types'
import * as S from './DataBlock.styled'

const DataBlock = ({ description, children }) => {
    return (
        <S.Container>
            {description && <S.Description>{description}</S.Description>}
            <S.Body>{children}</S.Body>
        </S.Container>
    )
}

DataBlock.propTypes = {
    description: PropTypes.string.isRequired,
    children: PropTypes.any.isRequired
}

export default DataBlock
