import styled from 'styled-components'

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    padding: 0.5rem 0;
    margin-bottom: 1rem;
`

export const Description = styled.h2`
    margin: 0 0 1rem;
    @media only screen and (max-width: 960px){
        margin-bottom: 1rem;
    }
    font: inherit;
    color: ${(props) => props.theme.colors.getGrayColor(0.5)};
`
export const Body = styled.div`
    margin: 0;
`
