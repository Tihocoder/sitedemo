import React from 'react'
import PropTypes from 'prop-types'
import * as S from './BlockMaterialShadow.styled'

function BlockMaterialShadow(props) {
    return (
        <S.Container isHover={props.isHover} onClick={props.clickHandler}Ï>
            {props.children}
        </S.Container>
    )
}

BlockMaterialShadow.propTypes = {
    children: PropTypes.any.isRequired,
    clickHandler: PropTypes.func.isRequired
}
BlockMaterialShadow.defaultProps = {
    isHover: false
}

export default BlockMaterialShadow
