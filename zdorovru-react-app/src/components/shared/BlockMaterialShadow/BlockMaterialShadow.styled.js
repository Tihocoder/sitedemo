import styled, { css } from 'styled-components'

export const Container = styled.div`
    background: #fff;
    border-radius: 0.3rem;
    cursor: pointer;
    box-shadow: ${(props) => props.theme.shadows.shadowKitLightBlock};
    transition: box-shadow 0.2s ease;
    ${(props) =>
        props.isHover &&
        css`
            :hover {
                box-shadow: ${(props) => props.theme.shadows.shadowKitHover};
                transform: translateY(-1px);
            }
        `}
`
