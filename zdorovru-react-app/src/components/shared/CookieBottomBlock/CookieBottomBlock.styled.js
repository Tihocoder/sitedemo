import styled from 'styled-components'

export const Container = styled.div`
    display: flex;
    transition: height 0.8s ease-in-out;
    width: 100%;
    position: fixed;
    min-height: 5rem;
    z-index: 9999;
    bottom: 0;
    left: 0;
    background: #fff;
    box-shadow: 0 2px 10px 0 #999;
    padding: 10px 0;
    outline: 0;
    transform: translateY(2px);
    justify-content: center;
`

export const ContentBody = styled.div`
    display: flex;
    justify-content: space-between;
    box-sizing: border-box;
    align-items: center;
    width: 1024px;
`

export const ContentText = styled.div`
    padding: 0 1rem;
`

export const ContentCLoseBlock = styled.div`
    width: 2rem;
    cursor: pointer;
    & > svg {
        fill: ${(props) => props.theme.colors.getGrayColor(0.6)};
        transition: all 0.4s ease-in-out;
    }
    :hover {
        & > svg {
            fill: ${(props) => props.theme.colors.getGrayColor(0.8)};
        }
    }
`
