import React from 'react'
import PropTypes from 'prop-types'
import * as S from './CookieBottomBlock.styled'
import Cross from '../../Icons/Cross'

const CookieBottomBlock = (props) => {
    return (
        <S.Container>
            <S.ContentBody>
                <S.ContentText>{props.children}</S.ContentText>
                <S.ContentCLoseBlock onClick={props.closeHandler}>
                    <Cross />
                </S.ContentCLoseBlock>
            </S.ContentBody>
        </S.Container>
    )
}

CookieBottomBlock.propTypes = {
    children: PropTypes.any.isRequired,
    closeHandler: PropTypes.func.isRequired
}

export default CookieBottomBlock
