import styled, { css } from 'styled-components'

const invalidatedInput = css`
    border: 1px solid ${(props) => props.theme.colors.accentRed};
`

export const Body = styled.div`
    display: flex;
    flex-direction: ${(props) => (props.isColumn ? 'column' : 'row')};
`

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    ${(props) => !props.isValid && invalidatedInput}
    border-radius: 0.3rem;
    position: relative;
    ${(props) =>
        props.placeholder &&
        css`
            padding: 0.844rem;
        `}
`
export const Label = styled.label`
    position: absolute;
    pointer-events: none;
    transition: 0.2s ease all;
    color: ${(props) => props.theme.colors.getGrayColor(0.4)};
    top: 0;
    left: 1rem;
    font-size: 0.75rem;
`
