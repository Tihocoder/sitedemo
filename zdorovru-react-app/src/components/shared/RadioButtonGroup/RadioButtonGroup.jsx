import React from 'react'
import PropTypes from 'prop-types'
import * as S from './RadioButtonGroup.styled'

const RadioButtonGroup = (props) => {
    const getCheckedState = (value) => props.selectValue === value

    const changeCheckedState = (value) => () => {
        props.changeValue(value)
    }

    const checkBoxGroup = []

    let children = null
    if (props.children) {
        children = React.Children.toArray(props.children)

        React.Children.forEach(children, (radioButton) => {
            if (React.isValidElement(radioButton)) {
                const checkBoxNode = React.cloneElement(radioButton, {
                    key: radioButton.props.id,
                    value: radioButton.props.value,
                    checked: getCheckedState(radioButton.props.value),
                    clickHandler: changeCheckedState(radioButton.props.value),
                    ...props,
                    children: radioButton.props.children
                })
                checkBoxGroup.push(checkBoxNode)
            }
        })
    }
    return (
        <S.Container isValid={props.isValid} placeholder={props.placeholder}>
            {props.placeholder && <S.Label>{props.placeholder}</S.Label>}
            <S.Body isColumn={props.isColumn}>
                {checkBoxGroup.map((Component) => React.cloneElement(Component))}
            </S.Body>
        </S.Container>
    )
}

RadioButtonGroup.propTypes = {
    selectValue: PropTypes.any.isRequired,
    changeValue: PropTypes.func.isRequired,
    isColumn: PropTypes.bool,
    placeholder: PropTypes.string,
    isValid: PropTypes.bool.isRequired
}
RadioButtonGroup.defaultProps = {
    isValid: true,
    isColumn: false
}

export default RadioButtonGroup
