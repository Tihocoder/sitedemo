import styled from 'styled-components'

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    color: ${({ theme }) => theme.colors.getGrayColor(0.4)};
    font-size: 1.1rem;
    justify-content: space-between;
    & > div {
        margin-bottom: 0.5rem;
    }
`

export const ContentRow = styled.div`
    cursor: pointer;
    :hover {
        color: ${({ theme }) => theme.colors.accentGreenLight};
    }
`
