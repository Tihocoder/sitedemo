import React from 'react'
import PropTypes from 'prop-types'
import * as S from './ListSelector.styled'

const ListSelector = (props) => {
    if (!props.children || !props.children.length === 0) return <React.Fragment />

    const clickHandler = (id) => () => {
        props.clickItemHandler(id)
    }

    return (
        <S.Container>
            {props.children.map((item) => (
                <S.ContentRow key={item.id} id={item.id} onClick={clickHandler(item.id)}>
                    {item.content}
                </S.ContentRow>
            ))}
        </S.Container>
    )
}

ListSelector.propTypes = {
    children: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.number.isRequired,
            content: PropTypes.any.isRequired
        })
    ).isRequired,
    clickItemHandler: PropTypes.func.isRequired
}

export default ListSelector
