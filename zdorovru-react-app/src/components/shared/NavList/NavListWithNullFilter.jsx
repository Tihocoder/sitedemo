import React from 'react'
import PropTypes from 'prop-types'
import * as S from './NavList.styled'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { linkWithShipQuery } from '../../../helpers/site'
import { useSelector } from 'react-redux'

const NavListWithNullFilter = (props) => {
    if (!props.navItems || props.navItems.length < 1) {
        return <S.Container></S.Container>
    }
    const renderChilds = (navItems) => {
        if (!navItems) return <React.Fragment></React.Fragment>

        return (
            <S.ChildNavItemsContainer>
                {navItems.map((x) => (
                    <S.NavItemContainer key={x.id}>
                        <Link href={x.link} passHref>
                            <S.StyledLink isSelected={x.id === props.selectedGroup}>
                                {x.text}{' '}
                                {x.count && x.count > 0 ? (
                                    <S.CountBlock>({x.count})</S.CountBlock>
                                ) : (
                                    <React.Fragment />
                                )}
                            </S.StyledLink>
                        </Link>
                        {renderChilds(x.childs)}
                    </S.NavItemContainer>
                ))}
            </S.ChildNavItemsContainer>
        )
    }

    return (
        <S.Container>
            <S.Title>{props.title}</S.Title>
            {props.navItems.map((item) => (
                <S.NavItemContainer key={item.id}>
                    <Link key={item.id} href={item.link} passHref>
                        <S.StyledLink isSelected={item.id === props.selectedGroup}>
                            {item.text}{' '}
                            {item.count && item.count > 0 ? (
                                <S.CountBlock>({item.count})</S.CountBlock>
                            ) : (
                                <React.Fragment />
                            )}
                        </S.StyledLink>
                    </Link>
                    {renderChilds(item.childs)}
                </S.NavItemContainer>
            ))}
        </S.Container>
    )
}

NavListWithNullFilter.propTypes = {
    title: PropTypes.string,
    navItems: PropTypes.arrayOf(
        PropTypes.shape({
            link: PropTypes.string.isRequired,
            id: PropTypes.number.isRequired,
            text: PropTypes.string.isRequired,
            count: PropTypes.number,
            childs: PropTypes.arrayOf(
                PropTypes.shape({
                    link: PropTypes.string.isRequired,
                    text: PropTypes.string.isRequired,
                    count: PropTypes.number
                })
            )
        })
    ),
    isCatalog: PropTypes.bool,
    groupId: PropTypes.number
}

export default NavListWithNullFilter
