import styled, { css } from 'styled-components'
import * as Theme from '../../../styles/theme-const'

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    padding: 0 0.6rem;
    width: 100%;
    font-size: inherit;
    box-sizing: border-box;
    max-width: 18.4rem;
    overflow-x: auto;
    max-height: 23rem;
`

export const Title = styled.div`
    font-size: 0.9rem;
    font-weight: normal;
    color: ${(props) => props.theme.colors.getGrayColor(0.9)};
    margin-bottom: 0.5rem;
`
export const StyledLink = styled.a`
    font-size: 0.8rem;
    color: ${(props) => props.theme.colors.getGrayColor(0.8)};
    cursor: pointer;
    user-select: none;
    text-decoration: none;
    transition: color 0.3s ease;
    ${(props) =>
        props.isSelected &&
        css`
            font-weight: bold;
            font-size: 0.85rem;
        `}
    & :hover {
        color: ${Theme.ColorConsts.accentGreen};
        transition: color 0.3s ease;
    }
`

export const CountBlock = styled.span`
    white-space: nowrap;
`

export const ChildNavItemsContainer = styled.div`
    padding-left: 0.75rem;
`

export const NavItemContainer = styled.div`
    margin: 0;
    padding: 0.2rem 0;
`
