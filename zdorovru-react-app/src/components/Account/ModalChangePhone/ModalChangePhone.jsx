import React, { useEffect, useState } from 'react'
import ModalWindowCustom from '@shared/ModalWindowCustom/ModalWindowCustom'
import InputMask from 'react-input-mask'
import Input from '@shared/Input/Input'
import Button from '@shared/Button/Button'
import * as Theme from '@styles/theme-const'
import PropTypes from 'prop-types'
import { profileChangePhone, profileConfirmChangePhone } from '@api/siteApis'
import * as S from './ModalChangePhone.styled'
import { useRouter } from 'next/router'

const smsTimeout = 60

const ModalChangePhone = (props) => {
    const [isSendSms, setIsSendSms] = useState(false)
    const [phoneNumber, setPhoneNumber] = useState('')
    const [code, setCode] = useState('')
    const [errorText, setErrorText] = useState('')
    const router = useRouter()
    const [timer, setTimer] = useState({
        seconds: smsTimeout,
        isActive: false
    })

    useEffect(() => {
        let interval = null
        if (timer.isActive) {
            interval = setInterval(() => {
                if (timer.seconds < 1) {
                    setTimer({ ...timer, seconds: 0, isActive: false })
                } else {
                    setTimer({ ...timer, seconds: timer.seconds - 1 })
                }
            }, 1000)
        } else if (!timer.isActive && timer.seconds !== 0) {
            clearInterval(interval)
        }
        return () => clearInterval(interval)
    }, [timer.isActive, timer.seconds])

    const submitPhone = async () => {
        setErrorText('')
        const response = await profileConfirmChangePhone(phoneNumber.replace(/\D+/g, ''))
        if (response.status === 4) {
            setTimer({
                ...timer,
                seconds: smsTimeout,
                isActive: true
            })
        }
        if (response.status !== 5) {
            setErrorText(response.message)
            return
        }
        setIsSendSms(true)
        setTimer({
            ...timer,
            seconds: smsTimeout,
            isActive: true
        })
    }

    const ChangePhone = async (submitCode) => {
        const response = await profileChangePhone(phoneNumber.replace(/\D+/g, ''), submitCode)
        if (response.status === 0) {
            await router.reload()
            return
        }
        if (response.status !== 0) {
            setErrorText(response.message)
            setCode('')
        }
        if (response.status !== 1) {
            if (response.status === 3) {
                setErrorText('Неверный код. Попробуйте еще раз')
                setCode('')
            }
            if (response.status === 2) {
                setErrorText('Превышено число попыток')
            }
            if (response.status === 5) {
                setErrorText('Вы недавно отправляли запрос, пожалуйста попробуйте позже')
            }
        }
    }

    const changeCodeSubmit = async (e) => {
        setCode(e.target.value)
        setErrorText('')
        if (e.target.value && e.target.value.length === 4) {
            await ChangePhone(e.target.value)
        }
    }
    const selectInputPhonePage = () => {
        setIsSendSms(false)
    }

    return (
        <ModalWindowCustom
            cancel={{
                text: 'Отмена',
                clickHandler: props.closeHandler
            }}>
            {!isSendSms ? (
                <S.ConfirmContainer>
                    <S.Title>Новый номер</S.Title>
                    <S.ConfirmBody>
                        <S.InputPhoneContainer>
                            <InputMask
                                value={phoneNumber}
                                onChange={(e) => setPhoneNumber(e.target.value)}
                                isValid
                                type="tel"
                                mask="+7(999) 999 99 99">
                                <Input placeholderRaised="Мобильный телефон" />
                            </InputMask>
                        </S.InputPhoneContainer>
                        {errorText && <S.CodeConfirmErrorBlock>{errorText}</S.CodeConfirmErrorBlock>}
                        <S.ButtonSubmitContainer>
                            {timer.isActive ? (
                                <S.TImeDescriptionBlock>
                                    <S.TImeDescription>
                                        Вы недавно запрашивали код. Повторная отправка кода через{' '}
                                        {timer.seconds} сек
                                    </S.TImeDescription>
                                </S.TImeDescriptionBlock>
                            ) : (
                                <Button
                                    elementSizeTypeValue={Theme.ElementSizeType.large}
                                    clickHandler={submitPhone}>
                                    Получить код
                                </Button>
                            )}
                        </S.ButtonSubmitContainer>
                    </S.ConfirmBody>
                </S.ConfirmContainer>
            ) : (
                <S.CodeConfirmContainer>
                    <S.CodeConfirmTitle>Введите код</S.CodeConfirmTitle>
                    <S.ConfirmBody>
                        <S.CodeConfirmInput>
                            <Input value={code} onChange={(e) => changeCodeSubmit(e)} />
                        </S.CodeConfirmInput>
                        {errorText && <S.CodeConfirmErrorBlock>{errorText}</S.CodeConfirmErrorBlock>}
                        {timer.isActive ? (
                            <S.TImeDescriptionBlock>
                                <S.TImeDescription>Повторная отправка</S.TImeDescription>
                                <S.TImeDescription>кода через {timer.seconds} сек</S.TImeDescription>
                            </S.TImeDescriptionBlock>
                        ) : (
                            <S.ResetSms onClick={submitPhone}>Получить новый код</S.ResetSms>
                        )}
                        <S.CodeConfirmPhone>
                            {phoneNumber.replace(/(\d{1})(\d{3})(\d{3})(\d{2})(\d{2})/, '+$1 ($2) $3-$4-$5')}
                        </S.CodeConfirmPhone>
                        <S.CodeConfirmAgain onClick={selectInputPhonePage}>
                            Ввести другой номер
                        </S.CodeConfirmAgain>
                    </S.ConfirmBody>
                </S.CodeConfirmContainer>
            )}
        </ModalWindowCustom>
    )
}

ModalChangePhone.propTypes = {
    closeHandler: PropTypes.func.isRequired
}

export default ModalChangePhone
