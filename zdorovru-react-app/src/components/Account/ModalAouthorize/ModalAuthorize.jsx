import React from 'react'
import PropTypes from 'prop-types'
import InputMask from 'react-input-mask'
import { useAuthorize } from '@hooks/account/autorize-hook'
import ModalWindowCustom from '../../shared/ModalWindowCustom/ModalWindowCustom'
import * as S from './ModalAouthorize.styled'
import * as Theme from '../../../styles/theme-const'
import Input from '../../shared/Input/Input'
import Button from '../../shared/Button/Button'

const ModalAuthorize = (props) => {
    const {
        isSendSms,
        phoneNumber,
        setPhoneNumber,
        errorText,
        timer,
        code,
        submitPhone,
        changeCodeSubmit,
        selectInputPhonePage
    } = useAuthorize(props.closeHandler)

    return (
        <ModalWindowCustom
            cancel={{
                text: 'Отмена',
                clickHandler: props.closeHandler
            }}>
            {!isSendSms ? (
                <S.ConfirmContainer>
                    <S.Title>Вход и регистрация</S.Title>
                    <S.ConfirmBody>
                        <S.InputPhoneContainer>
                            <InputMask
                                value={phoneNumber}
                                onChange={(e) => setPhoneNumber(e.target.value)}
                                isValid
                                type="tel"
                                mask="+7(999) 999 99 99">
                                <Input placeholderRaised="Мобильный телефон" />
                            </InputMask>
                        </S.InputPhoneContainer>
                        {errorText && <S.CodeConfirmErrorBlock>{errorText}</S.CodeConfirmErrorBlock>}
                        <S.ButtonSubmitContainer>
                            {timer.isActive ? (
                                <S.TImeDescriptionBlock>
                                    <S.TImeDescription>
                                        Вы недавно запрашивали код. Повторная отправка кода через{' '}
                                        {timer.seconds} сек
                                    </S.TImeDescription>
                                </S.TImeDescriptionBlock>
                            ) : (
                                <Button
                                    elementSizeTypeValue={Theme.ElementSizeType.large}
                                    clickHandler={submitPhone}>
                                    Получить код
                                </Button>
                            )}
                        </S.ButtonSubmitContainer>
                    </S.ConfirmBody>
                </S.ConfirmContainer>
            ) : (
                <S.CodeConfirmContainer>
                    <S.CodeConfirmTitle>Введите код</S.CodeConfirmTitle>
                    <S.ConfirmBody>
                        <S.CodeConfirmInput>
                            <Input value={code} onChange={(e) => changeCodeSubmit(e, props.isReload)} />
                        </S.CodeConfirmInput>
                        {errorText && <S.CodeConfirmErrorBlock>{errorText}</S.CodeConfirmErrorBlock>}
                        {timer.isActive ? (
                            <S.TImeDescriptionBlock>
                                <S.TImeDescription>Повторная отправка</S.TImeDescription>
                                <S.TImeDescription>кода через {timer.seconds} сек</S.TImeDescription>
                            </S.TImeDescriptionBlock>
                        ) : (
                            <S.ResetSms onClick={submitPhone}>Получить новый код</S.ResetSms>
                        )}
                        <S.CodeConfirmPhone>
                            {phoneNumber.replace(/(\d{1})(\d{3})(\d{3})(\d{2})(\d{2})/, '+$1 ($2) $3-$4-$5')}
                        </S.CodeConfirmPhone>
                        <S.CodeConfirmAgain onClick={selectInputPhonePage}>
                            Ввести другой номер
                        </S.CodeConfirmAgain>
                    </S.ConfirmBody>
                </S.CodeConfirmContainer>
            )}
        </ModalWindowCustom>
    )
}

ModalAuthorize.propTypes = {
    closeHandler: PropTypes.func.isRequired,
    isReload: PropTypes.bool
}

ModalAuthorize.defaultProps = {
    isReload: false
}

export default ModalAuthorize
