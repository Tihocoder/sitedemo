import styled, { css } from 'styled-components'

export const ConfirmContainer = styled.div`
    display: flex;
    flex-direction: column;
    height: 16rem;
    width: 15rem;
`
export const ConfirmBody = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    height: 100%;
    & > div {
        padding: 0.3rem 0;
        width: 100%;
    }
`

export const Title = styled.div`
    display: flex;
    justify-content: flex-start;
    align-items: center;
    font-size: 1.25rem;
`

export const TImeDescriptionBlock = styled.div`
    display: flex;
    justify-content: flex-start;
    align-items: center;
    flex-direction: column;
    justify-content: center;
    font-size: 0.75rem;
`

export const TImeDescription = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    font-size: 0.75rem;
`

export const ResetSms = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    font-size: 0.85rem;
    cursor: pointer;
    color: ${(props) => props.theme.colors.getBlue60Color(1)};
`

export const InputPhoneContainer = styled.div`
    min-width: 15rem;
    & > input {
        width: 100%;
        padding: 0.7rem 1rem;
        border: 1px solid rgba(50, 62, 52, 0.1);
        color: ${(props) => props.theme.colors.getGrayColor(0.6)};
        box-sizing: border-box;
        outline: none;
        -webkit-appearance: none;
        border-radius: 7px;
        transition: all 0.35s ease;
        font: inherit;

        ${(props) => (props.isValid ? validatedInput : notValidatedInput)}
        ::placeholder {
            color: ${(props) => props.theme.colors.getGrayColor(0.4)};
        }
        &:focus {
            color: ${(props) => props.theme.colors.getGrayColor(0.6)};
            box-shadow: ${(props) => props.theme.shadows.shadowKitHover};
            ::placeholder {
                transition: opacity 0.45s ease;
                color: ${(props) => props.theme.colors.getGrayColor(0.4)};
            }
        }
    }
`

const validatedInput = css`
    border: 1px solid ${(props) => props.theme.colors.getGrayColor(0.1)};
    background-color: inherit;
`
const notValidatedInput = css`
    border: 1px solid ${(props) => props.theme.colors.accentGreen};
`

export const ButtonSubmitContainer = styled.div`
    & button {
        width: 100%;
    }
`

export const CodeConfirmContainer = styled.div`
    display: flex;
    flex-direction: column;
    height: 16rem;
    width: 15rem;
`
export const CodeConfirmTitle = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    font-size: 1.25rem;
`
export const CodeConfirmInput = styled.div`
    min-width: 5rem;
    display: flex;
    justify-content: center;
    & input {
        width: 5rem;
        text-align: center;
    }
`
export const CodeConfirmErrorBlock = styled.div`
    color: ${(props) => props.theme.colors.pureRed};
    text-align: center;
`

export const CodeConfirmPhone = styled.div`
    text-align: center;
    color: ${(props) => props.theme.colors.getGrayColor(1)};
`

export const CodeConfirmAgain = styled.div`
    cursor: pointer;
    text-align: center;
    color: ${(props) => props.theme.colors.getBlue60Color(1)};
`
