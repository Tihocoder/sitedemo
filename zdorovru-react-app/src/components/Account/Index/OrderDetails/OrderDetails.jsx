import React from 'react'
import PropTypes from 'prop-types'
import { useOrderDetails } from '@hooks/account/order-details-hook'
import { OrderType } from 'src/consts'
import * as S from './OrderDetails.styled'
import HistoryOrderItem from '../HistoryOrder/HistoryOrderItem/HistoryOrderItem'
import CatalogGoodItem from '../../../CatalogGoodItem/CatalogGoodItem'

const OrderDetails = ({ order }) => {
    const orderDetails = useOrderDetails(order.id)

    return (
        <S.Container>
            <HistoryOrderItem order={order} selectOrderHandler={orderDetails.closeOrder} isMore />
            <S.OrderDetailsBlock>
                {orderDetails.goods &&
                    orderDetails.goods.map((good) => (
                        <CatalogGoodItem
                            key={good.webData.goodId}
                            basketItemCount={orderDetails.getBasketItem(good.webData.goodId)}
                            count={good.count}
                            availableStocksCount={good.availableStocksCount}
                            fixPrice={good.fixPrice}
                            analoguesQty={good.analoguesQty}
                            anotherOutFormsQty={good.anotherOutFormsQty}
                            webData={good.webData}
                            stockPrice={good.stockPrice}
                            deliveryPrice={good.deliveryPrice}
                            minPrice={good.minPrice}
                            deliveryStatus={good.deliveryStatus}
                            isDemand={good.isDemand}
                            aptEtaDateTime={good.aptEtaDateTime}
                            isAvailable={good.isAvailable}
                            isAvailableOnStock={good.isAvailableOnStock}
                            isStrictlyByPrescription={good.isStrictlyByPrescription}
                            isOriginalGood={good.isOriginalGood}
                            isCold={good.isCold}
                            shipment={orderDetails.shipment}
                            options={{
                                isPositions: true
                            }}
                        />
                    ))}
            </S.OrderDetailsBlock>
        </S.Container>
    )
}

OrderDetails.propTypes = {
    order: PropTypes.shape(OrderType).isRequired
}

export default OrderDetails
