// import React, { useEffect, useState } from 'react'
// import PropTypes from 'prop-types'
// import { useRouter } from 'next/router'
// import * as S from './Index.styled'
// import AccountNavigation from './AccountNavigation/AccountNavigation'
// import HistoryOrder from './HistoryOrder/HistoryOrder'
// import Profile from './Profile/Profile'
// import { getCustomerOrders } from '../../../api/siteApis'
// import { initGoodsCustomerPurchasedItems, initGoodsFromLiked } from '../../../actions/catalog'
// import { useDispatch, useSelector } from 'react-redux'
// import CatalogGoodsWithFilters from '../../CatalogGoodsWithFilters/CatalogGoodsWithFilters'
// import { destroyCookie } from 'nookies'
// import OrderDetails from './OrderDetails/OrderDetails'

// export const AccountPaths = {
//     history: '/account/history',
//     liked: '/account/liked',
//     purchased: '/account/purchased',
//     order: 'account/order'
// }

// const Index = (props) => {
//     const dispatch = useDispatch()
//     const router = useRouter()
//     const shipment = useSelector((state) => state.customer.shipment)
//     const likedGoods = useSelector((state) => state.customer.likedGoods)
//     const [isProfilleEditor, setIsProfileEditor] = useState(false)
//     const [customerOrder, setCustomerOrders] = useState([])
//     const [selectedOrderId, setSelectedOrderId] = useState(0)

//     const initGoods = async () => {
//         await dispatch(initGoodsFromLiked())
//     }
//     const initGoodsPurchased = async () => {
//         await dispatch(initGoodsCustomerPurchasedItems())
//     }

//     const initCustomerOrder = async () => {
//         const orders = await getCustomerOrders()
//         setCustomerOrders(orders)
//     }

//     const selectedTabHandler = (selectTab) => {
//         if (selectTab === ProfileTabs.profile) {
//             setIsProfileEditor(true)
//         } else {
//             setTab(selectTab)
//         }
//     }

//     const selectOrderHandler = async (orderId) => {
//         setSelectedOrderId(orderId)
//         setTab(ProfileTabs.order)
//     }

//     useEffect(() => {
//         if (tab === ProfileTabs.liked) {
//             initGoods()
//             return
//         }
//         if (tab === ProfileTabs.purchased) {
//             initGoodsPurchased()
//             return
//         }
//         if (tab === ProfileTabs.history) {
//             initCustomerOrder()
//             return
//         }
//     }, [tab, shipment.stockId, shipment.cityId, likedGoods.length])

//     const switchTab = () => {
//         if (tab === ProfileTabs.history) {
//             return <HistoryOrder orders={customerOrder} selectOrderHandler={selectOrderHandler} />
//         }
//         if (tab === ProfileTabs.liked) {
//             return <CatalogGoodsWithFilters />
//         }
//         if (tab === ProfileTabs.purchased) {
//             return <CatalogGoodsWithFilters />
//         }
//         if (tab === ProfileTabs.exit) {
//             destroyCookie({}, 'customer_token', { path: '/' })
//             router.push('/')
//         }
//         if (tab === ProfileTabs.order) {
//             const order = customerOrder.find((x) => x.id === selectedOrderId)
//             return (
//                 <OrderDetails
//                     orderId={order.id}
//                     shipment={shipment}
//                     order={order}
//                     closeHandler={() => setTab(ProfileTabs.history)}
//                 />
//             )
//         }
//         return <HistoryOrder orders={customerOrder} selectOrderHandler={selectOrderHandler} />
//     }

//     const closeEditProfile = async () => {
//         setIsProfileEditor(false)
//     }
//     return (
//         <S.Container>
//             {isProfilleEditor && (
//                 <Profile
//                     genderTypes={props.genderTypes}
//                     profile={props.profile}
//                     closeEditProfile={closeEditProfile}
//                 />
//             )}
//             <AccountNavigation tab={tab} selectTab={selectedTabHandler} profile={props.profile} />
//             <S.Body>{switchTab()}</S.Body>
//         </S.Container>
//     )
// }

// Index.propTypes = {
//     orders: PropTypes.array,
//     profile: PropTypes.shape({
//         firstName: PropTypes.string,
//         lastName: PropTypes.string,
//         middleName: PropTypes.string,
//         gender: PropTypes.number,
//         birthday: PropTypes.string,
//         phone: PropTypes.string,
//         email: PropTypes.string,
//         isPromoConfirmed: PropTypes.bool
//     }),
//     genderTypes: PropTypes.arrayOf(
//         PropTypes.shape({
//             typeId: PropTypes.number,
//             text: PropTypes.string
//         })
//     )
// }

// export default Index
