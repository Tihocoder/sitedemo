import styled, { css } from 'styled-components'
import { RadioButtonContainer } from '@shared/RadioButton/RadioButton.styled'

const validatedInput = css`
    border: 1px solid ${(props) => props.theme.colors.accentGreen};

    :focus {
        border: 1px solid ${(props) => props.theme.colors.getGreenColor(0.8)};
    }

    background-color: inherit;
`
const notValidatedInput = css`
    border: 1px solid ${(props) => props.theme.colors.accentRed};

    :focus {
        border: 1px solid ${(props) => props.theme.colors.accentRed};
    }
`

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
    max-width: 29.625rem;
    //padding: 1.25rem;
    /* & input {
        border-color: ${(props) => props.theme.colors.accentGreenLight};
        border-width: 0.5px;
        :focus {
            border-color: ${(props) => props.theme.colors.accentGreenLight};
        }
    } */

    ${RadioButtonContainer} {
        ::before {
            border-color: ${(props) => props.theme.colors.accentGreenLight};
        }
    }
`

export const BaseProfileDataContainer = styled.div`
    display: flex;
    flex-direction: column;
    max-width: 22.188rem;

    & > * {
        margin: 0;
        margin-bottom: 0.625rem;
    }

    & input {
        color: ${(props) => props.theme.colors.getGrayColor(1)};
    }
`
export const BirthdayInputContainer = styled.div`
    position: relative;
    display: flex;
    align-items: center;

    & > input:focus ~ label,
    input:not(:focus):valid ~ label {
        top: 0.1rem;
        bottom: 0.5rem;
        left: 1rem;
        font-size: 0.75rem;
        opacity: 1;
        height: 0.9rem;
    }

    & input {
        width: 100%;
        padding: 0.7rem 1rem;
        border: 1px solid ${(props) => props.theme.colors.accentGreen};
        color: ${(props) => props.theme.colors.getGrayColor(1)};
        box-sizing: border-box;
        outline: none;
        border-radius: 0.3rem;
        transition: all 0.35s ease;
        font: inherit;

        ${(props) => (props.isValid ? validatedInput : notValidatedInput)}
        ::placeholder {
            color: ${(props) => props.theme.colors.getGrayColor(0.4)};
        }

        &:focus {
            color: ${(props) => props.theme.colors.getGrayColor(1)};
            box-shadow: ${(props) => props.theme.shadows.shadowKitHover};

            ::placeholder {
                transition: opacity 0.45s ease;
                color: ${(props) => props.theme.colors.getGrayColor(0.4)};
            }
        }
    }
`
export const BirthdayLabel = styled.label`
    position: absolute;
    pointer-events: none;
    font-size: 1rem;
    left: 1.2rem;
    top: 0.875rem;
    transition: 0.2s ease all;
    color: ${(props) => props.theme.colors.getGrayColor(0.4)};
`

export const BirthdayBlock = styled.div`
    margin-bottom: 0.625rem;
    max-width: 9.9rem;
    min-width: 9rem;
`
export const PhoneBlock = styled.div`
    min-width: 11.563rem;
    width: 11.563rem;
`

export const Title = styled.h2`
    font-size: 1.25rem;
`

export const Description = styled.div`
    font-size: 0.75rem;
`
export const ContainerWithDescription = styled.div`
    display: flex;
    width: 100%;
    /* & > *:first-child {
} */

    & > ${Description} {
        margin-left: 1rem;
    }
`
export const ContainerRow = styled.div`
    display: flex;
    width: 100%;
    height: 3rem;
    margin-bottom: 0.625rem;

    & > * {
        margin-right: 0.5rem;
    }

    & > *:last-child {
        margin-right: 0.5rem;
    }

    & > button {
        height: 100%;
    }
`
export const EmailBlock = styled.div`
    margin-bottom: 0.625rem;
    min-width: 11.563rem;
    width: 11.563rem;
`
export const ContainerCheckbox = styled.div`
    font-size: 0.75rem;
    margin-bottom: 0.625rem;
    max-width: 20.186rem;
`

export const FooterContainer = styled.div`
    display: flex;
    justify-content: space-between;
    width: 100%;
    & button {
        max-width: 11rem;
    }
`
export const FooterContainerColumn = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;

    & button {
        width: 100%;
        margin-top: 0.8rem;
    }
`

export const BodyWindow = styled.div`
    max-width: 28.313rem;
`
export const MessageText = styled.div`
    max-width: 21rem;
`
export const ParagraphInWindowBody = styled.p`
    text-indent: 1rem;
    margin-bottom: 1rem;
`
