import React from 'react'
import PropTypes from 'prop-types'
import InputMask from 'react-input-mask'
import { useProfile } from '@hooks/account/profile-hook'
// eslint-disable-next-line import/no-named-as-default,import/no-named-as-default-member
import ModalAuthorize from '@components/Account/ModalAouthorize/ModalAuthorize'
import * as S from './Profile.styled'
import * as Theme from '../../../../styles/theme-const'
import Input from '../../../shared/Input/Input'
import RadioButtonGroup from '../../../shared/RadioButtonGroup/RadioButtonGroup'
import RadioButton from '../../../shared/RadioButton/RadioButton'
import Button from '../../../shared/Button/Button'
import ButtonTransparent from '../../../shared/ButtonTransparent/ButtonTransparent'
import ModalWindowCustom from '../../../shared/ModalWindowCustom/ModalWindowCustom'
import ModalChangePhone from '../../ModalChangePhone/ModalChangePhone'

const borderInputColor = Theme.ColorConsts.accentGreen
const colorsButtonRedText = {
    color: Theme.ColorConsts.white,
    backgroundColor: Theme.ColorConsts.pureRed
}
const Profile = (props) => {
    const {
        state,
        validate,
        changeHandler,
        submitHandler,
        colorsButtonText,
        optionsButtonText,
        changePhoneState,
        changePhoneEditStage,
        closeChangePhone
    } = useProfile(props.profile)

    return (
        <ModalWindowCustom
            cancel={{
                text: 'Отмена',
                clickHandler: props.closeEditProfile
            }}>
            <S.Container>
                <S.Title>Личные данные</S.Title>
                <S.BaseProfileDataContainer>
                    <Input
                        value={state.lastName}
                        onChange={changeHandler('lastName')}
                        placeholderRaised="Фамилия"
                        borderColor={borderInputColor}
                    />
                    <Input
                        value={state.firstName}
                        onChange={changeHandler('firstName')}
                        placeholderRaised="Имя"
                        borderColor={borderInputColor}
                    />
                    <Input
                        value={state.middleName}
                        onChange={changeHandler('middleName')}
                        placeholderRaised="Отчество"
                        borderColor={borderInputColor}
                    />
                    <RadioButtonGroup
                        placeholder="Пол"
                        changeValue={changeHandler('gender')}
                        selectValue={state.gender}>
                        {props.genderTypes.map((x) => (
                            <RadioButton key={x.typeId} id={x.typeId} value={x.typeId}>
                                {x.text}
                            </RadioButton>
                        ))}
                    </RadioButtonGroup>
                </S.BaseProfileDataContainer>

                <S.BirthdayBlock>
                    <InputMask
                        value={state.birthday}
                        onChange={changeHandler('birthday')}
                        placeholderRaised="Дата рождения"
                        borderColor={borderInputColor}
                        mask="99.99.9999">
                        <Input type="text" disableUnderline />
                    </InputMask>
                </S.BirthdayBlock>
                <S.ContainerRow>
                    <S.PhoneBlock>
                        <InputMask
                            value={state.phone}
                            placeholderRaised="мобильный телефон"
                            borderColor={borderInputColor}
                            mask="+7\(999) 999 99 99">
                            <Input type="tel" disableUnderline />
                        </InputMask>
                    </S.PhoneBlock>
                    <Button
                        elementSizeTypeValue={Theme.ElementSizeType.regular}
                        colors={Theme.successButtonColorBlcackLight}
                        clickHandler={() => changePhoneEditStage(1)}>
                        Изменить телефон
                    </Button>
                </S.ContainerRow>
                <S.ContainerWithDescription>
                    <S.EmailBlock>
                        <Input
                            value={state.email}
                            isValid={validate.email}
                            onChange={changeHandler('email')}
                            placeholderRaised="e-mail"
                            borderColor={borderInputColor}
                        />
                    </S.EmailBlock>
                    <S.Description>
                        e-mail для отправки статусов заказов, электронных кассовых чеков и специальных
                        предложений
                    </S.Description>
                </S.ContainerWithDescription>
                <S.FooterContainer>
                    <Button elementSizeTypeValue={Theme.ElementSizeType.regular} clickHandler={submitHandler}>
                        Сохранить изменения
                    </Button>
                    <ButtonTransparent
                        options={optionsButtonText}
                        colors={colorsButtonText}
                        elementSizeTypeValue={Theme.ElementSizeType.regular}
                        clickHandler={props.closeEditProfile}>
                        Отменить изменения
                    </ButtonTransparent>
                </S.FooterContainer>
                {changePhoneState.stage === 1 ? (
                    <ModalWindowCustom
                        title="Внимание!"
                        cancel={{
                            clickHandler: closeChangePhone
                        }}
                        footer={
                            <S.FooterContainerColumn>
                                <Button
                                    elementSizeTypeValue={Theme.ElementSizeType.regular}
                                    colors={Theme.successButtonColor}
                                    clickHandler={() => changePhoneEditStage(4)}>
                                    Войти в личный кабинет под другим номером
                                </Button>
                                <Button
                                    elementSizeTypeValue={Theme.ElementSizeType.regular}
                                    colors={colorsButtonRedText}
                                    clickHandler={() => changePhoneEditStage(2)}>
                                    Переместить все заказы на новый номер
                                </Button>
                                <Button
                                    elementSizeTypeValue={Theme.ElementSizeType.regular}
                                    colors={Theme.successButtonLight}
                                    clickHandler={closeChangePhone}>
                                    Отмена
                                </Button>
                            </S.FooterContainerColumn>
                        }>
                        <S.BodyWindow>
                            <S.ParagraphInWindowBody>
                                Если Вы хотите войти в другой личный кабинет с помощью другого номера
                                телефона, нажмите “Войти в личный кабинет под другим номером”
                            </S.ParagraphInWindowBody>
                            <S.ParagraphInWindowBody>
                                Если Вы хотите сменить телефонный номер текущего личного кабинета (например,
                                при смене сим-карты), нажмите “Переместить все заказы на новый номер”.
                                Обращаем внимание, что все заказы будут удалены со старого номера и перемещены
                                на новый. Это действие является необратимым.
                            </S.ParagraphInWindowBody>
                        </S.BodyWindow>
                    </ModalWindowCustom>
                ) : (
                    <React.Fragment />
                )}
                {changePhoneState.stage === 2 ? (
                    <ModalWindowCustom
                        title="Внимание!"
                        cancel={{
                            clickHandler: () => changePhoneEditStage(1)
                        }}
                        footer={
                            <S.FooterContainer>
                                <Button
                                    elementSizeTypeValue={Theme.ElementSizeType.regular}
                                    colors={colorsButtonRedText}
                                    clickHandler={() => changePhoneEditStage(3)}>
                                    Изменить номер телефона
                                </Button>
                                <Button
                                    elementSizeTypeValue={Theme.ElementSizeType.regular}
                                    colors={Theme.successButtonLight}
                                    clickHandler={() => changePhoneEditStage(1)}>
                                    Оставить старый номер
                                </Button>
                            </S.FooterContainer>
                        }>
                        <S.MessageText>
                            При смене номера телефона все заказы будут перемещены на новый номер.
                        </S.MessageText>
                    </ModalWindowCustom>
                ) : (
                    <React.Fragment />
                )}
                {changePhoneState.stage === 3 ? (
                    <ModalChangePhone closeHandler={closeChangePhone} />
                ) : (
                    <React.Fragment />
                )}
                {changePhoneState.stage === 4 ? (
                    <ModalAuthorize isReload closeHandler={() => changePhoneEditStage(1)} />
                ) : (
                    <React.Fragment />
                )}
            </S.Container>
        </ModalWindowCustom>
    )
}

Profile.propTypes = {
    changeProfile: PropTypes.func,
    submitHandler: PropTypes.func,
    cancelHandler: PropTypes.func
}

export default Profile
