import styled, * as S from 'styled-components'

export const Container = styled.div`
    display: flex;
`

export const HistoryOrderBlock = styled.div`
    display: flex;
    flex-direction: column;
    padding: 1rem;
`
export const Body = styled.div`
    margin-left: 2rem;
    width: 100%;
`
