import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { useRouter } from 'next/router'
import { destroyCookie } from 'nookies'
import { useDispatch } from 'react-redux'
import { clearLikedGoods } from '@actions/customer'
import * as S from './AccountNavigation.styled'
import Profile from '../Profile/Profile'

export const AccountPaths = {
    history: '/account/history',
    liked: '/account/liked',
    purchased: '/account/purchased',
    order: 'account/order'
}
const AccountNavigation = ({ genderTypes, profile }) => {
    const router = useRouter()
    const dispatch = useDispatch()
    const [isProfilleEditor, setIsProfileEditor] = useState(false)
    // const clickTab = (x) => () => {
    //     selectTab(x)
    // }

    const closeEditProfile = async () => {
        setIsProfileEditor(false)
    }

    const clickNavItem = async (itemTab) => {
        if (itemTab === 'exit') {
            destroyCookie({}, 'customer_token', { path: '/' })
            await dispatch(clearLikedGoods())
            await router.push('/')
            return
        }
        if (itemTab === 'person') {
            setIsProfileEditor(true)
            return
        }
        router.push(itemTab)
    }
    const drawNavItem = (itemTab, imageUrl, title) => (
        <S.NavItem
            onClick={async () => {
                await clickNavItem(itemTab)
            }}
            isSelected={router.asPath.startsWith(itemTab)}>
            <S.NavIconBlock>
                <S.Icon src={imageUrl} />
            </S.NavIconBlock>
            <S.NavTitleBlock>{title}</S.NavTitleBlock>
        </S.NavItem>
    )

    return (
        <S.Container>
            {isProfilleEditor && (
                <Profile genderTypes={genderTypes} profile={profile} closeEditProfile={closeEditProfile} />
            )}
            <S.ProfileHeaderContainer>
                <S.ProfileHeaderPhotoBlock>
                    <S.ProfileHeaderPhotoCircleBlock imageUrl={profile.photoUrl} />
                </S.ProfileHeaderPhotoBlock>
                <S.ProfileHeaderInfoBlock>
                    <S.ProfileHeaderInfoName>{profile.firstName}</S.ProfileHeaderInfoName>
                    <S.ProfileHeaderInfoPhone>
                        {profile?.phone.replace(/(\d{1})(\d{3})(\d{3})(\d{2})(\d{2})/, '+$1 ($2) $3-$4-$5')}
                    </S.ProfileHeaderInfoPhone>
                </S.ProfileHeaderInfoBlock>
            </S.ProfileHeaderContainer>
            <S.NavList>
                {drawNavItem(
                    AccountPaths.history,
                    `${process.env.BASE_PATH || ''}/images/shopping_cart.svg`,
                    <S.TitleHeader>История заказов</S.TitleHeader>
                )}
                {drawNavItem(
                    AccountPaths.liked,
                    `${process.env.BASE_PATH || ''}/images/favorite_border.svg`,
                    <S.TitleHeader>Мои избранные</S.TitleHeader>
                )}
                {drawNavItem(
                    AccountPaths.purchased,
                    `${process.env.BASE_PATH || ''}/images/archive.svg`,
                    <S.TitleHeader>Купленные товары</S.TitleHeader>
                )}
                {drawNavItem(
                    'person',
                    `${process.env.BASE_PATH || ''}/images/person.svg`,
                    <S.TitleHeader>Изменить личные данные</S.TitleHeader>
                )}
                {drawNavItem(
                    'exit',
                    `${process.env.BASE_PATH || ''}/images/exit_to_app.svg`,
                    <S.TitleHeader>Выйти</S.TitleHeader>
                )}
            </S.NavList>
        </S.Container>
    )
}

AccountNavigation.propTypes = {
    selectTab: PropTypes.func,
    profile: PropTypes.object,
    genderTypes: PropTypes.any
}

export default AccountNavigation
