import styled, { css } from 'styled-components'

export const Container = styled.div`
    box-shadow: ${(props) => props.theme.shadows.shadowKit};
    box-sizing: border-box;
    min-width: 18rem;
    width: 18rem;
    background-color: ${(props) => props.theme.colors.white};
    height: fit-content;
    border-radius: 8px;
`

export const NavList = styled.div`
    display: flex;
    flex-direction: column;
`

export const ProfileHeaderContainer = styled.div`
    padding: 0.5rem;
    display: flex;
`

export const ProfileHeaderPhotoBlock = styled.div`
    width: 3.75rem;
    height: 3.75rem;
`

export const ProfileHeaderPhotoCircleBlock = styled.div`
    ${(props) =>
        props.imageUrl
            ? css`
                  background: url('${process.env.BASE_PATH || ''}${props.imageUrl}') no-repeat center center;
              `
            : css`
                  background: url('${process.env.BASE_PATH || ''}/images/user-photo.svg') no-repeat center
                      center;
              `}
    background-size: cover;
    height: 100%;
    width: 100%;
`

export const ProfileHeaderInfoBlock = styled.div`
    display: flex;
    flex-direction: column;
    margin-left: 1rem;
    justify-content: space-between;
    & > span {
        padding: 0.2rem 0;
    }
`

export const ProfileHeaderInfoName = styled.span`
    font-size: 1.25rem;
`

export const ProfileHeaderInfoPhone = styled.span`
    font-size: 0.875rem;
    color: ${(props) => props.theme.colors.grayAkm40};
`

export const NavItem = styled.div`
    padding: 0.5rem;
    padding-right: 0.8rem;
    display: flex;
    cursor: pointer;
    ${(props) =>
        props.isSelected &&
        css`
            background-color: ${props.theme.colors.grayAkm5};
        `}
`

export const NavIconBlock = styled.div`
    margin-right: 0.5rem;
`

export const Icon = styled.img``

export const NavTitleBlock = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
`

export const TitleHeader = styled.span`
    white-space: nowrap;
`
