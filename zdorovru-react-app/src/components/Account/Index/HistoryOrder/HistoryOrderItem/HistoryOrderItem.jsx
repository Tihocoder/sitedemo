import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { useDispatch, useSelector } from 'react-redux'
import * as S from './HistoryOrderItem.styled'
import * as Theme from '../../../../../styles/theme-const'
import SymbolRuble from '../../../../shared/SymbolRuble/SymbolRuble'
import { generateDate } from '../../../../../helpers/site'
import Button from '../../../../shared/Button/Button'
import { orderDetails } from '../../../../../api/siteApis'
import CatalogGoodItem from '../../../../CatalogGoodItem/CatalogGoodItem'

const HistoryOrderItem = (props) => {
    const dispatch = useDispatch()
    const [more, setMore] = useState({
        isOpened: false,
        orderDetails: {
            items: []
        }
    })
    const shipment = useSelector((state) => state.customer.shipment)

    const clickMoreHandler = async () => {
        await props.selectOrderHandler(props.order.id)
    }

    const getInfoBLock = () => {
        if (!props.order.isDelivery) {
            return (
                <S.InfoBlock>
                    <S.InfoItem>
                        <S.InfoItemTitle>Самовывоз:</S.InfoItemTitle>
                        <S.InfoItemDescription>
                            {props.order.stockData.title}, {props.order.stockData.address}
                        </S.InfoItemDescription>
                    </S.InfoItem>
                    {props.order.state.isShowBuyDate && (
                        <S.InfoItem>
                            <S.InfoItemTitle>Получение:</S.InfoItemTitle>
                            <S.InfoItemDescription>
                                {props.order.isDemand
                                    ? `с ${generateDate(props.order.lastAptEtaDatetime)}`
                                    : 'сегодня'}
                            </S.InfoItemDescription>
                        </S.InfoItem>
                    )}
                    <S.InfoItem>
                        <S.InfoItemTitle>Время работы:</S.InfoItemTitle>
                        <S.InfoItemDescription>{props.order.stockData.timeString}</S.InfoItemDescription>
                    </S.InfoItem>
                </S.InfoBlock>
            )
        }
        return (
            <S.InfoBlock>
                <S.InfoItem>
                    <S.InfoItemTitle>Доставка:</S.InfoItemTitle>
                    <S.InfoItemDescription>{props.order.deliveryData.shipAddressText}</S.InfoItemDescription>
                </S.InfoItem>
                {props.order.state.isShowBuyDate && (
                    <S.InfoItem>
                        <S.InfoItemTitle>Время доставки:</S.InfoItemTitle>
                        <S.InfoItemDescription>{props.order.deliveryData.timeSlotText}</S.InfoItemDescription>
                    </S.InfoItem>
                )}
            </S.InfoBlock>
        )
    }

    return (
        <S.Container>
            <S.TitleBlock>
                <S.TitleFirst>
                    № {props.order.no} от {generateDate(props.order.date)}
                </S.TitleFirst>
                <S.TitleLast>
                    <S.OrderSumBlock>
                        <S.OrderSumTitle>Сумма:</S.OrderSumTitle>
                        <S.OrderSum>
                            {props.order.sum?.toLocaleString('RU-ru') || ''} <SymbolRuble />
                        </S.OrderSum>
                    </S.OrderSumBlock>
                </S.TitleLast>
            </S.TitleBlock>
            {getInfoBLock()}
            <S.Footer>
                <S.FooterButtonMenu>
                    <Button
                        elementSizeTypeValue={Theme.ElementSizeType.regular}
                        colors={Theme.successButtonColorBlcackLight}
                        clickHandler={clickMoreHandler}>
                        {props.isMore ? 'К заказам' : 'Подробнее'}
                    </Button>
                    {/*  {props.order.state.isAvailablePaid && (
                        <Button
                            elementSizeTypeValue={Theme.ElementSizeType.regular}
                            colors={Theme.successButtonColorBlcackLight}
                            clickHandler={() => {}}>
                            Оплатить заказ
                        </Button>
                    )}
                    {props.order.state.isRevocable && (
                        <Button
                            elementSizeTypeValue={Theme.ElementSizeType.regular}
                            colors={Theme.deleteButtonColors}
                            clickHandler={() => {}}>
                            Отменить заказ
                        </Button>
                    )}
                    {props.order.state.isDeletable && (
                        <Button
                            elementSizeTypeValue={Theme.ElementSizeType.regular}
                            colors={Theme.deleteButtonColors}
                            clickHandler={() => {}}>
                            Удалить заказ
                        </Button>
                    )} */}
                </S.FooterButtonMenu>
                <S.StatusBlock>
                    {/* <S.StatusItem>
                        <S.StatusTitle>Статус заказа</S.StatusTitle>
                        <S.Status>{props.order.state.title}</S.Status>
                    </S.StatusItem> */}
                    {/* <S.StatusItem>
                        <S.StatusTitle>Статус оплаты</S.StatusTitle>
                        <S.Status>{props.order.state.paymentTitle}</S.Status>
                    </S.StatusItem> */}
                </S.StatusBlock>
            </S.Footer>
            {more.isOpened && (
                <S.OrderDetailsBlock>
                    {more.orderDetails.items &&
                        more.orderDetails.items.map((good) => (
                            <CatalogGoodItem
                                key={good.webData.goodId}
                                // basketItemCount={getBasketItem(good.webData.goodId)}
                                count={good.count}
                                availableStocksCount={good.availableStocksCount}
                                fixPrice={good.fixPrice}
                                analoguesQty={good.analoguesQty}
                                anotherOutFormsQty={good.anotherOutFormsQty}
                                webData={good.webData}
                                isStrictlyByPrescription={good.isStrictlyByPrescription}
                                isOriginalGood={good.isOriginalGood}
                                isCold={good.isCold}
                                shipment={shipment}
                                options={{
                                    isPositions: true
                                }}
                            />
                        ))}
                </S.OrderDetailsBlock>
            )}
        </S.Container>
    )
}

HistoryOrderItem.propTypes = {
    isMore: PropTypes.bool,
    order: {
        id: PropTypes.number.isRequired,
        sum: PropTypes.number.isRequired,
        no: PropTypes.string.isRequired,
        date: PropTypes.string.isRequired,
        stateTitle: PropTypes.string.isRequired,
        statePaymentTitle: PropTypes.string,
        isDelivery: PropTypes.bool.isRequired,
        stockData: PropTypes.shape({
            id: PropTypes.number,
            title: PropTypes.string,
            address: PropTypes.string,
            timeString: PropTypes.string
        }),
        deliveryData: PropTypes.shape({
            timeSlotText: PropTypes.string,
            shipAddressText: PropTypes.string,
            paymentTypeName: PropTypes.string
        })
    }
}

HistoryOrderItem.defaultProps = {}

export default HistoryOrderItem
