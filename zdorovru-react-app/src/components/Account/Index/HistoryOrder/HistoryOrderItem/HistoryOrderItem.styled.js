import styled from 'styled-components'

export const Container = styled.div`
    display: flex;
    width: 100%;
    flex-direction: column;
    padding: 1rem 0;
    border-bottom: 1px solid ${(props) => props.theme.colors.accentGreen};
    &:first-child {
        border-top: 1px solid ${(props) => props.theme.colors.accentGreen};
    }
    & > * {
        margin-bottom: 0.5rem;
    }
    & > *:last-child {
        margin-bottom: 0;
    }
`

export const TitleBlock = styled.div`
    font-size: 1.25rem;
    display: flex;
    justify-content: space-between;
`
export const TitleFirst = styled.div``
export const TitleLast = styled.div``

export const InfoBlock = styled.div`
    display: flex;
    flex-wrap: wrap;
    max-width: 40rem;
    width: 100%;
`
export const InfoItem = styled.div`
    display: flex;
    margin-right: 0.5rem;
`

export const InfoItemTitle = styled.span`
    color: ${(props) => props.theme.colors.grayAkm40};
    margin-right: 0.3rem;
`
export const InfoItemDescription = styled.span`
    color: ${(props) => props.theme.colors.getGrayColor()};
`

export const Footer = styled.div`
    display: flex;
    justify-content: space-between;
`

export const FooterButtonMenu = styled.div`
    display: flex;
    flex-wrap: wrap;

    & > button {
        //width: calc((100% / 2.8) - (1rem / 7));
        //flex: 1 1 calc((100% / 3) - 2rem);
        margin-right: 0.4rem;
        margin-bottom: 0.4rem;
    }
    margin-right: 1rem;
`

export const OrderSumBlock = styled.div`
    display: flex;
    font-size: 1.25rem;
    font-weight: 500;
    flex-wrap: wrap;
    margin-right: 6px;
`

export const OrderSumTitle = styled.span`
    color: ${(props) => props.theme.colors.grayAkm40};
    margin-right: 6px;
`

export const OrderSum = styled.span``

export const StatusBlock = styled.div`
    display: flex;
    flex-direction: column;
    //min-width: 10rem;
`
export const StatusItem = styled.div`
    display: flex;
    flex-wrap: wrap;
`
export const StatusTitle = styled.span`
    color: ${(props) => props.theme.colors.grayAkm40};
    margin-right: 6px;
`
export const Status = styled.span``

export const OrderDetailsBlock = styled.div`
    display: flex;
    flex-direction: column;
`
export const OrderDetailsItemsBlock = styled.div`
    display: flex;
    flex-direction: column;
`
