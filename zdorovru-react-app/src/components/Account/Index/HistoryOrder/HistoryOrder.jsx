import React from 'react'
import PropTypes from 'prop-types'
import * as S from './HistoryOrder.styled'
import HistoryOrderItem from './HistoryOrderItem/HistoryOrderItem'
import { useRouter } from 'next/router'

const HistoryOrder = (props) => {
    const router = useRouter()
    const selectOrderHandler = async (orderId) => {
        router.push({
            pathname: '/account/order/[orderId]',
            query: { orderId: orderId }
        })
    }
    return (
        <S.Container>
            {props.orders.map((x) => (
                <HistoryOrderItem key={x.id} order={x} selectOrderHandler={selectOrderHandler} />
            ))}
        </S.Container>
    )
}

HistoryOrder.propTypes = {
    orders: PropTypes.array.isRequired,
    selectOrderHandler: PropTypes.func.isRequired
}

export default HistoryOrder
