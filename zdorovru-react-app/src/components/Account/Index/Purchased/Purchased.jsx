import React from 'react'
import { usePurchased } from '@hooks/account/purchased-hook'
import CatalogGoodsWithFilters from '../../../CatalogGoodsWithFilters/CatalogGoodsWithFilters'

const Purchased = () => {
    const pur = usePurchased()
    return (
        <CatalogGoodsWithFilters
            options={{
                isLeftBlock: false,
                pur
            }}
        />
    )
}

Purchased.propTypes = {}

export default Purchased
