import React, { useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { initGoodsFromLiked } from '@actions/catalog'
import CatalogGoodsWithFilters from '../../../CatalogGoodsWithFilters/CatalogGoodsWithFilters'

const Liked = () => {
    const dispatch = useDispatch()

    const initGoods = async () => {
        await dispatch(initGoodsFromLiked())
    }

    useEffect(() => {
        initGoods()
    }, [])
    return (
        <CatalogGoodsWithFilters
            options={{
                isLeftBlock: false
            }}
        />
    )
}

export default Liked
