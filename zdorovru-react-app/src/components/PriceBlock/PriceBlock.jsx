import React from 'react'
import PropTypes from 'prop-types'
import * as S from './PriceBlock.styled'
import Button from '../shared/Button/Button'
import SymbolRuble from '../shared/SymbolRuble/SymbolRuble'
import CountChanger from '../shared/CountChanger/CountChanger'
import { _PricePropTypes } from '../../consts'

export const PriceType = {
    Delivery: 1,
    Stock: 2
}

const PriceBlock = (props) => {
    const isPriceAvailible = () => {
        if (!props.stockPrice && !props.deliveryPrice) return false
        if (props.stockPrice.normal || props.deliveryPrice.normal) return true
        return false
    }

    if (!isPriceAvailible()) {
        return (
            <S.Container>
                <S.TextNotAvailible>Нет в наличии</S.TextNotAvailible>
            </S.Container>
        )
    }

    const getPriceBlock = (price, priceType) => {
        if (!price || !price.normal) {
            return priceType === PriceType.Stock ? (
                <S.Price>
                    <S.TextNotAvailible>Данный товар отсутствует в выбранной аптеке</S.TextNotAvailible>
                </S.Price>
            ) : (
                <React.Fragment />
            )
        }
        const description = priceType === PriceType.Delivery ? 'Цена доставки' : 'Цена самовывоза'
        return (
            <S.Price>
                <S.PriceDescription>{description}</S.PriceDescription>
                <S.PriceDataBody>
                    {priceType === PriceType.Stock && props.isMinPrice ? (
                        <S.PricePrefixText>от</S.PricePrefixText>
                    ) : (
                        <React.Fragment />
                    )}
                    <S.PriceDataActual>
                        <S.PriceNumberActualBold>{price.discount}</S.PriceNumberActualBold>
                        <SymbolRuble />
                    </S.PriceDataActual>
                </S.PriceDataBody>
            </S.Price>
        )
    }

    return (
        <S.Container>
            <S.PriceDataContainer>
                {getPriceBlock(props.deliveryPrice, PriceType.Delivery)}
                {getPriceBlock(props.stockPrice, PriceType.Stock)}
            </S.PriceDataContainer>
            <S.ButtonBasketContainer>
                {props.isInBasket ? (
                    <CountChanger value={props.count} changeValueHandler={props.changePriceHandler} />
                ) : (
                    <Button clickHandler={props.addBasketHandler}>В корзину</Button>
                )}
            </S.ButtonBasketContainer>
        </S.Container>
    )
}

PriceBlock.propTypes = {
    stockPrice: _PricePropTypes,
    deliveryPrice: _PricePropTypes,
    count: PropTypes.number,
    isMinPrice: PropTypes.bool.isRequired,
    isInBasket: PropTypes.bool.isRequired,
    addBasketHandler: PropTypes.func.isRequired,
    changePriceHandler: PropTypes.func.isRequired,
    selectCount: PropTypes.number
}

export default PriceBlock
