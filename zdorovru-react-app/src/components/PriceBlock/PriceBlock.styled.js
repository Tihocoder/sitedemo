import styled from 'styled-components'
import * as Theme from '../../styles/theme-const'
import * as CC from '../shared/CountChanger/CountChanger.styled'

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    height: 100%;
`

export const TextNotAvailible = styled.div`
    color: ${(props) => props.theme.colors.getGrayColor(0.7)};
`

export const PriceDataContainer = styled.div``

export const Price = styled.div`
    flex-direction: column;
    display: flex;
    align-items: flex-end;
`
export const PriceDataBody = styled.div`
    display: flex;
`
export const PriceDescription = styled.span`
    color: ${(props) => props.theme.colors.getGrayColor(0.7)};
    font-size: 0.8rem;
`
export const PricePrefixText = styled.div`
    font-size: 1.4rem;
    color: ${Theme.ColorConsts.black};
    margin-right: 0.2rem;
`

export const PriceDataActual = styled.div`
    font-size: 1rem;
    display: flex;
    flex-wrap: nowrap;
    flex-direction: row;
    align-items: center;
    & span:first-child {
        margin-right: 0.2rem;
    }
`
export const PriceDataOld = styled.div`
    font-size: 0.8rem;
    display: flex;
    flex-direction: row;
    flex-wrap: nowrap;
    color: ${Theme.ColorConsts.accentGreenLight};
    text-decoration: line-through;
    align-items: center;
    margin-right: 0.3rem;
    & span:first-child {
        margin-right: 0.2rem;
    }
`
export const PriceNumberActualBold = styled.span`
    font-weight: bold;
    font-size: 1.4rem;
`
export const PriceNumberOld = styled.span`
    font-size: 1rem;
`
export const ButtonBasketContainer = styled.div`
    font-size: 1.4rem;
    box-sizing: border-box;
    display: flex;
    justify-content: flex-end;
    ${CC.Container} {
        max-height: 2.5rem;
    }
    & > button {
        font-size: 1rem;
    }
`

export const Button = styled.button`
    border: 0;
    background-color: rgba(147, 193, 26, 1);
    color: #ffffff;
    border-radius: 0.3rem;
    -webkit-transition: color 0.2s ease;
    transition: color 0.2s ease;
    font-size: inherit;
    cursor: pointer;
    font-size: 1rem;
    padding: 0.7rem 1.3rem;
    outline: none;
    box-shadow: 0 4px 20px 0 rgba(0, 0, 0, 0.14), 0 7px 10px -5px rgba(147, 193, 26, 0.4);
    box-sizing: border-box;
`
