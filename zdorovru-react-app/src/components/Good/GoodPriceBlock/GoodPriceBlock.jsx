import React from 'react'
import PropTypes from 'prop-types'
import * as S from './GoodPriceBlock.styled'
import * as Theme from '../../../styles/theme-const'
import { _PricePropTypes } from '../../../consts'
import Button from '../../shared/Button/Button'
import CountChanger from '../../shared/CountChanger/CountChanger'
import {
    ColorConsts,
    ElementSizeType,
    errorButtonLightColors,
    grayButtonLight
} from '../../../styles/theme-const'
import PriceItem from '../GoodPriceItem'
import { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { toggleLikedGood } from '../../../actions/customer'
import LikeIcon from '../../Icons/LikeIkon'
import Marker from '../../shared/Marker/Marker'
import { generateDate } from '../../../helpers/site'
import ReactTooltip from 'react-tooltip'
import GoodPriceTooltipe from '../GoodPriceTooltipe/GoodPriceTooltipe'

export const PriceType = {
    Delivery: 1,
    Stock: 2
}
export const buttonDeliveryNotAvailableColors = {
    backgroundColor: ColorConsts.white,
    color: 'rgba(0, 0, 0, 0.87)',
    hoverBackgroundColor: ColorConsts.redLightBackground,
    shadowColor: ColorConsts.redLight,
    shadowHoverColor: ColorConsts.redLightBackground,
    borderColor: ColorConsts.redLight
}

const GoodPriceBlock = (props) => {
    const dispatch = useDispatch()
    const likedGoods = useSelector((state) => state.customer.likedGoods)

    const isStockAvl = props.isAvailableOnStock && props.stockPrice.normal
    const isDeliveryAvl = props.deliveryStatus.isAvailableForDelivery && props.deliveryPrice.normal

    const toggleLikeHandler = async (webId) => {
        await dispatch(toggleLikedGood(webId))
    }

    if (!props.isAvailable) {
        return (
            <S.Container>
                <S.ButtonBasketContainer>
                    <S.DeleteContainer>
                        <Button elementSizeTypeValue={ElementSizeType.regular} colors={grayButtonLight}>
                            Нет в наличии
                        </Button>
                    </S.DeleteContainer>
                </S.ButtonBasketContainer>
            </S.Container>
        )
    }

    const clickHandlerDeliveryReasons = () => {
        props.openDeliveryReasons(
            props.deliveryStatus.reasons.map((reason) => ({
                id: reason.deliveryReasonID,
                text: reason.warningText
            }))
        )
    }

    const getAptEtaDateTime = (isStockAvl, isDemand, aptEtaDateTime) => {
        if (!isStockAvl || !isDemand) return <React.Fragment />
        let currentDate = new Date()
        currentDate.setDate(currentDate.getDate() + 1)
        if (generateDate(currentDate) === generateDate(aptEtaDateTime)) {
            return <Marker backgroundColor={Theme.ColorConsts.accentRed}>завтра</Marker>
        } else return <Marker>{generateDate(aptEtaDateTime)}</Marker>
    }

    const generatePrice = () => {
        let prices = []
        prices.push(
            <S.PriceItemBlock>
                <PriceItem
                    isSelectedStock={props.isSelectedStock}
                    availableStocksCount={props.availableStocksCount}
                    clickOpenAnotherStockHandler={props.clickOpenAnotherStockHandler}
                    isFrom={!isStockAvl}
                    price={isStockAvl ? props.stockPrice : props.minPrice}
                    stockPrice={props.stockPrice}
                    deliveryPrice={props.deliveryPrice}
                    minPrice={props.minPrice}
                    deliveryStatus={props.deliveryStatus}
                    isAvailable={props.isAvailable}
                    isAvailableOnStock={props.isAvailableOnStock}
                    expanstion={getAptEtaDateTime(
                        isStockAvl,
                        props.stockPrice.isDemand,
                        props.stockPrice.aptEtaDateTime
                    )}
                    isShowHelper
                    title={'Самовывоз'}
                />
            </S.PriceItemBlock>
        )
        if (isDeliveryAvl) {
            prices.push(<PriceItem price={props.deliveryPrice} title={'Цена доставки'} />)
        } else {
            prices.push(
                <S.Price>
                    <S.ButtonBasketContainer>
                        <Button
                            colors={errorButtonLightColors}
                            clickHandler={clickHandlerDeliveryReasons}
                            textTransform={'none'}
                            elementSizeTypeValue={ElementSizeType.regular}>
                            Доставка невозможна
                        </Button>
                    </S.ButtonBasketContainer>
                </S.Price>
            )
        }
        return prices.map((x, index) => React.cloneElement(x, { key: index }))
    }
    const likedColor = likedGoods.includes(props.goodId)
        ? Theme.ColorConsts.pureRed
        : Theme.ColorConsts.getGrayColor(0.1)

    return (
        <S.Container>
            <S.ButtonBasketContainer>
                {props.count ? (
                    <CountChanger value={props.count} changeValueHandler={props.changePriceHandler} />
                ) : (
                    <Button
                        elementSizeTypeValue={ElementSizeType.regular}
                        clickHandler={props.addBasketHandler}>
                        В корзину
                    </Button>
                )}
                <S.LikedContainer>
                    <S.LikedBlock>
                        <S.LikedTitle>Избранное</S.LikedTitle>
                        <S.LikedIconBlock onClick={() => toggleLikeHandler(props.goodId)}>
                            <LikeIcon color={likedColor} />
                        </S.LikedIconBlock>
                    </S.LikedBlock>
                </S.LikedContainer>
            </S.ButtonBasketContainer>
            <S.PriceDataContainer>{generatePrice()}</S.PriceDataContainer>
        </S.Container>
    )
}

GoodPriceBlock.propTypes = {
    stockPrice: _PricePropTypes,
    deliveryPrice: _PricePropTypes,
    minPrice: _PricePropTypes,
    deliveryStatus: PropTypes.shape({
        isAvailableForDelivery: PropTypes.bool.isRequired,
        reasons: PropTypes.arrayOf(
            PropTypes.shape({
                deliveryReasonID: PropTypes.number.isRequired,
                warningText: PropTypes.string
            })
        )
    }),
    count: PropTypes.number,
    isAvailable: PropTypes.bool.isRequired,
    isAvailableOnStock: PropTypes.bool.isRequired,
    addBasketHandler: PropTypes.func.isRequired,
    changePriceHandler: PropTypes.func.isRequired,
    showedAnotherStocks: PropTypes.func.isRequired,
    openDeliveryReasons: PropTypes.func.isRequired,
    closeDeliveryReasons: PropTypes.func.isRequired,
    hiddenItemHandler: PropTypes.func,
    deletePositionHandler: PriceType.func,
    isSelectedStock: PropTypes.bool.isRequired,
    goodName: PriceType.string,
    options: PropTypes.shape({
        isDelivery: PropTypes.bool
    })
}

GoodPriceBlock.defaultProps = {}

export default GoodPriceBlock
