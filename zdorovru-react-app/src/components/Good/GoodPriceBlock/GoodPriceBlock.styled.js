import styled from 'styled-components'
import * as CC from '../../shared/CountChanger/CountChanger.styled'

export const Container = styled.div`
    display: flex;
    // justify-content: space-between;
    height: 100%;
    min-height: 6.5rem;
    font-size: 1.1rem;
    align-items: flex-start;
    //max-width: 9.4rem;
    width: 100%;
    justify-content: space-between;
    & > div:first-child {
        margin-right: 1.25rem;
    }
`

export const PriceItemBlock = styled.div`
    display: flex;
`

export const TextNotAvailible = styled.div`
    color: ${(props) => props.theme.colors.getGrayColor(0.7)};
`

export const PriceDataContainer = styled.div`
    display: flex;
    flex-direction: column;
    align-items: flex-end;
    height: 100%;
    justify-content: space-between;
`

export const AnotherStocksWarning = styled.span`
    color: ${(props) => props.theme.colors.pureRed};
    cursor: pointer;
    :hover {
        color: ${(props) => props.theme.colors.pureRedLight};
    }
    font-size: 0.82rem;
    text-align: right;
`

export const Price = styled.div`
    flex-direction: column;
    display: flex;
    align-items: flex-end;
    & > button {
        font-size: 0.82rem;
        width: 9.2rem;
        padding: 0.6rem 1.4rem;
    }
`

export const AnotherStocksLink = styled.a`
    font: inherit;
    cursor: pointer;
    font-size: 0.8rem;
    color: ${(props) => props.theme.colors.getGrayColor(0.6)};
    text-decoration: underline;
`

export const PriceDataBody = styled.div`
    display: flex;
    align-items: center;
`

export const PriceNumberSaleBold = styled.span`
    font-weight: bold;
    color: ${(props) => props.theme.colors.pureRed};
    white-space: nowrap;
`

export const PriceDataBodyChangeStock = styled.div`
    display: flex;
    align-items: center;
    border-bottom: 1px dashed ${(props) => props.theme.colors.getGrayColor(0.7)};
    line-height: 1.3rem;
    :hover {
        border-bottom: 1px dashed ${(props) => props.theme.colors.getGrayColor(0.5)};
        color: ${(props) => props.theme.colors.getGrayColor(0.5)};
        ${PriceNumberSaleBold} {
            color: ${(props) => props.theme.colors.pureRedLight};
        }
    }
    cursor: pointer;
`

export const PriceDescription = styled.span`
    color: ${(props) => props.theme.colors.getGrayColor(0.7)};
    font-size: 0.82rem;
`

export const PriceDataActual = styled.div`
    display: flex;
    flex-wrap: nowrap;
    flex-direction: row;
    align-items: center;
    & span:first-child {
        margin-right: 0.2rem;
    }
`
export const PriceDataOld = styled.div`
    font-size: 1.05rem;
    display: flex;
    flex-direction: row;
    flex-wrap: nowrap;
    text-decoration: line-through;
    align-items: center;
    margin-right: 0.3rem;
    & span:first-child {
        margin-left: 0.2rem;
    }
`
export const PriceNumberActualBold = styled.span`
    font-weight: bold;
`

export const PriceNumberOld = styled.span`
    font-size: 1rem;
    white-space: nowrap;
`
export const ButtonBasketContainer = styled.div`
    font-size: 1rem;
    box-sizing: border-box;
    display: flex;
    justify-content: space-between;
    flex-direction: column;
    width: 8rem;
    height: 100%;
    ${CC.Container} {
        max-height: 2.5rem;
        width: 100%;
    }
    & > *:last-child {
        margin-top: 0.5rem;
    }
    & > button {
        text-align: center;
        padding: 0.5rem 1rem;
        width: 100%;
    }
`

export const DeleteContainer = styled.div`
    display: flex;
    margin-top: 0.5rem;
    flex-direction: column;
    justify-content: space-between;
    height: 8.3rem;
    & button {
        padding: 0.5rem 1rem;
        font-size: 1rem;
    }
`
export const DeleteContainerWithoutNotAvailable = styled.div`
    display: flex;
    margin-top: 0.5rem;
    flex-direction: column;
    justify-content: space-between;
    height: 4.56rem;
    & button {
        padding: 0.5rem 1rem;
        font-size: 1rem;
    }
`

export const WarningLabel = styled.div`
    color: ${(props) => props.theme.getRedheadColor(0.8)};
`
export const LikedContainer = styled.div`
    display: flex;
    justify-content: flex-start;
`

export const LikedBlock = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    width: 100%;
`

export const LikedTitle = styled.span`
    color: ${(props) => props.theme.colors.getGrayColor(0.7)};
    font-size: 0.75rem;
`

export const LikedIconBlock = styled.div`
    cursor: pointer;
    padding: 0.2rem;
    width: 1.8rem;
    height: 1.6rem;
`
