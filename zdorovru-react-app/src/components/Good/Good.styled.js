import styled, { css } from 'styled-components'
import * as Theme from '../../styles/theme-const'

export const Container = styled.div`
    margin-top: 2rem;
    display: flex;
    flex-direction: column;
`

export const IntersectionWrapper = styled.div``
export const GoodDataContainer = styled.div`
    margin: 0;
    width: 100%;
    padding: 0 3rem 0 3rem;
    box-sizing: border-box;
`
export const GoodExtendedDataContainer = styled.div`
    width: 100%;
    padding: 3rem 0;
    box-sizing: border-box;
`

export const NameBlock = styled.div``

export const BodyModel = styled.div`
    max-width: 32rem;
`
export const StickyBlock = styled.div`
    position: sticky;
    top: 0;
    //background-color: ${(props) => props.theme.colors.grayLightColor};
    /* ${(props) =>
        props.isSticky &&
        css`
            & > * {
                box-shadow: -1px 15px 8px -8px rgba(115, 140, 134, 0.25);
            }
        `} */

    & > div + & > div {
        margin-left: 1rem;
    }

    display: flex;
    justify-content: space-between;
`
export const StickyBlockGoodNav = styled.div`
    width: 100%;
    box-shadow: ${(props) => props.theme.shadows.shadowKit};
    background-color: ${(props) => props.theme.colors.white};
    border-radius: 8px;
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    padding-left: 0.2rem;
`

export const StickyBlockPriceBlock = styled.div`
    padding: 0.4rem;
    margin-left: 1rem;
    min-width: 19rem;
    box-shadow: ${(props) => props.theme.shadows.shadowKit};
    background-color: ${(props) => props.theme.colors.white};
    border-radius: 8px;
    display: flex;
    justify-content: flex-start;
`

export const LikedContainer = styled.div`
    display: flex;
    align-items: flex-end;
`
export const LikedIconBlock = styled.div`
    cursor: pointer;
    padding: 0.2rem;
    padding-right: 0.6rem;
    width: 1.8rem;
`

export const GeneralDataContainer = styled.div`
    display: flex;
    justify-content: space-between;
    width: 100%;
    align-items: flex-start;
    padding: 1rem 0;
`
export const ImageContainer = styled.div`
    height: 100%;
    box-sizing: border-box;
    display: flex;
    align-items: flex-end;
    cursor: ${(props) => props.isPointer && 'pointer'};
`

export const InfoContainer = styled.div`
    display: flex;
    height: 100%;
    width: 100%;
    flex-direction: column;
    justify-content: space-between;
    margin-left: 2rem;
`
export const InfoList = styled.div`
    max-width: 26.5rem;
    display: flex;
    flex-wrap: wrap;
`

export const FullInfo = styled.div``

export const FullTitle = styled.div`
    color: ${Theme.ColorConsts.getGrayColor(1)};
    font-size: 1.3rem;
    margin: 1rem 0;
`
export const FullTitleSpan = styled.span`
    padding-left: 0.2rem;
`

export const TextInfoContainer = styled.div``

export const ButtonBasketContainer = styled.div`
    width: 11rem;
`

export const TextInfo = styled.div`
    font-size: 0.9rem;
    display: flex;
    margin-bottom: 0.4rem;
`

export const TextInfoName = styled.div`
    color: ${Theme.ColorConsts.getGrayColor(0.6)};
`

export const TextInfoDescription = styled.div`
    color: ${Theme.ColorConsts.getGrayColor(0.75)};
    font-weight: bold;
`

export const PriceContainer = styled.div`
    display: flex;
    width: 14rem;
    flex-direction: column;
    justify-content: space-between;
    align-items: flex-end;
    height: 100%;
`

export const NotFoundText = styled.div`
    color: ${Theme.ColorConsts.getGrayColor(0.6)};
`
export const PriceDataContainer = styled.div``

export const Price = styled.div`
    flex-direction: column;
    display: flex;
    align-items: flex-end;
`
export const MarkerBlock = styled.div`
    display: flex;
    flex-wrap: wrap;
    & > div {
        margin: 0.2rem;
    }
`

export const PriceDescription = styled.span`
    color: ${Theme.ColorConsts.getGrayColor(0.6)};
    font-size: 0.8rem;
`
export const PriceData = styled.div`
    color: ${Theme.ColorConsts.accentGreen};
    font-size: 1.3rem;
    display: flex;
    flex-wrap: nowrap;
`

export const GoodImage = styled.img`
    height: auto;
    width: 240px;
`

export const InfoListBlock = styled.div`
    display: flex;
    flex-direction: column;
    & table {
        border: 1px solid ${(props) => props.theme.colors.getGrayColor(0.05)};
        border-collapse: collapse;
    }
    /* & table tr {
        border: 1px solid ${(props) => props.theme.colors.getGrayColor(0.04)};
        border-collapse: collapse;
    } */
    & table td {
        padding: 0.1rem;
        border: 1px solid ${(props) => props.theme.colors.getGrayColor(0.05)};
    }
    & table .PrepInSost {
        padding: 3px;
        background: ${(props) => props.theme.colors.getGrayColor(0.04)};
    }
    & table .PrepInSost_Val {
        padding: 3px;
        background: ${(props) => props.theme.colors.getGrayColor(0.04)};
    }
`

export const InfoItem = styled.div`
    display: flex;
    flex-direction: column;
    margin-bottom: 1rem;
`
export const InfoItemTitle = styled.div`
    color: ${Theme.ColorConsts.getGrayColor(0.75)};
    font-weight: bold;
    font-size: 1.3rem;
    margin-bottom: 0.2rem;
`
export const InfoItemBody = styled.div`
    color: ${Theme.ColorConsts.getGrayColor(0.6)};
`
export const LineTabs = styled.div`
    width: 100%;
    height: 2rem;
    display: flex;
    box-sizing: border-box;
    margin-bottom: -0.3rem;

    //border-top: 1px solid ${(props) => props.theme.colors.getGrayColor(0.1)};
    //background-color: ${(props) => props.theme.colors.backgroundAkmGreen};
`
export const CountIndicate = styled.div`
    font-weight: bold;
    border-radius: 0.5rem;
    padding: 0.15rem 0.35rem;
    height: 0.8rem;
    display: flex;
    margin-left: 0.5rem;
    justify-content: center;
    align-items: center;
    font-size: 0.82rem;
`
export const Tab = styled.div`
    height: 100%;
    padding: 0 0.2rem;
    cursor: pointer;
    /* border-top: 2px solid #5BD240; */
    display: flex;
    align-items: center;
    margin-right: 0.625rem;
    color: ${(props) => props.theme.colors.getGrayColor(1)};
    ${CountIndicate} {
        background-color: ${(props) => props.theme.colors.grayAkmBackgroundGray};
        color: ${(props) => props.theme.colors.grayText};
    }

    ${(props) =>
        props.isSelected &&
        css`
            // background-color: ${(props) => props.theme.colors.accentGreen};
            border-bottom: 3px solid ${(props) => props.theme.colors.accentGreen};
            // color: ${(props) => props.theme.colors.white};
            font-weight: bold;
            ${CountIndicate} {
                background-color: ${(props) => props.theme.colors.getRedColor(0.6)};
                color: white;
            }
        `}
`

export const ReasonContainer = styled.div`
    display: flex;
    flex-direction: column;
`

export const ReasonTextItem = styled.div``

export const ModalIsNotShipmentContainer = styled.div`
    display: flex;
    flex-direction: column;
    max-width: 32rem;
`
export const ModalIsNotShipmentFooter = styled.div`
    width: 100%;
    display: flex;
    justify-content: flex-end;
`
export const ModalIsNotShipmentFooterButtons = styled.div`
    display: flex;
    flex-direction: column;
    max-width: 17rem;
    margin: -3px 0;
    & button {
        width: 100%;
        flex: 1 1 auto;
        margin: 3px 0;
    }
`

export const ModalIsNotShipmentP = styled.p``

export const ModalIsNotShipmentUl = styled.ul``

export const ModalIsNotShipmentLi = styled.li``
