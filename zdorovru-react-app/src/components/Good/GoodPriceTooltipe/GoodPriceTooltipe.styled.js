import styled from 'styled-components'

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    color: ${(props) => props.theme.colors.getGrayColor(1)};
`
export const Title = styled.h2`
    margin: 0;
    font-size: 1rem;
`

export const Body = styled.div`
    display: flex;
    flex-direction: column;
    font-size: 0.85;
    margin-top: 1rem;
`
export const PriceItem = styled.div`
    display: flex;
    justify-content: space-between;
    padding: 0.4rem 0;
`
export const PriceDescription = styled.div``

export const PriceNumber = styled.span`
    font-size: 1rem;
    margin-left: 0.5rem;
    font-weight: bold;
    white-space: nowrap;
`
