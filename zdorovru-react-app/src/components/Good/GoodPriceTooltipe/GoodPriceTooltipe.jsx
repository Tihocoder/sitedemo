import React from 'react'
import PropTypes from 'prop-types'
import * as S from './GoodPriceTooltipe.styled'
import { _PricePropTypes } from '../../../consts'
import SymbolRuble from '../../shared/SymbolRuble/SymbolRuble'

const GoodPriceTooltipe = (props) => {
    return (
        <S.Container>
            <S.Title>Цены на товар</S.Title>
            <S.Body>
            {props.isAvailableOnStock && props.stockPrice?.normal ? (
                    <S.PriceItem>
                        <S.PriceDescription>Цена в аптеке</S.PriceDescription>
                        <S.PriceNumber>
                            {props.stockPrice.normal} <SymbolRuble />
                        </S.PriceNumber>
                    </S.PriceItem>
                ) : (
                    <React.Fragment />
                )}
                  {props.isAvailableOnStock && props.stockPrice?.discount ? (
                    <S.PriceItem>
                        <S.PriceDescription>Цена в аптеке по карте постоянного покупателя</S.PriceDescription>
                        <S.PriceNumber>
                            {props.stockPrice.discount} <SymbolRuble />
                        </S.PriceNumber>
                    </S.PriceItem>
                ) : (
                    <React.Fragment />
                )}
                {props.isAvailableOnStock && props.stockPrice?.discount ? (
                    <S.PriceItem>
                        <S.PriceDescription>Цена самовывоза при бронировании на сайте</S.PriceDescription>
                        <S.PriceNumber>
                            {props.stockPrice.discount} <SymbolRuble />
                        </S.PriceNumber>
                    </S.PriceItem>
                ) : (
                    <React.Fragment />
                )}
                {props.isAvailableForDelivery && props.deliveryPrice?.discount ? (
                    <S.PriceItem>
                        <S.PriceDescription>Цена при заказе доставки</S.PriceDescription>
                        <S.PriceNumber>
                            {props.deliveryPrice.discount} <SymbolRuble />
                        </S.PriceNumber>
                    </S.PriceItem>
                ) : (
                    <React.Fragment />
                )}
            </S.Body>
        </S.Container>
    )
}

GoodPriceTooltipe.propTypes = {
    stockPrice: _PricePropTypes,
    deliveryPrice: _PricePropTypes,
    minPrice: _PricePropTypes,
    isAvailableOnStock: PropTypes.bool.isRequired,
    isAvailableForDelivery: PropTypes.bool.isRequired
}

export default GoodPriceTooltipe
