import React from 'react'
import PropTypes from 'prop-types'
import * as S from './GoodPriceItem.styled'
import { _PricePropTypes } from '../../consts'
import SymbolRuble from '../shared/SymbolRuble/SymbolRuble'
import { declOfNum } from '../../helpers/site'
import ReactTooltip from 'react-tooltip'
import GoodPriceTooltipe from './GoodPriceTooltipe/GoodPriceTooltipe'

const GoodPriceItem = ({
    title,
    price,
    isFrom,
    expanstion,
    availableStocksCount,
    isSelectedStock,
    clickOpenAnotherStockHandler,
    stockPrice,
    deliveryPrice,
    minPrice,
    deliveryStatus,
    isShowHelper,
    isAvailableOnStock
}) => {
    const getPriceActual = (price) => {
        if (price.withoutPromo) {
            return (
                <React.Fragment>
                    <S.PriceDataActual>
                        <S.PriceDataOld>
                            {price.withoutPromo} <SymbolRuble />
                        </S.PriceDataOld>
                        <S.PriceNumberSaleBold>
                            {price.normal.toLocaleString('ru-RU')} <SymbolRuble />
                        </S.PriceNumberSaleBold>
                    </S.PriceDataActual>
                </React.Fragment>
            )
        }
        return (
            <React.Fragment>
                <S.PriceDataActual>
                    <S.PriceNumberActualBold>
                        {price.normal.toLocaleString('ru-RU')} <SymbolRuble />
                    </S.PriceNumberActualBold>
                </S.PriceDataActual>
            </React.Fragment>
        )
    }
    return (
        <S.Price>
            <S.PriceRow>
                {isFrom && isSelectedStock && availableStocksCount && availableStocksCount > 0 ? (
                    <S.AnotherStocksWarning onClick={clickOpenAnotherStockHandler}>
                        Нет в выбранной, самовывоз из {availableStocksCount}{' '}
                        {declOfNum(availableStocksCount, ['аптеки', 'аптек', 'аптек'])}
                    </S.AnotherStocksWarning>
                ) : (
                    <React.Fragment />
                )}
                {isFrom && isSelectedStock && availableStocksCount === 0 ? (
                    <S.AnotherStocksWarning>Нет в выбранной</S.AnotherStocksWarning>
                ) : (
                    <React.Fragment />
                )}
                {!isFrom && (
                    <S.PriceDescriptionBlock>
                        <S.PriceDescription>{title}</S.PriceDescription>
                        {expanstion || <React.Fragment />}
                    </S.PriceDescriptionBlock>
                )}
                {!isFrom && isShowHelper ? (
                    <React.Fragment>
                        <S.HelpContainer>
                            <S.HelpImage data-tip="test" data-for="help-price" />
                        </S.HelpContainer>
                        <ReactTooltip
                            id="help-price"
                            place="left"
                            multiline={true}
                            backgroundColor="white"
                            className="class_tooltip"
                            effect="solid">
                            <GoodPriceTooltipe
                                stockPrice={stockPrice}
                                deliveryPrice={deliveryPrice}
                                minPrice={minPrice}
                                isAvailableOnStock={isAvailableOnStock}
                                isAvailableForDelivery={deliveryStatus.isAvailableForDelivery}
                            />
                        </ReactTooltip>
                    </React.Fragment>
                ) : (
                    <React.Fragment></React.Fragment>
                )}
            </S.PriceRow>
            {isFrom ? (
                <S.PriceDataBodyChangeStock onClick={clickOpenAnotherStockHandler}>
                    <S.PricePrefixText>от</S.PricePrefixText>
                    {getPriceActual(price)}
                </S.PriceDataBodyChangeStock>
            ) : (
                <S.PriceDataBody>{getPriceActual(price)}</S.PriceDataBody>
            )}
        </S.Price>
    )
}
GoodPriceItem.propTypes = {
    title: PropTypes.string,
    price: _PricePropTypes,
    availableStocksCount: PropTypes.number,
    isSelectedStock: PropTypes.bool.isRequired,
    expanstion: PropTypes.element,
    isShowHelper: PropTypes.bool.isRequired,
    clickOpenAnotherStockHandler: PropTypes.func
}

GoodPriceItem.defaultProps = {
    isFrom: false,
    isSelectedStock: true,
    isShowHelper: false
}

export default GoodPriceItem
