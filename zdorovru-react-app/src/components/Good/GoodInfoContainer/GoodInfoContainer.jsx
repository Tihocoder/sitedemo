import React from 'react'
import PropTypes from 'prop-types'
import * as S from './GoodInfoContainer.styled'
import { _PricePropTypes } from '../../../consts'
import { generateDate } from '../../../helpers/site'

const drawInfoItem = (title, text) => (
    <S.InfoItemBlock>
        <S.InfoItemBlockTitleContainer>
            <S.InfoItemBlockTitle>{title}:</S.InfoItemBlockTitle>
        </S.InfoItemBlockTitleContainer>
        <S.InfoItemBlockTextContainer>
            <S.InfoItemText>{text}</S.InfoItemText>
        </S.InfoItemBlockTextContainer>
    </S.InfoItemBlock>
)

const GoodInfoContainer = (props) => {
    let infoItems = []

    infoItems['makerTitle'] = drawInfoItem('Производитель', props.good.webData.makerTitle)
    const mnn = props.good.webData.mnnRusTitle
    if (mnn && mnn.toLowerCase().trim() !== 'не установлено' && mnn.toLowerCase().trim() !== '-') {
        infoItems.push(drawInfoItem('Действующее вещество', mnn))
    }
    if (
        props.good.deliveryPrice.isDemand &&
        props.good.stockPrice.isDemand &&
        generateDate(props.good.deliveryPrice.aptEtaDateTime) ===
            generateDate(props.good.stockPrice.aptEtaDateTime)
    ) {
        infoItems.push(drawInfoItem('Поставка', generateDate(props.good.stockPrice.aptEtaDateTime)))
    }

    if (
        props.good.deliveryPrice.isDemand &&
        props.good.stockPrice.isDemand &&
        generateDate(props.good.deliveryPrice.aptEtaDateTime) !==
            generateDate(props.good.stockPrice.aptEtaDateTime)
    ) {
        infoItems.push(drawInfoItem('Доставка', generateDate(props.good.deliveryPrice.aptEtaDateTime)))
        infoItems.push(drawInfoItem('Самовывоз', generateDate(props.good.stockPrice.aptEtaDateTime)))
    }

    if (props.good.deliveryPrice.isDemand && !props.good.stockPrice.isDemand) {
        infoItems['deliveryisDemand'] = drawInfoItem(
            'Доставка',
            generateDate(props.good.deliveryPrice.aptEtaDateTime)
        )
    }
    // если поставка только для cамовывоза
    if (!props.good.deliveryPrice.isDemand && props.good.stockPrice.isDemand) {
        infoItems.push(drawInfoItem('Самовывоз', generateDate(props.good.stockPrice.aptEtaDateTime)))
    }
    if (props.good.isCold) {
        infoItems.push(drawInfoItem('Хранение', 'В холоде'))
    }


    if (props.good.isStrictlyByPrescription) {
        infoItems.push(drawInfoItem('Рецепт', 'Строго по рецепту'))
    }
    return (
        <React.Fragment>
            {infoItems.map((Component, index) => React.cloneElement(Component, { key: index }))}
        </React.Fragment>
    )
}

GoodInfoContainer.propTypes = {
    good: PropTypes.shape({
        analoguesQty: PropTypes.bool,
        isStrictlyByPrescription: PropTypes.bool,
        isOriginalGood: PropTypes.bool,
        isHidden: PropTypes.bool.isRequired,
        isCold: PropTypes.bool,
        isDemand: PropTypes.bool,
        aptEtaDateTime: PropTypes.string,
        stockPrice: _PricePropTypes,
        deliveryPrice: _PricePropTypes
    })
}

export default GoodInfoContainer
