import styled from 'styled-components'

export const InfoItemBlock = styled.div`
    // padding: 0.2rem 1rem;
    display: flex;
    //justify-content: space-between;
    border-bottom: 1px solid ${(props) => props.theme.colors.getGrayColor(0.1)};
    // height: 1.7rem;
    align-items: center;
    width: 100%;
`
export const InfoItemBlockTitleContainer = styled.div`
    width: 14rem;
    height: 100%;
    padding: 0.5rem;
    box-sizing: border-box;
    flex-grow: 1;
`

export const InfoItemBlockTextContainer = styled.div`
    height: 100%;
    width: 100%;
    padding: 0.5rem;
    box-sizing: border-box;
    flex-grow: 1;
`

export const InfoItemText = styled.span``

export const InfoItemBlockTitle = styled.span`
    color: ${(props) => props.theme.colors.grayText};
`
