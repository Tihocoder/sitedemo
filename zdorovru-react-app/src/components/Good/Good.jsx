import React, { useContext, useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { useDispatch, useSelector } from 'react-redux'
import Head from 'next/head'
import { useRouter } from 'next/router'
import { useInView } from 'react-intersection-observer'
import { isIE } from 'react-device-detect'
import { _PricePropTypes, _WebDataPropTypes, FeaturesEnum } from 'src/consts'
import { getGoodStocks, getInfoList } from '@api/siteApis'
import { addBasketItem, changeBasketItemPrice, changeShipment, toggleLikedGood } from '@actions/customer'
import { AlertsStateContext } from '@context/AlertsState'
import { generateDate, generateTime, generateUrlImage } from '@helpers/site'
import { getAnaloguesAction, getAnotherOutFormsAction } from '@actions/catalog'
import { MenuContext } from '@context/CatalogMenuState'
import { ImageSizeType } from '@services/catalogService'
import * as S from './Good.styled'
import * as Theme from '../../styles/theme-const'
import CatalogGoodsWithFilters from '../CatalogGoodsWithFilters/CatalogGoodsWithFilters'
import ModalWindow from '../shared/ModalWindow/ModalWindow'
import ImageModal from '../shared/ImageModal/ImageModal'
import SelectorStock from '../SelectorStock/SelectorStock'
import ModalAddressSelect from '../Basket/BasketShipment/ModalAddressSelect/ModalAddressSelect'
import ModalWindowCustom from '../shared/ModalWindowCustom/ModalWindowCustom'
import Button from '../shared/Button/Button'
import GoodInfoContainer from './GoodInfoContainer/GoodInfoContainer'
import GoodPriceBlock from './GoodPriceBlock/GoodPriceBlock'

const CartTabTypes = {
    anotherOutForms: 1,
    infoList: 2,
    analogue: 3
}

const Good = (props) => {
    const router = useRouter()
    const [tabs, setTabs] = useState([])
    const [selectedTab, setSelectedTab] = useState(CartTabTypes.infoList)
    const basketItems = useSelector((state) => state.customer.basketItems)
    const [infoList, setInfoList] = useState([])
    const [isImageShowedModal, setIsImageShowedModal] = useState(false)
    const shipment = useSelector((state) => state.customer.shipment)
    const features = useSelector((state) => state.catalog.features)
    const baseImageUrl = useSelector((state) => state.catalog.baseImageUrl)
    const dispatch = useDispatch()
    const [isShowedModalDemand, setIsShowedModalDemand] = useState(false)
    const stocksAll = useSelector((state) => state.company.stocks)
    const cities = useSelector((state) => state.company.cities)
    const { deleteFromBasket } = useContext(AlertsStateContext)
    const { toggleStockSelectorByGood } = useContext(MenuContext)
    const [deliveryReasonState, setDeliveryReasonState] = useState({
        isShowedModal: false,
        reasons: []
    })

    const openDeliveryReasons = (reasons) => {
        setDeliveryReasonState({
            ...setDeliveryReasonState,
            isShowedModal: true,
            reasons
        })
    }
    const closeDeliveryReasons = () => {
        setDeliveryReasonState({
            ...setDeliveryReasonState,
            isShowedModal: false,
            reasons: []
        })
    }

    const clickOpenAnotherStockHandler = () => {
        toggleStockSelectorByGood([props.good.webData.goodId])
    }

    const [anotherStocks, setAnotherStocks] = useState({
        isShowedModal: false,
        stocks: []
    })

    const getIndicateTabs = (id) => {
        if (id === CartTabTypes.anotherOutForms && props.good.anotherOutFormsQty) {
            return <S.CountIndicate>{props.good.anotherOutFormsQty}</S.CountIndicate>
        }
        if (id === CartTabTypes.analogue && props.good.analoguesQty) {
            return <S.CountIndicate>{props.good.analoguesQty}</S.CountIndicate>
        }
        return <React.Fragment />
    }

    const showedAnotherStocks = async () => {
        const stocks = await getGoodStocks(props.good.webData.goodId, shipment.cityId)

        setAnotherStocks({
            isShowedModal: true,
            stocks
        })
    }
    const getFeatureHtml = () => {
        if (props.good.deliveryPrice.isDemand && props.good.stockPrice.isDemand) {
            return features[FeaturesEnum.DescriptionDemandStockAndDeliveryBasketAdd]
                .replace('[dateStock]', generateDate(props.good.stockPrice.aptEtaDateTime, false))
                .replace('[dateDelivery]', generateDate(props.good.deliveryPrice.aptEtaDateTime, false))
                .replace('[time]', generateTime(props.good.deliveryPrice.aptEtaDateTime))
        }
        if (props.good.deliveryPrice.isDemand) {
            return features[FeaturesEnum.DemandDeliveryBasketAdd]
                .replace('[date]', generateDate(props.good.deliveryPrice.aptEtaDateTime, false))
                .replace('[time]', generateTime(props.good.deliveryPrice.aptEtaDateTime))
        }
        if (props.good.stockPrice.isDemand) {
            return features[FeaturesEnum.DemandPVZBasketAdd].replace(
                '[date]',
                generateDate(props.good.stockPrice.aptEtaDateTime, true)
            )
        }
    }

    const [isShowedModalNotSelectedShipment, setIsShowedModalNotSelectedShipment] = useState(false)
    const [isShowedSelectorAddress, setIsShowedSelectorAddress] = useState(false)
    const [isShowedSelectorStock, setIsShowedSelectorStock] = useState(false)

    const selectedStocAndCitykHandler = async (id, cityId) => {
        await dispatch(changeShipment({ stockId: id, cityId }))
        setIsShowedSelectorStock(!isShowedSelectorStock)
        router.reload()
    }

    const changeSelectedAddressHandler = async (id) => {
        await dispatch(changeShipment({ shipAddressId: id }))
        setIsShowedSelectorAddress(false)
        router.reload()
    }

    const modalIsNotShipmentHandler = () => {
        setIsShowedModalNotSelectedShipment(false)
        setIsShowedSelectorStock(true)
    }
    const modalIsNotShipmentAddressHandler = () => {
        setIsShowedModalNotSelectedShipment(false)
        setIsShowedSelectorAddress(true)
    }

    const getModalIsNotShipmentFooter = () => (
        <S.ModalIsNotShipmentFooter>
            <S.ModalIsNotShipmentFooterButtons>
                <Button
                    elementSizeTypeValue={Theme.ElementSizeType.regular}
                    clickHandler={modalIsNotShipmentHandler}>
                    Выбрать аптеку самовывоза
                </Button>
                <Button
                    elementSizeTypeValue={Theme.ElementSizeType.regular}
                    clickHandler={modalIsNotShipmentAddressHandler}>
                    Выбрать адрес доставки
                </Button>
                <Button
                    elementSizeTypeValue={Theme.ElementSizeType.regular}
                    clickHandler={modalIsNotShipmentAddBasket}>
                    Сделать выбор позже
                </Button>
            </S.ModalIsNotShipmentFooterButtons>
        </S.ModalIsNotShipmentFooter>
    )

    const toggleImageShowedModal = () => {
        if (props.good.webData.hasImage) {
            setIsImageShowedModal(!isImageShowedModal)
        }
    }

    const selectedTabHandler = async (id) => {
        setSelectedTab(id)
        if (id === CartTabTypes.anotherOutForms) {
            await dispatch(getAnotherOutFormsAction(props.good.webData.goodId))
        }
        if (id === CartTabTypes.analogue) {
            await dispatch(getAnaloguesAction(props.good.webData.goodId))
        }
        if (id === CartTabTypes.infoList) {
            const responseInfoList = await getInfoList(props.good.webData.goodId)
            setInfoList(responseInfoList)
        }
    }

    const changePriceHandler = async (count) => {
        if (count > 0) {
            await dispatch(changeBasketItemPrice(props.good.webData.webId, count))
        } else {
            deleteFromBasket.openHandler(props.good.webData.webId)
        }
    }

    const addBasket = async () => {
        await dispatch(
            addBasketItem({
                webData: props.good.webData,
                stockPrice: props.good.stockPrice,
                deliveryPrice: props.good.deliveryPrice,
                minPrice: props.good.minPrice,
                isAvailable: props.good.isAvailable,
                isAvailableOnStock: props.good.isAvailableOnStock,
                deliveryStatus: props.good.deliveryStatus,
                isStrictlyByPrescription: props.good.isStrictlyByPrescription,
                analoguesQty: props.good.analoguesQty,
                isOriginalGood: props.good.isOriginalGood,
                isCold: props.good.isCold,
                isDemand: props.good.isDemand,
                aptEtaDateTime: props.good.aptEtaDateTime
            })
        )
        setIsShowedModalDemand(false)
    }
    const closeDemandModal = () => {
        setIsShowedModalDemand(false)
    }
    const addBasketHandler = async () => {
        if (basketItems.length === 0 && !shipment.shipAddressId && !shipment.stockId) {
            setIsShowedModalNotSelectedShipment(true)
            await addBasket()
            return
        }
        if (props.good.isDemand) {
            setIsShowedModalDemand(true)
            return
        }
        await addBasket()
    }

    const modalIsNotShipmentAddBasket = async () => {
        setIsShowedModalNotSelectedShipment(false)
        // await addBasket()
    }

    // const getMNNTitle = () => {
    //     const infoItem = props.good.infoList?.find((x) => x.typeId === infoListTypes.MNN)
    //     //TODO: тут что то не так
    //     return infoItem?.text || ''
    // }
    // const mnn = getMNNTitle()

    const basketItem = basketItems.find((x) => x.webData.webId === props.good.webData.webId)
    let isInBasket = false
    if (basketItem) {
        isInBasket = true
    }

    const getLayout = (Component, props) => {
        if (!Component) {
            const html = ReactDOMServer.renderToString(<Component {...props} />)
            const Layout = ymaps.templateLayoutFactory.createClass(`<div id='balloon'>${html}</div>`, {
                build() {
                    Layout.superclass.build.call(this)
                    ReactDOM.hydrate(<Component {...props} />, document.getElementById('balloon'))
                }
            })

            return Layout
        }
        return null
    }

    const getBasketItem = (goodId) => {
        const basketItem = basketItems.find((x) => x.webData.goodId === goodId)
        return basketItem ? basketItem.count : null
    }

    useEffect(() => {
        const newtabs = []
        if (props.good.anotherOutFormsQty > 0) {
            newtabs.push({
                id: CartTabTypes.anotherOutForms,
                title: 'Формы выпуска',
                count: props.good.anotherOutFormsQty,
                clickHandler: () => selectedTabHandler(CartTabTypes.anotherOutForms)
            })
        }
        newtabs.push({
            id: CartTabTypes.infoList,
            title: 'Инструкция',
            count: null,
            clickHandler: () => selectedTabHandler(CartTabTypes.infoList)
        })
        if (props.good.analoguesQty > 0) {
            newtabs.push({
                id: CartTabTypes.analogue,
                title: 'Аналоги',
                count: props.good.analoguesQty,
                clickHandler: () => selectedTabHandler(CartTabTypes.analogue)
            })
        }
        setTabs(newtabs)
        // selectedTabHandler(CartTabTypes.infoList)
    }, [])

    useEffect(() => {
        selectedTabHandler(selectedTab)
    }, [shipment.cityId, shipment.stockId, shipment.shipAddressId])

    const getExtended = () => {
        switch (selectedTab) {
            case CartTabTypes.anotherOutForms: {
                return <CatalogGoodsWithFilters />
            }
            case CartTabTypes.analogue: {
                return <CatalogGoodsWithFilters />
            }
            case CartTabTypes.infoList: {
                return (
                    <S.InfoListBlock>
                        {infoList.map((x) => (
                            <S.InfoItem key={x.typeId}>
                                <S.InfoItemTitle>{x.title}</S.InfoItemTitle>
                                <S.InfoItemBody
                                    dangerouslySetInnerHTML={{
                                        __html: x.text
                                    }}
                                />
                            </S.InfoItem>
                        ))}
                    </S.InfoListBlock>
                )
            }
        }
    }
    const getSelectedStock = () => stocksAll.find((stock) => stock.id === shipment.stockId)
    const mnn = props.good.webData.mnnRusTitle
    const isMNN = mnn && mnn.toLowerCase().trim() !== 'не установлено' && mnn.toLowerCase().trim() !== '-'

    // const likedColor = likedGoods.includes(props.good.webData.goodId)
    //     ? Theme.ColorConsts.pureRed
    //     : Theme.ColorConsts.getGrayColor(0.1)

    const { ref, inView, entry } = useInView({
        /* Optional options */
        threshold: 0
    })

    const isNotVisibale = !inView && !isIE

    return (
        <S.Container>
            <S.IntersectionWrapper ref={ref} />
            <Head>
                <title>
                    {`${props.good.webData.drugTitle} ${props.good.webData.outFormTitle} - купить по выгодной цене, инструкция и отзывы в интернет-аптеке ЗДОРОВ.ру`}
                </title>
            </Head>
            <S.StickyBlock isSticky={isNotVisibale}>
                <S.StickyBlockGoodNav>
                    <S.FullTitle>
                        <S.FullTitleSpan>{`${props.good.webData.drugTitle} ${props.good.webData.outFormTitle}`}</S.FullTitleSpan>
                    </S.FullTitle>

                    <S.LineTabs>
                        {tabs.map((tab) => (
                            <S.Tab
                                key={tab.id}
                                isSelected={tab.id === selectedTab}
                                onClick={tab.clickHandler}>
                                {tab.title}
                                {getIndicateTabs(tab.id)}
                            </S.Tab>
                        ))}
                    </S.LineTabs>
                </S.StickyBlockGoodNav>
                <S.StickyBlockPriceBlock>
                    <GoodPriceBlock
                        addBasketHandler={addBasketHandler}
                        changePriceHandler={changePriceHandler}
                        stockPrice={props.good.stockPrice}
                        deliveryPrice={props.good.deliveryPrice}
                        minPrice={props.good.minPrice}
                        deliveryStatus={props.good.deliveryStatus}
                        isAvailable={props.good.isAvailable}
                        isAvailableOnStock={props.good.isAvailableOnStock}
                        count={getBasketItem(props.good.webData.goodId)}
                        openDeliveryReasons={openDeliveryReasons}
                        closeDeliveryReasons={closeDeliveryReasons}
                        isSelectedStock={!!(shipment.stockId && shipment.stockId > 0)}
                        availableStocksCount={props.good.availableStocksCount}
                        clickOpenAnotherStockHandler={clickOpenAnotherStockHandler}
                        showedAnotherStocks={showedAnotherStocks}
                        goodId={props.good.webData.goodId}
                        goodName={`${props.good.webData.drugTitle} ${props.good.webData.outFormTitle}`}
                    />
                </S.StickyBlockPriceBlock>
            </S.StickyBlock>
            <S.GoodDataContainer>
                <S.GeneralDataContainer>
                    <S.ImageContainer
                        onClick={toggleImageShowedModal}
                        isPointer={props.good.webData.hasImage}>
                        <S.GoodImage
                            onError={(e) => {
                                e.target.onerror = null
                                e.target.src = `${process.env.BASE_PATH || ''}/images/noimg.gif`
                            }}
                            src={generateUrlImage(
                                baseImageUrl,
                                props.good.webData.goodId,
                                props.good.webData.hasImage
                            )}
                        />
                    </S.ImageContainer>
                    <S.InfoContainer>
                        <S.InfoList>
                            <GoodInfoContainer good={props.good} />
                            {/* {drawInfoItem('Производитель', props.good.webData.makerTitle)}
                            {isMNN && drawInfoItem('Действующее вещество', mnn)} */}
                        </S.InfoList>
                        <S.FullInfo>
                            {/* <S.TextInfoContainer>
                                    <S.TextInfo>
                                        <S.TextInfoName>Производитель: </S.TextInfoName>
                                        <S.TextInfoDescription>
                                            {props.good.webData.makerTitle}
                                        </S.TextInfoDescription>
                                    </S.TextInfo>
                                    {isMNN && (
                                        <S.TextInfo>
                                            <S.TextInfoName>Действующее вещество: </S.TextInfoName>
                                            <S.TextInfoDescription>{mnn}</S.TextInfoDescription>
                                        </S.TextInfo>
                                    )}
                                </S.TextInfoContainer> 
                                 <S.MarkerBlock>
                                <MarkerContainer
                                    stockPrice={props.good.stockPrice}
                                    deliveryPrice={props.good.deliveryPrice}
                                    analoguesQty={props.good.analoguesQty}
                                    aptEtaDateTime={props.good.aptEtaDateTime}
                                    isCold={props.good.isCold}
                                    isDemand={props.good.isDemand}
                                    isHidden={props.good.isHidden}
                                    isOriginalGood={props.good.isOriginalGood}
                                    isStrictlyByPrescription={props.good.isStrictlyByPrescription}
                                />
                            </S.MarkerBlock> */}
                        </S.FullInfo>
                    </S.InfoContainer>
                </S.GeneralDataContainer>
            </S.GoodDataContainer>

            <S.GoodExtendedDataContainer>{getExtended()}</S.GoodExtendedDataContainer>

            {deliveryReasonState.isShowedModal && (
                <ModalWindow
                    title="Доставка невозможна"
                    isButtonFooter={false}
                    isFooterOnlyOneButton
                    submit={{
                        text: 'Ок',
                        clickHandler: closeDeliveryReasons
                    }}
                    cancel={{
                        text: 'Ок',
                        clickHandler: closeDeliveryReasons
                    }}>
                    <S.ReasonContainer>
                        {deliveryReasonState.reasons.map((reason) => (
                            <S.ReasonTextItem key={reason.id}>{reason.text}</S.ReasonTextItem>
                        ))}
                    </S.ReasonContainer>
                </ModalWindow>
            )}
            {isImageShowedModal && (
                <ImageModal
                    closeHandler={toggleImageShowedModal}
                    imageUrl={generateUrlImage(
                        baseImageUrl,
                        props.good.webData.goodId,
                        props.good.webData.hasImage,
                        ImageSizeType.large
                    )}
                />
            )}
            {isShowedModalNotSelectedShipment && (
                <ModalWindowCustom
                    footer={getModalIsNotShipmentFooter()}
                    title="Внимание"
                    cancel={{
                        text: 'Отмена',
                        clickHandler: () => setIsShowedModalNotSelectedShipment(false)
                    }}>
                    <S.ModalIsNotShipmentContainer>
                        <S.ModalIsNotShipmentP>
                            Для Вашего удобства рекомендуем выбрать аптеку самовывоза или адрес доставки. Это
                            позволит:
                        </S.ModalIsNotShipmentP>
                        <S.ModalIsNotShipmentUl>
                            <S.ModalIsNotShipmentLi>
                                Видеть наличие товаров и цены в конкретной аптеке самовывоза
                            </S.ModalIsNotShipmentLi>
                            <S.ModalIsNotShipmentLi>
                                Определить возможность доставки и ассортимент товаров, доступных по
                                конкретному адресу
                            </S.ModalIsNotShipmentLi>
                        </S.ModalIsNotShipmentUl>
                        <S.ModalIsNotShipmentP>
                            Выбор можно сделать позже (на главной странице или в корзине), но не все товары,
                            добавленные в корзину ранее, могут быть доступны для самовывоза или доставки из-за
                            их отсутствия.
                        </S.ModalIsNotShipmentP>
                    </S.ModalIsNotShipmentContainer>
                </ModalWindowCustom>
            )}
            {isShowedSelectorStock && (
                <SelectorStock
                    stocks={stocksAll}
                    cities={cities}
                    goodIds={basketItems.map((x) => x.webData.goodId)}
                    changeSelectedStockHandler={selectedStocAndCitykHandler}
                    closeHandler={() => setIsShowedSelectorStock(false)}
                    selectedStock={getSelectedStock()}
                    cityId={shipment.cityId}
                />
            )}
            {isShowedSelectorAddress && (
                <ModalAddressSelect
                    shipAddressId={shipment.shipAddressId}
                    closeHandler={() => setIsShowedSelectorAddress(false)}
                    changeAddressHandler={changeSelectedAddressHandler}
                />
            )}
            {isShowedModalDemand && (
                <ModalWindow
                    title="Внимание"
                    submit={{
                        text: 'Добавить в корзину',
                        clickHandler: addBasket
                    }}
                    cancel={{
                        text: 'Отмена',
                        clickHandler: closeDemandModal
                    }}>
                    <S.BodyModel
                        dangerouslySetInnerHTML={{
                            __html: getFeatureHtml()
                        }}
                    />
                </ModalWindow>
            )}
        </S.Container>
    )
}

Good.propTypes = {
    good: PropTypes.shape({
        webData: _WebDataPropTypes,
        infoList: PropTypes.arrayOf(
            PropTypes.shape({
                typeId: PropTypes.number.isRequired,
                title: PropTypes.string.isRequired,
                text: PropTypes.string.isRequired,
                priorityCount: PropTypes.number.isRequired
            })
        ),
        stockPrice: _PricePropTypes,
        deliveryPrice: _PricePropTypes,
        minPrice: _PricePropTypes,
        isAvailable: PropTypes.bool.isRequired,
        isAvailableOnStock: PropTypes.bool.isRequired,
        isStrictlyByPrescription: PropTypes.bool,
        isOriginalGood: PropTypes.bool,
        isCloud: PropTypes.bool,
        analoguesQty: PropTypes.number,
        anotherOutFormsQty: PropTypes.number
    }).isRequired
}

export default Good
