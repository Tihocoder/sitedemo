import React, { useState } from 'react'
import PropTypes from 'prop-types'
import * as S from './BasketShipment.styled'
import ModalWindow from '../../shared/ModalWindow/ModalWindow'
import SelectorStock from '../../SelectorStock/SelectorStock'
import ModalAddressSelect from './ModalAddressSelect/ModalAddressSelect'
const BasketShipment = (props) => {
    const [showedSelector, setShowedSelector] = useState({
        stock: false,
        delivery: false
    })
    const [selectedAddress, setSelectedAddress] = useState({
        id: 0,
        title: ''
    })
    const [addressList, setAddressList] = useState([])

    const changeSelectedStockHandler = (id, cityId) => {
        setShowedSelector({ ...showedSelector, stock: false })
        props.changeSelectedStockHandler(id, cityId)
    }
    const changeSelectedAddressHandler = (id, title) => {
        setShowedSelector({ ...showedSelector, delivery: false })
        props.changeSelectedAddressHandler(id, title)
    }

    const closeSelectorAddressHandler = () => {
        setShowedSelector({ ...showedSelector, delivery: false })
    }

    const openSelectorAddressHandler = () => {
        setShowedSelector({ ...showedSelector, delivery: true })
    }

    const closeSelectorStockHandler = () => {
        setShowedSelector({ ...showedSelector, stock: false })
    }
    const openSelectorStockHandler = () => {
        setShowedSelector({ ...showedSelector, stock: true })
    }

    const changeTextAddressHandler = (e) => {
        const term = e.target.value
        setSelectedAddress(term)
        updateAddresses(term, setAddressList)
    }
    const selectItemHandler = (e) => {
        const id = parseInt(e.target.id, 10)
        const selectAddress = addressList.find((x) => x.id === id)
        const text = id <= 0 ? `${selectAddress.text}, ` : selectAddress.text
        setSelectedAddress(text)
        setFormData({
            ...formData,
            deliveryData: {
                ...formData.deliveryData,
                shipAddressId: id
            }
        })
        updateAddresses(text, setAddressList)
    }
    const stockTitle = props.selectedStock.id
        ? `${props.selectedStock.title} ${props.selectedStock.address}`
        : 'Выбрать'
    const deliveryTitle = props.selectedShipAddress.id ? props.selectedShipAddress.title : 'Выбрать'
    return (
        <S.Container>
            {showedSelector.stock && (
                <SelectorStock
                    selectedStock={props.selectedStock}
                    changeSelectedStockHandler={changeSelectedStockHandler}
                    closeHandler={closeSelectorStockHandler}
                    goodIds={props.basketItems.map((x) => x.webData.goodId)}
                    stocks={props.stocks}
                    cities={props.cities}
                    cityId={props.cityId}
                />
            )}
            {showedSelector.delivery && (
                <ModalAddressSelect
                    selectedAddress={selectedAddress}
                    closeHandler={closeSelectorAddressHandler}
                    changeAddressHandler={changeSelectedAddressHandler}
                />
            )}

            <S.StockBlock onClick={openSelectorStockHandler}>
                Аптека самовывоза:
                <S.LinkText>{stockTitle}</S.LinkText>
            </S.StockBlock>
            <S.StockBlock onClick={openSelectorAddressHandler}>
                Адрес доставки: <S.LinkText>{deliveryTitle}</S.LinkText>
            </S.StockBlock>
        </S.Container>
    )
}

BasketShipment.propTypes = {
    selectedShipAddress: PropTypes.shape({
        id: PropTypes.string.isRequired,
        title: PropTypes.string.isRequired
    }),
    selectedStock: PropTypes.shape({
        id: PropTypes.string.isRequired,
        title: PropTypes.string.isRequired
    }),
    stocks: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.number.isRequired,
            title: PropTypes.string.isRequired,
            name: PropTypes.string.isRequired,
            isMetroLocation: PropTypes.bool.isRequired,
            metroColor: PropTypes.string.isRequired,
            address: PropTypes.string.isRequired
        })
    ),
    basketItems: PropTypes.array.isRequired,
    changeSelectedStockHandler: PropTypes.func.isRequired,
    changeSelectedAddressHandler: PropTypes.func.isRequired
}

export default BasketShipment
