import styled from 'styled-components'

export const Container = styled.div`
    display: flex;
    flex-direction: column;
`

export const StockBlock = styled.div`
    display: flex;
`
export const ShipAddressBlock = styled.div`
    display: flex;
`

export const LinkText = styled.a`
    margin-left: 1rem;
    color: ${(props) => props.theme.colors.getGrayColor(0.9)};
    text-decoration: underline;
    cursor: pointer;
`
