import styled from 'styled-components'

export const Container = styled.div`
    display: flex;
    width: 100%;
    flex-direction: column;
`
export const SelectedAddressTitle = styled.h2`
    font-size: normal;
    font-size: 1rem;
    font-weight: normal;
    color: ${(props) => props.theme.colors.getGrayColor(0.8)};
`
