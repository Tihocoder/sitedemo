import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import * as S from './ModalAddressSelect.styled'
import ModalWindow from '../../../shared/ModalWindow/ModalWindow'
import { getAddressById, getAddresses } from '../../../../api/siteApis'
import InputSearch from '../../../shared/InputSearch/InputSearch'
import * as _ from 'lodash'

const updateAddresses = _.debounce(async (termText, setAddressList) => {
    if (termText) {
        const addresses = await getAddresses(termText)
        setAddressList([...addresses])
    }
}, 250)

const ModalAddressSelect = (props) => {
    const [selectedGeneralAddress, setSelectedGeneralAddress] = useState({
        id: 0,
        title: ''
    })
    const [selectedAddress, setSelectedAddress] = useState({
        id: 0,
        title: ''
    })
    const [addressList, setAddressList] = useState([])
    const [addressTitle, setAddressTitile] = useState('')
    const changeTextAddressHandler = (e) => {
        const term = e.target.value
        setSelectedAddress({
            id: 0,
            title: term
        })
        updateAddresses(term, setAddressList)
    }

    const selectItemHandler = (e) => {
        const id = parseInt(e.target.id, 10)
        const selectAddress = addressList.find((x) => x.id === id)
        const text = id === -1 ? `${e.currentTarget.textContent}, ` : selectAddress.text
        setSelectedAddress({
            id: id,
            title: text
        })
        updateAddresses(text, setAddressList)
        if (id !== -1) {
            setSelectedGeneralAddress({
                id: id,
                title: text
            })
        }
    }

    const changeAddressHandler = () => {
        if (selectedAddress.id !== -1) props.changeAddressHandler(selectedAddress.id, selectedAddress.title)
    }

    const initShipAddress = async (id) => {
        const response = await getAddressById(id)
        if (response && response.address)
            setSelectedGeneralAddress({
                id: response.id,
                title: response.address
            })
    }

    useEffect(() => {
        if (props.shipAddressId) initShipAddress(props.shipAddressId)
    }, [props.shipAddressId])

    return (
        <ModalWindow
            title={'Выбор адреса'}
            cancel={{
                clickHandler: props.closeHandler,
                text: 'Отмена'
            }}
            submit={{
                clickHandler: changeAddressHandler,
                text: 'Выбрать'
            }}>
            <S.Container>
                {selectedGeneralAddress.title && (
                    <S.SelectedAddressTitle>{selectedGeneralAddress.title}</S.SelectedAddressTitle>
                )}
                <InputSearch
                    value={selectedAddress.title}
                    changeValue={changeTextAddressHandler}
                    selectHandler={selectItemHandler}
                    items={addressList}
                    placeholder={'Адрес доставки'}
                />
            </S.Container>
        </ModalWindow>
    )
}

ModalAddressSelect.propTypes = {
    shipAddressId: PropTypes.number.isRequired,
    closeHandler: PropTypes.func.isRequired,
    changeAddressHandler: PropTypes.func.isRequired
}

export default ModalAddressSelect
