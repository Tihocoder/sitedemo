import React, { useEffect, useRef, useState } from 'react'
import PropTypes from 'prop-types'
import * as _ from 'lodash'
import { useDispatch, useSelector } from 'react-redux'
import { useRouter } from 'next/router'
import {
    changeShipment,
    toggleBasketIsdelivery,
    toggleHiddenBasketItem,
    toggleLikedGood,
    updateActualBasketPostions,
    updateBasketFromPreorder
} from '@actions/customer'
import { getAddressById } from '@api/siteApis'
import { LoaderBlock, LoaderTitle, PageTitile } from '@styles/base.styled'
import * as preorderConnector from '@services/preorderConnector'
import { analyzingBasketAndUpdateGoodsCount } from '@helpers/order'
import { preorderChange, preorderStop } from '@actions/createOrder'
import { _PricePropTypes, _WebDataPropTypes } from 'src/consts'
import * as S from './Basket.styled'
import * as Theme from '../../styles/theme-const'
import Button from '../shared/Button/Button'
import CatalogGoodItem from '../CatalogGoodItem/CatalogGoodItem'
import SymbolRuble from '../shared/SymbolRuble/SymbolRuble'
import ModalAddressSelect from './BasketShipment/ModalAddressSelect/ModalAddressSelect'
import SelectorStock from '../SelectorStock/SelectorStock'
import ModalWindow from '../shared/ModalWindow/ModalWindow'
import Loader from '../shared/Loader/Loader'
import { DesktopBodyModalTitle } from './Basket.styled'

const defaultModalState = {
    isShowed: false,
    title: '',
    content: <React.Fragment />,
    buttonTitle: 'Продолжить',
    buttonHandler: () => {},
    cancelTitle: 'Отмена',
    footer: null
}

const Basket = () => {
    const customerShipment = useSelector((state) => state.customer.shipment)
    const basket = useSelector((state) => state.customer.basket)
    const stocks = useSelector((state) => state.company.stocks)
    const cities = useSelector((state) => state.company.cities)
    const dispatch = useDispatch()
    const router = useRouter()
    const basketItemsAll = useSelector((state) => state.customer.basketItems)
    const basketItems = basketItemsAll.filter((item) => !item.isHidden)
    const likedGoods = useSelector((state) => state.customer.likedGoods)
    const isDelivery = useSelector((state) => state.customer.basket.isDelivery)
    const preorder = useSelector((state) => state.createOrder.preorder)
    const [modalInfo, setModalInfo] = useState({
        isShowed: false,
        title: '',
        content: <React.Fragment />,
        buttonTitle: 'Продолжить',
        buttonHandler: () => {},
        cancelTitle: 'Отмена',
        footer: null
    })
    const [isShowedWarningBorder, setIsShowedWarningBorder] = useState(false)
    const signalRTimer = useRef(null)
    const closeModalInfo = () =>
        setModalInfo({ ...modalInfo, ...defaultModalState, content: <React.Fragment />, isShowed: false })
    const showModalInfo = (
        content,
        buttonTitle = 'Продолжить',
        buttonHandler,
        cancelTitle,
        title = 'Внимание',
        footer = null
    ) => {
        setModalInfo({
            ...modalInfo,
            isShowed: true,
            content,
            buttonTitle,
            buttonHandler,
            cancelTitle,
            title,
            footer
        })
    }
    const redirectOnCreateOrder = async (uid) => {
        if (!isDelivery && stock && stock.id && !isNotAvailableStock) {
            await router.push({
                pathname: '/order/create',
                query: { isDelivery, preorderUid: uid }
            })
        }
        if (isDelivery && !isNotAvailableDelivery && selectedAddress.id) {
            /// /RuTarget
            const _rutarget = window._rutarget || []
            _rutarget.push({ event: 'thankYou', conv_id: 'confirm' })

            await router.push({
                pathname: '/order/create',
                query: { isDelivery, preorderUid: uid }
            })
        }
    }

    const [isPreorderLoading, setIsPreorderLoading] = useState(false)
    const [showedSelector, setShowedSelector] = useState({
        stock: false,
        delivery: false
    })

    const [selectedAddress, setSelectedAddress] = useState({
        id: 0,
        title: ''
    })
    const [isShowedWarningSelectStock, setIsShowedWarningSelectStock] = useState(false)
    const [isShowedWarningSelectDelivery, setIsShowedWarningSelectDelivery] = useState(false)
    const [isShowedModalNotPostionStock, setIsShowedModalNotPostionStock] = useState(false)
    const [errorText, setErrorText] = useState('')

    const changePreorder = (data) => {
        dispatch(preorderChange(data))
    }

    const updateBasketPosition = async () => {
        await dispatch(updateActualBasketPostions(isDelivery))
    }
    const stopConnect = () => {
        const con = preorder.connection

        if (con) {
            try {
                con.stop().then(() => {})
                dispatch(preorderChange({ connection: null, message: null }))
            } catch (error) {
                console.error(error)
            }
        }
    }
    const updatePreorder = async (positions) => {
        await dispatch(updateBasketFromPreorder(positions))
    }

    useEffect(() => {
        if (preorder.connection) {
            return () => {
                stopConnect()
                setIsPreorderLoading(false)
                clearTimeout(signalRTimer.current)
            }
        }
    }, [preorder.connection])

    useEffect(() => {
        if (!preorder.message) return

        const positionData = analyzingBasketAndUpdateGoodsCount(
            preorder.message.preorderItems,
            basketItemsAll,
            preorder.message.isDelivery
        )
        if (positionData.isChanged) {
            updatePreorder(positionData.newBasketPositions)
            showModalInfo(
                <S.DesktopBodyModalContainer>
                    <S.DesktopBodyModalP>Некоторых товаров нет в необходимом количестве.</S.DesktopBodyModalP>
                    <S.DesktopBodyModalP>
                        Для оформления заказа Вам необходимо уменьшить требуемое количество в корзине или
                        заменить товары на аналогичные.
                    </S.DesktopBodyModalP>
                </S.DesktopBodyModalContainer>,
                'Вернуться к корзине',
                closeModalInfo,
                'Отмена',
                <S.DesktopBodyModalTitle>Извините, товар закончился</S.DesktopBodyModalTitle>
            )
            stopConnect()
            setIsPreorderLoading(false)
        }
        if (!positionData.isChanged) {
            stopConnect()
            redirectOnCreateOrder(preorder.message.preorderUid)
        }
    }, [preorder.message])

    const hiddenItemHandler = async (goodId) => {
        setIsShowedWarningBorder(false)
        await dispatch(toggleHiddenBasketItem(goodId))
    }

    const toggleisShowedWarningSelectStock = () => setIsShowedWarningSelectStock(!isShowedWarningSelectStock)
    const toggleisShowedWarningSelectDelivery = () =>
        setIsShowedWarningSelectDelivery(!isShowedWarningSelectDelivery)
    const toggleSelectorAddressHandler = () => {
        setIsShowedWarningBorder(false)
        setIsShowedWarningSelectDelivery(false)
        setShowedSelector({ ...showedSelector, delivery: !showedSelector.delivery })
    }
    const toggleSelectorStockHandler = () => {
        setIsShowedWarningBorder(false)
        setIsShowedWarningSelectStock(false)
        setIsShowedModalNotPostionStock(false)
        setShowedSelector({ ...showedSelector, stock: !showedSelector.stock })
    }
    const toggleShowedModalNotPositionStock = () => {
        setIsShowedModalNotPostionStock(!isShowedModalNotPostionStock)
    }
    useEffect(() => {
        if (customerShipment.shipAddressId && customerShipment.shipAddressId !== selectedAddress.id) {
            initShipAddress(customerShipment.shipAddressId)
        }
    }, [customerShipment.shipAddressId, selectedAddress.id])

    useEffect(() => {
        if (!basketItems || basketItems.length === 0) return
        updateBasketPosition()

        return () => {
            dispatch(preorderStop())
            setIsPreorderLoading(false)
            clearTimeout(signalRTimer.current)
        }
    }, [])

    const initShipAddress = async (id) => {
        const response = await getAddressById(id)
        setSelectedAddress({ id: response.id, title: response.address })
    }

    const changeSelectedAddressHandler = async (id, title) => {
        setSelectedAddress({ id, title })
        await dispatch(changeShipment({ shipAddressId: id }))
        toggleSelectorAddressHandler()
        await router.reload()
    }
    const changeSelectedStockHandler = async (id, cityId) => {
        await dispatch(changeShipment({ stockId: id, cityId }))
        toggleSelectorStockHandler()
        await router.reload()
    }

    const getSelectedStock = () => stocks.find((stock) => stock.id === changeShipment.stockId)

    const sumStock = _.sumBy(basketItems, (item) => item.stockPriceSum)
    const sumMin = _.sumBy(basketItems, (item) => item.minPriceSum)
    const sumDelivery = _.sumBy(basketItems, (item) => item.deliveryPriceSum)
    let sumDeliveryFull = sumDelivery
    let deliveryCost = 0
    if (sumDelivery <= basket.deliveryTreshhold.minSum) {
        sumDeliveryFull = sumDelivery + basket.deliveryTreshhold.cost
        deliveryCost = basket.deliveryTreshhold.cost
    }
    const selectedDeliveryShipment = async () => {
        stopConnect()
        await dispatch(updateActualBasketPostions(isDelivery))
        if (!isDelivery) await dispatch(toggleBasketIsdelivery())
    }
    const selectedStockShipment = async () => {
        stopConnect()
        if (isDelivery) await dispatch(toggleBasketIsdelivery())
        await dispatch(updateActualBasketPostions(isDelivery))
    }

    const stock = stocks.find((x) => x.id === customerShipment.stockId)
    const isNotAvailableStock = basketItems.some(
        (item) => !item.isAvailableOnStock || !item.stockPrice.normal
    )
    const isNotAvailableDelivery = basketItems.some(
        (item) => !item.deliveryStatus.isAvailableForDelivery || !item.deliveryPrice.normal
    )

    const createOrderHandler = async () => {
        // setErrorText('Произошла ошибка попробуйте оформить еще раз')
        setIsShowedWarningBorder(true)
        if (!isDelivery && !stock) {
            toggleisShowedWarningSelectStock()
            return
        }
        if (isDelivery && !selectedAddress.id) {
            toggleisShowedWarningSelectDelivery()
            return
        }

        if (!isDelivery && isNotAvailableStock) {
            toggleShowedModalNotPositionStock()
            return
        }
        if (isDelivery && isNotAvailableDelivery) {
            showModalInfo(
                <S.BodyModalContainer>
                    В корзине есть товары, недоступные для доставки. Для оформления заказа необходимо удалить
                    их из корзины
                </S.BodyModalContainer>,
                'Вернуться к корзине',
                closeModalInfo,
                'Отмена'
            )
            return
        }

        if (basketItems.some((item) => item.isChanged)) {
            showModalInfo(
                <S.DesktopBodyModalContainer>
                    <S.DesktopBodyModalP>Некоторых товаров нет в необходимом количестве.</S.DesktopBodyModalP>
                    <S.DesktopBodyModalP>
                        Для оформления заказа Вам необходимо уменьшить требуемое количество в корзине или
                        заменить товары на аналогичные.
                    </S.DesktopBodyModalP>
                </S.DesktopBodyModalContainer>,
                'Вернуться к корзине',
                closeModalInfo,
                'Отмена',
                <S.DesktopBodyModalTitle>Извините, товар закончился</S.DesktopBodyModalTitle>,
                <S.ModalFooterContainer>
                    <Button
                        elementSizeTypeValue={Theme.ElementSizeType.regular}
                        clickHandler={closeModalInfo}>
                        Понятно
                    </Button>
                </S.ModalFooterContainer>
            )
            return
        }
        setIsPreorderLoading(true)
        try {
            const preorderData = await preorderConnector.createConnectionPreorder(
                basketItems.filter((item) => !item.isHidden),
                customerShipment,
                isDelivery
            )
            if (!preorderData) {
                setErrorText('Произошла ошибка попробуйте оформить заказ еще раз')
                setIsPreorderLoading(false)
                return
            }
            if (preorderData.response.isNeedAwaitBooking) {
                changePreorder({
                    uid: preorderData.response.preorderUid,
                    connection: preorderData.connection
                })

                preorderData.connection
                    .start()
                    .then(() => {
                        console.log('Connected!')
                        preorderData.connection.on('SendPreorder', (message) => {
                            console.log('signalR', message)
                            changePreorder({ message })
                        })
                    })
                    .catch((e) => console.log('Connection failed: ', e))

                signalRTimer.current = setTimeout(async () => {
                    stopConnect()
                    await redirectOnCreateOrder(preorderData.response.preorderUid)
                }, 15000)
                return
            }
            await stopConnect()
            await redirectOnCreateOrder(preorderData.response.preorderUid)
        } catch (error) {
            setErrorText('Произошла ошибка попробуйте оформить еще раз')
            changePreorder(preorderStop())
            setIsPreorderLoading(false)
        }
    }
    const returnHomeHandler = () => router.push('/')

    useEffect(() => {
        updateBasketPosition()
    }, [isDelivery])
    const toggleLikeHandler = async (webId) => {
        await dispatch(toggleLikedGood(webId))
    }
    const isSelectedShipment = !!((isDelivery && selectedAddress.id) || (!isDelivery && stock))
    return (
        <S.Container>
            <S.ErrorMessage>{errorText}</S.ErrorMessage>
            {isPreorderLoading && (
                <S.LoaderWrapper>
                    <LoaderTitle>Проверка наличия товаров в аптеке. Не закрывайте вкладку</LoaderTitle>
                    <LoaderBlock>
                        <Loader />
                    </LoaderBlock>
                </S.LoaderWrapper>
            )}

            <S.Header>
                <PageTitile>Корзина</PageTitile>
            </S.Header>
            {basketItemsAll.length > 0 ? (
                <S.Body>
                    <S.OrderBasketContainer>
                        <S.PositionContainer>
                            {basketItemsAll.length > 0 &&
                                basketItemsAll.map((good) => (
                                    <CatalogGoodItem
                                        key={good.webData.goodId}
                                        isShowedWarninBorder={isSelectedShipment && isShowedWarningBorder}
                                        isSelectedShipment={isSelectedShipment}
                                        webData={good.webData}
                                        availableStocksCount={good.availableStocksCount}
                                        basketItemCount={good.count}
                                        bookedCount={good.bookedCount}
                                        isChanged={good.isChanged}
                                        stockPrice={good.stockPrice}
                                        deliveryPrice={good.deliveryPrice}
                                        minPrice={good.minPrice}
                                        deliveryStatus={good.deliveryStatus}
                                        isAvailable={good.isAvailable}
                                        isAvailableOnStock={good.isAvailableOnStock}
                                        isStrictlyByPrescription={good.isStrictlyByPrescription}
                                        isOriginalGood={good.isOriginalGood}
                                        isCold={good.isCold}
                                        shipment={customerShipment}
                                        liked={likedGoods.includes(good.webData.goodId)}
                                        toggleLikeHandler={toggleLikeHandler}
                                        isHidden={good.isHidden}
                                        hiddenPositionHandler={hiddenItemHandler}
                                        options={{
                                            isBasket: true,
                                            isDelivery
                                        }}
                                    />
                                ))}
                        </S.PositionContainer>
                        <S.BasketBlockForOrderPrice>
                            <S.OrderPriceBlock>
                                <S.TitleInBlock>Ваш заказ</S.TitleInBlock>
                                <S.ToggleShipmentContainer>
                                    <S.TabShipment onClick={selectedStockShipment} isActive={!isDelivery}>
                                        <S.TabShipmentTitle>
                                            <S.TabShipmentHeaderTitle>Самовывоз </S.TabShipmentHeaderTitle>
                                            <S.TabShipmentPrefixLight>
                                                (стоимость товаров)
                                            </S.TabShipmentPrefixLight>
                                        </S.TabShipmentTitle>
                                        <S.TabShipmentPrice>
                                            {stock && stock.id && sumStock ? (
                                                <S.TabShipmentPriceCost>
                                                    {sumStock.toLocaleString('RU-ru')} <SymbolRuble />
                                                </S.TabShipmentPriceCost>
                                            ) : (
                                                <React.Fragment>
                                                    <S.TabShipmentPrefixLight>от</S.TabShipmentPrefixLight>{' '}
                                                    <S.TabShipmentPriceCost>
                                                        {sumMin.toLocaleString('RU-ru')} <SymbolRuble />
                                                    </S.TabShipmentPriceCost>
                                                </React.Fragment>
                                            )}
                                        </S.TabShipmentPrice>
                                    </S.TabShipment>
                                    <S.TabShipment onClick={selectedDeliveryShipment} isActive={isDelivery}>
                                        <S.TabShipmentTitle>
                                            <S.TabShipmentHeaderTitle>Доставка </S.TabShipmentHeaderTitle>
                                            <S.TabShipmentPrefixLight>
                                                (стоимость товаров)
                                            </S.TabShipmentPrefixLight>
                                        </S.TabShipmentTitle>
                                        <S.TabShipmentPrice>
                                            <S.TabShipmentPriceCost>
                                                {sumDelivery.toLocaleString('RU-ru')} <SymbolRuble />
                                            </S.TabShipmentPriceCost>
                                        </S.TabShipmentPrice>
                                    </S.TabShipment>
                                </S.ToggleShipmentContainer>
                                {isDelivery ? (
                                    <React.Fragment>
                                        <S.PriceBlockList>
                                            <S.PriceItem>
                                                <S.PriceItemTitle>Стоимость товаров:</S.PriceItemTitle>
                                                <S.PriceItemSum>
                                                    {sumDelivery.toLocaleString('RU-ru')} <SymbolRuble />
                                                </S.PriceItemSum>
                                            </S.PriceItem>
                                            <S.PriceItem>
                                                <S.PriceItemTitle>Стоимость доставки:</S.PriceItemTitle>
                                                <S.PriceItemSum>
                                                    {deliveryCost.toLocaleString('RU-ru')} <SymbolRuble />
                                                </S.PriceItemSum>
                                            </S.PriceItem>
                                            <S.PriceItem>
                                                <S.PriceItemTitle>Общая стоимость:</S.PriceItemTitle>
                                                <S.PriceItemGeneralSum>
                                                    {sumDeliveryFull.toLocaleString('RU-ru')} <SymbolRuble />
                                                </S.PriceItemGeneralSum>
                                            </S.PriceItem>
                                        </S.PriceBlockList>
                                        <S.ShipmentDescriptionContainer>
                                            <S.ShipmentDescriptionTitle>
                                                Адрес доставки:
                                            </S.ShipmentDescriptionTitle>
                                            <S.ShipmentDescription>
                                                {selectedAddress.title || 'Не выбран адрес доставки'}
                                            </S.ShipmentDescription>
                                        </S.ShipmentDescriptionContainer>
                                        <S.OrderPriceBlockButtonContainer
                                            isShowedWarninBorder={
                                                !!(!selectedAddress.title && isShowedWarningBorder)
                                            }>
                                            <S.OrderPriceButton
                                                isLight={selectedAddress.title}
                                                onClick={toggleSelectorAddressHandler}>
                                                {!selectedAddress.title
                                                    ? 'Выбрать адрес доставки'
                                                    : 'Изменить адрес доставки'}
                                            </S.OrderPriceButton>
                                            <S.OrderPriceButton
                                                isLight={isNotAvailableDelivery || !selectedAddress.title}
                                                onClick={createOrderHandler}>
                                                Оформить доставку
                                            </S.OrderPriceButton>
                                        </S.OrderPriceBlockButtonContainer>
                                    </React.Fragment>
                                ) : (
                                    <React.Fragment>
                                        <S.PriceBlockList>
                                            <S.PriceItem>
                                                <S.PriceItemTitle>Стоимость товаров:</S.PriceItemTitle>
                                                {sumStock ? (
                                                    <S.PriceItemGeneralSum>
                                                        {sumStock.toLocaleString('RU-ru')} <SymbolRuble />
                                                    </S.PriceItemGeneralSum>
                                                ) : (
                                                    <S.PriceItemGeneralSum>
                                                        {sumMin.toLocaleString('RU-ru')} <SymbolRuble />
                                                    </S.PriceItemGeneralSum>
                                                )}
                                            </S.PriceItem>
                                        </S.PriceBlockList>
                                        <S.ShipmentDescriptionContainer>
                                            <S.ShipmentDescriptionTitle>
                                                Аптека самовывоза:
                                            </S.ShipmentDescriptionTitle>
                                            <S.ShipmentDescription>
                                                {stock
                                                    ? `${stock.title} ${stock.address}`
                                                    : 'Не выбрана аптека самовывоза'}
                                            </S.ShipmentDescription>
                                        </S.ShipmentDescriptionContainer>
                                        <S.OrderPriceBlockButtonContainer
                                            isShowedWarninBorder={!!(!stock && isShowedWarningBorder)}>
                                            <S.OrderPriceButton
                                                isLight={stock}
                                                onClick={toggleSelectorStockHandler}>
                                                {!stock
                                                    ? 'Выбрать аптеку самовывоза'
                                                    : 'Изменить аптеку самовывоза'}
                                            </S.OrderPriceButton>
                                            <S.OrderPriceButton
                                                isLight={isNotAvailableStock}
                                                onClick={createOrderHandler}>
                                                Оформить самовывоз
                                            </S.OrderPriceButton>
                                        </S.OrderPriceBlockButtonContainer>
                                    </React.Fragment>
                                )}
                            </S.OrderPriceBlock>
                        </S.BasketBlockForOrderPrice>
                    </S.OrderBasketContainer>
                </S.Body>
            ) : (
                <S.Body>
                    <S.BasketEmptyContainer>
                        <S.BasketEmptyDescription>
                            <S.WarningIconContainer>
                                <S.WarningImage />
                            </S.WarningIconContainer>
                            Ваша корзина пуста
                        </S.BasketEmptyDescription>
                        <Button
                            elementSizeTypeValue={Theme.ElementSizeType.large}
                            clickHandler={returnHomeHandler}>
                            Вернуться на главную
                        </Button>
                    </S.BasketEmptyContainer>
                </S.Body>
            )}
            {showedSelector.delivery && (
                <ModalAddressSelect
                    shipAddressId={customerShipment.shipAddressId}
                    closeHandler={toggleSelectorAddressHandler}
                    changeAddressHandler={changeSelectedAddressHandler}
                />
            )}
            {showedSelector.stock && (
                <SelectorStock
                    closeHandler={toggleSelectorStockHandler}
                    stocks={stocks}
                    cities={cities}
                    goodIds={basketItems.map((x) => x.webData.goodId)}
                    changeSelectedStockHandler={changeSelectedStockHandler}
                    selectedStock={getSelectedStock()}
                    cityId={customerShipment.cityId}
                />
            )}
            {isShowedWarningSelectStock && (
                <ModalWindow
                    title="Не выбрана аптека самовывоза"
                    submit={{
                        text: 'Выбрать аптеку',
                        clickHandler: toggleSelectorStockHandler
                    }}
                    cancel={{
                        text: 'Отмена',
                        clickHandler: toggleisShowedWarningSelectStock
                    }}>
                    <S.BodyModalContainer>
                        Для оформления заказа необходимо выбрать аптеку самовывоза
                    </S.BodyModalContainer>
                </ModalWindow>
            )}
            {isShowedModalNotPostionStock && (
                <ModalWindow
                    title=""
                    isButtonFooter={false}
                    footer={
                        <React.Fragment>
                            <Button
                                elementSizeTypeValue={Theme.ElementSizeType.regular}
                                clickHandler={toggleSelectorStockHandler}>
                                Сменить аптеку
                            </Button>
                            <Button
                                elementSizeTypeValue={Theme.ElementSizeType.regular}
                                clickHandler={toggleShowedModalNotPositionStock}>
                                Вернуться к корзине
                            </Button>
                        </React.Fragment>
                    }
                    cancel={{
                        text: 'Выбрать аптеку',
                        clickHandler: toggleShowedModalNotPositionStock
                    }}>
                    <S.BodyModalContainer>
                        В корзине есть товары, отсутствующие в аптеке {stock ? `"${stock.title}"` : ''}.
                        Выберите другую аптеку самовывоза или удалите эти товары
                    </S.BodyModalContainer>
                </ModalWindow>
            )}
            {isShowedWarningSelectDelivery && (
                <ModalWindow
                    title="Не выбран адрес доставки"
                    submit={{
                        text: 'Выбрать адрес',
                        clickHandler: toggleSelectorAddressHandler
                    }}
                    cancel={{
                        text: 'Отмена',
                        clickHandler: toggleisShowedWarningSelectDelivery
                    }}>
                    <S.BodyModalContainer>
                        Для оформления необходимо выбрать адрес доставки
                    </S.BodyModalContainer>
                </ModalWindow>
            )}
            {modalInfo.isShowed && (
                <ModalWindow
                    title={modalInfo.title}
                    submit={{
                        text: modalInfo.buttonTitle,
                        clickHandler: modalInfo.buttonHandler
                    }}
                    footer={modalInfo.footer}
                    cancel={{
                        text: modalInfo.cancelTitle,
                        clickHandler: closeModalInfo
                    }}>
                    {modalInfo.content}
                </ModalWindow>
            )}
        </S.Container>
    )
}

Basket.propTypes = {
    basketItems: PropTypes.arrayOf(
        PropTypes.shape({
            webData: _WebDataPropTypes,
            stockPrice: _PricePropTypes,
            deliveryPrice: _PricePropTypes,
            priceSumData: PropTypes.shape({
                priceStock: PropTypes.number,
                priceDelivery: PropTypes.number
            }).isRequired,
            deliveryStatus: PropTypes.any,
            isInAnothersStock: PropTypes.bool.isRequired,
            isMinPrice: PropTypes.bool.isRequired,
            IsDemand: PropTypes.bool.isRequired
        }).isRequired
    )
}

export default Basket
