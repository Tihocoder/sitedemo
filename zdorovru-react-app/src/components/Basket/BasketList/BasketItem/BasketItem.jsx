import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import Link from 'next/link'
import { useDispatch, useSelector } from 'react-redux'
import * as S from './BasketItem.styled'
import Marker from '../../../shared/Marker/Marker'
import { _WebDataPropTypes, _PricePropTypes } from '../../../../consts'
// import noimg from './noimg.gif'
import BasketPriceBlock from './BasketPriceBlock/BasketPriceBlock'
import { changeBasketItemPrice } from '../../../../actions/customer'
import { AlertsStateContext } from '../../../../context/AlertsState'
import { generateUrlImage } from '../../../../helpers/site'
import { getBaseImageUrl } from '../../../../api/siteApis'

const BasketItem = (props) => {
    const dispatch = useDispatch()
    const baseImageUrl = useSelector((state) => state.catalog.baseImageUrl)
    const { deleteFromBasket } = useContext(AlertsStateContext)

    const changePriceHandler = async (count) => {
        if (count > 0) {
            await dispatch(
                changeBasketItemPrice(
                    props.good.webData.webId,
                    props.good.priceData,
                    props.good.isMinPrice,
                    count
                )
            )
        } else {
            deleteFromBasket.openHandler(props.good.webData.webId)
        }
    }

    const markerContainer = () => (
        <S.MarkerContainer>
            {props.good.isDemand && <Marker>Доступен после {props.good.price.aptEtaDateTime}</Marker>}
            {!props.good.deliveryStatus?.isAvailableForDelivery && <Marker>Доставка невозможна</Marker>}
            {props.good.webData.isStrictlyByPrescription && (
                <Marker
                    color={Theme.ColorConsts.accentRed}
                    borderColor={Theme.ColorConsts.accentRed}
                    backgroundColor="inhert">
                    Строго по рецепту
                </Marker>
            )}
        </S.MarkerContainer>
    )
    return (
        <S.Container>
            <S.GeneralBlock>
                <Link href={`/catalog/goods/${props.good.webData.webId}`}>
                    <S.ImageContainer>
                        <S.GoodImage
                            src={generateUrlImage(
                                baseImageUrl,
                                props.good.webData.goodId,
                                props.good.webData.hasImage
                            )}
                        />
                    </S.ImageContainer>
                </Link>
                <S.InfoContainer>
                    <S.FullInfo>
                        <Link href={`/catalog/goods/${props.good.webData.webId}`}>
                            <S.FullTitle>{`${props.good.webData.drugTitle} ${props.good.webData.outFormTitle}`}</S.FullTitle>
                        </Link>
                        <S.TextInfoContainer>
                            <S.TextInfo>
                                <S.TextInfoDescription>{props.good.webData.makerTitle}</S.TextInfoDescription>
                            </S.TextInfo>
                            <S.TextInfo>
                                <S.TextInfoName>Дествующее вещество: </S.TextInfoName>
                                <S.TextInfoDescription>
                                    {props.good.webData.mnnRusTitle}
                                </S.TextInfoDescription>
                            </S.TextInfo>
                        </S.TextInfoContainer>
                        {markerContainer()}
                    </S.FullInfo>
                </S.InfoContainer>
            </S.GeneralBlock>
            <S.PriceContainer>
                <S.PriceDataContainer>
                    <BasketPriceBlock
                        price={props.good.priceData}
                        priceSum={props.good.priceSumData}
                        isMinPrice={props.good.isMinPrice}
                        count={props.good.count}
                        changePriceHandler={changePriceHandler}
                    />
                </S.PriceDataContainer>
            </S.PriceContainer>
        </S.Container>
    )
}

BasketItem.propTypes = {
    good: PropTypes.shape({
        webData: _WebDataPropTypes,
        priceData: _PricePropTypes,
        priceSumData: PropTypes.shape({
            priceStock: PropTypes.number,
            priceDelivery: PropTypes.number
        }).isRequired,
        deliveryStatus: PropTypes.any,
        isInAnothersStock: PropTypes.bool.isRequired,
        isMinPrice: PropTypes.bool.isRequired,
        IsDemand: PropTypes.bool.isRequired
    }).isRequired,
    isMinPrice: PropTypes.bool.isRequired
}

export default BasketItem
