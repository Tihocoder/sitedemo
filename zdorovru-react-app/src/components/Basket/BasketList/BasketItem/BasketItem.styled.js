import styled from 'styled-components'

export const Container = styled.div`
    display: flex;
    justify-content: space-between;
`

export const GoodImage = styled.img`
    width: 100%;
`
export const ImageContainer = styled.div`
    box-sizing: border-box;
    display: flex;
    align-items: flex-start;
    cursor: pointer;
    width: 12rem;
`

export const MarkerContainer = styled.div`
    display: flex;
    flex-wrap: wrap;
    & > div {
        margin: 0.2rem;
    }
`
export const PriceContainer = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    align-items: flex-end;
    height: 100%;
`

export const PriceDataContainer = styled.div``

export const TextInfoDescription = styled.div`
    color: ${(props) => props.theme.colors.getGrayColor(0.75)};
    font-weight: bold;
`

export const TextInfoName = styled.div`
    color: ${(props) => props.theme.colors.getGrayColor(0.6)};
`

export const InfoContainer = styled.div`
    display: flex;
    height: 100%;
    width: 100%;
    padding: 0 0.3rem;
    flex-direction: column;
    justify-content: space-between;
`

export const GeneralBlock = styled.div`
    display: flex;
    padding: 0 0.3rem;
`

export const FullInfo = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    height: 100%;
`
export const FullTitle = styled.div`
    color: ${(props) => props.theme.colors.getGrayColor(0.75)};
    font-weight: bold;
    font-size: 1.1rem;
    margin-bottom: 1rem;
    cursor: pointer;
`

export const TextInfoContainer = styled.div``

export const ButtonBasketContainer = styled.div`
    width: 11rem;
`

export const TextInfo = styled.div`
    font-size: 0.9rem;
    display: flex;
    margin-bottom: 0.4rem;
`
