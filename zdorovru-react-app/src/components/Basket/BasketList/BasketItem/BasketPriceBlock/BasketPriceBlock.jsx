import React from 'react'
import PropTypes from 'prop-types'
import * as S from './BasketPriceBlock.styled'
import { _PricePropTypes } from '../../../../../consts'
import CountChanger from '../../../../shared/CountChanger/CountChanger'
import SymbolRuble from '../../../../shared/SymbolRuble/SymbolRuble'

const priceBlock = (title, price) => {
    if (!price) return <React.Fragment />
    return (
        <S.Price>
            <S.PriceDescription>{title}</S.PriceDescription>
            <S.PriceDataBody>
                <S.PriceDataActual>
                    <S.PriceNumberActualBold>{price}</S.PriceNumberActualBold>
                    <SymbolRuble />
                </S.PriceDataActual>
            </S.PriceDataBody>
        </S.Price>
    )
}
const prcieItem = (title, price, priceDiscount, isMinPrice) => {
    if (!price) return <React.Fragment />
    const isPriceOldShow = priceDiscount && priceDiscount < price ? true : false
    const priceGeneral = priceDiscount || price
    return (
        <S.PriceItemBLock>
            <S.PriceItemTitle>{title}</S.PriceItemTitle>
            <S.PriceItemData>
                {isPriceOldShow && (
                    <S.PriceItemOld>
                        {price} <SymbolRuble />
                    </S.PriceItemOld>
                )}
                <S.PriceItem>
                    {`${isMinPrice ? 'От ' : ''} ${priceGeneral}`}
                    <SymbolRuble />
                </S.PriceItem>
            </S.PriceItemData>
        </S.PriceItemBLock>
    )
}

const BasketPriceBlock = (props) => {
    return (
        <S.Container>
            <S.PriceDataContainer>
                {priceBlock('Цена самовывоза', props.priceSum.priceStock)}
                {priceBlock('Цена доставки', props.priceSum.priceDelivery)}
            </S.PriceDataContainer>
            <S.PriceChangeContainer>
                <S.CountChangeContainer>
                    <CountChanger value={props.count} changeValueHandler={props.changePriceHandler} />
                </S.CountChangeContainer>
                {prcieItem(
                    'Цена самовывоза за 1 шт.',
                    props.price.priceStockNormal,
                    props.price.priceStockDiscount,
                    props.isMinPrice
                )}
                {prcieItem(
                    'Цена доставки за 1 шт.',
                    props.price.priceDeliveryNormal,
                    props.price.priceDeliveryDiscount,
                    props.isMinPrice
                )}
            </S.PriceChangeContainer>
        </S.Container>
    )
}

BasketPriceBlock.propTypes = {
    price: _PricePropTypes,
    priceSum: PropTypes.shape({
        priceStock: PropTypes.number,
        priceDelivery: PropTypes.number
    }),
    changePriceHandler: PropTypes.func.isRequired,
    isMinPrice: PropTypes.bool.isRequired,
    count: PropTypes.number.isRequired
}

export default BasketPriceBlock
