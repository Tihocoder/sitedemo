import styled from 'styled-components'

export const Container = styled.div`
    display: flex;
`

export const PriceDataContainer = styled.div``

export const Price = styled.div`
    flex-direction: column;
    display: flex;
    align-items: flex-end;
`
export const PriceDataBody = styled.div`
    display: flex;
`
export const PriceDescription = styled.span`
    color: ${(props) => props.theme.colors.getGrayColor(0.6)};
    white-space: nowrap;
    font-size: 0.8rem;
`
export const PriceDataActual = styled.div`
    font-size: 1rem;
    display: flex;
    flex-wrap: nowrap;
    flex-direction: row;
    align-items: center;
    font-weight: bold;
    & span:first-child {
        margin-right: 0.2rem;
    }
`
export const PriceDataOld = styled.div`
    font-size: 0.8rem;
    display: flex;
    flex-direction: row;
    flex-wrap: nowrap;
    color: ${(props) => props.theme.colors.accentGreenLight};
    text-decoration: line-through;
    align-items: center;
    margin-right: 0.3rem;
    & span:first-child {
        margin-right: 0.2rem;
    }
`
export const PriceNumberActualBold = styled.span`
    font-size: 1.4rem;
`
export const PriceNumberOld = styled.span`
    font-size: 1rem;
`
export const PriceItemBLock = styled.div`
    display: flex;
    flex-direction: column;
    align-items: flex-end;
`
export const PriceItem = styled.span`
    color: ${(props) => props.theme.colors.greyLight};
`

export const PriceItemTitle = styled.span`
    color: ${(props) => props.theme.colors.getGrayColor(0.6)};
    font-size: 0.8rem;
    white-space: nowrap;
`
export const PriceItemData = styled.div`
    display: flex;
`
export const PriceItemOld = styled.span`
    font-size: 0.8rem;
    display: flex;
    flex-direction: row;
    flex-wrap: nowrap;
    color: ${(props) => props.theme.colors.accentGreenLight};
    text-decoration: line-through;
    align-items: center;
    margin-right: 0.3rem;
`

export const PriceChangeContainer = styled.div`
    display: flex;
    flex-direction: column;
    margin-left: 2rem;
    align-items: flex-end;
`
export const CountChangeContainer = styled.div`
    width: 8rem;
    height: 2.1rem;
    font-size: 1.4rem;
    justify-content: flex-end;
`
