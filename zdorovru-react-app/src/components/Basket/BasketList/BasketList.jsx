import React from 'react'
import PropTypes from 'prop-types'
import * as S from './BasketList.styled'
import BasketItem from './BasketItem/BasketItem'

const BasketList = props => {

    return (
        <S.Container>
            {props.basketItems.map(x=> <BasketItem key={x.webData.webId} good={x}/>)}
        </S.Container>
    )
}

BasketList.propTypes = {
    basketItems: PropTypes.array.isRequired
}

export default BasketList
