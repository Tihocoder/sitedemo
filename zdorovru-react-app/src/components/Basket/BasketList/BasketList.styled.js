import styled from 'styled-components'

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    & > div {
        padding: .5rem 0;
        border-bottom:1px solid ${props => props.theme.colors.grayLight};
    }
    & > div:last-child {
        border-bottom: none;
    }
    width: 77vw;
`