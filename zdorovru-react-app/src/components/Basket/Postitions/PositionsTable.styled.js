import styled from 'styled-components'
import * as SharedStyled from '../../../styles/base.styled'
import CountChanger from '../../shared/CountChanger/CountChanger'

export const Container = styled.div`
    display: flex;
    ${SharedStyled.Table} {
        box-shadow: none;
        border-collapse: separate;
        border-spacing: 0 0.5rem;
    }
    ${SharedStyled.TableHeaderRow} {
        & > th {
            vertical-align: bottom;
            text-align: left;
        }
        & > th.double {
            vertical-align: center;
            text-align: left;
        }
        & > th.center {
            text-align: center;
        }
        & > th.right {
            text-align: right;
        }
        border-bottom: none;
    }
    ${SharedStyled.TableRow} {
        border-radius: 0.3rem;
        box-shadow: ${(props) => props.theme.shadows.shadowKit};
    }
    ${SharedStyled.TableColumn} {
        text-align: left;
        vertical-align: middle;
        &.right {
            text-align: right;
        }
        &.td-block > div {
            width: 7rem;
        }
    }
`

export const WarningNonSelectedBlock = styled.span`
    font-weight: bold;
`
