import React from 'react'
import PropTypes from 'prop-types'
import * as SharedStyled from '../../../styles/base.styled.js'
import * as S from './PositionsTable.styled'
import { _PricePropTypes, _WebDataPropTypes } from '../../../consts'
import PriceBlock from '../../PriceBlock/PriceBlock'
import CountChanger from '../../shared/CountChanger/CountChanger'

const PositionsTable = (props) => {
    const changePriceHandler = (webId) => async (count) => {
        await props.changePriceHandler(webId, count)
    }
    const generateDate = (date) => {
        const dateInstance = new Date(date)
        let options = { year: 'numeric', month: 'numeric', day: 'numeric' }
        return dateInstance.toLocaleDateString('ru-RU', options)
    }
    const getDateStock = (position) => {
        if (props.isSelectedStock) {
            return position.stockPrice.isDemand ? generateDate(position.stockPrice.aptEtaDateTime) : 'Сегодня'
        }
        return <S.WarningNonSelectedBlock>Аптека не выбрана</S.WarningNonSelectedBlock>
    }
    const getDateDelivery = (position) => {
        if (props.isSelectedShipAddress) {
            return position.deliveryPrice.isDemand
                ? generateDate(position.deliveryPrice.aptEtaDateTime)
                : 'Сегодня'
        }
        return <S.WarningNonSelectedBlock>Адрес не выбран</S.WarningNonSelectedBlock>
    }
    return (
        <S.Container>
            <SharedStyled.Table>
                <SharedStyled.TableHeader>
                    <SharedStyled.TableHeaderRow>
                        <SharedStyled.TableHeaderColumn rowSpan="2">
                            Наименование
                        </SharedStyled.TableHeaderColumn>
                        <SharedStyled.TableHeaderColumn rowSpan="2">
                            Производитель
                        </SharedStyled.TableHeaderColumn>
                        <SharedStyled.TableHeaderColumn colSpan="2">Самовывоз</SharedStyled.TableHeaderColumn>
                        <SharedStyled.TableHeaderColumn colSpan="2">Доставка</SharedStyled.TableHeaderColumn>
                        <SharedStyled.TableHeaderColumn rowSpan="2" className="center">
                            Кол-во
                        </SharedStyled.TableHeaderColumn>
                    </SharedStyled.TableHeaderRow>
                    <SharedStyled.TableHeaderRow>
                        <SharedStyled.TableHeaderColumn className="double">
                            Цена
                        </SharedStyled.TableHeaderColumn>
                        <SharedStyled.TableHeaderColumn className="double">
                            Дата
                        </SharedStyled.TableHeaderColumn>
                        <SharedStyled.TableHeaderColumn className="double">
                            Цена
                        </SharedStyled.TableHeaderColumn>
                        <SharedStyled.TableHeaderColumn className="double">
                            Дата
                        </SharedStyled.TableHeaderColumn>
                    </SharedStyled.TableHeaderRow>
                </SharedStyled.TableHeader>
                <SharedStyled.TableBody>
                    {props.positions.map((position) => (
                        <SharedStyled.TableRow>
                            <SharedStyled.TableColumn>{`${position.webData.drugTitle} ${position.webData.outFormTitle}`}</SharedStyled.TableColumn>
                            <SharedStyled.TableColumn>{position.webData.makerTitle}</SharedStyled.TableColumn>
                            <SharedStyled.TableColumn>
                                {position.stockPriceSum
                                    ? props.isSelectedStock
                                        ? `${position.stockPriceSum} ₽`
                                        : `от ${position.stockPriceSum} ₽`
                                    : 'Нет в наличии'}
                            </SharedStyled.TableColumn>
                            <SharedStyled.TableColumn>{getDateStock(position)}</SharedStyled.TableColumn>
                            <SharedStyled.TableColumn>
                                {position.deliveryPriceSum
                                    ? `${position.deliveryPriceSum} ₽`
                                    : 'Нет в наличии'}
                            </SharedStyled.TableColumn>
                            <SharedStyled.TableColumn>{getDateDelivery(position)}</SharedStyled.TableColumn>
                            <SharedStyled.TableColumn className="right td-block" align="right">
                                <CountChanger
                                    value={position.count}
                                    changeValueHandler={changePriceHandler(position.webData.webId)}
                                />
                            </SharedStyled.TableColumn>
                        </SharedStyled.TableRow>
                    ))}
                </SharedStyled.TableBody>
            </SharedStyled.Table>
        </S.Container>
    )
}

PositionsTable.propTypes = {
    positions: PropTypes.arrayOf(
        PropTypes.shape({
            webData: _WebDataPropTypes,
            stockPrice: _PricePropTypes,
            deliveryPrice: _PricePropTypes,
            priceSumData: PropTypes.shape({
                priceStock: PropTypes.number,
                priceDelivery: PropTypes.number
            }).isRequired,
            deliveryStatus: PropTypes.any,
            isInAnothersStock: PropTypes.bool.isRequired,
            isMinPrice: PropTypes.bool.isRequired,
            IsDemand: PropTypes.bool.isRequired
        }).isRequired
    ).isRequired,
    isSelectedShipAddress: PropTypes.bool.isRequired,
    isSelectedStock: PropTypes.bool.isRequired,
    changePriceHandler: PropTypes.func.isRequired
}

export default PositionsTable
