import styled, { css } from 'styled-components'

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    position: relative;
`
export const AbsoluteContainer = styled.div`
    height: 100vh;
    width: 100%;
    position: fixed;
    left: 0;
    top: 0;
    display: flex;
    justify-content: center;
    align-items: center;
    z-index: 10;
`

export const Window = styled.div`
    min-width: 25rem;
    background: #fff;
    border-radius: 7px;
    box-shadow: 0 19px 38px rgba(0, 0, 0, 0.3), 0 15px 12px rgba(0, 0, 0, 0.22);
    padding: 2rem;
`
export const Header = styled.header`
    height: 3rem;
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin-bottom: 1rem;
`

export const HeaderTitle = styled.h1`
    font-size: 1rem;
    color: ${(props) => props.theme.colors.getGrayColor(0.6)};
`
export const StockSelectedTitle = styled.h2`
    font-size: 1rem;
    font-weight: normal;
    color: ${(props) => props.theme.colors.getGrayColor(0.8)};
`

export const ButtonClose = styled.button`
    box-shadow: 0px;
    outline: none;
    margin: 0;
    background-color: inherit;
    border: none;
    cursor: pointer;
    width: 2.6rem;

    & svg {
        fill: ${(props) => props.theme.colors.blackTr};
    }

    &:hover > svg {
        fill: ${(props) => props.theme.colors.black};
    }
`
export const Body = styled.div`
    min-height: 10rem;
`
export const BasketEmptyContainer = styled.div`
    display: flex;
    justify-content: space-between;
    flex-direction: column;

    & > button {
        max-width: 18rem;
    }

    min-height: 6rem;
`

export const WarningIconContainer = styled.div`
    display: flex;
    height: 1.85rem;
`
export const WarningImage = styled.img.attrs({
    src: `${process.env.BASE_PATH}/images/warning.svg`
})`
    height: 100%;
    width: auto;
`

export const BasketEmptyDescription = styled.div`
    color: ${(props) => props.theme.colors.pureRed};
    display: flex;
    align-items: center;

    & > svg {
        height: 100%;
        min-height: 1.4rem;
        margin-right: 1rem;
    }
`

export const Footer = styled.footer`
    display: flex;
    font-size: 0.9rem;
    justify-content: flex-end;
    padding-top: 2rem;

    & > a {
        width: 6rem;
    }

    & > button:last-child {
        margin-right: 0;
    }
`

export const ButtonsContainer = styled.div`
    display: flex;
    flex-direction: column;

    & > div {
        padding: 0.8rem 0;
    }
`

export const ModalBodyDeletePositionContainer = styled.div`
    display: flex;
    flex-direction: column;
    height: 8rem;
    justify-content: space-between;
`

export const ButtonBlockCreateOrder = styled.div`
    display: flex;
    align-items: center;

    & > button {
        width: 14rem;
    }
`

export const ButtonBlockCancel = styled.div`
    display: flex;
    justify-content: flex-end;

    & button {
        width: 5rem;
    }
`
export const OrderPrice = styled.span`
    white-space: nowrap;
    font: inherit;
    font-size: 1.2rem;
    margin-right: 1rem;
`

export const OrderBasketContainer = styled.div`
    display: flex;
    justify-content: space-between;
`
export const BasketBlockForOrderPrice = styled.div`
    height: 100%;
    padding: 1rem 0.3rem;
    width: 100%;
    max-width: 22rem;
    display: flex;
    justify-content: flex-end;
`
export const OrderPriceBlock = styled.div`
    position: sticky;
    box-shadow: ${(props) => props.theme.shadows.shadowKit};
    width: 100%;
    box-sizing: border-box;
    padding: 0.9rem;
    border-radius: 0.3rem;

    & > * {
        margin-top: 1rem;
    }

    & > *:first-child {
        margin-top: 0;
    }
`

export const PriceBlockList = styled.div`
    display: flex;
    flex-direction: column;
`
export const PriceItem = styled.div`
    display: flex;
    justify-content: space-between;
    font-size: 1rem;
`
export const PriceItemTitle = styled.span``

export const PriceItemSum = styled.span`
    white-space: nowrap;
`
export const PriceItemGeneralSum = styled.span`
    white-space: nowrap;
    font-weight: bold;
`

export const ShipmentDescriptionContainer = styled.div`
    display: flex;
    flex-direction: column;
`

export const ShipmentDescriptionTitle = styled.span`
    font-size: 0.82rem;
    color: ${(props) => props.theme.colors.getGrayColor(0.6)};
`

export const ShipmentDescription = styled.span``

export const TitleInBlock = styled.div`
    width: 100%;
`

export const PositionContainer = styled.div`
    display: flex;
    flex-direction: column;
    padding: 1rem 0;

    & > *:first-child {
        margin-top: 0;
    }

    min-width: 42.438rem;
    max-width: 48rem;
    width: 100%;
`

export const ToggleShipmentContainer = styled.div`
    display: flex;
    width: 100%;
`
export const InformationBlock = styled.div`
    width: 100%;
`
export const Information = styled.div`
    margin-top: 1rem;
    box-sizing: border-box;
    height: 100%;
    padding: 0.6rem 0;
    display: flex;
    align-items: center;
    color: ${(props) => props.theme.colors.getRedheadColor(0.8)};

    & > svg {
        height: 100%;
    }
`
export const InformationText = styled.span`
    margin-left: 0.5rem;
`

export const TabShipment = styled.div`
    padding: 0.5rem 0.5rem;
    width: 50%;
    height: 3.9rem;
    cursor: pointer;
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    justify-content: space-between;

    &:first-child {
        border-top-left-radius: 0.3rem;
        border-bottom-left-radius: 0.3rem;
    }

    &:last-child {
        border-top-right-radius: 0.3rem;
        border-bottom-right-radius: 0.3rem;
    }

    ${(props) =>
        props.isActive
            ? css`
                  background-color: ${props.theme.colors.getGreenColor(1)};
                  color: ${(props) => props.theme.colors.white};

                  :hover {
                      background-color: ${props.theme.colors.getGreenColor(0.85)};
                  }
              `
            : css`
                  background-color: ${props.theme.colors.getGrayColor(0.03)};

                  :hover {
                      background-color: ${props.theme.colors.getGrayColor(0.05)};
                  }
              `}
`

export const TabShipmentTitle = styled.div`
    line-height: 1.1rem;

    & > span {
        font-size: 0.8rem;
    }

    display: flex;
    flex-direction: column;
`

export const TabShipmentPrice = styled.div`
    display: flex;
    align-items: center;
`
export const TabShipmentPriceCost = styled.span`
    font-size: 1.2rem;
    margin-left: 0.3rem;
`

export const TabShipmentHeaderTitle = styled.span`
    white-space: nowrap;
`

export const TabShipmentPrefixLight = styled.span`
    white-space: nowrap;
`
export const OrderPriceBlockButtonContainer = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
    height: 5.5rem;
    justify-content: space-between;

    ${(props) =>
        props.isShowedWarninBorder &&
        css`
            & button:first-child {
                border: 2px solid ${props.theme.colors.accentRed};
            }
        `}
`

export const OrderPriceButton = styled.button`
    border: 0;
    cursor: pointer;
    ${(props) =>
        props.isLight
            ? css`
                  border: 1px solid ${(props) => props.theme.colors.accentGreen};
                  color: ${(props) => props.theme.colors.blackGreenText};
                  background-color: ${(props) => props.theme.colors.white};

                  :hover {
                      background-color: ${(props) => props.theme.colors.greenBackground};
                  }
              `
            : css`
                  background-color: ${(props) => props.theme.colors.accentGreen};
                  color: ${(props) => props.theme.colors.white};

                  &:hover {
                      background-color: ${(props) => props.theme.colors.accentGreenLight};
                  }
              `}

    ${(props) =>
        props.disabled &&
        css`
            opacity: 0.6;

            &:hover {
                background-color: ${(props) => props.theme.colors.getGrayColor(0.1)};
            }

            // cursor: default;
        `}
  border-radius: 0.3rem;
    font-size: inherit;
    box-sizing: border-box;
    padding: 0.7rem 1.2rem;
`

export const ButtonModalContainer = styled.div`
    display: flex;
    justify-content: flex-end;
`
export const ErrorMessage = styled.div`
    color: ${(props) => props.theme.colors.getRedheadColor(1)};
    padding: 1rem 0;
`

export const BodyModalContainer = styled.div`
    display: flex;
    flex-direction: column;
    max-width: 32rem;
    font-size: 1.2rem;
`
export const ModalFooterContainer = styled.div`
    width: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 1rem;
    & > button {
        padding: 1rem 4rem;
    }
`

export const BodyModalContentContainer = styled.div``

export const LoaderWrapper = styled.div`
    height: 100%;
    width: calc(100% + 1rem);
    margin-left: -0.5rem;
    min-height: 50vh;
    align-items: center;
    position: absolute;
    justify-content: center;
    background-color: white;
    z-index: 10;
`
export const DesktopBodyModalContainer = styled.div`
    display: flex;
    flex-direction: column;
    max-width: 23.063rem;
`
export const DesktopBodyModalTitle = styled.span`
    font-style: normal;
    font-weight: normal;
    font-size: 20px;
    line-height: 32px;
`
export const DesktopBodyModalP = styled.p`
    text-indent: 1rem;
    margin: 8px 0;
    line-height: 24px;
`
