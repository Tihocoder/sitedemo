import styled, { css } from 'styled-components'
import { PromotionBannerTypes } from './Banners'

export const PageContainer = styled.div`
    display: flex;
    flex-direction: column;
`
export const BlockInfo = styled.div`
    border-radius: 7px;
    padding: 1rem 2rem;
    background-color: ${(props) => props.theme.colors.getGrayColor(0.03)};
    color: ${(props) => props.theme.colors.pureRed};
    font-size: 1.1rem;
`

export const BlockDefaultInfo = styled.div`
    margin-top: 3rem;
    border-radius: 7px;
    padding: 1rem 2rem;
    color: ${(props) => props.theme.colors.getGrayColor(0.7)};
    background-color: ${(props) => props.theme.colors.getGrayColor(0.03)};
`

export const Ul = styled.ul``

export const Li = styled.li``

export const P = styled.p``

export const HT = styled.h1`
    font-size: 1rem;
`

export const BoldSpan = styled.span`
    font-weight: bold;
`

export const BlockInfoLink = styled.a`
    color: #3580fc;
    border-bottom: 1px dashed #3580fc;
`

export const Container = styled.div`
    position: relative;
    display: flex;
    justify-content: space-between;
    box-sizing: border-box;
    z-index: 0;

    flex-direction: row;
    align-items: flex-start;
    flex: none;
    order: 3;
    align-self: stretch;
    flex-grow: 0;
`

export const BannerBlock = styled.div`
    height: 100%;
    flex: none;
    order: 0;
    flex-grow: 1;
    ${(props) => {
        if (props.type === PromotionBannerTypes.GeneralBigBanner) {
            return css`
                /* width: 533px;
                height: 300px; */
                height: 300px;
                width: 100%;
                max-width: 533px;
            `
        }
        if (props.type === PromotionBannerTypes.GeneralRegular) {
            return css`
                /* width: 300px; */
                width: 100%;
                height: 300px;
                max-width: 300px;
            `
        }
        if (props.type === PromotionBannerTypes.GeneralSmallUp) {
            return css`
                width: 100%;
                height: 145px;
                max-width: 327px;
            `
        }
        if (props.type === PromotionBannerTypes.GeneralSmallDown) {
            return css`
                width: 100%;
                height: 145px;
                max-width: 327px;
            `
        }
    }};
    ${(props) =>
        props.imageUrl &&
        css`
            background: url('${props.imageUrl}') no-repeat center center;
            background-size: cover;
        `}

    background-color: ${(props) => props.theme.colors.getGrayColor(0.03)};
`
export const ImageLinkBlock = styled.a`
    height: 100%;
    ${(props) => {
        if (props.type === PromotionBannerTypes.GeneralBigBanner) {
            return css`
                width: 45.17%;
            `
        }
        if (props.type === PromotionBannerTypes.GeneralRegular) {
            return css`
                width: 25.425%;
            `
        }
        if (props.type === PromotionBannerTypes.GeneralSmallUp) {
            return css`
                width: 100%;
                height: 145px;
            `
        }
        if (props.type === PromotionBannerTypes.GeneralSmallDown) {
            return css`
                width: 100%;
                height: 145px;
            `
        }
    }};
`

export const Img = styled.img`
    width: 100%;
    height: 100%;
`
export const Column = styled.div`
    height: 100%;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    /* width: 327px; */
    width: 27.712%;
    height: 300px;
    & a:last-child {
        display: flex;
        align-items: flex-end;
    }
    & a:first-child {
        display: flex;
        align-items: flex-start;
    }
`
export const BannerContainer = styled.div`
    display: flex;
    height: 100%;
`
// export const Block1 = styled.div`
//      display: flex;
//     flex-basis:40%;
// `
// export const Block2 = styled.div`
//     display: flex;
//     flex-basis:30%;
//     justify-content: space-between;
//     flex-direction: column;
// `
// export const Block3 = styled.div`
//     width: 30%;
// `
