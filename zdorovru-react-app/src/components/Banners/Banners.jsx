import React, { useEffect, useState } from 'react'
import * as S from './Banners.styled'
import { getBanners } from '../../api/siteApis'
import Link from 'next/link'

export const PromotionBannerTypes = {
    GeneralBigBanner: 1,
    GeneralRegular: 2,
    GeneralSmallUp: 3,
    GeneralSmallDown: 4,
    Default: 5
}

const Banners = (props) => {
    const [banners, setBanners] = useState({
        big: {
            id: 0,
            promotionId: 0,
            promotionTypes: 0,
            promotionTypeName: '',
            beginDate: '',
            endDate: '',
            promotionName: '',
            promotionDescription: '',
            type: 1,
            typeName: '',
            imageUrl: ''
        },
        regular: {
            id: 0,
            promotionId: 0,
            promotionTypes: 0,
            promotionTypeName: '',
            beginDate: '',
            endDate: '',
            promotionName: '',
            promotionDescription: '',
            type: 2,
            typeName: '',
            imageUrl: ''
        },
        smallUp: {
            id: 0,
            promotionId: 0,
            promotionTypes: 0,
            promotionTypeName: '',
            beginDate: '',
            endDate: '',
            promotionName: '',
            promotionDescription: '',
            type: 3,
            typeName: '',
            imageUrl: ''
        },
        smallDown: {
            id: 0,
            promotionId: 0,
            promotionTypes: 0,
            promotionTypeName: '',
            beginDate: '',
            endDate: '',
            promotionName: '',
            promotionDescription: '',
            type: 4,
            typeName: '',
            imageUrl: ''
        }
    })
    const [isLoading, setIsLoading] = useState(true)

    useEffect(() => {
        const initBanners = async () => {
            const responseBanners = await getBanners()
            if (responseBanners) {
                setBanners(responseBanners)
            }
        }
        initBanners()

        setIsLoading(false)
    }, [])

    const drawBanner = (banner) => {
        if (banner.id && banner.imageUrl) {
            return (
                <Link href={`/catalog/promotion/${banner.promotionId}`} passHref>
                    <S.ImageLinkBlock key={banner.id} type={banner.type}>
                        <S.BannerBlock type={banner.type} imageUrl={banner.imageUrl}></S.BannerBlock>
                    </S.ImageLinkBlock>
                </Link>
            )
        }
        return <S.BannerBlock type={banner.type} />
    }

    return (
        <S.PageContainer>
            {/* <S.BlockInfo>Внимание! Аптека Славянский бульвар 6,7 января работает с 9:00 до 20:00</S.BlockInfo> */}
            <S.Container>
                {drawBanner(banners.big)}
                {drawBanner(banners.regular)}
                <S.Column>
                    {drawBanner(banners.smallUp)}
                    {drawBanner(banners.smallDown)}
                </S.Column>
            </S.Container>
            {/* <S.BlockDefaultInfo id="jobtime">
                <S.P>
                    <S.BoldSpan>График работы аптек в новогодние праздники:</S.BoldSpan>{' '}
                    <S.P>
                        31 декабря 2020 аптеки работают до 18:00, и 1 января 2021 начинают работу в 15:00,{' '}
                    </S.P>
                    <S.P>
                        <S.BoldSpan>за исключением:</S.BoldSpan>
                    </S.P>
                </S.P>
                <S.Ul>
                    <S.HT>31.12.2021</S.HT>
                    <S.Li>Бибирево 7:00-17:00</S.Li>
                    <S.Li>Коломна 10:00-20:00</S.Li>
                    <S.Li>Свиблово 8:00-20:00</S.Li>
                    <S.HT>01.01.2021</S.HT>
                    <S.Li>Бибирево – выходной</S.Li>
                    <S.Li>Водный стадион – выходной</S.Li>
                    <S.Li>Заречная – выходной</S.Li>
                    <S.Li>Коминтерна – выходной</S.Li>
                    <S.Li>Речной вокзал – выходной</S.Li>
                    <S.Li>Тимирязевская – выходной</S.Li>
                    <S.Li>Тушинская – выходной</S.Li>
                    <S.Li>Химки – выходной</S.Li>
                    <S.Li>Щербинка – выходной</S.Li>
                    <S.Li>Юго-западная – выходной</S.Li>
                    <S.Li>Ногинск – выходной</S.Li>
                    <S.Li>Коминтерна (Нижний Новгород) – выходной</S.Li>
                    <S.HT>Славянский бульвар:</S.HT>
                    <S.Li>
                        1, 2 января – выходной, 3, 4, 5 января с 9:00 до 21:00, 6,7 января с 9:00 до 20:00
                        далее по обычному графику
                    </S.Li>
                </S.Ul>
            </S.BlockDefaultInfo> */}
        </S.PageContainer>
    )
}

Banners.propTypes = {}

export default Banners
