import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import * as Theme from '../../styles/theme-const'
import { _PricePropTypes } from '../../consts'
import Marker from '../shared/Marker/Marker'
import { generateDate } from '../../helpers/site'

const MarkerContainer = (props) => {
    const [markers, setMarkers] = useState([])

    useEffect(() => {
        let newMarkers = []
        if (!props.options.isBasket && !props.isPositions) {
            // Если дата поставки для доставки и самовывоза совпадает
            if (
                props.deliveryPrice?.isDemand &&
                props.stockPrice.isDemand &&
                generateDate(props.deliveryPrice.aptEtaDateTime) ===
                    generateDate(props.stockPrice.aptEtaDateTime)
            ) {
                newMarkers.push(
                    <Marker key={'generalDemand'} backgroundColor={Theme.ColorConsts.accentOrrange}>
                        Поставка {generateDate(props.stockPrice.aptEtaDateTime)}
                    </Marker>
                )
            }
            // Если дата поставки для доставки и самовывоза не совпадает
            if (
                props.deliveryPrice?.isDemand &&
                props.stockPrice.isDemand &&
                generateDate(props.deliveryPrice.aptEtaDateTime) !==
                    generateDate(props.stockPrice.aptEtaDateTime)
            ) {
                newMarkers.push(
                    <Marker key={'deliveryisDemand'} backgroundColor={Theme.ColorConsts.accentOrrange}>
                        Доставка {generateDate(props.deliveryPrice.aptEtaDateTime)}
                    </Marker>
                )
                newMarkers.push(
                    <Marker key={'stockisDemand'} backgroundColor={Theme.ColorConsts.accentOrrange}>
                        Самовывоз {generateDate(props.stockPrice.aptEtaDateTime)}
                    </Marker>
                )
            }
            // если поставка только для доставки
            if (props.deliveryPrice?.isDemand && !props.stockPrice.isDemand) {
                newMarkers.push(
                    <Marker key={'deliveryisDemand'} backgroundColor={Theme.ColorConsts.accentOrrange}>
                        Доставка {generateDate(props.deliveryPrice.aptEtaDateTime)}
                    </Marker>
                )
            }
            // если поставка только для cамовывоза
            if (!props.deliveryPrice?.isDemand && props.stockPrice?.isDemand) {
                newMarkers.push(
                    <Marker key={'stockisDemand'} backgroundColor={Theme.ColorConsts.accentOrrange}>
                        Самовывоз {generateDate(props.stockPrice.aptEtaDateTime)}
                    </Marker>
                )
            }
        } else if (!props.isPositions) {
            if (props.options.isDelivery && props.deliveryPrice.isDemand) {
                newMarkers.push(
                    <Marker key={'isDemand'} backgroundColor={Theme.ColorConsts.accentOrrange}>
                        Доставка {generateDate(props.deliveryPrice.aptEtaDateTime)}
                    </Marker>
                )
            } else if (!props.options.isDelivery && props.stockPrice.isDemand) {
                newMarkers.push(
                    <Marker key={'isDemand'} backgroundColor={Theme.ColorConsts.accentOrrange}>
                        Самовывоз {generateDate(props.stockPrice.aptEtaDateTime)}
                    </Marker>
                )
            }
        }
        if (props.isOriginalGood) {
            newMarkers.push(
                <Marker key={'isOriginalGood'} backgroundColor={Theme.ColorConsts.greenYellowLight}>
                    Оригинальный препарат
                </Marker>
            )
        }
        if (props.isCold) {
            newMarkers.push(
                <Marker key={'isCold'} backgroundColor={Theme.ColorConsts.blueLight}>
                    Хранение в холоде
                </Marker>
            )
        }
        if (props.isStrictlyByPrescription) {
            newMarkers.push(
                <Marker key={'isStrictlyByPrescription'} backgroundColor={Theme.ColorConsts.redLight}>
                    Строго по рецепту
                </Marker>
            )
        }
        // if (props.anotherOutFormsQty) {
        //     newMarkers.push(
        //         <Marker key={'anotherOutFormsQty'} backgroundColor={Theme.ColorConsts.orange}>
        //             формы выпуска ({props.anotherOutFormsQty})
        //         </Marker>
        //     )
        // }

        // if (props.analoguesQty && props.analoguesQty > 0) {
        //     markers.push(
        //         <Marker key={'analoguesQty'} backgroundColor={Theme.ColorConsts.orange}>
        //             Аналоги ({props.analoguesQty})
        //         </Marker>
        //     )
        // }
        setMarkers(newMarkers)
    }, [props])

    return <React.Fragment>{markers.map((x) => x)}</React.Fragment>
}

MarkerContainer.propTypes = {
    options: PropTypes.shape({
        isBasket: PropTypes.bool,
        isDelivery: PropTypes.bool,
        isPositions: PropTypes.bool
    }),
    analoguesQty: PropTypes.number,
    isStrictlyByPrescription: PropTypes.bool,
    isOriginalGood: PropTypes.bool,
    isHidden: PropTypes.bool.isRequired,
    isCold: PropTypes.bool,
    isDemand: PropTypes.bool,
    aptEtaDateTime: PropTypes.string,
    stockPrice: _PricePropTypes,
    deliveryPrice: _PricePropTypes
}

MarkerContainer.defaultProps = {
    options: {
        isBasket: false,
        isDelivery: false,
        isPositions: false
    },
    isHidden: false
}

export default MarkerContainer
