import styled from 'styled-components'
import { ButtonElement } from '@shared/Button/Button.styled'

export const Container = styled.div`
    padding: 0.625rem;
    position: fixed;
    width: 100%;
    height: 100%;
    z-index: 70;
    top: 0;
    left: 0;
    display: flex;
    flex-direction: column;
    background-color: white;
    line-height: 24px;
    box-sizing: border-box;
    color: ${(props) => props.theme.colors.getGrayColor()};
    letter-spacing: 0.44px;
`

export const Paragraph = styled.p`
    text-indent: 1rem;
`

export const List = styled.ul`
    text-indent: 1rem;
    list-style-position: inside;
`

export const ListItem = styled.li``

export const ButtonsContainer = styled.div`
    display: flex;
    flex-direction: column;
    box-sizing: border-box;

    ${ButtonElement} {
        margin-top: 0.5rem;
        padding: 0.75rem;
    }
`
