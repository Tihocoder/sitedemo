// eslint-disable-next-line import/no-duplicates
import React, { useState } from 'react'
import PropTypes from 'prop-types'
import MobileStockSelector from '@mobilePages/MobileStockSelector/MobileStockSelector'
import MobileAddressSelector from '@mobilePages/MobileAddressSelector/MobileAddressSelector'
import { useSelector } from 'react-redux'
import Button from '@shared/Button/Button'
import * as Theme from '@styles/theme-const'
import * as S from './MessageSelectShipment.styled'

const MessageSelectShipment = (props) => {
    const [isShowedStockSelector, setIsShowedStockSelector] = useState(false)
    const [isShowedAddressSelector, setIsShowedAddressSelector] = useState(false)
    const shipment = useSelector((state) => state.customer.shipment)
    if (isShowedStockSelector) {
        return (
            <MobileStockSelector
                closeHandler={() => {
                    setIsShowedStockSelector(!isShowedStockSelector)
                }}
            />
        )
    }
    if (isShowedAddressSelector) {
        return (
            <MobileAddressSelector
                closeHandler={() => setIsShowedAddressSelector(!isShowedAddressSelector)}
                shipAddressId={shipment.shipAddressId}
            />
        )
    }
    return (
        <S.Container>
            <S.Paragraph>
                Для Вашего удобства рекомендуем выбрать аптеку самовывоза или адрес доставки. Это позволит:
            </S.Paragraph>
            <S.List>
                <S.ListItem>Видеть наличие товаров и цены в конкретной аптеке самовывоза</S.ListItem>
                <S.ListItem>
                    Определить возможность доставки и ассортимент товаров, доступных по конкретному адресу
                </S.ListItem>
            </S.List>
            <S.Paragraph>
                Выбор можно сделать позднее (в меню или в корзине), но не все товары, добавленные в корзину
                ранее, могут быть доступны для самовывоза или доставки из-за их отсутствия
            </S.Paragraph>
            <S.ButtonsContainer>
                <Button
                    elementSizeTypeValue={Theme.ElementSizeType.regular}
                    clickHandler={() => setIsShowedStockSelector(!isShowedStockSelector)}>
                    Выбрать аптеку самовывоза
                </Button>
                <Button
                    elementSizeTypeValue={Theme.ElementSizeType.regular}
                    clickHandler={() => setIsShowedAddressSelector(!isShowedAddressSelector)}>
                    Выбрать адрес доставки
                </Button>
                <Button
                    elementSizeTypeValue={Theme.ElementSizeType.regular}
                    clickHandler={props.addBasketHandler}>
                    Сделать выбор позднее
                </Button>
            </S.ButtonsContainer>
        </S.Container>
    )
}

MessageSelectShipment.propTypes = {
    // eslint-disable-next-line react/no-unused-prop-types
    addBasketHandler: PropTypes.func.isRequired
}

export default MessageSelectShipment
