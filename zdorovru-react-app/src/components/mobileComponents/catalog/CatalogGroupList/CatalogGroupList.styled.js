/* eslint-disable @typescript-eslint/no-unsafe-member-access */
import styled from 'styled-components'

export const Container = styled.div`
    display: flex;
    flex-direction: column;

    & > a {
        border-bottom: 1px solid ${(props) => props.theme.colors.getGrayColor(0.1)};
    }
    & > a:first-child {
        border-top: 1px solid ${(props) => props.theme.colors.getGrayColor(0.1)};
    }
`
