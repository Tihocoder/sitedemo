import React from 'react'
import PropTypes from 'prop-types'
import * as S from './CatalogGroupList.styled'
import { useRouter } from 'next/router'
import CatalogGroupItem from './CatalogGroupItem/CatalogGroupItem'
import { useEffect } from 'react'
import { removeGroupsShipment } from '../../../../actions/catalog'
import { useDispatch } from 'react-redux'
import MobileRowItem from '../../mobileShared/MobileRowItem/MobileRowItem'

const CatalogGroupList = ({ children }) => {
    const router = useRouter()
    const dispatch = useDispatch()

    const deleteGroups = async () => {
        await dispatch(removeGroupsShipment())
    }

    useEffect(() => {
        return () => {
            deleteGroups()
        }
    }, [router.asPath])
    return (
        <S.Container>
            {children.map((x) => (
                <MobileRowItem key={x.groupId} linkUrl={`${router.asPath}/${x.groupId}`} isLink isShowedRow>
                    {x.groupTitle}
                </MobileRowItem>
            ))}
        </S.Container>
    )
}

CatalogGroupList.propTypes = {}

export default CatalogGroupList
