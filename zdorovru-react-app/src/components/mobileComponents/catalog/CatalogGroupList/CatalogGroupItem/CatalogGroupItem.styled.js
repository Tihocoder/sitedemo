import styled from 'styled-components'

export const Container = styled.a`
    padding: 0.2rem;
    width: 100%;
    display: flex;
    justify-content: space-between;
`
export const Title = styled.span`
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
`

export const ArrowContainer = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    height: 100%;
    & > * {
        max-height: 1rem;
        max-width: 1rem;
    }
`
