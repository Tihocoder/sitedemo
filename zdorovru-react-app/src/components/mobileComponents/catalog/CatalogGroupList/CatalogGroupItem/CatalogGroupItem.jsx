import React from 'react'
import PropTypes from 'prop-types'
import Link from 'next/link'
import * as S from './CatalogGroupItem.styled'
import ArrowRight from '../../../../Icons/ArrowRight'

const CatalogGroupItem = (props) => {
    return (
        <Link href={`${props.currentGroupUrl}/${props.children.groupId}`} passHref>
            <S.Container>
                <S.Title>{props.children.groupTitle}</S.Title>
                <S.ArrowContainer>
                    <ArrowRight />
                </S.ArrowContainer>
            </S.Container>
        </Link>
    )
}

CatalogGroupItem.propTypes = {
    children: PropTypes.any.isRequired,
    currentGroupUrl: PropTypes.string.isRequired
}

export default CatalogGroupItem
