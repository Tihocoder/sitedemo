import React, { useContext, useState } from 'react'
import { addBasketItem, changeBasketItemPrice } from '@actions/customer'
import { useDispatch, useSelector } from 'react-redux'
import { AlertsStateContext } from '@context/AlertsState'
import * as Theme from '@styles/theme-const'
import PropTypes from 'prop-types'
import CountChanger from '@shared/CountChanger/CountChanger'
import ModalWindow from '@shared/ModalWindow/ModalWindow'
import * as S from './BasketController.styled'
import Button from '../../../shared/Button/Button'
import * as catalogService from '@services/catalogService'
import MessageSelectShipment from '@mobileComponents/catalog/MessageSelectShipment/MessageSelectShipment'

const BasketController = ({ count, good, webId, shipment }) => {
    const { deleteFromBasket } = useContext(AlertsStateContext)
    const dispatch = useDispatch()
    const features = useSelector((state) => state.catalog.features)
    const basketItems = useSelector((state) => state.customer.basketItems)
    const [isShowedModalDemand, setIsShowedModalDemand] = useState(false)
    const [isShowedModalNotSelectedShipment, setIsShowedModalNotSelectedShipment] = useState(false)
    // const [isShowedSelectorAddress, setIsShowedSelectorAddress] = useState(false)
    // const [isShowedSelectorStock, setIsShowedSelectorStock] = useState(false)
    const toggleDemandModal = () => {
        setIsShowedModalDemand(!isShowedModalDemand)
    }

    const addBasket = async () => {
        /// /RuTarget
        const _rutarget = window._rutarget || []
        _rutarget.push({ event: 'thankYou', conv_id: 'cart' })

        await dispatch(addBasketItem(good))
        setIsShowedModalDemand(false)
    }

    const addBasketBaseHandler = async () => {
        setIsShowedModalNotSelectedShipment(false)
        if (good.isDemand) {
            toggleDemandModal()
            return
        }
        await addBasket()
    }

    const addBasketHandler = async () => {
        if (!shipment.shipAddressId && !shipment.stockId && !basketItems?.length) {
            setIsShowedModalNotSelectedShipment(true)
            //await addBasket()
            return
        }
        await addBasketBaseHandler()
    }
    // eslint-disable-next-line no-shadow
    const changePriceHandler = async (count) => {
        if (count > 0) {
            await dispatch(changeBasketItemPrice(webId, count))
        } else {
            deleteFromBasket.openHandler(webId)
        }
    }
    return (
        <S.Container>
            {isShowedModalNotSelectedShipment && (
                <MessageSelectShipment
                    closeHandler={() =>
                        setIsShowedModalNotSelectedShipment(!isShowedModalNotSelectedShipment)
                    }
                    addBasketHandler={addBasketBaseHandler}
                />
            )}
            {!count ? (
                <Button clickHandler={addBasketHandler} elementSizeTypeValue={Theme.ElementSizeType.regular}>
                    В Корзину
                </Button>
            ) : (
                <CountChanger value={count} changeValueHandler={changePriceHandler} />
            )}
            {isShowedModalDemand && (
                <ModalWindow
                    title="Внимание"
                    isMobile
                    isButtonFooter={false}
                    cancel={{
                        text: 'Отмена',
                        clickHandler: toggleDemandModal
                    }}>
                    <S.BodyModel
                        dangerouslySetInnerHTML={{
                            __html: catalogService.getFeatureHtml(good, features)
                        }}
                    />
                    <S.FooterFuterBlock>
                        <Button elementSizeTypeValue={Theme.ElementSizeType.regular} clickHandler={addBasket}>
                            В корзину
                        </Button>
                        <Button
                            elementSizeTypeValue={Theme.ElementSizeType.regular}
                            clickHandler={toggleDemandModal}>
                            Отмена
                        </Button>
                    </S.FooterFuterBlock>
                </ModalWindow>
            )}
        </S.Container>
    )
}

BasketController.propTypes = {
    count: PropTypes.number,
    webId: PropTypes.number,
    shipment: PropTypes.any,
    good: PropTypes.object.isRequired
}

export default BasketController
