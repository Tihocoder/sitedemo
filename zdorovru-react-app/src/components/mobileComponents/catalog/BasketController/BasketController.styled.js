import styled from 'styled-components'
import { ButtonElement } from '@shared/Button/Button.styled'
import { ButtonLeft, ButtonRight, Input } from '@shared/CountChanger/CountChanger.styled'

export const Container = styled.div`
    max-width: 6.1rem;
    box-sizing: border-box;
    max-height: 2.1rem;
    height: 100%;
    display: flex;

    ${ButtonLeft} {
        width: 40%;
        padding: 0.2rem;
        display: flex;
        align-items: center;
        justify-content: center;
    }

    ${ButtonRight} {
        width: 40%;
        padding: 0.02rem;
        display: flex;
        align-items: center;
        justify-content: center;
    }

    ${Input} {
        width: 50%;
        padding: 0.5rem 0.4rem;
    }

    ${ButtonElement} {
        width: 100%;
        height: 100%;
    }
`
export const BodyModel = styled.div`
    & hr {
        border: none;
        color: ${(props) => props.theme.colors.getGrayColor(0.2)};
        height: 1px;
        background-color: ${(props) => props.theme.colors.getGrayColor(0.2)};
    }
`

export const FooterFuterBlock = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
    height: 5rem;
    justify-content: space-between;
    margin-top: 0.4rem;
    // & > button {
    //     margin-top: 1rem;
    //     height: 3rem;
    // }
    //
    ${ButtonElement} {
        margin-top: 0.4rem;
    }
`
