import styled from 'styled-components'

export const Container = styled.div`
    display: flex;
    border-bottom: 1px solid ${(props) => props.theme.colors.getGreenColor(0.4)};
    padding: 0.2rem;
`
