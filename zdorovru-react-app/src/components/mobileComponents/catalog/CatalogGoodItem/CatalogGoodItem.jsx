import React from 'react'
import PropTypes from 'prop-types'
import * as catalogService from '@services/catalogService'
import * as S from './CatalogGoodItem.styled'
import CatalogGoodInfo from './CatalogGoodInfo/CatalogGoodInfo'
import CatalogGoodImageBlock from './CatalogGoodImageBlock/CatalogGoodImageBlock'

const CatalogGoodItem = ({ children, shipment, groupId, isDelivery }) => {
    const linkGoodCart = catalogService.getGoodUrl(children.webData, groupId)

    return (
        <S.Container>
            <CatalogGoodImageBlock
                id={children.webData.goodId}
                linkGoodCart={linkGoodCart}
                hasImage={children.webData.hasImage}
                isCold={children.isCold}
                isStrictlyByPrescription={children.isStrictlyByPrescription}
            />
            <CatalogGoodInfo isDelivery={isDelivery} linkGoodCart={linkGoodCart} shipment={shipment}>
                {children}
            </CatalogGoodInfo>
        </S.Container>
    )
}

CatalogGoodItem.propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    children: PropTypes.object.isRequired,
    groupId: PropTypes.number
}

export default CatalogGoodItem
