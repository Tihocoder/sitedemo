import React from 'react'
import Link from 'next/link'
import * as S from './CatalogGoodImageBlock.styled'
import * as Theme from '../../../../../styles/theme-const'

const CatalogGoodImageBlock = (props) => {
    const imageUrl = props.hasImage
        ? `https://zdorov.ru/goodsimg/240/${props.id}.jpg`
        : `${process.env.BASE_PATH || ''}/images/noimg.gif`

    return (
        <Link href={props.linkGoodCart} passHref>
            <S.Container>
                <S.ImageContainer>
                    <S.Image
                        src={imageUrl}
                        onError={(e) => {
                            e.target.onerror = null
                            e.target.src = `${process.env.BASE_PATH || ''}/images/noimg.gif`
                        }}
                    />
                </S.ImageContainer>
                <S.ParamtersBlock>
                    {props.isCold && (
                        <S.ParameterItem color={Theme.ColorConsts.colorCold}>
                            Хранение в холоде
                        </S.ParameterItem>
                    )}
                    {props.isStrictlyByPrescription && (
                        <S.ParameterItem color={Theme.ColorConsts.getRedheadColor(1)}>
                            Строго по рецепту
                        </S.ParameterItem>
                    )}
                </S.ParamtersBlock>
            </S.Container>
        </Link>
    )
}

CatalogGoodImageBlock.propTypes = {}

export default CatalogGoodImageBlock
