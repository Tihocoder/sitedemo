import styled from 'styled-components'

export const Container = styled.a`
    display: flex;
    flex-direction: column;
`
export const ImageContainer = styled.div`
    height: 100%;
    box-sizing: border-box;
    display: flex;
    align-items: flex-end;
    cursor: pointer;
`

export const Image = styled.img`
    height: 100%;
    max-height: 7.8125rem;
    max-width: 7.8125rem;
`
export const ParamtersBlock = styled.div`
    display: flex;
    flex-direction: column;
`
export const ParameterItem = styled.span`
    font-size: 0.75rem;
    color: ${(props) => props.color};
`
