import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { useRouter } from 'next/router'
import * as S from './CatalogGoodInfo.styled'
import GoodDescription from './GoodDescription/GoodDescription'
import GoodPriceAndChanger from './GoodPriceAndChanger/GoodPriceAndChanger'
import BasketGoodPriceAndChanger from './BasketGoodPriceAndChanger/BasketGoodPriceAndChanger'
import AnaloguesPage from '@mobilePages/catalog/GoodPageMobile/AnaloguesPage/AnaloguesPage'

const CatalogGoodInfo = ({ children, shipment, linkGoodCart, isDelivery }) => {
    const router = useRouter()
    const [isAnaloguesPage, setIsAnaloguesPage] = useState(false)
    const toggleAnaloguesPage = () => {
        setIsAnaloguesPage(!isAnaloguesPage)
    }
    return (
        <S.Container>
            <GoodDescription
                linkGoodCart={linkGoodCart}
                name={children.webData.drugTitle}
                description={children.webData.outFormTitle}
                maker={children.webData.makerTitle}
            />
            {router.asPath.includes('/order/basket') ? (
                <BasketGoodPriceAndChanger
                    isDelivery={isDelivery}
                    good={children}
                    shipment={shipment}
                    toggleAnaloguesPage={toggleAnaloguesPage}
                />
            ) : (
                <GoodPriceAndChanger
                    good={children}
                    shipment={shipment}
                    toggleAnaloguesPage={toggleAnaloguesPage}
                />
            )}
            {isAnaloguesPage && (
                <AnaloguesPage
                    closeHandler={() => setIsAnaloguesPage(!isAnaloguesPage)}
                    goodId={children.webData.goodId}
                    shipment={shipment}
                />
            )}
        </S.Container>
    )
}

CatalogGoodInfo.propTypes = {
    children: PropTypes.any.isRequired,
    shipment: PropTypes.any.isRequired
}

export default CatalogGoodInfo
