import styled from 'styled-components'

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
    margin-left: 0.2rem;
    justify-content: space-between;
`
