import React from 'react'
import PropTypes from 'prop-types'
import Link from 'next/link'
import * as S from './GoodDescription.styled'

const GoodDescription = ({ name, description, maker, linkGoodCart }) => (
    <Link href={linkGoodCart} passHref>
        <S.Container>
            <S.Name>{name}</S.Name>
            <S.Description>{description}</S.Description>
            <S.Maker>{maker}</S.Maker>
        </S.Container>
    </Link>
)

GoodDescription.propTypes = {
    // eslint-disable-next-line react/require-default-props
    name: PropTypes.string,
    // eslint-disable-next-line react/require-default-props
    description: PropTypes.string,
    // eslint-disable-next-line react/require-default-props
    maker: PropTypes.string,
    linkGoodCart: PropTypes.string.isRequired
}

export default GoodDescription
