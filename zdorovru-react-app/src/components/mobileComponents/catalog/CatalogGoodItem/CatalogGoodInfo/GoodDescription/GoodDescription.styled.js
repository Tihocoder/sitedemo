/* eslint-disable @typescript-eslint/no-unsafe-member-access */
import styled from 'styled-components'

export const Container = styled.a`
    display: flex;
    flex-direction: column;
`

export const Name = styled.h2`
    margin: 0;
    padding: 0;
    line-height: 1.5rem;
    font-size: inherit;
`

export const Description = styled.span`
    color: ${(props) => props.theme.colors.getGrayColor()};
    font-size: 0.875rem;
    line-height: 1.25rem;
`

export const Maker = styled.span`
    font-size: 0.75rem;
    line-height: 1rem;
`
