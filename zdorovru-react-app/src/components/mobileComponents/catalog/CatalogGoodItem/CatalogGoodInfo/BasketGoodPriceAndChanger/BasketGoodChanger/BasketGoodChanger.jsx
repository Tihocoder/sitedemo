import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import { AlertsStateContext } from '../../../../../../../context/AlertsState'
import { useDispatch } from 'react-redux'
import { changeBasketItemPrice } from '../../../../../../../actions/customer'
import CountChanger from '../../../../../../shared/CountChanger/CountChanger'
import * as S from './BasketGoodChanger.styled'

const BasketGoodChanger = ({ count, webId, shipment }) => {
    const { deleteFromBasket } = useContext(AlertsStateContext)
    const dispatch = useDispatch()
    const changePriceHandler = async (count) => {
        if (count > 0) {
            await dispatch(changeBasketItemPrice(webId, count))
        } else {
            deleteFromBasket.openHandler(webId)
        }
    }
    return (
        <S.Container>
            <CountChanger value={count} changeValueHandler={changePriceHandler} />
        </S.Container>
    )
}

BasketGoodChanger.propTypes = {
    count: PropTypes.number,
    webId: PropTypes.number,
    shipment: PropTypes.any
}

export default BasketGoodChanger
