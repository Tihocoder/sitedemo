import React, { useContext } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import * as Theme from '@styles/theme-const'
import { ElementSizeType } from '@styles/theme-const'
import Button from '@shared/Button/Button'
import FavoriteIndicate from '@mobileComponents/catalog/FavoriteIndicate/FavoriteIndicate'
import { toggleLikedGood } from '@actions/customer'
import { AlertsStateContext } from '@context/AlertsState'
import * as S from './BasketGoodPriceAndChanger.styled'
import BasketGoodPriceDescription from './BasketGoodPriceDescription/BasketGoodPriceDescription'
import BasketGoodChanger from './BasketGoodChanger/BasketGoodChanger'

const BasketGoodPriceAndChanger = ({ good, shipment, isDelivery }) => {
    const likedGoods = useSelector((state) => state.customer.likedGoods)
    const dispatch = useDispatch()
    const { deleteFromBasket } = useContext(AlertsStateContext)
    const basketItem = useSelector(
        (state) => state.customer.basketItems.find((x) => x.webData.webId === good.webData.webId) || null
    )
    const toggleLikeHandler = async (goodId) => {
        await dispatch(toggleLikedGood(goodId))
    }
    const colorsButtonText = {
        color: Theme.ColorConsts.white,
        backgroundColor: Theme.ColorConsts.getRedheadColor(0.5)
    }

    const isNotShowDeletes =
        (!isDelivery && good.isAvailableOnStock && good.stockPrice.normal) ||
        (isDelivery && good.deliveryStatus.isAvailableForDelivery && good.deliveryPrice.normal)

    const deleteItemHandler = async () => deleteFromBasket.openHandler(good.webData.webId)
    return (
        <S.Container>
            <BasketGoodPriceDescription good={good} isSelectedStock={!!shipment.stockId} />
            <S.BottomBlock>
                <FavoriteIndicate
                    isSelected={likedGoods.includes(good.webData.goodId) || false}
                    clickHandler={async () => await toggleLikeHandler(good.webData.goodId)}
                />
                {isNotShowDeletes && good.isAvailable ? (
                    <BasketGoodChanger
                        count={basketItem?.count}
                        webId={good.webData.webId}
                        shipment={shipment}
                    />
                ) : (
                    <Button
                        colors={colorsButtonText}
                        elementSizeTypeValue={ElementSizeType.regular}
                        clickHandler={deleteItemHandler}>
                        Удалить
                    </Button>
                )}
            </S.BottomBlock>
        </S.Container>
    )
}

BasketGoodPriceAndChanger.propTypes = {}

export default BasketGoodPriceAndChanger
