import styled from 'styled-components'
import { ButtonElement } from '@shared/Button/Button.styled'
import { ButtonLeft, ButtonRight, Input } from '@shared/CountChanger/CountChanger.styled'

// eslint-disable-next-line import/prefer-default-export
export const Container = styled.div`
    max-width: 8.1rem;
    box-sizing: border-box;
    height: 2.1rem;

    ${ButtonLeft} {
        width: 35%;
    }

    ${ButtonRight} {
        width: 35%;
    }

    ${Input} {
        width: 60%;
    }

    ${ButtonElement} {
        width: 100%;
        height: 100%;
        padding: 0.2rem 0.8rem;
    }
`
