import React, { useContext, useState } from 'react'
import { declOfNum, generateDate } from '@helpers/site'
import { useSelector } from 'react-redux'
import ModalWindow from '@shared/ModalWindow/ModalWindow'
import { MenuContext } from '@context/CatalogMenuState'
import SymbolRuble from '../../../../../../shared/SymbolRuble/SymbolRuble'
import * as S from './BasketGoodPriceDescription.styled'

const getPriceItem = (
    isAvailable,
    isDeliveryPrice,
    price,
    availableStocksCount,
    isSelectedStock,
    good,
    clickHandlerDeliveryReasons,
    clickOpenAnotherStockHandler,
    isSelectedAddress
) => {
    const isChangedCount = good.bookedCount < good.count && good.isChanged

    const getAnotherStockDescription = () => {
        if (availableStocksCount) {
            return (
                <S.AnotherStockCountDescription onClick={clickOpenAnotherStockHandler}>
                    Нет в выбранной, самовывоз из {availableStocksCount}{' '}
                    {declOfNum(availableStocksCount, ['аптеки', 'аптек', 'аптек'])}
                </S.AnotherStockCountDescription>
            )
        }
        return <S.AnotherStockCountDescription>Нет в выбранной</S.AnotherStockCountDescription>
    }

    if (isDeliveryPrice && !isAvailable) {
        return (
            <S.PriceItemContainer>
                <S.PriceItem>
                    <S.ShipmentTitle onClick={clickHandlerDeliveryReasons}>
                        Доставка <S.DeliveryStatusTitle>невозможна</S.DeliveryStatusTitle>
                    </S.ShipmentTitle>
                </S.PriceItem>
            </S.PriceItemContainer>
        )
    }

    return (
        <S.PriceItemContainer>
            <S.PriceItem>
                {!isAvailable && isSelectedStock ? (
                    getAnotherStockDescription()
                ) : (
                    <S.ShipmentTitle>
                        {isDeliveryPrice ? 'Доставка' : 'Самовывоз'}{' '}
                        {(!price.isDemand && isSelectedAddress && isDeliveryPrice) ||
                        (!price.isDemand && !isDeliveryPrice)
                            ? 'сегодня'
                            : ''}
                        {price.isDemand && <S.DateRed>{generateDate(price.aptEtaDateTime)}</S.DateRed>}
                    </S.ShipmentTitle>
                )}
                {price.withoutPromo ? (
                    <S.PriceBlock>
                        <S.PriceDataOld>
                            {price.withoutPromo.toLocaleString('RU-ru')} <SymbolRuble />
                        </S.PriceDataOld>
                        <S.PriceNumberSaleBold>
                            {price.normal.toLocaleString('RU-ru')} <SymbolRuble />
                        </S.PriceNumberSaleBold>
                    </S.PriceBlock>
                ) : (
                    <S.PriceBlock>
                        {!isAvailable && !isDeliveryPrice ? (
                            <S.PriceSumFrom onClick={clickOpenAnotherStockHandler}>
                                от {price.normal.toLocaleString('RU-ru')} <SymbolRuble />
                            </S.PriceSumFrom>
                        ) : (
                            <S.PriceSum>
                                {price.normal.toLocaleString('RU-ru')} <SymbolRuble />
                            </S.PriceSum>
                        )}
                    </S.PriceBlock>
                )}
            </S.PriceItem>
            {isChangedCount && (
                <S.PreorderWarningContainer>
                    <S.WarningCountSpan>В наличии {good.bookedCount} уп.</S.WarningCountSpan>
                </S.PreorderWarningContainer>
            )}
        </S.PriceItemContainer>
    )
}

const BasketGoodPriceDescription = (props) => {
    const { toggleStockSelectorByGood } = useContext(MenuContext)
    const shipment = useSelector((state) => state.customer.shipment)
    const clickOpenAnotherStockHandler = () => {
        toggleStockSelectorByGood([props.good.webData.goodId])
    }
    const isDelivery = useSelector((state) => state.customer.basket.isDelivery)
    const isDeliveryAvl = props.good.deliveryStatus?.isAvailableForDelivery || false
    const priceFroStock = props.good.isAvailableOnStock ? props.good.stockPrice : props.good.minPrice

    const [deliveryReasonState, setDeliveryReasonState] = useState({
        isShowedModal: false,
        reasons: []
    })
    const openDeliveryReasons = (reasons) => {
        setDeliveryReasonState({
            ...deliveryReasonState,
            isShowedModal: true,
            reasons
        })
    }
    const closeDeliveryReasons = () => {
        setDeliveryReasonState({
            ...deliveryReasonState,
            isShowedModal: false,
            reasons: []
        })
    }

    const clickHandlerDeliveryReasons = () => {
        openDeliveryReasons(
            props.good.deliveryStatus.reasons.map((reason) => ({
                id: reason.deliveryReasonID,
                text: reason.warningText
            }))
        )
    }

    return (
        <S.Container>
            {deliveryReasonState.isShowedModal && (
                <ModalWindow
                    title="Внимание"
                    isMobile
                    isButtonFooter={false}
                    isFooterOnlyOneButton
                    submit={{
                        text: 'Понятно',
                        clickHandler: closeDeliveryReasons
                    }}
                    cancel={{
                        text: 'Ок',
                        clickHandler: closeDeliveryReasons
                    }}>
                    <S.ReasonContainer>
                        {deliveryReasonState.reasons.map((reason) => (
                            <S.ReasonTextItem
                                key={reason.id}
                                dangerouslySetInnerHTML={{ __html: reason.text }}
                            />
                        ))}
                    </S.ReasonContainer>
                </ModalWindow>
            )}
            {!isDelivery
                ? getPriceItem(
                      props.good.isAvailableOnStock,
                      false,
                      priceFroStock,
                      props.good.availableStocksCount,
                      props.isSelectedStock,
                      props.good,
                      () => {},
                      clickOpenAnotherStockHandler
                  )
                : getPriceItem(
                      isDeliveryAvl,
                      true,
                      props.good.deliveryPrice,
                      props.good.availableStocksCount,
                      props.isSelectedStock,
                      props.good,
                      clickHandlerDeliveryReasons,
                      clickOpenAnotherStockHandler,
                      !!shipment.shipAddressId
                  )}
        </S.Container>
    )
}

BasketGoodPriceDescription.propTypes = {}

export default BasketGoodPriceDescription
