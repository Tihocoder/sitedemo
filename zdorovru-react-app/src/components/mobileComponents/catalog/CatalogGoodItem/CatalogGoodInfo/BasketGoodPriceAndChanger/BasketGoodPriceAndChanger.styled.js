import styled from 'styled-components'

export const Container = styled.div`
    display: flex;
    flex-direction: column;
`
export const BottomBlock = styled.div`
    display: flex;
    flex-wrap: wrap;
    //margin: 0 -0.5rem;
    & > div {
        flex: 1 1 auto;
    }
`