/* eslint-disable @typescript-eslint/no-unsafe-member-access */
import styled from 'styled-components'

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    font-size: 0.875rem;
    padding: 0.25rem 0;
`
export const PriceItemContainer = styled.div`
    display: flex;
    flex-direction: column;
`
export const PriceItem = styled.div`
    display: flex;
    justify-content: space-between;
`

export const AnotherStockCountDescription = styled.span`
    color: ${(props) => props.theme.colors.getRedheadColor()};
    padding-right: 1rem;
`
export const DeliveryStatusTitle = styled.span`
    color: ${(props) => props.theme.colors.getBlue60Color()};
`
export const ShipmentTitle = styled.span`
    color: ${(props) => props.theme.colors.getGrayColor(0.45)};
`

export const DateRed = styled.span`
    color: ${(props) => props.theme.colors.getRedheadColor(1)};
`

export const PriceSum = styled.span`
    color: ${(props) => props.theme.colors.getGrayColor(1)};
    font-size: 1rem;
    white-space: nowrap;
    font-weight: bold;
`
export const PriceSumFrom = styled.span`
    color: ${(props) => props.theme.colors.getBlue60Color(1)};
    font-size: 1rem;
    white-space: nowrap;
    font-weight: bold;
`

export const PriceDataOld = styled.div`
    font-size: 0.85rem;
    display: flex;
    flex-direction: row;
    flex-wrap: nowrap;
    text-decoration: line-through;
    align-items: center;
    margin-right: 0.3rem;

    & span:first-child {
        margin-left: 0.2rem;
    }
`
export const PriceBlock = styled.div`
    display: flex;
    flex-direction: column;
`
export const PriceNumberSaleBold = styled.span`
    font-weight: bold;
    color: ${(props) => props.theme.colors.pureRed};
    white-space: nowrap;
    font-size: 1rem;
`
export const ReasonContainer = styled.div`
    display: flex;
    flex-direction: column;
    max-width: 32rem;
`
export const ReasonTextItem = styled.div``
