/* eslint-disable react/prop-types */
import React from 'react'
import * as S from './GoodPriceForOrder.styled'
import SymbolRuble from '@shared/SymbolRuble/SymbolRuble'

const GoodPriceForOrder = (props) => {
    return (
        <S.Container>
            <S.PriceItemContainer>
                <S.PriceItem>
                    <S.PriceTitle>Стоимость</S.PriceTitle>
                    <S.PriceBlock>
                        <S.PriceSum>
                            {(props.fixPrice.normal * props.count).toLocaleString('RU-ru')} <SymbolRuble />
                        </S.PriceSum>
                    </S.PriceBlock>
                </S.PriceItem>
                <S.PriceCountInfoBlock>
                    <S.PriceCountInfo>
                        {props.count} шт. по {props.fixPrice.normal.toLocaleString('ru-RU')} <SymbolRuble />
                    </S.PriceCountInfo>
                </S.PriceCountInfoBlock>
            </S.PriceItemContainer>
        </S.Container>
    )
}

GoodPriceForOrder.propTypes = {}

export default GoodPriceForOrder
