import React, { useContext, useState } from 'react'
import { declOfNum, generateDate } from '@helpers/site'
import ModalWindow from '@shared/ModalWindow/ModalWindow'
import PropTypes from 'prop-types'
import { DeliveryStatusType, PriceType } from 'src/consts'
import SymbolRuble from '@shared/SymbolRuble/SymbolRuble'
import { MenuContext } from '@context/CatalogMenuState'
import { useSelector } from 'react-redux'
import * as S from './GoodPriceDescription.styled'

const getPriceItem = (
    isAvailable,
    isDeliveryPrice,
    price,
    availableStocksCount,
    isSelectedStock,
    clickHandlerDeliveryReasons,
    clickOpenAnotherStockHandler,
    isSelectedAddress
) => {
    const getAnotherStockDescription = () => {
        if (availableStocksCount) {
            return (
                <S.AnotherStockCountDescription onClick={clickOpenAnotherStockHandler}>
                    Нет в выбранной, самовывоз из {availableStocksCount}{' '}
                    {declOfNum(availableStocksCount, ['аптеки', 'аптек', 'аптек'])}
                </S.AnotherStockCountDescription>
            )
        }
        return <S.AnotherStockCountDescription>Нет в выбранной</S.AnotherStockCountDescription>
    }

    if (isDeliveryPrice && !isAvailable) {
        return (
            <S.PriceItemContainer>
                <S.PriceItem>
                    <S.ShipmentTitle onClick={clickHandlerDeliveryReasons}>
                        Доставка <S.DeliveryStatusTitle>невозможна</S.DeliveryStatusTitle>
                    </S.ShipmentTitle>
                </S.PriceItem>
            </S.PriceItemContainer>
        )
    }

    if (!isDeliveryPrice && !availableStocksCount && !price.normal) {
        return (
            <S.PriceItemContainer>
                <S.PriceItem>
                    <S.ShipmentTitle>
                        Самовывоз{' '}
                        <S.AnotherStockCountDescription>Нет в наличии</S.AnotherStockCountDescription>
                    </S.ShipmentTitle>
                </S.PriceItem>
            </S.PriceItemContainer>
        )
    }

    return (
        <S.PriceItemContainer>
            <S.PriceItem>
                {!isAvailable && isSelectedStock ? (
                    getAnotherStockDescription()
                ) : (
                    <S.ShipmentTitle>
                        {isDeliveryPrice ? 'Доставка' : 'Самовывоз'}{' '}
                        {(!price.isDemand && isSelectedAddress && isDeliveryPrice) ||
                        (!price.isDemand && !isDeliveryPrice)
                            ? 'сегодня'
                            : ''}
                        {price.isDemand && <S.DateRed>{generateDate(price.aptEtaDateTime)}</S.DateRed>}
                    </S.ShipmentTitle>
                )}
                {price.withoutPromo && isAvailable ? (
                    <S.PriceBlock>
                        <S.PriceDataOld>
                            {price.withoutPromo.toLocaleString('RU-ru')} <SymbolRuble />
                        </S.PriceDataOld>
                        <S.PriceNumberSaleBold>
                            {price.normal.toLocaleString('RU-ru')} <SymbolRuble />
                        </S.PriceNumberSaleBold>
                    </S.PriceBlock>
                ) : (
                    <S.PriceBlock>
                        {!isAvailable && !isDeliveryPrice ? (
                            <S.PriceSumFrom onClick={clickOpenAnotherStockHandler}>
                                от {price.normal?.toLocaleString('RU-ru')} <SymbolRuble />
                            </S.PriceSumFrom>
                        ) : (
                            <S.PriceSum>
                                {price.normal?.toLocaleString('RU-ru')} <SymbolRuble />
                            </S.PriceSum>
                        )}
                    </S.PriceBlock>
                )}
            </S.PriceItem>
        </S.PriceItemContainer>
    )
}

const GoodPriceDescription = (props) => {
    const { toggleStockSelectorByGood } = useContext(MenuContext)
    const shipment = useSelector((state) => state.customer.shipment)
    const clickOpenAnotherStockHandler = () => {
        toggleStockSelectorByGood([props.goodId])
    }
    const isDeliveryAvl = props.deliveryStatus?.isAvailableForDelivery || false
    const priceFroStock = props.isAvailableOnStock ? props.stockPrice : props.minPrice

    const [deliveryReasonState, setDeliveryReasonState] = useState({
        isShowedModal: false,
        reasons: []
    })
    const openDeliveryReasons = (reasons) => {
        setDeliveryReasonState({
            ...deliveryReasonState,
            isShowedModal: true,
            reasons
        })
    }
    const closeDeliveryReasons = () => {
        setDeliveryReasonState({
            ...deliveryReasonState,
            isShowedModal: false,
            reasons: []
        })
    }

    const clickHandlerDeliveryReasons = () => {
        openDeliveryReasons(
            props.deliveryStatus.reasons.map((reason) => ({
                id: reason.deliveryReasonID,
                text: reason.warningText
            }))
        )
    }
    return (
        <S.Container>
            {deliveryReasonState.isShowedModal && (
                <ModalWindow
                    title="Внимание"
                    isMobile
                    isButtonFooter={false}
                    isFooterOnlyOneButton
                    submit={{
                        text: 'Понятно',
                        clickHandler: closeDeliveryReasons
                    }}
                    cancel={{
                        text: 'Понятно',
                        clickHandler: closeDeliveryReasons
                    }}>
                    <S.ReasonContainer>
                        {deliveryReasonState.reasons.map((reason) => (
                            <S.ReasonTextItem
                                key={reason.id}
                                dangerouslySetInnerHTML={{ __html: reason.text }}
                            />
                        ))}
                    </S.ReasonContainer>
                </ModalWindow>
            )}

            {getPriceItem(
                props.isAvailableOnStock,
                false,
                priceFroStock,
                props.availableStocksCount,
                props.isSelectedStock,
                () => {},
                clickOpenAnotherStockHandler
            )}
            {getPriceItem(
                isDeliveryAvl,
                true,
                props.deliveryPrice,
                props.availableStocksCount,
                props.isSelectedStock,
                clickHandlerDeliveryReasons,
                clickOpenAnotherStockHandler,
                !!shipment.shipAddressId
            )}
        </S.Container>
    )
}

GoodPriceDescription.propTypes = {
    isSelectedStock: PropTypes.bool.isRequired,
    availableStocksCount: PropTypes.number,
    isAvailableOnStock: PropTypes.bool.isRequired,
    deliveryStatus: PropTypes.shape(DeliveryStatusType).isRequired,
    deliveryPrice: PropTypes.shape(PriceType).isRequired,
    stockPrice: PropTypes.shape(PriceType).isRequired,
    minPrice: PropTypes.shape(PriceType).isRequired,
    goodId: PropTypes.number.isRequired
}
GoodPriceDescription.defaultProps = {
    availableStocksCount: 0
}

export default GoodPriceDescription
