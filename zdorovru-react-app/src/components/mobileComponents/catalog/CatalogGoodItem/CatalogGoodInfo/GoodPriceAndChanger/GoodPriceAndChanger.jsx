import React from 'react'
import PropTypes from 'prop-types'
import { useDispatch, useSelector } from 'react-redux'
import { toggleLikedGood } from '@actions/customer'
import { useRouter } from 'next/router'
import GoodPriceForOrder from '@mobileComponents/catalog/CatalogGoodItem/CatalogGoodInfo/GoodPriceAndChanger/GoodPriceForOrder/GoodPriceForOrder'
import Button from '@shared/Button/Button'
import * as Theme from '@styles/theme-const'
import { successButtonColorBlcackLight } from '@styles/theme-const'
import * as catalogService from '@services/catalogService'
import * as S from './GoodPriceAndChanger.styled'
import GoodPriceDescription from './GoodPriceDescription/GoodPriceDescription'
import BasketController from '../../../BasketController/BasketController'
import FavoriteIndicate from '../../../FavoriteIndicate/FavoriteIndicate'

const GoodPriceAndChanger = ({ shipment, good, groupId, toggleAnaloguesPage }) => {
    const likedGoods = useSelector((state) => state.customer.likedGoods)
    const router = useRouter()
    const dispatch = useDispatch()
    const basketItem = useSelector(
        (state) => state.customer.basketItems.find((x) => x.webData.webId === good.webData.webId) || null
    )
    const linkGoodCart = catalogService.getGoodUrl(good.webData, groupId)
    const toggleLikeHandler = async (goodId) => {
        await dispatch(toggleLikedGood(goodId))
    }
    return (
        <S.Container>
            {router.asPath.includes('/account/order') ||
            router.asPath.includes('/order/pickup/success') ||
            router.asPath.includes('/order/delivery/success') ? (
                <GoodPriceForOrder count={good.count} fixPrice={good.fixPrice} />
            ) : (
                <GoodPriceDescription
                    availableStocksCount={good.availableStocksCount}
                    isSelectedStock={!!shipment.stockId}
                    deliveryStatus={good.deliveryStatus}
                    goodId={good.webData.goodId}
                    isAvailable={good.isAvailable}
                    isAvailableOnStock={good.isAvailableOnStock}
                    minPrice={good.minPrice}
                    stockPrice={good.stockPrice}
                    deliveryPrice={good.deliveryPrice}
                />
            )}
            <S.BottomBlock>
                <FavoriteIndicate
                    isSelected={likedGoods.includes(good.webData.goodId) || false}
                    clickHandler={async () => {
                        await toggleLikeHandler(good.webData.goodId)
                    }}
                />
                {good.analoguesQty ? (
                    <S.AnaloguesButtonContainer>
                        <Button
                            elementSizeTypeValue={Theme.ElementSizeType.small}
                            colors={successButtonColorBlcackLight}
                            clickHandler={toggleAnaloguesPage}>
                            Аналоги <S.AnaloguesSpanCount>({good.analoguesQty})</S.AnaloguesSpanCount>
                        </Button>
                    </S.AnaloguesButtonContainer>
                ) : (
                    <React.Fragment />
                )}
                <BasketController
                    count={basketItem?.count}
                    good={good}
                    webId={good.webData.webId}
                    shipment={shipment}
                />
            </S.BottomBlock>
        </S.Container>
    )
}

GoodPriceAndChanger.propTypes = {
    shipment: PropTypes.any,
    groupId: PropTypes.number.isRequired,
    good: PropTypes.any,
    toggleAnaloguesPage: PropTypes.func
}
GoodPriceAndChanger.defaultProps = {
    groupId: 0,
    toggleAnaloguesPage: () => {}
}
export default GoodPriceAndChanger
