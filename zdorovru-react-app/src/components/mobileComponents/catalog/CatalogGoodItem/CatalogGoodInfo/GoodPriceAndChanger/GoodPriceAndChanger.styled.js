import styled from 'styled-components'
import { ButtonElement } from '@shared/Button/Button.styled'

export const Container = styled.div`
    display: flex;
    flex-direction: column;
`

export const BottomBlock = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    height: 2.1rem;

    ${ButtonElement} {
        width: 100%;
        height: 100%;
        padding: 0.2rem 0.45rem;
        font-size: 1rem;
    }

    @media (max-width: 340px) {
        height: 1.9rem;
        ${ButtonElement} {
            padding: 0.2rem 0.4rem;
            font-size: 0.9rem;
        }
    }
`
export const RightItems = styled.div`
    display: flex;
`
export const AnaloguesSpanCount = styled.span``
export const AnaloguesButtonContainer = styled.div`
    max-width: 9.1rem;
    height: 100%;
    box-sizing: border-box;
    display: flex;
    margin-right: 0.2rem;

    @media (max-width: 370px) {
        ${AnaloguesSpanCount} {
            display: none;
        }
    }
`
