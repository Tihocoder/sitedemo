import styled from 'styled-components'

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    padding: 0.25rem 0;
`
export const PriceItemContainer = styled.div`
    display: flex;
    flex-direction: column;
`
export const PriceItem = styled.div`
    display: flex;
    justify-content: space-between;
`

export const PriceSum = styled.span`
    color: ${(props) => props.theme.colors.getGrayColor(1)};
    white-space: nowrap;
    font-weight: bold;
`

export const PriceDataOld = styled.div`
    font-size: 0.85rem;
    display: flex;
    flex-direction: row;
    flex-wrap: nowrap;
    text-decoration: line-through;
    align-items: center;
    margin-right: 0.3rem;

    & span:first-child {
        margin-left: 0.2rem;
    }
`
export const PriceBlock = styled.div`
    display: flex;
    flex-direction: column;
`
export const PriceTitle = styled.span`
    color: ${(props) => props.theme.colors.getGrayColor(0.55)};
`

export const PriceCountInfoBlock = styled.div`
    width: 100%;
    display: flex;
    justify-content: flex-end;
`
export const PriceCountInfo = styled.span`
    color: ${(props) => props.theme.colors.getGrayColor()};
`
