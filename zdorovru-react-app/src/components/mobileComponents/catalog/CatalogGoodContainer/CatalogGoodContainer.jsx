import React from 'react'
import PropTypes from 'prop-types'
import * as S from './CatalogGoodContainer.styled'
import CatalogGoodItem from '../CatalogGoodItem/CatalogGoodItem'

const CatalogGoodContainer = (props) => (
    <S.Container>
        {props.children.map((x) => (
            <CatalogGoodItem
                isDelivery={props.isDelivery}
                shipment={props.shipment}
                key={x.webData.goodId}
                groupId={props.groupId}>
                {x}
            </CatalogGoodItem>
        ))}
    </S.Container>
)

CatalogGoodContainer.propTypes = {
    // eslint-disable-next-line react/require-default-props
    isDelivery: PropTypes.bool,
    // eslint-disable-next-line react/forbid-prop-types
    shipment: PropTypes.object.isRequired,
    // eslint-disable-next-line react/require-default-props
    groupId: PropTypes.number,
    // eslint-disable-next-line react/forbid-prop-types
    children: PropTypes.array.isRequired
}

export default CatalogGoodContainer
