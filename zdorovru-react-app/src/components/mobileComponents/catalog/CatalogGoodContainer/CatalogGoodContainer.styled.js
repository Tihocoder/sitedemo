import styled from 'styled-components'

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    background-color: white;
`

export const GoodsContainer = styled.div`
    display: flex;
    flex-direction: column;
`
