import React from 'react'
import PropTypes from 'prop-types'
import * as S from './FavoriteIndicate.styed'

const FavoriteIndicate = ({ isSelected, clickHandler }) => {
    return (
        <S.Container onClick={clickHandler}>
            <S.FavoriteIcon
                src={
                    isSelected
                        ? `${process.env.BASE_PATH}/images/mobile_favorite_border_red.svg`
                        : `${process.env.BASE_PATH}/images/mobile_favorite_border.svg`
                }
            />
        </S.Container>
    )
}

FavoriteIndicate.propTypes = {
    isSelected: PropTypes.bool.isRequired,
    clickHandler: PropTypes.func.isRequired
}

export default FavoriteIndicate
