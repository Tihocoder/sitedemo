import styled from 'styled-components'

export const Container = styled.div`
    display: flex;
    height: 1.529rem;
`

export const FavoriteIcon = styled.img`
    height: 100%;
    width: auto;
`
