import React from 'react'
import * as S from '@mobilePages/order/CreateOrderMobile/CreateOrderMobile.styled'
import PropTypes from 'prop-types'

const BlockWithTitle = ({ title, children }) => (
    <React.Fragment>
        <S.Wrapper>
            <S.DescriptionLabelBlock>
                <S.DescriptionBlockTitle>{title}</S.DescriptionBlockTitle>
            </S.DescriptionLabelBlock>
        </S.Wrapper>
        <S.ContentBlockWhite>{children}</S.ContentBlockWhite>
    </React.Fragment>
)

BlockWithTitle.propTypes = {
    // eslint-disable-next-line react/require-default-props
    title: PropTypes.string,
    // eslint-disable-next-line react/forbid-prop-types
    children: PropTypes.any.isRequired
}

export default BlockWithTitle
