import styled from 'styled-components'
import { CountBlock } from '@shared/NotificationCount/NotificationCount.styled'

export const Container = styled.footer`
    position: fixed;
    width: 100%;
    bottom: 0px;
    border-top: 1px solid ${(props) => props.theme.colors.getGrayColor(0.2)};
    z-index: 60;
`
export const Body = styled.div`
    display: flex;
    width: 100%;
    justify-content: space-between;
    background-color: ${(props) => props.theme.colors.white};
    min-height: 1rem;
    padding: 0.645rem 0.5rem;
    box-sizing: border-box;
`
export const BlockMenuItem = styled.a`
    display: flex;
    flex-direction: column;
    position: relative;

    ${CountBlock} {
        right: -0.5rem;
        top: -0.5rem;
        padding: 0.1rem 0.35rem;
        font-size: 0.75rem;
        display: flex;
        align-items: center;
    }
`
export const LogoBlock = styled.div`
    background-repeat: no-repeat;
    background-image: ${(props) => `url('${process.env.BASE_PATH}${props.imageUrl}')`};
    background-position: center;
    height: 2rem;
`

export const Name = styled.span`
    margin-top: 0.4rem;
    color: ${(props) => props.theme.colors.grayText};
    font-size: 0.85rem;
`
export const NameSelected = styled.span`
    margin-top: 0.4rem;
    color: ${(props) => props.theme.colors.getGrayColor(1)};
    font-size: 0.85rem;
`
