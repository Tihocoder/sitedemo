import React, { useContext } from 'react'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { useSelector } from 'react-redux'
import * as _ from 'lodash'
import NotificationCount from '@shared/NotificationCount/NotificationCount'
import MobileStockSelector from '@mobilePages/MobileStockSelector/MobileStockSelector'
import { MenuContext } from '@context/CatalogMenuState'
import * as S from './FooterMenu.styled'

const MenuList = [
    {
        id: 'home',
        imageUrl: '/images/home.svg',
        imageSelectedUrl: '/images/selected-home.svg',
        path: '/',
        name: 'Главная'
    },
    {
        id: 'search',
        imageUrl: '/images/CatalogNotSelected.svg',
        imageSelectedUrl: '/images/selected-catalog.svg',
        path: '/catalog',
        name: 'Каталог'
    },
    {
        id: 'shopping',
        imageUrl: '/images/shopping.svg',
        imageSelectedUrl: '/images/selected-shopping_cart.svg',
        path: '/order/basket',
        name: 'Корзина'
    },
    {
        id: 'liked',
        imageUrl: '/images/liked.svg',
        imageSelectedUrl: '/images/selected-favorite_border.svg',
        path: '/catalog/liked',
        name: 'Избранное'
    },
    {
        id: 'person',
        imageUrl: '/images/perm_identity.svg',
        imageSelectedUrl: '/images/selected-perm_identity.svg',
        path: '/account',
        name: 'Кабинет'
    }
]

const getIsSelected = (id, asPath, path) => {
    if (id === 'home' && asPath !== '/') return false
    if (id === 'search' && asPath.startsWith('/catalog/liked')) return false
    if (id === 'search' && asPath.startsWith('/catalog/search')) return false
    if (asPath.startsWith(path)) return true
    if (id === 'shopping') if (asPath.startsWith('/order')) return true
    if (id === 'person') {
        if (asPath.startsWith('/company')) return true
        if (asPath.startsWith('/contact')) return true
    }
    return false
}

const FooterMenu = () => {
    const { stockSelectorByGood, toggleStockSelectorByGood } = useContext(MenuContext)
    const router = useRouter()
    const basketItems = useSelector((state) => state.customer.basketItems.filter((x) => !x.isHidden))
    const positionsCount = _.sumBy(basketItems, (x) => x.count)
    return (
        <S.Container>
            <S.Body>
                {MenuList.map((x) => (
                    <Link key={x.id} href={x.path} passHref>
                        {getIsSelected(x.id, router.asPath, x.path) ? (
                            <S.BlockMenuItem>
                                {x.id === 'shopping' && positionsCount ? (
                                    <NotificationCount>{positionsCount}</NotificationCount>
                                ) : (
                                    <React.Fragment />
                                )}
                                <S.LogoBlock imageUrl={x.imageSelectedUrl} />
                                <S.NameSelected>{x.name}</S.NameSelected>
                            </S.BlockMenuItem>
                        ) : (
                            <S.BlockMenuItem>
                                {x.id === 'shopping' && positionsCount ? (
                                    <NotificationCount>{positionsCount}</NotificationCount>
                                ) : (
                                    <React.Fragment />
                                )}
                                <S.LogoBlock imageUrl={x.imageUrl} />
                                <S.Name>{x.name}</S.Name>
                            </S.BlockMenuItem>
                        )}
                    </Link>
                ))}
            </S.Body>
            {stockSelectorByGood.isOpened && (
                <MobileStockSelector
                    closeHandler={toggleStockSelectorByGood}
                    changeOptions={{
                        customFilterStock: {
                            isEnabled: true,
                            filteredGoodsId: stockSelectorByGood.selectedGoodIds
                        }
                    }}
                />
            )}
        </S.Container>
    )
}

FooterMenu.propTypes = {}

export default FooterMenu
