import styled from 'styled-components'

export const Container = styled.div`
    position: fixed;
    width: 100%;
    bottom: 5rem;
    border-top: 1px solid ${(props) => props.theme.colors.getGrayColor(0.2)};
    display: flex;
    flex-direction: column;
    background-color: ${(props) => props.theme.colors.white};
`

export const UpBlock = styled.div`
    display: flex;
    height: 2.5rem;
    padding-right: 0.5rem;
`

export const BottomBlock = styled.div`
    display: flex;
    justify-content: space-between;
    padding: 0 0.5rem 0.5rem 0.5rem;
    align-items: center;

    & > button {
        padding: 0.657rem 1rem;
    }
`

export const ShipmentSelectorsBlock = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: space-between;
`

export const PriceBlock = styled.div`
    display: flex;
    flex-direction: column;
    height: 100%;
    align-items: flex-end;
    justify-content: space-between;
    width: 100%;
`
export const PriceDescription = styled.span`
    color: ${(props) => props.theme.colors.getGrayColor(0.6)};
    font-size: 0.875rem;
    white-space: nowrap;
`
export const PriceSum = styled.span`
    color: ${(props) => props.theme.colors.getGrayColor()};
    font-weight: bold;
    font-size: 1.25rem;
`
export const ButtonText = styled.span`
    cursor: pointer;
    color: ${(props) => props.theme.colors.getBlue60Color()};
    white-space: nowrap;
`

export const SelectedShipment = styled.div``
