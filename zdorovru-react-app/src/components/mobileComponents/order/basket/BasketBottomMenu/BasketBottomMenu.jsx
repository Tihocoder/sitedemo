import React from 'react'
import SymbolRuble from '@shared/SymbolRuble/SymbolRuble'
import Button from '@shared/Button/Button'
import { ElementSizeType } from '@styles/theme-const'
import PropTypes from 'prop-types'
import BoolSwitcher from '@mobileShared/BoolSwitcher/BoolSwitcher'
import { useSelector } from 'react-redux'
import * as S from './BasketBottomMenu.styled'

const BasketBottomMenu = ({ preorder, toggleAddressSelector, toggleStockSelector }) => {
    const stocksAll = useSelector((state) => state.company.stocks)
    const shipmentTab = () => {
        if (preorder.isDelivery) {
            return (
                <S.ShipmentSelectorsBlock>
                    <S.SelectedShipment>
                        {preorder.shipment.shipAddressTitle || 'Адрес доставки не выбран'}
                    </S.SelectedShipment>
                    <S.ButtonText onClick={toggleAddressSelector}>Изменить адрес доставки</S.ButtonText>
                </S.ShipmentSelectorsBlock>
            )
        }
        const stock = stocksAll?.find((x) => x.id === preorder.shipment.stockId) || ''
        return (
            <S.ShipmentSelectorsBlock>
                <S.SelectedShipment>{stock?.title || 'Адрес доставки не выбран'}</S.SelectedShipment>
                <S.ButtonText onClick={toggleStockSelector}>Изменить аптеку самовывоза</S.ButtonText>
            </S.ShipmentSelectorsBlock>
        )
    }

    return (
        <S.Container>
            <S.UpBlock>
                <BoolSwitcher
                    isActive={!preorder.isDelivery}
                    clickHandlers={{
                        tab1Handler: () => preorder.toggleIsDelivery(false),
                        tab2Handler: () => preorder.toggleIsDelivery(true)
                    }}
                    titles={{
                        tab1Title: 'Самовывоз',
                        tab2Title: 'Доставка'
                    }}
                />
                <S.PriceBlock>
                    <S.PriceDescription>Общая сумма</S.PriceDescription>
                    {preorder.isDelivery ? (
                        <S.PriceSum>
                            {preorder.sums.sumDeliveryFull.toLocaleString('RU-ru')} <SymbolRuble />
                        </S.PriceSum>
                    ) : (
                        <S.PriceSum>
                            {preorder.sums.sumStock.toLocaleString('RU-ru')} <SymbolRuble />
                        </S.PriceSum>
                    )}
                </S.PriceBlock>
            </S.UpBlock>
            <S.BottomBlock>
                {shipmentTab()}
                <Button
                    clickHandler={preorder.createOrderHandler}
                    elementSizeTypeValue={ElementSizeType.regular}>
                    Оформление
                </Button>
            </S.BottomBlock>
        </S.Container>
    )
}

BasketBottomMenu.propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    preorder: PropTypes.any.isRequired
}

export default BasketBottomMenu
