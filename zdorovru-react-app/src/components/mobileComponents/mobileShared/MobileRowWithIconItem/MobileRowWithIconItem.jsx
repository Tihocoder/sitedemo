import React from 'react'
import MobileRowItem from '@mobileShared/MobileRowItem/MobileRowItem'
import PropTypes from 'prop-types'
import * as S from './MobileRowWithIconItem.styled'

export const IconsMobileRowItem = {
    ClockMobile: 'clock-mobile',
    PaymentMobile: 'payment-mobile',
    UserCreateOrder: 'user-create-order',
    GeoCrateOrder: 'geo-crate-order',
    Delivery: 'local_shipping'
}
const MobileRowWithIconItem = (props) => (
    // eslint-disable-next-line react/jsx-props-no-spreading
    <MobileRowItem {...props}>
        <S.Wrapper>
            <S.RowContainer>
                <S.IconContainer>
                    <S.Icon src={`${process.env.BASE_PATH}/images/${props.iconName}.svg`} />
                </S.IconContainer>
                <S.RowContainerBody isValid={props.isValid}>{props.children}</S.RowContainerBody>
            </S.RowContainer>
        </S.Wrapper>
    </MobileRowItem>
)

MobileRowWithIconItem.propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    children: PropTypes.any.isRequired,
    iconName: PropTypes.string.isRequired,
    isValid: PropTypes.bool,
    // eslint-disable-next-line react/require-default-props
    isShowedRow: PropTypes.bool.isRequired,
    // eslint-disable-next-line react/require-default-props
    isSelected: PropTypes.bool.isRequired,
    // eslint-disable-next-line react/require-default-props
    clickHandler: PropTypes.func.isRequired,
    // eslint-disable-next-line react/require-default-props
    isLink: PropTypes.bool.isRequired,
    // eslint-disable-next-line react/require-default-props
    linkUrl: PropTypes.string
}
MobileRowWithIconItem.defaultProps = {
    // eslint-disable-next-line react/default-props-match-prop-types
    isShowedRow: false,
    // eslint-disable-next-line react/default-props-match-prop-types
    isSelected: false,
    // eslint-disable-next-line react/default-props-match-prop-types
    clickHandler: () => {},
    // eslint-disable-next-line react/default-props-match-prop-types
    isLink: false,
    isValid: true
}

export default MobileRowWithIconItem
