import styled, { css } from 'styled-components'

export const Wrapper = styled.div`
    & > div {
        margin-top: 0.5rem;
    }

    & > div:first-child {
        margin-top: 0;
    }
`
export const RowContainer = styled.div`
    display: flex;
    align-items: center;
`

export const IconContainer = styled.div`
    width: 1.45rem;
    display: flex;
    align-items: center;
    margin-right: 0.4rem;
`

export const RowContainerBody = styled.div`
    display: flex;
    flex-direction: column;
    ${(props) =>
        !props.isValid &&
        css`
            color: ${props.theme.colors.getRedheadColor()};

            &::placeholder {
                color: ${props.theme.colors.getRedheadColor()};
            }
        `}
`
export const Icon = styled.img`
    height: auto;
    width: 100%;
`
