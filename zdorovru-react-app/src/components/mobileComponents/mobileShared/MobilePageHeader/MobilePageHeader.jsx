import React, { useRef } from 'react'
import PropTypes from 'prop-types'
import { useDispatch, useSelector } from 'react-redux'
import { popPath } from '@actions/customer'
import { useRouter } from 'next/router'
import * as S from './MobilePageHeader.styled'
import * as Theme from '../../../../styles/theme-const'
import ArrowLeft from '../../../Icons/ArrowLeft'
import IconSearch from '../../../Icons/IconSearch'
import { IconArrowBack } from "./MobilePageHeader.styled";

const MobilePageHeader = ({
    children,
    isShowArrowBack,
    backClickHandler,
    searchInput,
    backgroundColor,
    right,
    isDefaultRouterBack,
    blockRef
}) => {
    const searchRef = useRef(null)
    const historyPaths = useSelector((state) => state.customer.historyPaths)
    const router = useRouter()
    const dispatch = useDispatch()
    const keyDownHandler = (e) => {
        if (searchInput.keyDownHandler)
            searchInput.keyDownHandler(e, () => {
                if (searchInput.searchRef) searchInput.searchText.current.blur()
                else searchRef.current?.blur()
            })
    }
    const popPathHandler = async () => {
        await dispatch(popPath(router.back))
    }
    const generateArrowCustom = () =>
        isShowArrowBack ? (
            <S.ArrowBlock onClick={backClickHandler}>
                <S.IconArrowBack />
            </S.ArrowBlock>
        ) : (
            <React.Fragment />
        )

    const generateDefaultBlock = () => (
        <S.ArrowBlock onClick={popPathHandler}>
            <S.IconArrowBack />
        </S.ArrowBlock>
    )
    const isBackFromHistory =
        isDefaultRouterBack && historyPaths && historyPaths.length && historyPaths.length > 1
    return (
        <S.Container ref={blockRef} backgroundColor={backgroundColor}>
            <S.HeaderTitle>
                {isBackFromHistory ? generateDefaultBlock() : generateArrowCustom()}
                <S.HeaderText>{children}</S.HeaderText>
                {right}
            </S.HeaderTitle>
            {searchInput.isShowed && (
                <S.SearchBlock>
                    <S.IconContainer>
                        <IconSearch />
                    </S.IconContainer>
                    <S.Input
                        value={searchInput.searchText}
                        onChange={searchInput.changeSearchHandler}
                        placeholder={searchInput.placeholder}
                        onKeyDown={keyDownHandler}
                        onFocus={searchInput.focusHandler}
                        onBlur={searchInput.blurHandler}
                        // onClick={(e) => {
                        //     // eslint-disable-next-line no-param-reassign
                        //     searchInput.searchRef.current.selectionStart = e.target.value.length
                        //     // eslint-disable-next-line no-param-reassign
                        //     searchInput.searchRef.current.selectionEnd = e.target.value.length
                        // }}
                        // onFocus={(e) => {
                        //     searchInput.searchRef.current.selectionStart = e.target.value.length
                        //     searchInput.searchRef.current.selectionEnd = e.target.value.length
                        // }}
                        ref={searchInput.searchRef}
                        type="search"
                    />
                </S.SearchBlock>
            )}
        </S.Container>
    )
}

MobilePageHeader.propTypes = {
    isShowArrowBack: PropTypes.bool.isRequired,
    backClickHandler: PropTypes.func,
    children: PropTypes.any,
    backgroundColor: PropTypes.string.isRequired,
    isDefaultRouterBack: PropTypes.bool,
    searchInput: PropTypes.shape({
        isShowed: PropTypes.bool.isRequired,
        placeholder: PropTypes.string,
        searchText: PropTypes.string,
        changeSearchHandler: PropTypes.func,
        keyDownHandler: PropTypes.func,
        searchRef: PropTypes.any,
        focusHandler: PropTypes.func,
        blurHandler: PropTypes.func
    }).isRequired,
    right: PropTypes.element,
    blockRef: PropTypes.any
}

MobilePageHeader.defaultProps = {
    isShowArrowBack: false,
    backgroundColor: Theme.ColorConsts.backgroundAkmGreen,
    searchInput: {
        isShowed: false,
        keyDownHandler: () => {},
        focusHandler: () => {},
        blurHandler: () => {}
    },
    isDefaultRouterBack: true,
    right: <React.Fragment />
}

export default MobilePageHeader
