import styled from 'styled-components'

export const Container = styled.header`
    background-color: ${(props) => props.backgroundColor};
    color: ${(props) => props.theme.colors.getGrayColor(1)};
    display: flex;
    flex-direction: column;
    padding: 0.4rem;
    position: fixed;
    width: 100%;
    z-index: 12;
    box-sizing: border-box;
`

export const HeaderTitle = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 0.4rem 0.4rem 0.6rem 0.4rem;
`
export const IconArrowBack = styled.img.attrs({
    src: `${process.env.BASE_PATH || ''}/images/arrow_back_ios_24px.svg`
})``
export const ArrowBlock = styled.div`
    height: 100%;
    width: 3rem;
    box-sizing: border-box;
    display: flex;
    align-items: center;
    position: absolute;
    left: 0.313rem;
`
export const HeaderText = styled.span`
    width: 100%;
    text-align: center;
    font-size: 1.1rem;
`
export const SearchBlock = styled.div`
    position: relative;
    height: 2.375rem;
`

export const Input = styled.input`
    width: 100%;
    padding: 0.75rem 2.1rem 0.75rem 2.5rem;
    font-size: 1.25rem;
    height: 100%;
    border: 0;
    color: ${(props) => props.theme.colors.getGrayColor(0.7)};
    box-sizing: border-box;
    outline: none;
    -webkit-appearance: none;
    border-radius: 7px;

    &::-webkit-search-cancel-button {
        -webkit-appearance: none;
        height: 1em;
        width: 1em;
        background: ${`url('${process.env.BASE_PATH}/images/close-mobile-red.svg')`} no-repeat 50% 50%;
        background-size: contain;
        opacity: 0;
        pointer-events: none;
        position: absolute;
        right: 5px;
    }

    &:focus::-webkit-search-cancel-button {
        opacity: 1;
        pointer-events: all;
    }
`
export const IconContainer = styled.div`
    position: absolute;
    width: fit-content;
    height: fit-content;
    top: 0.5rem;
    left: 0.6rem;
    cursor: pointer;
`
