import React from 'react'
import PropTypes from 'prop-types'
import * as S from './BoolSwitcher.styled'

const BoolSwitcher = ({ isActive, titles, clickHandlers, isDefault }) => (
    <S.Container>
        <S.Tab isDefault={isDefault} onClick={clickHandlers.tab1Handler} isActive={isActive}>
            {titles.tab1Title}
        </S.Tab>
        <S.Tab isDefault={isDefault} onClick={clickHandlers.tab2Handler} isActive={!isActive}>
            {titles.tab2Title}
        </S.Tab>
    </S.Container>
)

BoolSwitcher.propTypes = {
    isActive: PropTypes.bool.isRequired,
    titles: PropTypes.shape({
        tab1Title: PropTypes.string.isRequired,
        tab2Title: PropTypes.string.isRequired
    }).isRequired,
    clickHandlers: PropTypes.shape({
        tab1Handler: PropTypes.func.isRequired,
        tab2Handler: PropTypes.func.isRequired
    }).isRequired,
    isDefault: PropTypes.bool
}

BoolSwitcher.defaultProps = {
    isDefault: false
}

export default BoolSwitcher
