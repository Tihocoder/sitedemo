import React from 'react'
import PropTypes from 'prop-types'
import IconSearch from '@components/Icons/IconSearch'
import * as S from './SearchBlock.styled'
import { CancelButtonContainer, InputContainer } from './SearchBlock.styled'

const SearchBlock = ({ searchInput }) => {
    const keyDownHandler = (e) => {
        if (searchInput.keyDownHandler)
            searchInput.keyDownHandler(e, () => {
                if (searchInput.searchRef) searchInput.searchRef.current.blur()
            })
    }

    const clearText = () => {
        searchInput.changeSearchHandler('')
    }

    return (
        <S.Container>
            <S.IconContainer>
                <IconSearch />
            </S.IconContainer>
            <S.InputContainer>
                <S.Input
                    value={searchInput.searchText}
                    onChange={searchInput.changeSearchHandler}
                    placeholder={searchInput.placeholder}
                    onKeyDown={keyDownHandler}
                    onFocus={searchInput.focusHandler}
                    onBlur={searchInput.blurHandler}
                    ref={searchInput.searchRef}
                />

                {searchInput.searchText?.length > 0 ? (
                    <S.CancelButtonContainer onClick={clearText}>
                        <S.CancelButton />
                    </S.CancelButtonContainer>
                ) : (
                    <React.Fragment />
                )}
            </S.InputContainer>
            {searchInput.searchText?.length > 0 && searchInput.searchButtonHandler ? (
                <S.ButtonSearch onClick={searchInput.searchButtonHandler}>Найти</S.ButtonSearch>
            ) : (
                <React.Fragment />
            )}
        </S.Container>
    )
}

SearchBlock.propTypes = {
    isEnabledSearchButton: PropTypes.bool,
    searchInput: PropTypes.shape({
        isShowed: PropTypes.bool.isRequired,
        placeholder: PropTypes.string,
        searchText: PropTypes.string,
        changeSearchHandler: PropTypes.func,
        searchButtonHandler: PropTypes.func,
        keyDownHandler: PropTypes.func,
        searchRef: PropTypes.any,
        focusHandler: PropTypes.func,
        blurHandler: PropTypes.func
    }).isRequired
}
SearchBlock.defaultProps = {
    searchInput: {
        isShowed: false,
        keyDownHandler: () => {},
        focusHandler: () => {},
        searchButtonHandler: () => {},
        blurHandler: () => {}
    }
}
export default SearchBlock
