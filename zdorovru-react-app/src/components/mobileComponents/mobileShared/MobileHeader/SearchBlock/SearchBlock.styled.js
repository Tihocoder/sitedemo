import styled from 'styled-components'

export const Container = styled.div`
    position: relative;
    height: 2.375rem;
    border: 2px solid ${(props) => props.theme.colors.getGreenColor()};
    display: flex;
    align-items: center;
    cursor: pointer;
    box-sizing: border-box;
    outline: none;
    -webkit-appearance: none;
    border-radius: 7px;
    margin: 0.1rem;
`
export const IconContainer = styled.div`
    display: flex;
    align-items: center;
    height: 100%;
    margin-left: 0.2rem;
`
export const InputContainer = styled.div`
    background-color: white;
    width: 100%;
    height: 100%;
    display: flex;
`
export const ButtonSearch = styled.button`
    background-color: ${(props) => props.theme.colors.getGreenColor()};
    color: ${(props) => props.theme.colors.white};
    padding: 0.2rem;
    border: 0;
    height: 100%;
`

export const CancelButtonContainer = styled.div`
    height: 100%;
    padding: 0.2rem;
    width: 2rem;
    display: flex;
    align-items: center;
    justify-content: center;
    box-sizing: border-box;
`
export const CancelButton = styled.button`
    box-shadow: none;
    outline: none;
    margin: 0;
    background-color: inherit;
    border: none;
    cursor: pointer;
    -webkit-appearance: none;
    height: 1.2em;
    width: 1.2em;
    background: ${`url('${process.env.BASE_PATH}/images/close-mobile-gray.svg')`} no-repeat 50% 50%;
    background-size: contain;
    pointer-events: none;
    display: flex;
    align-items: center;
    //position: absolute;
    //right: 3px;
    //top: 0.45rem;
`

export const Input = styled.input`
    width: 100%;
    padding: 0.2rem 0;
    font-size: 1.25rem;
    height: 100%;
    color: ${(props) => props.theme.colors.getGrayColor(0.7)};
    border: none;
    -webkit-appearance: none;
    appearance: none;
    outline: none;
    position: relative;
    box-sizing: border-box;

    &:focus {
        outline: none;
    }

    &::-webkit-search-cancel-button {
        -webkit-appearance: none;
        height: 1em;
        width: 1em;
        background: ${`url('${process.env.BASE_PATH}/images/close-mobile-gray.svg')`} no-repeat 50% 50%;
        background-size: contain;
        opacity: 0;
        pointer-events: none;
        position: absolute;
        right: 3px;
        display: flex;
        align-items: center;
        top: 0.45rem;
    }

    &:focus::-webkit-search-cancel-button {
        opacity: 1;
        pointer-events: all;
    }
`
