import React from 'react'
import PropTypes from 'prop-types'
import { useDispatch, useSelector } from 'react-redux'
import { useRouter } from 'next/router'
import { popPath } from '@actions/customer'
import SearchBlock from '@mobileShared/MobileHeader/SearchBlock/SearchBlock'
import * as S from './MobileHeader.styled'

const MobileHeader = (props) => {
    const historyPaths = useSelector((state) => state.customer.historyPaths)
    const router = useRouter()
    const dispatch = useDispatch()
    const isBackFromHistory =
        props.isDefaultRouterBack && historyPaths && historyPaths.length && historyPaths.length > 1
    const popPathHandler = async () => {
        await dispatch(popPath(router.back))
    }
    const generateDefaultBlock = () => (
        <S.ArrowBlock onClick={popPathHandler}>
            <S.IconArrowBack />
        </S.ArrowBlock>
    )
    const generateCustomBlock = () => {
        if (props.backClickHandler && !props.isDefaultRouterBack)
            return (
                <S.ArrowBlock onClick={props.backClickHandler}>
                    <S.IconArrowBack />
                </S.ArrowBlock>
            )
        return <React.Fragment />
    }
    return (
        <S.Container isBorderAvailable={props.isBorderAvailable} ref={props.blockRef}>
            {props.isShowedTitle && (
                <S.HeaderTitle>
                    {isBackFromHistory ? generateDefaultBlock() : generateCustomBlock()}
                    <S.HeaderText>{props.children}</S.HeaderText>
                    {props.right}
                </S.HeaderTitle>
            )}
            {props.searchInput.isShowed && <SearchBlock searchInput={props.searchInput} />}
        </S.Container>
    )
}

MobileHeader.propTypes = {
    blockRef: PropTypes.any,
    isDefaultRouterBack: PropTypes.bool,
    backClickHandler: PropTypes.func,
    right: PropTypes.element,
    isShowedTitle: PropTypes.string,
    isBorderAvailable: PropTypes.bool,
    searchInput: PropTypes.shape({
        isShowed: PropTypes.bool,
        placeholder: PropTypes.string,
        searchText: PropTypes.string,
        changeSearchHandler: PropTypes.func,
        searchButtonHandler: PropTypes.func,
        keyDownHandler: PropTypes.func,
        // eslint-disable-next-line react/forbid-prop-types
        searchRef: PropTypes.any,
        focusHandler: PropTypes.func,
        blurHandler: PropTypes.func
    })
}
MobileHeader.defaultProps = {
    isDefaultRouterBack: true,
    isBorderAvailable: true,
    searchInput: {
        isShowed: false
    },
    isShowedTitle: true,
    right: <React.Fragment />,
    backClickHandler: null
}

export default MobileHeader
