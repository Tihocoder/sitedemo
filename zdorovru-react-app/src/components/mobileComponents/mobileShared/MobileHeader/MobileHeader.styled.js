import styled, { css } from 'styled-components'

export const Container = styled.header`
    background-color: ${(props) => props.theme.colors.white};
    color: ${(props) => props.theme.colors.getGrayColor(1)};
    display: flex;
    flex-direction: column;
    position: fixed;
    width: 100%;
    z-index: 6;
    box-sizing: border-box;
    ${(props) =>
        props.isBorderAvailable &&
        css`
            border-bottom: 1px solid ${props.theme.colors.getGrayColor(0.12)};
        `}
`
export const IconArrowBack = styled.img.attrs({
    src: `${process.env.BASE_PATH || ''}/images/arrow_back_ios_24px.svg`
})``
export const HeaderTitle = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 0.65rem 0.4rem 0.65rem 0.4rem;
`

export const ArrowBlock = styled.div`
    height: 100%;
    width: 3rem;
    box-sizing: border-box;
    display: flex;
    align-items: center;
    position: absolute;
    left: 0.313rem;
`
export const HeaderText = styled.span`
    width: 100%;
    text-align: center;
    font-size: 1.1rem;
    font-weight: bold;
`
