import styled, { css } from 'styled-components'

export const Container = styled.div`
    display: flex;
    min-width: 15.026rem;
    background-color: ${(props) => props.theme.colors.getGrayColor(0.08)};
    padding: 0.3rem;
`

export const Tab = styled.div`
    padding: 0.5rem 0.5rem;
    width: 50%;
    cursor: pointer;
    display: flex;
    justify-content: center;
    align-items: center;
    border-radius: 7px;
    background-color: initial;

    &:last-child {
        margin-left: 0.2rem;
    }

    ${(props) =>
        props.isActive && !props.isDefault
            ? css`
                  background-color: ${props.theme.colors.getGreenColor()};
                  color: ${props.theme.colors.white};

                  :hover {
                      background-color: ${props.theme.colors.getGreenColor(0.85)};
                  }
              `
            : ''}

    ${(props) =>
        props.isActive && props.isDefault
            ? css`
                  background-color: ${props.theme.colors.white};
                  color: ${props.theme.colors.getGrayColor()};
              `
            : ''}
`
