import React from 'react'
import PropTypes from 'prop-types'
import * as S from '@mobileShared/BoolSwitcher/BoolSwitcher.styled'

const MultipleSwitcher = ({ items, isDefault, selectedHandler, selectedItemName }) => {
    return (
        <S.Container>
            {items.map(x => <S.Tab
                isDefault={isDefault}
                onClick={async () => await selectedHandler(x.value)}
                isActive={x.value === selectedItemName}>
                {x.title}
            </S.Tab>)}
        </S.Container>
    )
}

MultipleSwitcher.propTypes = {
    selectedItemName: PropTypes.string,
    selectedHandler: PropTypes.func.isRequired,
    items: PropTypes.arrayOf(PropTypes.shape({
        name: PropTypes.string.isRequired,
        title: PropTypes.string.isRequired
    })).isRequired,
    isDefault: PropTypes.bool
}
MultipleSwitcher.defaultProps = {
    isDefault: true
}

export default MultipleSwitcher