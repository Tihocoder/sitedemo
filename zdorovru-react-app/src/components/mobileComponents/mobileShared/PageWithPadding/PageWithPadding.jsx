import React from 'react'
import PropTypes from 'prop-types'
import MobilePageHeader from '@mobileShared/MobilePageHeader/MobilePageHeader'
import * as S from './PageWithPadding.styled'
import { MobileBody } from '@styles/ui-element.styled'
import MobileHeader from "@mobileShared/MobileHeader/MobileHeader";

const PageWithPadding = ({ pageHeaderAttributes, children, headerText }) => (
    <S.Container>
        {/* eslint-disable-next-line react/jsx-props-no-spreading */}
        <MobileHeader {...pageHeaderAttributes}>{headerText}</MobileHeader>
        <MobileBody>
            <S.Body>{children}</S.Body>
        </MobileBody>
    </S.Container>
)

PageWithPadding.propTypes = {
    headerText: PropTypes.string.isRequired,
    pageHeaderAttributes: PropTypes.shape({
        backClickHandler: PropTypes.func,
        isShowArrowBack: PropTypes.bool
    }),
    // eslint-disable-next-line react/forbid-prop-types
    children: PropTypes.any.isRequired
}
PageWithPadding.defaultProps = {
    pageHeaderAttributes: {
        backClickHandler: () => {},
        isShowArrowBack: false
    }
}

export default PageWithPadding
