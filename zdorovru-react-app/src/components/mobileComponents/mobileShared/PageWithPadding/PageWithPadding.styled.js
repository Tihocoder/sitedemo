import styled from 'styled-components'

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    color: ${(props) => props.theme.colors.getGrayColor()};
    box-sizing: border-box;
`
export const Body = styled.div`
    padding: 1rem 0.5rem;

    & table {
        width: auto;
    }
`
