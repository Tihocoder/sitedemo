import React from 'react'
import PropTypes from 'prop-types'
import MobilePageHeader from '@mobileShared/MobilePageHeader/MobilePageHeader'
import MobileRowItem from '@mobileShared/MobileRowItem/MobileRowItem'
import * as S from './PageWithSelectorItems.styled'
import { MobileBody } from '@styles/ui-element.styled'
import Button from '@shared/Button/Button'
import * as Theme from '@styles/theme-const'
import { successButtonLight } from '@styles/theme-const'
import MobileHeader from "@mobileShared/MobileHeader/MobileHeader";

const PageWithSelectorItems = (props) => {
    const selectorHandler = (id) => {
        if (props.isCloseWithSelect) {
            props.selectedHandler(id)
            props.closeHandler()
        } else {
            props.selectedHandler(id)
        }
    }
    const submitAndCloseHandler = async () => {
        await props.submitHandler()
        props.closeHandler()
    }
    return (
        <S.Container>
            <MobileHeader
                isShowArrowBack={props.isShowArrowBack}
                backClickHandler={props.closeHandler}
                isDefaultRouterBack={false}>
                {props.headerTitle}
            </MobileHeader>
            <MobileBody>
                <S.ItemsContainer>
                    {props.items.map((x) => (
                        <MobileRowItem
                            key={x.id}
                            clickHandler={() => selectorHandler(x.id)}
                            isSelected={x.id === props.selectedItemId}>
                            {x.title}
                        </MobileRowItem>
                    ))}
                </S.ItemsContainer>
                {props.isSubmitHandler && (
                    <S.ButtonContainer>
                        {props.isChanged ? (
                            <Button
                                elementSizeTypeValue={Theme.ElementSizeType.large}
                                clickHandler={submitAndCloseHandler}>
                                Сохранить изменения
                            </Button>
                        ) : (
                            <Button
                                elementSizeTypeValue={Theme.ElementSizeType.large}
                                colors={successButtonLight}
                                clickHandler={props.closeHandler}>
                                Сохранить изменения
                            </Button>
                        )}
                    </S.ButtonContainer>
                )}
            </MobileBody>
        </S.Container>
    )
}

PageWithSelectorItems.propTypes = {
    headerTitle: PropTypes.string.isRequired,
    // eslint-disable-next-line react/forbid-prop-types
    items: PropTypes.array.isRequired,
    // eslint-disable-next-line react/forbid-prop-types
    selectedItemId: PropTypes.any.isRequired,
    selectedHandler: PropTypes.func.isRequired,
    closeHandler: PropTypes.func.isRequired,
    // eslint-disable-next-line react/require-default-props
    isCloseWithSelect: PropTypes.bool.isRequired,
    // eslint-disable-next-line ,react/require-default-props
    isShowArrowBack: PropTypes.bool.isRequired,
    isSubmitHandler: PropTypes.bool,
    submitHandler: PropTypes.func,
    isChanged: PropTypes.bool
}

PageWithSelectorItems.defaultProps = {
    // eslint-disable-next-line react/default-props-match-prop-types
    isCloseWithSelect: true,
    // eslint-disable-next-line react/default-props-match-prop-types
    isShowArrowBack: false,
    isSubmitHandler: false,
    isChanded: false,
    submitHandler: () => {}
}

export default PageWithSelectorItems
