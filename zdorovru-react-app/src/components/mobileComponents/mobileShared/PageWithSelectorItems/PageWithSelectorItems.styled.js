import styled from 'styled-components'

export const Container = styled.div`
    background-color: white;
    position: fixed;
    height: 100%;
    width: 100%;
    top: 0;
    left: 0;
    z-index: 20;
`
export const ItemsContainer = styled.div`
    display: flex;
    flex-direction: column;
    & > div {
        border-bottom: 1px solid ${(props) => props.theme.colors.getGrayColor(0.1)};
    }
    overflow-x: auto;
    max-height: 100%;
    box-sizing: border-box;
`
export const ButtonContainer = styled.div`
    margin-top: 3.125rem;
    padding: 0 0.5rem;

    & > button {
        width: 100%;
    }
`
