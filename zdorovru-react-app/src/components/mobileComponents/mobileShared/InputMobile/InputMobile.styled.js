import styled, { css } from 'styled-components'

export const Container = styled.div`
    display: flex;
    align-items: center;
    padding: 0.5rem 1rem;
`

export const Label = styled.span`
    width: 10rem;
    ${(props) =>
        !props.isValid &&
        css`
            color: ${props.theme.colors.getRedheadColor()};
        `}
`

export const Input = styled.input`
    font: inherit;
    background: transparent;
    border: none;
    outline: none;
    padding: 0;
    margin: 0;
    height: 100%;
    width: 100%;
    max-width: 100%;
    vertical-align: bottom;
    text-align: inherit;
    box-sizing: content-box;
    -webkit-appearance: none;

    &::placeholder {
        color: ${(props) => props.theme.colors.getGrayColor(0.5)};
    }

    ${(props) =>
        !props.isValid &&
        css`
            color: ${props.theme.colors.getRedheadColor()};

            &::placeholder {
                color: ${props.theme.colors.getRedheadColor()};
            }
        `}
    &::-webkit-calendar-picker-indicator {
        background: white;
    }
`
export const InputWithLabel = styled.input`
    font: inherit;
    background: transparent;
    border: none;
    outline: none;
    padding: 0;
    margin: 0;
    height: 100%;
    width: 100%;
    max-width: 100%;
    vertical-align: bottom;
    text-align: inherit;
    box-sizing: content-box;
    -webkit-appearance: none;

    &::placeholder {
        color: ${(props) => props.theme.colors.getGrayColor(0.5)};
    }

    &::-webkit-calendar-picker-indicator {
        background: white;
    }
`

export const InputsMobileBlock = styled.div`
    display: flex;
    flex-direction: column;

    ${Container} {
        height: 2rem;
        border-bottom: 1px solid ${(props) => props.theme.colors.getGrayColor(0.1)};
    }
`
