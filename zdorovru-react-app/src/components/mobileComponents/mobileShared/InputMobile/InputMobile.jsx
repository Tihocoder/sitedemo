/* eslint-disable react/require-default-props */
import React from 'react'
import PropTypes from 'prop-types'
import InputMask from 'react-input-mask'
import * as S from './InputMobile.styled'

export const MaskType = {
    phone: 1
}

// eslint-disable-next-line consistent-return
const getPropsForMask = (type) => {
    // eslint-disable-next-line default-case
    switch (type) {
        case 1: {
            return {
                type: 'tel',
                mask: '+7(999) 999 99 99'
            }
        }
    }
}

const InputMobile = ({
    label,
    value,
    changeHandler,
    placeholder,
    inputMaskProps,
    customInputProps,
    inputRef,
    isValid
}) => {
    const changeInput = (e) => changeHandler(e.target.value)
    let inputProps = {
        onChange: changeInput,
        value
    }
    if (customInputProps) {
        inputProps = {
            ...inputProps,
            ...customInputProps
        }
    }
    if (placeholder) {
        inputProps.placeholder = placeholder
    }
    if (inputMaskProps.isEnabled) {
        inputProps = {
            ...inputProps,
            ...getPropsForMask(inputMaskProps.type)
        }
    }
    return (
        <S.Container>
            {label && <S.Label isValid={isValid}>{label}</S.Label>}
            {inputMaskProps.isEnabled ? (
                // eslint-disable-next-line react/jsx-props-no-spreading
                <InputMask {...inputProps}>
                    <S.InputWithLabel ref={inputRef} />
                </InputMask>
            ) : (
                // eslint-disable-next-line react/jsx-props-no-spreading
                <S.Input isValid ref={inputRef} {...inputProps} />
            )}
        </S.Container>
    )
}

InputMobile.propTypes = {
    changeHandler: PropTypes.func.isRequired,
    placeholder: PropTypes.string,
    value: PropTypes.string.isRequired,
    label: PropTypes.string,
    inputMaskProps: PropTypes.shape({
        isEnabled: PropTypes.bool,
        type: PropTypes.number
    }),
    // eslint-disable-next-line react/forbid-prop-types
    customInputProps: PropTypes.object,
    isValid: PropTypes.bool,
    // eslint-disable-next-line react/forbid-prop-types
    inputRef: PropTypes.any
}
InputMobile.defaultProps = {
    inputMaskProps: {
        isEnabled: false
    },
    isValid: true,
    inputRef: null
}

export default InputMobile
