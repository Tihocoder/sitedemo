import React, { useEffect, useState } from 'react'
import IconSearch from '@components/Icons/IconSearch'
import PropTypes from 'prop-types'
import { useDispatch, useSelector } from 'react-redux'
import { useRouter } from 'next/router'
import { popPath } from '@actions/customer'
import * as S from './MobilePageHeaderSearch.styled'

const MobilePageHeaderSearch = ({ isDefaultRouterBack, searchInput, isShowedShipment, blockRef }) => {
    const historyPaths = useSelector((state) => state.customer.historyPaths)
    const router = useRouter()
    const dispatch = useDispatch()
    const shipment = useSelector((state) => state.customer.shipment)

    const popPathHandler = async () => {
        await dispatch(popPath(router.back))
    }
    const generateDefaultBlock = () => (
        <S.ArrowBlock onClick={popPathHandler}>
            <S.IconArrowBack />
        </S.ArrowBlock>
    )
    const keyDownHandler = (e) => {
        if (searchInput.keyDownHandler)
            searchInput.keyDownHandler(e, () => {
                if (searchInput.searchRef) searchInput.searchText.current.blur()
            })
    }
    const isBackFromHistory =
        isDefaultRouterBack && historyPaths && historyPaths.length && historyPaths.length > 1

    return (
        <S.Container ref={blockRef}>
            <S.SearchContainer>
                {isBackFromHistory ? generateDefaultBlock() : <React.Fragment />}
                <S.SearchBlock>
                    <S.IconContainer>
                        <IconSearch />
                    </S.IconContainer>
                    <S.Input
                        value={searchInput.searchText}
                        onChange={searchInput.changeSearchHandler}
                        placeholder={searchInput.placeholder}
                        onKeyDown={keyDownHandler}
                        onFocus={searchInput.focusHandler}
                        onBlur={searchInput.blurHandler}
                        ref={searchInput.searchRef}
                        type="search"
                    />
                </S.SearchBlock>
            </S.SearchContainer>
        </S.Container>
    )
}

MobilePageHeaderSearch.propTypes = {
    isDefaultRouterBack: PropTypes.bool,
    blockRef: PropTypes.any,
    searchInput: PropTypes.shape({
        isShowed: PropTypes.bool.isRequired,
        placeholder: PropTypes.string,
        searchText: PropTypes.string,
        changeSearchHandler: PropTypes.func,
        keyDownHandler: PropTypes.func,
        searchRef: PropTypes.any,
        focusHandler: PropTypes.func,
        blurHandler: PropTypes.func
    }).isRequired,
    isShowedShipment: PropTypes.bool
}
MobilePageHeaderSearch.defaultProps = {
    isDefaultRouterBack: true,
    isShowedShipment: false
}

export default MobilePageHeaderSearch
