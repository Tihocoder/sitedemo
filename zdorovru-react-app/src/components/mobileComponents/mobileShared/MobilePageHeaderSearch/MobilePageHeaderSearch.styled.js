import styled from 'styled-components'

export const Container = styled.div`
    position: fixed;
    width: 100%;
    z-index: 12;
    box-sizing: border-box;
`
export const SearchContainer = styled.header`
    background-color: ${(props) => props.theme.colors.white};
    color: ${(props) => props.theme.colors.getGrayColor(1)};
    display: flex;
    padding: 0.4rem;
    align-items: center;
`
export const SearchBlock = styled.div`
    position: relative;
    height: 2.375rem;
    width: 100%;
`

export const Input = styled.input`
    width: 100%;
    padding: 0.75rem 2.1rem 0.75rem 2.5rem;
    font-size: 1.25rem;
    height: 100%;
    color: ${(props) => props.theme.colors.getGrayColor(0.7)};
    border: 1px solid ${(props) => props.theme.colors.getGreenColor()};
    box-sizing: border-box;
    outline: none;
    -webkit-appearance: none;
    border-radius: 5px;

    &::-webkit-search-cancel-button {
        -webkit-appearance: none;
        height: 1em;
        width: 1em;
        background: ${`url('${process.env.BASE_PATH}/images/close-mobile-gray.svg')`} no-repeat 50% 50%;
        background-size: contain;
        opacity: 0;
        pointer-events: none;
        position: absolute;
        right: 5px;
    }

    &:focus::-webkit-search-cancel-button {
        opacity: 1;
        pointer-events: all;
    }
`
export const IconContainer = styled.div`
    position: absolute;
    width: fit-content;
    height: fit-content;
    top: 0.5rem;
    left: 0.6rem;
    cursor: pointer;
`
export const ArrowBlock = styled.div`
    height: 100%;
    width: 2.2rem;
    box-sizing: border-box;
    display: flex;
`
export const IconArrowBack = styled.img.attrs({
    src: `${process.env.BASE_PATH || ''}/images/arrow_back.svg`
})``

export const ChangeTitle = styled.span`
    font-size: 0.85rem;
    color: ${(props) => props.theme.colors.getGrayColor(0.5)};
`
export const SpanDataItem = styled.span`
    color: ${(props) => props.theme.colors.getGrayColor(1)};
`
export const ShipmentContainer = styled.div`
    & > a {
        padding: 0.2rem 0.313rem;
        border-bottom: 1px solid ${(props) => props.theme.colors.getGrayColor(0.2)};
    }

    & > a:last-child {
        border-bottom: 1px solid ${(props) => props.theme.colors.getGrayColor(0.2)};
    }
`
