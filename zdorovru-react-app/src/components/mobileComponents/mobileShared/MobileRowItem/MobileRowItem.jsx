import React from 'react'
import PropTypes from 'prop-types'
import Link from 'next/link'
import * as S from './MobileRowItem.styled'
import ArrowRight from '../../../Icons/ArrowRight'

const MobileRowItem = ({
    children,
    isShowedRow,
    isSelected,
    clickHandler,
    isLink,
    linkUrl,
    mouseDownHandler
}) => {
    const generateRowItem = () => (
        <S.Container onClick={clickHandler} onMouseDown={mouseDownHandler}>
            {children}
            <S.ExtendElements>
                {isSelected && <S.SelectedImage />}
                {isShowedRow && (
                    <S.ArrowContainer>
                        <ArrowRight />
                    </S.ArrowContainer>
                )}
            </S.ExtendElements>
        </S.Container>
    )

    if (isLink)
        return (
            <Link href={linkUrl} passHref>
                {generateRowItem()}
            </Link>
        )
    return generateRowItem()
}

MobileRowItem.propTypes = {
    isShowedRow: PropTypes.bool.isRequired,
    isSelected: PropTypes.bool.isRequired,
    clickHandler: PropTypes.func.isRequired,
    isLink: PropTypes.bool.isRequired,
    mouseDownHandler: PropTypes.func,
    linkUrl: PropTypes.string
}

MobileRowItem.defaultProps = {
    isShowedRow: false,
    isSelected: false,
    clickHandler: () => {},
    onMouseDown: () => {},
    isLink: false
}

export default MobileRowItem
