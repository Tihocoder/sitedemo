import styled from 'styled-components'

export const ExtendElements = styled.div`
    display: flex;
    justify-content: flex-end;
`
export const ArrowContainer = styled.div`
    width: 2rem;
    height: 100%;
    display: flex;
    align-items: center;
`

export const Container = styled.a`
    display: flex;
    padding: 0.563rem 0.313rem;
    border-bottom: 1px solid ${(props) => props.theme.colors.getGrayColor(0.1)};
    justify-content: space-between;
    align-items: center;
    min-height: fit-content;
    background-color: ${(props) => props.theme.colors.white};
    color: ${(props) => props.theme.colors.getGrayColor(1)};
    line-height: 1.5rem;
`
export const SelectedImage = styled.img.attrs({
    src: `${process.env.BASE_PATH}/images/check_circle.svg`
})``

export const ChangeTitle = styled.span`
    font-size: 0.85rem;
    color: ${(props) => props.theme.colors.getGrayColor(0.5)};
`
