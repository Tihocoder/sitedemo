import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import * as _ from 'lodash'
import NavList from '../shared/NavList/NavList'

const CategoriesNavigation = (props) => {
    const [categoriesLinks, setCategoriesLinks] = useState([])

    const generateNav = (category) => {
        let result = {
            id: category.id,
            text: category.title,
            childs: []
        }
        const childsCategories = props.categories.filter((cat) => cat.parentGroupId === category.id)
        if (!childsCategories || childsCategories.length < 1) return result
        childsCategories.forEach((cat) => {
            result.childs.push(generateNav(cat))
        })

        return result
    }

    useEffect(() => {
        const maxLevel = _.maxBy(props.categories, (x) => x.level)
        const generalCategories = props.categories.filter((x) => x.level === 0)
        let resultNav = []
        generalCategories.forEach((category) => resultNav.push(generateNav(category)))
        setCategoriesLinks(resultNav)
    }, [props.categories])

    return <NavList title={'Найдено в категориях'} navItems={categoriesLinks} />
}

CategoriesNavigation.propTypes = {
    categories: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.number.isRequired,
            parentGroupId: PropTypes.number.isRequired,
            title: PropTypes.string.isRequired,
            level: PropTypes.number.isRequired,
            textOrder: PropTypes.string.isRequired
        })
    ).isRequired
}

export default CategoriesNavigation
