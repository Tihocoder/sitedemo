// eslint-disable-next-line import/no-duplicates
import React, { useContext, useEffect, useRef, useState } from 'react'
import { useSelector } from 'react-redux'
import { MobileBody } from '@styles/ui-element.styled'
import * as catalogService from '@services/catalogService'
import * as S from '@mobileShared/MobilePageHeaderSearch/MobilePageHeaderSearch.styled'
import MobileRowWithIconItem from '@mobileShared/MobileRowWithIconItem/MobileRowWithIconItem'
import MobileStockSelector from '@mobilePages/MobileStockSelector/MobileStockSelector'
import MobileAddressSelector from '@mobilePages/MobileAddressSelector/MobileAddressSelector'
import MobileHeader from '@mobileShared/MobileHeader/MobileHeader'
import CatalogGoodContainer from '@mobileComponents/catalog/CatalogGoodContainer/CatalogGoodContainer'
import { SearchContext } from '@components/Layout/MobileLayout'

const MobileSearchPage = () => {
    const shipment = useSelector((state) => state.customer.shipment)
    const goods = useSelector((state) => state.catalog.goods)
    const [isShowedShipmentState, setIsShowedShipmentState] = useState(false)
    const [heightHeader, setHeightHeader] = useState(null)
    const blockHeaderRef = useRef(null)
    const searchRef = useRef(null)
    const [isStockSelector, setIsStockSelector] = useState(false)
    const [isShipAddressSelector, setIsShipAddressSelector] = useState(false)
    const toggleIsStockSelector = () => setIsStockSelector(!isStockSelector)
    const toggleIsShipAddressSelector = () => setIsShipAddressSelector(!isShipAddressSelector)

    const { searchText, changeSearchHandler, keyDownHandler, searchButtonHandler } = useContext(SearchContext)

    useEffect(() => {
        const isShowed = catalogService.checkShowedShipmentSearch()
        setIsShowedShipmentState(isShowed)
        catalogService.updateShowedDateShipmentSearch()
    }, [])
    useEffect(() => {
        if (blockHeaderRef?.current?.getBoundingClientRect().height !== heightHeader)
            setHeightHeader(blockHeaderRef?.current?.getBoundingClientRect().height)
    }, [blockHeaderRef?.current?.clientHeight, shipment.shipAddressId])

    if (isStockSelector) {
        return <MobileStockSelector closeHandler={toggleIsStockSelector} />
    }
    if (isShipAddressSelector) {
        return (
            <MobileAddressSelector
                closeHandler={toggleIsShipAddressSelector}
                shipAddressId={shipment.shipAddressId}
            />
        )
    }

    return (
        <React.Fragment>
            <MobileHeader
                isShowedTitle={false}
                isBorderAvailable={false}
                isDefaultRouterBack={false}
                blockRef={blockHeaderRef}
                searchInput={{
                    isShowed: true,
                    searchText,
                    searchRef,
                    searchButtonHandler,
                    changeSearchHandler,
                    keyDownHandler,
                    placeholder: 'Поиск на ЗДОРОВ.ру'
                }}
            />
            <MobileBody isSearchHeader isShowedShipment={isShowedShipmentState} customPixel={heightHeader}>
                {isShowedShipmentState && (
                    <S.ShipmentContainer>
                        <MobileRowWithIconItem
                            iconName="geo"
                            isShowedRow
                            clickHandler={toggleIsStockSelector}>
                            <S.SpanDataItem>{shipment.stockTitle || 'Не выбрана'}</S.SpanDataItem>
                            <S.ChangeTitle>Выбрать аптеку самовывоза</S.ChangeTitle>
                        </MobileRowWithIconItem>
                        <MobileRowWithIconItem
                            iconName="auto"
                            isShowedRow
                            clickHandler={toggleIsShipAddressSelector}>
                            <S.SpanDataItem>{shipment.shipAddressTitle || 'Не выбран'}</S.SpanDataItem>
                            <S.ChangeTitle>Изменить адрес доставки</S.ChangeTitle>
                        </MobileRowWithIconItem>
                    </S.ShipmentContainer>
                )}
                <CatalogGoodContainer shipment={shipment}>{goods}</CatalogGoodContainer>
            </MobileBody>
        </React.Fragment>
    )
}

export default MobileSearchPage
