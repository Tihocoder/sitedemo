import React from 'react'
import { MobileBody } from '@styles/ui-element.styled'
import MobileHeader from '@mobileShared/MobileHeader/MobileHeader'
import CatalogGoodContainer from '../../../mobileComponents/catalog/CatalogGoodContainer/CatalogGoodContainer'
import { useLiked } from '../../../../hooks/liked-hooks'

const LikedPage = () => {
    const { shipment, goods } = useLiked()

    return (
        <React.Fragment>
            <MobileHeader>Избранное</MobileHeader>
            <MobileBody>
                <CatalogGoodContainer shipment={shipment}>{goods}</CatalogGoodContainer>
            </MobileBody>
        </React.Fragment>
    )
}

export default LikedPage
