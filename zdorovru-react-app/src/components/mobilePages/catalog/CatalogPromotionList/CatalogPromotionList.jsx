import React from 'react'
import MobilePageHeader from '@mobileShared/MobilePageHeader/MobilePageHeader'
import Link from 'next/link'
import PropTypes from 'prop-types'
import { PromotionType } from 'src/consts'
import { MobileBody } from '@styles/ui-element.styled'
import * as S from './CatalogPromotionList.styled'
import MobileHeader from "@mobileShared/MobileHeader/MobileHeader";

const DateToString = (date) => {
    const dateInstance = new Date(date)
    const options = { year: 'numeric', month: 'long', day: 'numeric' }
    return dateInstance.toLocaleDateString('ru-RU', options)
}
const CatalogPromotionList = (props) => (
    <S.Container>
        <MobileHeader>Скидки и акции</MobileHeader>
        <MobileBody>
            <S.ContainerPromotion>
                {props.promotions.map((promotion) => (
                    <Link
                        key={promotion.promotionId}
                        href={`/catalog/promotion/${promotion.promotionId}`}
                        passHref>
                        <S.BockWithShadow key={promotion.promotionId}>
                            <S.Promo>
                                <S.PromoImageHeader>
                                    <S.Image src={promotion.imageUrl} />
                                </S.PromoImageHeader>
                                <S.PromoBody>
                                    <S.Description>{promotion.promotionName}</S.Description>
                                    {promotion.isActual ? (
                                        <S.BottomBlock>
                                            <S.DateDesct>До {DateToString(promotion.endDate)}</S.DateDesct>
                                            <S.DescriptionBottomRigth>
                                                Подробнее об акции
                                            </S.DescriptionBottomRigth>
                                        </S.BottomBlock>
                                    ) : (
                                        <S.BottomBlockIsNotActual>
                                            <S.DateDesct>
                                                Завершилась {DateToString(promotion.endDate)}
                                            </S.DateDesct>
                                            <S.DescriptionBottomRigth>
                                                Подробнее об акции
                                            </S.DescriptionBottomRigth>
                                        </S.BottomBlockIsNotActual>
                                    )}
                                </S.PromoBody>
                            </S.Promo>
                        </S.BockWithShadow>
                    </Link>
                ))}
            </S.ContainerPromotion>
        </MobileBody>
    </S.Container>
)

CatalogPromotionList.propTypes = {
    promotions: PropTypes.arrayOf(PropTypes.shape(PromotionType)).isRequired
}

export default CatalogPromotionList
