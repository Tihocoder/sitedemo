import React, { useContext, useEffect, useRef, useState } from 'react'
import { useSelector } from 'react-redux'
import { useRouter } from 'next/router'
import { MobileBody } from '@styles/ui-element.styled'
import MobileHeader from '@mobileShared/MobileHeader/MobileHeader'
import * as S from './CatalogPage.styled'
import MobileRowItem from '../../../mobileComponents/mobileShared/MobileRowItem/MobileRowItem'
import { SearchContext } from '../../../Layout/MobileLayout'
import MobilePageHeader from '../../../mobileComponents/mobileShared/MobilePageHeader/MobilePageHeader'

const CatalogPage = (props) => {
    const groups = useSelector((state) => state.catalog.mainGroups)
    const generals = groups.filter((group) => group.isGeneral)
    const router = useRouter()
    const [heightHeader, setHeightHeader] = useState(null)
    const blockHeaderRef = useRef(null)
    const searchRef = useRef(null)
    useEffect(() => {
        if (blockHeaderRef?.current?.getBoundingClientRect().height !== heightHeader)
            setHeightHeader(blockHeaderRef?.current?.getBoundingClientRect().height)
    }, [blockHeaderRef?.current?.clientHeight])
    const { searchText, changeSearchHandler, keyDownHandler, searchButtonHandler } = useContext(SearchContext)
    if (generals.length)
        return (
            <S.Container>
                <MobileHeader
                    blockRef={blockHeaderRef}
                    searchInput={{
                        isShowed: true,
                        searchText,
                        changeSearchHandler,
                        keyDownHandler,
                        searchButtonHandler,
                        placeholder: 'Поиск на ЗДОРОВ.ру'
                    }}>
                    Каталог
                </MobileHeader>
                <MobileBody customPixel={heightHeader} isExtendBlock>
                    {generals.map((x) => (
                        <MobileRowItem key={x.id} linkUrl={`${router.asPath}/${x.id}`} isLink isShowedRow>
                            {x.title}
                        </MobileRowItem>
                    ))}
                </MobileBody>
            </S.Container>
        )
    return (
        <React.Fragment>
            <MobileHeader
                isDefaultRouterBack={false}
                searchInput={{
                    isShowed: true,
                    searchText,
                    changeSearchHandler,
                    keyDownHandler,
                    placeholder: 'Поиск'
                }}>
                Каталог
            </MobileHeader>
        </React.Fragment>
    )
}

CatalogPage.propTypes = {}

export default CatalogPage
