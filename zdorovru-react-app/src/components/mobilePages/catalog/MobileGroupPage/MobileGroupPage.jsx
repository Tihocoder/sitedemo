import React, { useContext, useEffect, useRef, useState } from 'react'
import { useSelector } from 'react-redux'
import { MobileBody } from '@styles/ui-element.styled'
import CatalogGoodContainer from '@mobileComponents/catalog/CatalogGoodContainer/CatalogGoodContainer'
import CatalogGroupList from '@mobileComponents/catalog/CatalogGroupList/CatalogGroupList'
import { useRouter } from 'next/router'
import PropTypes from 'prop-types'
import MobileHeader from '@mobileShared/MobileHeader/MobileHeader'
import * as S from './MobileGroupPage.styled'
import { SearchContext } from '../../../Layout/MobileLayout'

const MobileGroupPage = (props) => {
    const goods = useSelector((state) => state.catalog.goods)
    const goodsGroups = useSelector((state) => state.catalog.goodsGroups)
    const groups = useSelector((state) => state.catalog.mainGroups)
    const shipment = useSelector((state) => state.customer.shipment)
    const [heightHeader, setHeightHeader] = useState(null)
    const router = useRouter()
    const blockHeaderRef = useRef(null)
    const searchRef = useRef(null)
    useEffect(() => {
        if (blockHeaderRef?.current?.getBoundingClientRect().height !== heightHeader)
            setHeightHeader(blockHeaderRef?.current?.getBoundingClientRect().height)
    }, [blockHeaderRef?.current?.clientHeight, shipment.shipAddressId])
    const { searchText, changeSearchHandler, keyDownHandler, searchButtonHandler } = useContext(SearchContext)
    const group = groups.find((x) => x.id === props.groupId)
    const clickBackHandler = async () => {
        if (!props.groupPosition) return
        if (props.groupPosition === 'group3') {
            await router.push(`/catalog/${router.query.group1}/${router.query.group2}`)
        }
        if (props.groupPosition === 'group2') {
            await router.push(`/catalog/${router.query.group1}`)
        }
        if (props.groupPosition === 'group1') {
            await router.push(`/catalog`)
        }
    }

    return (
        <S.Container>
            <MobileHeader
                isDefaultRouterBack
                blockRef={blockHeaderRef}
                searchInput={{
                    isShowed: true,
                    searchText,
                    changeSearchHandler,
                    searchButtonHandler,
                    keyDownHandler,
                    searchRef,
                    placeholder: 'Поиск на ЗДОРОВ.ру'
                }}>
                {group?.title || 'Каталог'}
            </MobileHeader>
            <MobileBody customPixel={heightHeader} isExtendBlock>
                {goodsGroups && goodsGroups.length > 0 ? (
                    <CatalogGroupList>{goodsGroups}</CatalogGroupList>
                ) : (
                    <React.Fragment />
                )}
                <CatalogGoodContainer groupId={props.groupId} shipment={shipment}>
                    {goods}
                </CatalogGoodContainer>
            </MobileBody>
        </S.Container>
    )
}
MobileGroupPage.propTypes = {
    groupPosition: PropTypes.string
}
MobileGroupPage.defaultProps = {
    groupPosition: ''
}
export default MobileGroupPage
