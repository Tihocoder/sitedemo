/* eslint-disable @typescript-eslint/no-unsafe-member-access */
import styled from 'styled-components'

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    overflow-x: hidden;
`
