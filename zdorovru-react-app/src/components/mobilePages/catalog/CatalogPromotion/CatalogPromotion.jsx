import React, { useEffect, useState } from 'react'
import { getPromotionById } from '@api/siteApis'
import { useRouter } from 'next/router'
import { useSelector } from 'react-redux'
import * as S from './CatalogPromotion.styled'
import CatalogGoodContainer from '@mobileComponents/catalog/CatalogGoodContainer/CatalogGoodContainer'
import MobilePageHeader from '@mobileShared/MobilePageHeader/MobilePageHeader'
import { MobileBody } from '@styles/ui-element.styled'
import MobileHeader from "@mobileShared/MobileHeader/MobileHeader";

const DateToString = (date) => {
    const dateInstance = new Date(date)
    const options = { year: 'numeric', month: 'long', day: 'numeric' }
    return dateInstance.toLocaleDateString('ru-RU', options)
}

const CatalogPromotion = () => {
    const router = useRouter()
    const { promotionId } = router.query
    const shipment = useSelector((state) => state.customer.shipment)
    const goods = useSelector((state) => state.catalog.goods)
    const [currentPromotion, setCurrentPromotion] = useState({})
    useEffect(() => {
        const initCurrentPromotion = async () => {
            const promotion = await getPromotionById(promotionId)
            setCurrentPromotion(promotion)
        }
        initCurrentPromotion()
        return () => {
            setCurrentPromotion(0)
        }
    }, [promotionId])

    return (
        <S.Container>
            <MobileHeader>Акция</MobileHeader>
            <MobileBody>
                <S.BockWithShadow>
                    <S.Promo>
                        <S.PromoImageHeader>
                            <S.Image src={currentPromotion.imageUrl} />
                        </S.PromoImageHeader>
                        <S.PromoBody>
                            <S.Description>{currentPromotion.promotionName}</S.Description>
                            {currentPromotion.isActual ? (
                                <S.BottomBlock>
                                    <S.DateDesct>До {DateToString(currentPromotion.endDate)}</S.DateDesct>
                                </S.BottomBlock>
                            ) : (
                                <S.BottomBlockIsNotActual>
                                    <S.DateDesct>
                                        Завершилась {DateToString(currentPromotion.endDate)}
                                    </S.DateDesct>
                                </S.BottomBlockIsNotActual>
                            )}
                        </S.PromoBody>
                    </S.Promo>
                </S.BockWithShadow>
                <S.DescriptionBetweenBlock>Товары, участвующие в акции</S.DescriptionBetweenBlock>
                <CatalogGoodContainer shipment={shipment}>{goods}</CatalogGoodContainer>
            </MobileBody>
        </S.Container>
    )
}

CatalogPromotion.propTypes = {}

export default CatalogPromotion
