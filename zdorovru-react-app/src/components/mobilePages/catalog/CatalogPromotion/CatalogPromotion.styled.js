import styled from 'styled-components'

// eslint-disable-next-line import/prefer-default-export
export const Container = styled.div`
    display: flex;
    flex-direction: column;
    color: ${(props) => props.theme.colors.getGrayColor()};
`

export const BockWithShadow = styled.div`
    transition: all 0.2s ease;
    box-sizing: border-box;
    padding: 1.5rem;
    border-radius: 0.3rem;
    text-decoration: none;
    flex: 1 1 auto;
`
export const Promo = styled.div`
    display: flex;
    flex-direction: column;
    height: 100%;
`
export const PromoImageHeader = styled.div`
    box-sizing: border-box;
    background-color: ${(props) => props.theme.colors.getGrayColor(0.03)};
`
export const PromoBody = styled.div`
    padding: 0.2rem;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    height: 100%;
    min-height: 4rem;
`

export const Image = styled.img`
    width: 100%;
    height: auto;
    box-sizing: border-box;
    border-top-left-radius: 0.3rem;
    border-top-right-radius: 0.3rem;
`

export const Description = styled.div``

export const DescriptionBetweenBlock = styled.div`
    padding: 1rem;
    background-color: ${(props) => props.theme.colors.getGrayColor(0.07)};
    color: ${(props) => props.theme.colors.getGrayColor(0.4)};
`

export const BottomBlock = styled.div`
    display: flex;
    justify-content: space-between;
    color: ${(props) => props.theme.colors.getGrayColor(0.5)};
    flex-wrap: wrap;
`
export const BottomBlockIsNotActual = styled.div`
    display: flex;
    justify-content: space-between;
    color: ${(props) => props.theme.colors.getGrayColor(0.5)};
    flex-wrap: wrap;
    font-size: 0.8rem;
`
export const Footer = styled.div`
    display: flex;
    flex-direction: row;
    flex-wrap: nowrap;
    justify-content: space-between;
`

export const DateDesct = styled.span`
    user-select: none;
    white-space: nowrap;
`
export const Link = styled.a``
