import React, { useContext, useEffect, useState } from 'react'
import MobilePageHeader from '@mobileShared/MobilePageHeader/MobilePageHeader'
import PropTypes from 'prop-types'
import { MobileBody, MobileWhiteBlock } from '@styles/ui-element.styled'
import * as catalogService from '@services/catalogService'
import DataBlock from '@shared/DataBlock/DataBlock'
import { useDispatch, useSelector } from 'react-redux'
import { getInfoList } from '@api/siteApis'
import MobileRowItem from '@mobileShared/MobileRowItem/MobileRowItem'
import AnaloguesPage from '@mobilePages/catalog/GoodPageMobile/AnaloguesPage/AnaloguesPage'
import { _PricePropTypes, _WebDataPropTypes, DeliveryStatusType } from 'src/consts'
import AnotherOutForms from '@mobilePages/catalog/GoodPageMobile/AnotherOutForms/AnotherOutForms'
import PriceView from '@mobilePages/catalog/GoodPageMobile/PriceView/PriceView'
import Button from '@shared/Button/Button'
import * as Theme from '@styles/theme-const'
import { AlertsStateContext } from '@context/AlertsState'
import { addBasketItem, changeBasketItemPrice, toggleLikedGood } from '@actions/customer'
import ModalWindow from '@shared/ModalWindow/ModalWindow'
import CountChanger from '@shared/CountChanger/CountChanger'
import { useRouter } from 'next/router'
import FavoriteIndicate from '@mobileComponents/catalog/FavoriteIndicate/FavoriteIndicate'
import * as S from './GoodPageMobile.styled'
import MobileHeader from "@mobileShared/MobileHeader/MobileHeader";

const GoodPageMobile = (props) => {
    const { webData } = props.good
    const [infoList, setInfoList] = useState([])
    const router = useRouter()
    const shipment = useSelector((state) => state.customer.shipment)
    const [isAnaloguesPage, setIsAnaloguesPage] = useState(false)
    const [isAnotherOutForms, setIsAnotherOutForms] = useState(false)
    const { deleteFromBasket } = useContext(AlertsStateContext)
    const basketItem = useSelector(
        (state) =>
            state.customer.basketItems.find((x) => x.webData.webId === props.good.webData.webId) || null
    )
    const dispatch = useDispatch()
    const features = useSelector((state) => state.catalog.features)
    const likedGoods = useSelector((state) => state.customer.likedGoods)
    const [isShowedModalDemand, setIsShowedModalDemand] = useState(false)
    const toggleDemandModal = () => {
        setIsShowedModalDemand(!isShowedModalDemand)
    }

    const addBasket = async () => {
        /// /RuTarget
        const _rutarget = window._rutarget || []
        _rutarget.push({ event: 'thankYou', conv_id: 'cart' })

        await dispatch(addBasketItem(props.good))
        setIsShowedModalDemand(false)
    }

    const addBasketHandler = async () => {
        if (!shipment.shipAddressId && !shipment.stockId) {
            //  setIsShowedModalNotSelectedShipment(true)
            await addBasket()
            return
        }
        if (props.good.isDemand) {
            toggleDemandModal()
            return
        }
        await addBasket()
    }
    // eslint-disable-next-line no-shadow
    const changePriceHandler = async (count) => {
        if (count > 0) {
            await dispatch(changeBasketItemPrice(props.good.webData.webId, count))
        } else {
            deleteFromBasket.openHandler(props.good.webData.webId)
        }
    }
    const initInfoList = async () => {
        const responseInfoList = await getInfoList(webData.goodId)
        setInfoList(responseInfoList)
    }
    const toggleLikeHandler = async (goodId) => {
        await dispatch(toggleLikedGood(goodId))
    }
    useEffect(() => {
        initInfoList()
    }, [])

    useEffect(() => {
        if (isAnaloguesPage !== props.isShowedAnalogues) {
            setIsAnaloguesPage(props.isShowedAnalogues)
        }
    }, [])
    return (
        <S.Container>
            {isAnaloguesPage && (
                <AnaloguesPage
                    closeHandler={() => setIsAnaloguesPage(false)}
                    goodId={webData.goodId}
                    shipment={shipment}
                />
            )}
            {isAnotherOutForms && (
                <AnotherOutForms
                    closeHandler={() => setIsAnotherOutForms(!isAnotherOutForms)}
                    goodId={webData.goodId}
                    shipment={shipment}
                />
            )}
            <MobileHeader
                right={
                    <FavoriteIndicate
                        isSelected={likedGoods.includes(props.good.webData.goodId) || false}
                        clickHandler={async () => {
                            await toggleLikeHandler(props.good.webData.goodId)
                        }}
                    />
                }>
                {webData.drugTitle}
            </MobileHeader>
            <MobileBody>
                <MobileWhiteBlock>
                    <S.ImageContainer isPointer={webData.hasImage}>
                        <S.GoodImage
                            src={catalogService.getUslImage(
                                webData.hasImage,
                                webData.goodId,
                                catalogService.ImageSizeType.large
                            )}
                            onError={(e) => {
                                e.target.onerror = null
                                e.target.src = `${process.env.BASE_PATH || ''}/images/noimg.gif`
                            }}
                        />
                    </S.ImageContainer>
                    <S.DescriptionNameBlock>
                        <S.DescriptionName>{webData.drugTitle}</S.DescriptionName>
                        <S.DescriptionOutFormTitle>{webData.outFormTitle}</S.DescriptionOutFormTitle>
                        <S.DescriptionMaker>{webData.makerTitle}</S.DescriptionMaker>
                        {webData.mnnRusTitle !== 'Не установлено' ? (
                            <DataBlock description="Действующее вещество">{webData.mnnRusTitle}</DataBlock>
                        ) : (
                            <React.Fragment />
                        )}
                        <S.BlockMarkers>
                            {props.good.isCold && (
                                <S.GoodMarker>
                                    <S.GoodMarkerLogoCold />
                                    <S.GoodMarkerSpanCold>Хранение в холоде</S.GoodMarkerSpanCold>
                                </S.GoodMarker>
                            )}
                            {props.good.isStrictlyByPrescription && (
                                <S.GoodMarker>
                                    <S.GoodMarkerLogoPrescription />
                                    <S.GoodMarkerSpanPrescription>
                                        Строго по рецепту
                                    </S.GoodMarkerSpanPrescription>
                                </S.GoodMarker>
                            )}
                        </S.BlockMarkers>
                    </S.DescriptionNameBlock>
                </MobileWhiteBlock>
                <MobileWhiteBlock>
                    <PriceView
                        goodId={props.good.webData.goodId}
                        isAvailableOnStock={props.good.isAvailableOnStock}
                        availableStocksCount={props.good.availableStocksCount}
                        isSelectedStock={!!shipment.stockId}
                        stockPrice={props.good.stockPrice}
                        minPrice={props.good.minPrice}
                        deliveryPrice={props.good.deliveryPrice}
                        deliveryStatus={props.good.deliveryStatus}
                    />
                    <S.AddBasketBlock>
                        {basketItem?.count ? (
                            <S.CountChangeContainer>
                                <CountChanger
                                    value={basketItem.count}
                                    changeValueHandler={changePriceHandler}
                                />
                            </S.CountChangeContainer>
                        ) : (
                            <Button
                                clickHandler={addBasketHandler}
                                elementSizeTypeValue={Theme.ElementSizeType.regular}>
                                Добавить в корзину
                            </Button>
                        )}
                    </S.AddBasketBlock>
                </MobileWhiteBlock>
                {props.good.analoguesQty ? (
                    <MobileRowItem isShowedRow clickHandler={() => setIsAnaloguesPage(true)}>
                        Аналоги ({props.good.analoguesQty})
                    </MobileRowItem>
                ) : (
                    <React.Fragment />
                )}
                {props.good.anotherOutFormsQty ? (
                    <MobileRowItem isShowedRow clickHandler={() => setIsAnotherOutForms(true)}>
                        Формы выпуска ({props.good.anotherOutFormsQty})
                    </MobileRowItem>
                ) : (
                    <React.Fragment />
                )}
                <MobileWhiteBlock>
                    <S.InfoListBlock>
                        {infoList.map((x) => (
                            <S.InfoItem key={x.typeId}>
                                <S.InfoItemTitle>{x.title}</S.InfoItemTitle>
                                <S.InfoItemBody
                                    dangerouslySetInnerHTML={{
                                        __html: x.text
                                    }}
                                />
                            </S.InfoItem>
                        ))}
                    </S.InfoListBlock>
                </MobileWhiteBlock>
            </MobileBody>
            {isShowedModalDemand && (
                <ModalWindow
                    title="Внимание"
                    isMobile
                    isButtonFooter={false}
                    cancel={{
                        text: 'Отмена',
                        clickHandler: toggleDemandModal
                    }}>
                    <S.BodyModel
                        dangerouslySetInnerHTML={{
                            __html: catalogService.getFeatureHtml(props.good, features)
                        }}
                    />
                    <S.FooterFuterBlock>
                        <Button elementSizeTypeValue={Theme.ElementSizeType.regular} clickHandler={addBasket}>
                            В корзину
                        </Button>
                        <Button
                            elementSizeTypeValue={Theme.ElementSizeType.regular}
                            clickHandler={toggleDemandModal}>
                            Отмена
                        </Button>
                    </S.FooterFuterBlock>
                </ModalWindow>
            )}
        </S.Container>
    )
}

GoodPageMobile.propTypes = {
    good: PropTypes.shape({
        webData: _WebDataPropTypes,
        infoList: PropTypes.arrayOf(
            PropTypes.shape({
                typeId: PropTypes.number.isRequired,
                title: PropTypes.string.isRequired,
                text: PropTypes.string.isRequired,
                priorityCount: PropTypes.number.isRequired
            })
        ),
        isDemand: PropTypes.bool.isRequired,
        stockPrice: _PricePropTypes,
        deliveryPrice: _PricePropTypes,
        availableStocksCount: PropTypes.number.isRequired,
        minPrice: _PricePropTypes,
        isAvailable: PropTypes.bool.isRequired,
        isAvailableOnStock: PropTypes.bool.isRequired,
        isStrictlyByPrescription: PropTypes.bool,
        isOriginalGood: PropTypes.bool,
        isCloud: PropTypes.bool,
        analoguesQty: PropTypes.number,
        anotherOutFormsQty: PropTypes.number,
        deliveryStatus: PropTypes.shape(DeliveryStatusType).isRequired
    }).isRequired,
    isShowedAnalogues: PropTypes.bool
}

GoodPageMobile.defaultProps = {
    isShowedAnalogues: true
}

export default GoodPageMobile
