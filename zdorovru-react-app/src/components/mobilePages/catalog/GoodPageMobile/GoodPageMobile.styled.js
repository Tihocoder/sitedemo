import styled from 'styled-components'
import * as Theme from '@styles/theme-const'
import { ButtonElement } from '@shared/Button/Button.styled'
import { ButtonLeft, ButtonRight, Input } from '@shared/CountChanger/CountChanger.styled'

export const Container = styled.div`
    display: flex;
    flex-direction: column;
`

export const ImageContainer = styled.div`
    width: 100%;
    box-sizing: border-box;
    display: flex;
    justify-content: center;
    cursor: ${(props) => props.isPointer && 'pointer'};
`

export const GoodImage = styled.img`
    width: auto;
    max-height: 16rem;
`

export const DescriptionNameBlock = styled.div`
    display: flex;
    flex-direction: column;

    & > * {
        margin-bottom: 0.2rem;
    }
`
export const BlockMarkers = styled.div`
    display: flex;
    flex-wrap: wrap;
`

export const GoodMarker = styled.div`
    display: flex;
    align-items: center;
    flex: 1 1 auto;
`
export const GoodMarkerLogoCold = styled.img.attrs({
    src: `${process.env.BASE_PATH}/images/ac_unit.svg`
})``
export const GoodMarkerSpanCold = styled.div`
    color: ${(props) => props.theme.colors.colorCold};
`
export const GoodMarkerLogoPrescription = styled.img.attrs({
    src: `${process.env.BASE_PATH}/images/warning_amber.svg`
})``
export const GoodMarkerSpanPrescription = styled.div`
    color: ${(props) => props.theme.colors.getRedheadColor()};
`

export const DescriptionName = styled.span`
    font-size: 1.25rem;
`
export const DescriptionOutFormTitle = styled.span``
export const DescriptionMaker = styled.span``

export const InfoListBlock = styled.div`
    display: flex;
    flex-direction: column;

    & table {
        border: 1px solid ${(props) => props.theme.colors.getGrayColor(0.05)};
        border-collapse: collapse;
    }

    /* & table tr {
        border: 1px solid ${(props) => props.theme.colors.getGrayColor(0.04)};
        border-collapse: collapse;
    } */
    & table td {
        padding: 0.1rem;
        border: 1px solid ${(props) => props.theme.colors.getGrayColor(0.05)};
    }
    & table .PrepInSost {
        padding: 3px;
        background: ${(props) => props.theme.colors.getGrayColor(0.04)};
    }
    & table .PrepInSost_Val {
        padding: 3px;
        background: ${(props) => props.theme.colors.getGrayColor(0.04)};
    }
`

export const InfoItem = styled.div`
    display: flex;
    flex-direction: column;
    margin-bottom: 1rem;
`
export const InfoItemTitle = styled.div`
    color: ${Theme.ColorConsts.getGrayColor(0.75)};
    font-weight: bold;
    font-size: 1.3rem;
    margin-bottom: 0.2rem;
`
export const InfoItemBody = styled.div`
    color: ${Theme.ColorConsts.getGrayColor(0.6)};
`
export const AddBasketBlock = styled.div`
    ${ButtonElement} {
        width: 100%;
    }

    display: flex;
    justify-content: flex-end;
`
export const BodyModel = styled.div`
    & hr {
        border: none;
        color: ${(props) => props.theme.colors.getGrayColor(0.2)};
        height: 1px;
        background-color: ${(props) => props.theme.colors.getGrayColor(0.2)};
    }
`

export const FooterFuterBlock = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
    height: 5rem;
    justify-content: space-between;
    margin-top: 0.4rem;

    ${ButtonElement} {
        margin-top: 0.4rem;
    }
`
export const CountChangeContainer = styled.div`
    max-width: 8.5rem;
    box-sizing: border-box;
    height: 2.586rem;
    display: flex;

    ${ButtonLeft} {
        width: 35%;
        padding: 0.2rem;
        display: flex;
        align-items: center;
        justify-content: center;
    }

    ${ButtonRight} {
        width: 35%;
        padding: 0.02rem;
        display: flex;
        align-items: center;
        justify-content: center;
    }

    ${Input} {
        width: 60%;
        padding: 0.5rem 0.4rem;
    }

    ${ButtonElement} {
        width: 100%;
        height: 100%;
        padding: 0.2rem 0.6rem;
    }
`
