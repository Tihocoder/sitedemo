import React, { useContext, useState } from 'react'
import { declOfNum, generateDate } from '@helpers/site'
import ModalWindow from '@shared/ModalWindow/ModalWindow'
import PropTypes from 'prop-types'
import SymbolRuble from '@shared/SymbolRuble/SymbolRuble'
// eslint-disable-next-line import/named
import { DeliveryStatusType, PriceType } from 'src/consts'
import { MenuContext } from '@context/CatalogMenuState'
import * as S from './PriceView.styled'

const getPriceItem = (
    isAvailable,
    isDeliveryPrice,
    price,
    availableStocksCount,
    isSelectedStock,
    clickHandlerDeliveryReasons,
    clickOpenAnotherStockHandler
) => {
    const getAnotherStockDescription = () => {
        if (availableStocksCount) {
            return (
                <S.AnotherStockCountDescription>
                    Нет в выбранной, самовывоз из {availableStocksCount}{' '}
                    {declOfNum(availableStocksCount, ['аптеки', 'аптек', 'аптек'])}
                </S.AnotherStockCountDescription>
            )
        }
        return <S.AnotherStockCountDescription>Нет в выбранной</S.AnotherStockCountDescription>
    }

    if (isDeliveryPrice && !isAvailable) {
        return (
            <S.PriceItemContainer>
                <S.PriceItem>
                    <S.ShipmentTitle onClick={clickHandlerDeliveryReasons}>
                        Доставка <S.DeliveryStatusTitle>невозможна</S.DeliveryStatusTitle>
                    </S.ShipmentTitle>
                </S.PriceItem>
            </S.PriceItemContainer>
        )
    }

    return (
        <S.PriceItemContainer>
            <S.PriceItem>
                {!isAvailable && isSelectedStock ? (
                    getAnotherStockDescription()
                ) : (
                    <S.ShipmentTitle>
                        {isDeliveryPrice ? 'Доставка' : 'Самовывоз'} {!price.isDemand && 'сегодня'}
                        {price.isDemand && <S.DateRed>{generateDate(price.aptEtaDateTime)}</S.DateRed>}
                    </S.ShipmentTitle>
                )}
                {price.withoutPromo ? (
                    <S.PriceBlock>
                        <S.PriceDataOld>
                            {price.withoutPromo.toLocaleString('RU-ru')} <SymbolRuble />
                        </S.PriceDataOld>
                        <S.PriceNumberSaleBold>
                            {price.normal.toLocaleString('RU-ru')} <SymbolRuble />
                        </S.PriceNumberSaleBold>
                    </S.PriceBlock>
                ) : (
                    <S.PriceBlock>
                        {!isAvailable && !isDeliveryPrice ? (
                            <S.PriceSumFrom onClick={clickOpenAnotherStockHandler}>
                                от {price.normal.toLocaleString('RU-ru')} <SymbolRuble />
                            </S.PriceSumFrom>
                        ) : (
                            <S.PriceSum>
                                {price.normal.toLocaleString('RU-ru')} <SymbolRuble />
                            </S.PriceSum>
                        )}
                    </S.PriceBlock>
                )}
            </S.PriceItem>
        </S.PriceItemContainer>
    )
}

const PriceView = (props) => {
    const { toggleStockSelectorByGood } = useContext(MenuContext)

    const clickOpenAnotherStockHandler = () => {
        toggleStockSelectorByGood([props.goodId])
    }

    const isDeliveryAvl = props.deliveryStatus?.isAvailableForDelivery || false
    const priceFroStock = props.isAvailableOnStock ? props.stockPrice : props.minPrice

    const [deliveryReasonState, setDeliveryReasonState] = useState({
        isShowedModal: false,
        reasons: []
    })
    const openDeliveryReasons = (reasons) => {
        setDeliveryReasonState({
            ...deliveryReasonState,
            isShowedModal: true,
            reasons
        })
    }
    const closeDeliveryReasons = () => {
        setDeliveryReasonState({
            ...deliveryReasonState,
            isShowedModal: false,
            reasons: []
        })
    }

    const clickHandlerDeliveryReasons = () => {
        openDeliveryReasons(
            props.deliveryStatus.reasons.map((reason) => ({
                id: reason.deliveryReasonID,
                text: reason.warningText
            }))
        )
    }
    return (
        <S.Container>
            {deliveryReasonState.isShowedModal && (
                <ModalWindow
                    title="Внимание"
                    isMobile
                    isButtonFooter={false}
                    isFooterOnlyOneButton
                    submit={{
                        text: 'Понятно',
                        clickHandler: closeDeliveryReasons
                    }}
                    cancel={{
                        text: 'Понятно',
                        clickHandler: closeDeliveryReasons
                    }}>
                    <S.ReasonContainer>
                        {deliveryReasonState.reasons.map((reason) => (
                            <S.ReasonTextItem
                                key={reason.id}
                                dangerouslySetInnerHTML={{ __html: reason.text }}
                            />
                        ))}
                    </S.ReasonContainer>
                </ModalWindow>
            )}

            {getPriceItem(
                props.isAvailableOnStock,
                false,
                priceFroStock,
                props.availableStocksCount,
                props.isSelectedStock,
                () => {},
                clickOpenAnotherStockHandler
            )}
            {getPriceItem(
                isDeliveryAvl,
                true,
                props.deliveryPrice,
                props.availableStocksCount,
                props.isSelectedStock,
                clickHandlerDeliveryReasons,
                clickOpenAnotherStockHandler
            )}
        </S.Container>
    )
}

PriceView.propTypes = {
    isSelectedStock: PropTypes.bool.isRequired,
    availableStocksCount: PropTypes.number,
    isAvailableOnStock: PropTypes.bool.isRequired,
    deliveryStatus: PropTypes.shape(DeliveryStatusType).isRequired,
    deliveryPrice: PropTypes.shape(PriceType).isRequired,
    stockPrice: PropTypes.shape(PriceType).isRequired,
    minPrice: PropTypes.shape(PriceType).isRequired
}

PriceView.defaultProps = {
    deliveryStatus: {
        reasons: []
    },
    availableStocksCount: 0
}

export default PriceView
