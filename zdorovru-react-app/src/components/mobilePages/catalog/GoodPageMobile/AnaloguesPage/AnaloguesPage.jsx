import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import MobilePageHeader from '@mobileShared/MobilePageHeader/MobilePageHeader'
import { getAnalogues } from '@api/siteApis'
import CatalogGoodItem from '@mobileComponents/catalog/CatalogGoodItem/CatalogGoodItem'
import { FixedPageContainer, MobileBody } from '@styles/ui-element.styled'
import MobileHeader from "@mobileShared/MobileHeader/MobileHeader";

const AnaloguesPage = (props) => {
    const [goods, setGoods] = useState([])
    useEffect(() => {
        const initGoods = async () => {
            const responseGoods = await getAnalogues(props.goodId, props.shipment)
            setGoods(responseGoods)
        }
        initGoods()
    }, [props.goodId, props.shipment])
    return (
        <FixedPageContainer>
            <MobileHeader backClickHandler={props.closeHandler} isDefaultRouterBack={false}>
                Аналоги
            </MobileHeader>
            <MobileBody>
                {goods.map((x) => (
                    <CatalogGoodItem shipment={props.shipment} key={x.webData.goodId}>
                        {x}
                    </CatalogGoodItem>
                ))}
            </MobileBody>
        </FixedPageContainer>
    )
}

AnaloguesPage.propTypes = {
    closeHandler: PropTypes.func.isRequired,
    goodId: PropTypes.number.isRequired,
    // eslint-disable-next-line react/forbid-prop-types
    shipment: PropTypes.object.isRequired
}

export default AnaloguesPage
