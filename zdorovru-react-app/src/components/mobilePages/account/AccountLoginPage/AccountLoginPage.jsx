import React, { useEffect, useRef } from 'react'
import { useAuthorize } from '@hooks/account/autorize-hook'
import MobilePageHeader from '@mobileShared/MobilePageHeader/MobilePageHeader'
import InputMask from 'react-input-mask'
import Input from '@shared/Input/Input'
import Button from '@shared/Button/Button'
import * as Theme from '@styles/theme-const'
import { MobileBody } from '@styles/ui-element.styled'
import PropTypes from 'prop-types'
import MobileWriteCodePage from '@components/Layout/MobileWriteCodePage/MobileWriteCodePage'
import * as S from './AccountLoginPage.styled'
import MobileHeader from "@mobileShared/MobileHeader/MobileHeader";

const AccountLoginPage = ({ closeHandler }) => {
    const authorize = useAuthorize(closeHandler)
    const inputPhoneRef = useRef(null)
    useEffect(() => {
        setTimeout(() => {
            if (inputPhoneRef.current) {
                inputPhoneRef.current.focus()
            }
        }, 100)
    }, [])

    if (authorize.isShowedSubmitSmsWindow) {
        return <MobileWriteCodePage authorize={authorize} />
    }

    return (
        <S.Container>
            <MobileHeader backClickHandler={closeHandler} isDefaultRouterBack={false}>
                Вход и регистрация
            </MobileHeader>
            <MobileBody>
                <S.Body>
                    <S.TitleDescription>Введите номер мобильного телефона</S.TitleDescription>
                    <S.InputPhoneContainer>
                        <InputMask
                            value={authorize.phoneNumber}
                            onChange={(e) => authorize.setPhoneNumber(e.target.value)}
                            isValid
                            type="tel"
                            autoFocus
                            mask="+7(999) 999 99 99">
                            <Input placeholderRaised="Мобильный телефон" autoFocus inputRef={inputPhoneRef} />
                        </InputMask>
                    </S.InputPhoneContainer>
                    {authorize.errorText && (
                        <S.CodeConfirmErrorBlock>{authorize.errorText}</S.CodeConfirmErrorBlock>
                    )}
                    <S.ButtonSubmitContainer>
                        {authorize.timer.isActive ? (
                            <S.TImeDescriptionBlock>
                                <S.TImeDescription>
                                    Вы недавно запрашивали код. Повторная отправка кода через{' '}
                                    {authorize.timer.seconds} сек
                                </S.TImeDescription>
                            </S.TImeDescriptionBlock>
                        ) : (
                            <Button
                                elementSizeTypeValue={Theme.ElementSizeType.large}
                                clickHandler={async () => {
                                    await authorize.submitPhone()
                                }}>
                                Получить код по СМС
                            </Button>
                        )}
                    </S.ButtonSubmitContainer>
                </S.Body>
            </MobileBody>
        </S.Container>
    )
}

AccountLoginPage.propTypes = {
    closeHandler: PropTypes
}

export default AccountLoginPage
