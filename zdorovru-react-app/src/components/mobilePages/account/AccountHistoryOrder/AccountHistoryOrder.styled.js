import styled from 'styled-components'

// eslint-disable-next-line import/prefer-default-export
export const Container = styled.div`
  display: flex;
  flex-direction: column;
  background-color: ${(props) => props.theme.colors.gray5};
  color: ${(props) => props.theme.colors.getGrayColor()};
`
export const OrdersContainer = styled.div`
    display: flex;
    flex-direction: column;
    box-sizing: border-box;
    & > div {
        margin: 0.5rem;
    }
`
export const Body = styled.div`
  display: flex;
  flex-direction: column;
  background-color: ${(props) => props.theme.colors.gray5};
  color: ${(props) => props.theme.colors.getGrayColor()};
`