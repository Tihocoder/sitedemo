import React, {useEffect, useState} from 'react'
import MobilePageHeader from '@mobileShared/MobilePageHeader/MobilePageHeader'
import {useRouter} from 'next/router'
import {getCustomerOrders} from '@api/siteApis'
import AccountHistoryOrderItem
    from '@mobilePages/account/AccountHistoryOrder/AccountHistoryOrderItem/AccountHistoryOrderItem'
import * as S from './AccountHistoryOrder.styled'
import {MobileBody} from '@styles/ui-element.styled'
import MobileHeader from "@mobileShared/MobileHeader/MobileHeader";

const AccountHistoryOrder = () => {
    const router = useRouter()
    const [orders, setOrders] = useState([])

    useEffect(() => {
        const initOrders = async () => {
            const response = await getCustomerOrders()
            setOrders(response)
        }
        initOrders()
    }, [])
    return (
        <S.Container>
            <MobileHeader>История заказов</MobileHeader>
            <MobileBody>
                <S.Body>
                    {orders.length ? (
                        <S.OrdersContainer>
                            {orders.map((order) => (
                                <AccountHistoryOrderItem key={order.id} order={order}/>
                            ))}
                        </S.OrdersContainer>
                    ) : (
                        <React.Fragment/>
                    )}
                </S.Body>
            </MobileBody>
        </S.Container>
    )
}

AccountHistoryOrder.propTypes = {}

export default AccountHistoryOrder
