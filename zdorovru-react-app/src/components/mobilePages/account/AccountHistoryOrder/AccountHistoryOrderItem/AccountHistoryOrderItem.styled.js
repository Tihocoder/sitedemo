import styled from 'styled-components'

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    padding: 0.5rem;
    background-color: white;
    border-radius: 8px;
    box-sizing: border-box;

    & > * {
        margin-bottom: 0.5rem;
    }

    & > *:last-child {
        margin-bottom: 0;
    }
`

export const TitleBlock = styled.div`
    display: flex;
    justify-content: space-between;
`
export const TitleFirst = styled.div``
export const SpanBold = styled.span`
    font-weight: bold;
    white-space: nowrap;
`
export const TitleLast = styled.div``

export const InfoBlock = styled.div`
    display: flex;
    flex-wrap: wrap;
    max-width: 40rem;
    width: 100%;
`
export const InfoItem = styled.div`
    display: flex;
    margin-right: 0.5rem;
    flex-flow: row wrap;
`

export const InfoItemTitle = styled.span`
    color: ${(props) => props.theme.colors.grayAkm40};
    margin-right: 0.3rem;
    white-space: nowrap;
`
export const InfoItemDescription = styled.span`
    color: ${(props) => props.theme.colors.getGrayColor()};
`

export const Footer = styled.div`
    display: flex;
    justify-content: space-between;
`

export const FooterButtonMenu = styled.div`
    display: flex;
    flex-wrap: wrap;

    & > button {
        //width: calc((100% / 2.8) - (1rem / 7));
        //flex: 1 1 calc((100% / 3) - 2rem);
        margin-right: 0.4rem;
        margin-bottom: 0.4rem;
    }

    margin-right: 1rem;
`

export const OrderSumBlock = styled.div`
    display: flex;
    font-size: 1.25rem;
    font-weight: 500;
    flex-wrap: nowrap;
    margin-right: 6px;
`

export const OrderSumTitle = styled.span`
    color: ${(props) => props.theme.colors.grayAkm40};
    margin-right: 6px;
`

export const OrderSum = styled.span``

export const StatusBlock = styled.div`
    display: flex;
    flex-direction: column;
    //min-width: 10rem;
`

export const ButtonsBlock = styled.div`
    display: flex;
    justify-content: space-between;
`
