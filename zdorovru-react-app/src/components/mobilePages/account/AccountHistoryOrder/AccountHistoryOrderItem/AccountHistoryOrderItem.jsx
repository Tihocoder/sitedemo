import React from 'react'
import PropTypes from 'prop-types'
import { generateDate } from '@helpers/site'
import SymbolRuble from '@shared/SymbolRuble/SymbolRuble'
import Button from '@shared/Button/Button'
import * as Theme from '@styles/theme-const'
import { useRouter } from 'next/router'
import { OrderType } from 'src/consts'
import * as S from './AccountHistoryOrderItem.styled'
import moment from 'moment'

const getInfoBLock = (order) => {
    if (!order.isDelivery) {
        return (
            <S.InfoBlock>
                <S.InfoItem>
                    <S.InfoItemTitle>Самовывоз из аптеки:</S.InfoItemTitle>
                    <S.InfoItemDescription>
                        {order.stockData.title}, {order.stockData.address}
                    </S.InfoItemDescription>
                </S.InfoItem>
                {order.state.isShowBuyDate && (
                    <S.InfoItem>
                        <S.InfoItemTitle>Получение</S.InfoItemTitle>
                        <S.InfoItemDescription>
                            {order.isDemand ? `с ${generateDate(order.lastAptEtaDatetime)}` : 'сегодня'}
                        </S.InfoItemDescription>
                    </S.InfoItem>
                )}
                <S.InfoItem>
                    <S.InfoItemTitle>Время работы</S.InfoItemTitle>
                    <S.InfoItemDescription>{order.stockData.timeString}</S.InfoItemDescription>
                </S.InfoItem>
                <S.InfoItem>
                    <S.InfoItemTitle>Бронь</S.InfoItemTitle>
                    <S.InfoItemDescription>
                        {moment(order.maxReserveDateTime).format('DD.MM.YYYY HH:mm')}
                    </S.InfoItemDescription>
                </S.InfoItem>
            </S.InfoBlock>
        )
    }
    return (
        <S.InfoBlock>
            <S.InfoItem>
                <S.InfoItemTitle>Доставка по адресу:</S.InfoItemTitle>
                <S.InfoItemDescription>{order.deliveryData.shipAddressText}</S.InfoItemDescription>
            </S.InfoItem>
            {order.deliveryData.timeSlotText && (
                <S.InfoItem>
                    <S.InfoItemTitle>Доставка</S.InfoItemTitle>
                    <S.InfoItemDescription>{order.deliveryData.timeSlotText}</S.InfoItemDescription>
                </S.InfoItem>
            )}
        </S.InfoBlock>
    )
}

const AccountHistoryOrderItem = (props) => {
    const router = useRouter()
    return (
        <S.Container>
            <S.TitleBlock>
                <S.TitleFirst>
                    № <S.SpanBold>{props.order.no}</S.SpanBold> от {generateDate(props.order.date)}
                </S.TitleFirst>
                <S.TitleLast>
                    <S.OrderSumBlock>
                        <S.OrderSumTitle>Сумма:</S.OrderSumTitle>
                        <S.OrderSum>
                            <S.SpanBold>
                                {props.order.sum?.toLocaleString('RU-ru') || ''} <SymbolRuble />
                            </S.SpanBold>
                        </S.OrderSum>
                    </S.OrderSumBlock>
                </S.TitleLast>
            </S.TitleBlock>
            {getInfoBLock(props.order)}
            <S.ButtonsBlock>
                {!props.isOrderMoreInfo && (
                    <Button
                        elementSizeTypeValue={Theme.ElementSizeType.regular}
                        colors={Theme.successButtonColorBlcackLight}
                        clickHandler={() => router.push(`/account/order/${props.order.id}`)}>
                        Подробнее
                    </Button>
                )}
            </S.ButtonsBlock>
        </S.Container>
    )
}

AccountHistoryOrderItem.propTypes = {
    order: PropTypes.shape(OrderType).isRequired,
    isOrderMoreInfo: PropTypes.bool.isRequired
}

export default AccountHistoryOrderItem
