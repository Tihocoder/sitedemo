import styled, { css } from 'styled-components'
import * as InputStyled from '@mobileShared/InputMobile/InputMobile.styled'

export const Container = styled.div`
    color: ${(props) => props.theme.colors.getGrayColor()};
    background-color: white;
    height: 100%;
    width: 100%;
    z-index: 6;

    ${InputStyled.Container} {
        height: 2rem;
    }
`

export const Body = styled.div`
    padding: 1.5rem;
    display: flex;
    flex-direction: column;
    align-items: center;

    & > * {
        margin-bottom: 1rem;
    }
`

export const TitleDescription = styled.div``
const validatedInput = css`
    border: 1px solid ${(props) => props.theme.colors.getGrayColor(0.1)};
    background-color: inherit;
`
const notValidatedInput = css`
    border: 1px solid ${(props) => props.theme.colors.accentGreen};
`
export const InputPhoneContainer = styled.div`
    width: 15rem;
    font-size: 1.5rem;
    font-weight: bold;

    & > input {
        width: 100%;
        padding: 0.7rem 1rem;
        border: 1px solid rgba(50, 62, 52, 0.1);
        color: ${(props) => props.theme.colors.getGrayColor(0.6)};
        box-sizing: border-box;
        outline: none;
        -webkit-appearance: none;
        border-radius: 7px;
        transition: all 0.35s ease;
        font-size: 1.5rem;
        font-weight: bold;

        ${(props) => (props.isValid ? validatedInput : notValidatedInput)}
        ::placeholder {
            color: ${(props) => props.theme.colors.getGrayColor(0.4)};
        }

        &:focus {
            color: ${(props) => props.theme.colors.getGrayColor(0.6)};
            box-shadow: ${(props) => props.theme.shadows.shadowKitHover};

            ::placeholder {
                transition: opacity 0.45s ease;
                color: ${(props) => props.theme.colors.getGrayColor(0.4)};
            }
        }
    }
`
export const CodeConfirmErrorBlock = styled.div`
    color: ${(props) => props.theme.colors.pureRed};
    text-align: center;
`

export const ButtonSubmitContainer = styled.div`
    & button {
        width: 20rem;
    }
`
export const TImeDescriptionBlock = styled.div`
    display: flex;
    align-items: center;
    flex-direction: column;
    justify-content: center;
    font-size: 0.75rem;
`

export const TImeDescription = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    font-size: 0.75rem;
`

export const CodeInputsBlock = styled.div`
    display: flex;
    width: 15rem;
    justify-content: space-between;

    & input {
        font-size: 1.5rem;
        font-weight: bold;
        width: 3.3rem;
        text-align: center;
    }
`
export const Input = styled.input`
    width: 100%;
    padding: 1.1rem 0.844rem 0.544rem 0.844rem;
    border: 1px solid ${(props) => props.theme.colors.accentGreen};
    color: ${(props) => props.theme.colors.getGrayColor(1)};
    box-sizing: border-box;
    outline: none;
    -webkit-appearance: none;
    border-radius: 7px;
    transition: all 0.35s ease;
    font: inherit;
    ${(props) =>
        props.borderColor
            ? props.isValid
                ? css`
                      border-color: ${props.borderColor};
                  `
                : notValidatedInput
            : props.isValid
            ? validatedInput
            : notValidatedInput};
`
export const ResetSms = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    font-size: 0.85rem;
    cursor: pointer;
    color: ${(props) => props.theme.colors.getBlue60Color(1)};
`
export const CodeConfirmPhone = styled.div`
    text-align: center;
    color: ${(props) => props.theme.colors.getGrayColor(1)};
`

export const CodeConfirmAgain = styled.div`
    cursor: pointer;
    text-align: center;
    color: ${(props) => props.theme.colors.getBlue60Color(1)};
`
