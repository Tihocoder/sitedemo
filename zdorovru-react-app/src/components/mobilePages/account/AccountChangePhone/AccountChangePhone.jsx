import React, { useEffect, useRef } from 'react'
import PropTypes from 'prop-types'
import * as S from '@mobilePages/account/AccountLoginPage/AccountLoginPage.styled'
import MobilePageHeader from '@mobileShared/MobilePageHeader/MobilePageHeader'
import { MobileBody } from '@styles/ui-element.styled'
import InputMask from 'react-input-mask'
import Input from '@shared/Input/Input'
import Button from '@shared/Button/Button'
import * as Theme from '@styles/theme-const'
import { useChangePhone } from '@hooks/account/autorize-hook'
import MobileWriteCodePage from '@components/Layout/MobileWriteCodePage/MobileWriteCodePage'
import { ContainerWindowPage } from '@mobilePages/account/AccountProfile/AccountProfile.styled'
import MobileHeader from "@mobileShared/MobileHeader/MobileHeader";

const AccountChangePhone = ({ closeHandler }) => {
    const changePhone = useChangePhone(closeHandler)
    const inputPhoneRef = useRef(null)
    useEffect(() => {
        inputPhoneRef.current.focus()
    }, [inputPhoneRef.current])

    if (changePhone.isShowedSubmitSmsWindow) {
        return <MobileWriteCodePage authorize={changePhone} />
    }
    return (
        <ContainerWindowPage>
            <MobileHeader backClickHandler={closeHandler} isDefaultRouterBack={false}>
                Изменить номер телефона
            </MobileHeader>
            <MobileBody>
                <S.Body>
                    <S.TitleDescription>Введите номер мобильного телефона</S.TitleDescription>
                    <S.InputPhoneContainer>
                        <InputMask
                            value={changePhone.phoneNumber}
                            onChange={(e) => changePhone.setPhoneNumber(e.target.value)}
                            isValid
                            type="tel"
                            mask="+7(999) 999 99 99">
                            <Input placeholderRaised="Мобильный телефон" inputRef={inputPhoneRef} autoFocus />
                        </InputMask>
                    </S.InputPhoneContainer>
                    {changePhone.errorText && (
                        <S.CodeConfirmErrorBlock>{changePhone.errorText}</S.CodeConfirmErrorBlock>
                    )}
                    <S.ButtonSubmitContainer>
                        {changePhone.timer.isActive ? (
                            <S.TImeDescriptionBlock>
                                <S.TImeDescription>
                                    Вы недавно запрашивали код. Повторная отправка кода через{' '}
                                    {changePhone.timer.seconds} сек
                                </S.TImeDescription>
                            </S.TImeDescriptionBlock>
                        ) : (
                            <Button
                                elementSizeTypeValue={Theme.ElementSizeType.large}
                                clickHandler={async () => {
                                    await changePhone.submitPhone()
                                }}>
                                Получить код по СМС
                            </Button>
                        )}
                    </S.ButtonSubmitContainer>
                </S.Body>
            </MobileBody>
        </ContainerWindowPage>
    )
}

AccountChangePhone.propTypes = {
    closeHandler: PropTypes
}

export default AccountChangePhone
