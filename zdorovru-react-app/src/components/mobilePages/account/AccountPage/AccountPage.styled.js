import styled from 'styled-components'

// eslint-disable-next-line import/prefer-default-export
export const Container = styled.div`
    background-color: white;
`
export const MenuContainer = styled.div`
    display: flex;
    flex-direction: column;
    padding-bottom: 5rem;
`
export const ChangeTitle = styled.span`
    font-size: 0.85rem;
    color: ${(props) => props.theme.colors.getGrayColor(0.5)};
`
export const SpanDataItem = styled.span`
    color: ${(props) => props.theme.colors.getGrayColor(1)};
`
export const BodyNavItemList = styled.div`
    display: flex;
    flex-direction: column;

    & button {
        margin-top: 0.5rem;
        padding: 0.9rem 1.2rem;
    }
`
export const NavItem = styled.a`
    font-size: 0.88rem;
    user-select: none;
    text-decoration: none;
    cursor: pointer;
    white-space: nowrap;
    color: ${(props) => props.theme.colors.getGrayColor(0.45)};

    :hover {
        color: ${(props) => props.theme.colors.getGreenColor(1)};
    }

    list-style-type: none;
`
