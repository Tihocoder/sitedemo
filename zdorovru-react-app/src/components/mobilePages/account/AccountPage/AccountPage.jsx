import React, { useContext, useState } from 'react'
import MobilePageHeader from '@mobileShared/MobilePageHeader/MobilePageHeader'
import MobileRowWithIconItem from '@mobileShared/MobileRowWithIconItem/MobileRowWithIconItem'
import { useDispatch, useSelector } from 'react-redux'
import PropTypes from 'prop-types'
import { useRouter } from 'next/router'
import MobileStockSelector from '@mobilePages/MobileStockSelector/MobileStockSelector'
import MobileAddressSelector from '@mobilePages/MobileAddressSelector/MobileAddressSelector'
import MobileCitySelector from '@mobilePages/MobileCitySelector/MobileCitySelector'
import { changeShipment } from '@actions/customer'
import ModalWindow from '@shared/ModalWindow/ModalWindow'
import { MobileBody, MobileBodyModal } from '@styles/ui-element.styled'
import { AlertsStateContext } from '@context/AlertsState'
import { useModalSubmit } from '@hooks/useModalInfo'
import Button from '@shared/Button/Button'
import * as Theme from '@styles/theme-const'
import * as S from './AccountPage.styled'
import MobileHeader from "@mobileShared/MobileHeader/MobileHeader";

const AccountPage = (props) => {
    const router = useRouter()
    const dispatch = useDispatch()
    const cities = useSelector((state) => state.company.cities)
    const shipment = useSelector((state) => state.customer.shipment)
    const customModal = useModalSubmit()
    const { confirmCustomer } = useContext(AlertsStateContext)
    // eslint-disable-next-line no-shadow
    const city = cities.find((city) => city.id === shipment.cityId)

    const [isStockSelector, setIsStockSelector] = useState(false)
    const [isShipAddressSelector, setIsShipAddressSelector] = useState(false)
    const [isCitySelector, setCitySelector] = useState(false)
    const [isCompanyInfo, setIsCompanyInfo] = useState(false)
    const toggleIsStockSelector = () => setIsStockSelector(!isStockSelector)
    const toggleIsShipAddressSelector = () => setIsShipAddressSelector(!isShipAddressSelector)
    const toggleIsCitySelector = () => setCitySelector(!isCitySelector)
    const toggleIsCompanyInfo = () => setIsCompanyInfo(!isCompanyInfo)

    const changeSelectedCity = async (id) => {
        if (shipment.cityId !== id) {
            await dispatch(changeShipment({ cityId: id, stockId: 0, shipAddressId: 0 }))
            toggleIsCitySelector()
            router.reload()
        } else {
            toggleIsCitySelector()
        }
    }

    const showModalNeedConfirm = async (text) => {
        customModal.showModalInfo(
            <MobileBodyModal>{text}</MobileBodyModal>,
            'Войти в кабинет',
            () => {
                confirmCustomer.toggleClose()
                customModal.closeModalInfo()
            },
            'Отмена',
            customModal.closeModalInfo
        )
    }

    const clickProfileHandler = async () => {
        if (!confirmCustomer.isEnabled) {
            confirmCustomer.toggleClose()
        } else {
            await router.push('/account/profile')
        }
    }
    const clickHistoryHandler = async () => {
        if (!confirmCustomer.isEnabled) {
            await showModalNeedConfirm('Для просмотра моих заказов войдите в личный кабинет')
        } else {
            await router.push('/account/history')
        }
    }

    const clickPurchasedHandler = async () => {
        if (!confirmCustomer.isEnabled) {
            await showModalNeedConfirm('Для просмотра купленных товаров войдите в личный кабинет')
        } else {
            await router.push('/account/purchased')
        }
    }

    const clickLikedHandler = async () => {
        if (!confirmCustomer.isEnabled) {
            await showModalNeedConfirm('Для просмотра избранных товаров войдите в личный кабинет')
        } else {
            await router.push('/catalog/liked')
        }
    }

    return (
        <S.Container>
            {isStockSelector && <MobileStockSelector closeHandler={toggleIsStockSelector} />}
            {isShipAddressSelector && (
                <MobileAddressSelector
                    closeHandler={toggleIsShipAddressSelector}
                    shipAddressId={shipment.shipAddressId}
                />
            )}
            {isCitySelector && (
                <MobileCitySelector
                    changeCityId={changeSelectedCity}
                    closeHandler={toggleIsCitySelector}
                    cityId={shipment.cityId}
                />
            )}
            {isCompanyInfo && (
                <ModalWindow
                    title="Юридическая информация"
                    isMobile
                    isButtonFooter={false}
                    cancel={{
                        text: 'Отмена',
                        clickHandler: toggleIsCompanyInfo
                    }}>
                    <S.BodyNavItemList>
                        <Button
                            elementSizeTypeValue={Theme.ElementSizeType.regular}
                            clickHandler={async () => {
                                await router.push('/privacy-policy')
                            }}>
                            Политика конфиденциальности
                        </Button>
                        <Button
                            elementSizeTypeValue={Theme.ElementSizeType.regular}
                            clickHandler={async () => {
                                await router.push('/terms-of-use')
                            }}>
                            Пользовательское соглашение
                        </Button>
                        <Button
                            elementSizeTypeValue={Theme.ElementSizeType.regular}
                            clickHandler={toggleIsCompanyInfo}>
                            Отмена
                        </Button>
                    </S.BodyNavItemList>
                </ModalWindow>
            )}
            <MobileHeader>Личный кабинет</MobileHeader>
            <MobileBody>
                <S.MenuContainer>
                    <MobileRowWithIconItem iconName="user" clickHandler={clickProfileHandler} isShowedRow>
                        {confirmCustomer.isEnabled ? (
                            <S.SpanDataItem>
                                {props.profile?.phone
                                    ? props.profile?.phone.replace(
                                          /(\d{1})(\d{3})(\d{3})(\d{2})(\d{2})/,
                                          '+$1 ($2) $3-$4-$5'
                                      )
                                    : 'Контактные данные'}
                            </S.SpanDataItem>
                        ) : (
                            <S.SpanDataItem>Войти в личный кабинет</S.SpanDataItem>
                        )}
                        {confirmCustomer.isEnabled && props.profile?.phone && (
                            <S.ChangeTitle>Изменить мои данные</S.ChangeTitle>
                        )}
                    </MobileRowWithIconItem>
                    <MobileRowWithIconItem iconName="compass" isShowedRow clickHandler={toggleIsCitySelector}>
                        <S.SpanDataItem>{city?.title || 'Москва и МО'}</S.SpanDataItem>
                        <S.ChangeTitle>Изменить регион</S.ChangeTitle>
                    </MobileRowWithIconItem>
                    <MobileRowWithIconItem iconName="geo" isShowedRow clickHandler={toggleIsStockSelector}>
                        <S.SpanDataItem>{shipment.stockTitle || 'Не выбрана'}</S.SpanDataItem>
                        <S.ChangeTitle>Выбрать аптеку самовывоза</S.ChangeTitle>
                    </MobileRowWithIconItem>
                    <MobileRowWithIconItem
                        iconName="auto"
                        isShowedRow
                        clickHandler={toggleIsShipAddressSelector}>
                        <S.SpanDataItem>{shipment.shipAddressTitle || 'Не выбран'}</S.SpanDataItem>
                        <S.ChangeTitle>Изменить адрес доставки</S.ChangeTitle>
                    </MobileRowWithIconItem>
                    <MobileRowWithIconItem
                        iconName="basket-gray"
                        isShowedRow
                        clickHandler={clickHistoryHandler}>
                        Мои заказы
                    </MobileRowWithIconItem>
                    <MobileRowWithIconItem
                        iconName="archive"
                        isShowedRow
                        clickHandler={clickPurchasedHandler}>
                        Купленные товары
                    </MobileRowWithIconItem>
                    <MobileRowWithIconItem
                        iconName="favorite_border"
                        isShowedRow
                        clickHandler={clickLikedHandler}>
                        Мои избранные
                    </MobileRowWithIconItem>
                    <MobileRowWithIconItem
                        iconName="percent"
                        isShowedRow
                        clickHandler={() => router.push('/catalog/promotion/list')}>
                        Скидки и акции
                    </MobileRowWithIconItem>
                    <MobileRowWithIconItem
                        iconName="mobile-room"
                        isShowedRow
                        clickHandler={() => router.push('/condition/pickup')}>
                        Условия самовывоза
                    </MobileRowWithIconItem>
                    <MobileRowWithIconItem
                        iconName="local_shipping"
                        isShowedRow
                        clickHandler={() => router.push('/condition/delivery')}>
                        Условия доставки
                    </MobileRowWithIconItem>
                    <MobileRowWithIconItem
                        iconName="work_outline"
                        isShowedRow
                        clickHandler={toggleIsCompanyInfo}>
                        Юридическая информация
                    </MobileRowWithIconItem>
                    <MobileRowWithIconItem
                        iconName="account_balance"
                        isShowedRow
                        clickHandler={() => router.push('/company/about')}>
                        О компании
                    </MobileRowWithIconItem>
                    <MobileRowWithIconItem
                        iconName="insert_comment"
                        isShowedRow
                        clickHandler={() => router.push('/contact/feedback')}>
                        Обратная связь
                    </MobileRowWithIconItem>
                </S.MenuContainer>
            </MobileBody>
            {customModal.modalInfo.isShowed && (
                <ModalWindow
                    title="Внимание"
                    isMobile
                    submit={{
                        text: customModal.modalInfo.buttonTitle,
                        clickHandler: customModal.modalInfo.buttonHandler
                    }}
                    cancel={{
                        text: customModal.modalInfo.cancelTitle,
                        clickHandler: customModal.closeModalInfo
                    }}>
                    {customModal.modalInfo.content}
                </ModalWindow>
            )}
        </S.Container>
    )
}

AccountPage.propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    profile: PropTypes.any.isRequired
}

export default AccountPage
