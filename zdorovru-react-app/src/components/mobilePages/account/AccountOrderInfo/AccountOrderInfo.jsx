import React from 'react'
import MobilePageHeader from '@mobileShared/MobilePageHeader/MobilePageHeader'
import PropTypes from 'prop-types'
// eslint-disable-next-line import/named
import { OrderType } from 'src/consts'
import { useOrderDetails } from '@hooks/account/order-details-hook'
import CatalogGoodContainer from '@mobileComponents/catalog/CatalogGoodContainer/CatalogGoodContainer'
import AccountHistoryOrderItem from '@mobilePages/account/AccountHistoryOrder/AccountHistoryOrderItem/AccountHistoryOrderItem'
import * as S from './AccountOrderInfo.styled'
import { MobileBody } from '@styles/ui-element.styled'
import MobileHeader from "@mobileShared/MobileHeader/MobileHeader";

const AccountOrderInfo = (props) => {
    const orderInfo = useOrderDetails(props.order.id)
    return (
        <S.Container>
            <MobileHeader>
                Заказ № <S.SpanBold>{props.order.no}</S.SpanBold>
            </MobileHeader>
            <MobileBody>
                <S.BlockGrayLine>
                    <AccountHistoryOrderItem order={props.order} isOrderMoreInfo />
                </S.BlockGrayLine>
                {orderInfo.goods.length && (
                    <CatalogGoodContainer shipment={orderInfo.shipment}>
                        {orderInfo.goods}
                    </CatalogGoodContainer>
                )}
            </MobileBody>
        </S.Container>
    )
}

AccountOrderInfo.propTypes = {
    order: PropTypes.shape(OrderType).isRequired
}

export default AccountOrderInfo
