import styled from 'styled-components'

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    color: ${(props) => props.theme.colors.getGrayColor()};
`
export const SpanBold = styled.span`
    font-weight: bold;
`
export const BlockGrayLine = styled.div`
    border-bottom: 2px solid ${(props) => props.theme.colors.getGrayColor(0.1)};
`
