// eslint-disable-next-line import/no-duplicates
import React, { useRef, useState } from 'react'
import BlockWithTitle from '@mobileComponents/BlockWithTitle/BlockWithTitle'
import PropTypes from 'prop-types'
import MobileRowWithIconItem from '@mobileShared/MobileRowWithIconItem/MobileRowWithIconItem'
import { useProfile } from '@hooks/account/profile-hook'
import PageWithSelectorItems from '@mobileShared/PageWithSelectorItems/PageWithSelectorItems'
import MobilePageHeader from '@mobileShared/MobilePageHeader/MobilePageHeader'
import { useRouter } from 'next/router'
import AccountProfileEmail from '@mobilePages/account/AccountProfile/AccountProfileEmail/AccountProfileEmail'
import AccountProfileBirthdayChanger from '@mobilePages/account/AccountProfile/AccountProfileBirthdayChanger/AccountProfileBirthdayChanger'
// eslint-disable-next-line no-unused-vars
import { MobileBody } from '@styles/ui-element.styled'
import MobileRowItem from '@mobileShared/MobileRowItem/MobileRowItem'
import { destroyCookie } from 'nookies'
import ModalWindow from '@shared/ModalWindow/ModalWindow'
import AccountProfileFullName from '@mobilePages/account/AccountProfile/AccountProfileFullName/AccountProfileFullName'
import moment from 'moment'
import AccountProfileMobile from '@mobilePages/account/AccountProfile/AccountProfileMobile/AccountProfileMobile'
import AccountChangePhone from '@mobilePages/account/AccountChangePhone/AccountChangePhone'
import MobileHeader from '@mobileShared/MobileHeader/MobileHeader'
import { useDispatch } from 'react-redux'
import { clearLikedGoods } from '@actions/customer'
import * as S from './AccountProfile.styled'

const AccountProfile = (props) => {
    const profile = useProfile(props.profile, props.genderTypes)
    const router = useRouter()
    const dispatch = useDispatch()
    const [isShowedGenderSelector, setIsShowedGenderSelector] = useState(false)
    const [isShowedEmailChange, setIsShowedEmailChange] = useState(false)
    const [isShowedFullNameChanger, setIsShowedFullNameChanger] = useState(false)
    const [isShowedBirthdayChanger, setIsShowedBirthdayChanger] = useState(false)
    const [isShowedMessageExit, setIsShowedMessageExit] = useState(false)
    const [isShowedProfileMobile, setIsShowedProfileMobile] = useState(false)
    const [isShowedProfileChangePhone, setIsShowedProfileChangePhone] = useState(false)
    const toggleIsShowedGenderSelector = () => setIsShowedGenderSelector(!isShowedGenderSelector)
    const toggleIsShowedEmailChanger = () => setIsShowedEmailChange(!isShowedEmailChange)
    const toggleIsShowedFullNameChanger = () => setIsShowedFullNameChanger(!isShowedFullNameChanger)
    const toggleIsShowedBirthdayChanger = () => setIsShowedBirthdayChanger(!isShowedBirthdayChanger)
    const toggleIsShowedProfileMobile = () => setIsShowedProfileMobile(!isShowedProfileMobile)
    const toggleIsShowedProfileChangePhone = () => setIsShowedProfileChangePhone(!isShowedProfileChangePhone)
    // eslint-disable-next-line no-unused-vars
    const birthdayRef = useRef(null)
    const getGenderTypes = () =>
        props.genderTypes.map((x) => ({
            id: x.typeId,
            title: x.text
        }))

    const closeHandler = async () => {
        if (props.custom.isCustom) {
            await props.custom.closeHandler()
            return
        }
        await router.push('/account')
    }
    const fullName = `${profile.state.lastName || ''} ${profile.state.firstName || ''} ${
        profile.state.middleName || ''
    }`.trim()
    const isName = !!fullName
    moment.locale('ru')
    const genderCloaseAndRollback = () => {
        profile.rollbackStateProfile()
        toggleIsShowedGenderSelector()
    }
    const profileFullName = `${props.profile.lastName} ${props.profile.firstName} ${props.profile.lastName}`
    const stateFullName = `${profile.state.lastName} ${profile.state.firstName} ${profile.state.lastName}`
    return (
        <S.Container>
            {isShowedGenderSelector && (
                <PageWithSelectorItems
                    headerTitle="Выберите пол"
                    items={getGenderTypes()}
                    selectedItemId={profile.state.gender}
                    selectedHandler={(value) => profile.changeMobileHandler('gender', value)}
                    closeHandler={genderCloaseAndRollback}
                    isSubmitHandler
                    isCloseWithSelect={false}
                    submitHandler={profile.submitHandlerNew}
                    isChanged={profile.state.gender !== props.profile.gender}
                    isShowArrowBack
                />
            )}
            {isShowedMessageExit && (
                <ModalWindow
                    title="Внимание!"
                    isMobile
                    submit={{
                        text: 'Выйти из аккаунта',
                        clickHandler: async () => {
                            destroyCookie({}, 'customer_token', { path: '/' })
                            await dispatch(clearLikedGoods())
                            await router.push('/account')
                            await router.reload()
                        }
                    }}
                    cancel={{
                        text: 'Отмена',
                        clickHandler: () => setIsShowedMessageExit(false)
                    }}>
                    <S.BodyModalContainer>Вы уверены, что хотите выйти из аккаунта?</S.BodyModalContainer>
                </ModalWindow>
            )}
            {isShowedProfileMobile && (
                <AccountProfileMobile
                    changePhone={() => {
                        toggleIsShowedProfileChangePhone()
                        toggleIsShowedProfileMobile()
                    }}
                    closeHandler={toggleIsShowedProfileMobile}
                />
            )}
            {isShowedProfileChangePhone && (
                <AccountChangePhone closeHandler={toggleIsShowedProfileChangePhone} />
            )}
            {isShowedEmailChange && (
                <AccountProfileEmail
                    changeHandler={(value) => profile.changeMobileHandler('email', value)}
                    closeHandler={toggleIsShowedEmailChanger}
                    rollbackStateProfile={profile.rollbackStateProfile}
                    submitHandler={profile.submitHandlerNew}
                    isChanged={profile.state.email !== props.profile.email}
                    value={profile.state.email}
                />
            )}
            {isShowedFullNameChanger && (
                <AccountProfileFullName
                    profile={profile}
                    closeHandler={toggleIsShowedFullNameChanger}
                    isChanged={profileFullName !== stateFullName}
                />
            )}
            {isShowedBirthdayChanger && (
                <AccountProfileBirthdayChanger
                    changeHandler={(value) => profile.changeMobileHandler('birthday', value)}
                    closeHandler={toggleIsShowedBirthdayChanger}
                    value={profile.state.birthday}
                    isValid={profile.validate.birthday}
                    isChanged={
                        moment(props.profile.birthday).format('DD.MM.YYYY') !==
                        moment(profile.state.birthday, 'DD.MM.YYYY').format('DD.MM.YYYY')
                    }
                    rollbackStateProfile={profile.rollbackStateProfile}
                    submitHandler={profile.submitHandlerNew}
                />
            )}
            <MobileHeader backClickHandler={closeHandler} isShowArrowBack>
                Мои данные
            </MobileHeader>
            <MobileBody>
                <BlockWithTitle title="Персональная информация">
                    <MobileRowWithIconItem
                        clickHandler={toggleIsShowedProfileMobile}
                        iconName="phone_android"
                        isShowedRow>
                        <S.SpanDataItem>
                            {profile.state.phone
                                ? profile.state.phone.replace(
                                      /(\d{1})(\d{3})(\d{3})(\d{2})(\d{2})/,
                                      '+$1 ($2) $3-$4-$5'
                                  )
                                : 'Контактные данные'}
                        </S.SpanDataItem>
                        {profile.state.phone && <S.ChangeTitle>Изменить номер телефона</S.ChangeTitle>}
                    </MobileRowWithIconItem>
                    <MobileRowWithIconItem
                        clickHandler={toggleIsShowedEmailChanger}
                        iconName="alternate_email"
                        isShowedRow>
                        <S.SpanDataItem>
                            {profile.state.email ? profile.state.email : 'E-mail'}
                        </S.SpanDataItem>
                        {profile.state.email && <S.ChangeTitle>Изменить e-mail</S.ChangeTitle>}
                    </MobileRowWithIconItem>
                    <MobileRowWithIconItem
                        clickHandler={toggleIsShowedFullNameChanger}
                        iconName="user-create-order"
                        isShowedRow>
                        <S.SpanDataItem>{isName ? fullName : 'Не установлено'}</S.SpanDataItem>
                        <S.ChangeTitle>Изменить ФИО</S.ChangeTitle>
                    </MobileRowWithIconItem>
                    <MobileRowWithIconItem
                        clickHandler={toggleIsShowedGenderSelector}
                        iconName="wc"
                        isShowedRow>
                        <S.SpanDataItem>{profile.genderTitle || 'Выберите ваш пол'}</S.SpanDataItem>
                        <S.ChangeTitle>Пол</S.ChangeTitle>
                    </MobileRowWithIconItem>
                    <MobileRowWithIconItem
                        clickHandler={toggleIsShowedBirthdayChanger}
                        iconName="calendar"
                        isShowedRow>
                        <S.SpanDataItem>
                            {profile.state.birthday
                                ? moment(profile.state.birthday, 'DD.MM.YYYY').format('ll')
                                : 'Не установлено'}
                        </S.SpanDataItem>
                        <S.ChangeTitle>Дата рождения</S.ChangeTitle>
                    </MobileRowWithIconItem>
                </BlockWithTitle>
                <BlockWithTitle title=" ">
                    <MobileRowItem clickHandler={async () => setIsShowedMessageExit(true)}>
                        <S.SpanRed>Выйти из аккаунта</S.SpanRed>
                    </MobileRowItem>
                </BlockWithTitle>
            </MobileBody>
        </S.Container>
    )
}

AccountProfile.propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    profile: PropTypes.object.isRequired,
    // eslint-disable-next-line react/forbid-prop-types
    genderTypes: PropTypes.any.isRequired,
    custom: {
        isCustom: PropTypes.bool,
        closeHandler: PropTypes.func
    }
}

AccountProfile.defaultProps = {
    custom: {
        isCustom: false,
        closeHandler: () => {}
    }
}

export default AccountProfile
