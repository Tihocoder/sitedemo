import styled from 'styled-components'

export const BodyModalContainer = styled.div`
    display: flex;
    flex-direction: column;
    max-width: 32rem;
    font-size: 1rem;
`

export const P = styled.p`
    text-indent: 1rem;
    margin-bottom: 1rem;
`
export const PRed = styled.p`
    margin-bottom: 1rem;
    color: ${(props) => props.theme.colors.getRedheadColor()};
`
export const ButtonContainer = styled.div`
    display: flex;
    flex-direction: column;

    & button {
        margin-top: 0.5rem;
        padding: 0.8rem 1.2rem;
    }
`
