import React, { useContext, useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import MobilePageHeader from '@mobileShared/MobilePageHeader/MobilePageHeader'
import { ContainerWindowPage, GrayBlock } from '@mobilePages/account/AccountProfile/AccountProfile.styled'
import { MobileBody } from '@styles/ui-element.styled'
import ModalWindow from '@shared/ModalWindow/ModalWindow'
import Button from '@shared/Button/Button'
import * as Theme from '@styles/theme-const'
import { AlertsStateContext } from '@context/AlertsState'
import * as S from './AccountProfileMobile.styled'
import MobileHeader from "@mobileShared/MobileHeader/MobileHeader";

const AccountProfileMobile = ({ closeHandler, changePhone }) => {
    // eslint-disable-next-line no-unused-vars
    const [modalState, setModalState] = useState({
        state: 1
    })
    const { confirmCustomer } = useContext(AlertsStateContext)
    useEffect(
        () => () => {
            setModalState({ state: 1 })
        },
        []
    )
    return (
        <ContainerWindowPage>
            <MobileHeader backClickHandler={closeHandler} isDefaultRouterBack={false}>
                Изменить номер телефона
            </MobileHeader>
            <MobileBody>
                <GrayBlock />
            </MobileBody>
            {modalState.state === 1 ? (
                <ModalWindow
                    title="Внимание"
                    isButtonFooter={false}
                    isMobile
                    footer={
                        <S.ButtonContainer>
                            <Button
                                elementSizeTypeValue={Theme.ElementSizeType.regular}
                                colors={Theme.successButtonColor}
                                clickHandler={confirmCustomer.toggleClose}>
                                Войти в личный кабинет под другим номером
                            </Button>
                            <Button
                                elementSizeTypeValue={Theme.ElementSizeType.regular}
                                colors={Theme.errorButtonColors}
                                clickHandler={() => {
                                    setModalState({
                                        state: 2
                                    })
                                }}>
                                Переместить все заказы на новый номер
                            </Button>
                            <Button
                                elementSizeTypeValue={Theme.ElementSizeType.regular}
                                colors={Theme.successButtonColorBlcackLight}
                                clickHandler={closeHandler}>
                                Отмена
                            </Button>
                        </S.ButtonContainer>
                    }
                    cancel={{
                        text: 'Отмена',
                        clickHandler: closeHandler
                    }}>
                    <S.BodyModalContainer>
                        <S.P>
                            Если Вы хотите войти в другой личный кабинет с помощью другого номера телефона,
                            нажмите “Войти в личный кабинет под другим номером”
                        </S.P>
                        <S.P>
                            Если Вы хотите сменить телефонный номер текущего личного кабинета (например, при
                            смене сим-карты), нажмите “Переместить все заказы на новый номер”.
                        </S.P>
                    </S.BodyModalContainer>
                </ModalWindow>
            ) : (
                <React.Fragment />
            )}
            {modalState.state === 2 ? (
                <ModalWindow
                    title="Внимание"
                    isButtonFooter={false}
                    isMobile
                    footer={
                        <S.ButtonContainer>
                            <Button
                                elementSizeTypeValue={Theme.ElementSizeType.regular}
                                colors={Theme.errorButtonColors}
                                clickHandler={changePhone}>
                                Переместить все заказы на новый номер
                            </Button>
                            <Button
                                elementSizeTypeValue={Theme.ElementSizeType.regular}
                                colors={Theme.successButtonColor}
                                clickHandler={() => {
                                    setModalState({
                                        state: 1
                                    })
                                }}>
                                Отмена
                            </Button>
                        </S.ButtonContainer>
                    }
                    cancel={{
                        text: 'Отмена',
                        clickHandler: () => {
                            setModalState({
                                state: 1
                            })
                        }
                    }}>
                    <S.BodyModalContainer>
                        <S.P>Вы уверены, что хотите перенести все заказы на новый номер?</S.P>
                        <S.P>
                            Обращаем внимание, что все заказы будут удалены со старого номера и перемещены на
                            новый.
                        </S.P>
                        <S.PRed>Это действие является необратимым.</S.PRed>
                    </S.BodyModalContainer>
                </ModalWindow>
            ) : (
                <React.Fragment />
            )}
        </ContainerWindowPage>
    )
}

AccountProfileMobile.propTypes = {
    closeHandler: PropTypes.func.isRequired,
    changePhone: PropTypes.func.isRequired
}

export default AccountProfileMobile
