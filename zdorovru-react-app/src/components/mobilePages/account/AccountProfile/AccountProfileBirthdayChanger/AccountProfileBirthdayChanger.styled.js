import styled, { css } from 'styled-components'

// eslint-disable-next-line import/prefer-default-export
export const Container = styled.div`
    background-color: white;
    position: fixed;
    height: 100%;
    width: 100%;
    top: 0;
    left: 0;
    z-index: 20;
`
export const ButtonContainer = styled.div`
    margin-top: 3.125rem;
    padding: 0 0.5rem;

    & > button {
        width: 100%;
    }
`
export const BirthdayInputContainer = styled.div`
    position: relative;
    display: flex;
    align-items: center;
    padding: 0.5rem 1rem;
    height: 2rem;
    border-bottom: 1px solid ${(props) => props.theme.colors.getGrayColor(0.1)};

    & > input:focus ~ label,
    input:not(:focus):valid ~ label {
        top: 0.1rem;
        bottom: 0.5rem;
        left: 1rem;
        font-size: 0.75rem;
        opacity: 1;
        height: 0.9rem;
    }

    & input {
        font: inherit;
        background: transparent;
        border: none;
        outline: none;
        padding: 0;
        margin: 0;
        height: 100%;
        width: 100%;
        max-width: 100%;
        vertical-align: bottom;
        text-align: inherit;
        box-sizing: content-box;
        -webkit-appearance: none;

        &::placeholder {
            color: ${(props) => props.theme.colors.getGrayColor(0.5)};
        }

        ${(props) =>
            !props.isValid &&
            css`
                color: ${props.theme.colors.getRedheadColor()};

                &::placeholder {
                    color: ${props.theme.colors.getRedheadColor()};
                }
            `}
    }
`
