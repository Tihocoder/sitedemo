import React from 'react'
import PropTypes from 'prop-types'
import MobilePageHeader from '@mobileShared/MobilePageHeader/MobilePageHeader'
import { InputsMobileBlock } from '@mobileShared/InputMobile/InputMobile.styled'
import { MobileBody } from '@styles/ui-element.styled'
import Cleave from 'cleave.js/react'
import Button from '@shared/Button/Button'
import * as Theme from '@styles/theme-const'
import { successButtonLight } from '@styles/theme-const'
import MobileHeader from '@mobileShared/MobileHeader/MobileHeader'
import * as S from './AccountProfileBirthdayChanger.styled'

const AccountProfileBirthdayChanger = ({
    closeHandler,
    changeHandler,
    value,
    submitHandler,
    isValid,
    rollbackStateProfile,
    isChanged
}) => {
    const submitAndCloseHandler = async () => {
        await submitHandler()
        closeHandler()
    }
    const closeAndRollBack = () => {
        rollbackStateProfile()
        closeHandler()
    }
    return (
        <S.Container>
            <MobileHeader backClickHandler={closeAndRollBack} isDefaultRouterBack={false}>
                Введите вашу Дату рождения
            </MobileHeader>
            <MobileBody>
                <InputsMobileBlock>
                    <S.BirthdayInputContainer isValid={isValid}>
                        <Cleave
                            placeholder="Дата рождения"
                            value={value}
                            type="tel"
                            onChange={(e) => changeHandler(e.target.value)}
                            options={{
                                date: true,
                                delimiter: '.',
                                numericOnly: true,
                                datePattern: ['d', 'm', 'Y']
                            }}
                        />
                    </S.BirthdayInputContainer>
                </InputsMobileBlock>
                <S.ButtonContainer>
                    <Button
                        elementSizeTypeValue={Theme.ElementSizeType.large}
                        clickHandler={isChanged ? submitAndCloseHandler : closeAndRollBack}>
                        Сохранить изменения
                    </Button>
                </S.ButtonContainer>
            </MobileBody>
        </S.Container>
    )
}

AccountProfileBirthdayChanger.propTypes = {
    closeHandler: PropTypes.func.isRequired,
    changeHandler: PropTypes.func.isRequired,
    value: PropTypes.string.isRequired,
    submitHandler: PropTypes.func,
    rollbackStateProfile: PropTypes.func.isRequired,
    isValid: PropTypes.bool.isRequired,
    isChanged: PropTypes.bool
}
AccountProfileBirthdayChanger.defaultProps = {
    submitHandler: () => {},
    isChanged: false
}

export default AccountProfileBirthdayChanger
