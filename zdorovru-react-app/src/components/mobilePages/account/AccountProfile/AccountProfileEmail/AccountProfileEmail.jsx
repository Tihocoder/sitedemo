import React, { useEffect, useRef, useState } from 'react'
import PropTypes from 'prop-types'
import MobilePageHeader from '@mobileShared/MobilePageHeader/MobilePageHeader'
import { InputsMobileBlock } from '@mobileShared/InputMobile/InputMobile.styled'
import InputMobile from '@mobileShared/InputMobile/InputMobile'
import { MobileBody } from '@styles/ui-element.styled'
import BlockWithTitle from '@mobileComponents/BlockWithTitle/BlockWithTitle'
import Button from '@shared/Button/Button'
import * as Theme from '@styles/theme-const'
import { successButtonLight } from '@styles/theme-const'
import MobileHeader from '@mobileShared/MobileHeader/MobileHeader'
import * as S from './AccountProfileEmail.styled'

const AccountProfileEmail = ({
    closeHandler,
    changeHandler,
    value,
    submitHandler,
    rollbackStateProfile,
    isChanged
}) => {
    const blockHeaderRef = useRef(null)
    const [heightHeader, setHeightHeader] = useState(null)
    useEffect(() => {
        if (blockHeaderRef?.current?.getBoundingClientRect().height !== heightHeader)
            setHeightHeader(blockHeaderRef?.current?.getBoundingClientRect().height)
    }, [blockHeaderRef?.current?.clientHeight])
    const submitAndCloseHandler = async () => {
        await submitHandler()
        closeHandler()
    }
    const closeAndRollBack = () => {
        rollbackStateProfile()
        closeHandler()
    }
    return (
        <S.Container>
            <MobileHeader
                backClickHandler={closeAndRollBack}
                blockRef={blockHeaderRef}
                isDefaultRouterBack={false}>
                Изменить e-mail
            </MobileHeader>
            <MobileBody customPixel={heightHeader}>
                <S.GrayBlock>
                    <BlockWithTitle title="Введите e-mail">
                        <InputsMobileBlock>
                            <InputMobile
                                changeHandler={changeHandler}
                                value={value}
                                placeholder="E-mail"
                                customInputProps={{ type: 'email' }}
                            />
                        </InputsMobileBlock>
                    </BlockWithTitle>

                    <S.ButtonContainer>
                        {isChanged ? (
                            <Button
                                elementSizeTypeValue={Theme.ElementSizeType.large}
                                clickHandler={submitAndCloseHandler}>
                                Сохранить изменения
                            </Button>
                        ) : (
                            <Button
                                elementSizeTypeValue={Theme.ElementSizeType.large}
                                colors={successButtonLight}
                                clickHandler={closeAndRollBack}>
                                Сохранить изменения
                            </Button>
                        )}
                    </S.ButtonContainer>
                </S.GrayBlock>
            </MobileBody>
        </S.Container>
    )
}

AccountProfileEmail.propTypes = {
    closeHandler: PropTypes.func.isRequired,
    changeHandler: PropTypes.func.isRequired,
    value: PropTypes.string.isRequired,
    submitHandler: PropTypes.func.isRequired,
    rollbackStateProfile: PropTypes.func.isRequired
}

export default AccountProfileEmail
