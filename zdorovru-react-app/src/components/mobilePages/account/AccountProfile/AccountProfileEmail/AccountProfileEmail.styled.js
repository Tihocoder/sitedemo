import styled from 'styled-components'

// eslint-disable-next-line import/prefer-default-export
export const Container = styled.div`
    background-color: white;
    position: fixed;
    height: 100%;
    width: 100%;
    top: 0;
    left: 0;
    z-index: 20;
`
export const GrayBlock = styled.div`
    background-color: ${(props) => props.theme.colors.gray5};
    height: 100%;
`
export const ButtonContainer = styled.div`
    margin-top: 3.125rem;
    padding: 0 0.5rem;

    & > button {
        width: 100%;
    }
`
export const BodyContainer = styled.div`
    background-color: ${(props) => props.theme.colors.grayLightColor};
`
