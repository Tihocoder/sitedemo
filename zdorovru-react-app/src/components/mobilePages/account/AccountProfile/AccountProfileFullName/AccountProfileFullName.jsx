import React from 'react'
import MobilePageHeader from '@mobileShared/MobilePageHeader/MobilePageHeader'
import { MobileBody } from '@styles/ui-element.styled'
import BlockWithTitle from '@mobileComponents/BlockWithTitle/BlockWithTitle'
import InputMobile from '@mobileShared/InputMobile/InputMobile'
import Button from '@shared/Button/Button'
import * as Theme from '@styles/theme-const'
import { successButtonLight } from '@styles/theme-const'
import * as S from '@mobilePages/account/AccountProfile/AccountProfile.styled'
import {
    ButtonContainer,
    ContainerWindowPage,
    GrayBlock
} from '@mobilePages/account/AccountProfile/AccountProfile.styled'
import PropTypes from 'prop-types'
import MobileHeader from '@mobileShared/MobileHeader/MobileHeader'

const AccountProfileFullName = ({ profile, closeHandler, isChanged }) => {
    const submitAndCloseHandler = async () => {
        await profile.submitHandlerNew()
        closeHandler()
    }
    const closeAndRollBack = () => {
        profile.rollbackStateProfile()
        closeHandler()
    }
    return (
        <ContainerWindowPage>
            <MobileHeader backClickHandler={closeAndRollBack} isDefaultRouterBack={false}>
                Изменить ФИО
            </MobileHeader>
            <MobileBody>
                <GrayBlock>
                    <BlockWithTitle title="ФИО">
                        <S.InputsBlock>
                            <InputMobile
                                changeHandler={(value) => profile.changeMobileHandler('lastName', value)}
                                value={profile.state.lastName}
                                placeholder="Фамилия"
                            />
                            <InputMobile
                                changeHandler={(value) => profile.changeMobileHandler('firstName', value)}
                                value={profile.state.firstName}
                                placeholder="Имя"
                            />
                            <InputMobile
                                changeHandler={(value) => profile.changeMobileHandler('middleName', value)}
                                value={profile.state.middleName}
                                placeholder="Отчество"
                            />
                        </S.InputsBlock>
                    </BlockWithTitle>

                    <ButtonContainer>
                        {isChanged ? (
                            <Button
                                elementSizeTypeValue={Theme.ElementSizeType.large}
                                clickHandler={submitAndCloseHandler}>
                                Сохранить изменения
                            </Button>
                        ) : (
                            <Button
                                elementSizeTypeValue={Theme.ElementSizeType.large}
                                colors={successButtonLight}
                                clickHandler={closeAndRollBack}>
                                Сохранить изменения
                            </Button>
                        )}
                    </ButtonContainer>
                </GrayBlock>
            </MobileBody>
        </ContainerWindowPage>
    )
}

AccountProfileFullName.propTypes = {
    profile: PropTypes.shape({
        // eslint-disable-next-line react/forbid-prop-types
        state: PropTypes.object.isRequired,
        changeMobileHandler: PropTypes.func.isRequired,
        submitHandlerNew: PropTypes.func.isRequired,
        rollbackStateProfile: PropTypes.func.isRequired
    }).isRequired,
    closeHandler: PropTypes.func.isRequired
}

export default AccountProfileFullName
