import styled, { css } from 'styled-components'
import * as InputStyled from '@mobileShared/InputMobile/InputMobile.styled'

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    background-color: ${(props) => props.theme.colors.gray5};
    color: ${(props) => props.theme.colors.getGrayColor()};
    height: 100%;
    width: 100%;
    top: 0;
    left: 0;
    z-index: 5;
`

export const SpanDataItem = styled.span`
    color: ${(props) => props.theme.colors.getGrayColor(1)};
`
export const ChangeTitle = styled.span`
    font-size: 0.85rem;
    color: ${(props) => props.theme.colors.getGrayColor(0.5)};
`
export const BlockDateInput = styled.div`
    & > div {
        padding: 0;
    }
`
export const SpanRed = styled.span`
    color: ${(props) => props.theme.colors.getPureRedColor()};
    width: 100%;
    text-align: center;
    padding: 0.25rem;
`
export const InputsBlock = styled.div`
    display: flex;
    flex-direction: column;

    ${InputStyled.Container} {
        height: 2rem;
        border-bottom: 1px solid ${(props) => props.theme.colors.getGrayColor(0.1)};
    }
`
export const BodyModalContainer = styled.div`
    display: flex;
    flex-direction: column;
    max-width: 32rem;
    font-size: 1rem;
    padding-bottom: 2rem;
`
export const BirthdayInputContainer = styled.div`
    position: relative;
    display: flex;
    align-items: center;

    & > input:focus ~ label,
    input:not(:focus):valid ~ label {
        top: 0.1rem;
        bottom: 0.5rem;
        left: 1rem;
        font-size: 0.75rem;
        opacity: 1;
        height: 0.9rem;
    }

    & input {
        font: inherit;
        background: transparent;
        border: none;
        outline: none;
        padding: 0;
        margin: 0;
        height: 100%;
        width: 100%;
        max-width: 100%;
        vertical-align: bottom;
        text-align: inherit;
        box-sizing: content-box;
        -webkit-appearance: none;

        &::placeholder {
            color: ${(props) => props.theme.colors.getGrayColor(0.5)};
        }

        ${(props) =>
            !props.isValid &&
            css`
                color: ${props.theme.colors.getRedheadColor()};

                &::placeholder {
                    color: ${props.theme.colors.getRedheadColor()};
                }
            `}
    }
`
export const ButtonContainer = styled.div`
    margin-top: 3.125rem;
    padding: 0 0.5rem;

    & > button {
        width: 100%;
    }
`

export const ContainerWindowPage = styled.div`
    background-color: white;
    position: fixed;
    height: 100%;
    width: 100%;
    top: 0;
    left: 0;
    z-index: 20;
`
export const GrayBlock = styled.div`
    background-color: ${(props) => props.theme.colors.gray5};
    height: 100%;
`
