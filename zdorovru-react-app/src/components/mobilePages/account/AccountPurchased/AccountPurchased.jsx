import React from 'react'
import * as S from '@mobilePages/account/AccountOrderInfo/AccountOrderInfo.styled'
import MobilePageHeader from '@mobileShared/MobilePageHeader/MobilePageHeader'
import CatalogGoodContainer from '@mobileComponents/catalog/CatalogGoodContainer/CatalogGoodContainer'
import { usePurchased } from '@hooks/account/purchased-hook'
import { useRouter } from 'next/router'
import { useSelector } from 'react-redux'
import { MobileBody } from '@styles/ui-element.styled'
import MobileHeader from "@mobileShared/MobileHeader/MobileHeader";

const AccountPurchased = (props) => {
    const router = useRouter()
    const shipment = useSelector((state) => state.customer.shipment)
    const purchased = usePurchased()
    return (
        <S.Container>
            <MobileHeader>Купленные товары</MobileHeader>
            <MobileBody>
                {purchased.goods.length && (
                    <CatalogGoodContainer shipment={shipment}>{purchased.goods}</CatalogGoodContainer>
                )}
            </MobileBody>
        </S.Container>
    )
}

AccountPurchased.propTypes = {}

export default AccountPurchased
