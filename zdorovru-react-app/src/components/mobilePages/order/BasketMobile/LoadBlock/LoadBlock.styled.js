import styled, { keyframes } from 'styled-components'

const centerAnimate = keyframes`
  0% {
    transform: scale(1);
  }
  100% {
    transform: scale(1.5);
  }
`

export const Container = styled.div`
    height: 100%;
    color: ${(props) => props.theme.colors.getGrayColor()};
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    width: 100%;
`

export const TextBlock = styled.div`
    margin-top: 3rem;
    display: flex;
    flex-direction: column;
    width: 100%;
    align-items: center;
`
export const TextGeneralTitle = styled.span`
    font-size: 1.25rem;
    padding: 0 1rem;
`

export const TextDescription = styled.span`
    margin-top: 1rem;
`

export const LogoBlock = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    width: 100%;
`

export const ZdorovLogo = styled.img.attrs({
    src: `${process.env.BASE_PATH}/images/g20.svg`
})`
    animation: ${centerAnimate} 1s ease infinite alternate;
`
