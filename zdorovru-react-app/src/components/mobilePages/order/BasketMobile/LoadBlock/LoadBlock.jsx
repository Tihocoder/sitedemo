import React from 'react'
import PropTypes from 'prop-types'
import { GrayAbsoluteBlock, MobileBody } from '@styles/ui-element.styled'
import Button from '@shared/Button/Button'
import * as Theme from '@styles/theme-const'
import * as S from './LoadBlock.styled'

const LoadBlock = ({ cancelHandler }) => (
    <GrayAbsoluteBlock>
        <S.Container>
            <S.TextBlock>
                <S.TextGeneralTitle>Пожалуйста, подождите...</S.TextGeneralTitle>
                <S.TextDescription>Проверка наличия товаров</S.TextDescription>
            </S.TextBlock>
            <S.LogoBlock>
                <S.ZdorovLogo />
            </S.LogoBlock>
            <Button
                elementSizeTypeValue={Theme.ElementSizeType.regular}
                clickHandler={cancelHandler}
                colors={{
                    color: Theme.ColorConsts.white,
                    backgroundColor: Theme.ColorConsts.getPureRedColor(),
                    hoverBackgroundColor: Theme.ColorConsts.getPureRedColor(0.8)
                }}>
                Отменить
            </Button>
        </S.Container>
    </GrayAbsoluteBlock>
)

LoadBlock.propTypes = {
    cancelHandler: PropTypes.func.isRequired
}

export default LoadBlock
