import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import CatalogGoodContainer from '@mobileComponents/catalog/CatalogGoodContainer/CatalogGoodContainer'
import BasketBottomMenu from '@mobileComponents/order/basket/BasketBottomMenu/BasketBottomMenu'
import { usePreorder } from '@hooks/preorder-hooks'
import { MobileBody } from '@styles/ui-element.styled'
import SymbolRuble from '@shared/SymbolRuble/SymbolRuble'
import ModalWindow from '@shared/ModalWindow/ModalWindow'
import MobileStockSelector from '@mobilePages/MobileStockSelector/MobileStockSelector'
import MobileAddressSelector from '@mobilePages/MobileAddressSelector/MobileAddressSelector'
import Button from '@shared/Button/Button'
import * as Theme from '@styles/theme-const'
import LoadBlock from '@mobilePages/order/BasketMobile/LoadBlock/LoadBlock'
import MobileHeader from '@mobileShared/MobileHeader/MobileHeader'
import { updateActualBasketPostions } from '@actions/customer'
import * as S from './BasketMobile.styled'

const showPrice = (isDelivery, sums, isClearBasket) => {
    if (isClearBasket) return <React.Fragment />
    return (
        <S.PriceBlockList>
            <S.PriceItem>
                <S.PriceItemTitle>Стоимость товаров:</S.PriceItemTitle>
                <S.PriceItemSum>
                    {(isDelivery ? sums.sumDelivery : sums.sumStock).toLocaleString('RU-ru')} <SymbolRuble />
                </S.PriceItemSum>
            </S.PriceItem>
            {isDelivery && (
                <React.Fragment>
                    <S.PriceItem>
                        <S.PriceItemTitle>Стоимость доставки:</S.PriceItemTitle>
                        <S.PriceItemSum>
                            {sums.deliveryCost.toLocaleString('RU-ru')} <SymbolRuble />
                        </S.PriceItemSum>
                    </S.PriceItem>
                    <S.PriceItem>
                        <S.PriceItemTitle>Общая стоимость:</S.PriceItemTitle>
                        <S.PriceItemGeneralSum>
                            {sums.sumDeliveryFull.toLocaleString('RU-ru')} <SymbolRuble />
                        </S.PriceItemGeneralSum>
                    </S.PriceItem>
                </React.Fragment>
            )}
        </S.PriceBlockList>
    )
}

// eslint-disable-next-line no-unused-vars
const BasketMobile = ({ isDelivery }) => {
    const goods = useSelector((state) => state.customer.basketItems)
    const shipment = useSelector((state) => state.customer.shipment)
    const preorder = usePreorder(isDelivery)
    const basketItems = goods.filter((item) => !item.isHidden)
    const dispatch = useDispatch()

    useEffect(() => {
        const updateBasketItems = async () => {
            await dispatch(updateActualBasketPostions())
        }
        updateBasketItems()
    }, [])

    if (preorder.basket.basketState.isShowedStockSelector) {
        return <MobileStockSelector closeHandler={preorder.toggleSelectorStockHandler} />
    }
    if (preorder.basket.basketState.isShowedAddressSelector) {
        return (
            <MobileAddressSelector
                closeHandler={preorder.toggleSelectorAddressHandler}
                shipAddressId={preorder.shipment.shipAddressId}
            />
        )
    }
    if (preorder.basket.basketState.isPreorderLoading) {
        return <LoadBlock cancelHandler={preorder.cancelPreorder} />
    }
    const isClearBasket = basketItems.length === 0
    return (
        <S.Container>
            <MobileHeader>Корзина</MobileHeader>
            <MobileBody>
                <S.ContentContainer>
                    {isClearBasket && <S.TextInfoClearBasket>Корзина пуста</S.TextInfoClearBasket>}
                    <CatalogGoodContainer isDelivery={preorder.isDelivery} shipment={shipment}>
                        {basketItems}
                    </CatalogGoodContainer>
                    {preorder.isDelivery
                        ? showPrice(true, preorder.sums, isClearBasket)
                        : showPrice(false, preorder.sums, isClearBasket)}
                </S.ContentContainer>
                <BasketBottomMenu
                    preorder={preorder}
                    toggleStockSelector={preorder.toggleSelectorStockHandler}
                    toggleAddressSelector={preorder.toggleSelectorAddressHandler}
                />
            </MobileBody>
            {preorder.basket.basketState.isShowedWarningSelectStock && (
                <ModalWindow
                    title="Не выбрана аптека самовывоза"
                    isMobile
                    submit={{
                        text: 'Выбрать аптеку',
                        clickHandler: preorder.toggleSelectorStockHandler
                    }}
                    cancel={{
                        text: 'Отмена',
                        clickHandler: () => preorder.basket.toggleFlag('isShowedWarningSelectStock')
                    }}>
                    <S.BodyModalContainer>
                        Для оформления заказа необходимо выбрать аптеку самовывоза
                    </S.BodyModalContainer>
                </ModalWindow>
            )}
            {preorder.basket.basketState.isShowedWarningSelectDelivery && (
                <ModalWindow
                    title="Не выбран адрес доставки"
                    isMobile
                    submit={{
                        text: 'Выбрать адрес',
                        clickHandler: preorder.toggleSelectorAddressHandler
                    }}
                    cancel={{
                        text: 'Отмена',
                        clickHandler: () => preorder.basket.toggleFlag('isShowedWarningSelectDelivery')
                    }}>
                    <S.BodyModalContainer>
                        Для оформления необходимо выбрать адрес доставки
                    </S.BodyModalContainer>
                </ModalWindow>
            )}
            {preorder.basket.basketState.isShowedModalNotPositionStock && (
                <ModalWindow
                    title=""
                    isButtonFooter={false}
                    isMobile
                    footer={
                        <React.Fragment>
                            <Button
                                elementSizeTypeValue={Theme.ElementSizeType.regular}
                                clickHandler={preorder.toggleSelectorStockHandler}>
                                Сменить аптеку
                            </Button>
                            <Button
                                elementSizeTypeValue={Theme.ElementSizeType.regular}
                                clickHandler={() =>
                                    preorder.basket.toggleFlag('isShowedModalNotPositionStock')
                                }>
                                Вернуться к корзине
                            </Button>
                        </React.Fragment>
                    }
                    cancel={{
                        text: 'Отмена',
                        clickHandler: () => preorder.basket.toggleFlag('isShowedModalNotPositionStock')
                    }}>
                    <S.BodyModalContainer>
                        В корзине есть товары, отсутствующие в аптеке{' '}
                        {shipment.stockTitle ? `"${shipment.stockTitle}"` : ''}. Выберите другую аптеку
                        самовывоза или удалите эти товары
                    </S.BodyModalContainer>
                </ModalWindow>
            )}
            {preorder.basket.basketState.isShowedWarningSelectDelivery && (
                <ModalWindow
                    title="Не выбран адрес доставки"
                    isMobile
                    submit={{
                        text: 'Выбрать адрес',
                        clickHandler: preorder.toggleSelectorAddressHandler
                    }}
                    cancel={{
                        text: 'Отмена',
                        clickHandler: () => preorder.basket.toggleFlag('isShowedWarningSelectDelivery')
                    }}>
                    <S.BodyModalContainer>
                        Для оформления необходимо выбрать адрес доставки
                    </S.BodyModalContainer>
                </ModalWindow>
            )}
            {preorder.customModal.modalInfo.isShowed && (
                <ModalWindow
                    title={preorder.customModal.modalInfo.title}
                    isButtonFooter={preorder.customModal.modalInfo.isButtonFooter}
                    isFooterOnlyOneButton={preorder.customModal.modalInfo.isFooterOnlyOneButton}
                    isMobile
                    submit={{
                        text: preorder.customModal.modalInfo.buttonTitle,
                        clickHandler: preorder.customModal.modalInfo.buttonHandler
                    }}
                    cancel={{
                        text: preorder.customModal.modalInfo.cancelTitle,
                        clickHandler: preorder.customModal.closeModalInfo
                    }}>
                    {preorder.customModal.modalInfo.content}
                </ModalWindow>
            )}
        </S.Container>
    )
}

export default BasketMobile
