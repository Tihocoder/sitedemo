/* eslint-disable @typescript-eslint/no-unsafe-member-access */
import styled from 'styled-components'

export const Container = styled.div`
    display: flex;
    flex-direction: column;
`
export const ContentContainer = styled.div`
    margin-bottom: 8rem;
    overflow-x: auto;
`
export const TextInfoClearBasket = styled.div`
    font-size: 1.25rem;
    width: 100%;
    height: 100%;
    text-align: center;
    margin-top: 10rem;
`

export const PriceBlockList = styled.div`
    margin-top: 1rem;
    display: flex;
    flex-direction: column;
    padding: 0.5rem;
`
export const PriceItem = styled.div`
    display: flex;
    justify-content: space-between;
    font-size: 1rem;
`
export const PriceItemTitle = styled.span``

export const PriceItemSum = styled.span`
    white-space: nowrap;
`
export const PriceItemGeneralSum = styled.span`
    white-space: nowrap;
    font-weight: bold;
`
export const BodyModalContainer = styled.div`
    display: flex;
    flex-direction: column;
    max-width: 32rem;
    font-size: 1rem;
    padding-bottom: 2rem;
`
