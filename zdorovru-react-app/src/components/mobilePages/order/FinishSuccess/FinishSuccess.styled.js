import styled from 'styled-components'

export const Container = styled.div``

export const BlockDescriptionHead = styled.div`
    color: ${(props) => props.theme.colors.getPureRedColor()};
    background-color: white;
    padding: 0.5rem 0.6rem;
    text-align: center;
`
export const DescriptionBlockRow = styled.div`
    color: ${(props) => props.theme.colors.getGrayColor(0.5)};
    display: flex;
    flex-wrap: wrap;
    padding: 0.5rem 0.6rem;

    & > * {
        flex: 1 1 5rem;
    }
`

export const DescriptionTitle = styled.span`
    color: ${(props) => props.theme.colors.getGrayColor(0.5)};
`
export const DescriptionContainer = styled.div`
    flex-wrap: wrap;
`
export const DescriptionInfo = styled.span`
    color: ${(props) => props.theme.colors.getGrayColor()};
    font-size: 1.25rem;
    font-weight: bold;
`
export const DescriptionDefaultInfo = styled.span`
    color: ${(props) => props.theme.colors.getGrayColor()};
`
export const DescriptionInfoRed = styled.span`
    color: ${(props) => props.theme.colors.getPureRedColor()};
`
export const BlockGray = styled.div`
    background-color: ${(props) => props.theme.colors.gray5};
`
export const BlockWhite = styled.div`
    background-color: ${(props) => props.theme.colors.white};
    margin-top: 0.1rem;
`
