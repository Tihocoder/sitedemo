import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { useDispatch, useSelector } from 'react-redux'
import { clearBasketOrderCreated } from '@actions/customer'
import { MobileBody, MobileContainerGrayBody } from '@styles/ui-element.styled'
import CatalogGoodContainer from '@mobileComponents/catalog/CatalogGoodContainer/CatalogGoodContainer'
import BlockWithTitle from '@mobileComponents/BlockWithTitle/BlockWithTitle'
import MobileRowWithIconItem, {
    IconsMobileRowItem
} from '@mobileShared/MobileRowWithIconItem/MobileRowWithIconItem'
import moment from 'moment'
import SymbolRuble from '@shared/SymbolRuble/SymbolRuble'
import MobileRowItem from '@mobileShared/MobileRowItem/MobileRowItem'
import MobileHeader from '@mobileShared/MobileHeader/MobileHeader'
import * as siteHelpers from '@helpers/site'
import { useRouter } from 'next/router'
import * as S from './FinishSuccess.styled'

const getShipmentInfo = (isDelivery, stock, deliveryData, toggleIsShowedMap) => {
    if (!isDelivery)
        return (
            <BlockWithTitle title="Аптека самовывоза">
                <MobileRowWithIconItem
                    iconName={IconsMobileRowItem.GeoCrateOrder}
                    clickHandler={toggleIsShowedMap}
                    isShowedRow>
                    {stock.title} {stock.address}
                </MobileRowWithIconItem>
            </BlockWithTitle>
        )
    return (
        <BlockWithTitle title="Адрес доставки">
            <MobileRowItem>{deliveryData.shipAddressText}</MobileRowItem>
        </BlockWithTitle>
    )
}

const FinishSuccess = ({ orderId, orderDetails, orderGoods }) => {
    const router = useRouter()
    const shipment = useSelector((state) => state.customer.shipment)
    const dispatch = useDispatch()
    const [isShowedMap, setIsShowedMap] = useState(false)
    const basketItemsRedux = useSelector((state) => state.customer.basketItems)
    const toggleIsShowedMap = () => setIsShowedMap(!isShowedMap)

    useEffect(() => {
        moment.locale('ru')
        localStorage.removeItem('create-order-data')
    }, [])

    const clearBasketAsync = async () => {
        await dispatch(clearBasketOrderCreated(orderId))
    }

    useEffect(() => {
        if (basketItemsRedux.length > 0) {
            clearBasketAsync()
        }
    }, [basketItemsRedux.length])

    const redirectToHome = async () => {
        await router.push('/')
    }

    if (!orderDetails.orderNo) return <React.Fragment />
    return (
        <S.Container>
            <MobileHeader backClickHandler={redirectToHome} isDefaultRouterBack={false}>
                Заказ принят
            </MobileHeader>
            <MobileBody>
                <MobileContainerGrayBody>
                    <S.BlockDescriptionHead>
                        Заказ принят и будет подтвержден после получения СМС или звонка оператора.
                    </S.BlockDescriptionHead>
                    <BlockWithTitle title="Данные о заказе">
                        <S.DescriptionBlockRow>
                            <S.DescriptionTitle>Номер заказа</S.DescriptionTitle>
                            <S.DescriptionInfo>{orderDetails.orderNo}</S.DescriptionInfo>
                        </S.DescriptionBlockRow>
                        {!orderDetails.isDelivery && (
                            <React.Fragment>
                                <S.DescriptionBlockRow>
                                    <S.DescriptionTitle>Сумма заказа</S.DescriptionTitle>
                                    <S.DescriptionInfo>
                                        {orderDetails.ordSum} <SymbolRuble />
                                    </S.DescriptionInfo>
                                </S.DescriptionBlockRow>
                                <S.DescriptionBlockRow>
                                    <S.DescriptionTitle>Бронь до</S.DescriptionTitle>
                                    <S.DescriptionInfo>
                                        {siteHelpers.convertDateTime(
                                            orderDetails.statesInfo.maxReserveDateTime
                                        )}
                                    </S.DescriptionInfo>
                                </S.DescriptionBlockRow>
                                {orderDetails.statesInfo.isDemand && (
                                    <S.DescriptionBlockRow>
                                        <S.DescriptionTitle>Получение</S.DescriptionTitle>
                                        <S.DescriptionInfoRed>
                                            {siteHelpers.convertDate(orderDetails.statesInfo.receiptDate)}
                                            после получения СМС
                                        </S.DescriptionInfoRed>
                                    </S.DescriptionBlockRow>
                                )}
                            </React.Fragment>
                        )}
                        {orderDetails.isDelivery && (
                            <S.DescriptionBlockRow>
                                <S.DescriptionTitle>Интервал доставки</S.DescriptionTitle>
                                <S.DescriptionInfo>
                                    {orderDetails.deliveryData.timeSlotText}
                                </S.DescriptionInfo>
                            </S.DescriptionBlockRow>
                        )}
                    </BlockWithTitle>
                    {getShipmentInfo(
                        orderDetails.isDelivery,
                        orderDetails.stock,
                        orderDetails.deliveryData,
                        toggleIsShowedMap
                    )}
                    {!orderDetails.isDelivery && (
                        <S.BlockWhite>
                            <S.DescriptionBlockRow>
                                <S.DescriptionTitle>Часы работы</S.DescriptionTitle>
                                <S.DescriptionDefaultInfo>
                                    {orderDetails.stock.timeString}
                                </S.DescriptionDefaultInfo>
                            </S.DescriptionBlockRow>
                        </S.BlockWhite>
                    )}
                    {orderDetails.isDelivery && (
                        <BlockWithTitle title="Стоимость заказа">
                            <S.DescriptionBlockRow>
                                <S.DescriptionTitle>Сумма товаров</S.DescriptionTitle>
                                <S.DescriptionDefaultInfo>
                                    {orderDetails.ordSum} <SymbolRuble />
                                </S.DescriptionDefaultInfo>
                            </S.DescriptionBlockRow>
                            <S.DescriptionBlockRow>
                                <S.DescriptionTitle>Доставка</S.DescriptionTitle>
                                {orderDetails.deliveryData.deliveryPrice ? (
                                    <S.DescriptionDefaultInfo>
                                        {orderDetails.deliveryData.deliveryPrice} <SymbolRuble />
                                    </S.DescriptionDefaultInfo>
                                ) : (
                                    <S.DescriptionDefaultInfo>Бесплатно</S.DescriptionDefaultInfo>
                                )}
                            </S.DescriptionBlockRow>
                            <S.DescriptionBlockRow>
                                <S.DescriptionTitle>Итого</S.DescriptionTitle>
                                <S.DescriptionInfo>
                                    {orderDetails.ordSum + (orderDetails.deliveryData.deliveryPrice ?? 0)}{' '}
                                    <SymbolRuble />
                                </S.DescriptionInfo>
                            </S.DescriptionBlockRow>
                        </BlockWithTitle>
                    )}

                    <BlockWithTitle title="Оплата">
                        {orderDetails.ordStateTitle ? (
                            <S.DescriptionBlockRow>
                                <S.DescriptionTitle>Статус оплаты</S.DescriptionTitle>
                                <S.DescriptionDefaultInfo>
                                    {orderDetails.ordStateTitle}
                                </S.DescriptionDefaultInfo>
                            </S.DescriptionBlockRow>
                        ) : (
                            <React.Fragment />
                        )}
                        {orderDetails.paymentInfo?.paymentTypeTitle ? (
                            <S.DescriptionBlockRow>
                                <S.DescriptionTitle>Способ оплаты</S.DescriptionTitle>
                                <S.DescriptionDefaultInfo>
                                    {orderDetails.paymentInfo.paymentTypeTitle}
                                </S.DescriptionDefaultInfo>
                            </S.DescriptionBlockRow>
                        ) : (
                            <React.Fragment />
                        )}
                    </BlockWithTitle>
                    <BlockWithTitle title="Контакты получателя">
                        {orderDetails.customer.name && (
                            <S.DescriptionBlockRow>
                                <S.DescriptionTitle>Имя</S.DescriptionTitle>
                                <S.DescriptionDefaultInfo>
                                    {orderDetails.customer.name}
                                </S.DescriptionDefaultInfo>
                            </S.DescriptionBlockRow>
                        )}
                        <S.DescriptionBlockRow>
                            <S.DescriptionTitle>Телефон</S.DescriptionTitle>
                            <S.DescriptionDefaultInfo>
                                {orderDetails.customer.phone.replace(
                                    /(\d{1})(\d{3})(\d{3})(\d{2})(\d{2})/,
                                    '+$1 ($2) $3-$4-$5'
                                )}
                            </S.DescriptionDefaultInfo>
                        </S.DescriptionBlockRow>
                        {orderDetails.customer.email && (
                            <S.DescriptionBlockRow>
                                <S.DescriptionTitle>e-mail</S.DescriptionTitle>
                                <S.DescriptionDefaultInfo>
                                    {orderDetails.customer.email}
                                </S.DescriptionDefaultInfo>
                            </S.DescriptionBlockRow>
                        )}
                    </BlockWithTitle>

                    <BlockWithTitle title="Товары в заказе">
                        {orderGoods.length && (
                            <CatalogGoodContainer shipment={shipment}>{orderGoods}</CatalogGoodContainer>
                        )}
                    </BlockWithTitle>
                </MobileContainerGrayBody>
            </MobileBody>
        </S.Container>
    )
}

FinishSuccess.propTypes = {
    orderId: PropTypes.number.isRequired,
    orderGoods: PropTypes.array.isRequired,
    orderDetails: PropTypes.shape({
        siteOrderNo: PropTypes.string.isRequired,
        isDelivery: PropTypes.bool.isRequired,
        orderNo: PropTypes.string.isRequired,
        maxReserveDateTime: PropTypes.string,
        items: PropTypes.array.isRequired,
        customer: PropTypes.shape({
            name: PropTypes.string,
            phone: PropTypes.string,
            email: PropTypes.string
        }),
        paymentInfo: PropTypes.shape({
            paymentTypeId: PropTypes.number,
            paymentTypeTitle: PropTypes.string,
            cashExchange: PropTypes.number,
            isOnlinePaymentAvailable: PropTypes.bool,
            isPaid: PropTypes.bool
        }),
        statesInfo: PropTypes.shape({
            ordStateId: PropTypes.number.isRequired,
            ordStateTitle: PropTypes.string.isRequired,
            isDeletable: PropTypes.bool.isRequired,
            isCanRepeat: PropTypes.bool.isRequired,
            isCanProlong: PropTypes.bool.isRequired,
            isCanCancel: PropTypes.bool.isRequired,
            isShowBuyDate: PropTypes.bool.isRequired,
            lastAptEtaDatetime: PropTypes.string,
            isDemand: PropTypes.bool.isRequired,
            receiptDate: PropTypes.string,
            maxReserveDateTime: PropTypes.string
        }).isRequired,
        stock: PropTypes.shape({
            id: PropTypes.number,
            title: PropTypes.string,
            address: PropTypes.string,
            opTimeFrom: PropTypes.string,
            opTimeTo: PropTypes.string,
            timeString: PropTypes.string
        }),
        deliveryData: PropTypes.shape({
            addressText: PropTypes.string,
            apt: PropTypes.string,
            cashExchange: PropTypes.number,
            code: PropTypes.string,
            comment: PropTypes.string,
            deliveryPrice: PropTypes.number,
            entrance: PropTypes.string,
            floor: PropTypes.string,
            house: PropTypes.string,
            houseType: PropTypes.number,
            isNearestTime: PropTypes.bool,
            office: PropTypes.string,
            organization: PropTypes.string,
            paymentTypeId: PropTypes.number,
            timeSlotText: PropTypes.string
        }),
        ordSum: PropTypes.number,
        ordStateTitle: PropTypes.string,
        paymentTypeTitle: PropTypes.string
    })
}
FinishSuccess.defaultProps = {
    orderDetails: {
        customer: {
            name: '',
            phone: '',
            email: ''
        },
        deliveryData: {
            addressText: '',
            apt: '',
            cashExchange: null,
            code: '',
            comment: '',
            deliveryPrice: 0,
            entrance: '',
            floor: '',
            house: '',
            houseType: 0,
            isNearestTime: false,
            office: '',
            organization: '',
            paymentTypeId: 0,
            timeSlotText: ''
        },
        ordSum: 0,
        ordStateTitle: '',
        paymentTypeTitle: ''
    }
}

export default FinishSuccess
