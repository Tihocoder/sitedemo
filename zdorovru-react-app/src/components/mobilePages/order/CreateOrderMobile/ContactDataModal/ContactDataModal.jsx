import React from 'react'
import PropTypes from 'prop-types'
import InputMobile, { MaskType } from '@mobileShared/InputMobile/InputMobile'
import MobilePageHeader from '@mobileShared/MobilePageHeader/MobilePageHeader'
import * as S from './ContactDataModal.styled'
import { MobileBody } from '@styles/ui-element.styled'
import MobileHeader from "@mobileShared/MobileHeader/MobileHeader";

const ContactDataModal = ({ changeContactData, contactData, closeHandler }) => {
    // eslint-disable-next-line no-return-await
    const changeInput = (name) => async (value) => await changeContactData(name, value)

    return (
        <S.Container>
            <MobileHeader backClickHandler={closeHandler} isShowArrowBack>
                Контактные данные
            </MobileHeader>
            <MobileBody>
                <InputMobile
                    label="Имя"
                    placeholder="Обязательно"
                    changeHandler={changeInput('name')}
                    value={contactData.name}
                />
                <InputMobile
                    label="Телефон"
                    placeholder="Обязательно"
                    changeHandler={changeInput('phone')}
                    inputMaskProps={{
                        isEnabled: true,
                        type: MaskType.phone
                    }}
                    value={contactData.phone}
                />
                <InputMobile
                    label="E-mail"
                    placeholder="Не обязательно"
                    changeHandler={changeInput('email')}
                    value={contactData.email}
                />
            </MobileBody>
        </S.Container>
    )
}

ContactDataModal.propTypes = {
    changeContactData: PropTypes.func.isRequired,
    closeHandler: PropTypes.func.isRequired,
    // eslint-disable-next-line react/forbid-prop-types
    contactData: PropTypes.any.isRequired
}

export default ContactDataModal
