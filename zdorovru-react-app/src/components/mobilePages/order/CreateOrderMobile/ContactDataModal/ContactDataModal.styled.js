import styled from 'styled-components'
import * as InputStyled from '@mobileShared/InputMobile/InputMobile.styled'

// eslint-disable-next-line import/prefer-default-export
export const Container = styled.div`
    color: ${(props) => props.theme.colors.getGrayColor()};
    background-color: white;
    position: fixed;
    height: 100%;
    width: 100%;
    top: 0;
    left: 0;
    z-index: 6;

    ${InputStyled.Container} {
        height: 2rem;
    }
`
