import React from 'react'
import PropTypes from 'prop-types'
import * as S from '../CreateOrderMobile.styled'
import { useSelector } from 'react-redux'

const OrderStockData = props => {
    const shipment = useSelector(state => state.customer.shipment)
    return (
        <React.Fragment>
            <S.DescriptionBlockRow>
                <S.DescriptionTitle>Часы работы</S.DescriptionTitle>
                {/*<S.DescriptionInfo></S.DescriptionInfo>*/}
            </S.DescriptionBlockRow>
        </React.Fragment>
    )
}

OrderStockData.propTypes = {

}

export default OrderStockData