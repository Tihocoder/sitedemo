import styled from 'styled-components'
import { ButtonElement } from '@shared/Button/Button.styled'
import * as InputStyled from '@mobileShared/InputMobile/InputMobile.styled'
import * as BS from '@mobileShared/BoolSwitcher/BoolSwitcher.styled'

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    color: ${(props) => props.theme.colors.getGrayColor()};
`

export const BodyContainer = styled.div`
    background-color: ${(props) => props.theme.colors.gray5};
`

export const FooterDescription = styled.div`
    color: ${(props) => props.theme.colors.grayAkm40};
    font-size: 0.875rem;
`

export const LinkBlue = styled.a`
    color: ${(props) => props.theme.colors.getBlue60Color()};
`

export const ContentBlock = styled.div`
    // padding: 0.7rem;
`

export const DescriptionBlockRow = styled.div`
    color: ${(props) => props.theme.colors.getGrayColor(0.5)};
    display: flex;
    flex-wrap: wrap;
    padding: 0.5rem 0.6rem;

    & > * {
        flex: 1 1 5rem;
    }
`
export const DescriptionTitle = styled.span`
    color: ${(props) => props.theme.colors.getGrayColor(0.5)};
    max-width: 10rem;
`
export const DescriptionBlockTitle = styled.span`
    color: ${(props) => props.theme.colors.getGrayColor(0.5)};
`
export const DescriptionContainer = styled.div`
    flex-wrap: wrap;
`
export const DescriptionInfo = styled.span`
    color: ${(props) => props.theme.colors.getGrayColor()};
`
export const DescriptionInfoRed = styled.span`
    color: ${(props) => props.theme.colors.getRedheadColor()};
`

export const DescriptionLabelBlock = styled.div`
    color: ${(props) => props.theme.colors.getGrayColor(0.9)};
    display: flex;
    margin-top: 0.4rem;
`

export const RowContainer = styled.div`
    display: flex;
    align-items: center;
`

export const IconContainer = styled.div`
    width: 1.45rem;
    display: flex;
    align-items: center;
    margin-right: 0.4rem;
`

export const IconUser = styled.img.attrs({
    src: `${process.env.BASE_PATH}/images/user-create-order.svg`
})`
    height: auto;
    width: 100%;
`
export const IconClock = styled.img.attrs({
    src: `${process.env.BASE_PATH}/images/clock-mobile.svg`
})`
    height: auto;
    width: 100%;
`
export const IconPayment = styled.img.attrs({
    src: `${process.env.BASE_PATH}/images/payment-mobile.svg`
})`
    height: auto;
    width: 100%;
`
export const IconGeo = styled.img.attrs({
    src: `${process.env.BASE_PATH}/images/geo-crate-order.svg`
})`
    height: 100%;
    width: auto;
`

export const RowContainerBody = styled.div`
    display: flex;
    flex-direction: column;
`
export const ContactDataItem = styled.span`
    color: ${(props) => props.theme.colors.getGrayColor(1)};
`
export const ChangeTitle = styled.span`
    font-size: 0.75rem;
    color: ${(props) => props.theme.colors.getGrayColor(0.5)};
    line-height: 1rem;
`

export const BlockWithDescription = styled.div`
    ${DescriptionLabelBlock} {
        margin-bottom: 0.2rem;
    }
`
export const Wrapper = styled.div`
    padding: 0.5rem 0.6rem;
    background-color: ${(props) => props.theme.colors.gray5};
    min-height: 1.688rem;

    & > div {
        margin-top: 0.5rem;
    }
`

export const BetweenCenter = styled.div`
    display: flex;
    align-items: center;
    justify-content: space-between;
`

export const PriceDescription = styled.span`
    color: ${(props) => props.theme.colors.getGrayColor(0.6)};
    font-size: 1rem;
    white-space: nowrap;
    line-height: 1.25rem;
`
export const PriceSum = styled.span`
    color: ${(props) => props.theme.colors.getGrayColor()};
    font-weight: bold;
    font-size: 2rem;
    line-height: 2rem;
`
export const PriceSumDefault = styled.span`
    color: ${(props) => props.theme.colors.getGrayColor()};
    font-weight: 500;
    font-size: 1.25rem;
    line-height: 2rem;
`
export const ButtonCreateOrderContainer = styled.div`
    ${ButtonElement} {
        width: 100%;
    }
`
export const ContentBlockWhite = styled.div`
    background-color: white;
`

export const InputsBlock = styled.div`
    display: flex;
    flex-direction: column;

    ${InputStyled.Container} {
        height: 2rem;
        border-bottom: 1px solid ${(props) => props.theme.colors.getGrayColor(0.1)};
    }
`

export const WhiteAndPaddingContainer = styled.div`
    padding: 0.625rem;
    justify-content: space-between;
    align-items: center;
    background-color: ${(props) => props.theme.colors.white};

    ${BS.Container} {
        padding: 0.313rem;
    }
`

export const BodyModalContainer = styled.div`
    display: flex;
    flex-direction: column;
    max-width: 32rem;
    font-size: 1rem;
    padding-bottom: 2rem;
`
