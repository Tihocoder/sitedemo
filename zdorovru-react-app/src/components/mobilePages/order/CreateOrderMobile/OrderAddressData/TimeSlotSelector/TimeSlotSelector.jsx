import React, { useState } from 'react'
import PropTypes from 'prop-types'
import MobilePageHeader from '@mobileShared/MobilePageHeader/MobilePageHeader'
import TimeSlotsItem from '@components/Order/CreateOrder/TimeSlotsItem/TimeSlotsItem'
import MobileRowItem from '@mobileShared/MobileRowItem/MobileRowItem'
import { MobileBody } from '@styles/ui-element.styled'
import * as S from './TimeSlotSelector.styled'
import MobileHeader from "@mobileShared/MobileHeader/MobileHeader";

const TimeSlotSelector = (props) => {
    const [searchText, changeSearchHandler] = useState('')

    const filteredTimeSlots = searchText
        ? props.timeSlots.filter((timeSlot) =>
              timeSlot.title.toLowerCase().includes(searchText.toLowerCase())
          )
        : props.timeSlots
    const changeTextAddressHandler = (e) => {
        changeSearchHandler(e.target.value)
    }
    const selectTimeSlot = (id) => {
        props.selectTimeSlotHandler(id)
        props.closeHandler()
    }
    return (
        <S.Container>
            <MobileHeader
                searchInput={{
                    isShowed: true,
                    searchText,
                    changeSearchHandler: changeTextAddressHandler,
                    placeholder: 'Поиск'
                }}
                isShowArrowBack
                backClickHandler={props.closeHandler}
                isDefaultRouterBack={false}>
                Выбор интервала доставки
            </MobileHeader>
            <MobileBody isExtendBlock>
                {filteredTimeSlots.length ? (
                    <S.TimeSlotsContainer>
                        {filteredTimeSlots.map((timeSlot) => (
                            <MobileRowItem
                                key={timeSlot.id}
                                clickHandler={async () => {
                                    await selectTimeSlot(timeSlot.id)
                                }}
                                isSelected={timeSlot.id === props.shipTimeSlotId}>
                                <S.TimeSlotContainer>
                                    <TimeSlotsItem
                                        title={timeSlot.title}
                                        paymentTypes={props.isDelivery ? timeSlot.paymentTypes : null}
                                    />
                                </S.TimeSlotContainer>
                            </MobileRowItem>
                        ))}
                    </S.TimeSlotsContainer>
                ) : (
                    <React.Fragment />
                )}
            </MobileBody>
        </S.Container>
    )
}

TimeSlotSelector.propTypes = {
    timeSlots: PropTypes.arrayOf({
        webId: PropTypes.string.isRequired,
        goodId: PropTypes.number.isRequired
    }),
    shipTimeSlotId: PropTypes.number.isRequired,
    selectTimeSlotHandler: PropTypes.func.isRequired,
    closeHandler: PropTypes.func.isRequired
}

export default TimeSlotSelector
