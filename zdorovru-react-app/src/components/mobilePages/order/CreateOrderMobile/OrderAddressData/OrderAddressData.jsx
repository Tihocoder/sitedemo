import React, { useState } from 'react'
import PropTypes from 'prop-types'
import MultipleSwitcher from '@mobileShared/MultipleSwitcher/MultipleSwitcher'
import { HouseType } from 'src/consts'
import InputMobile from '@mobileShared/InputMobile/InputMobile'
import TimeSlotSelector from '@mobilePages/order/CreateOrderMobile/OrderAddressData/TimeSlotSelector/TimeSlotSelector'
import BlockWithTitle from '@mobileComponents/BlockWithTitle/BlockWithTitle'
import MobileRowWithIconItem, {
    IconsMobileRowItem
} from '@mobileShared/MobileRowWithIconItem/MobileRowWithIconItem'
import PaymentTypeSelector from '@mobilePages/order/CreateOrderMobile/OrderAddressData/PaymentTypeSelector/PaymentTypeSelector'
import MobileAddressSelector from '@mobilePages/MobileAddressSelector/MobileAddressSelector'
import { useRouter } from 'next/router'
import ModalWindow from '@shared/ModalWindow/ModalWindow'
import * as S from '../CreateOrderMobile.styled'

// eslint-disable-next-line no-unused-vars
const propName = (obj, type) => Object.keys(obj).find((key) => obj[key] === type)

const HouseItems = [
    {
        value: HouseType.apt,
        title: 'Квартира'
    },
    {
        value: HouseType.house,
        title: 'Дом'
    },
    {
        value: HouseType.office,
        title: 'Офис'
    }
]

const OrderAddressData = (props) => {
    const [isSelectorShipAddress, setIsSelectorShipAddress] = useState(false)
    const router = useRouter()
    const [openedSelectorPaymentType, setOpenedSelectorPaymentType] = useState(false)
    const [openedSelectorTimeSlots, setOpenedSelectorTimeSlots] = useState(false)
    const [messageNotSelectedTimeSlot, setMessageNotSelectedTimeSlot] = useState(false)
    const toggleMessageNotSelectedTimeSlot = () => setMessageNotSelectedTimeSlot(!messageNotSelectedTimeSlot)
    const showSelectorPaymentTypeHandler = () => {
        if (props.createOrder.stateFormData.shipTimeSlotInfo?.shipTimeSlotId)
            setOpenedSelectorPaymentType(true)
        else toggleMessageNotSelectedTimeSlot()
    }
    const closeSelectorShipAddress = async () => {
        setIsSelectorShipAddress(false)
        await router.push(`/order/basket?isDelivery=${true}`)
        await router.reload()
    }
    if (isSelectorShipAddress)
        return (
            <MobileAddressSelector
                isNotReload
                closeHandler={closeSelectorShipAddress}
                shipAddressId={props.shipment.shipAddressId}
            />
        )

    const { validate } = props.createOrder.stateFormData
    return (
        <React.Fragment>
            <BlockWithTitle title="Адрес доставки">
                <MobileRowWithIconItem
                    iconName={IconsMobileRowItem.Delivery}
                    isShowedRow
                    clickHandler={() => setIsSelectorShipAddress(!isSelectorShipAddress)}>
                    {props.shipment.shipAddressTitle}
                </MobileRowWithIconItem>
                <S.Wrapper>
                    <MultipleSwitcher
                        selectedItemName={props.deliveryData.houseType}
                        selectedHandler={props.changeDeliveryData('houseType')}
                        items={HouseItems}
                    />
                </S.Wrapper>
                <S.InputsBlock>
                    {props.deliveryData.houseType === HouseType.apt ? (
                        <React.Fragment>
                            <InputMobile
                                label="Квартира"
                                placeholder="Обязательно"
                                isValid={props.validateDeliveryData.apt}
                                changeHandler={props.changeDeliveryData('apt')}
                                value={props.deliveryData.apt}
                            />
                            <InputMobile
                                label="Подъезд"
                                placeholder="Желательно"
                                changeHandler={props.changeDeliveryData('entrance')}
                                value={props.deliveryData.entrance}
                            />
                            <InputMobile
                                label="Этаж"
                                placeholder="Желательно"
                                changeHandler={props.changeDeliveryData('floor')}
                                value={props.deliveryData.floor}
                            />
                            <InputMobile
                                label="Домофон"
                                placeholder="Желательно"
                                changeHandler={props.changeDeliveryData('code')}
                                value={props.deliveryData.code}
                            />
                        </React.Fragment>
                    ) : (
                        <React.Fragment />
                    )}
                    {props.deliveryData.houseType === HouseType.office ? (
                        <React.Fragment>
                            <InputMobile
                                label="Организация"
                                placeholder="Желательно"
                                changeHandler={props.changeDeliveryData('organization')}
                                value={props.deliveryData.organization}
                            />
                            <InputMobile
                                label="Офис"
                                placeholder="Желательно"
                                changeHandler={props.changeDeliveryData('office')}
                                value={props.deliveryData.office}
                            />
                        </React.Fragment>
                    ) : (
                        <React.Fragment />
                    )}
                    <InputMobile
                        label="Коментарий"
                        placeholder="Не обязательно"
                        changeHandler={props.changeDeliveryData('comment')}
                        value={props.deliveryData.comment}
                    />
                </S.InputsBlock>
            </BlockWithTitle>
            <BlockWithTitle title="Время доставки">
                <MobileRowWithIconItem
                    isValid={validate.shipTimeSlotInfo.shipTimeSlotId}
                    clickHandler={() => setOpenedSelectorTimeSlots(!openedSelectorTimeSlots)}
                    iconName={IconsMobileRowItem.ClockMobile}
                    isShowedRow>
                    {props.createOrder.getSelectedTimeSlotTitle()}
                </MobileRowWithIconItem>
            </BlockWithTitle>
            <BlockWithTitle title="Способ оплаты">
                <MobileRowWithIconItem
                    isValid={validate.paymentInfo.paymentType}
                    clickHandler={showSelectorPaymentTypeHandler}
                    iconName={IconsMobileRowItem.PaymentMobile}
                    isShowedRow>
                    {props.createOrder.getPaymentTitle()}
                </MobileRowWithIconItem>
            </BlockWithTitle>
            {openedSelectorTimeSlots && (
                <TimeSlotSelector
                    isDelivery
                    timeSlots={props.timeSlots}
                    closeHandler={() => setOpenedSelectorTimeSlots(!openedSelectorTimeSlots)}
                    selectTimeSlotHandler={props.createOrder.changeTimeSlotHandler}
                    shipTimeSlotId={props.createOrder.stateFormData.shipTimeSlotInfo.shipTimeSlotId}
                />
            )}
            {openedSelectorPaymentType && (
                <PaymentTypeSelector
                    changeHandler={props.createOrder.changePaymentType}
                    selectType={props.createOrder.stateFormData.paymentInfo?.paymentType}
                    paymentTypes={props.createOrder.paymentTypes}
                    closeHandler={() => setOpenedSelectorPaymentType(!openedSelectorPaymentType)}
                />
            )}
            {messageNotSelectedTimeSlot && (
                <ModalWindow
                    title="Внимание"
                    isMobile
                    isButtonFooter={false}
                    isFooterOnlyOneButton
                    submit={{
                        text: 'Понятно',
                        clickHandler: toggleMessageNotSelectedTimeSlot
                    }}
                    cancel={{
                        text: 'Отмена',
                        clickHandler: toggleMessageNotSelectedTimeSlot
                    }}>
                    <S.BodyModalContainer>
                        Для выбора способа оплаты сначала необходимо выбрать интервал доставки
                    </S.BodyModalContainer>
                </ModalWindow>
            )}
        </React.Fragment>
    )
}

OrderAddressData.propTypes = {
    changeDeliveryData: PropTypes.func.isRequired,
    timeSlots: PropTypes.array.isRequired,
    deliveryData: PropTypes.object.isRequired,
    shipment: PropTypes.object.isRequired,
    validateDeliveryData: PropTypes.object.isRequired,
    selectedAddress: PropTypes.shape({
        id: PropTypes.number.isRequired,
        title: PropTypes.string.isRequired
    })
}

export default OrderAddressData
