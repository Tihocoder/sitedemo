import styled from 'styled-components'

export const Container = styled.div`
  background-color: white;
  position: fixed;
  height: 100%;
  width: 100%;
  top: 0;
  left: 0;
  z-index: 6;
`
export const ItemsContainer = styled.div`
  overflow-x: auto;
  max-height: 100%;
  box-sizing: border-box;
`
export const ItemBlock = styled.div`
  padding: 0.5rem 1rem;
  border-bottom: 1px solid ${props => props.theme.colors.getGrayColor(0.1)};
`
