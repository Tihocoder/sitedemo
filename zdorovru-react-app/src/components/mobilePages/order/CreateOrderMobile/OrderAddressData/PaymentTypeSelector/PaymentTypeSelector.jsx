import React from 'react'
import * as S from '@mobilePages/MobileCitySelector/MobileCitySelector.styled'
import MobilePageHeader from '@mobileShared/MobilePageHeader/MobilePageHeader'
import MobileRowItem from '@mobileShared/MobileRowItem/MobileRowItem'
import { MobileBody } from '@styles/ui-element.styled'
import MobileHeader from "@mobileShared/MobileHeader/MobileHeader";

const PaymentTypeSelector = ({ closeHandler, paymentTypes, changeHandler, selectType }) => {
    const changePaymentType = async (id) => {
        await changeHandler(id)
        closeHandler()
    }
    return (
        <S.Container>
            <MobileHeader isDefaultRouterBack={false} backClickHandler={closeHandler} isShowArrowBack>
                Выбор способа оплаты
            </MobileHeader>
            <MobileBody>
                {paymentTypes.map((x) => (
                    <MobileRowItem
                        key={x.id}
                        clickHandler={async () => await changePaymentType(x.id)}
                        isSelected={x.id === selectType}>
                        <S.PaymentTypeСontainer>{x.title}</S.PaymentTypeСontainer>
                    </MobileRowItem>
                ))}
            </MobileBody>
        </S.Container>
    )
}

PaymentTypeSelector.propTypes = {}

export default PaymentTypeSelector
