import React, { useState } from 'react'
import PropTypes from 'prop-types'
import * as S from '@mobilePages/order/CreateOrderMobile/CreateOrderMobile.styled'
import MobileRowWithIconItem, {
    IconsMobileRowItem
} from '@mobileShared/MobileRowWithIconItem/MobileRowWithIconItem'
import BlockWithTitle from '@mobileComponents/BlockWithTitle/BlockWithTitle'
import MobileStockSelector from '@mobilePages/MobileStockSelector/MobileStockSelector'
import { useRouter } from 'next/router'
import moment from 'moment'
import TimeSlotSelector from '@mobilePages/order/CreateOrderMobile/OrderAddressData/TimeSlotSelector/TimeSlotSelector'
import PaymentTypeSelector from '@mobilePages/order/CreateOrderMobile/OrderAddressData/PaymentTypeSelector/PaymentTypeSelector'
import ModalWindow from '@shared/ModalWindow/ModalWindow'

const StockOrderInfo = (props) => {
    const [isShowedStockSelector, setIsShowedStockSelector] = useState(false)
    const router = useRouter()
    const toggleIsShowedStockSelector = () => setIsShowedStockSelector(!isShowedStockSelector)

    const [openedSelectorPaymentType, setOpenedSelectorPaymentType] = useState(false)
    const [openedSelectorTimeSlots, setOpenedSelectorTimeSlots] = useState(false)
    const toggleSelectorTimeSlots = () => setOpenedSelectorTimeSlots(!openedSelectorTimeSlots)
    const toggleSelectorPaymentType = () => setOpenedSelectorPaymentType(!openedSelectorPaymentType)

    const closeStockSelectorHandler = async () => {
        setIsShowedStockSelector(false)
        await router.push(`/order/basket?isDelivery=${false}`)
        await router.reload()
    }
    const receiptDate = moment(props.preorderInfo.receiptDate)
    const isTomorrow = receiptDate.format('DD.MM.YY') === moment().add(1, 'days').format('DD.MM.YY')
    const isToday = receiptDate.format('DD.MM.YY') === moment().format('DD.MM.YY')
    const current = moment().startOf('day')
    const receiptDateDiff = moment.duration(receiptDate.diff(current)).asDays()

    if (isShowedStockSelector)
        return <MobileStockSelector isNotReload closeHandler={closeStockSelectorHandler} />

    const { validate } = props.createOrder.stateFormData
    const firstTimeSlot = props.timeSlots?.length ? props.timeSlots[0] : null
    return (
        <S.Container>
            <BlockWithTitle title="Аптека самовывоза">
                <MobileRowWithIconItem
                    iconName={IconsMobileRowItem.GeoCrateOrder}
                    clickHandler={toggleIsShowedStockSelector}
                    isShowedRow>
                    {props.shipment.stockTitle}
                    <S.ChangeTitle>Изменить адрес самовывоза</S.ChangeTitle>
                </MobileRowWithIconItem>
            </BlockWithTitle>
            <S.DescriptionBlockRow>
                <S.DescriptionTitle>Часы работы</S.DescriptionTitle>
                <S.DescriptionInfo>{props.timeString}</S.DescriptionInfo>
            </S.DescriptionBlockRow>
            <S.DescriptionBlockRow>
                <S.DescriptionTitle>Бронь до</S.DescriptionTitle>
                <S.DescriptionInfo>
                    {moment(props.preorderInfo.maxReserveDateTime).format('DD.MM.YYYY HH:mm')}
                </S.DescriptionInfo>
            </S.DescriptionBlockRow>
            {receiptDateDiff < 1 ? (
                <React.Fragment />
            ) : (
                <S.DescriptionBlockRow>
                    <S.DescriptionTitle>Получение</S.DescriptionTitle>
                    {moment(props.preorderInfo.receiptDate).format('DD.MM.YY') ===
                        moment().format('DD.MM.YY')}
                    <S.DescriptionInfoRed>
                        {isTomorrow && 'Завтра, после получения СМС'}
                        {!isToday && !isTomorrow ? (
                            `${moment(props.preorderInfo.receiptDate).format(
                                'DD.MM.YY'
                            )}, после получения СМС`
                        ) : (
                            <React.Fragment />
                        )}
                    </S.DescriptionInfoRed>
                </S.DescriptionBlockRow>
            )}
            {firstTimeSlot && (
                <React.Fragment>
                    <BlockWithTitle title="Способ оплаты">
                        <MobileRowWithIconItem
                            isValid={validate.paymentInfo.paymentType}
                            clickHandler={toggleSelectorPaymentType}
                            iconName={IconsMobileRowItem.PaymentMobile}
                            isShowedRow>
                            {props.createOrder.getPaymentTitle()}
                        </MobileRowWithIconItem>
                    </BlockWithTitle>
                    <BlockWithTitle title="Примерное время получения заказа">
                        <MobileRowWithIconItem
                            isValid={validate.shipTimeSlotInfo.shipTimeSlotId}
                            clickHandler={toggleSelectorTimeSlots}
                            iconName={IconsMobileRowItem.ClockMobile}
                            isShowedRow>
                            {props.createOrder.getSelectedTimeSlotTitle()}
                        </MobileRowWithIconItem>
                    </BlockWithTitle>
                </React.Fragment>
            )}
            {openedSelectorTimeSlots && (
                <TimeSlotSelector
                    isDelivery={false}
                    timeSlots={props.timeSlots}
                    closeHandler={toggleSelectorTimeSlots}
                    selectTimeSlotHandler={props.createOrder.changeTimeSlotHandler}
                    shipTimeSlotId={props.createOrder.stateFormData.shipTimeSlotInfo.shipTimeSlotId}
                />
            )}
            {openedSelectorPaymentType && (
                <PaymentTypeSelector
                    changeHandler={props.createOrder.changePaymentType}
                    selectType={props.createOrder.stateFormData.paymentInfo?.paymentType}
                    paymentTypes={firstTimeSlot.paymentTypes}
                    closeHandler={toggleSelectorPaymentType}
                />
            )}
        </S.Container>
    )
}

StockOrderInfo.propTypes = {
    timeString: PropTypes.string.isRequired,
    shipment: PropTypes.object.isRequired,
    preorderInfo: PropTypes.object.isRequired
}

export default StockOrderInfo
