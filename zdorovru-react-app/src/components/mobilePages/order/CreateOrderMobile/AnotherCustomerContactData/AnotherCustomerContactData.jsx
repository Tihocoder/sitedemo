import React from 'react'
import PropTypes from 'prop-types'
import Switch from 'react-ios-switch'
import InputMobile, { MaskType } from '@mobileShared/InputMobile/InputMobile'
import * as S from './AnotherCustomerContactData.styled'

const AnotherCustomerContactData = ({ changeContactData, contactData }) => {
    const changeInput = (name) => async (value) => await changeContactData(name, value)

    return (
        <S.Container>
            <S.ToggleBlock>
                <S.ToggleLabel>Заказ заберет другой человек</S.ToggleLabel>
                <Switch
                    checked={contactData.isEnabled}
                    onChange={async () => {
                        await changeInput('isEnabled')(!contactData.isEnabled)
                    }}
                />
            </S.ToggleBlock>
            {contactData.isEnabled && <S.InputsBlock>
                <InputMobile
                    label="Имя"
                    placeholder="Обязательно"
                    changeHandler={changeInput('name')}
                    value={contactData.name}
                />
                <InputMobile
                    label="Телефон"
                    placeholder="Обязательно"
                    changeHandler={changeInput('phone')}
                    inputMaskProps={{
                        isEnabled: true,
                        type: MaskType.phone
                    }}
                    value={contactData.phone}
                />
                <InputMobile
                    label="E-mail"
                    placeholder="Не обязательно"
                    changeHandler={changeInput('email')}
                    value={contactData.email}
                />
            </S.InputsBlock>}
        </S.Container>
    )
}

AnotherCustomerContactData.propTypes = {
    changeContactData: PropTypes.func.isRequired,
    // eslint-disable-next-line react/forbid-prop-types
    contactData: PropTypes.any.isRequired
}

export default AnotherCustomerContactData
