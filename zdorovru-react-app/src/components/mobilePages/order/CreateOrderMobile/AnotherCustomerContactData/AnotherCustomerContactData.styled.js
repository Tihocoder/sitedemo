import styled from 'styled-components'
import * as InputStyled from '@mobileShared/InputMobile/InputMobile.styled'

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    background-color: white;
`
export const ToggleBlock = styled.div`
    display: flex;
    padding: 0.5rem 1rem;
    align-items: center;
    justify-content: space-between;
`

export const ToggleLabel = styled.span``

// eslint-disable-next-line import/prefer-default-export
export const InputsBlock = styled.div`
  display: flex;
  flex-direction: column;

  ${InputStyled.Container} {
    height: 2rem;
    border-bottom: 1px solid ${(props) => props.theme.colors.getGrayColor(0.1)};
  }
`
