import React, { useState } from 'react'
import { useCreateOrder } from '@hooks/order/order-hooks'
import StockOrderInfo from '@mobilePages/order/CreateOrderMobile/StockOrderInfo/StockOrderInfo'
import MobilePageHeader from '@mobileShared/MobilePageHeader/MobilePageHeader'
import BoolSwitcher from '@mobileShared/BoolSwitcher/BoolSwitcher'
import OrderAddressData from '@mobilePages/order/CreateOrderMobile/OrderAddressData/OrderAddressData'
import BlockWithTitle from '@mobileComponents/BlockWithTitle/BlockWithTitle'
import { MobileBody } from '@styles/ui-element.styled'
import InputMobile, { MaskType } from '@mobileShared/InputMobile/InputMobile'
import { InputsMobileBlock } from '@mobileShared/InputMobile/InputMobile.styled'
import MobileRowWithIconItem, {
    IconsMobileRowItem
} from '@mobileShared/MobileRowWithIconItem/MobileRowWithIconItem'
import AccountProfile from '@mobilePages/account/AccountProfile/AccountProfile'
import PropTypes from 'prop-types'
import * as Theme from '@styles/theme-const'
import SymbolRuble from '@shared/SymbolRuble/SymbolRuble'
import Button from '@shared/Button/Button'
import { useRouter } from 'next/router'
import Link from 'next/link'
import ModalWindow from '@shared/ModalWindow/ModalWindow'
import MobileHeader from '@mobileShared/MobileHeader/MobileHeader'
import TimeSlotSelector from '@mobilePages/order/CreateOrderMobile/OrderAddressData/TimeSlotSelector/TimeSlotSelector'
import PaymentTypeSelector from '@mobilePages/order/CreateOrderMobile/OrderAddressData/PaymentTypeSelector/PaymentTypeSelector'
import * as S from './CreateOrderMobile.styled'

const CreateOrderMobile = (props) => {
    const createOrder = useCreateOrder(props)
    const [isShowedContactDataModal, setIsShowedContactDataModal] = useState(false)
    const router = useRouter()
    const toggleShowedContactDataModal = () => setIsShowedContactDataModal(!isShowedContactDataModal)
    const { name, email, phone } = createOrder.stateFormData.userContacts
    const isContactData = name || email || phone
    const [openedSelectorTimeSlots, setOpenedSelectorTimeSlots] = useState(false)
    const [openedSelectorPaymentType, setOpenedSelectorPaymentType] = useState(false)
    const [messageNotSelectedTimeSlot, setMessageNotSelectedTimeSlot] = useState(false)
    const toggleMessageNotSelectedTimeSlot = () => setMessageNotSelectedTimeSlot(!messageNotSelectedTimeSlot)
    return (
        <S.Container>
            <MobileHeader>{props.isDelivery ? 'Оформление доставки' : 'Оформление самовывоза'}</MobileHeader>
            <MobileBody>
                <S.BodyContainer>
                    {props.isDelivery ? (
                        <OrderAddressData
                            createOrder={createOrder}
                            changeDeliveryData={createOrder.changeDeliveryDataHandler}
                            timeSlots={props.timeSlots}
                            validateDeliveryData={createOrder.stateFormData.validate.deliveryData}
                            deliveryData={createOrder.stateFormData.deliveryData}
                            preorderInfo={props.preorderInfo}
                            shipment={createOrder.shipment}
                        />
                    ) : (
                        <StockOrderInfo
                            createOrder={createOrder}
                            timeSlots={props.timeSlots}
                            timeString={createOrder.selectedShipment?.timeString || ''}
                            preorderInfo={props.preorderInfo}
                            shipment={createOrder.shipment}
                        />
                    )}
                    <BlockWithTitle title="Личные данные">
                        {createOrder.isLogging ? (
                            <MobileRowWithIconItem
                                title="Личные данные"
                                clickHandler={toggleShowedContactDataModal}
                                isShowedRow
                                iconName={IconsMobileRowItem.UserCreateOrder}>
                                {isContactData ? (
                                    <React.Fragment>
                                        {name && <S.ContactDataItem>{name}</S.ContactDataItem>}
                                        {phone && (
                                            <S.ContactDataItem>
                                                {phone.replace(
                                                    /(\d{1})(\d{3})(\d{3})(\d{2})(\d{2})/,
                                                    '+$1 ($2) $3-$4-$5'
                                                )}
                                            </S.ContactDataItem>
                                        )}
                                        {email && <S.ContactDataItem>{email}</S.ContactDataItem>}
                                    </React.Fragment>
                                ) : (
                                    <S.ContactDataItem>Введите свои контактные данные</S.ContactDataItem>
                                )}
                                <S.ChangeTitle>Изменить мои данные</S.ChangeTitle>
                            </MobileRowWithIconItem>
                        ) : (
                            <InputsMobileBlock>
                                <InputMobile
                                    label="Имя"
                                    placeholder={props.isDelivery ? 'Обязательно' : 'Не обязательно'}
                                    changeHandler={async (value) => {
                                        await createOrder.changeContactDataHandler('name', value)
                                    }}
                                    isValid={createOrder.stateFormData.validate.form.name}
                                    value={createOrder.stateFormData.userContacts.name}
                                />
                                <InputMobile
                                    label="Телефон"
                                    placeholder="Обязательно"
                                    changeHandler={async (value) => {
                                        await createOrder.changeContactDataHandler('phone', value)
                                    }}
                                    inputMaskProps={{
                                        isEnabled: true,
                                        type: MaskType.phone
                                    }}
                                    isValid={createOrder.stateFormData.validate.form.phone}
                                    value={createOrder.stateFormData.userContacts.phone}
                                />
                                <InputMobile
                                    label="E-mail"
                                    placeholder={props.isDelivery ? 'Для отправки чека' : 'Для подтверждения'}
                                    changeHandler={async (value) => {
                                        await createOrder.changeContactDataHandler('email', value)
                                    }}
                                    isValid={createOrder.stateFormData.validate.form.email}
                                    value={createOrder.stateFormData.userContacts.email}
                                />
                            </InputsMobileBlock>
                        )}
                    </BlockWithTitle>
                    <BlockWithTitle title="Способ подтверждения заказа">
                        <S.WhiteAndPaddingContainer>
                            <BoolSwitcher
                                isActive={!createOrder.stateFormData.userContacts.isNeedOperatorCall}
                                clickHandlers={{
                                    tab1Handler: () =>
                                        createOrder.changeContactDataHandler('isNeedOperatorCall', false),
                                    tab2Handler: () =>
                                        createOrder.changeContactDataHandler('isNeedOperatorCall', true)
                                }}
                                titles={{
                                    tab2Title: 'Звонок оператора',
                                    tab1Title: 'СМС'
                                }}
                                isDefault
                            />
                        </S.WhiteAndPaddingContainer>
                    </BlockWithTitle>
                    {isShowedContactDataModal && (
                        <AccountProfile
                            profile={props.customerProfile.profile}
                            genderTypes={props.customerProfile.genderTypes}
                            custom={{
                                isCustom: true,
                                closeHandler: () => {
                                    router.reload()
                                    toggleShowedContactDataModal()
                                }
                            }}
                        />
                    )}
                    <S.Wrapper>
                        {props.isDelivery && (
                            <React.Fragment>
                                <S.BetweenCenter>
                                    <S.PriceDescription>Стоимость товаров</S.PriceDescription>
                                    <S.PriceSumDefault>
                                        {createOrder.sums.sumDelivery.toLocaleString('RU-ru')} <SymbolRuble />
                                    </S.PriceSumDefault>
                                </S.BetweenCenter>
                                <S.BetweenCenter>
                                    <S.PriceDescription>Стоимость доставки</S.PriceDescription>
                                    <S.PriceSumDefault>
                                        {createOrder.sums.deliveryCost.toLocaleString('RU-ru')}{' '}
                                        <SymbolRuble />
                                    </S.PriceSumDefault>
                                </S.BetweenCenter>
                            </React.Fragment>
                        )}
                        <S.BetweenCenter>
                            <S.PriceDescription>Общая стоимость</S.PriceDescription>
                            {props.isDelivery ? (
                                <S.PriceSum>
                                    {createOrder.sums.sumDeliveryFull.toLocaleString('RU-ru')} <SymbolRuble />
                                </S.PriceSum>
                            ) : (
                                <S.PriceSum>
                                    {createOrder.sums.sumStock.toLocaleString('RU-ru')} <SymbolRuble />
                                </S.PriceSum>
                            )}
                        </S.BetweenCenter>
                        <S.ButtonCreateOrderContainer>
                            <Button
                                elementSizeTypeValue={Theme.ElementSizeType.large}
                                disabled={createOrder.isWaitingSubmit}
                                clickHandler={createOrder.submitHandler}>
                                {props.isDelivery ? 'Оформить доставку' : 'Оформить самовывоз'}
                            </Button>
                        </S.ButtonCreateOrderContainer>
                    </S.Wrapper>
                    <S.Wrapper>
                        <S.FooterDescription>
                            Нажимая кнопку “{props.isDelivery ? 'Оформить доставку' : 'Оформить самовывоз'}”,
                            Вы подтверждаете наличие рецепта и соглашаетесь с{' '}
                            <Link href="/privacy-policy" passHref>
                                <S.LinkBlue>Политикой конфиденциальности</S.LinkBlue>
                            </Link>
                            ,{' '}
                            <Link href="/terms-of-use" passHref>
                                <S.LinkBlue>Пользовательским соглашением</S.LinkBlue>
                            </Link>{' '}
                            и{' '}
                            {props.isDelivery ? (
                                <Link href="/condition/delivery" passHref>
                                    <S.LinkBlue>Условиями доставки</S.LinkBlue>
                                </Link>
                            ) : (
                                <Link href="/condition/pickup" passHref>
                                    <S.LinkBlue>Условиями самовывоза</S.LinkBlue>
                                </Link>
                            )}
                            .
                        </S.FooterDescription>
                    </S.Wrapper>
                </S.BodyContainer>
            </MobileBody>
            {createOrder.isInvalidMessage && (
                <ModalWindow
                    title="Внимание"
                    isMobile
                    isButtonFooter={false}
                    isFooterOnlyOneButton
                    submit={{
                        text: 'Понятно',
                        clickHandler: createOrder.toggleIsInvalidMessage
                    }}
                    cancel={{
                        text: 'Отмена',
                        clickHandler: createOrder.toggleIsInvalidMessage
                    }}>
                    <S.BodyModalContainer>Заполните необходимые поля</S.BodyModalContainer>
                </ModalWindow>
            )}
            {openedSelectorTimeSlots && (
                <TimeSlotSelector
                    timeSlots={props.timeSlots}
                    closeHandler={() => setOpenedSelectorTimeSlots(!openedSelectorTimeSlots)}
                    selectTimeSlotHandler={createOrder.changeTimeSlotHandler}
                    shipTimeSlotId={createOrder.stateFormData.shipTimeSlotInfo.shipTimeSlotId}
                />
            )}
            {openedSelectorPaymentType && (
                <PaymentTypeSelector
                    changeHandler={createOrder.changePaymentType}
                    selectType={createOrder.stateFormData.paymentType?.paymentTypeId}
                    paymentTypes={createOrder.paymentTypes}
                    closeHandler={() => setOpenedSelectorPaymentType(!openedSelectorPaymentType)}
                />
            )}
            {messageNotSelectedTimeSlot && (
                <ModalWindow
                    title="Внимание"
                    isMobile
                    isButtonFooter={false}
                    isFooterOnlyOneButton
                    submit={{
                        text: 'Понятно',
                        clickHandler: toggleMessageNotSelectedTimeSlot
                    }}
                    cancel={{
                        text: 'Отмена',
                        clickHandler: toggleMessageNotSelectedTimeSlot
                    }}>
                    <S.BodyModalContainer>
                        Для выбора способа оплаты сначала необходимо выбрать интервал доставки
                    </S.BodyModalContainer>
                </ModalWindow>
            )}
        </S.Container>
    )
}

export default CreateOrderMobile

CreateOrderMobile.propTypes = {
    isDelivery: PropTypes.bool.isRequired,
    timeSlots: PropTypes.array,
    customerProfile: PropTypes.object.isRequired
}
