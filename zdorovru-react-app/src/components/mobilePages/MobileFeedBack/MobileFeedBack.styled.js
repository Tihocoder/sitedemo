import styled, { css } from 'styled-components'

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    background-color: ${(props) => props.theme.colors.gray5};
    color: ${(props) => props.theme.colors.getGrayColor()};
`
export const ChangeTitle = styled.span`
    font-size: 0.85rem;
    color: ${(props) => props.theme.colors.getGrayColor(0.5)};
`
export const SpanDataItem = styled.span`
    color: ${(props) => props.theme.colors.getGrayColor(1)};
`
export const TextAreaContainer = styled.div`
    display: flex;
    align-items: center;
    padding: 0.5rem 1rem;
`
export const TextArea = styled.textarea`
    font: inherit;
    background: transparent;
    border: none;
    outline: none;
    padding: 0;
    margin: 0;
    height: 100%;
    width: 100%;
    max-width: 100%;
    vertical-align: bottom;
    text-align: inherit;
    box-sizing: content-box;
    -webkit-appearance: none;

    &::placeholder {
        color: ${(props) => props.theme.colors.getGrayColor(0.5)};
    }

    ${(props) =>
        !props.isValid &&
        css`
            color: ${props.theme.colors.getRedheadColor()};

            &::placeholder {
                color: ${props.theme.colors.getRedheadColor()};
            }
        `}
`

export const TextAreaMobileBlock = styled.div`
    display: flex;
    flex-direction: column;
    box-sizing: border-box;

    ${TextAreaContainer} {
        height: 6rem;
        border-bottom: 1px solid ${(props) => props.theme.colors.getGrayColor(0.1)};
    }
`

export const ButtonsContainer = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
    height: 5rem;
    padding: 0.5rem;
    justify-content: space-between;
    margin-top: 0.4rem;
    box-sizing: border-box;
`
