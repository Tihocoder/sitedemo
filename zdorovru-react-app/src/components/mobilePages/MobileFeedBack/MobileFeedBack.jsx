import React, { useState } from 'react'
import { InputsMobileBlock } from '@mobileShared/InputMobile/InputMobile.styled'
import InputMobile, { MaskType } from '@mobileShared/InputMobile/InputMobile'
import MobilePageHeader from '@mobileShared/MobilePageHeader/MobilePageHeader'
import { useFeedBack } from '@hooks/feedback-hook'
import BlockWithTitle from '@mobileComponents/BlockWithTitle/BlockWithTitle'
import MobileRowWithIconItem from '@mobileShared/MobileRowWithIconItem/MobileRowWithIconItem'
import MobileStockSelector from '@mobilePages/MobileStockSelector/MobileStockSelector'
import Button from '@shared/Button/Button'
import * as Theme from '@styles/theme-const'
import * as S from './MobileFeedBack.styled'
import { MobileBody } from '@styles/ui-element.styled'
import MobileHeader from "@mobileShared/MobileHeader/MobileHeader";

const MobileFeedBack = () => {
    const feedBack = useFeedBack()

    const [isStockSelector, setIsStockSelector] = useState(false)
    const toggleIsStockSelector = () => setIsStockSelector(!isStockSelector)
    return (
        <S.Container>
            {isStockSelector && (
                <MobileStockSelector
                    closeHandler={toggleIsStockSelector}
                    changeOptions={{
                        isCustomChange: true,
                        changeHandlerStockId: feedBack.selectedStockHandler
                    }}
                />
            )}
            <MobileHeader>Обратная связь</MobileHeader>
            <MobileBody>
                <BlockWithTitle title="Контактные данные">
                    <InputsMobileBlock>
                        <InputMobile
                            changeHandler={feedBack.changeFormDataValue('firstName')}
                            value={feedBack.formDataFeedBack.firstNam}
                            label="Имя"
                            isValid={feedBack.validateForm.firstName}
                            placeholder="Не обязательно"
                        />
                        <InputMobile
                            label="Телефон"
                            changeHandler={feedBack.changeFormDataValue('phone')}
                            value={feedBack.formDataFeedBack.phone}
                            inputMaskProps={{
                                isEnabled: true,
                                type: MaskType.phone
                            }}
                            placeholder="Обязательно"
                            customInputProps={{
                                type: 'tel',
                                disableUnderline: true
                            }}
                        />
                        <InputMobile
                            changeHandler={feedBack.changeFormDataValue('email')}
                            label="e-mail"
                            isValid={feedBack.validateForm.email}
                            placeholder="Желательно"
                            value={feedBack.formDataFeedBack.email}
                        />
                    </InputsMobileBlock>
                </BlockWithTitle>
                <BlockWithTitle title="Аптека">
                    <MobileRowWithIconItem iconName="geo" isShowedRow clickHandler={toggleIsStockSelector}>
                        <S.SpanDataItem>
                            {feedBack.formDataFeedBack.stockTitle || 'Не выбрана'}
                        </S.SpanDataItem>
                        <S.ChangeTitle>Выбрать аптеку</S.ChangeTitle>
                    </MobileRowWithIconItem>
                </BlockWithTitle>
                <BlockWithTitle title="Контактные данные">
                    <InputsMobileBlock>
                        <InputMobile
                            changeHandler={feedBack.changeFormDataValue('subjectMessage')}
                            isValid={feedBack.validateForm.subjectMessage}
                            placeholder="Тема"
                            value={feedBack.formDataFeedBack.subjectMessage}
                        />
                    </InputsMobileBlock>
                    <S.TextAreaMobileBlock>
                        <S.TextAreaContainer>
                            <S.TextArea
                                onChange={feedBack.changeFormData('bodyMessage')}
                                isValid={feedBack.validateForm.bodyMessage}
                                value={feedBack.formDataFeedBack.bodyMessage}
                                placeholder="Сообщение"
                            />
                        </S.TextAreaContainer>
                    </S.TextAreaMobileBlock>
                    <S.ButtonsContainer>
                        <Button
                            elementSizeTypeValue={Theme.ElementSizeType.regular}
                            clickHandler={feedBack.submitHandler}>
                            Отправить сообщение
                        </Button>
                    </S.ButtonsContainer>
                </BlockWithTitle>
            </MobileBody>
        </S.Container>
    )
}

MobileFeedBack.propTypes = {}

export default MobileFeedBack
