import React, { useContext, useEffect } from 'react'
import { SearchContext } from '../../Layout/MobileLayout'
import * as S from './Home.styled'
import IconSearch from '../../Icons/IconSearch'
import UpperBlocks from './UpperBlocks/UpperBlocks'
import BannersCarousel from './BannersCarusel/BannersCarousel'
import { useRouter } from 'next/router'
import SearchBlock from '@mobileShared/MobileHeader/SearchBlock/SearchBlock'

const Home = () => {
    const { searchText, changeSearchText, searchButtonHandler } = useContext(SearchContext)
    const router = useRouter()
    const changeSearchHandler = (e) => {
        changeSearchText(e.target.value)
    }
    useEffect(() => {
        changeSearchText('')
    }, [router.asPath])
    const keyDownHandler = async (e) => {
        if (e.key === 'Enter') {
            if (searchText.length > 2) {
                await router.push({
                    pathname: '/catalog/search',
                    query: {
                        q: searchText
                    }
                })
            }
        }
    }
    return (
        <S.Container>
            <S.Header>
                <S.HeaderBody>
                    <S.LogoContainer>
                        <S.GeneralLogo />
                    </S.LogoContainer>
                </S.HeaderBody>
                <SearchBlock
                    searchInput={{
                        searchText,
                        keyDownHandler,
                        changeSearchHandler,
                        searchButtonHandler,
                        placeholder: 'Поиск на ЗДОРОВ.ру'
                    }}
                />
            </S.Header>
            <S.Body>
                <UpperBlocks />
                <BannersCarousel />
            </S.Body>
        </S.Container>
    )
}

Home.propTypes = {}

export default Home
