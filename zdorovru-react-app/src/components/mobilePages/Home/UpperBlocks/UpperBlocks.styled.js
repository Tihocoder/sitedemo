import styled from 'styled-components'

export const Container = styled.div`
    display: flex;
    height: 6.8rem;
    padding: 0 0.5rem 1.5rem 0.5rem;
    box-sizing: border-box;
    justify-content: space-between;
`

export const Block = styled.a`
    width: 42%;
    height: 100%;
    cursor: pointer;
    display: flex;
    transition: all 0.2s ease;
    padding: 0.4rem 0.8rem;
    list-style-type: none;
    line-height: 1rem;
    border-radius: 0.3rem;
    box-shadow: 0 2px 8px #b4b4bb, 0 2px 4px #d2d2d6;
`

export const IconContainer = styled.div`
    display: flex;
    justify-content: flex-start;
    width: 2.1rem;
`

export const Icon = styled.img`
    width: 100%;
    height: auto;
`

export const Description = styled.div`
    font-size: 1.25rem;
    color: ${(props) => props.theme.colors.getGrayColor()};
    display: flex;
    line-height: 2rem;
    align-items: center;
    margin-left: 1rem;
`
export const SpanDescription = styled.span`
    width: 5rem;
`
