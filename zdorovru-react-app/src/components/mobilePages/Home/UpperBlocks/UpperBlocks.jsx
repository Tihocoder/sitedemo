import React, { useState } from 'react'
import Link from 'next/link'
import { useSelector } from 'react-redux'
import MobileStockSelector from '@mobilePages/MobileStockSelector/MobileStockSelector'
import * as S from './UpperBlocks.styled'

const UpperBlocks = (props) => {
    const shipment = useSelector((state) => state.customer.shipment)
    const [isStockSelector, setIsStockSelector] = useState(false)
    const toggleStockSelector = () => setIsStockSelector(!isStockSelector)
    return (
        <S.Container>
            {isStockSelector && <MobileStockSelector closeHandler={toggleStockSelector} />}
            <S.Block onClick={toggleStockSelector}>
                <S.IconContainer>
                    <S.Icon src={`${process.env.BASE_PATH}/images/room.svg`} />
                </S.IconContainer>
                <S.Description>
                    <S.SpanDescription>Адреса аптек </S.SpanDescription>
                </S.Description>
            </S.Block>
            <Link href="/catalog/promotion/list" passHref>
                <S.Block>
                    <S.IconContainer>
                        <S.Icon src={`${process.env.BASE_PATH}/images/sale-mobile.svg`} />
                    </S.IconContainer>
                    <S.Description>
                        <S.SpanDescription>Скидки и акции</S.SpanDescription>
                    </S.Description>
                </S.Block>
            </Link>
        </S.Container>
    )
}

UpperBlocks.propTypes = {}

export default UpperBlocks
