import styled from 'styled-components'

export const Container = styled.div`
    width: 100%;
    box-sizing: border-box;
`

export const PromotionImage = styled.img`
    width: 100%;
    height: auto;
`

export const CarouselItem = styled.div``

export const CarouselImage = styled.img``
