import React, { useEffect, useState } from 'react'
import { getMobileBanners } from '@api/siteApis'
import { Carousel } from 'react-responsive-carousel'
import * as S from './BannersCarusel.styled'
import 'react-responsive-carousel/lib/styles/carousel.min.css'
import { useRouter } from 'next/router'

const BannersCarousel = () => {
    const router = useRouter()
    const [promotions, setPromotions] = useState([])

    const initPromotions = async () => {
        const dbpromotions = await getMobileBanners()
        setPromotions(dbpromotions)
    }

    useEffect(() => {
        initPromotions()
    }, [])

    if (!promotions?.length) {
        return <S.Container />
    }
    const clickItemHandler = async (e, item) => {
        await router.push(`/catalog/promotion/${item.props.promotionId}`)
    }
    return (
        <S.Container>
            <Carousel
                autoPlay
                interval={5000}
                infiniteLoop
                showThumbs={false}
                onClickItem={clickItemHandler}
                showStatus={false}>
                {promotions.map((x) => (
                    <S.CarouselItem key={x.id} id={x.id} promotionId={x.promotionId}>
                        <S.CarouselImage src={x.imageUrl} />
                    </S.CarouselItem>
                ))}
            </Carousel>
        </S.Container>
    )
}

BannersCarousel.propTypes = {}

export default BannersCarousel
