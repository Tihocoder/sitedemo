import styled from 'styled-components'

export const Container = styled.div``

export const Header = styled.header`
    // height: 3.5rem;
    background-color: ${(props) => props.theme.colors.white};
    color: ${(props) => props.theme.colors.getGrayColor(1)};
    display: flex;
    flex-direction: column;
    /* justify-content: space-between; */
    padding: 0.4rem;
    /* align-items: center; */
`

export const HeaderBody = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 0.438rem;
`

export const ArrowBlock = styled.div`
    height: 100%;
    width: 3rem;
    box-sizing: border-box;
`
export const HeaderText = styled.span`
    width: 100%;
    text-align: center;
`

export const LogoContainer = styled.div`
    width: 15rem;
`

export const SerachBlock = styled.div`
    position: relative;
    height: 2.375rem;
`

export const Input = styled.input`
    width: 100%;
    padding: 0.75rem 1.2rem 0.75rem 2.5rem;
    font-size: 1.25rem;
    height: 100%;
    color: ${(props) => props.theme.colors.getGrayColor(0.7)};
    box-sizing: border-box;
    outline: none;
    -webkit-appearance: none;
    border-radius: 0.438rem;
    border: 2px solid ${(props) => props.theme.colors.getGreenColor()};

    &::-webkit-search-cancel-button {
        -webkit-appearance: none;
        height: 1em;
        width: 1em;
        background: ${`url('${process.env.BASE_PATH}/images/close-mobile-red.svg')`} no-repeat 50% 50%;
        background-size: contain;
        opacity: 0;
        pointer-events: none;
    }

    &:focus::-webkit-search-cancel-button {
        opacity: 1;
        pointer-events: all;
    }
`
export const IconContainer = styled.div`
    position: absolute;
    width: fit-content;
    height: fit-content;
    top: 0.5rem;
    left: 0.6rem;
    cursor: pointer;
`

export const GeneralLogo = styled.img.attrs({
    src: `${process.env.BASE_PATH}/images/logo-mobile.svg`
})`
    width: 100%;
    height: auto;
`

export const Body = styled.div``
