import React, { useEffect, useState } from 'react'
import ReactDOM from 'react-dom'
import ReactDOMServer from 'react-dom/server'
import PropTypes from 'prop-types'
import { useDispatch, useSelector } from 'react-redux'
import { useRouter } from 'next/router'
import { changeShipment } from '@actions/customer'
import {
    FullscreenControl,
    GeolocationControl,
    Map,
    Placemark,
    RouteButton,
    RulerControl,
    SearchControl,
    TrafficControl,
    TypeSelector,
    YMaps,
    ZoomControl
} from 'react-yandex-maps'
import { MobileBody } from '@styles/ui-element.styled'
import * as _ from 'lodash'
import { getStocksGoods } from '@api/siteApis'
import MobileHeader from '@mobileShared/MobileHeader/MobileHeader'
import * as S from './MobileStockSelector.styled'
import MobilePageHeader from '../../mobileComponents/mobileShared/MobilePageHeader/MobilePageHeader'
import StockItem from '../../libs/Stock/StockItem/StockItem'
import MobileRowItem from '../../mobileComponents/mobileShared/MobileRowItem/MobileRowItem'
import MobileCitySelector from '../MobileCitySelector/MobileCitySelector'
import BalloonMap from '../../SelectorStock/BalloonMap/BalloonMap'

const MobileStockSelector = (props) => {
    const stocks = useSelector((state) => state.company.stocks)
    const cities = useSelector((state) => state.company.cities)
    const shipment = useSelector((state) => state.customer.shipment)
    const router = useRouter()
    const dispatch = useDispatch()

    const [selectedCityId, setSelectedCityId] = useState(1)
    const [isShowedCitySelector, setIsShowedCitySelector] = useState(false)
    const [isStockListViewType, setIsStockListViewType] = useState(true)
    const [searchText, setSearchText] = useState('')
    const [ymaps, setYmaps] = useState(null)
    const [selectedStocks, setSelectedStocks] = useState([])
    const [mapState, setMapState] = useState({
        center: [55.751574, 37.573856],
        zoom: 9
    })

    const changeSearch = (e) => {
        setSearchText(e.target.value)
    }

    const getLayout = (Component, props) => {
        if (ymaps && ymaps.templateLayoutFactory) {
            const html = ReactDOMServer.renderToString(<Component {...props} />)
            const Layout = ymaps.templateLayoutFactory.createClass(`<div id="balloon">${html}</div>`, {
                build() {
                    Layout.superclass.build.call(this)
                    ReactDOM.hydrate(<Component {...props} />, document.getElementById('balloon'))
                }
            })

            return Layout
        }
        return null
    }

    useEffect(() => {
        const city = cities.find((x) => x.id === shipment.cityId)
        setSelectedCityId(shipment.cityId)
        setMapState({
            center: [city.coordinateX, city.coordinateY],
            zoom: city.mapZoom
        })
    }, [shipment.cityId])
    const getFilteredStocks = async () => {
        let filteredStocks = stocks.filter((x) => x.cityId === selectedCityId && x.isOpened)
        if (props.changeOptions.customFilterStock.isEnabled) {
            const newStocksGoods = await getStocksGoods(
                selectedCityId,
                props.changeOptions.customFilterStock.filteredGoodsId
            )
            filteredStocks = filteredStocks.filter((st) => newStocksGoods.some((nws) => nws.id === st.id))
        }

        if (searchText) {
            filteredStocks = filteredStocks.filter((stock) =>
                `${stock.name} ${stock.city} ${stock.address}`
                    .toLowerCase()
                    .includes(searchText.toLowerCase())
            )
        }
        setSelectedStocks(_.orderBy(filteredStocks, 'name'))
    }
    useEffect(() => {
        getFilteredStocks()
        return () => {
            setSelectedStocks([])
        }
    }, [selectedCityId, props.changeOptions.customFilterStock.filteredGoodsId, searchText])

    const selectedStockHandler = async (id) => {
        if (props.changeOptions.isCustomChange) {
            await props.changeOptions.changeHandlerStockId(id)
            await props.closeHandler()
            return
        }
        await dispatch(
            changeShipment({
                stockId: id,
                cityId: selectedCityId
            })
        )
        await props.closeHandler()
        if (!props.isNotReload) router.reload()
    }

    const clickPlacemarkHandler = (id) => () => {
        const stock = stocks.find((stock) => stock.id === id)
        setMapState({
            ...mapState,
            center: [stock.coordinateX, stock.coordinateY],
            zoom: 16
        })
    }

    const toggleShowedCitySelector = () => {
        setIsShowedCitySelector(!isShowedCitySelector)
    }

    const city = cities.find((x) => x.id === selectedCityId)

    const selectedCity = (id) => {
        const city = cities.find((x) => x.id === id)
        setMapState({
            center: [city.coordinateX, city.coordinateY],
            zoom: city.mapZoom
        })
        setSelectedCityId(id)
        setIsShowedCitySelector(false)
    }

    if (isShowedCitySelector) {
        return (
            <MobileCitySelector
                closeHandler={toggleShowedCitySelector}
                cityId={selectedCityId}
                changeCityId={selectedCity}
            />
        )
    }

    const bodyListView = () => (
        <S.StocksContainer>
            {selectedStocks.map((x) => (
                <MobileRowItem
                    key={x.id}
                    isSelected={x.id === shipment.stockId}
                    clickHandler={() => selectedStockHandler(x.id)}>
                    <S.StockItemBlock>
                        <StockItem
                            isMetroLocation={x.isMetroLocation}
                            metroColor={x.metroColor}
                            title={x.title}
                            name={x.name}
                        />
                        <S.StockItemRowText>{x.address}</S.StockItemRowText>
                        <S.StockItemRowText>{x.timeString}</S.StockItemRowText>
                    </S.StockItemBlock>
                </MobileRowItem>
            ))}
        </S.StocksContainer>
    )

    const bodyMapView = () => (
        <S.MapContainer>
            <YMaps>
                <Map
                    modules={[
                        'geolocation',
                        'geocode',
                        'templateLayoutFactory',
                        'geoObject.addon.balloon',
                        'geoObject.addon.hint'
                    ]}
                    onLoad={(ymaps) => setYmaps(ymaps)}
                    width="100%"
                    height="100%"
                    state={{
                        center: mapState.center,
                        zoom: mapState.zoom
                    }}>
                    <FullscreenControl />
                    <GeolocationControl options={{ float: 'left' }} />
                    <RouteButton options={{ float: 'right' }} />
                    <RulerControl options={{ float: 'right' }} />
                    <TypeSelector options={{ float: 'right' }} />
                    <TrafficControl options={{ float: 'right' }} />
                    <SearchControl options={{ float: 'left' }} />
                    <ZoomControl options={{ float: 'right' }} />
                    {selectedStocks.map((stock) => (
                        <Placemark
                            key={stock.id}
                            onClick={clickPlacemarkHandler(stock.id)}
                            options={{
                                iconLayout: 'default#image',
                                iconImageHref: stock.isOpened
                                    ? `${process.env.BASE_PATH || ''}/images/map_logo.png`
                                    : `${process.env.BASE_PATH || ''}/images/map_logo_notopened.png`,
                                iconImageSize: [25, 23],
                                iconImageOffset: [-13, -14],
                                balloonCloseButton: true,
                                hideIconOnBalloonOpen: false,
                                hintContent: stock.title,
                                balloonContentLayout: getLayout(BalloonMap, {
                                    stock,
                                    changeStockHandler: () => selectedStockHandler(stock.id)
                                })
                            }}
                            geometry={[stock.coordinateX, stock.coordinateY]}
                        />
                    ))}
                </Map>
            </YMaps>
        </S.MapContainer>
    )

    return (
        <S.Container>
            <MobileHeader
                isEnabledSearchButton={false}
                searchInput={{
                    isShowed: true,
                    searchText,
                    changeSearchHandler: changeSearch,
                    isEnabledSearchButton: false,
                    placeholder: 'Адрес, метро, город, ТЦ'
                }}
                backClickHandler={props.closeHandler}
                isDefaultRouterBack={false}>
                Изменить аптеку самовывоза
            </MobileHeader>
            <MobileBody isExtendBlock isNotOverflow>
                <MobileRowItem clickHandler={toggleShowedCitySelector} isShowedRow>
                    <S.CityOpenBlock>
                        <S.CityChangeImage />
                        <S.CityOpenSelected>
                            <S.CitySelectedSpan>{city.title}</S.CitySelectedSpan>
                            <S.CityChangeDescriptionSpan>Изменить регион</S.CityChangeDescriptionSpan>
                        </S.CityOpenSelected>
                    </S.CityOpenBlock>
                </MobileRowItem>
                <S.ToggleStockViewType>
                    <S.ButtonStockViewType
                        onClick={() => setIsStockListViewType(true)}
                        isActive={isStockListViewType}>
                        Список аптек
                    </S.ButtonStockViewType>
                    <S.ButtonStockViewType
                        onClick={() => setIsStockListViewType(false)}
                        isActive={!isStockListViewType}>
                        Аптеки на карте
                    </S.ButtonStockViewType>
                </S.ToggleStockViewType>
                {isStockListViewType ? bodyListView() : bodyMapView()}
            </MobileBody>
        </S.Container>
    )
}

MobileStockSelector.propTypes = {
    closeHandler: PropTypes.func.isRequired,
    isNotReload: PropTypes.bool,
    changeOptions: PropTypes.shape({
        isCustomChange: PropTypes.bool,
        changeHandlerStockId: PropTypes.func,
        customFilterStock: PropTypes.shape({
            isEnabled: PropTypes.bool.isRequired,
            filteredGoodsId: PropTypes.arrayOf(PropTypes.number)
        })
    })
}
MobileStockSelector.defaultProps = {
    changeOptions: {
        isCustomChange: false,
        changeHandlerStockId: () => {},
        customFilterStock: {
            isEnabled: false,
            filteredGoodsId: []
        }
    },
    isNotReload: false
}

export default MobileStockSelector
