import styled, { css } from 'styled-components'

export const Container = styled.div`
    background-color: white;
    position: fixed;
    height: 100%;
    width: 100%;
    top: 0;
    left: 0;
    z-index: 6;
`

export const Header = styled.div`
    background-color: ${(props) => props.theme.colors.mobileGreenLight};
    padding: 0.2rem;
    height: 3rem;
`

export const SerachBlock = styled.div`
    position: relative;
    height: 2.375rem;
`

export const Input = styled.input`
    width: 100%;
    padding: 0.75rem 1.2rem;
    padding-left: 2.5rem;
    font-size: 1.25rem;
    height: 100%;
    border: 0;
    color: ${(props) => props.theme.colors.getGrayColor(0.7)};
    box-sizing: border-box;
    outline: none;
    -webkit-appearance: none;
    border-radius: 7px;
`
export const IconContainer = styled.div`
    position: absolute;
    width: fit-content;
    height: fit-content;
    top: 0.5rem;
    left: 0.6rem;
    cursor: pointer;
`

export const StocksContainer = styled.div`
    display: flex;
    flex-direction: column;
    & > div {
        border-bottom: 1px solid ${(props) => props.theme.colors.getGrayColor(0.1)};
    }
    overflow-x: auto;
    max-height: calc(100% - 6rem);
    box-sizing: border-box;
`
export const StockItemBlock = styled.div`
    display: flex;
    flex-direction: column;
`

export const StockItemRowText = styled.span`
    color: ${(props) => props.theme.colors.getGrayColor(0.6)};
`
export const CityOpenBlock = styled.div`
    display: flex;
`

export const CityOpenSelected = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    margin-left: 0.5rem;
`

export const CitySelectedSpan = styled.span`
    color: ${(props) => props.theme.colors.getGrayColor(1)};
`

export const CityChangeDescriptionSpan = styled.span`
    color: ${(props) => props.theme.colors.getGrayColor(0.6)};
`

export const CityChangeImage = styled.img.attrs({
    src: `${process.env.BASE_PATH}/images/compass.svg`
})``

export const ToggleStockViewType = styled.div`
    display: flex;
    min-width: 15.026rem;
    background-color: ${(props) => props.theme.colors.getGrayColor(0.08)};
    padding: 0.3rem;
`

export const ButtonStockViewType = styled.button`
    border: 0;
    margin: 0;
    padding: 0.5rem 0.5rem;
    font-size: 1rem;
    width: 50%;
    cursor: pointer;
    display: flex;
    justify-content: center;
    align-items: center;
    color: ${(props) => props.theme.colors.getGrayColor(0.5)};
    border-radius: 7px;
    ${(props) =>
        props.isActive &&
        css`
            background-color: ${props.theme.colors.white};
            color: ${(props) => props.theme.colors.getGrayColor(1)};
        `}
`
export const MapContainer = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
    height: 20rem;
`
export const TimeSlotContainer = styled.div``
