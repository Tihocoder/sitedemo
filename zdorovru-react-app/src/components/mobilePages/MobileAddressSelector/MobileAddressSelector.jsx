/* eslint-disable react/no-array-index-key */
import React, { useEffect, useRef, useState } from 'react'
import PropTypes from 'prop-types'
import * as _ from 'lodash'
import { useDispatch } from 'react-redux'
import { useRouter } from 'next/router'
import { getAddressById, getAddresses } from '@api/siteApis'
import { changeShipment } from '@actions/customer'
import { MobileBody } from '@styles/ui-element.styled'
import MobileRowItem from '@mobileShared/MobileRowItem/MobileRowItem'
import BlockWithTitle from '@mobileComponents/BlockWithTitle/BlockWithTitle'
import * as S from './MobileAddressSelector.styled'
import Button from '../../shared/Button/Button'
import MobilePageHeader from '../../mobileComponents/mobileShared/MobilePageHeader/MobilePageHeader'
import { BlockContent, DescriptionText } from './MobileAddressSelector.styled'
import MobileHeader from "@mobileShared/MobileHeader/MobileHeader";

const updateAddresses = _.debounce(async (termText, setAddressList) => {
    if (termText) {
        const addresses = await getAddresses(termText)
        setAddressList([...addresses])
    } else {
        setAddressList([])
    }
}, 250)

const MobileAddressSelector = (props) => {
    const dispatch = useDispatch()
    const router = useRouter()
    const searchRef = useRef(null)
    const [selectedAddress, setSelectedAddress] = useState({
        id: 0,
        title: ''
    })
    const blockHeaderRef = useRef(null)
    const [addressList, setAddressList] = useState([])
    const [isShowedList, setIsShowedList] = useState(false)
    const [heightHeader, setHeightHeader] = useState(null)
    useEffect(() => {
        if (searchRef.current && selectedAddress.id <= 0) {
            searchRef.current.blur()
            searchRef.current.focus()
            searchRef.current.selectionStart = selectedAddress.title.length
            searchRef.current.selectionEnd = selectedAddress.title.length
            searchRef.current.setSelectionRange(
                searchRef.current.value.length,
                searchRef.current.value.length
            )
        }
    }, [selectedAddress.id])

    useEffect(() => {
        if (blockHeaderRef?.current?.getBoundingClientRect().height !== heightHeader)
            setHeightHeader(blockHeaderRef?.current?.getBoundingClientRect().height)
    }, [blockHeaderRef?.current?.clientHeight])

    const changeTextAddressHandler = (e) => {
        const term = e.target.value
        setSelectedAddress({
            id: 0,
            title: term
        })
        updateAddresses(term, setAddressList)
    }

    const selectItemHandler = (id, text) => {
        if (id > 0) {
            searchRef.current.blur()
            setIsShowedList(false)
        }
        const selectAddress = addressList.find((x) => x.id === id)
        const textTitle = id === -1 ? `${text}, ` : selectAddress.text
        setSelectedAddress({
            id,
            title: textTitle
        })
        updateAddresses(textTitle, setAddressList)
    }

    const changeAddress = async () => {
        if (selectedAddress.id !== -1) {
            await dispatch(
                changeShipment({
                    shipAddressId: selectedAddress.id,
                    shipAddressTitle: selectedAddress.title
                })
            )
            props.closeHandler()
            if (!props.isNotReload) await router.reload()
        }
    }

    const initShipAddress = async (id) => {
        const response = await getAddressById(id)
        if (response && response.address) {
            setSelectedAddress({
                id: response.id,
                title: response.address
            })
        }
    }

    useEffect(() => {
        if (props.shipAddressId) initShipAddress(props.shipAddressId).then(() => {})
    }, [props.shipAddressId])
    const focusHandler = () => {
        setIsShowedList(true)
    }
    const blurHandler = () => {
        setIsShowedList(false)
    }
    return (
        <S.Container>
            <MobileHeader
                blockRef={blockHeaderRef}
                isEnabledSearchButton={false}
                searchInput={{
                    isShowed: true,
                    searchText: selectedAddress.title,
                    changeSearchHandler: changeTextAddressHandler,
                    placeholder: 'Введите адрес',
                    searchRef,
                    focusHandler,
                    blurHandler
                }}
                backClickHandler={props.closeHandler}
                isDefaultRouterBack={false}>
                Изменить адрес доставки
            </MobileHeader>
            <MobileBody customPixel={heightHeader}>
                {isShowedList && addressList.length > 0 ? (
                    <S.Header>
                        <S.SerachBlock>
                            <S.ListItem>
                                {addressList.map((item, index) => (
                                    <MobileRowItem
                                        key={index}
                                        id={item.id}
                                        isSelected={item.id === props.shipAddressId}
                                        mouseDownHandler={(e) => e.preventDefault()}
                                        clickHandler={() => selectItemHandler(item.id, item.text)}>
                                        <S.Item>{item.text}</S.Item>
                                    </MobileRowItem>
                                ))}
                            </S.ListItem>
                        </S.SerachBlock>
                    </S.Header>
                ) : (
                    <React.Fragment />
                )}
                {selectedAddress.id <= 0 ? (
                    <S.Body>
                        <S.BlockContent>
                            <S.Description>Введите адрес доставки</S.Description>
                            <S.DescriptionText>
                                Если Вашего адреса нет в списке, значит доставка по этому адресу временно
                                недоступна. Зайдите позднее, мы постоянно расширяем зону доставки
                            </S.DescriptionText>
                        </S.BlockContent>
                    </S.Body>
                ) : (
                    <S.Body>
                        <BlockWithTitle title="Адрес доставки">
                            <S.BlockSelectedAddress>
                                <S.AddressTitle>{selectedAddress.title}</S.AddressTitle>
                                <Button
                                    isClickable={selectedAddress.id > 0}
                                    disabled={selectedAddress.id <= 0}
                                    clickHandler={changeAddress}>
                                    Подтвердить адрес
                                </Button>
                            </S.BlockSelectedAddress>
                        </BlockWithTitle>
                    </S.Body>
                )}
            </MobileBody>
        </S.Container>
    )
}

MobileAddressSelector.propTypes = {
    closeHandler: PropTypes.func.isRequired,
    shipAddressId: PropTypes.number.isRequired,
    isNotReload: PropTypes.bool
}

MobileAddressSelector.defaultProps = {
    isNotReload: false
}

export default MobileAddressSelector
