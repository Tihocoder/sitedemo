import styled from 'styled-components'
import { MobileBody } from '@styles/ui-element.styled'

export const Container = styled.div`
    background-color: white;
    position: fixed;
    height: 100%;
    width: 100%;
    top: 0;
    left: 0;
    z-index: 12;
    ${MobileBody} {
      overflow: hidden;
    }
}
`

export const Header = styled.div`
    padding: 0.2rem;
    height: 3rem;
`

export const SerachBlock = styled.div`
    position: relative;
    box-sizing: border-box;
    height: 2.375rem;
`

export const Input = styled.input`
    width: 100%;
    padding: 0.75rem 1.2rem;
    padding-left: 2.5rem;
    font-size: 1.25rem;
    height: 100%;
    border: 0;
    color: ${(props) => props.theme.colors.getGrayColor(0.7)};
    box-sizing: border-box;
    outline: none;
    -webkit-appearance: none;
    border-radius: 7px;
`
export const IconContainer = styled.div`
    position: absolute;
    width: fit-content;
    height: fit-content;
    top: 0.5rem;
    left: 0.6rem;
    cursor: pointer;
`

export const Body = styled.div`
    display: flex;
    flex-direction: column;
    background-color: ${(props) => props.theme.colors.gray5};
    height: 100%;
`

export const BlockContent = styled.div`
    background-color: ${(props) => props.theme.colors.white};
`
export const BlockSelectedAddress = styled.div`
    padding: 0.5rem 0.6rem;

    & > button {
        height: 3.5rem;
        width: 100%;
        margin-top: 2rem;
    }
`

export const Description = styled.div`
    text-align: center;
    padding: 2.7rem 0;
    font-size: 1.25rem;
`

export const DescriptionText = styled.p`
    text-indent: 1rem;
    padding: 0 0.625rem 2.5rem 0.625rem;
`

export const AddressTitle = styled.p``

export const ListItem = styled.div`
    position: absolute;
    width: 100%;
    background-color: white;
    margin-top: 1rem;
    box-sizing: border-box;
    border-radius: 7px;
    box-shadow: ${(props) => props.theme.shadows.shadowKit};
    overflow: auto;
    max-height: 20rem;
    z-index: 10;
    top: -1rem;

    & > div {
        border-bottom: 1px solid ${(props) => props.theme.colors.getGrayColor(0.05)};
    }

    & > div:last-child {
        border-bottom: 0;
    }
`

export const Item = styled.div`
    padding: 0.6rem 1rem;
    cursor: pointer;
`
