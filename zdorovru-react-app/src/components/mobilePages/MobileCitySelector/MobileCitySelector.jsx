import React from 'react'
import PropTypes from 'prop-types'
import { useSelector } from 'react-redux'
import { MobileBody } from '@styles/ui-element.styled'
import MobileHeader from '@mobileShared/MobileHeader/MobileHeader'
import MobileRowItem from '@mobileComponents/mobileShared/MobileRowItem/MobileRowItem'
import * as S from './MobileCitySelector.styled'

const MobileCitySelector = ({ cityId, changeCityId, closeHandler }) => {
    const cities = useSelector((state) => state.company.cities)
    return (
        <S.Container>
            <MobileHeader backClickHandler={closeHandler} isDefaultRouterBack={false}>
                Изменить регион
            </MobileHeader>
            <MobileBody>
                {cities.map((x) => (
                    <MobileRowItem
                        key={x.id}
                        clickHandler={() => changeCityId(x.id)}
                        isSelected={x.id === cityId}>
                        {x.title}
                    </MobileRowItem>
                ))}
            </MobileBody>
        </S.Container>
    )
}

MobileCitySelector.propTypes = {
    cityId: PropTypes.number.isRequired,
    changeCityId: PropTypes.func.isRequired,
    closeHandler: PropTypes.func.isRequired
}

export default MobileCitySelector
