import React, { useState } from 'react'
import * as Theme from '../../../styles/theme-const'
import * as S from './BasketNavigation.styled'
import * as _ from 'lodash'
import { useSelector } from 'react-redux'
import { useRouter } from 'next/router'
import { useMediaQuery } from 'react-responsive'

const BasketNavigation = (props) => {
    const basketItemsAll = useSelector((state) => state.customer.basketItems)
    const basketItems = basketItemsAll.filter((x) => !x.isHidden)
    const shipment = useSelector((state) => state.customer.shipment)
    const likedGoods = useSelector((state) => state.customer.likedGoods)
    const isCheckAllItems = basketItemsAll.length > 0
    const isCheckItems = basketItems.length > 0
    const router = useRouter()
    const isTabletOrMobile = useMediaQuery({ query: '(max-width: 1180px)' })
    let sumDelivery = 0
    let sumStock = 0
    let sumMinPrice = 0
    if (isCheckItems) {
        sumStock = _.sumBy(basketItems, (item) => item.stockPriceSum)
        sumDelivery = _.sumBy(basketItems, (item) => item.deliveryPriceSum)
        sumMinPrice = _.sumBy(basketItems, (item) => item.minPriceSum)
    }

    const [isBasketShowed, setIsBasketShowed] = useState(false)

    const [createOrderState, setCreateOrderState] = useState({
        isOpened: false,
        isDelivery: false
    })

    const closeCreateOrderHandler = () => {
        setCreateOrderState({
            ...createOrderState,
            isOpened: false
        })
    }

    const closeBasketHandler = () => {
        setIsBasketShowed(false)
    }

    const showBasket = () => {
        if (isCheckAllItems) router.push('/order/basket')
    }
    const clickLiked = () => {
        if (likedGoods && likedGoods.length > 0) router.push('/catalog/liked')
    }

    const createOrderHanlder = (isDelivery) => () => {
        setCreateOrderState({
            isOpened: true,
            isDelivery: isDelivery
        })
    }
    const anyLikes = likedGoods && likedGoods.length > 0
    const likedColor = anyLikes ? Theme.ColorConsts.pureRed : Theme.ColorConsts.getGrayColor(0.1)

    const positionsCount = _.sumBy(basketItems, (x) => x.count)
    const allPositionCount = _.sumBy(basketItemsAll, (x) => x.count)

    return (
        <S.Container>
            <S.Icons>
                <S.Like
                    onClick={clickLiked}
                    isDisabled={likedGoods.length === 0}
                    cursor={likedGoods.length > 0 ? 'pointer' : 'default'}>
                    {anyLikes && <S.LikedCount>{likedGoods.length}</S.LikedCount>}
                </S.Like>
                <S.Basket onClick={showBasket} isDisabled={!isCheckItems}>
                    {isCheckItems ? (
                        <S.PositionsCount>{positionsCount}</S.PositionsCount>
                    ) : (
                        <S.PositionsCount>{isCheckAllItems ? allPositionCount : ''}</S.PositionsCount>
                    )}
                </S.Basket>
            </S.Icons>
            {!isTabletOrMobile && isCheckItems ? (
                <S.BasketSumContainer onClick={showBasket}>
                    {sumStock ? (
                        <S.SumContainer>
                            <S.SumTitle>Самовывоз</S.SumTitle>
                            <S.Sum>{`${sumStock.toLocaleString('ru-RU')} ₽`}</S.Sum>
                        </S.SumContainer>
                    ) : (
                        <S.SumContainer>
                            <S.SumTitle>Самовывоз</S.SumTitle>
                            <S.Sum>{`от ${sumMinPrice.toLocaleString('ru-RU')} ₽`}</S.Sum>
                        </S.SumContainer>
                    )}
                    {sumDelivery && sumStock ? (
                        <S.HRContainer>
                            <S.Sum>/</S.Sum>
                        </S.HRContainer>
                    ) : (
                        <React.Fragment />
                    )}

                    {sumDelivery > 0 && (
                        <S.SumContainer>
                            <S.SumTitle>Доставка</S.SumTitle>
                            <S.Sum>{`${sumDelivery.toLocaleString('ru-RU')} ₽`}</S.Sum>
                        </S.SumContainer>
                    )}
                </S.BasketSumContainer>
            ) : (
                <React.Fragment />
            )}
        </S.Container>
    )
}

BasketNavigation.propTypes = {}

export default BasketNavigation
