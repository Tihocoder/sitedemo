import styled, { css } from 'styled-components'
import * as Theme from '../../../styles/theme-const'

export const Container = styled.div`
    display: flex;
    align-items: center;
    line-height: 1.5rem;

    & > div {
        margin-right: 1rem;
    }
    & > div:last-child {
        margin-right: 0;
    }
    margin-left: 1rem;
`

const getLikeSvgText =
    "url('data:image/svg+xml;utf8,<svg viewBox='0 0 26 23' fill='none'xmlns='http://www.w3.org/2000/svg'><pathd='M4.40764 0.743311C5.73499 0.117071 7.24688 -0.117712 8.7086 0.055109C10.2971 0.237902 11.8184 0.923491 12.9977 1.97965C14.0149 1.07613 15.2759 0.437073 16.6217 0.162886C18.1327 -0.147387 19.7383 -0.0134964 21.1676 0.560757C22.707 1.17157 24.032 2.28233 24.8886 3.67534C25.703 4.98627 26.085 6.54518 25.9841 8.07543C25.8912 9.67812 25.3634 11.2316 24.6123 12.6498C23.8494 14.0882 22.8558 15.4008 21.7413 16.5965C19.5001 18.9855 16.7975 20.9184 13.9168 22.5079C13.3452 22.8233 12.6522 22.8233 12.0807 22.5078C9.27134 20.9572 6.63326 19.0803 4.42345 16.7731C3.24723 15.5364 2.1972 14.173 1.39801 12.6718C0.607823 11.1844 0.0580358 9.54615 0.00572923 7.8605C-0.0475248 6.52265 0.271431 5.17296 0.92261 3.99582C1.69574 2.58832 2.93461 1.43246 4.40764 0.743311Z' fill='#F44336'/></svg>')"

export const Icons = styled.div`
    display: flex;
    min-width: 5rem;
    justify-content: space-between;
    align-items: flex-end;
`

export const Like = styled.a`
    height: fit-content;
    position: relative;
    text-decoration: none;
    background-repeat: no-repeat;
    width: 1.9rem;
    height: 1.7rem;
    cursor: ${(props) => props.cursor || 'pointer'};
    background-image: ${(props) =>
        props.isDisabled
            ? `url('${process.env.BASE_PATH}/images/like-default.svg')`
            : `url('${process.env.BASE_PATH}/images/like.svg')`};
    display: flex;
    justify-content: center;
    align-items: center;
`

export const Basket = styled.a`
    height: fit-content;
    position: relative;
    text-decoration: none;
    background-repeat: no-repeat;
    background-image: ${(props) =>
        props.isDisabled
            ? `url('${process.env.BASE_PATH}/images/basket-default.svg')`
            : `url('${process.env.BASE_PATH}/images/basket.svg')`};
    cursor: ${(props) => props.cursor || 'pointer'};
    width: 2.6rem;
    height: 2.4rem;
    display: flex;
    justify-content: center;
    align-items: flex-end;
`

export const PositionsCount = styled.span`
    color: ${(props) => props.theme.colors.white};
    font-weight: bold;
    font-size: 1rem;
`

export const LikedCount = styled.span`
    color: ${(props) => props.theme.colors.white};
    font-size: 1rem;
    font-weight: bold;
`

export const BasketSumContainer = styled.div`
    margin: 0;
    position: relative;
    cursor: pointer;
    display: flex;
    & > div {
        margin-right: 1rem;
    }
    & > div:last-child {
        margin-right: 0;
    }
    /* & > div:first-child::after {
        content: '';
        position: absolute;
        top: 0.4rem;
        right: -0.55em;
        width: 1px;
        height: 2rem;
        background: ${Theme.ColorConsts.colorGrayLight};
    } */
`
export const SumContainer = styled.div`
    display: flex;
    flex-direction: column;
    position: relative;
`

export const HRContainer = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: flex-end;
`
export const SumTitle = styled.span`
    font-size: 0.82rem;
    color: ${(props) => props.theme.colors.accentGreen};
`
export const Sum = styled.span`
    white-space: nowrap;
    font-size: 1rem;
    color: ${(props) => props.theme.colors.blackGreenText};
`
