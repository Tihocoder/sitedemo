import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import { MenuContext } from '../../../context/CatalogMenuState'
import * as S from './HamburgerButton.styled'
import { ColorConsts } from '../../../styles/theme-const'
import * as BaseStyled from '../../../styles/base.styled'
import Label from '../../shared/Label/Label'

const HamburgerButton = (props) => {
    const { isCatalogMenuOpen, toggleMenuMode } = useContext(MenuContext)
    const clickHandler = () => {
        toggleMenuMode()
    }
    return (
        <S.Container>
            <BaseStyled.Button onClick={clickHandler}>
                <S.ContentContainer>
                    <S.MenuButton className={isCatalogMenuOpen ? 'active' : ''} aria-label="Открыть каталог">
                        <S.Bar />
                        <S.Bar />
                        <S.Bar />
                    </S.MenuButton>
                    <Label color={ColorConsts.white}>{props.title}</Label>
                </S.ContentContainer>
            </BaseStyled.Button>
        </S.Container>
    )
}

HamburgerButton.propTypes = {
    title: PropTypes.string.isRequired
}
HamburgerButton.defaultProps = {
    title: 'Каталог'
}
export default HamburgerButton
