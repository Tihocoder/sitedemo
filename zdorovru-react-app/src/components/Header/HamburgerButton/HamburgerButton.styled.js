import styled from 'styled-components'
import * as Shared from '../../../styles/base.styled'

export const MenuButton = styled.div`
    height: 100%;
    min-height: fit-content;
    box-sizing: border-box;
    span {
        transition: all 0.3s cubic-bezier(0.645, 0.045, 0.355, 1);
    }

    & span {
        margin-bottom: 0.25rem;
    }
    & span:last-child {
        margin-bottom: 0;
    }
    :focus {
        border: medium none rgb(111, 255, 176);
        box-shadow: rgb(111, 255, 176) 0 0 2px 2px;
        outline: 0;
    }

    :hover {
        span:nth-of-type(1) {
            width: 20px;
        }

        span:nth-of-type(2) {
            width: 23px;
        }

        span:nth-of-type(3) {
            width: 20px;
        }
    }

    &.active {
        span:nth-of-type(1) {
            transform: rotate(45deg) translate(4px, 6px);
            width: 22px;
        }

        span:nth-of-type(2) {
            opacity: 0;
            pointer-events: none;
        }

        span:nth-of-type(3) {
            transform: rotate(-45deg) translate(4px, -6px);
            width: 22px;
        }
    }
`

export const Bar = styled.span`
    display: block;
    width: 22px;
    height: 3px;
    border-radius: 0.3rem;
    background-color: #fff;
`

export const ContentContainer = styled.div`
    display: flex;
    align-items: center;
    justify-content: space-between;
`

export const Container = styled.div`
    font-size: 1rem;
    height: 100%;
    ${Shared.Button} {
        padding: 0.3rem 1.1rem;
        width: 9rem;
        height: 100%;
    }
`
