import styled from 'styled-components'
import * as Theme from '../../../styles/theme-const'

export const Container = styled.div`
    width: 100%;
    height: 100%;
    box-sizing: border-box;
`

export const SerachBlock = styled.div`
    position: relative;
    height: 100%;
`

export const TagsBlock = styled.div`
    max-width: 10rem;
    box-sizing: border-box;
    display: flex;
    padding: 0.15rem 1.2rem;
    color: ${(props) => props.theme.colors.getGrayColor(0.5)};
    font-size: 0.75rem;
    position: absolute;
`

export const TagItem = styled.a`
    padding: 0 0.2rem;
    user-select: none;
    text-decoration: none;
    color: inherit;
    font: inherit;
`
export const TagSpan = styled.span`
    color: inherit;
    font: inherit;
    white-space: nowrap;
    :hover {
        color: ${(props) => props.theme.colors.getGrayColor(0.5)};
    }
`

export const Input = styled.input`
    width: 100%;
    padding: 0.75rem 1.2rem;
    font-size: 0.9rem;
    height: 100%;
    border: 2px solid ${(props) => props.theme.colors.accentGreenLight};
    color: ${(props) => props.theme.colors.getGrayColor(0.7)};
    box-sizing: border-box;
    outline: none;
    -webkit-appearance: none;
    border-radius: 0.3rem;
`

export const IconContainer = styled.div`
    position: absolute;
    width: fit-content;
    height: fit-content;
    top: 0.5rem;
    right: 0.6rem;
    cursor: pointer;
`
