import React, { useEffect, useState } from 'react'
import * as S from './Search.styled'
import IconSearch from '../../Icons/IconSearch'
import { useRouter } from 'next/router'
import { getUrlWithParams } from '../../../helpers/site'
import { useSelector } from 'react-redux'
import { getSearchUnderlineTags } from '../../../api/siteApis'
import Link from 'next/link'

const Search = (props) => {
    const router = useRouter()
    const shipment = useSelector((state) => state.customer.shipment)
    const [searchText, setSearchText] = useState('')
    const [tags, setTags] = useState([])
    const changeSearch = (e) => {
        setSearchText(e.target.value)
    }

    const initTags = async () => {
        const searchTags = await getSearchUnderlineTags(shipment)
        setTags(searchTags)
    }

    useEffect(() => {
        setSearchText('')
    }, [router.asPath])
    useEffect(() => {
        initTags()
    }, [])

    const keyDownHandler = async (e) => {
        if (e.key === 'Enter') {
            if (searchText.length > 2) {
                await router.push({
                    pathname: '/catalog/search',
                    query: {
                        q: searchText
                    }
                })
            }
        }
    }
    const clickSearch = async () => {
        if (searchText.length > 2) {
            await router.push({
                pathname: '/catalog/search',
                query: {
                    q: searchText
                }
            })
        }
    }

    return (
        <S.Container>
            <S.SerachBlock>
                <S.Input
                    value={searchText}
                    onChange={changeSearch}
                    onKeyDown={keyDownHandler}
                    placeholder="Поиск по наименованию, МНН, симптому, бренду"
                />
                <S.IconContainer onClick={clickSearch}>
                    <IconSearch />
                </S.IconContainer>
            </S.SerachBlock>
            {tags && tags.length > 0 ? (
                <S.TagsBlock>
                    Например:{' '}
                    {tags.map((x) => (
                        <Link
                            key={x.id}
                            href={getUrlWithParams('/catalog/search', [{ name: 'q', value: x.title }])}
                            passHref>
                            <S.TagItem>
                                <S.TagSpan>{x.title}</S.TagSpan>
                            </S.TagItem>
                        </Link>
                    ))}
                </S.TagsBlock>
            ) : (
                <React.Fragment />
            )}
        </S.Container>
    )
}

export default Search
