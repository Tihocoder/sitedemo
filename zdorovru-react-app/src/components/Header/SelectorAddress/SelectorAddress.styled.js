import styled from 'styled-components'

export const Container = styled.div`
    display: flex;
    align-items: center;
    height: 100%;
    line-height: 1.3rem;
`

export const IconContainer = styled.div`
    display: flex;
    height: 1.25rem;
`
export const Image = styled.img.attrs({
    src: `${process.env.BASE_PATH}/images/icon-delivery.svg`
})`
    height: 100%;
    width: auto;
`

export const ShipmentContainer = styled.div`
    display: flex;
    flex-direction: column;
    cursor: pointer;
`

export const ShipmentContent = styled.div`
    display: flex;
    font-size: 1rem;
    color: ${(props) => props.theme.colors.getGrayColor(1)};
    & input {
        border: 0;
    }
`

export const ShipmentSelectorBlock = styled.div`
    display: flex;
    flex-direction: column;
    padding: 0 0.8rem;
    cursor: pointer;

    :hover {
        ${ShipmentContent} a {
            color: ${(props) => props.theme.colors.accentGreen};
        }
    }
`

export const ShipmentTitle = styled.span`
    color: ${(props) => props.theme.colors.grayText};
    font-size: 0.75rem;
`
export const SeclectedShipmentText = styled.span`
    color: ${(props) => props.theme.colors.getGrayColor(1)};
`
export const SeclectedShipmentTextButton = styled.a`
    color: ${(props) => props.theme.colors.getGrayColor(1)};
`
