import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'
import { useRouter } from 'next/router'
import * as S from './SelectorAddress.styled'
import { getAddressById } from '../../../api/siteApis'
import { changeShipment } from '../../../actions/customer'
import ModalAddressSelect from '../../Basket/BasketShipment/ModalAddressSelect/ModalAddressSelect'

const SelectorAddress = (props) => {
    const [addressTitle, setAddressTitile] = useState('')
    const dispatch = useDispatch()
    const router = useRouter()
    const [isShowedSelectorAddress, setIsShowedSelectorAddress] = useState(false)
    const toggleSelectorAddress = () => setIsShowedSelectorAddress(!isShowedSelectorAddress)
    const initShipAddress = async (id) => {
        const response = await getAddressById(id)
        if (response && response.address) setAddressTitile(response.address)
    }

    useEffect(() => {
        if (props.shipment.shipAddressId) initShipAddress(props.shipment.shipAddressId)
    }, [props.shipment.shipAddressId])

    const changeSelectedAddressHandler = async (id, title) => {
        await dispatch(changeShipment({ shipAddressId: id, shipAddressTitle: title }))
        toggleSelectorAddress()
        router.reload()
    }
    return (
        <S.Container>
            <S.IconContainer>
                <S.Image />
            </S.IconContainer>
            <S.ShipmentContainer>
                <S.ShipmentSelectorBlock onClick={toggleSelectorAddress}>
                    <S.ShipmentTitle>Адрес доставки</S.ShipmentTitle>
                    <S.ShipmentContent>
                        {addressTitle ? (
                            <S.SeclectedShipmentTextButton>{addressTitle}</S.SeclectedShipmentTextButton>
                        ) : (
                            <S.SeclectedShipmentTextButton>Ничего не выбрано</S.SeclectedShipmentTextButton>
                        )}
                    </S.ShipmentContent>
                </S.ShipmentSelectorBlock>
            </S.ShipmentContainer>
            {isShowedSelectorAddress && (
                <ModalAddressSelect
                    shipAddressId={props.shipment.shipAddressId}
                    closeHandler={toggleSelectorAddress}
                    changeAddressHandler={changeSelectedAddressHandler}
                />
            )}
        </S.Container>
    )
}

SelectorAddress.propTypes = {}

export default SelectorAddress
