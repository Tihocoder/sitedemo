import React, { useContext, useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { MenuContext } from '@context/CatalogMenuState'
import { useDispatch, useSelector } from 'react-redux'
import { AlertsStateContext } from '@context/AlertsState'
import { useRouter } from 'next/router'
import * as S from './Header.styled'
import InfoBlock from './InfoBlock/InfoBlock'
import CatalogSelectorBlock from './CatalogSelectorBlock/CatalogSelectorBlock'
import CatalogNavigator from './CatalogNavigator/CatalogNavigator'
import ModalWindow from '../shared/ModalWindow/ModalWindow'
import SelectorStock from '../SelectorStock/SelectorStock'
import { changeShipment } from '../../actions/customer'

const Header = (props) => {
    const { isCatalogMenuOpen, toggleMenuMode, stockSelectorByGood, toggleStockSelectorByGood } = useContext(
        MenuContext
    )
    const { deleteFromBasket } = useContext(AlertsStateContext)

    const [modalChangeStock, setModalChangeStock] = useState({
        isOpened: false,
        shipment: {}
    })
    const stocks = useSelector((state) => state.company.stocks)
    const shipment = useSelector((state) => state.customer.shipment)
    const cities = useSelector((state) => state.company.cities)
    const dispatch = useDispatch()
    const router = useRouter()

    const selectedStocAndCitykHandler = async (id, cityId) => {
        setModalChangeStock({
            isOpened: true,
            shipment: { stockId: id, cityId }
        })
    }

    const submitModalChangeStock = async () => {
        closeModalChangeStock()
        await dispatch(changeShipment(modalChangeStock.shipment))
        toggleStockSelectorByGood()
        router.reload()
    }

    const closeModalChangeStock = () => {
        setModalChangeStock({ shipment: {}, isOpened: false })
    }

    return (
        <S.Container>
            <InfoBlock />
            <CatalogSelectorBlock />
            {isCatalogMenuOpen && <CatalogNavigator toggleShowedMenu={toggleMenuMode} />}
            {deleteFromBasket.isShowed && (
                <ModalWindow
                    title="Удаление из корзины"
                    submit={{
                        text: 'Удалить',
                        clickHandler: deleteFromBasket.confirmHandler
                    }}
                    cancel={{
                        text: 'Отмена',
                        clickHandler: deleteFromBasket.closeHandler
                    }}>
                    Вы действительно хотите удалить выбранный товар из корзины?
                </ModalWindow>
            )}
            {modalChangeStock.isOpened && (
                <ModalWindow
                    title="Изменение аптеки самовывоза"
                    submit={{
                        text: 'Подтвердить',
                        clickHandler: submitModalChangeStock
                    }}
                    cancel={{
                        text: 'Отмена',
                        clickHandler: closeModalChangeStock
                    }}>
                    Вы действительно хотите изменить аптеку самовывоза?
                </ModalWindow>
            )}
            {stockSelectorByGood.isOpened && (
                <SelectorStock
                    closeHandler={toggleStockSelectorByGood}
                    stocks={stocks}
                    cities={cities}
                    goodIds={stockSelectorByGood.selectedGoodIds}
                    changeSelectedStockHandler={selectedStocAndCitykHandler}
                    selectedStock={stocks.find((stock) => stock.id === shipment.stockId)}
                    cityId={shipment.cityId}
                    fromGood
                />
            )}
        </S.Container>
    )
}

export default Header
