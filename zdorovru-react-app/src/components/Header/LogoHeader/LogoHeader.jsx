import React from 'react'
import PropTypes from 'prop-types'
import * as S from './LogoHeader.styled'
import Link from 'next/link'
const LogoHeader = (props) => {
    return (
        <Link href="/" passHref>
            <S.Container>
                <S.LogoImg src={`${process.env.BASE_PATH || ''}/images/logo-zdorov.svg`} />
            </S.Container>
        </Link>
    )
}

export default LogoHeader
