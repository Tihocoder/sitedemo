import styled from 'styled-components'

export const Container = styled.div`
    margin-top: 0.6rem;
    height: 6.5rem;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    position: relative;
`
