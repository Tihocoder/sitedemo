import styled from 'styled-components'
import * as Theme from '../../../styles/theme-const'

export const Container = styled.div`
    display: flex;
    align-items: center;
    height: 100%;
    line-height: 1.3rem;
`

export const IconContainer = styled.div`
    display: flex;
    height: 1.665rem;
`
export const Image = styled.img.attrs({
    src: `${process.env.BASE_PATH}/images/icon-pickup.svg`
})`
    height: 100%;
    width: auto;
`

export const ShipmentContainer = styled.div`
    display: flex;
    flex-direction: column;
    cursor: pointer;
`
export const ShipmentContent = styled.div`
    display: flex;
    font-size: 1rem;
    color: ${(props) => props.theme.colors.getGrayColor(1)};
    & input {
        border: 0;
    }
`
export const ShipmentSelectorBlock = styled.div`
    display: flex;
    flex-direction: column;
    padding: 0 0.8rem;
    cursor: pointer;

    :hover {
        ${ShipmentContent} a {
            color: ${(props) => props.theme.colors.getGreenColor(1)};
        }
    }
`

export const ShipmentChangeLink = styled.a`
    border-bottom: 1px dashed;
    font-weight: bold;
`

export const ShipmentTitle = styled.span`
    color: ${(props) => props.theme.colors.grayText};
    font-size: 0.75rem;
`
export const SeclectedShipmentText = styled.span`
    color: ${(props) => props.theme.colors.getGrayColor(0.85)};
`
export const SeclectedShipmentTextButton = styled.a`
    color: inherit;
`

export const Separator = styled.span`
    color: ${Theme.ColorConsts.getGrayColor(0.75)};
    margin-left: 0.2rem;
    margin-right: 0.2rem;
`

export const StockItem = styled.div`
    display: flex;
`
export const StockColorChar = styled.div`
    ${(props) => {
        switch (props.metroColor) {
            case 'shabalovka': {
                return 'color: #ff8103'
            }
            case 'darkblue': {
                return 'color: #283981'
            }
            case 'domoded': {
                return 'color: #029a55'
            }
            case 'vernads': {
                return 'color: #b1d332'
            }
            case 'maryno': {
                return ' color: #b1d332'
            }
            case 'varshav': {
                return 'color: #5091bb'
            }
            case 'sokol': {
                return 'color: #029a55'
            }
            case 'butovo': {
                return 'color: #85d4f3'
            }
            case 'orange': {
                return 'color: #ff8103'
            }
            case 'kolomenskaya': {
                return 'color: #029a55'
            }
            case 'saddlebrown': {
                return 'color: #945340'
            }
            case 'yellow': {
                return 'color: #ffd803'
            }
            case 'darkgreen': {
                return 'color: #029a55'
            }
            case 'red': {
                return 'color: #ef1e25'
            }
            case 'lightgreen': {
                return 'color: #b1d332'
            }
            case 'lightblue': {
                return 'color: #85d4f3'
            }
            case 'black': {
                return 'color: black'
            }
            case 'medblue': {
                return 'color: #5091bb'
            }
            case 'purple': {
                return 'color: #b61d8e'
            }
            case 'gray': {
                return 'color: #acadaf'
            }
            case 'aquablue': {
                return 'color: #019ee0'
            }
            default: {
                return 'color: black'
            }
        }
    }};
    font-weight: bold;
    margin-right: 0.4rem;
`
export const StockTitle = styled.div`
    margin: 0;
`
