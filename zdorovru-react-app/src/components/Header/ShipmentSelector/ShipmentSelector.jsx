import React, { useState, useEffect } from 'react'
import Geo from '../../Icons/Geo'
import PropTypes from 'prop-types'
import * as Theme from '../../../styles/theme-const'
import * as S from './ShipmentSelector.styled'
import { useSelector, useDispatch } from 'react-redux'
import { useRouter } from 'next/router'
import StockItem from '../../libs/Stock/StockItem/StockItem'
import SelectorStock from '../../SelectorStock/SelectorStock'
import { changeShipment, selectCity } from '../../../actions/customer'
import { changeLinkExceptQueryParams, linkWithShipQuery } from '../../../helpers/site'
import { getAddressById } from '../../../api/siteApis'
import ModalAddressSelect from '../../Basket/BasketShipment/ModalAddressSelect/ModalAddressSelect'

const ShipmentSelector = (props) => {
    const router = useRouter()
    const stocksAll = useSelector((state) => state.company.stocks)
    const cities = useSelector((state) => state.company.cities)
    const basketItems = useSelector((state) => state.customer.basketItems)
    const [isShowedSelectorStock, setIsShowedSelectorStock] = useState(false)
    const dispatch = useDispatch()

    const selectedStocAndCitykHandler = async (id, cityId) => {
        const newStock = stocksAll.find((stock) => stock.id === props.shipment.stockId)
        await dispatch(
            changeShipment({
                stockId: id,
                cityId: cityId
            })
        )
        setIsShowedSelectorStock(!isShowedSelectorStock)
        router.reload()
    }

    const getSelectedStock = () => stocksAll.find((stock) => stock.id === props.shipment.stockId)

    const toggleIsShowedSelectorStockHandler = () => {
        setIsShowedSelectorStock(!isShowedSelectorStock)
    }
    const stock = stocksAll.find((stock) => stock.id === props.shipment.stockId)
    return (
        <S.Container>
            <S.IconContainer>
                <S.Image />
            </S.IconContainer>
            <S.ShipmentContainer>
                <S.ShipmentSelectorBlock onClick={toggleIsShowedSelectorStockHandler}>
                    <S.ShipmentTitle>Аптека самовывоза</S.ShipmentTitle>
                    <S.ShipmentContent>
                        {stock ? (
                            <StockItem
                                isMetroLocation={stock.isMetroLocation}
                                name={stock.name}
                                title={stock.title}
                                metroColor={stock.metroColor}
                                isHover
                            />
                        ) : (
                            <S.SeclectedShipmentTextButton>Ничего не выбрано</S.SeclectedShipmentTextButton>
                        )}
                    </S.ShipmentContent>
                </S.ShipmentSelectorBlock>
            </S.ShipmentContainer>
            {isShowedSelectorStock && (
                <SelectorStock
                    stocks={stocksAll}
                    cities={cities}
                    goodIds={basketItems.map((x) => x.webData.goodId)}
                    changeSelectedStockHandler={selectedStocAndCitykHandler}
                    closeHandler={toggleIsShowedSelectorStockHandler}
                    selectedStock={getSelectedStock()}
                    cityId={props.shipment.cityId}
                />
            )}
        </S.Container>
    )
}

ShipmentSelector.propTypes = {
    shipment: PropTypes.shape({
        stockId: PropTypes.number.isRequired,
        cityId: PropTypes.number.isRequired
    })
}

export default ShipmentSelector
