import React from 'react'
import PropTypes from 'prop-types'
import * as S from './CatalogSelectorBlock.styled'
import HamburgerButton from '../HamburgerButton/HamburgerButton'
import Search from '../Search/Search'
import BasketNavigation from '../BasketNavigation/BasketNavigation'
import LogoHeader from '../LogoHeader/LogoHeader'
import { useRouter } from 'next/router'

const CatalogSelectorBlock = (props) => {
    return (
        <S.Container>
            <S.FirstBlock>
                <LogoHeader />
                <HamburgerButton />
            </S.FirstBlock>
            <S.LastBlock>
                <Search />
                <BasketNavigation />
            </S.LastBlock>
        </S.Container>
    )
}

export default CatalogSelectorBlock
