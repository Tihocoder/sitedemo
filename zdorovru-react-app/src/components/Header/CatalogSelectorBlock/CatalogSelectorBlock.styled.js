import styled from 'styled-components'

export const Container = styled.div`
    display: flex;
    height: 2.4rem;
    justify-content: space-between;
    width: 100%;
    min-height: fit-content;
`

export const FirstBlock = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    width: 36rem;
`

export const LastBlock = styled.div`
    display: flex;
    width: 100%;
    margin-left: 1rem;
    justify-content: space-between;
`

export const ItemContainer = styled.div`
    margin: 0;
`
