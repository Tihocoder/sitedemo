import styled from 'styled-components'
import * as Theme from '../../../styles/theme-const'

export const Container = styled.a`
    display: flex;
    align-items: center;
    text-decoration: none;
    cursor: pointer;
    height: 100%;
    line-height: 1.3rem;
`
export const IconContainer = styled.div`
    display: flex;
    height: 1.765rem;
`
export const Image = styled.img.attrs({
    src: `${process.env.BASE_PATH}/images/sale.svg`
})`
    height: 100%;
    width: auto;
`
export const Title = styled.span`
    padding: 0 0.8rem;
    color: ${(props) => props.theme.colors.getGrayColor(1)};
    white-space: nowrap;
`
