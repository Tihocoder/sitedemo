import React, { useEffect, useState } from 'react'
import * as S from './SaleLink.styled'
import Link from 'next/link'
import { getPromotionsCount } from '../../../api/siteApis'

const SaleLink = (props) => {
    const [saleCount, setSaleCount] = useState()
    const initPromotionCount = async () => {
        const promotionsCount = await getPromotionsCount()
        setSaleCount(promotionsCount)
    }
    useEffect(() => {
        initPromotionCount()
    }, [])
    return (
        <Link href="/catalog/promotion/list" passHref>
            <S.Container>
                <S.IconContainer>
                    <S.Image />
                </S.IconContainer>
                <S.Title>Скидки и акции {saleCount ? `(${saleCount})` : ''}</S.Title>
            </S.Container>
        </Link>
    )
}

export default SaleLink
