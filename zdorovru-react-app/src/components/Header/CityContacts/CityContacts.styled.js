import styled from 'styled-components'
import * as Theme from '../../../styles/theme-const'

export const Container = styled.div`
    display: flex;
    align-items: center;
    height: 100%;
    line-height: 1.3rem;
`
export const Image = styled.img.attrs({
    src: `${process.env.BASE_PATH}/images/phone.svg`
})`
    height: 100%;
    width: auto;
`

export const IconContainer = styled.div`
    display: flex;
    height: 1.41rem;
`

export const ContentContainer = styled.div`
    display: flex;
    flex-direction: column;
    padding: 0 1rem;
    cursor: pointer;
    padding: 0 0.8rem;
`
export const ContentBody = styled.a`
    display: flex;
    font-size: 1rem;
    text-decoration: none;
    color: ${(props) => props.theme.colors.getGrayColor(1)};
    & input {
        border: 0;
    }
`

export const PhoneSpan = styled.span`
    white-space: nowrap;
`

export const ContentTitle = styled.span`
    color: ${(props) => props.theme.colors.grayText};
    font-size: 0.75rem;
`

export const CityContactContainer = styled.div`
    display: flex;
    flex-direction: column;
`
export const CityContactDescription = styled.span`
    color: ${Theme.ColorConsts.getGrayColor(0.6)};
    font-size: 1rem;
    white-space: nowrap;
`

export const CityContactTitle = styled.a`
    color: ${(props) => props.theme.colors.getGrayColor(0.75)};
    cursor: pointer;
    font-weight: bold;
    text-decoration: none;
    font-size: 1.2rem;
    white-space: nowrap;
`
