import React from 'react'
import PropTypes from 'prop-types'
import * as S from './CityContacts.styled'
import Phone from '../../Icons/Phone'
import { useSelector } from 'react-redux'

const CityContacts = (props) => {
    const city = useSelector((state) => {
        return state.company.cities.find((city) => city.id === (state.customer.shipment.cityId || 1))
    })
    return (
        <S.Container>
            <S.IconContainer>
                <S.Image />
            </S.IconContainer>
            <S.ContentContainer>
                <S.ContentTitle>с 07:00 до 23:00</S.ContentTitle>
                <S.ContentBody
                    href={`tel:${((city ? city.phone : false) || '+7 (495) 363-3500').replace(/\D+/g, '')}`}>
                    {' '}
                    <S.PhoneSpan>{(city ? city.phone : false) || '+7 (495) 363-3500'}</S.PhoneSpan>
                </S.ContentBody>
            </S.ContentContainer>
        </S.Container>
    )
}

export default CityContacts
