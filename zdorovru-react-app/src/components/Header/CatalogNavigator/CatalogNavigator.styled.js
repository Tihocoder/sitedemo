import styled from 'styled-components'
import * as Theme from '../../../styles/theme-const'

export const AbsoluteContainer = styled.div`
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    z-index: 30;
    height: 100vh;
    display: flex;
    justify-content: center;
`
export const Container = styled.div`
    display: flex;
    height: 34rem;
    /* box-shadow: 0 1px 3px hsla(0, 0%, 0%, 0.2); */
    box-shadow: ${(props) => props.theme.shadows.shadowKit};
    border-radius: 0.3rem;
    background-color: ${(props) => props.theme.colors.white};
    width: 69rem;
    margin-top: 8rem;
`

export const GeneralGroupsContainer = styled.div`
    display: flex;
    flex-direction: column;
    width: 32rem;
    height: 100%;
    min-height: 19rem;
    height: 100%;
    box-sizing: border-box;
    background-color: ${(props) => props.theme.colors.getGrayColor(0.05)};
    padding: 2.4rem 0;
`

export const GroupContainer = styled.div`
    display: flex;
    padding: 2.4rem;
    flex-wrap: wrap;
    flex-direction: column;
    width: 100%;
    box-sizing: border-box;
`

export const GroupTitle = styled.a`
    font-size: 1rem;
    min-height: 1.7rem;
    display: flex;
    align-items: center;
    color: ${Theme.ColorConsts.getGrayColor(0.6)};
    transition: color 0.3s ease;
    & :hover {
        color: ${Theme.ColorConsts.getGreenColor(1)};
        transition: color 0.3s ease;
    }
    cursor: pointer;
    text-decoration: none;
    width: 15.5rem;
    padding-bottom: 0.3rem;
    padding-right: 1rem;
    box-sizing: border-box;
`

export const GeneralGroupTitle = styled.span`
    font-weight: bold;
    font-size: 1rem;
    min-height: 2.2rem;
    padding: 0.2rem 2rem;
    color: ${Theme.ColorConsts.getGrayColor(0.75)};
    background-color: ${(props) => props.isSelected && props.theme.colors.white};
    transition: color 0.3s ease;
    & :hover {
        color: ${(props) => props.theme.colors.accentGreen};
        transition: color 0.3s ease;
    }
    cursor: pointer;
    display: flex;
    align-items: center;
`
