import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import * as S from './CatalogNavigator.styled'
import { useSelector } from 'react-redux'
import { useRouter } from 'next/router'
import { linkWithShipQuery } from '../../../helpers/site'
import Link from 'next/link'

const CatalogNavigator = (props) => {
    const router = useRouter()
    const groups = useSelector((state) => state.catalog.mainGroups)
    const shipment = useSelector((state) => state.customer.shipment)
    const generals = groups.filter((group) => group.isGeneral)
    const firstGroup = generals.find((x) => x != undefined)
    const [selectedGeneralId, setSelectedGeneralId] = useState(0)
    const [generalGroups, setGeneralGroups] = useState([])

    const changeSelectedGeneralGroups = (groupId) => {
        const newGeneralGroups = groups.filter((x) => x.parentClsGroupId === groupId)
        setGeneralGroups(newGeneralGroups)
    }

    useEffect(() => {
        if (firstGroup) {
            setSelectedGeneralId(firstGroup.id)
            changeSelectedGeneralGroups(firstGroup.id)
        }
    }, [firstGroup && firstGroup.id])

    useEffect(() => {
        changeSelectedGeneralGroups(selectedGeneralId)
    }, [selectedGeneralId])

    const clickGeneral = (groupId) => (e) => {
        e.stopPropagation()
        setSelectedGeneralId(groupId)
    }

    return (
        <S.AbsoluteContainer onClick={props.toggleShowedMenu}>
            <S.Container>
                <S.GeneralGroupsContainer>
                    {generals.map((group) => (
                        <S.GeneralGroupTitle
                            onClick={clickGeneral(group.id)}
                            isSelected={selectedGeneralId === group.id}
                            key={group.id}>
                            {group.title}
                        </S.GeneralGroupTitle>
                    ))}
                </S.GeneralGroupsContainer>
                <S.GroupContainer>
                    {generalGroups.map((childGroup) => (
                        <Link
                            href={`/catalog/${selectedGeneralId}/${childGroup.id}`}
                            key={childGroup.id}
                            passHref>
                            <S.GroupTitle key={childGroup.id}>{childGroup.title}</S.GroupTitle>
                        </Link>
                    ))}
                </S.GroupContainer>
            </S.Container>
        </S.AbsoluteContainer>
    )
}

CatalogNavigator.propTypes = {
    toggleShowedMenu: PropTypes.func.isRequired
}

export default CatalogNavigator
