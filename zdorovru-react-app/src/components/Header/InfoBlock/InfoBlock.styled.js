import styled from 'styled-components'

export const Container = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    line-height: 1.4rem;
`
export const SiteInfoContainer = styled.div`
    display: flex;
    align-items: center;
    justify-content: space-between;
    width: 100%;
`

export const RightContainer = styled.div`
    align-items: center;
    display: flex;
`
