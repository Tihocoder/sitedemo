import React from 'react'
import PropTypes from 'prop-types'
import * as S from './InfoBlock.styled'
import LogoHeader from '../LogoHeader/LogoHeader'
import ShipmentSelector from '../ShipmentSelector/ShipmentSelector'
import CityContacts from '../CityContacts/CityContacts'
import SaleLink from '../SaleLink/SaleLink'
import { useSelector } from 'react-redux'
import { useRouter } from 'next/router'
import { linkWithShipQuery } from '../../../helpers/site'
import Link from 'next/link'
import SelectorAddress from '../SelectorAddress/SelectorAddress'

const InfoBlock = (props) => {
    const shipment = useSelector((state) => state.customer.shipment)

    return (
        <S.Container>
            <S.SiteInfoContainer>
                <CityContacts />
                <ShipmentSelector shipment={shipment} />
                <SelectorAddress shipment={shipment} />
                <SaleLink />
            </S.SiteInfoContainer>
        </S.Container>
    )
}

export default InfoBlock
