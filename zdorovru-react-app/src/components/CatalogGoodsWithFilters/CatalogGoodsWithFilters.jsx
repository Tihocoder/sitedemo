import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import * as S from './CatalogGoodsWithFilters.styled'
import * as SharedStyled from '../../styles/base.styled'
import { useDispatch, useSelector } from 'react-redux'
import * as _ from 'lodash'
import { getCategoriesTreeByGoodIds } from '../../api/siteApis'
import Filter from '../shared/Filter/Filter'
import Paginator from '../shared/Paginator/Paginator'
import CatalogGoodItem from '../CatalogGoodItem/CatalogGoodItem'
import { _PricePropTypes, _WebDataPropTypes } from '../../consts'
import { filterGoods, FilterTypes, usePage } from '../../helpers/site'
import { toggleLikedGood } from '../../actions/customer'
import { setGoodsFilter } from '../../actions/catalog'
import FoundCategories from '../Search/FoundCategories/FoundCategories'
import { useRouter } from 'next/router'

const CatalogGoodsWithFilters = (props) => {
    const shipment = useSelector((state) => state.customer.shipment)
    const [goodsCategoriesTree, setGoodsCategoriesTree] = useState([])
    const basketItems = useSelector((state) => state.customer.basketItems)
    const goods = useSelector((state) => state.catalog.goods)
    const goodsFilters = useSelector((state) => state.catalog.goodsFilters)
    const goodsData = useSelector((state) => state.catalog.goodsData)
    const dispatch = useDispatch()
    const likedGoods = useSelector((state) => state.customer.likedGoods)

    const page = usePage()

    useEffect(() => {
        const initGroupsTreeByGoods = async () => {
            const categoriesTree = await getCategoriesTreeByGoodIds(goods.map((x) => x.webData.goodId))
            setGoodsCategoriesTree(categoriesTree)
        }
        initGroupsTreeByGoods()

        return () => {
            setGoodsCategoriesTree([])
        }
    }, [goods.length])

    const currentFilters = [
        {
            name: FilterTypes.outForms,
            title: 'Формы выпуска'
        },
        {
            name: FilterTypes.makers,
            title: 'Производитель'
        }
    ]

    const getBasketItem = (goodId) => {
        const basketItem = basketItems.find((x) => x.webData.goodId === goodId)
        return basketItem ? basketItem.count : null
    }

    const changeSelectedFilter = (name) => async (newItems) => {
        await dispatch(setGoodsFilter(name, newItems))
        page.changePage(1)
    }
    const filteredGoods = filterGoods(goods, goodsFilters, [FilterTypes.makers, FilterTypes.outForms])
    var startNumberGoods = (page.number - 1) * 8
    const goodsVisible = filteredGoods.slice(startNumberGoods, startNumberGoods + 8)
    const goodsPageCount = Math.ceil(filteredGoods.length / 8)

    const toggleLikeHandler = async (webId) => {
        await dispatch(toggleLikedGood(webId))
    }

    return (
        <S.Container>
            {props.options.isLeftBlock && (
                <SharedStyled.LeftMenu>
                    <S.Categories>
                        <FoundCategories categories={goodsCategoriesTree} goods={filteredGoods} />
                    </S.Categories>
                    {currentFilters.map((cf) => (
                        <Filter
                            key={cf.name}
                            title={cf.title}
                            items={goodsData[cf.name]}
                            selectedItems={goodsFilters[cf.name]}
                            changeSelectedItemsHandler={changeSelectedFilter(cf.name)}
                        />
                    ))}
                </SharedStyled.LeftMenu>
            )}
            <SharedStyled.RightBlock isLeftBlock={props.options.isLeftBlock}>
                {goodsPageCount > 1 ? (
                    <Paginator
                        pageCount={goodsPageCount}
                        currentPage={page.number}
                        selectPageHandler={page.changePage}
                    />
                ) : (
                    <React.Fragment />
                )}
                <S.GoodsContainer>
                    {goodsVisible.length > 0 &&
                        goodsVisible.map((good) => (
                            <CatalogGoodItem
                                key={good.webData.goodId}
                                basketItemCount={getBasketItem(good.webData.goodId)}
                                availableStocksCount={good.availableStocksCount}
                                analoguesQty={good.analoguesQty}
                                anotherOutFormsQty={good.anotherOutFormsQty}
                                webData={good.webData}
                                stockPrice={good.stockPrice}
                                deliveryPrice={good.deliveryPrice}
                                minPrice={good.minPrice}
                                deliveryStatus={good.deliveryStatus}
                                isDemand={good.isDemand}
                                aptEtaDateTime={good.aptEtaDateTime}
                                isAvailable={good.isAvailable}
                                isAvailableOnStock={good.isAvailableOnStock}
                                isStrictlyByPrescription={good.isStrictlyByPrescription}
                                isOriginalGood={good.isOriginalGood}
                                isCold={good.isCold}
                                liked={likedGoods.includes(good.webData.goodId)}
                                toggleLikeHandler={toggleLikeHandler}
                                shipment={shipment}
                            />
                        ))}
                </S.GoodsContainer>
                <Paginator
                    pageCount={goodsPageCount}
                    currentPage={page.number}
                    selectPageHandler={page.changePage}
                />
            </SharedStyled.RightBlock>
        </S.Container>
    )
}
CatalogGoodsWithFilters.propTypes = {
    options: {
        isLeftBlock: PropTypes.bool.isRequired
    }
}
CatalogGoodsWithFilters.defaultProps = {
    options: {
        isLeftBlock: true
    }
}
export default CatalogGoodsWithFilters
