import styled, { css } from 'styled-components'

export const Container = styled.div`
    display: flex;
    justify-content: space-between;
`

export const Categories = styled.div`
    margin: 0;
`

export const FilterBlock = styled.div`
    display: flex;
    flex-direction: column;
`

export const SearchResult = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
    box-sizing: border-box;
    padding: 0 3.5rem;
`
export const TopCategories = styled.div`
    width: 100%;
    display: flex;
    justify-content: flex-start;
    & > div {
        margin-right: 1rem;
    }
    & > div:last-child {
        margin-right: 0;
    }
    margin-bottom: 1rem;
`

export const GoodsContainer = styled.div`
    width: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
    box-sizing: content-box;
    padding-bottom: 1rem;
`
