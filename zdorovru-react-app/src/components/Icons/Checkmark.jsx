import React from 'react'
import PropTypes from 'prop-types'
import * as Theme from '../../styles/theme-const'

const Checkmark = (props) => {
    return (
        <svg
            height="100%"
            id="Layer_1"
            width="100%"
            version="1.1"
            viewBox="0 0 92 92"
            xmlns="http://www.w3.org/2000/svg"
            x="0px"
            y="0px"
            xmlSpace="preserve">
            <path
                id="XMLID_712_"
                d="M34.4,72c-1.2,0-2.3-0.4-3.2-1.3L11.3,50.8c-1.8-1.8-1.8-4.6,0-6.4c1.8-1.8,4.6-1.8,6.4,0l16.8,16.7&#xA;&#x9;l39.9-39.8c1.8-1.8,4.6-1.8,6.4,0c1.8,1.8,1.8,4.6,0,6.4l-43.1,43C36.7,71.6,35.6,72,34.4,72z"
            />
        </svg>
    )
}

Checkmark.propTypes = {
    color: PropTypes.string.isRequired
}
Checkmark.defaultProps = {
    color: Theme.ColorConsts.getGrayColor(0.6)
}
export default Checkmark
