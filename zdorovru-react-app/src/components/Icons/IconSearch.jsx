import React from 'react'
import PropTypes from 'prop-types'
import * as Theme from '../../styles/theme-const'

const IconSearch = (props) => {
    return (
        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path
                d="M16.3199 14.9056L21 19.5858C21.3905 19.9763 21.3905 20.6095 21 21C20.6095 21.3905 19.9763 21.3905 19.5858 21L14.9056 16.3199C13.551 17.3729 11.8487 18 10 18C5.58172 18 2 14.4183 2 10C2 5.58172 5.58172 2 10 2C14.4183 2 18 5.58172 18 10C18 11.8487 17.3729 13.551 16.3199 14.9056ZM10 16C13.3137 16 16 13.3137 16 10C16 6.68629 13.3137 4 10 4C6.68629 4 4 6.68629 4 10C4 13.3137 6.68629 16 10 16Z"
                fill={props.color}
            />
        </svg>
    )
}

IconSearch.propTypes = {
    color: PropTypes.string.isRequired
}
IconSearch.defaultProps = {
    color: Theme.ColorConsts.pureRed
}

export default IconSearch
