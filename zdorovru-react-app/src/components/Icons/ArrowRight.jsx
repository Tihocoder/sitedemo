import React from 'react'
import * as Theme from '../../styles/theme-const'
import PropTypes from 'prop-types'

const ArrowRight = (props) => {
    return (
        <svg
            height="100%"
            id="Layer_1"
            width="100%"
            version="1.1"
            viewBox="0 0 92 92"
            xmlns="http://www.w3.org/2000/svg"
            fill={props.color}
            x="0px"
            y="0px"
            xmlSpace="preserve">
            <path
                id="XMLID_439_"
                d="M63,46c0,1.1-0.4,2.1-1.2,2.9l-26,25C35,74.6,34,75,33,75c-1.1,0-2.1-0.4-2.9-1.2c-1.5-1.6-1.5-4.1,0.1-5.7&#xA;&#x9;l23-22.1l-23-22.1c-1.6-1.5-1.6-4.1-0.1-5.7c1.5-1.6,4.1-1.6,5.7-0.1l26,25C62.6,43.9,63,44.9,63,46z"
            />
        </svg>
    )
}

ArrowRight.propTypes = {
    color: PropTypes.string.isRequired
}

ArrowRight.defaultProps = {
    color: Theme.ColorConsts.getGrayColor(0.6)
}

export default ArrowRight
