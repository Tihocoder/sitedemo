import React from 'react'
import * as Theme from '../../styles/theme-const'
import PropTypes from 'prop-types'

const ArrowLeft = (props) => {
    return (
        <svg
            height="100%"
            id="Layer_1"
            width="100%"
            version="1.1"
            viewBox="0 0 92 92"
            xmlns="http://www.w3.org/2000/svg"
            fill={props.color}
            x="0px"
            y="0px"
            xmlSpace="preserve">
            <path
                id="XMLID_423_"
                d="M61.8,68.1c1.6,1.5,1.6,4.1,0.1,5.7C61.1,74.6,60,75,59,75c-1,0-2-0.4-2.8-1.1l-26-25&#xA;&#x9;C29.4,48.1,29,47.1,29,46s0.4-2.1,1.2-2.9l26-25c1.6-1.5,4.1-1.5,5.7,0.1c1.5,1.6,1.5,4.1-0.1,5.7L38.8,46L61.8,68.1z"
            />
        </svg>
    )
}

ArrowLeft.propTypes = {
    color: PropTypes.string.isRequired
}

ArrowLeft.defaultProps = {
    color: Theme.ColorConsts.getGrayColor(0.6)
}

export default ArrowLeft
