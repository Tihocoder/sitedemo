import React from 'react'
import PropTypes from 'prop-types'
import * as Theme from '../../styles/theme-const'

const Geo = (props) => {
    return (
        <svg viewBox="0 0 21 27" xmlns="http://www.w3.org/2000/svg">
            <path
                d="M10.0738 12.6411C9.11961 12.6411 8.2045 12.2906 7.52978 11.6668C6.85507 11.0429 6.47602 10.1968 6.47602 9.31452C6.47602 8.43224 6.85507 7.58611 7.52978 6.96225C8.2045 6.33839 9.11961 5.9879 10.0738 5.9879C11.028 5.9879 11.9431 6.33839 12.6178 6.96225C13.2925 7.58611 13.6716 8.43224 13.6716 9.31452C13.6716 9.75137 13.5785 10.184 13.3977 10.5876C13.2169 10.9912 12.9519 11.3579 12.6178 11.6668C12.2837 11.9757 11.8871 12.2207 11.4506 12.3879C11.0141 12.5551 10.5463 12.6411 10.0738 12.6411ZM10.0738 0C7.40206 0 4.83975 0.981348 2.95055 2.72816C1.06134 4.47497 0 6.84415 0 9.31452C0 16.3004 10.0738 26.6129 10.0738 26.6129C10.0738 26.6129 20.1476 16.3004 20.1476 9.31452C20.1476 6.84415 19.0863 4.47497 17.1971 2.72816C15.3078 0.981348 12.7455 0 10.0738 0Z"
                fill={props.color}
                stroke={props.stroke}
                strokeWidth={props.strokeWidth}
            />
        </svg>
    )
}
//width: 21
//height: 27

Geo.propTypes = {
    color: PropTypes.string,
    stroke: PropTypes.string.isRequired,
    strokeWidth: PropTypes.number.isRequired,
    width: PropTypes.number.isRequired,
    height: PropTypes.number.isRequired
}

Geo.defaultProps = {
    color: Theme.ColorConsts.accentGreen,
    stroke: 'none',
    strokeWidth: 1
}

export default Geo
