import React from 'react'
import PropTypes from 'prop-types'

const Plus = (props) => {
    return (
        <svg
            id="Layer_1"
            fill={props.color}
            version="1.1"
            viewBox="0 0 92 92"
            xmlns="http://www.w3.org/2000/svg"
            x="0px"
            y="0px"
            xmlSpace="preserve">
            <path
                id="XMLID_933_"
                d="M72.5,46.5c0,2.5-2,4.5-4.5,4.5H50v17c0,2.5-2,4.5-4.5,4.5S41,70.5,41,68V51H24c-2.5,0-4.5-2-4.5-4.5&#xA;&#x9;s2-4.5,4.5-4.5h17V24c0-2.5,2-4.5,4.5-4.5s4.5,2,4.5,4.5v18h18C70.5,42,72.5,44,72.5,46.5z"
            />
        </svg>
    )
}

Plus.propTypes = {
    color: PropTypes.string.isRequired
}
Plus.defaultProps = {
    color: '#000000'
}

export default Plus
