import React from 'react'
import PropTypes from 'prop-types'

const Minus = (props) => {
    return (
        <svg
            id="Layer_1"
            version="1.1"
            fill={props.color}
            viewBox="0 0 92 92"
            xmlns="http://www.w3.org/2000/svg"
            x="0px"
            y="0px"
            xmlSpace="preserve">
            <path
                id="XMLID_38_"
                d="M68,50.5H24c-2.5,0-4.5-2-4.5-4.5s2-4.5,4.5-4.5h44c2.5,0,4.5,2,4.5,4.5S70.5,50.5,68,50.5z"
            />
        </svg>
    )
}

Minus.propTypes = {
    color: PropTypes.string
}

export default Minus
