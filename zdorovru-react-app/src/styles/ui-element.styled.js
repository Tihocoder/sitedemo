import styled, { css } from 'styled-components'

export const ValidatedInput = css`
    border: 1px solid ${(props) => props.theme.colors.accentGreen};

    :focus {
        border: 1px solid ${(props) => props.theme.colors.getGreenColor(0.8)};
    }

    background-color: inherit;
`
export const NotValidatedInput = css`
    border: 1px solid ${(props) => props.theme.colors.accentRed};

    :focus {
        border: 1px solid ${(props) => props.theme.colors.accentRed};
    }
`

export const BlockColored = styled.div`
    border: 1px solid ${(props) => props.theme.colors.getGreenColor()};
    padding: 0.3rem;
`
export const LoaderWrapper = styled.div`
    width: calc(100% + 1rem);
    margin-left: -0.5rem;
    min-height: 50vh;
    align-items: center;
    position: absolute;
    justify-content: center;
    background-color: white;
    z-index: 10;
`
export const MobileWhiteBlock = styled.div`
    color: ${(props) => props.theme.colors.getGrayColor()};
    border-bottom: 1px solid ${(props) => props.theme.colors.getGrayColor(0.2)};
    padding: 0 0.625rem 0.625rem 0.625rem;
`

export const FixedPageContainer = styled.div`
    background-color: white;
    position: fixed;
    height: 100%;
    width: 100%;
    top: 0;
    left: 0;
    z-index: 20;
`
export const MobileBody = styled.div`
    overflow-x: ${(props) => (props.isNotOverflow ? 'hidden' : 'auto')};
    box-sizing: border-box;
    padding-top: ${(props) => {
        if (props.customPixel) {
            return `${props.customPixel}px`
        }
        if (props.isSearchHeader) {
            if (props.isShowedShipment) return '10.025rem'
            return '3.125rem'
        }
        if (props.isExtendBlock) {
            return '5rem'
        }
        return '2.7rem'
    }};
    padding-bottom: 5rem;
    height: 100%;
    background-color: ${(props) => props.theme.colors.white};
    min-height: 99%;
`

export const MobileBodyModal = styled.div`
    display: flex;
    flex-direction: column;
    max-width: 32rem;
    font-size: 1.2rem;
    padding-bottom: 2rem;
    //color: ${(props) => props.theme.colors.getGrayColor()};
`
export const MobileContainerGrayBody = styled.div`
    background-color: ${(props) => props.theme.colors.grayAkm5};
`
export const GrayAbsoluteBlock = styled.div`
    height: 100%;
    width: 100%;
    position: fixed;
    left: 0;
    top: 0;
    display: flex;
    justify-content: center;
    align-items: center;
    box-sizing: border-box;
    z-index: 10;
    background-color: ${(props) => props.theme.colors.grayAkmBackgroundGray};
    padding: 6rem 1rem;
`
export const SpanNoWrap = styled.span`
    white-space: nowrap;
`
