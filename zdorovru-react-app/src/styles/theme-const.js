export const ColorConsts = {
    black: '#19191D',
    gray: '#B4B4BB',
    grayBackground: '#f7f7f8',
    grayLightColor: '#F9F9FA',
    grayAkmBackgroundGray: '#E1E1E3',
    grayAkm40: '#9696A0',
    grayAkm5: '#F0F0F1',
    grayText: '#787885',
    white: '#FFFFFF',
    blackNavBar: '#132C14',
    blueLight: '#9DC2FF',
    accentGreen: '#5BD240',
    colorCold: '#2979FF',
    accentGreenLight: '#8CE36D',
    backgroundAkmGreen: '#F0FDE2',
    greenYellowLight: '#EAFCD9',
    greenBackground: '#EAFCD9',
    blackGreenText: '#147918',
    accentOrrange: '#FFC147',
    accentOrangeBackground: '#FFCF70',
    accentOrangeBackgroundLight: '#ffd685',
    accentRed: '#EC1C24',
    accentRedLight: '#f04f54',
    redLight: '#F8877F',
    redLightHover: '#f99b94',
    redLightBackground: '#fde2e0',
    pureRed: '#F44336',
    pureRedLight: '#f2786f',
    pureRedBackground: '#FEE7E5',
    redBackground: '#ffdbde',
    orange: '#FCAF5C',
    gray5: '#F0F0F1',

    // getRedheadColor: (opacity = 1) => `rgba(182, 87, 57, ${opacity})`,
    getGreenColor: (opacity = 1) => `rgba(91, 210, 64, ${opacity})`,
    getGrayColor: (opacity = 1) => `rgba(2, 13, 3, ${opacity})`,
    getBlue60Color: (opacity = 1) => `rgba(41, 121, 255, ${opacity})`,
    getBlueColor: (opacity = 1) => `rgba(43, 66, 91, ${opacity})`,
    getRedheadColor: (opacity = 1) => `rgba(236, 28, 36, ${opacity})`,
    getPureRedColor: (opacity = 1) => `rgba(244, 67, 54, ${opacity})`,
    getRedColor: (opacity) => '#EC1C24',
    getRedAccent: (opacity = 1) => `rgba(154, 60, 69, ${opacity})`,
    getBlackColor: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`
}

export const shadowMaterialList = {
    shadow1: '0 1px 3px hsla(0, 0%, 0%, .12), 0 1px 2px hsla(0,0%,0%,.24)',
    shadow2: '0 3px 6px hsla(0, 0%, 0%, .15), 0 2px 4px hsla(0, 0%, 0%, .12)',
    shadow3: '0 10px 20px hsla(0, 0%, 0%, .15), 0 2px 4px hsla(0, 0%, 0%, .10)',
    shadow4: '0 15px 25px hsla(0, 0%, 0%, .15), 0 5px 10px hsla(0, 0%, 0%, .5)',
    shadow5: '0 20px 30px hsla(0, 0%, 0%, .2)',
    shadowKit: '0px -2px 7px 1px rgba(0,0,0,0.045), 0px 8px 8px 0px rgba(0,0,0,0.052);',
    shadowKitHover: '0px -2px 7px 1px rgba(0,0,0,0.04), 0px 8px 8px 0px rgba(0,0,0,0.08);',
    shadowKitLightBlock: '0px -2px 7px 1px rgba(0,0,0,0.03), 0px 8px 8px 0px rgba(0,0,0,0.04);',
    shadowKitLight: '0px 7px 20px -8px rgba(157,157,163,0.1);',
    getShadowKitColor: (color) => `0 4px 20px 0 rgba(0, 0, 0, 0.14), 0 7px 10px -5px ${color}`
}

const orrangelight = ColorConsts.getRedheadColor(0.7)

export const warningButtonLightColors = {
    backgroundColor: ColorConsts.white,
    color: orrangelight,
    hoverBackgroundColor: ColorConsts.getRedheadColor(0.2),
    shadowColor: orrangelight,
    shadowHoverColor: ColorConsts.getRedheadColor(0.2),
    borderColor: orrangelight
}

export const warningButtonColors = {
    backgroundColor: ColorConsts.accentOrangeBackground,
    color: ColorConsts.white,
    hoverBackgro8undColor: ColorConsts.accentOrangeBackgroundLight
}

export const errorButtonLightColors = {
    backgroundColor: ColorConsts.white,
    color: ColorConsts.redLight,
    hoverBackgroundColor: ColorConsts.redLightBackground,
    shadowColor: ColorConsts.redLight,
    shadowHoverColor: ColorConsts.redLightBackground,
    borderColor: ColorConsts.redLight
}
export const errorButtonColors = {
    backgroundColor: ColorConsts.redLight,
    color: ColorConsts.white,
    hoverBackgroundColor: ColorConsts.redLightHover
}

export const deleteButtonColors = {
    backgroundColor: ColorConsts.white,
    color: ColorConsts.accentRedLight,
    hoverBackgroundColor: ColorConsts.redBackground,
    shadowColor: ColorConsts.accentRedLight,
    shadowHoverColor: ColorConsts.redBackground,
    borderColor: ColorConsts.accentRedLight
}

export const successButtonColor = {
    backgroundColor: ColorConsts.accentGreen,
    color: ColorConsts.white,
    hoverBackgroundColor: ColorConsts.accentGreenLight,
    shadowColor: ColorConsts.accentGreenLight,
    shadowHoverColor: ColorConsts.accentGreenLight
}

export const successButtonLight = {
    backgroundColor: ColorConsts.white,
    color: ColorConsts.accentGreen,
    hoverBackgroundColor: ColorConsts.greenBackground,
    shadowColor: ColorConsts.accentGreen,
    shadowHoverColor: ColorConsts.greenBackground,
    borderColor: ColorConsts.accentGreen
}
export const successButtonColorBlcackLight = {
    backgroundColor: ColorConsts.white,
    color: ColorConsts.getGrayColor(1),
    hoverBackgroundColor: ColorConsts.greenBackground,
    shadowColor: ColorConsts.accentGreen,
    shadowHoverColor: ColorConsts.greenBackground,
    borderColor: ColorConsts.accentGreen
}

export const grayButtonLight = {
    backgroundColor: ColorConsts.white,
    color: ColorConsts.gray,
    shadowColor: ColorConsts.gray,
    shadowHoverColor: ColorConsts.grayBackground,
    borderColor: ColorConsts.gray
}

export const ElementSizeType = {
    small: 'small',
    regular: 'regular',
    large: 'large',
    superSmall: 'superSmall'
}

export const PagintaionMaxCount = 6

export const goodsCountOnPage = 30
