import styled, { css } from 'styled-components'
import * as Shared from './base.styled'

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    color: ${(props) => props.theme.colors.getGrayColor(0.7)};
    padding: ${(props) => (props.isMobile ? '0.4rem' : '3rem 0')};
`

export const Title = styled.h2`
    font-size: 1.4rem;
    margin: 0;
    font-weight: bold;
`

export const ContentBlock = styled.div`
    margin-top: 1.8rem;
    &:first-child {
        margin-top: 0;
    }
`

export const Header = styled.h3`
    font-size: 0.9rem;
    margin: 0;
    font-weight: bold;
`
export const BlockBody = styled.div`
    & > * {
        margin-top: 1rem;
    }

    ${Shared.Table} {
        max-width: 40rem;
    }
`
export const UlTitle = styled.div`
    font-size: 0.9rem;
`

export const TextInfo = styled.div``

export const ImagesBlock = styled.div`
    justify-content: space-between;
    max-width: 30rem;
    display: flex;
`

export const Img = styled.img`
    max-width: 7rem;
    height: auto;
    max-height: 4rem;
`

export const SpanBold = styled.span`
    font-weight: bold;
`

export const Ul = styled.ul`
    padding-left: 1rem;
`

export const Li = styled.li`
    ${SpanBold} {
        margin-right: 0.5rem;
    }
`
export const A = styled.a`
    border-bottom: 1px dashed #c1e3e4;
    text-decoration: none;
    width: fit-content;
    cursor: pointer;
    color: ${(props) => props.theme.colors.getGreenColor(1)};
`
export const InsideBlock = styled.div`
    margin-top: 1rem;
    line-height: 1.4rem;
    font-size: 0.9rem;
`

export const SpaceBetween = styled.div`
    display: flex;
    justify-content: space-between;
    width: 100%;
    padding: ${(props) => (props.isMobile ? '0.4rem' : '0')};

    ${(props) =>
        props.isMobile &&
        css`
            overflow-x: auto;
            height: calc(100vh - 8.5rem);
            box-sizing: border-box;
        `}
`
export const NavPageBlock = styled.div`
    display: flex;
    flex-direction: column;
    padding: 0;
    font-size: 0.9rem;
`
export const NavItem = styled.a`
    padding: 0.2rem 0;
    margin: 0;
    text-decoration: none;
    font-weight: bold;
    color: ${(props) => props.theme.colors.getGrayColor(0.7)};
`
export const TableContainerDescription = styled.div`
    & > table {
        box-shadow: none;
    }

    & > table > tr {
        border-bottom: 0;
        height: 2rem;
    }

    & > table > tr > td:nth-child(2) {
        font-weight: bold;
    }

    & > table > tr > td {
        padding: 0 0.8rem 0 0;
    }
`
export const TableCheckerContainer = styled.div`
    & > table {
        box-shadow: none;
    }
    & > table > tr {
        border-bottom: 0;
        height: 2rem;
    }
    & > table > tr:nth-child(2n) {
        background-color: ${(props) => props.theme.colors.getGrayColor(0.03)};
    }
    & > table > tr > td {
        padding: 0.8rem;
        //padding-right: 0.8rem;
    }
    & > table > tr > th {
        padding-right: 0.8rem;
    }
`
export const CartBlock = styled.div`
    padding-right: 4rem;
    padding-top: 1.8rem;
`

export const RuleContainer = styled.div``

export const ImageZdorovCart = styled.img.attrs({
    src: `${process.env.BASE_PATH || ''}/images/zdorov_card.jpg`
})``

export const ImageMosCart = styled.img.attrs({
    src: `${process.env.BASE_PATH || ''}/images/mos_card.jpg`
})``

export const ImageDiscount = styled.img.attrs({
    src: `${process.env.BASE_PATH || ''}/images/discount1_gift.jpg`
})``

export const BodyModalMobileContainer = styled.div`
    display: flex;
    flex-direction: column;
    max-width: 20rem;
    font-size: 1.2rem;
    padding-bottom: 2rem;
`
export const BodyModalMobileP = styled.p`
    text-indent: 1rem;
`
