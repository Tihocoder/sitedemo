import styled, * as S from 'styled-components'

export const Container = styled.div`
    display: flex;
`

export const HistoryOrderBlock = styled.div`
    display: flex;
    flex-direction: column;
    padding: 1rem;
`
export const Body = styled.div`
    margin-left: 1rem;
    width: 100%;
    & > div {
        width: 100%;
    }
`

export const TitlePage = styled.div`
    padding: 0.5rem 0 0.5rem 0;
    font-size: 1.25rem;
    font-weight: bold;
`
