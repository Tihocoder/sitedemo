import styled, { css } from 'styled-components'
import * as Theme from './theme-const'

export const CenterPage = styled.div`
    display: flex;
    justify-content: center;
    //overflow: auto;
    //
    //body {
    //    overflow: auto;
    //}
`

export const PageContainer = styled.div`
    display: flex;
    justify-content: center;
    flex-direction: column;
    height: 100%;
    max-width: 1180px;
    min-width: 960px;
    width: 100%;
`

export const PageMobile = styled.div`
    display: flex;
    flex-direction: column;
    background-color: white;
    font-size: 16px;
    margin: 0;
    min-height: 100%;
    overflow-x: hidden;
`

export const NoWrapSpan = styled.div`
    font: inherit;
    white-space: nowrap;
`

export const PageContainerWithPadding = styled.div`
    display: flex;
    justify-content: center;
    flex-direction: column;
    height: 100%;
    max-width: 1180px;
    min-width: 1180px;
    width: 100%;
    margin-top: 4rem;
    padding: 0 2rem;
    box-sizing: border-box;
`
export const Page = styled.div`
    max-width: 1180px;
    width: 100%;
    flex: 1 0 auto;
    background-color: white;
    height: 100%;
    min-height: calc(100vh - 11rem);
    border-radius: 0.3rem;
`

export const Body = styled.div`
    box-sizing: border-box;
    min-height: 100%;
    margin-top: 2rem;
    padding: ${(props) => props.isPageWrapper && '0 3rem'};
`

export const Button = styled.button`
    padding: 1rem 1.4rem;
    cursor: pointer;
    background-color: ${(props) => props.Color || Theme.ColorConsts.getGreenColor(1)};
    transition: box-shadow 0.2s ease;
    border-radius: 0.3rem;
    font-size: inherit;
    font-weight: bold;
    text-decoration: none;
    text-align: center;
    border: none;
    width: 100%;
    outline: none;
    &:hover {
        box-shadow: ${(props) => props.theme.shadows.getShadowKitColor(props.theme.colors.getGreenColor(1))};
        //transform: translateY(-1px);
    }
`
export const Center = styled.div`
    padding: 5rem;
    height: 100%;
    width: 100%;
    display: flex;
    justify-content: center;
    box-sizing: border-box;
`
export const LoaderWrapper = styled.div`
    width: 8rem;
    & svg circle {
        stroke: ${Theme.ColorConsts.accentGreenLight};
    }
`

export const PageWrapper = styled.div`
    display: flex;
    margin-top: 1rem;
`
export const LeftMenu = styled.div`
    display: flex;
    flex-direction: column;
    max-width: 18.4rem;
    width: 100%;
    & > div {
        /* box-shadow: ${(props) => props.theme.shadows.shadowKit};
        border-radius: 1rem; */
        // padding: 1.5rem;
        /* background-color: white; */
        margin-top: 1rem;
    }
`
export const RightBlock = styled.div`
    box-sizing: border-box;
    width: 100%;
    ${(props) =>
        props.isLeftBlock &&
        css`
            margin-left: 1rem;
        `}
`

export const BannerImageTest = styled.img.attrs({
    src: `${process.env.BASE_PATH}/images/image 8.png`
})``
export const BlockBanner = styled.div`
    width: 184px;
    height: 268px;
`
export const LoaderBlock = styled.div`
    height: 100%;
    width: 100%;
    z-index: 2;
    display: flex;
    justify-content: center;
    align-items: center;
    margin-top: 2rem;

    & > svg {
        width: 10%;
    }

    @media only screen and (max-width: 960px) {
        & > svg {
            width: 20%;
        }
    }

    & > svg circle {
        stroke: ${Theme.ColorConsts.accentGreenLight};
    }
`
export const LoderContentBlock = styled.div`
    display: block;
    flex-direction: column;
`
export const LoaderTitle = styled.div`
    font-size: 1.25rem;
    color: ${Theme.ColorConsts.getGrayColor(1)};
    margin: 3rem 0 0;
    text-align: center;
`

export const Table = styled.table`
    position: relative;
    color: ${(props) => props.theme.colors.getGrayColor(0.7)};
    //: 1px solid ${(props) => props.theme.colors.getGrayColor(0.2)};
    border-collapse: collapse;
    font-size: 13px;
    max-width: 27rem;
    display: block;
`

export const TableHeader = styled.thead`
    padding-bottom: 3px;
`

export const TableHeaderRow = styled.tr`
    border-bottom: 1px solid ${(props) => props.theme.colors.getGrayColor(0.2)};
`

export const TableHeaderColumn = styled.th.attrs({
    nowrap: true
})`
    position: relative;
    vertical-align: bottom;
    text-overflow: ellipsis;
    line-height: 2rem;
    letter-spacing: 0;
    height: 2.5rem;
    color: ${(props) => props.theme.colors.getGrayColor(0.4)};
    border: 1px solid ${(props) => props.theme.colors.getGrayColor(0.2)};
    padding: 0 0.6rem;
    box-sizing: border-box;
    text-align: left;
    white-space: nowrap;
`

export const TableBody = styled.tbody``

export const TableRow = styled.tr`
    position: relative;
    height: 2.5rem;
    transition-duration: 0.28s;
    transition-timing-function: cubic-bezier(0.4, 0, 0.2, 1);
    transition-property: background-color;
    border-bottom: 1px solid ${(props) => props.theme.colors.getGrayColor(0.2)};
    &:last-child {
        border-bottom: 0;
    }
`

export const TableColumn = styled.td`
    position: relative;
    vertical-align: top;
    padding: 0.8rem;
    box-sizing: border-box;
    border: 1px solid ${(props) => props.theme.colors.getGrayColor(0.2)};
`

export const BockWithShadow = styled.a`
    box-shadow: rgba(0, 0, 0, 0.04) 0px -2px 8px 0px, rgba(0, 0, 0, 0.04) 0px 8px 8px 0px;
    box-sizing: border-box;
    padding: 1.5rem;
    border-radius: 0.3rem;
    cursor: pointer;
    text-decoration: none;
`

export const BlockShadowCSS = css`
    box-shadow: rgba(0, 0, 0, 0.04) 0px -2px 8px 0px, rgba(0, 0, 0, 0.04) 0px 8px 8px 0px;
    border-radius: 0.3rem;
`
export const PaddingInPage = css`
    padding: 0px 3rem;
`
export const ButtonLink = styled.a`
    border: 0;
    background-color: ${(props) => props.colors.backgroundColor};
    color: ${(props) => props.colors.color};
    border-radius: 0.3rem;
    transition: all 0.2s ease;
    font-size: inherit;
    font-weight: bold;
    cursor: pointer;
    text-decoration: none;
    box-sizing: border-box;
    ${(props) =>
        props.isShadow && !props.colors.borderColor
            ? css`
                  box-shadow: ${props.theme.shadows.getShadowKitColor(props.colors.shadowColor)};
              `
            : ''}

    ${(props) =>
        props.colors.borderColor &&
        css`
            border: 1px solid ${props.colors.borderColor};
        `};
    &:hover {
        ${(props) =>
            props.isShadow
                ? css`
                      box-shadow: ${props.theme.shadows.getShadowKitColor(props.colors.shadowHoverColor)};
                  `
                : css`
                      background-color: ${props.colors.hoverBackgroundColor};
                  `};
    }
    ${(props) => {
        if (props.elementSizeTypeValue === Theme.ElementSizeType.large) {
            return css`
                font-size: 1.2rem;
                padding: 0.9rem 1.6rem;
            `
        }
        if (props.elementSizeTypeValue === Theme.ElementSizeType.regular) {
            return css`
                padding: 0.7rem 1.2rem;
            `
        }
        if (props.elementSizeTypeValue === Theme.ElementSizeType.small) {
            return css`
                font-size: 0.8rem;
                min-width: 9rem;
                padding: 0.7rem 1.2rem;
            `
        }
        if (props.elementSizeTypeValue === Theme.ElementSizeType.superSmall) {
            return css`
                font-size: 0.8rem;
            `
        }
    }};

    ${(props) =>
        props.isDisabled &&
        css`
            box-shadow: none;
            opacity: 0.6;
            &:hover {
                box-shadow: none;
            }
            // cursor: default;
        `}
`

export const PageTitile = styled.div`
    font-size: 1.2rem;
`

export const BodyDescriptionChangeStock = styled.div`
    max-width: 20rem;
`
