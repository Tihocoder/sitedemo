import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'
import { useRouter } from 'next/router'
import { BodyDescriptionChangeStock } from '../styles/base.styled'
import * as Theme from '../styles/theme-const'
import { parseCookies } from 'nookies'
import ym from 'react-yandex-metrika'
import ModalWindowCustom from '../components/shared/ModalWindowCustom/ModalWindowCustom'
import Button from '../components/shared/Button/Button'
import { addPathAndDeleteBack, changeShipment } from '../actions/customer'

const isProduction = process.env.ENVS === 'prod'

export const InitSiteData = (props) => {
    const dispatch = useDispatch()
    const router = useRouter()
    const [isChangeModalStock, setIsChangeModalStock] = useState(false)
    const [isChangeModalStockSavel, setIsChangeModalStockSavel] = useState(false)
    const [isChangeModalStockVashavskaya, setIsChangeModalStockVashavskaya] = useState(false)

    const selectedtock = async (stockId) => {
        await dispatch(changeShipment({ stockId: stockId }))
        setIsChangeModalStockSavel(false)
        router.reload()
    }

    useEffect(() => {
        const cookies = parseCookies()
        const shipmentCookie = cookies['storage-shipment']
        if (shipmentCookie) {
            const shipment = JSON.parse(shipmentCookie)
            if (shipment.stockId === 167) {
                setIsChangeModalStock(true)
            }
            if (shipment.stockId === 179) {
                setIsChangeModalStockSavel(true)
            }
            if (shipment.stockId === 291) {
                setIsChangeModalStockVashavskaya(true)
            }
        }
        if (isProduction) {
            ym('hit', router.asPath)
        }
        // initData()
    }, [])
    //

    useEffect(() => {
        const addPathAsync = async () => {
            await dispatch(addPathAndDeleteBack(router.asPath))
        }
        addPathAsync()
    }, [router.asPath])
    return (
        <React.Fragment>
            {isChangeModalStock && (
                <ModalWindowCustom
                    title={'Внимание'}
                    maxWidth={'40rem'}
                    cancel={{
                        text: 'Отмена',
                        clickHandler: () => selectedtock(307)
                    }}
                    footer={
                        <Button
                            elementSizeTypeValue={Theme.ElementSizeType.large}
                            clickHandler={() => selectedtock(307)}>
                            {'Подтвердить'}
                        </Button>
                    }>
                    <BodyDescriptionChangeStock>
                        Аптека м. Пражская переезжает по адресу ул. Красного маяка, 2Б, ТЦ Columbus Южная
                        галерея
                        <br />
                        <br />
                        Аптека самовывоза будет изменена автоматически.
                    </BodyDescriptionChangeStock>
                </ModalWindowCustom>
            )}
            {isChangeModalStockVashavskaya && (
                <ModalWindowCustom
                    title={'Внимание'}
                    maxWidth={'40rem'}
                    cancel={{
                        text: 'Отмена',
                        clickHandler: () => selectedtock(309)
                    }}
                    footer={
                        <Button
                            elementSizeTypeValue={Theme.ElementSizeType.large}
                            clickHandler={() => selectedtock(309)}>
                            {'Подтвердить'}
                        </Button>
                    }>
                    <BodyDescriptionChangeStock>
                        Аптека м.Варшавская переехала по адресу Чонгарский бульвар, д. 5к1
                        <br />
                        <br />
                        Аптека самовывоза будет изменена автоматически.
                    </BodyDescriptionChangeStock>
                </ModalWindowCustom>
            )}
            {isChangeModalStockSavel && (
                <ModalWindowCustom
                    cancel={{
                        text: 'Отмена',
                        clickHandler: () => selectedtock(0)
                    }}
                    footer={
                        <Button
                            elementSizeTypeValue={Theme.ElementSizeType.large}
                            clickHandler={() => selectedtock(0)}>
                            {'Подтвердить'}
                        </Button>
                    }>
                    <BodyDescriptionChangeStock>
                        Аптека м. Савеловская временно закрыта на ремонт. Пожалуйста выберите для бронирования
                        товара другую аптеку.
                    </BodyDescriptionChangeStock>
                </ModalWindowCustom>
            )}
            {props.children}
        </React.Fragment>
    )
}
