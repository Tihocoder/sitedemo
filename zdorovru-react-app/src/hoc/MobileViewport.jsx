import React, { useEffect, useRef, useState } from 'react'

export const MobileViewport = ({ as: Element = 'div', children, style = {}, ...props }) => {
    const ref = useRef(null)

    const [viewport, setViewport] = useState({
        maxHeight: '100vh',
        maxWidth: '100vw'
    })

    const updateViewport = () => {
        setViewport({
            maxHeight: window.visualViewport.height,
            maxWidth: window.visualViewport.width
        })

        //window.scrollTo(0, ref.current.offsetTop)
    }

    // eslint-disable-next-line consistent-return
    useEffect(() => {
        if (typeof window !== 'undefined' && typeof window.visualViewport !== 'undefined') {
            updateViewport()

            window.visualViewport.addEventListener('resize', updateViewport)

            return () => window.visualViewport.removeEventListener('resize', updateViewport)
        }
    }, [])

    return (
        // eslint-disable-next-line react/jsx-props-no-spreading
        <Element ref={ref} style={{ ...style, ...viewport }} {...props}>
            {children}
        </Element>
    )
}
