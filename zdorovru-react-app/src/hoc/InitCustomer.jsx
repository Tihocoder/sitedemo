import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { initCustomerData } from '../actions/customer'
import { useRouter } from 'next/router'

export const InitCustomer = (props) => {
    const dispatch = useDispatch()

    const initCustomerAsync = async () => {
        await dispatch(initCustomerData())
    }
    useEffect(() => {
        initCustomerAsync()
    }, [])

    return <React.Fragment>{props.children}</React.Fragment>
}
