import PropTypes from 'prop-types'

export const infoListTypes = {
    FarmGroup: 1,
    FarmAction: 2,
    MNN: 10
}

export const PaymentType = {
    Unknown: 0,
    CashCourier: 1,
    CardOnline: 2,
    CardCourier: 3,
    Pickup: 4
}



export const EmailRegular = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
export const CountDaysInLocalStorage = 30

export const _PricePropTypes = PropTypes.shape({
    normal: PropTypes.number,
    withoutPromo: PropTypes.number,
    aptEtaDateTime: PropTypes.string,
    isDemand: PropTypes.bool
})


export const _WebDataGoodGroups = PropTypes.arrayOf(
    PropTypes.shape({
        clsGroupID: PropTypes.number.isRequired,
        isDefault: PropTypes.bool.isRequired,
        groupUrl: PropTypes.string.isRequired
    })
)
export const _WebDataPropTypes = PropTypes.shape({
    webId: PropTypes.number.isRequired,
    goodId: PropTypes.number,
    drugTitle: PropTypes.string.isRequired,
    outFormTitle: PropTypes.string.isRequired,
    makerTitle: PropTypes.string.isRequired,
    stockPrice: _PricePropTypes,
    deliveryPrice: _PricePropTypes,
    url: PropTypes.string,
    goodGroups: _WebDataGoodGroups,
    isStrictlyByPrescription: PropTypes.bool.isRequired,
    hasImage: PropTypes.bool.isRequired
}).isRequired

export const HouseType = {
    apt: 0,
    house: 1,
    office: 2
}

export const GoodsPageType = {
    catalog: 1,
    search: 2,
    promotion: 3
}

export const CompanyEnum = {
    PharmProgress: 6,
    HippoRuber: 9,
    ZdorovRu: 10,
    TechnoPharm: 13,
    AlphaPharm: 14,
    NordPhram: 15
}

export const FeaturesEnum = {
    Cold: 'DescriptionCold',
    DemandBasketAdd: 'DescriptionDemandBasketAdd',
    DemandDeliveryBasketAdd: 'DescriptionDemandDeliveryBasketAdd',
    DemandPVZBasketAdd: 'DescriptionDemandPVZBasketAdd',
    DescriptionDemandStockAndDeliveryBasketAdd: 'DescriptionDemandStockAndDeliveryBasketAdd',
    NotAvailable: 'DescriptionNotAvailable',
    Prescription: 'DescriptionPrescription'
}

export const RuleEnum = {
    Delivery: 1,
    Pickup: 2,
    TermsOfUse: 3,
    PrivacyPolicy: 4
}

export const OrderType = {
    id: PropTypes.number.isRequired,
    sum: PropTypes.number.isRequired,
    no: PropTypes.string.isRequired,
    date: PropTypes.string.isRequired,
    stateTitle: PropTypes.string,
    statePaymentTitle: PropTypes.string,
    isDelivery: PropTypes.bool.isRequired,
    stockData: PropTypes.shape({
        id: PropTypes.number,
        title: PropTypes.string,
        address: PropTypes.string,
        timeString: PropTypes.string
    }),
    deliveryData: PropTypes.shape({
        timeSlotText: PropTypes.string,
        shipAddressText: PropTypes.string,
        paymentTypeName: PropTypes.string
    })
}

export const PromotionType = {
    id: PropTypes.number.isRequired,
    promotionId: PropTypes.number.isRequired,
    promotionTypes: PropTypes.number.isRequired,
    promotionTypeName: PropTypes.string.isRequired,
    beginDate: PropTypes.string.isRequired,
    endDate: PropTypes.string.isRequired,
    promotionName: PropTypes.string.isRequired,
    promotionDescription: PropTypes.string.isRequired,
    type: PropTypes.number.isRequired,
    typeName: PropTypes.string.isRequired,
    imageUrl: PropTypes.string.isRequired
}
export const PriceType = {
    normal: PropTypes.number,
    withoutPromo: PropTypes.number,
    aptEtaDateTime: PropTypes.string,
    isDemand: PropTypes.bool
}
export const DeliveryStatusType = {
    reasons: PropTypes.arrayOf(
        PropTypes.shape({
            deliveryReasonID: PropTypes.number.isRequired,
            warningText: PropTypes.string.isRequired
        })
    ),
    isAvailableForDelivery: PropTypes.bool.isRequired
}
