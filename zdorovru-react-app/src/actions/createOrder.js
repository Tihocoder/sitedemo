import { Emailvalidate, PhoneValidate } from '@helpers/validations'
import {
    CREATE_ORDER__SET_PAYMENT,
    CREATE_ORDER__SET_TIMESLOT,
    CREATE_ORDER_INIT_VALIDATE,
    INIT_CREATE_ORDER_DATA,
    PREORDER_CHANGE,
    PREORDER_CLEAR,
    PREORDER_STOP,
    PREORDER_STOP_LOADING,
    RESET_VALIDATE,
    SET_ANOTHER_USER_CONTACT,
    SET_DELIVERY_DATA,
    SET_USER_CONTACT,
    SET_VALIDATE_DELIVERY_DATA,
    SET_VALIDATE_FORM,
    SET_VALIDATE_FORM_ANOTHER_USER
} from './types'

export const resetValidate = () => (dispatch) => {
    console.log('resetValidate run')
    dispatch({ type: RESET_VALIDATE })
}

export const changeFormData = (name, value) => async (dispatch) => {
    if (name === 'name') {
        const isValid = !!value
        dispatch({ type: SET_VALIDATE_FORM, data: { name, value: isValid } })
    }
    if (name === 'email') {
        dispatch({ type: SET_VALIDATE_FORM, data: { name, value: Emailvalidate(value) } })
    }
    if (name === 'phone') {
        dispatch({ type: SET_VALIDATE_FORM, data: { name, value: PhoneValidate(value) } })
    }
    dispatch({ type: SET_USER_CONTACT, data: { name, value } })
}

export const changeAnotherContactData = (name, value) => async (dispatch) => {
    if (name === 'name') {
        const isValid = !!value
        dispatch({ type: SET_VALIDATE_FORM_ANOTHER_USER, data: { name, value: isValid } })
    }
    if (name === 'email') {
        dispatch({ type: SET_VALIDATE_FORM_ANOTHER_USER, data: { name, value: Emailvalidate(value) } })
    }
    if (name === 'phone') {
        dispatch({ type: SET_VALIDATE_FORM_ANOTHER_USER, data: { name, value: PhoneValidate(value) } })
    }
    dispatch({ type: SET_ANOTHER_USER_CONTACT, data: { name, value } })
}

export const changeDeliveryData = (name, value) => async (dispatch, getState) => {
    dispatch({ type: SET_DELIVERY_DATA, data: { name, value } })
    dispatch({ type: SET_VALIDATE_DELIVERY_DATA, data: { name, value: true } })
}

export const initValidate = (validateForm) => async (dispatch) => {
    dispatch({ type: CREATE_ORDER_INIT_VALIDATE, data: validateForm })
}

export const initCreateOrderData = () => async (dispatch) => {
    const createOrderData = JSON.parse(localStorage.getItem('create-order-data'))
    if (createOrderData) {
        dispatch({ type: INIT_CREATE_ORDER_DATA, data: createOrderData })
    }
}

export const setPayment = (type, cashExchange = null) => (dispatch) => {
    dispatch(resetValidate())
    dispatch({
        type: CREATE_ORDER__SET_PAYMENT,
        data: {
            paymentType: type,
            cashExchange
        }
    })
}
export const setShipTimeSlot = (id, isNearestTime = false) => (dispatch) => {
    dispatch(resetValidate())
    dispatch({
        type: CREATE_ORDER__SET_TIMESLOT,
        data: {
            shipTimeSlotId: id,
            isNearestTime
        }
    })
}

export const preorderChange = (data) => (dispatch) => {
    dispatch({ type: PREORDER_CHANGE, data })
}

export const preorderStop = () => async (dispatch) => {
    await dispatch({ type: PREORDER_STOP })
}

export const preorderStopLoading = () => (dispatch) => {
    dispatch({ type: PREORDER_STOP_LOADING })
}

export const preorderClear = () => (dispatch) => {
    dispatch({ type: PREORDER_CLEAR })
}
