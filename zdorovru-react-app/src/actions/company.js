import { getCities, getFeatures, getMainGroups, getStocks } from '../api/siteApis'
import { INIT_FEATURES, INIT_GROUPS, SET_CITY, SET_STOCKS } from './types'
import { initBaseImageUrl } from './catalog'

export const initSiteData = () => {
    return async (dispatch, getState) => {
        if (getState().company.cities.length === 0) {
            const city = await getCities()
            dispatch({ type: SET_CITY, data: city })
        }
        if (getState().company.stocks.length === 0) {
            const stocks = await getStocks()
            dispatch({ type: SET_STOCKS, data: stocks })
        }
        if (getState().catalog.mainGroups.length === 0) {
            const mainGroups = await getMainGroups()
            dispatch({ type: INIT_GROUPS, data: mainGroups })
        }
        if (!getState().catalog.baseImageUrl) {
            await dispatch(initBaseImageUrl())
        }

        if (Object.entries(getState().catalog.features).length === 0) {
            const features = await getFeatures()
            let featuresObj = {}
            features.forEach((feature) => {
                if (feature) featuresObj = { ...featuresObj, [feature.name]: feature.value }
            })
            dispatch({ type: INIT_FEATURES, data: featuresObj })
        }
    }
}
