import { destroyCookie, parseCookies, setCookie } from 'nookies'
import {
    getUpdatedGoods,
    mergeFavorits,
    saveFavorite,
    deleteFavorite,
    getFavorits,
    getAddressById
} from '@api/siteApis'
import { sumPrice, updateBasketPostitions, updateBasketPostitionsAndChangePrice } from '@helpers/order'
import { getActualBasketPosition, getActualGoods, updateCustomerStorageInLocal } from '@helpers/site'
import { CountDaysInLocalStorage } from '../consts'
import { initShipment } from './catalog'
import { initSiteData } from './company'
import {
    ADD_BASKET_ITEM,
    ADD_HISTORY_PATH,
    CHANGE_BASKET_ITEM,
    POP_HISTORY_PATH,
    REMOVE_BASKET_ITEM,
    REMOVE_BASKET_ITEM_BY_ID,
    SELECT_SHIPMENT,
    SELECT_STOCK,
    SELECTED_CITY,
    SET_BASKET_ISDELIVERY,
    SET_BASKET_SHIPMENT,
    SET_IS_CONVERTED,
    SET_ORDER_ID_ON_BASKET,
    TOGGLE_BASKET_ISDELIVERY,
    TOGGLE_HIDDEN_BASKET_ITEM,
    TOGGLE_LIKED_GOOD,
    INIT_BASKET_ITEMS,
    INIT_LIKED_GOODS,
    INIT_DELIVERY_TRESHHOLD,
    INIT_TIME_SLOTS,
    CLEAR_BASKET,
    SET_BASKET_SHIPADDRESS,
    SET_BASKET_SHIPMENT_STOCK,
    INIT_PICKUP_TIME_SLOTS
} from './types'

export const GoodDataObj = {
    goodId: 0,
    webId: '',
    drugTitle: '',
    outFormTitle: '',
    makerTitle: '',
    hasImage: false
}
export const PriceDataObj = {
    normal: 0,
    discount: 0,
    promo: 0,
    isDemand: false,
    aptEtaDateTime: null
}

export const initCustomerLikedGoods = () => async (dispatch, getState) => {
    const cookies = parseCookies()
    const token = cookies.customer_token
    // eslint-disable-next-line no-use-before-define
    const likedGoods = getLikedSorage()
    const newLikedGoods = await getActualGoods(likedGoods)
    if (token) {
        const favorits = await getFavorits()
        if (favorits === 401) {
            if (newLikedGoods && newLikedGoods.length > 0) {
                dispatch({
                    type: INIT_LIKED_GOODS,
                    data: newLikedGoods
                })
            }
        } else {
            dispatch({
                type: INIT_LIKED_GOODS,
                data: favorits
            })
        }
    } else if (newLikedGoods && newLikedGoods.length > 0) {
        dispatch({
            type: INIT_LIKED_GOODS,
            data: newLikedGoods
        })
    }
    setCookie(null, 'is-converted-liked', true, {
        maxAge: 1 * 365 * 24 * 60 * 60,
        path: '/'
    })
    await dispatch({ type: SET_IS_CONVERTED, value: true })
    updateCustomerStorageInLocal(getState)
}
export const updateActualBasketPostions = (isDeliverySelect) => async (dispatch, getState) => {
    const isDelivery =
        isDeliverySelect !== true && isDeliverySelect !== false
            ? getState().customer.basket.isDelivery
            : isDeliverySelect

    const { shipment } = getState().customer
    const { basketItems } = getState().customer
    const response = await getUpdatedGoods(
        basketItems.map((item) => ({
            webId: item.webData.webId,
            goodId: item.webData.goodId
        })),
        shipment
    )
    const updatedBasketItems = updateBasketPostitions(basketItems, response.basketItems)
    dispatch({ type: INIT_BASKET_ITEMS, data: updatedBasketItems })
    dispatch({
        type: INIT_DELIVERY_TRESHHOLD,
        data: {
            minSum: response.deliveryTreshhold.minFreeDeliverySum,
            cost: response.deliveryTreshhold.deliveryCost
        }
    })
}

export const initCustomerData = () => async (dispatch, getState) => {
    const cookies = parseCookies()
    const shipmentCookieString = cookies['storage-shipment']
    if (shipmentCookieString) {
        const shipmentCookie = JSON.parse(shipmentCookieString)
        const { shipment } = getState().customer
        if (!shipment.stockId) {
            await dispatch(initShipment(shipmentCookie))
        }
    }
    await dispatch(initSiteData())
    const storageCookie = localStorage.getItem('zdorovru-customer')

    if (storageCookie) {
        const customerStorageString = JSON.parse(storageCookie)
        const oneDay = 24 * 60 * 60 * 1000
        const { customer } = customerStorageString
        const basketUpdateDate = Date.parse(customer.updateDate)
        const currentDate = Date.now()
        const diffDays = Math.abs((currentDate - basketUpdateDate) / oneDay)
        if (diffDays > CountDaysInLocalStorage) return

        const storageBasketItems = getActualBasketPosition(customer.basketItems)
        dispatch({ type: INIT_BASKET_ITEMS, data: storageBasketItems })
        setCookie(null, 'is-converted-basket', true, {
            maxAge: 1 * 365 * 24 * 60 * 60,
            path: '/'
        })
        await dispatch(initCustomerLikedGoods())

        dispatch({ type: SET_ORDER_ID_ON_BASKET, data: customer.orderId })
        await dispatch(updateActualBasketPostions())
    }

    updateCustomerStorageInLocal(getState)
}
const getLikedSorage = () => {
    const storage = localStorage.getItem('zdorovru-customer')
    if (!storage) return []
    const customerStorageString = JSON.parse(storage)
    return customerStorageString?.customer?.likedGoods
}

export const mergeCustomerLikedGoods = (token) => async (dispatch, getState) => {
    const { likedGoods } = getState().customer

    if (token) {
        const favorits = await mergeFavorits(likedGoods)
        if (favorits && favorits.length > 0)
            dispatch({
                type: INIT_LIKED_GOODS,
                data: favorits
            })
    }
    updateCustomerStorageInLocal(getState)
}

export const addBasketItem = (good, count = 1) => async (dispatch, getState) => {
    const item = getState().customer.basketItems.find((x) => x.webData.webId === good.webData.webId)
    if (item) return
    dispatch({
        type: ADD_BASKET_ITEM,
        data: {
            ...good,
            count,
            deliveryPriceSum: sumPrice(
                good.deliveryPrice.normal,
                count,
                good.isAvailable,
                good.deliveryStatus.isAvailableForDelivery
            ),
            stockPriceSum: sumPrice(good.stockPrice.normal, count, good.isAvailable, good.isAvailableOnStock),
            minPriceSum: sumPrice(good.minPrice.normal, count, true, true),
            isHidden: false
        }
    })
    updateCustomerStorageInLocal(getState)
}
export const hiddenBasketItem = (goodId) => async (dispatch, getState) => {
    const item = getState().customer.basketItems.find((x) => x.webData.goodId === goodId)
    if (!item) return
    const basketItems = getState().customer.basketItems.map((x) => {
        if (x.webData.goodId === goodId) {
            return { ...x, isHidden: true }
        }
        return x
    })
    dispatch({ type: INIT_BASKET_ITEMS, data: basketItems })
}

export const toggleHiddenBasketItem = (goodId) => async (dispatch, getState) => {
    dispatch({ type: TOGGLE_HIDDEN_BASKET_ITEM, data: goodId })
    updateCustomerStorageInLocal(getState)
}
export const clearLikedGoods = () => async (dispatch, getState) => {
    dispatch({
        type: INIT_LIKED_GOODS,
        data: []
    })
    updateCustomerStorageInLocal(getState)
}

export const clearBasketOrderCreated = (orderId) => (dispatch, getState) => {
    const cookies = parseCookies()
    const cookieOrderId = cookies.orderid
    if (cookieOrderId && cookieOrderId !== 0) {
        const cookieOrderIdInt = parseInt(cookieOrderId, 10)
        if (cookieOrderIdInt === orderId) {
            setCookie(null, 'orderid', 0, {
                maxAge: 365 * 24 * 60 * 60,
                path: '/'
            })
            destroyCookie({}, 'orderid', { path: '/' })
            dispatch({ type: CLEAR_BASKET })
        }
    }

    updateCustomerStorageInLocal(getState)
}

export const removeBasketItem = (webId) => (dispatch, getState) => {
    dispatch({ type: REMOVE_BASKET_ITEM, data: { webId } })
    updateCustomerStorageInLocal(getState)
}
export const removeBasketItemById = (goodId) => (dispatch, getState) => {
    dispatch({ type: REMOVE_BASKET_ITEM_BY_ID, data: goodId })
    updateCustomerStorageInLocal(getState)
}

export const setBasketStock = (stock) => async (dispatch) => {
    dispatch({ type: SET_BASKET_SHIPMENT_STOCK, data: stock })
}

export const setOrderId = (orderId, redirectUrl, router) => async (dispatch, getState) => {
    dispatch({ type: SET_ORDER_ID_ON_BASKET, data: orderId })
    updateCustomerStorageInLocal(getState)
    router.push(redirectUrl)
}

export const toggleLikedGood = (goodId) => async (dispatch, getState) => {
    const isFavorite = getState().customer.likedGoods.includes(goodId)
    if (isFavorite) deleteFavorite(goodId)
    else saveFavorite(goodId)
    dispatch({ type: TOGGLE_LIKED_GOOD, data: goodId })
    updateCustomerStorageInLocal(getState)
}

export const setShipmentBasket = (shipment) => async (dispatch) => {
    dispatch({ type: SET_BASKET_SHIPMENT, data: shipment })
}

export const changeBasketItemPrice = (id, count) => async (dispatch, getState) => {
    const currentBasketItem = {
        ...getState().customer.basketItems.find((item) => item.webData.webId === id)
    }
    if (
        currentBasketItem.isChanged &&
        currentBasketItem.bookedCount &&
        count <= currentBasketItem.bookedCount
    ) {
        currentBasketItem.isChanged = false
    }
    currentBasketItem.stockPriceSum = sumPrice(
        currentBasketItem.stockPrice.normal,
        count,
        currentBasketItem.isAvailable,
        currentBasketItem.isAvailableOnStock
    )
    currentBasketItem.deliveryPriceSum = sumPrice(
        currentBasketItem.deliveryPrice.normal,
        count,
        currentBasketItem.isAvailable,
        currentBasketItem.deliveryStatus.isAvailableForDelivery
    )
    currentBasketItem.minPriceSum = sumPrice(currentBasketItem.minPrice.normal, count, true, true)

    currentBasketItem.count = count
    dispatch({ type: CHANGE_BASKET_ITEM, data: currentBasketItem })
    updateCustomerStorageInLocal(getState)
}

export const selectCity = (cityId) => async (dispatch, getState) => {
    if (getState().customer.shipment.cityId !== cityId) {
        dispatch({ type: SELECTED_CITY, data: cityId })
        dispatch({ type: SELECT_STOCK, data: 0 })
        updateCustomerStorageInLocal(getState)
    }
}

export const changeShipment = (newShipment) => async (dispatch, getState) => {
    const { shipment } = getState().customer
    const { basketItems } = getState().customer
    if (
        shipment.cityId !== newShipment.cityId ||
        shipment.stockId !== newShipment.stockId ||
        shipment.shipAddressId !== newShipment.shipAddressId
    ) {
        dispatch({ type: SELECT_SHIPMENT, data: newShipment })

        if (newShipment.stockId && !newShipment.stockTitle) {
            const { stocks } = getState().company
            if (stocks.length) {
                const stock = stocks.find((x) => x.id === newShipment.stockId)
                dispatch({
                    type: SELECT_SHIPMENT,
                    data: { stockTitle: `${stock.title} ${stock.address}` }
                })
            }
        }

        if (newShipment.shipAddressId && !newShipment.shipAddressTitle) {
            const responseAddress = await getAddressById(newShipment.shipAddressId)
            if (responseAddress?.address) {
                dispatch({
                    type: SELECT_SHIPMENT,
                    data: { shipAddressTitle: responseAddress.address }
                })
            }
        }

        if (basketItems.length > 0) {
            const response = await getUpdatedGoods(
                basketItems.map((item) => ({
                    webId: item.webData.webId,
                    goodId: item.webData.goodId
                })),
                {
                    ...shipment,
                    ...newShipment
                }
            )
            const updatedBasketItems = updateBasketPostitionsAndChangePrice(basketItems, response.basketItems)
            dispatch({ type: INIT_BASKET_ITEMS, data: updatedBasketItems })
            dispatch({
                type: INIT_DELIVERY_TRESHHOLD,
                data: {
                    minSum: response.deliveryTreshhold.minFreeDeliverySum,
                    cost: response.deliveryTreshhold.deliveryCost
                }
            })
        }
        updateCustomerStorageInLocal(getState)
    }
}
export const toggleBasketIsdelivery = (isInit, isDelivery) => async (dispatch, getState) => {
    if (isInit) {
        const isDeliveryBasket = getState().customer.basket.isDelivery
        if (isDeliveryBasket !== isDelivery) {
            dispatch({ type: TOGGLE_BASKET_ISDELIVERY })
        }
    } else {
        dispatch({ type: TOGGLE_BASKET_ISDELIVERY })
        updateCustomerStorageInLocal(getState)
    }
}

export const changeAddress = (shipAddress) => async (dispatch, getState) => {
    const { shipment } = getState().customer
    const { basketItems } = getState().customer
    const response = await getUpdatedGoods(
        basketItems.map((item) => ({
            webId: item.webData.webId,
            goodId: item.webData.goodId
        })),
        { ...shipment, shipAddressId: shipAddress.id }
    )
    dispatch({ type: SET_BASKET_SHIPADDRESS, data: shipAddress })
    const updatedBasketItems = updateBasketPostitionsAndChangePrice(basketItems, response.basketItems)
    dispatch({ type: INIT_BASKET_ITEMS, data: updatedBasketItems })
    dispatch({
        type: INIT_DELIVERY_TRESHHOLD,
        data: {
            minSum: response.deliveryTreshhold.minFreeDeliverySum,
            cost: response.deliveryTreshhold.deliveryCost
        }
    })
    dispatch({ type: INIT_TIME_SLOTS, data: response.timeSlots })
}

export const setBasketShipment = (isDelivery) => async (dispatch) => {
    dispatch({ type: SET_BASKET_ISDELIVERY, value: isDelivery })
}

export const popPath = (backHandler) => async (dispatch, getState) => {
    const paths = [...getState().customer.historyPaths]
    if (paths && paths.length && paths.length > 1) {
        await dispatch({ type: POP_HISTORY_PATH })
        await backHandler()
    }
}
export const updateBasketFromPreorder = (basketPositions) => async (dispatch, getState) => {
    dispatch({ type: INIT_BASKET_ITEMS, data: basketPositions })
    updateCustomerStorageInLocal(getState)
}
export const addPathAndDeleteBack = (path) => async (dispatch, getState) => {
    const paths = getState().customer.historyPaths
    if (!paths.length || paths.length === 0) {
        await dispatch({ type: ADD_HISTORY_PATH, value: path })
        return
    }
    if (paths.length === 1) {
        if (paths[0] === path) return
        await dispatch({ type: ADD_HISTORY_PATH, value: path })
        return
    }
    if (paths[paths.length - 1] === path) return
    if (paths[paths.length - 2] === path) {
        await dispatch({ type: POP_HISTORY_PATH })
        return
    }
    await dispatch({ type: ADD_HISTORY_PATH, value: path })
}
