import { destroyCookie, parseCookies } from 'nookies'
import {
    getGroupGoods,
    getSearch,
    searchGroups,
    getPromotionGoods,
    getAnotherOutForms,
    getBaseImageUrl,
    getCategoriesTreeByGoodIds,
    getGoodsByIds,
    getAnalogues,
    getPurchasedItems,
    getFavoritsCatalog,
    getAddressById
} from '@api/siteApis'
import {
    CURRENT_GROUP_ID,
    GOODS_SHIPMENT,
    INIT_GOODS,
    PAGE_LANDING_OFF,
    PAGE_LANDING_ON,
    REMOVE_GOODS_FILTER,
    SELECT_SHIPMENT,
    SET_BASE_IMAGE_URL,
    SET_FOUND_CATEGORIES,
    INIT_CATEGORIES,
    SET_GOODS_FILTER,
    INIT_GOODS_GROUPS,
    SET_SEARCH_TEXT,
    REMOVE_GOODS_GROUPS
} from './types'

export const initBaseImageUrl = () => async (dispatch, getState) => {
    if (!getState().catalog.baseImageUrl) {
        const baseImageUrl = await getBaseImageUrl()
        if (baseImageUrl?.url) dispatch({ type: SET_BASE_IMAGE_URL, data: baseImageUrl.url })
    }
}

export const initGoodsByGroupId = (groupId, shipment) => async (dispatch) => {
    await dispatch(initBaseImageUrl())
    dispatch({ type: PAGE_LANDING_ON })
    const fullGroup = await getGroupGoods(groupId, shipment)
    dispatch({
        type: SELECT_SHIPMENT,
        data: shipment
    })
    dispatch({ type: INIT_GOODS, data: fullGroup.goods })
    dispatch({ type: INIT_GOODS_GROUPS, data: fullGroup.groups })
    dispatch({ type: GOODS_SHIPMENT, data: shipment })
    dispatch({ type: CURRENT_GROUP_ID, data: parseInt(groupId, 10) })
    dispatch({ type: PAGE_LANDING_OFF })
}
export const removeGroupsShipment = () => async (dispatch) => {
    dispatch({ type: REMOVE_GOODS_GROUPS })
}

export const initShipment = (shipment) => async (dispatch, getState) => {
    dispatch({ type: SELECT_SHIPMENT, data: shipment })

    if (shipment.stockId && !shipment.stockTitle) {
        const { stocks } = getState().company
        if (stocks.length) {
            const stock = stocks.find((x) => x.id === shipment.stockId)
            dispatch({
                type: SELECT_SHIPMENT,
                data: { stockTitle: `${stock.title} ${stock.address}` }
            })
        }
    }

    if (shipment.shipAddressId && !shipment.shipAddressTitle) {
        const responseAddress = await getAddressById(shipment.shipAddressId)
        if (responseAddress?.address) {
            dispatch({
                type: SELECT_SHIPMENT,
                data: { shipAddressTitle: responseAddress.address }
            })
        }
    }
}

export const setGoodsFilter = (name, array) => async (dispatch) => {
    dispatch({ type: SET_GOODS_FILTER, data: { name, value: array } })
}
export const removerGoodsFilter = (name) => async (dispatch) => {
    dispatch({ type: REMOVE_GOODS_FILTER, data: name })
}

export const initGoodsBySearch = (q, shipment) => async (dispatch, getState) => {
    const { searchText } = getState().catalog

    if (searchText !== q) {
        dispatch({ type: PAGE_LANDING_ON })
        const goods = await getSearch(q, shipment)
        const foundGroups = await searchGroups(shipment.cityId, q)
        dispatch({ type: SET_FOUND_CATEGORIES, data: foundGroups })
        const categoriesTree = await getCategoriesTreeByGoodIds(
            goods?.length ? goods.map((x) => x.webData.goodId) : []
        )
        dispatch({ type: INIT_CATEGORIES, data: categoriesTree?.length ? categoriesTree : [] })
        dispatch({ type: INIT_GOODS, data: goods })
        dispatch({ type: GOODS_SHIPMENT, data: shipment })
        dispatch({ type: SET_SEARCH_TEXT, data: q })
        dispatch({ type: CURRENT_GROUP_ID, data: 0 })
        dispatch({ type: PAGE_LANDING_OFF })
    }
}

export const initGoodsFromLiked = () => async (dispatch, getState) => {
    const { shipment } = getState().customer
    const likedGoodsLocal = getState().customer.likedGoods
    await dispatch(initBaseImageUrl())
    const cookies = parseCookies()
    const token = cookies.customer_token
    if (token) {
        const myLikedGoods = await getFavoritsCatalog(shipment)
        dispatch({ type: INIT_GOODS, data: myLikedGoods })
        if (myLikedGoods === 401) {
            const likedGoodsCatalog = await getGoodsByIds(likedGoodsLocal, shipment, false)
            dispatch({ type: INIT_GOODS, data: likedGoodsCatalog })
        }
        return
    }
    const likedGoodsCatalog = await getGoodsByIds(likedGoodsLocal, shipment, false)
    dispatch({ type: INIT_GOODS, data: likedGoodsCatalog })
}

export const initGoodsCustomerPurchasedItems = () => async (dispatch, getState) => {
    const { shipment } = getState().customer
    const goods = await getPurchasedItems(shipment)
    await dispatch(initBaseImageUrl())
    dispatch({ type: INIT_GOODS, data: goods })
}

export const initPromotionGoods = (promotionId, shipment) => async (dispatch) => {
    const goods = await getPromotionGoods(promotionId, shipment)
    await dispatch(initBaseImageUrl())
    if (goods) {
        dispatch({ type: INIT_GOODS, data: goods })
        dispatch({ type: GOODS_SHIPMENT, data: shipment })
        dispatch({ type: CURRENT_GROUP_ID, data: 0 })
    } else {
        dispatch({ type: INIT_GOODS, data: [] })
    }

    dispatch({ type: PAGE_LANDING_OFF })
}

export const getAnotherOutFormsAction = (goodId) => async (dispatch, getState) => {
    const { shipment } = getState().customer
    const goods = await getAnotherOutForms(goodId, shipment)
    await dispatch(initBaseImageUrl())
    dispatch({ type: INIT_GOODS, data: goods })
}

export const getAnaloguesAction = (goodId) => async (dispatch, getState) => {
    const { shipment } = getState().customer
    const goods = await getAnalogues(goodId, shipment)
    await dispatch(initBaseImageUrl())
    dispatch({ type: INIT_GOODS, data: goods })
}
