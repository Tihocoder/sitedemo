export const basketStateReducer = (state, action) => {
    switch (action.type) {
        case 'toggleFlag': {
            const stateFlag = state[action.name]
            return {
                ...state,
                [action.name]: !stateFlag
            }
        }
        case 'setFlag': {
            return {
                ...state,
                [action.name]: action.value
            }
        }
        case 'setErrorText': {
            return {
                ...state,
                errorText: action.value
            }
        }
        case 'setBasketNotAvailable': {
            return {
                ...state,
                basketNotAvailable: {
                    isShowed: true,
                    notAvailablePositions: action.value
                }
            }
        }
        default:
            return state
    }
}
export const initialBasketState = {
    isShowedWarningBorder: false,
    isShowedWarningSelectStock: false,
    isShowedWarningSelectDelivery: false,
    isShowedModalNotPositionStock: false,
    isShowedStockSelector: false,
    isPreorderLoading: false,
    isShowedAddressSelector: false,
    errorText: '',
    basketNotAvailable: {
        isShowed: false,
        notAvailablePosition: []
    }
}
