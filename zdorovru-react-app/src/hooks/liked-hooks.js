import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { initGoodsFromLiked } from '../actions/catalog'
import { initCustomerLikedGoods } from '../actions/customer'

export const useLiked = () => {
    const dispatch = useDispatch()
    const shipment = useSelector((state) => state.customer.shipment)
    const likedGoods = useSelector((state) => state.customer.likedGoods)
    const goods = useSelector((state) => state.catalog.goods)

    const initGoods = async () => {
        await dispatch(initGoodsFromLiked())
    }
    const mergeLikedGoods = async () => {
        await dispatch(initCustomerLikedGoods())
    }
    useEffect(() => {
        mergeLikedGoods()
    }, [])

    useEffect(() => {
        initGoods()
    }, [shipment.stockId, shipment.cityId, likedGoods.length])

    return { goods, shipment }
}
