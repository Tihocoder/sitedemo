// noinspection JSUnresolvedVariable

import React, { useEffect, useReducer, useRef } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import * as _ from 'lodash'
import { useRouter } from 'next/router'
import {
    initValidate,
    preorderChange,
    preorderClear,
    preorderStop,
    preorderStopLoading
} from '@actions/createOrder'
import {
    setBasketShipment,
    toggleHiddenBasketItem,
    updateActualBasketPostions,
    updateBasketFromPreorder
} from '@actions/customer'
import { analyzingBasketAndUpdateGoodsCount } from '@helpers/order'
import { BodyModalMobileContainer, BodyModalMobileP } from '@styles/info.styled'
import * as preorderConnector from '../services/preorderConnector'
import { useModalInfo } from './useModalInfo'
import { basketStateReducer, initialBasketState } from './basketState/basketState'

const initShowDeletePositionState = {
    isShowed: false,
    goodId: 0,
    goodName: ''
}

const defaultInit = {
    form: {
        name: true,
        email: true,
        phone: true
    },
    anotherUserContacts: {
        name: true,
        email: true,
        phone: true
    },
    deliveryData: {
        apt: true,
        paymentTypeId: true,
        shipTimeSlotId: true
    }
}

export const usePreorder = (props) => {
    const preorder = useSelector((state) => state.createOrder.preorder)
    const router = useRouter()
    const dispatch = useDispatch()
    const stateFormData = useSelector((state) => state.createOrder)
    const basket = useSelector((state) => state.customer.basket)
    const shipment = useSelector((state) => state.customer.shipment)
    const basketItemsAll = useSelector((state) => state.customer.basketItems)
    const basketItems = basketItemsAll.filter((item) => !item.isHidden)
    const signalRTimer = useRef(null)
    const customModal = useModalInfo()
    const [basketState, dispatchBasketState] = useReducer(basketStateReducer, initialBasketState, undefined)

    const isNotAvailableStock = basketItems.some(
        (item) => !item.isAvailable || !item.isAvailableOnStock || !item.stockPrice.normal
    )
    const isNotAvailableDelivery = basketItems.some(
        (item) =>
            !item.isAvailable || !item.deliveryStatus.isAvailableForDelivery || !item.deliveryPrice.normal
    )

    const setBasketNotAvailable = (notAvailablePositions) => {
        dispatchBasketState({ type: 'setBasketNotAvailable', value: notAvailablePositions })
    }
    const toggleFlag = (name) => dispatchBasketState({ type: 'toggleFlag', name })

    const setFlag = (name, value) => dispatchBasketState({ type: 'setFlag', name, value })

    const setErrorText = (value) => dispatchBasketState({ type: 'setErrorText', value })
    const redirectOnCreateOrder = async (uid) => {
        if (!basket.isDelivery && shipment.stockId && !isNotAvailableStock) {
            await router.push({
                pathname: '/order/create',
                query: { isDelivery: basket.isDelivery, preorderUid: uid }
            })
        }
        if (basket.isDelivery && !isNotAvailableDelivery && shipment.shipAddressId) {
            /// /RuTarget
            // eslint-disable-next-line no-underscore-dangle
            const _rutarget = window._rutarget || []
            _rutarget.push({ event: 'thankYou', conv_id: 'confirm' })

            await router.push({
                pathname: '/order/create',
                query: { isDelivery: basket.isDelivery, preorderUid: uid }
            })
        }
    }

    useEffect(() => {
        if (!basketItems || basketItems.length === 0) return
        dispatch(updateActualBasketPostions())

        // eslint-disable-next-line consistent-return
        return () => {
            dispatch(preorderStop())
            setFlag('isPreorderLoading', false)
            clearTimeout(signalRTimer.current)
        }
    }, [])

    // useEffect(() => {
    //     if (preorder.connection) {
    //         return () => {
    //             dispatch(preorderStop())
    //             setFlag('isPreorderLoading', false)
    //             clearTimeout(signalRTimer.current)
    //         }
    //     }
    // }, [preorder.connection])

    const toggleShipmentTab = async (isDelivery) => {
        await dispatch(preorderClear())
        await dispatch(setBasketShipment(isDelivery))
        await dispatch(updateActualBasketPostions(isDelivery))
    }

    useEffect(() => {
        toggleShipmentTab(router.query?.isDelivery === 'true' || false)
    }, [router.query?.isDelivery])

    const toggleIsDelivery = async (isDelivery) => {
        if (basket.isDelivery === isDelivery) return
        await dispatch(initValidate(defaultInit))
        await router.push({
            pathname: '/order/basket',
            query: { isDelivery },
            shallow: true
        })
    }
    const stopConnect = () => {
        const con = preorder.connection

        if (con) {
            try {
                con.stop().then(() => {})
                dispatch(preorderChange({ connection: null, message: null }))
            } catch (error) {
                console.error(error)
            }
        }
    }
    const cancelPreorder = async () => {
        await setFlag('isPreorderLoading', false)
        clearTimeout(signalRTimer.current)
        await dispatch(preorderStop())
    }

    const updateBasketFromPreorderMessage = async (actualPreorder) => {
        const positionData = analyzingBasketAndUpdateGoodsCount(
            actualPreorder.message.preorderItems,
            basketItemsAll,
            actualPreorder.message.isDelivery
        )
        stopConnect()
        await cancelPreorder()
        if (positionData.isChanged) {
            await dispatch(updateBasketFromPreorder(positionData.newBasketPositions))
            customModal.showModalInfo(
                <BodyModalMobileContainer>
                    <BodyModalMobileP>Некоторых товаров нет в необходимом количестве.</BodyModalMobileP>
                    <BodyModalMobileP>
                        Для оформления заказа Вам необходимо уменьшить требуемое количество в корзине или
                        заменить товары на аналогичные.
                    </BodyModalMobileP>
                </BodyModalMobileContainer>,
                'Понятно',
                customModal.closeModalInfo,
                'Отмена',
                'Товар закончился',
                false,
                true
            )
            return
        }

        await redirectOnCreateOrder(actualPreorder.message.preorderUid)
    }
    useEffect(() => {
        if (preorder.message) {
            updateBasketFromPreorderMessage(preorder)
        }
        return () => {
            setFlag('isPreorderLoading', false)
        }
    }, [preorder.message])

    const sumStock = _.sumBy(basketItems, (item) => item.stockPriceSum)
    const sumDelivery = _.sumBy(basketItems, (item) => item.deliveryPriceSum)
    const isNotFreeDelivery = sumDelivery <= basket.deliveryTreshhold.minSum
    const sumDeliveryFull = isNotFreeDelivery ? sumDelivery + basket.deliveryTreshhold.cost : sumDelivery
    const deliveryCost = isNotFreeDelivery ? basket.deliveryTreshhold.cost : 0

    const updateInvalid = async () => {
        await dispatch(initValidate(defaultInit))
    }
    const toggleSelectorStockHandler = async () => {
        await updateInvalid()
        setFlag('isShowedWarningBorder', false)
        setFlag('isShowedWarningSelectStock', false)
        setFlag('isShowedModalNotPositionStock', false)
        toggleFlag('isShowedStockSelector')
    }
    const toggleSelectorAddressHandler = async () => {
        await updateInvalid()
        setFlag('isShowedWarningBorder', false)
        setFlag('isShowedWarningSelectDelivery', false)
        toggleFlag('isShowedAddressSelector')
    }

    const createOrderHandler = async () => {
        setFlag('isShowedWarningBorder', true)
        setFlag('isPreorderLoading', true)
        setFlag('isShowedWarningBorder', true)
        if (!basket.isDelivery && !shipment.stockId) {
            toggleFlag('isShowedWarningSelectStock')
            setFlag('isPreorderLoading', false)
            return
        }
        if (basket.isDelivery && !shipment.shipAddressId) {
            toggleFlag('isShowedWarningSelectDelivery')
            setFlag('isPreorderLoading', false)
            return
        }

        if (!basket.isDelivery && isNotAvailableStock) {
            toggleFlag('isShowedModalNotPositionStock')
            setFlag('isPreorderLoading', false)
            return
        }
        if (basket.isDelivery && isNotAvailableDelivery) {
            customModal.showModalInfo(
                <BodyModalMobileContainer>
                    В корзине есть товары, недоступные для доставки. Для оформления заказа необходимо удалить
                    их из корзины
                </BodyModalMobileContainer>,
                'Вернуться к корзине',
                customModal.closeModalInfo,
                'Отмена'
            )
            setFlag('isPreorderLoading', false)
            return
        }

        if (basketItems.some((item) => item.isChanged)) {
            customModal.showModalInfo(
                <BodyModalMobileContainer>
                    <BodyModalMobileP>Некоторых товаров нет в необходимом количестве.</BodyModalMobileP>
                    <BodyModalMobileP>
                        Для оформления заказа Вам необходимо уменьшить требуемое количество в корзине или
                        заменить товары на аналогичные.
                    </BodyModalMobileP>
                </BodyModalMobileContainer>,
                'Понятно',
                customModal.closeModalInfo,
                'Отмена',
                'Товар закончился',
                false,
                true
            )
            setFlag('isPreorderLoading', false)
            return
        }
        setFlag('isPreorderLoading', true)

        try {
            const preorderData = await preorderConnector.createConnectionPreorder(
                basketItems.filter((item) => !item.isHidden),
                shipment,
                basket.isDelivery
            )
            if (!preorderData) {
                dispatch(preorderStopLoading())
                setErrorText('Произошла ошибка попробуйте оформить заказ еще раз')
                setFlag('isPreorderLoading', false)
                return
            }
            // noinspection JSUnresolvedVariable
            if (preorderData.response.isNeedAwaitBooking) {
                dispatch(
                    preorderChange({
                        uid: preorderData.response.preorderUid,
                        connection: preorderData.connection
                    })
                )

                preorderData.connection
                    .start()
                    .then(() => {
                        console.log('Connected!')
                        preorderData.connection.on('SendPreorder', (message) => {
                            console.log('signalR message', message)
                            dispatch(preorderChange({ message }))
                        })
                    })
                    .catch((e) => console.log('Connection failed: ', e))

                signalRTimer.current = setTimeout(async () => {
                    await dispatch(preorderStop())
                    setFlag('isPreorderLoading', false)
                    await redirectOnCreateOrder(preorderData.response.preorderUid)
                }, 15000)
                return
            }
            await dispatch(preorderStop())
            await redirectOnCreateOrder(preorderData.response.preorderUid)
        } catch (error) {
            dispatch(preorderStopLoading())
            setErrorText('Произошла ошибка попробуйте оформить еще раз')
            dispatch(preorderStop())
            setFlag('isPreorderLoading', false)
        }
    }
    const isSelectedShipment = !!(
        (basket.isDelivery && shipment.shipAddressId) ||
        (!basket.isDelivery && shipment.stockId)
    )


    return {
        isDelivery: basket.isDelivery,
        signalRTimer,
        toggleIsDelivery,
        createOrderHandler,
        shipment,
        customerBasket: basket,
        customModal,
        isSelectedShipment,
        toggleSelectorStockHandler,
        toggleSelectorAddressHandler,
        cancelPreorder,
        basket: {
            basketState,
            setBasketNotAvailable,
            toggleFlag,
            setFlag,
            setErrorText
        },
        sums: { sumDelivery, sumStock, sumDeliveryFull, deliveryCost, isNotFreeDelivery }
    }
}
