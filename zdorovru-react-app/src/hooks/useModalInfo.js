import React, { useState } from 'react'

const initialModalState = {
    isShowed: false,
    content: <React.Fragment />,
    buttonTitle: 'Продолжить',
    cancelTitle: 'Отмена',
    buttonHandler: () => {},
    title: '',
    isButtonFooter: true,
    isFooterOnlyOneButton: false
}

const initialModalSubmitState = {
    isShowed: false,
    content: <React.Fragment />,
    buttonTitle: 'Ок',
    cancelTitle: 'Отмена',
    buttonHandler: () => {},
    cancelHandler: () => {}
}
export const useModalInfo = () => {
    const [modalInfo, setModalInfo] = useState(initialModalState)
    const closeModalInfo = () => setModalInfo({ ...initialModalState })
    const showModalInfo = (
        content,
        buttonTitle,
        buttonHandler,
        cancelTitle,
        title = '',
        isButtonFooter = true,
        isFooterOnlyOneButton = false
    ) => {
        setModalInfo({
            ...modalInfo,
            isShowed: true,
            content,
            buttonTitle,
            buttonHandler,
            cancelTitle,
            title,
            isButtonFooter,
            isFooterOnlyOneButton
        })
    }

    return {
        modalInfo,
        closeModalInfo,
        showModalInfo
    }
}

export const useModalSubmit = () => {
    const [modalInfo, setModalInfo] = useState(initialModalSubmitState)
    const closeModalInfo = () => setModalInfo({ ...initialModalSubmitState })
    const showModalInfo = (content, buttonTitle = 'Ок', buttonHandler, cancelTitle, cancelHandler) => {
        setModalInfo({
            ...modalInfo,
            isShowed: true,
            content,
            buttonTitle,
            buttonHandler,
            cancelTitle,
            cancelHandler
        })
    }

    return {
        modalInfo,
        closeModalInfo,
        showModalInfo
    }
}
