import { useDispatch, useSelector } from 'react-redux'
import { useRouter } from 'next/router'
import React, { useEffect, useState } from 'react'
import {
    changeAnotherContactData,
    changeDeliveryData,
    changeFormData,
    initCreateOrderData,
    initValidate,
    setPayment,
    setShipTimeSlot
} from '@actions/createOrder'
import { setCookie } from 'nookies'
import { GenerateCreateOrderFromPreorder } from '@helpers/order'
import * as orderService from '@services/orderService'
import { createFromPreorder, frontErrorLog, getCustomerProfile } from '@api/siteApis'
import * as _ from 'lodash'
import * as consts from 'src/consts'
import TimeSlotsItem from '@components/Order/CreateOrder/TimeSlotsItem/TimeSlotsItem'

export const CreateOrderResponseStatus = {
    OK: 1,
    Error: 2,
    IsNotMobileConfirm: 3,
    IsNotValid: 4,
    CustomerNotFound: 5,
    CustomerNotActivated: 6,
    IsNotItemsValid: 7,
    PhoneChanged: 8,
    ErrorPayment: 9
}

// eslint-disable-next-line import/prefer-default-export
export const useCreateOrder = (props) => {
    // eslint-disable-next-line no-unused-vars
    const preorder = useSelector((state) => state.createOrder.preorder)
    const router = useRouter()
    const dispatch = useDispatch()
    const stocks = useSelector((state) => state.company.stocks)
    const stateFormData = useSelector((state) => state.createOrder)
    // eslint-disable-next-line no-unused-vars
    const basket = useSelector((state) => state.customer.basket)
    const shipment = useSelector((state) => state.customer.shipment)
    const basketItemsAll = useSelector((state) => state.customer.basketItems)
    const basketItems = basketItemsAll.filter((item) => !item.isHidden)
    const [isLogging, setIsLogging] = useState(false)
    const [isWaitingSubmit, setIsWaitingSubmit] = useState(false)
    const [isInvalidMessage, setIsInvalidMessage] = useState(false)
    const [paymentTypes, setPaymentTypes] = useState([])
    const [timeSlotItems, setTimeslotItems] = useState([])
    const changeDeliveryDataHandler = (name) => async (value) => {
        await dispatch(changeDeliveryData(name, value))
    }
    const toggleIsInvalidMessage = () => setIsInvalidMessage(!isInvalidMessage)

    const getShipmentData = () => {
        if (props.isDelivery) {
            return { shipAddressId: shipment.shipAddressId, shipAddressTitle: shipment.shipAddressTitle }
        }
        return stocks.find((x) => x.id === shipment.stockId)
    }

    useEffect(() => {
        if (props.timeSlots?.length) {
            const tsItems = props.timeSlots.map((timeSlot) => ({
                id: timeSlot.id,
                text: (
                    <TimeSlotsItem
                        title={timeSlot.title}
                        paymentTypes={props.isDelivery ? timeSlot.paymentTypes : null}
                    />
                )
            }))
            setTimeslotItems(tsItems)
        }
    }, [props.timeSlots?.length])

    // eslint-disable-next-line no-return-await
    const changeContactDataHandler = async (name, value) => await dispatch(changeFormData(name, value))
    const changeTimeSlotHandler = async (value) => {
        const timeSlot = props.timeSlots.find((x) => x.id === value)
        await dispatch(setShipTimeSlot(timeSlot.id, timeSlot.isNearestTime))
    }
    const changePaymentType = async (value) => {
        await dispatch(setPayment(value))
    }
    const initData = async () => {
        await dispatch(initCreateOrderData())
        const customerProfile = await getCustomerProfile()
        if (customerProfile.IsNotAuthorize) return
        if (customerProfile && customerProfile.profile) {
            setIsLogging(true)
            await changeContactDataHandler('name', customerProfile.profile.firstName)
            await changeContactDataHandler('phone', customerProfile.profile.phone)
            await changeContactDataHandler('email', customerProfile.profile.email)
        }
    }
    useEffect(() => {
        initData().then(() => {})
    }, [])

    useEffect(() => {
        try {
            const shipTimeSlotId = stateFormData?.shipTimeSlotInfo?.shipTimeSlotId
            if (props.isDelivery) {
                if (!shipTimeSlotId) {
                    setPaymentTypes([])
                } else {
                    const selectedTimeSlot = props.timeSlots?.find(
                        (timeSlot) => timeSlot.id === shipTimeSlotId
                    )
                    if (!selectedTimeSlot?.paymentTypes?.length) {
                        dispatch(setShipTimeSlot(0, false))
                        dispatch(setPayment(0))
                        frontErrorLog({
                            // eslint-disable-next-line react/prop-types
                            errorFront: `Нет способов оплат для shipTimeSlotId: ${shipTimeSlotId} preorderUid: ${props.preorderInfo?.preorderUid}`,
                            shipment
                        }).then(() => {
                            console.warn('error submit success!')
                        })
                    } else setPaymentTypes(selectedTimeSlot.paymentTypes)
                }
            } else {
                if (!props.timeSlots?.length) {
                    dispatch(setShipTimeSlot(0, false))
                    dispatch(setPayment(0))
                    return () => {}
                }
                const selectedTimeSlot = props.timeSlots?.find((timeSlot) => timeSlot.id === shipTimeSlotId)
                if (!selectedTimeSlot) {
                    dispatch(setShipTimeSlot(0, false))
                    dispatch(setPayment(0))
                }
            }
        } catch (e) {
            frontErrorLog({
                // eslint-disable-next-line react/prop-types
                errorFront: JSON.stringify(e),
                shipment
            }).then(() => {
                console.warn('error submit success!')
            })
        }
    }, [stateFormData?.shipTimeSlotInfo?.shipTimeSlotId, props.timeSlots?.length])

    const sumStock = _.sumBy(basketItems, (item) => item.stockPriceSum)
    const sumDelivery = _.sumBy(basketItems, (item) => item.deliveryPriceSum)
    const isNotFreeDelivery = sumDelivery <= basket.deliveryTreshhold.minSum
    const sumDeliveryFull = isNotFreeDelivery ? sumDelivery + basket.deliveryTreshhold.cost : sumDelivery
    const deliveryCost = isNotFreeDelivery ? basket.deliveryTreshhold.cost : 0

    const changeAnotherUserContactDataHandler = async (name, value) =>
        // eslint-disable-next-line no-return-await
        await dispatch(changeAnotherContactData(name, value))

    const submitHandler = async () => {
        setIsWaitingSubmit(true)
        try {
            // создаем структуру для апи
            const orderFromPreorder = await GenerateCreateOrderFromPreorder(
                stateFormData,
                props.isDelivery,
                basketItems,
                props.preorderInfo.preorderUid,
                basket.deliveryTreshhold
            )
            if (props.isDelivery) {
                // это нужно чтобы яндекс метрика записывала клики
                // RuTracker
                // eslint-disable-next-line no-underscore-dangle
                const _rutarget = window._rutarget || []
                _rutarget.push({ event: 'thankYou', conv_id: 'pay' })
                const validate = orderService.validateFormForDelivery(stateFormData, props.timeSlots)
                if (!validate.isValid) {
                    setIsInvalidMessage(true)
                    await dispatch(initValidate(validate.formValidate))
                    setIsWaitingSubmit(false)
                    return
                }
                // если мы не оформится заказ захэшируем введенные данные, чтобы заполнить их при повторном оформлении
                localStorage.setItem('create-order-data', JSON.stringify(stateFormData))

                // запрос в апишку
                const responseDelivery = await createFromPreorder(orderFromPreorder)
                if (responseDelivery.status === CreateOrderResponseStatus.Error) {
                    setIsWaitingSubmit(false)
                    await router.push(`/order/pickup/failed/${responseDelivery.orderData.orderID}`)
                    return
                }
                // чекаем валидацию со стороны сервера
                const responseValidate = orderService.checkInvalidResponse(stateFormData, responseDelivery)
                // TODO: кажется тут ошибка
                if (!validate.isValid) {
                    setIsInvalidMessage(true)
                    await dispatch(initValidate(responseValidate.formValidate))
                    setIsWaitingSubmit(false)
                    return
                }
                // это нужно чтобы на финишной странице отчислить корзину по orderid
                setCookie(null, 'orderid', JSON.stringify(responseDelivery.orderData.orderID), {
                    maxAge: 365 * 24 * 60 * 60,
                    path: '/'
                })

                // если онлайн опалата сразу редиректим в сбер
                if (responseDelivery.orderData.paymentType === consts.PaymentType.CardOnline) {
                    if (responseDelivery.token) {
                        setCookie({}, 'customer_token', responseDelivery.token, {
                            maxAge: 365 * 24 * 60 * 60,
                            path: '/'
                        })
                    }
                    await router.push(responseDelivery.sberbankOrderRegistrationData.paymentUrl)
                } else {
                    // setPreorder({ isLoading: false, connection: null, uid: '' })
                    await router.push(`/order/delivery/success/${responseDelivery.orderData.orderID}`)
                }
            } else {
                // валидация на стороне фронта
                const validate = orderService.validateFormForStock(stateFormData, props.timeSlots)
                if (!validate.isValid) {
                    setIsInvalidMessage(true)
                    await dispatch(initValidate(validate.formValidate))
                    setIsWaitingSubmit(false)
                    return
                }
                // если таймлотов нет, то не должно быть типа оплаты
                if (!props.timeSlots?.length) {
                    orderFromPreorder.paymentInfo.paymentType = 0
                    orderFromPreorder.shipTimeSlotInfo.shipTimeSlotId = 0
                }
                // отправка в апи
                const responseStock = await createFromPreorder(orderFromPreorder)
                // TODO: если ошибка надо что то делать)
                if (responseStock.status === CreateOrderResponseStatus.Error) {
                    setIsWaitingSubmit(false)
                    return
                }
                // валидация на стороне бэка
                const responseValidate = orderService.checkInvalidResponse(stateFormData, responseStock)
                if (!validate.isValid) {
                    setIsInvalidMessage(true)
                    await dispatch(initValidate(responseValidate.formValidate))
                    setIsWaitingSubmit(false)
                    return
                }
                // это нужно чтобы на финишной странице отчислить корзину по orderid
                setCookie(null, 'orderid', JSON.stringify(responseStock.orderData.orderID), {
                    maxAge: 365 * 24 * 60 * 60,
                    path: '/'
                })

                // если онлайн опалата сразу редиректим в сбер
                if (responseStock.orderData.paymentType === consts.PaymentType.CardOnline) {
                    if (responseStock.token) {
                        setCookie({}, 'customer_token', responseStock.token, {
                            maxAge: 365 * 24 * 60 * 60,
                            path: '/'
                        })
                    }
                    await router.push(responseStock.sberbankOrderRegistrationData.paymentUrl)
                } else {
                    await router.push(`/order/pickup/success/${responseStock.orderData.orderID}`)
                }
                if (responseStock.isError && responseStock.body) {
                    setIsWaitingSubmit(false)
                    return
                }
            }
            setIsWaitingSubmit(false)
        } catch (ex) {
            console.error('[order-hooks] ex:', ex)
            setIsWaitingSubmit(false)
        }
    }

    const getSelectedTimeSlotTitle = () => {
        if (!stateFormData.shipTimeSlotInfo?.shipTimeSlotId) return 'Выбрать'
        const currentTimeSlot = props.timeSlots.find(
            (timeSlot) => timeSlot.id === stateFormData.shipTimeSlotInfo?.shipTimeSlotId
        )
        return currentTimeSlot?.title || 'Выбрать'
    }

    const getPaymentTitle = () => {
        if (!stateFormData.paymentInfo?.paymentType) return 'Выбрать'
        if (!props.isDelivery) {
            const firstTimeSlots = props.timeSlots[0]
            const type = firstTimeSlots.paymentTypes?.find(
                (x) => x.id === stateFormData.paymentInfo.paymentType
            )
            return type?.title || 'Выбрать'
        }

        if (!stateFormData.shipTimeSlotInfo?.shipTimeSlotId) return 'Выбрать'
        const selectedTimeSlot = props.timeSlots.find(
            (timeSlot) => timeSlot.id === stateFormData.shipTimeSlotInfo.shipTimeSlotId
        )
        if (!selectedTimeSlot) return 'Выбрать'
        const type =
            selectedTimeSlot.paymentTypes?.find((x) => x.id === stateFormData.paymentInfo.paymentType) || null
        if (!type) return 'Выбрать'
        return type.title
    }

    return {
        submitHandler,
        changeContactDataHandler,
        changeAnotherUserContactDataHandler,
        changeDeliveryDataHandler,
        changeTimeSlotHandler,
        changePaymentType,
        isWaitingSubmit,
        isLogging,
        shipment,
        timeSlotItems,
        selectedShipment: getShipmentData(),
        getSelectedTimeSlotTitle,
        getPaymentTitle,
        isInvalidMessage,
        toggleIsInvalidMessage,
        paymentTypes,
        stateFormData,
        sums: { sumDelivery, sumStock, sumDeliveryFull, deliveryCost, isNotFreeDelivery }
    }
}
