import { useSelector } from 'react-redux'
import { useRouter } from 'next/router'
import { useGoogleReCaptcha } from 'react-google-recaptcha-v3'
import { useEffect, useState } from 'react'
import { EmailRegular } from 'src/consts'
import { sendFeedBack } from '@api/siteApis'

// eslint-disable-next-line import/prefer-default-export
export const useFeedBack = () => {
    const stocksAll = useSelector((state) => state.company.stocks)
    const shipment = useSelector((state) => state.customer.shipment)
    const router = useRouter()
    const { executeRecaptcha } = useGoogleReCaptcha()
    const [formDataFeedBack, setFormData] = useState({
        firstName: '',
        email: '',
        phone: '',
        subjectMessage: '',
        bodyMessage: '',
        selectedStockId: 0,
        stockTitle: ''
    })
    const [validateForm, setValidateForm] = useState({
        firstName: true,
        email: true,
        subjectMessage: true,
        bodyMessage: true
    })
    const [isSent, setIsSent] = useState(false)

    const toggleIsSent = () => setIsSent(!isSent)

    const selectedStockHandler = async (id) => {
        const stock = stocksAll.find((x) => x.id === id)
        setFormData({
            ...formDataFeedBack,
            selectedStockId: stock?.id || 0,
            stockTitle: stock?.title || 'Ничего не выбрано'
        })
    }
    useEffect(() => {
        selectedStockHandler(shipment.stockId)
    }, [])

    const changeFormDataValue = (name) => (value) => {
        const newState = {
            ...formDataFeedBack,
            [name]: value
        }
        setFormData(newState)
        setValidateForm({ ...validateForm, [name]: true })
    }
    const changeFormData = (name) => (e) => {
        changeFormDataValue(name)(e.target.value)
    }

    const isNotValidate =
        !validateForm.firstName ||
        !validateForm.email ||
        !validateForm.subjectMessage ||
        !validateForm.bodyMessage

    const submitHandler = async () => {
        const validateData = {
            firstName: true,
            email: true,
            subjectMessage: true,
            bodyMessage: true
        }
        let isValid = true
        if (!formDataFeedBack.firstName) {
            validateData.firstName = false
            isValid = false
        }
        if (!formDataFeedBack.email || !EmailRegular.test(formDataFeedBack.email)) {
            validateData.email = false
            isValid = false
        }
        if (!formDataFeedBack.subjectMessage) {
            validateData.subjectMessage = false
            isValid = false
        }
        if (!formDataFeedBack.bodyMessage) {
            validateData.bodyMessage = false
            isValid = false
        }

        setValidateForm({ ...validateData })

        if (isValid) {
            try {
                const token = await executeRecaptcha('feedback')
                // eslint-disable-next-line no-unused-vars
                const response = await sendFeedBack({ ...formDataFeedBack, token })
                await router.push('/contact/feedback-completed')
            } catch (e) {
                console.error('[FeedBack] e', e)
            }
        }
    }

    return {
        router,
        stocksAll,
        executeRecaptcha,
        formDataFeedBack,
        validateForm,
        toggleIsSent,
        isSent,
        changeFormData,
        isNotValidate,
        submitHandler,
        selectedStockHandler,
        changeFormDataValue
    }
}
