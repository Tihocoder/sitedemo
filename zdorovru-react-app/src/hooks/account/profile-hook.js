// eslint-disable-next-line no-unused-vars
import * as Theme from '@styles/theme-const'
import { useEffect, useReducer, useState } from 'react'
import { Emailvalidate } from '@helpers/validations'
import { postCustomerProfile } from '@api/siteApis'
import { useRouter } from 'next/router'
import moment from 'moment'

const profileInitialState = {
    firstName: '',
    lastName: '',
    middleName: '',
    gender: '',
    birthday: '',
    phone: '',
    email: '',
    isPromoConfirmed: null
}

const validateInitialState = {
    birthday: true,
    email: true
}

const profileReducer = (state, action) => {
    switch (action.type) {
        case 'init':
            if (action.value.birthday) {
                return {
                    ...action.value,
                    birthday: moment(action.value.birthday).format('DD.MM.YYYY')
                }
            }
            return {
                ...action.value
            }
        case 'change':
            return {
                ...state,
                [action.name]: action.value
            }
        default:
            return state
    }
}

const validateReducer = (state, action) => {
    switch (action.type) {
        case 'set':
            return {
                ...state,
                [action.name]: false
            }
        case 'reset-name': {
            return {
                ...state,
                [action.name]: true
            }
        }
        case 'reset':
            return validateInitialState
        default:
            return state
    }
}

// eslint-disable-next-line no-unused-vars
const borderInputColor = Theme.ColorConsts.accentGreen

// eslint-disable-next-line import/prefer-default-export
export const useProfile = (profile, genderTypes) => {
    const [state, profileDispatch] = useReducer(profileReducer, profileInitialState)
    const [validate, validateDispatch] = useReducer(validateReducer, validateInitialState)
    // eslint-disable-next-line no-shadow
    const router = useRouter()
    const changeValidate = (name) => validateDispatch({ type: 'set', name })
    const changeProfile = (name, value) => profileDispatch({ type: 'change', name, value })
    const initProfile = (value) => profileDispatch({ type: 'init', value })

    const [changePhoneState, setChangePhoneState] = useState({
        stage: 0
    })
    const closeChangePhone = () => {
        setChangePhoneState({ ...changePhoneState, stage: 0 })
    }
    const changePhoneEditStage = (stage) => {
        setChangePhoneState({ ...changePhoneState, stage })
    }
    useEffect(() => {
        // eslint-disable-next-line prefer-const
        // let newProfile = { ...profile }
        // if (newProfile && newProfile.birthday) {
        //     newProfile.birthday = moment(newProfile.birthday).format('YYYY-MM-DD')
        // }
        initProfile(profile)
    }, [profile, router.asPath])

    const changeHandler = (name) => (e) => {
        validateDispatch({ type: 'reset-name', name })
        if (name === 'isPromoConfirmed') {
            changeProfile(name, e)
            return
        }
        if (name === 'gender') {
            changeProfile(name, e)
            return
        }
        // if (name === 'birthday') {
        //     changeProfile(name, moment(e.target.value).format('YYYY-MM-DD'))
        //     return
        // }

        changeProfile(name, e.target.value)
    }

    const changeMobileHandler = (name, value) => {
        validateDispatch({ type: 'reset-name', name })
        // if (name === 'birthday') {
        //     changeProfile(name, moment(value).format('YYYY-MM-DD'))
        //     return
        // }
        changeProfile(name, value)
    }
    const submitHandler = async () => {
        if (!Emailvalidate(state.email)) {
            changeValidate('email')
            return
        }

        const profileEdit = { ...state }
        try {
            if (profileEdit.birthday) {
                const birthday = moment(profileEdit.birthday, 'DD.MM.YYYY')
                const isValidDate = birthday.isValid()
                if (!isValidDate) {
                    changeValidate('birthday')
                    return
                }
                profileEdit.birthday = moment(profileEdit.birthday, 'DD.MM.YYYY').format()
            } else {
                profileEdit.birthday = null
            }
        } catch (error) {
            changeValidate('birthday')
            return
        }

        const response = await postCustomerProfile(profileEdit)
        if (response && !response.isError) {
            await initProfile(response)
            router.reload()
        }
        if (response && response.isNotValidate) {
            if (response.invalidParameters && response.invalidParameters.length > 0) {
                response.invalidParameters.forEach((x) => {
                    changeValidate(x.name)
                })
            }
        }
    }

    const rollbackStateProfile = () => {
        initProfile(profile)
    }
    const submitHandlerNew = async () => {
        if (!Emailvalidate(state.email)) {
            changeValidate('email')
            return
        }
        const profileEdit = { ...state }
        try {
            if (profileEdit.birthday) {
                const birthday = moment(profileEdit.birthday, 'DD.MM.YYYY')
                const isValidDate = birthday.isValid()
                if (!isValidDate) {
                    changeValidate('birthday')
                    return
                }
                profileEdit.birthday = moment(profileEdit.birthday, 'DD.MM.YYYY').format()
            } else {
                profileEdit.birthday = null
            }
        } catch (error) {
            changeValidate('birthday')
            return
        }
        const response = await postCustomerProfile(profileEdit)
        if (response && !response.isError) {
            await initProfile(response)
        }
        if (response && response.isNotValidate) {
            if (response.invalidParameters && response.invalidParameters.length > 0) {
                response.invalidParameters.forEach((x) => {
                    changeValidate(x.name)
                })
            }
        }
        router.reload()
    }

    const colorsButtonText = {
        color: Theme.ColorConsts.pureRed
    }
    const optionsButtonText = {
        isBold: false
    }

    const genderTitle = genderTypes?.find((x) => x.typeId === state.gender)?.text || ''

    return {
        state,
        validate,
        changeHandler,
        submitHandler,
        submitHandlerNew,
        changeMobileHandler,
        colorsButtonText,
        optionsButtonText,
        rollbackStateProfile,
        genderTitle,
        changePhoneState,
        closeChangePhone,
        changePhoneEditStage
    }
}
