import { useSelector } from 'react-redux'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import { getOrder, orderDetails } from '@api/siteApis'

// eslint-disable-next-line import/prefer-default-export
export const useOrderDetails = (orderId) => {
    const basketItems = useSelector((state) => state.customer.basketItems)
    const shipment = useSelector((state) => state.customer.shipment)
    const router = useRouter()
    const [details, setDetails] = useState({
        orderNo: '',
        items: []
    })
    const [orderData, setOrderData] = useState(null)

    useEffect(() => {
        const initDetails = async () => {
            const response = await orderDetails(null, orderId, shipment)
            const order = await getOrder(orderId)
            setOrderData(order)
            if (response && response.items && response.items.length > 0) {
                setDetails(response)
            }
        }
        initDetails()
    }, [orderId])

    const closeOrder = () => {
        router.push('/account/history')
    }

    const redirectToHome = () => {
        router.push('/')
    }
    const getBasketItem = (goodId) => {
        const basketItem = basketItems.find((x) => x.webData.goodId === goodId)
        return basketItem ? basketItem.count : null
    }

    return {
        goods: details.items,
        closeOrder,
        redirectToHome,
        getBasketItem,
        shipment,
        orderData
    }
}
