import { useDispatch, useSelector } from 'react-redux'
import { initGoodsCustomerPurchasedItems } from '@actions/catalog'
import { useEffect } from 'react'

export const usePurchased = () => {
    const dispatch = useDispatch()
    const goods = useSelector((state) => state.catalog.goods)

    const initGoodsPurchased = async () => {
        await dispatch(initGoodsCustomerPurchasedItems())
    }

    useEffect(() => {
        initGoodsPurchased()
    }, [])

    return {
        goods
    }
}
