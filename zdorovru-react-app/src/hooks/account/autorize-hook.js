import { useDispatch } from 'react-redux'
import { useEffect, useState } from 'react'
import { parseCookies, setCookie } from 'nookies'
import { confirmCustomer, createCustomer, profileChangePhone, profileConfirmChangePhone } from '@api/siteApis'
import router from 'next/router'
import { mergeCustomerLikedGoods } from '@actions/customer'
import MobileDetect from 'mobile-detect'

const smsTimeout = 60

// eslint-disable-next-line import/prefer-default-export
export const useAuthorize = (closeHandler) => {
    const dispatch = useDispatch()
    const [isSendSms, setIsSendSms] = useState(false)
    const [phoneNumber, setPhoneNumber] = useState('')
    const [code, setCode] = useState('')
    const [errorText, setErrorText] = useState('')
    const [deviceId, setDeviceId] = useState('')
    const [isShowedSubmitSmsWindow, setIsShowedSubmitSmsWindow] = useState(false)
    const toggleShowedSubmitSmsWindow = () => setIsShowedSubmitSmsWindow(!isShowedSubmitSmsWindow)
    const [timer, setTimer] = useState({
        seconds: smsTimeout,
        isActive: false
    })

    useEffect(() => {
        let interval = null
        if (timer.isActive) {
            interval = setInterval(() => {
                if (timer.seconds < 1) {
                    setTimer({ ...timer, seconds: 0, isActive: false })
                } else {
                    setTimer({ ...timer, seconds: timer.seconds - 1 })
                }
            }, 1000)
        } else if (!timer.isActive && timer.seconds !== 0) {
            clearInterval(interval)
        }
        return () => clearInterval(interval)
    }, [timer.isActive, timer.seconds])

    const submitPhone = async () => {
        const cookies = parseCookies()
        const response = await confirmCustomer({
            phone: phoneNumber.replace(/\D+/g, ''),
            deviceId: cookies.deviceId || null
        })

        if (response.isTimeout) {
            setTimer({
                ...timer,
                seconds: smsTimeout,
                isActive: true
            })
            return
        }
        if (!response.isNeedCreate || response.token) {
            await router.push('/account/history')
        } else if (response.deviceId) {
            setCookie(null, 'deviceId', response.deviceId, {
                maxAge: 360 * 24 * 60 * 60,
                path: '/'
            })
            setDeviceId(response.deviceId)
            setIsSendSms(true)
            setTimer({
                ...timer,
                seconds: smsTimeout,
                isActive: true
            })
            setIsShowedSubmitSmsWindow(true)
        }
    }

    // eslint-disable-next-line consistent-return
    const registrationCustomer = async (submitCode, isReload) => {
        const response = await createCustomer({
            deviceId,
            phone: phoneNumber.replace(/\D+/g, ''),
            code: submitCode
        })
        if (response.statusType !== 1) {
            if (response.statusType === 3) {
                setErrorText('Неверный код. Попробуйте еще раз')
                setCode('')
            }
            if (response.statusType === 2) {
                setErrorText('Превышено число попыток')
            }
            if (response.statusType === 5) {
                setErrorText('Вы недавно отправляли запрос, пожалуйста попробуйте позже')
            }
            return false
        }
        if (response.token) {
            setCookie({}, 'customer_token', response.token, {
                maxAge: 365 * 24 * 60 * 60,
                path: '/'
            })
            await dispatch(mergeCustomerLikedGoods(response.token))
            setIsSendSms(false)
            setIsShowedSubmitSmsWindow(false)
            closeHandler()

            const ua = window.navigator.userAgent
            const md = new MobileDetect(ua)
            const isMobile = !!md.mobile()
            if (!isMobile) {
                if (isReload) {
                    router.reload()
                } else {
                    await router.push('/account/history')
                }
            }
            await router.reload()
        }
    }

    const changeCodeSubmit = async (e, isReload = false) => {
        setCode(e.target.value)
        setErrorText('')
        if (e.target.value && e.target.value.length === 4) {
            await registrationCustomer(e.target.value, isReload)
        }
    }
    // eslint-disable-next-line consistent-return,no-shadow
    const sendConde = async (code) => {
        setCode(code)
        setErrorText('')
        if (code && code.length === 4) {
            // eslint-disable-next-line no-return-await
            return await registrationCustomer(code)
        }
    }
    const selectInputPhonePage = () => {
        setIsSendSms(false)
    }

    return {
        isSendSms,
        phoneNumber,
        setPhoneNumber,
        errorText,
        timer,
        code,
        isShowedSubmitSmsWindow,
        toggleShowedSubmitSmsWindow,
        sendConde,
        submitPhone,
        changeCodeSubmit,
        selectInputPhonePage
    }
}

export const useChangePhone = () => {
    const [isSendSms, setIsSendSms] = useState(false)
    const [phoneNumber, setPhoneNumber] = useState('')
    const [code, setCode] = useState('')
    const [errorText, setErrorText] = useState('')
    const [isShowedSubmitSmsWindow, setIsShowedSubmitSmsWindow] = useState(false)
    const toggleShowedSubmitSmsWindow = () => setIsShowedSubmitSmsWindow(!isShowedSubmitSmsWindow)
    const [timer, setTimer] = useState({
        seconds: smsTimeout,
        isActive: false
    })

    useEffect(() => {
        let interval = null
        if (timer.isActive) {
            interval = setInterval(() => {
                if (timer.seconds < 1) {
                    setTimer({ ...timer, seconds: 0, isActive: false })
                } else {
                    setTimer({ ...timer, seconds: timer.seconds - 1 })
                }
            }, 1000)
        } else if (!timer.isActive && timer.seconds !== 0) {
            clearInterval(interval)
        }
        return () => clearInterval(interval)
    }, [timer.isActive, timer.seconds])

    const submitPhone = async () => {
        setErrorText('')
        const response = await profileConfirmChangePhone(phoneNumber.replace(/\D+/g, ''))
        if (response.status === 4) {
            setTimer({
                ...timer,
                seconds: smsTimeout,
                isActive: true
            })
        }
        if (response.status !== 5) {
            setErrorText(response.message)
            return
        }
        setIsSendSms(true)
        setIsShowedSubmitSmsWindow(true)
        setTimer({
            ...timer,
            seconds: smsTimeout,
            isActive: true
        })
    }

    const sendConde = async (submitCode) => {
        const response = await profileChangePhone(phoneNumber.replace(/\D+/g, ''), submitCode)
        if (response.status === 0) {
            await router.reload()
            return
        }
        if (response.status !== 0) {
            setErrorText(response.message)
            setCode('')
        }
        if (response.status !== 1) {
            if (response.status === 3) {
                setErrorText('Неверный код. Попробуйте еще раз')
                setCode('')
            }
            if (response.status === 2) {
                setErrorText('Превышено число попыток')
            }
            if (response.status === 5) {
                setErrorText('Вы недавно отправляли запрос, пожалуйста попробуйте позже')
            }
        }
    }

    const changeCodeSubmit = async (e) => {
        setCode(e.target.value)
        setErrorText('')
        if (e.target.value && e.target.value.length === 4) {
            await sendConde(e.target.value)
        }
    }
    const selectInputPhonePage = () => {
        setIsSendSms(false)
        setIsShowedSubmitSmsWindow(false)
    }

    return {
        isSendSms,
        phoneNumber,
        setPhoneNumber,
        errorText,
        timer,
        code,
        isShowedSubmitSmsWindow,
        toggleShowedSubmitSmsWindow,
        sendConde,
        submitPhone,
        changeCodeSubmit,
        selectInputPhonePage
    }
}
