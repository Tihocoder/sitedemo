//export const baseUrl = 'http://167.172.188.50/backend'
export const getApiUrl = () => {
    if (process.env.ENVS === 'local') {
        return 'http://localhost:5000/backend'
    }
    if (process.env.ENVS === 'prod') {
        return `https://zdorov.ru/backend`
    }
    if (process.env.ENVS === 'test') {
        return 'https://zdorov.ru/new/backend'
    } else {
        return 'http://167.172.188.50:5000/backend'
    }
}
export const baseUrl = getApiUrl()
//xport const baseUrl = 'http://192.168.99.100:5000/backend'
//export const baseUrl = 'https://zdorov.ru/backend'
