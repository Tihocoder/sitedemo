/* eslint-disable no-return-await */
import { dataProvider } from './dataProvider'

export const getCities = async () => await dataProvider(`${process.env.NEXT_PUBLIC_URL}/api/company/cities`)
export const getStocks = async () => await dataProvider(`${process.env.NEXT_PUBLIC_URL}/api/company/stocks`)
export const getMainGroups = async () =>
    await dataProvider(`${process.env.NEXT_PUBLIC_URL}/api/catalog/main-groups`)
export const getGroupGoods = async (groupId, shipmentState) =>
    await dataProvider(`${process.env.NEXT_PUBLIC_URL}/api/catalog/groups/${groupId}`, shipmentState, 'POST')
export const getGoodByWebGoodId = async (webGoodId, shipmentState) =>
    await dataProvider(`${process.env.NEXT_PUBLIC_URL}/api/catalog/goods/${webGoodId}`, shipmentState, 'POST')
export const getSearch = async (q, shipmentState) =>
    await dataProvider(
        `${process.env.NEXT_PUBLIC_URL}/api/catalog/goods/search/?q=${q}`,
        shipmentState,
        'POST'
    )
export const getGroupsByWebGoodId = async (webGoodId) =>
    await dataProvider(`${process.env.NEXT_PUBLIC_URL}/api/catalog/goods/${webGoodId}/groups`)
export const getAddresses = async (term) =>
    await dataProvider(`${process.env.NEXT_PUBLIC_URL}/api/order/addresses/?text=${term}`)

export const getAddressById = async (shipAddressId) =>
    await dataProvider(`${process.env.NEXT_PUBLIC_URL}/api/order/addresses/addressId/${shipAddressId}`)

export const checkBasket = async (data) =>
    await dataProvider(`${process.env.NEXT_PUBLIC_URL}/api/order/basket/check`, data, 'POST')

export const searchGroups = async (cityId, q) =>
    await dataProvider(`${process.env.NEXT_PUBLIC_URL}/api/catalog/groups/search/${cityId}?q=${q}`)
export const getCategoriesTreeByGoodIds = async (goodIds) =>
    await dataProvider(`${process.env.NEXT_PUBLIC_URL}/api/catalog/goods/groups-tree`, goodIds, 'POST')
export const getBanners = async () => await dataProvider(`${process.env.NEXT_PUBLIC_URL}/api/catalog/banners`)
export const getMobileBanners = async () =>
    await dataProvider(`${process.env.NEXT_PUBLIC_URL}/api/catalog/mobile-banners`)

export const getPromotions = async () =>
    await dataProvider(`${process.env.NEXT_PUBLIC_URL}/api/catalog/promotions`)
export const getPromotionsCount = async () =>
    await dataProvider(`${process.env.NEXT_PUBLIC_URL}/api/catalog/promotions/count`)
export const sendFeedBack = async (data) =>
    await dataProvider(`${process.env.NEXT_PUBLIC_URL}/api/company/feedback`, data, 'POST')
export const getPromotionById = async (promotionId) =>
    await dataProvider(`${process.env.NEXT_PUBLIC_URL}/api/catalog/promotions/${promotionId}`)

export const createFromPreorder = async (data) =>
    await dataProvider(`${process.env.NEXT_PUBLIC_URL}/api/order/createFromPreorder/`, data, 'POST')
export const getPromotionGoods = async (promotionId, shipment) =>
    await dataProvider(
        `${process.env.NEXT_PUBLIC_URL}/api/catalog/promotions/${promotionId}/goods`,
        shipment,
        'POST'
    )
export const basketUpdateStock = async (stockId, basketItems) =>
    await dataProvider(
        `${process.env.NEXT_PUBLIC_URL}/api/order/stock/${stockId}/basket-update`,
        basketItems,
        'POST'
    )
export const basketUpdateDelivery = async (cityId, basketItems) =>
    await dataProvider(
        `${process.env.NEXT_PUBLIC_URL}/api/order/delivery/${cityId}/basket-update`,
        basketItems,
        'POST'
    )
export const getStocksGoods = async (cityId, goodIds) =>
    await dataProvider(
        `${process.env.NEXT_PUBLIC_URL}/api/order/stock/get-stocks-goods/${cityId}`,
        goodIds,
        'POST'
    )

export const getTimeSlots = async (shipAddressId, maxAptEtaDateTime) =>
    await dataProvider(
        `${process.env.NEXT_PUBLIC_URL}/api/order/delivery/timeSlots/shipAddressId/${shipAddressId}${
            maxAptEtaDateTime ? `?maxAptEtaDateTime=${maxAptEtaDateTime}` : ''
        }`
    )

export const getOrderFullInfo = async (orderId, ctx) =>
    await dataProvider(`${process.env.NEXT_PUBLIC_URL}/api/order/${orderId}/full-info`, null, 'GET', ctx)

export const getAnotherOutForms = async (goodId, shipment) =>
    await dataProvider(
        `${process.env.NEXT_PUBLIC_URL}/api/catalog/goods/${goodId}/AnotherOutForms`,
        shipment,
        'POST'
    )

export const getAnalogues = async (goodId, shipment) =>
    await dataProvider(
        `${process.env.NEXT_PUBLIC_URL}/api/catalog/goods/${goodId}/analogues`,
        shipment,
        'POST'
    )

export const getCompanyStock = async (companyId) =>
    await dataProvider(`${process.env.NEXT_PUBLIC_URL}/api/company/${companyId}/stocks`)

export const getInfoList = async (goodId) =>
    await dataProvider(`${process.env.NEXT_PUBLIC_URL}/api/catalog/goods/${goodId}/infoList`)

export const getUpdatedGoods = async (basketItems, shipment) =>
    await dataProvider(
        `${process.env.NEXT_PUBLIC_URL}/api/order/basket/update`,
        {
            basketItems,
            shipmentState: shipment
        },
        'POST'
    )

export const getGoodStocks = async (goodId, cityId) =>
    await dataProvider(`${process.env.NEXT_PUBLIC_URL}/api/catalog/goods/${goodId}/city/${cityId}/stocks`)

export const getGoodsByIds = async (goodIds, shipment, isFiltredAvailable = true) =>
    await dataProvider(
        `${process.env.NEXT_PUBLIC_URL}/api/catalog/goods-by-ids`,
        { goodIds, shipment, isFiltredAvailable },
        'POST'
    )

export const getFeatures = async () =>
    await dataProvider(`${process.env.NEXT_PUBLIC_URL}/api/catalog/goods/features`)

export const getBonusCartsCampaign = async (shipment) =>
    await dataProvider(`${process.env.NEXT_PUBLIC_URL}/api/catalog/bonusCardsCampaigns`, shipment, 'POST')

export const getGiftCardsCampaigns = async (shipment) =>
    await dataProvider(`${process.env.NEXT_PUBLIC_URL}/api/catalog/giftCardsCampaigns`, shipment, 'POST')

export const getRule = async (ruleId) =>
    await dataProvider(`${process.env.NEXT_PUBLIC_URL}/api/company/rules/${ruleId}`)

export const getSearchUnderlineTags = async (shipment) =>
    await dataProvider(`${process.env.NEXT_PUBLIC_URL}/api/catalog/searchUnderlineTags`, shipment, 'POST')

export const getTreshold = async () =>
    await dataProvider(`${process.env.NEXT_PUBLIC_URL}/api/order/threshold`)

export const getOrder = async (orderId) =>
    await dataProvider(`${process.env.NEXT_PUBLIC_URL}/api/order/${orderId}`)

export const getOrderItems = async (orderId, shipment, ctx) =>
    await dataProvider(`${process.env.NEXT_PUBLIC_URL}/api/order/${orderId}/goods`, shipment, 'POST', ctx)

export const getCustomerOrders = async (ctx) =>
    await dataProvider(`${process.env.NEXT_PUBLIC_URL}/api/customer/orders`, null, 'GET', ctx)

export const checkAuthorization = async () =>
    await dataProvider(`${process.env.NEXT_PUBLIC_URL}/api/customer/check-authorization`)

export const confirmCustomer = async (confirmData) =>
    await dataProvider(`${process.env.NEXT_PUBLIC_URL}/api/customer/confirm`, confirmData, 'POST')
export const createCustomer = async (createCustomerData) =>
    await dataProvider(
        `${process.env.NEXT_PUBLIC_URL}/api/customer/create-customer`,
        createCustomerData,
        'POST'
    )

export const getCustomerProfile = async (ctx) =>
    await dataProvider(`${process.env.NEXT_PUBLIC_URL}/api/customer/profile`, null, 'GET', ctx)

export const postCustomerProfile = async (profile) =>
    await dataProvider(`${process.env.NEXT_PUBLIC_URL}/api/customer/profile/edit`, profile, 'POST')

export const getPurchasedItems = async (shipment) =>
    await dataProvider(`${process.env.NEXT_PUBLIC_URL}/api/customer/purchased-items`, shipment, 'POST')

export const orderDetails = async (ctx, orderId, shipment) =>
    await dataProvider(
        `${process.env.NEXT_PUBLIC_URL}/api/customer/order/${orderId}/details`,
        shipment,
        'POST',
        ctx
    )

// preorder
export const createOrder = async (order) =>
    await dataProvider(`${process.env.NEXT_PUBLIC_URL}/api/order/create`, order, 'POST')

export const createPreorder = async (order) =>
    await dataProvider(`${process.env.NEXT_PUBLIC_URL}/api/order/preorders/create`, order, 'POST')

export const getPreorderFullInfo = async (preorderUid) =>
    await dataProvider(
        `${process.env.NEXT_PUBLIC_URL}/api/order/preorders/GetPreorderFullInfo?preorderUid=${preorderUid}`,
        null,
        'GET'
    )

export const preorderModify = async (order) =>
    await dataProvider(`${process.env.NEXT_PUBLIC_URL}/api/order/preorders/modify`, order, 'POST')

export const preorderSetExpired = async (preorderUid) =>
    await dataProvider(
        `${process.env.NEXT_PUBLIC_URL}/api/order/preorders/setExpired`,
        {
            preorderUid
        },
        'POST'
    )

export const getPreorderTimeSlots = async (preorderUid) =>
    await dataProvider(
        `${process.env.NEXT_PUBLIC_URL}/api/order/preorders/GetShipTimeSlots?preorderUid=${preorderUid}`
    )

// favorite
export const mergeFavorits = async (goodIds) =>
    await dataProvider(`${process.env.NEXT_PUBLIC_URL}/api/customer/favorits/merge`, goodIds, 'POST')

export const getFavorits = async () =>
    await dataProvider(`${process.env.NEXT_PUBLIC_URL}/api/customer/favorits`, null, 'GET')

export const getFavoritsCatalog = async (shipment) =>
    await dataProvider(`${process.env.NEXT_PUBLIC_URL}/api/customer/favorits/catalog`, shipment, 'POST')

export const saveFavorite = async (goodId) =>
    await dataProvider(`${process.env.NEXT_PUBLIC_URL}/api/customer/favorite`, goodId, 'POST')

export const deleteFavorite = async (goodId) =>
    await dataProvider(`${process.env.NEXT_PUBLIC_URL}/api/customer/favorite/delete`, goodId, 'POST')
// ERROR LOGS
export const frontErrorLog = async (data) =>
    await dataProvider(`${process.env.NEXT_PUBLIC_URL}/api/error/basket-log`, data, 'POST')

export const errorLog = async (data) =>
    await dataProvider(`${process.env.NEXT_PUBLIC_URL}/api/error/error-log`, data, 'POST')

export const getBaseImageUrl = async () =>
    await dataProvider(`${process.env.NEXT_PUBLIC_URL}/api/catalog/goods/base-image-url`)

// convert
export const convertGoodIds = async (goodIds) =>
    await dataProvider(`${process.env.NEXT_PUBLIC_URL}/api/catalog/goods/convert-old-goods`, goodIds, 'POST')

export const profileConfirmChangePhone = async (phone) =>
    await dataProvider(
        `${process.env.NEXT_PUBLIC_URL}/api/Customer/profile/confirm-change-phone`,
        {
            phone
        },
        'POST'
    )

export const profileChangePhone = async (phone, code) =>
    await dataProvider(
        `${process.env.NEXT_PUBLIC_URL}/api/customer/profile/change-phone`,
        {
            phone,
            code
        },
        'POST'
    )
