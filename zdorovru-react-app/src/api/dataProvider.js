import nookies, { destroyCookie, parseCookies, setCookie } from 'nookies'
import { errorLog } from "@api/siteApis";

export const responseStatuses = {
    OK: 'Ok'
}

const setTokenAndUid = (response, ctx) => {
    if (response.token) {
        setCookie(ctx || {}, 'customer_token', response.token, {
            maxAge: 365 * 24 * 60 * 60,
            path: '/'
        })
    }
    if (response.customerUid) {
        setCookie(ctx, 'customer_uid', response.customerUid, {
            path: '/'
        })
    }
}

const getTokenFromCookie = (ctx, newTokent) => {
    if (newTokent) return `Bearer ${token}`
    const cookies = parseCookies(ctx)
    const token = cookies.customer_token
    return token ? `Bearer ${token}` : ''
}

// eslint-disable-next-line consistent-return
const error = async (response) => {
    console.log('FAIL RESPONSE error:', response)
    try {
        const contentType = response.headers.get('content-type')
        if (contentType && contentType.includes('application/json')) {
            const responseDataJson = await response.json()
            if (responseDataJson.isError) {
                console.error('Error RESPONSE JSON', JSON.stringify(responseDataJson))
                return responseDataJson
            }
        } else {
            const responseData = await response.text()
            console.error('Error RESPONSE Text', responseData)
            await errorLog({
                description: 'Error response text',
                error: responseData
            })
        }
    } catch (ex) {
        console.error(ex)
    }
}

const checkException = (url, response) => {
    if (response && response.isError) {
        console.error(`Ошибка в запросе ${url}: ${response}`)
    }
}

export const dataProvider = async (url, data = null, type = 'GET', ctx = null, token = null) => {
    if (type == 'GET') {
        const response = await fetch(url, {
            method: 'GET',
            cache: 'no-cache',
            headers: {
                Authorization: getTokenFromCookie(ctx, token)
            }
        })
        if (response.status === 401) {
            return 401
        }
        if (response.ok) {
            try {
                const responseDataJson = await response.json()
                setTokenAndUid(responseDataJson, ctx)
                checkException(url, responseDataJson)
                return responseDataJson
            } catch (ex) {
                console.error('error запрос', ex)
                return null
            }
        } else {
            return await error(response)
        }
    } else if (type == 'GET' && data) {
        const response = await fetch(url, {
            method: 'GET',
            body: JSON.stringify(data),
            // mode: 'cors',
            cache: 'no-cache',
            headers: {
                Accept: 'application/json',
                Authorization: getTokenFromCookie(ctx, token),
                'Content-Type': 'application/json'
            }
        })
        if (response.status === 401) {
            return 401
        }
        if (response.ok) {
            try {
                const responseDataJson = await response.json()
                setTokenAndUid(responseDataJson, ctx)
                checkException(url, responseDataJson)
                return responseDataJson
            } catch (e) {
                await errorLog({
                    description: 'Error response',
                    error: JSON.stringify(e)
                })
                return null
            }
        } else {
            // eslint-disable-next-line no-return-await
            return await error(response)
        }
    } else if (type == 'POST' && data) {
        const response = await fetch(url, {
            method: 'POST',
            body: JSON.stringify(data),
            cache: 'no-cache',
            // mode: 'cors',
            headers: {
                Accept: 'application/json',
                Authorization: getTokenFromCookie(ctx, token),
                'Content-Type': 'application/json'
            }
        })
        if (response.status === 401) {
            return 401
        }
        if (response.ok) {
            try {
                const responseDataJson = await response.json()
                setTokenAndUid(responseDataJson, ctx)
                checkException(url, responseDataJson)
                return responseDataJson
            } catch (e) {
                console.error()
            }
        } else {
            return await error(response)
        }
    }
    if (type == 'POST' && !data) {
        const response = await fetch(url, {
            method: 'POST',
            cache: 'no-cache',
            headers: {
                Authorization: getTokenFromCookie(ctx, token)
            }
        })
        if (response.status === 401) {
            return 401
        }
        if (response.ok) {
            try {
                const responseDataJson = await response.json()
                checkException(url, responseDataJson)
                setTokenAndUid(responseDataJson, ctx)
                return responseDataJson
            } catch {
                return null
            }
        } else {
            return await error(response)
        }
    }
    return null
}
