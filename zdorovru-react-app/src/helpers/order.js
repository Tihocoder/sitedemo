import * as _ from 'lodash'

export const getSum = (price) => price.normal

export const GenerateCreateOrderFromPreorder = async (
    stateFormData,
    isDelivery,
    basketItems,
    preorderUid,
    deliveryThreshold
) => {
    const customer = {
        name: stateFormData.userContacts.name,
        email: stateFormData.userContacts.email,
        phone: stateFormData.userContacts.phone.replace(/\D+/g, '')
    }

    let result = {
        customer,
        isDelivery,
        isNeedOperatorCall: stateFormData.userContacts.isNeedOperatorCall,
        preorderUid
    }
    if (stateFormData.shipTimeSlotInfo) {
        result.shipTimeSlotInfo = stateFormData.shipTimeSlotInfo
    }
    if (stateFormData.paymentInfo) {
        result.paymentInfo = stateFormData.paymentInfo
    }
    if (stateFormData.anotherUserContacts.isEnabled) {
        result.anotherCustomer = {
            name: stateFormData.anotherUserContacts.name,
            email: stateFormData.anotherUserContacts.email,
            phone: stateFormData.anotherUserContacts.phone.replace(/\D+/g, '')
        }
        result.isAnotherCustomer = true
    }

    // TODO: пока что костыль
    if (isDelivery) {
        let deliveryInfo = stateFormData.deliveryData
        const sumPositionDelivery = _.sumBy(basketItems, (item) => item.deliveryPriceSum)
        deliveryInfo = {
            ...stateFormData.deliveryData,
            deliveryPrice: sumPositionDelivery < deliveryThreshold.minSum ? deliveryThreshold.cost : 0
        }
        result = {
            ...result,
            deliveryInfo
        }
    }
    return result
}
export const GenerateCreatePreorderModel = (shipment, isDelivery, basketItems) => ({
    shipment,
    isDelivery,
    preorderItems: basketItems.map((basketItem) => ({
        goodId: basketItem.webData.goodId,
        goodWebDataId: basketItem.webData.webId,
        price: isDelivery ? getSum(basketItem.deliveryPrice) : getSum(basketItem.stockPrice),
        count: basketItem.count,
        isDemand: basketItem.isDemand,
        aptEtaDateTime: basketItem.aptEtaDateTime
    }))
})

const checkChangedPosition = (count, bookedCount, price, demand) => {
    if (price.isDemand) return false
    if (bookedCount < count) return true
    // if (price.isDemand !== demand.isDemand) return true
    // console.log(
    //     'checkChangedPosition',
    //     new Date(price.aptEtaDateTime).toString(),
    //     new Date(demand.aptEtaDateTime).toString(),
    //     new Date(price.aptEtaDateTime).toString() !== new Date(demand.aptEtaDateTime).toString()
    // )
    // if (price.isDemand && demand.isDemand) {
    //     if (new Date(price.aptEtaDateTime).toString() !== new Date(demand.aptEtaDateTime).toString()) {
    //         return true
    //     }
    // }
    return false
}

export const analyzingBasketAndUpdateGoodsCount = (preorderItems, basketPositions, isDelivery) => {
    if (!preorderItems || preorderItems.length === 0) {
        return { isChanged: basketPositions.some((x) => x.isChanged), basketPositions }
    }
    const newBasketPositions = basketPositions.map((position) => {
        const preorderItem = preorderItems.find((pi) => pi.goodId === position.webData.goodId)
        // если позиции больше нет
        if (!preorderItem) {
            let resultIsNotAvailable = {
                ...position
            }
            if (isDelivery) {
                resultIsNotAvailable = {
                    ...resultIsNotAvailable,
                    deliveryStatus: {
                        ...resultIsNotAvailable.deliveryStatus,
                        isAvailableForDelivery: false
                    }
                }
            } else {
                resultIsNotAvailable = {
                    ...resultIsNotAvailable,
                    IsAvailableOnStock: false
                }
            }
            return resultIsNotAvailable
        }
        // если есть позиция
        let resultAvailable = {
            ...position,
            bookedCount: preorderItem.bookedCount
        }
        if (isDelivery) {
            resultAvailable = {
                ...resultAvailable,
                isChanged: checkChangedPosition(
                    position.count,
                    preorderItem.bookedCount,
                    resultAvailable.deliveryPrice,
                    {
                        isDemand: preorderItem.isDemand,
                        aptEtaDateTime: preorderItem.aptEtaDateTime
                    }
                ),
                deliveryPrice: {
                    ...resultAvailable.deliveryPrice,
                    isDemand: preorderItem.isDemand,
                    aptEtaDateTime: preorderItem.aptEtaDateTime
                }
            }
        } else {
            resultAvailable = {
                ...resultAvailable,
                isChanged: checkChangedPosition(
                    position.count,
                    preorderItem.bookedCount,
                    resultAvailable.stockPrice,
                    {
                        isDemand: preorderItem.isDemand,
                        aptEtaDateTime: preorderItem.aptEtaDateTime
                    }
                ),
                stockPrice: {
                    ...resultAvailable.stockPrice,
                    isDemand: preorderItem.isDemand,
                    aptEtaDateTime: preorderItem.aptEtaDateTime
                }
            }
        }
        return resultAvailable
    })

    return { isChanged: newBasketPositions.some((x) => x.isChanged), newBasketPositions }
}

export const sumPrice = (priceNormal, count, isAvailable, isAvailableInside) => {
    if (!isAvailable || !isAvailableInside) return 0
    return priceNormal * count
}

export const updateBasketPostitions = (currentBasketItems, newBasketItems) => {
    const positions = currentBasketItems.map((x) => {
        const newItem = newBasketItems.find((i) => i.webData.goodId === x.webData.goodId)
        if (newItem) {
            return {
                ...x,
                deliveryStatus: newItem.deliveryStatus,
                isAvailable: newItem.isAvailable,
                isChanged: false,
                bookedCount: null,
                isAvailableOnStock: newItem.isAvailableOnStock,
                isDemand: newItem.isDemand,
                aptEtaDateTime: newItem.aptEtaDateTime,
                availableStocksCount: newItem.availableStocksCount,
                stockPrice: {
                    ...x.stockPrice,
                    aptEtaDateTime: newItem.stockPrice.aptEtaDateTime,
                    isDemand: newItem.stockPrice.isDemand
                },
                deliveryPrice: {
                    ...x.deliveryPrice,
                    aptEtaDateTime: newItem.deliveryPrice.aptEtaDateTime,
                    isDemand: newItem.deliveryPrice.isDemand
                },
                deliveryPriceSum: sumPrice(
                    newItem.deliveryPrice.normal,
                    x.count,
                    newItem.isAvailable,
                    newItem.deliveryStatus.isAvailableForDelivery
                ),
                stockPriceSum: sumPrice(
                    newItem.stockPrice.normal,
                    x.count,
                    newItem.isAvailable,
                    newItem.isAvailableOnStock
                ),
                minPriceSum: sumPrice(newItem.minPrice.normal, x.count, true, true)
            }
        }
        return { ...x, isAvailable: false }
    })
    return positions
}
export const updateBasketPostitionsAndChangePrice = (currentBasketItems, newBasketItems) => {
    const positions = currentBasketItems.map((x) => {
        const newItem = newBasketItems.find((i) => i.webData.goodId === x.webData.goodId)
        if (newItem) {
            return {
                ...x,
                deliveryStatus: newItem.deliveryStatus,
                isAvailable: newItem.isAvailable,
                isAvailableOnStock: newItem.isAvailableOnStock,
                minPrice: newItem.minPrice,
                deliveryPrice: newItem.deliveryPrice,
                stockPrice: newItem.stockPrice,
                isDemand: newItem.isDemand,
                aptEtaDateTime: newItem.aptEtaDateTime,
                deliveryPriceSum: sumPrice(
                    newItem.deliveryPrice.normal,
                    x.count,
                    newItem.isAvailable,
                    newItem.deliveryStatus.isAvailableForDelivery
                ),
                stockPriceSum: sumPrice(
                    newItem.stockPrice.normal,
                    x.count,
                    newItem.isAvailable,
                    newItem.isAvailableOnStock
                ),
                minPriceSum: sumPrice(newItem.minPrice.normal, x.count, true, true)
            }
        }
        return { ...x, isAvailable: false }
    })
    return positions
}
