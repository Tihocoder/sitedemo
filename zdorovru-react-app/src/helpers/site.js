import { useRouter } from 'next/router'
import { parseCookies, setCookie } from 'nookies'
import queryString from 'query-string'
import moment from 'moment'
import { convertGoodIds } from '../api/siteApis'

export const FilterTypes = {
    makers: 'makers',
    outForms: 'outForms'
}
moment.locale('ru')

export const linkWithShipQuery = (url, router, shipment, newParams, oldParams, isShallow = false) => {
    const params = new URLSearchParams()

    if (newParams && newParams.length > 0) {
        newParams.forEach((x) => params.set(x.name, x.value))
    }
    if (oldParams && oldParams.length > 0) {
        oldParams.forEach((x) => params.delete(x))
    }
    const urlResult =
        (newParams && newParams.length > 0) || (oldParams && oldParams.length > 0)
            ? `${url}?${params.toString()}`
            : url
    router.push(urlResult, undefined, { shallow: isShallow })
}

export const getUrlWithParams = (url, newParams) => {
    const params = new URLSearchParams()
    if (newParams && newParams.length > 0) {
        newParams.forEach((x) => params.set(x.name, x.value))
    }
    return newParams && newParams.length > 0 ? `${url}?${params.toString()}` : url
}

export const convertDate = (dateString) => moment(dateString).format('DD.MM.YYYY')

export const convertDateTime = (dateTimeSting) => moment(dateTimeSting).format('DD.MM.YYYY HH:mm')

export const declOfNum = (n, titles) =>
    titles[
        // eslint-disable-next-line no-nested-ternary
        n % 10 === 1 && n % 100 !== 11
            ? 0
            : n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 10 || n % 100 >= 20)
            ? 1
            : 2
    ]

export const generateLink = (url, newParams) => {
    const params = new URLSearchParams()
    if (newParams && newParams.length > 0) {
        newParams.forEach((x) => params.set(x.name, x.value))
    }
    return newParams.length > 0 ? `${url}?${params.toString()}` : url
}

export const getFilterArrayOrNull = (filters, name, types) => {
    if (!types.some((x) => x === name)) return null
    const array = filters[name]
    if (array && array.length > 0) {
        return array
    }
    return null
}

export const getColorPrefix = (title) => {
    if (title.includes('м. ')) return 'м.'
    if (title.includes('г. ')) return 'г.'
    return ''
}

export const filterGoods = (goods, filters, types) => {
    let resultGoods = [...goods]

    const makers = getFilterArrayOrNull(filters, FilterTypes.makers, types)
    if (makers) {
        resultGoods = resultGoods.filter((x) => makers.some((m) => m === x.webData.makerTitle))
    }
    const outForms = getFilterArrayOrNull(filters, FilterTypes.outForms, types)
    if (outForms) {
        resultGoods = resultGoods.filter((x) => outForms.some((m) => m === x.webData.outFormTitle))
    }

    return resultGoods
}

export const changeLinkExceptQueryParams = (router, newParams, oldParams, isShallow = false) => {
    const path = router.asPath.split('?')[0] || router.asPath
    const urlBase = new URL(window.location)
    const params = new URLSearchParams(urlBase.searchParams.toString())

    if (newParams && newParams.length > 0) {
        newParams.forEach((x) => params.set(x.name, x.value))
    }
    if (oldParams && oldParams.length > 0) {
        oldParams.forEach((x) => params.delete(x))
    }
    router.push(`${path}?${params.toString()}`, undefined, { shallow: isShallow })
}

export const generateDate = (date, isMonthString, isTime) => {
    const dateInstance = new Date(date)
    if (isTime) {
        const options = { hour: 'numeric', minute: 'numeric', timeZone: 'UTC' }
        return dateInstance.toLocaleTimeString('ru-RU', options)
    }
    const options = isMonthString
        ? { year: 'numeric', month: 'long', day: 'numeric', timeZone: 'UTC' }
        : { year: 'numeric', month: 'numeric', day: 'numeric', timeZone: 'UTC' }
    return dateInstance.toLocaleDateString('ru-RU', options)
}
export const generateTime = (date) => {
    const dateInstance = new Date(date)
    return dateInstance.toTimeString().match(/\d{2}:\d{2}/)[0]
}

export const updateCustomerStorageInLocal = (getState) => {
    const customerStorage = {
        customer: getState().customer,
        company: getState().company,
        updateDate: Date.now()
    }
    const jsonString = JSON.stringify(customerStorage)
    const oldState = localStorage.getItem('zdorovru-customer')
    if (oldState !== jsonString) {
        localStorage.setItem('zdorovru-customer', jsonString)
    }
    setCookie(null, 'storage-shipment', JSON.stringify(customerStorage.customer.shipment), {
        maxAge: 365 * 24 * 60 * 60,
        path: '/'
    })
}
export const usePage = () => {
    const isServer = typeof window === 'undefined'
    if (isServer)
        return {
            number: 1,
            changePage: () => {}
        }
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const router = useRouter()
    const { page } = router.query
    return {
        number: parseInt(page || 1, 10),
        changePage: (number) => {
            router.push(
                {
                    pathname: router.pathname,
                    query: { ...router.query, page: number },
                    shallow: true
                },
                undefined,
                { shallow: true }
            )
        }
    }
}
export const getNewShipment = (getState, cityId, stockId, shipAddress) => {
    const oldShipment = getState().customer.shipment
    const { company } = getState()
    const newShipment = {}
    if (!cityId) {
        if (!oldShipment.city || !oldShipment.city.id) {
            const city = company.cities.find((c) => c.id === 1)
            newShipment.city = city
        }
    } else if (cityId) {
        const city = company.cities.find((c) => c.id === cityId)
        newShipment.city = city
    }

    if (stockId) {
        const stock = company.stocks.find((s) => s.id === stockId)
        newShipment.stock = stock
    }
    if (shipAddress && shipAddress.id) {
        newShipment.shipAddress = shipAddress
    }
    if ((company.cities.length === 0 || company.stocks.length === 0) && !process.browser) {
        if (cityId) {
            newShipment.city = { id: cityId }
        }

        if (stockId) {
            newShipment.stock = { id: cityId }
        }
    }
    return { ...oldShipment, newShipment }
}
export const getShipmentRequest = (shipment, isShipAddress = false) => {
    const shipmentRequest = {
        cityId: shipment.city.id
    }
    if (shipment.stock && shipment.stock.id) {
        shipmentRequest.stockId = shipment.stock.id
    }
    if (isShipAddress && shipment.shipAddress && shipment.shipAddress.id) {
        shipmentRequest.shipAddressId = shipment.shipAddress.id
    }
    if (!shipment.city.id) {
        shipmentRequest.cityId = 1
    }
    return shipmentRequest
}

export const checkChangedShipment = (oldShipment, newShipment) => {
    if (newShipment.stock && newShipment.stock.id !== oldShipment.stock.id) return true
    if (newShipment.city && newShipment.city.id !== oldShipment.city.id) return true
    if (newShipment.shipAddress && newShipment.shipAddress.id !== oldShipment.shipAddress.id) return true
    return false
}

const urlRegular = /^(\w+:\/\/[^/?]+)?(.*?)(\?.+)?$/
const relativeUrlRegular = /^(\/\/[^/?]+)(.*?)(\?.+)?$/

const normalizeParts = (parts) =>
    parts
        .filter((part) => typeof part === 'string' || typeof part === 'number')
        .map((part) => `${part}`)
        .filter((part) => part)

const parseParts = (parts, options) => {
    const { protocolRelative } = options

    const partsStr = parts.join('/')
    const urlRegExp = protocolRelative ? relativeUrlRegular : urlRegular
    const [, prefix = '', pathname = '', suffix = ''] = partsStr.match(urlRegExp) || []

    return {
        prefix,
        pathname: {
            parts: pathname.split('/').filter((part) => part !== ''),
            hasLeading: suffix ? /^\/\/+/.test(pathname) : /^\/+/.test(pathname),
            hasTrailing: suffix ? /\/\/+$/.test(pathname) : /\/+$/.test(pathname)
        },
        suffix
    }
}

const buildUrl = (parsedParts, options) => {
    const { prefix, pathname, suffix } = parsedParts
    const { parts: pathnameParts, hasLeading, hasTrailing } = pathname
    const { leadingSlash, trailingSlash } = options
    const addLeading = leadingSlash === true || (leadingSlash === 'keep' && hasLeading)
    const addTrailing = trailingSlash === true || (trailingSlash === 'keep' && hasTrailing)
    let url = prefix
    if (pathnameParts.length > 0) {
        if (url || addLeading) {
            url += '/'
        }

        url += pathnameParts.join('/')
    }

    if (addTrailing) {
        url += '/'
    }

    if (!url && addLeading) {
        url += '/'
    }

    const query = { ...queryString.parse(suffix, options.queryOptions), ...options.query }
    const queryStr = queryString.stringify(query, options.queryOptions)

    if (queryStr) {
        url += `?${queryStr}`
    }

    return url
}

export const urlCombine = (...parts) => {
    const lastArg = parts[parts.length - 1]
    let options
    if (lastArg && typeof lastArg === 'object') {
        options = lastArg
        // eslint-disable-next-line no-param-reassign
        parts = parts.slice(0, -1)
    }

    options = {
        leadingSlash: true,
        trailingSlash: false,
        protocolRelative: false,
        ...options
    }

    // eslint-disable-next-line no-param-reassign
    parts = normalizeParts(parts)
    const parsedParts = parseParts(parts, options)
    return buildUrl(parsedParts, options)
}

export const generateUrlImage = (baseImageUrl, goodId, hasImage, size = 240) => {
    if (hasImage) {
        return urlCombine(baseImageUrl, size, `${goodId}.jpg`)
    }
    return '/images/noimg.gif'
}
export const getActualGoods = async (likedGoods) => {
    if (!likedGoods || !likedGoods.length) return []
    const cookies = parseCookies()
    const isConvertedString = cookies['is-converted-liked']
    if (isConvertedString) {
        const isConverted = JSON.parse(isConvertedString)
        if (isConverted) {
            return likedGoods
        }
    }
    const newGoodIds = await convertGoodIds(likedGoods)
    return newGoodIds
}
export const getActualBasketPosition = (basketItems) => {
    const cookies = parseCookies()
    const isConvertedBasketString = cookies['is-converted-basket']
    if (isConvertedBasketString) {
        const isConvertedBasket = JSON.parse(isConvertedBasketString)
        if (isConvertedBasket) {
            return basketItems || []
        }
    }
    return []
}
