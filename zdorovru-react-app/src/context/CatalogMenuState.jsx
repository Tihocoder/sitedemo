import React, { createContext, useState } from 'react'
import PropTypes from 'prop-types'

export const MenuContext = createContext({
    isCatalogMenuOpen: true,
    toggleMenu: () => {},
    stockSelectorByGood: {
        selectedGoodIds: 0,
        isOpened: false
    },
    toggleStockSelectorByGood: () => {}
})

const CatalogMenuState = ({ children }) => {
    const [isCatalogMenuOpen, toggleMenu] = useState(false)
    const initialStockSelectorByGood = {
        selectedGoodIds: [],
        isOpened: false
    }
    const [stockSelectorByGood, setStockSelectorByGood] = useState(initialStockSelectorByGood)

    const toggleMenuMode = () => {
        toggleMenu(!isCatalogMenuOpen)
    }
    const toggleStockSelectorByGood = (selectedGoodIds) => {
        if (stockSelectorByGood.isOpened) {
            setStockSelectorByGood(initialStockSelectorByGood)
        } else {
            setStockSelectorByGood({
                selectedGoodIds,
                isOpened: true
            })
        }
    }

    return (
        <MenuContext.Provider
            value={{
                isCatalogMenuOpen,
                toggleMenuMode,
                stockSelectorByGood,
                toggleStockSelectorByGood
            }}>
            {children}
        </MenuContext.Provider>
    )
}

CatalogMenuState.propTypes = {
    children: PropTypes.node.isRequired
}

export default CatalogMenuState
