import React, { createContext, useEffect, useReducer, useState } from 'react'
import PropTypes from 'prop-types'
import { useDispatch } from 'react-redux'
import { removeBasketItem } from '@actions/customer'
import { checkAuthorization } from '@api/siteApis'

export const AlertsStateContext = createContext({
    deleteFromBasket: {
        isShowed: false,
        openHandler: () => {},
        confirmHandler: () => {},
        closeHandler: () => {}
    },
    messageWindow: {
        isShowed: false,
        title: 'Продолжить',
        content: '',
        closeHandler: () => {},
        run: (titleButton, content) => {}
    },
    confirmCustomer: {
        isShowed: false,
        isEnabled: false,
        isNeedAuthorizeWindow: false,
        toggleIsNeedAuthorizeWindow: () => {},
        toggleClose: () => {}
    }
})

const AlertsState = ({ children }) => {
    const initaialMessageState = {
        messageWindow: {
            isShowed: false,
            title: 'Продолжить',
            content: '',
            closeHandler: () => {},
            run: () => {}
        }
    }

    function messageReducer(state, action) {
        switch (action.type) {
            case 'changeMessageState': {
                return {
                    ...state,
                    messageWindow: {
                        ...state.messageWindow,
                        ...action.payload
                    }
                }
            }
            case 'closeMessageWindow': {
                return {
                    ...state,
                    messageWindow: { ...initaialMessageState }
                }
            }
            default:
                throw new Error()
        }
    }

    const dispatch = useDispatch()
    const [message, messageDispatch] = useReducer(messageReducer, initaialMessageState)
    const [isAlertBasketShowed, setAlertBasketShowed] = useState(false)
    const [isConfirmCustomerWindow, setIsConfirmCustomerWindow] = useState(false)
    const [isConfirmCustomerEnabled, setIsConfirmCustomerEnabled] = useState(false)
    const [isNeedAuthorize, setIsNeedAuthorize] = useState(false)
    const [webId, setWebId] = useState(0)

    const checkAuthorize = async () => {
        const isAuthorization = await checkAuthorization()
        if (isAuthorization) {
            setIsConfirmCustomerEnabled(true)
        }
    }

    useEffect(() => {
        checkAuthorize()
    }, [])

    const toggleIsConfirmCustomerWindow = () => {
        setIsConfirmCustomerWindow(!isConfirmCustomerWindow)
    }
    const openHandler = (newWebId) => {
        setAlertBasketShowed(true)
        setWebId(newWebId)
    }
    const confirmHandler = async () => {
        await dispatch(removeBasketItem(webId))
        setAlertBasketShowed(false)
    }
    const closeHandler = () => {
        setAlertBasketShowed(false)
    }

    const runMessage = (titleButton, content) => {
        messageDispatch({
            type: 'changeMessageState',
            payload: {
                title: titleButton || 'Продолжить',
                content,
                isShowed: true,
                closeHandler: () => messageDispatch({ type: 'closeMessageWindow' })
            }
        })
    }

    return (
        <AlertsStateContext.Provider
            value={{
                deleteFromBasket: {
                    isShowed: isAlertBasketShowed,
                    openHandler,
                    confirmHandler,
                    closeHandler
                },
                messageWindow: {
                    ...message.messageWindow,
                    run: runMessage,
                    confirmHandler,
                    closeHandler
                },
                confirmCustomer: {
                    isShowed: isConfirmCustomerWindow,
                    isNeedAuthorizeWindow: isNeedAuthorize,
                    toggleIsNeedAuthorizeWindow: () => setIsNeedAuthorize(!isNeedAuthorize),
                    toggleClose: toggleIsConfirmCustomerWindow,
                    isEnabled: isConfirmCustomerEnabled
                }
            }}>
            {children}
        </AlertsStateContext.Provider>
    )
}

AlertsState.propTypes = {
    children: PropTypes.node.isRequired
}

export default AlertsState
